@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-3">
        <p>&nbsp;</p>
        <h5>Select Date</h5><br>
        <div id="date-log-picker"></div>
      </div>
      <div class="col-sm-9">
        <div class="table-responsive">
          <table class="table compact display row-border cell-border stripe hover order-column" id="data-admin-log">
             <thead>
                <tr>
                   <th>Date</th>
                   <th>Admin</th>
                   <th>Action</th>
                </tr>
             </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
