@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="row">
  <!-- left -->
  <div class="col-md-9">
    <div class="card-2 card-docu-holder">
        <div class="card-body-2">
          @if($contract->signed == 'yes')
          <div>
          <iframe src="/form_templates/{{$contract->uid}}.pdf" type="application/pdf" style="width: 100%; height: 120vh; padding: 0px !important;"></iframe>
          </div>
          @else
          <div style="width: 100%; height: 120vh; background-color: #fff;  overflow: auto; padding: 30px;">
            <div style="width: 100%; text-align:center"><img src="https://virtudeskpro.com/img/backend/brand/virtudesk_logo.png"></div><br><br>
            {!!html_entity_decode($doc)!!}
          </div>
          @endif
        </div>
    </div>
  </div>

  <!--right -->
  <div class="col-md-3">
    <div class="card-3">
        <div class="card-body-3">

            @if($contract->recipient_action  == 'signed')
              <div class="font-weight-bold float-left">STATUS: <span class="text-green">SIGNED</span></div>
              <div class="float-right"><a target="_blank" href="/form_templates/{{$contract->uid}}.pdf">Download</a></div><br>
            @elseif($contract->recipient_action == 'declined')
              <p class="font-weight-bold">STATUS: <span class="text-red">DECLINED</span></p>
            @else
              <p class="font-weight-bold" style="text-transform: capitalize;">STATUS: <span class="text-orange">{{$contract->signed ?? ''}}</span></p>
            @endif

            <hr>

            @php
            $email_use = $applicant->email;
            @endphp

            <p>Document Type: {{$contractType}}<br>
            Recipient: {{ucwords($applicant->name)}}<br>
            Email: {{$email_use ?? '***VD Gmail set yet'}}</p>
            <p>&nbsp;</p>

            @if(count($contract->actions) > 0)

            <p class="font-weight-bold">Dcoument Trail</p>
            <p>Document ID: {{$contract->contract_id}}</p>

              @foreach($contract->actions as $ca)
              <div class="row mb-2">
                <div class="col-sm-6">{{date('M d, Y h:i:s a', strtotime($ca->date_action))}}</div>
                <div class="col-sm-6">Recipient {{$ca->action}}<br>{{$ca->ip_address}}<br></div>
              </div>
              @endforeach

              <div class="row mb-2">
                <div class="col-sm-6">{{date('M d, Y h:i:s a', strtotime($contract->recipient_signed_date))}}</div>
                <div class="col-sm-6">Admin sent contract<br></div>
              </div>
            @endif

            <p>&nbsp;</p>

            @php
            $contract_type = [
              'caf' => 'hr-operation-record',
              'gva_quarterly' => 'hr-operation-record',
              'final' => 'hr-operation-record',
              'hold-suspension' => 'hr-operation-record',
              'isa-quarterly' => 'hr-operation-record',
              'fte-form' => 'hr-operation-record',
              'nte-form' => 'hr-operation-record',
              'termination-letter' => 'hr-operation-record',
              'disclosure-form' => 'pre-training-requirements',
              'memorandum-form' => 'pre-training-requirements',
              'assurance-form' => 'pre-training-requirements',
              'codeofconduct-form' => 'pre-training-requirements',
              'ica-form' => 'pre-training-requirements',
              'nca-form' => 'hr-operation-record',
            ];
            @endphp

            <a href="/admin/{{$contract_type[$contract->contract_type]}}/{{$contract->employee_id}}/edit">Back</a>
            @if(session('success'))
            <br><br>
            &nbsp;
            <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success</strong> {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
            @endif
        </div>

        @if($contract_type[$contract->contract_type] == 'pre-training-requirements')
        <p>&nbsp;</p>
        <div class="card-3">
          <div class="card-body-3">Send New Contract<br>
            <select class="form-control mt-2 pre-training-dp" rel="{{request()->to}}">
              <option selected disabled>Please choose</option>
              <option value="ica-form" rel="{{request()->to}}">Independent Contract</option>
              <option value="memorandum-form" rel="{{request()->to}}">Leave Memorandum</option>
              <option value="disclosure-form" rel="{{request()->to}}">Disclosure Form</option>
              <option value="assurance-form" rel="{{request()->to}}">VD Assurance Form</option>
              <option value="codeofconduct-form" rel="{{request()->to}}">Code of Conduct</option>
              <option value="nca-form" rel="{{request()->to}}">Non-Compete Agreement</option>
            </select>
          </div>
        </div>
        @endif
    </div>
  </div>
</div>
@endsection
