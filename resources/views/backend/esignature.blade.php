@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="row">
  <!-- left -->
  <div class="col-md-9">
    <div class="card-2 card-docu-holder">
        <div class="card-body-2">
          <textarea id="document_renderer" class="form-control">{{ $doc ?? '' }}</textarea>
        </div>
    </div>
  </div>

  <!--right -->
  <div class="col-md-3">
    <div class="card-3">
        <div class="card-body-3">
          @if($contract)
            @if($contract->recipient_action == 'declined')
              <p class="font-weight-bold">STATUS: <span class="text-red">DECLINED</span></p>
            @else
              <p class="font-weight-bold">STATUS: <span class="text-red">{{$contract->recipient_action}}</span></p>
            @endif
          @else
            <p>STATUS: {{$contract->signed ?? ''}}</p>

          @endif

            <hr>

            @php
            $email_use = $applicant->email;
            @endphp

            <p>Document Type: {{$contractType}}<br>
            Recipient: {{ucwords($applicant->name)}}<br>
            Email: {{$email_use ?? '***VD Gmail not set'}}</p>
            <p>&nbsp;</p>

            @if(!$contract || $contract->signed != 'sent')

            <form action="/admin/esignature-send" method="post" style="width: 60%;" id="contract_send" class="mx-auto">
              @csrf
              <textarea id="doc_body2" name="doc_body" value="" style="display: none;"></textarea>
              <input type="hidden" name="employee_id" value="{{request()->to ?? ''}}">
              <input type="hidden" name="contract_type" value="{{request()->type ?? ''}}">
              <input type="hidden" name="contract_id" value="{{$contract->uid ?? ''}}">
              <input type="hidden" name="memoid" value="{{request()->memoid ?? ''}}">
              <button {{$email_use ? '' : 'disabled'}} class="btn btn-primary" style="width: 100%;" title="{{$email_use ?? 'Email not set'}}">Send</button>
            </form>

            @endif



            <p>&nbsp;</p>
            @if($contract)
              @if(count($contract->actions) > 0)

              <p class="font-weight-bold">Document Trail</p>
              <p>Document ID: {{$contract->contract_id}}</p>

                @foreach($contract->actions as $ca)
                <div class="row mb-2">
                  <div class="col-sm-6">{{date('M d, Y h:i:s a', strtotime($ca->date_action))}}</div>
                  <div class="col-sm-6">Recipient {{$ca->action}}<br>{{$ca->ip_address}}<br></div>
                </div>
                @endforeach

                <div class="row mb-2">
                  <div class="col-sm-6">{{date('M d, Y h:i:s a', strtotime($contract->recipient_signed_date))}}</div>
                  <div class="col-sm-6">Admin sent contract<br></div>
                </div>
              @endif
            @endif

            @php
            $contract_type = [
              'caf' => 'hr-operation-record',
              'gva_quarterly' => 'hr-operation-record',
              'final' => 'hr-operation-record',
              'hold-suspension' => 'hr-operation-record',
              'isa-quarterly' => 'hr-operation-record',
              'fte-form' => 'hr-operation-record',
              'nte-form' => 'hr-operation-record',
              'termination-letter' => 'hr-operation-record',
              'disclosure-form' => 'pre-training-requirements',
              'memorandum-form' => 'pre-training-requirements',
              'assurance-form' => 'pre-training-requirements',
              'codeofconduct-form' => 'pre-training-requirements',
              'ica-form' => 'pre-training-requirements',
              'nca-form' => 'hr-operation-record',
              'ica-eva' => 'pre-training-requirements',
              'ica-isa' => 'pre-training-requirements',
              'ica-gva' => 'pre-training-requirements',
            ];

            @endphp


            <a href="/admin/pre-training-requirements/{{$applicant->id}}/edit">Back</a>

            @if(session('success'))
            <br><br>
            &nbsp;
            <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong><br>{{session('success')}}<br>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
            @endif

            @if(session('error'))
            <br><br>
            &nbsp;
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Error!</strong><br>{{session('error')}}<br>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
            @endif
        </div>
        <p>&nbsp;</p>
        <div class="card-3">

          <div class="card-body-3">
            @if($contract_type[request()->type] == 'pre-training-requirements')
            Send New Contract<br><br>
              <select class="form-control mt-2 pre-training-dp" rel="{{request()->to}}">
                <option selected disabled>Please choose</option>
                <option value="ica-form" rel="{{request()->to}}">Independent Contract</option>
                <option value="memorandum-form" rel="{{request()->to}}">Leave Memorandum</option>
                <option value="disclosure-form" rel="{{request()->to}}">Disclosure Form</option>
                <option value="assurance-form" rel="{{request()->to}}">VD Assurance Form</option>
                <option value="codeofconduct-form" rel="{{request()->to}}">Code of Conduct</option>
                <option value="nca-form" rel="{{request()->to}}">Non-Compete Agreement</option>
                </select><hr>
            @endif


            <label for="doc-uploader">
                  <span class="btn-block" aria-hidden="true" style="width: 100%;"><i class="fas fa-upload"></i> Upload from template</span>
                  <input type="file" id="doc-uploader" name="file" accept=".doc, .docx" style="display:none;">
            </label>

          </div>
        </div>
    </div>
  </div>
</div>
@include('backend.includes.new_applicant')
@endsection
