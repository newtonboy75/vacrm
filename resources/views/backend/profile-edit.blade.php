@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.edit'))

@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')

<form action="{{ route('admin.profile-update') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
{{ csrf_field() }}
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-5">
        <h4 class="card-title mb-0">
          @lang('labels.backend.access.users.management')
          <small class="text-muted">@lang('labels.backend.access.users.edit')</small>
        </h4>
      </div><!--col-->
    </div><!--row-->
    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
    <hr>

    <div class="row mt-4 mb-4">
      <div class="col">

        <div class="form-group row">
          <input type="hidden" value="{{$user->id}}" name="userid">
          {{ html()->label(__('validation.attributes.backend.access.users.first_name'))->class('col-md-2 form-control-label')->for('first_name') }}

          <div class="col-md-10">
            <input type="text" name="first_name" value="{{$user->first_name}}" class="form-control">
          </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
          {{ html()->label(__('validation.attributes.backend.access.users.last_name'))->class('col-md-2 form-control-label')->for('last_name') }}

          <div class="col-md-10">
            <input type="text" name="last_name" value="{{$user->last_name}}" class="form-control">
          </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
          {{ html()->label(__('validation.attributes.backend.access.users.email'))->class('col-md-2 form-control-label')->for('email') }}

          <div class="col-md-10">
            <input type="email" name="email" value="{{$user->email}}" class="form-control" required>
          </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
          {{ html()->label(__('validation.attributes.backend.access.users.password'))->class('col-md-2 form-control-label')->for('password') }}

          <div class="col-md-10">
            <input type="password" name="password" value="{{$user->password}}" class="form-control" required>
          </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
          {{ html()->label('Skype ID')->class('col-md-2 form-control-label')->for('skype_id') }}
          <div class="col-md-10">
            <input type="text" name="skype_id" value="{{$user->skype_id}}" class="form-control">
          </div>
        </div>

        <div class="form-group row">
          @php
            $ts = unserialize($user->timesched);

          @endphp
          {{ html()->label('Work Schedule')->class('col-md-2 form-control-label')->for('timesched') }}
          <div class="col-md-2">

            <div class="input-group mb-3">
              <input type="text" id="timesched" name="timesched[]" value="{{$ts[0]}}" required class="form-control" placeholder="Time in">
              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2"><i class="far fa-clock"></i></span>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="input-group mb-3">
              <input type="text" id="timesched" name="timesched[]" value="{{$ts[1]}}" required class="form-control" placeholder="Time out">
              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2"><i class="far fa-clock"></i></span>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group row">
          {{ html()->label('')->class('col-md-2 form-control-label')->for('timein') }}
          @php
          $dow = [];
            if(isset($user->day_of_week)){
              $dow = unserialize($user->day_of_week);
            }
          @endphp
          <div class="col-md-10">
            <div class="checkbox">
               <input value="monday" {{in_array('monday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[0]" id="day_of_week"><label class="mr-4" for="day_of_week">Monday</label>
               <input value="tuesday" {{in_array('tuesday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[1]" id="day_of_week2"><label class="mr-4" for="day_of_week2">Tuesday</label>
               <input value="wednesday" {{in_array('wednesday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[2]" id="day_of_week3"><label class="mr-4" for="day_of_week3">Wednesday</label>
               <input value="thursday" {{in_array('thursday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[3]" id="day_of_week4"><label class="mr-4" for="day_of_week4">Thrusday</label>
               <input value="friday" {{in_array('friday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[4]" id="day_of_week5"><label class="mr-4" for="day_of_week5">Friday</label>
               <input value="saturday" {{in_array('saturday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[5]" id="day_of_week6"><label class="mr-4" for="day_of_week6">Saturday</label>
               <input value="sunday" {{in_array('sunday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[6]" id="day_of_week7"><label class="mr-4" for="day_of_week7">Sunday</label>
            </div>
          </div>
        </div>

        @php
        $avatar = $user->avatar_location ?? 'temp.jpg';
        @endphp

        <div class="form-group row">
          <div class="col-md-2">Avatar</div>
          <div class="col-md-10">
            <img width="200" height="200" src="{{asset('/img/backend/employee_photo/avatar/' . $avatar)}}" name="preview" id="preview">
            <input id="avatar" type="file" name="avatar" accept=".gif, .jpg, .jpeg, .png|image/*|">
          </div>

        </div>

          </div><!--col-->
        </div><!--row-->

      </div><!--card-body-->

      <div class="card-footer">
        <div class="row">
          <div class="col-sm-6 text-right">
            {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}&nbsp;&nbsp;
            {{ form_submit(__('buttons.general.crud.update')) }}
          </div><!--col-->

          <div class="col-sm-6 float-left">

          </div><!--row-->
        </div><!--row-->
      </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
    @endsection
