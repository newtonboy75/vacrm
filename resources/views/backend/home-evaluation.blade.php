@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Applicants
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
              <div class="dropdown" id="custom-searchbox">
                  <button class="btn btn-orange btn-sm" id="btn-custom-search" data-toggle="dropdown"><i class="fas fa-search"></i> Search</button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                    <li role="presentation">
                      <div class="m-2" style="width: 220px;">
                        <input type="text" class="form-control" id="custom-search-input" placeholder="search name, email..."><br>
                        <small class="text-warning">(press enter to start searching)</small><br><br>
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="all-rec">
                          <label class="form-check-label" for="all-rec">Search all record</label>
                        </div>

                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="all-emp-stat">
                          <label class="form-check-label" for="all-emp-stat">Include inactive and archived applicants/employees</label>
                        </div>

                        <div id="cl-search"><a href="javascript:;"><i class="fas fa-times"></i> clear search</a></div>

                      </div>
                    </li>


                  </ul>
                </div>&nbsp;
              <button class="btn btn-green btn-sm float-right" id="btn-add-applicant" data-toggle="modal" data-target="#applicationModal"><i class="fas fa-user-plus"></i> Add New</button>

            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
              <div class="table-responsive">
                <table class="table compact display row-border cell-border stripe hover order-column" id="data-eval">
                   <thead>
                      <tr>
                         <th>Name</th>
                         <th>Application Date</th>
                         <th>Transition Date</th>
                         <th>Home Evaluation Result</th>
                         <th>Action</th>
                      </tr>
                   </thead>
                </table>
              </div>
            </div><!--col-->
        </div><!--row-->

    </div><!--card-body-->
</div><!--card-->
@include('backend.includes.new_applicant')
@endsection
