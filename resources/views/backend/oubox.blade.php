@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-5">
        <h4 class="card-title mb-0">
          Outbox
        </h4>

      </div><!--col-->
    </div><!--row-->
    <hr>
    <div class="row mt-3">
      <div class="col-sm-2">
        <ul class="nav flex-column custom-vert-menu">
          <li class="nav-item">
            <a class="nav-link active" rel="sourcing" href="javascript:;">Sourcing Record</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="talent_acquisition" href="javascript:;">Talent Acquisition</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="home_eval" href="javascript:;">Home Evaluation</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="pretraining" href="javascript:;">Pre-Training</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="training" href="javascript:;">Training</a>
          </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <div class="outbox-summary">
          <ul class="outbox-ul">
            <li>loading</li>
          </ul>
        </div>
      </div>
      <div class="col-sm-7">
        <div class="eml-body">loading...</div>
      </div>
    </div>

  </div><!--card-body-->
</div><!--card-->
@include('backend.includes.new_applicant')
@endsection
