@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="row">
  <div class="col">
    <div class="card">
      <div class="card-header">
        <strong>@lang('strings.backend.dashboard.welcome') {{ $logged_in_user->name }}!</strong>
      </div><!--card-header-->
      <div class="card-body">

        <div id="poll_divs">
        {{ $chart->container() }}

        </div>

      </div><!--card-body-->
    </div><!--card-->
  </div><!--col-->
</div><!--row-->

<div class="row mx-auto">
  @if($sourcing_record->count() >= 1)
  <div class="col-sm-6 card">
    <div style="padding-top:12px; padding-bottom: 12px;">
      <div class="float-left"><h5>Sourcing Record</h5></div>
      <div class="float-right"><span class="badge level level-error">{{$sourcing_record->count()}}</span>&nbsp;&nbsp;<a href="{{route('admin.sourcing-record')}}"><i class="fas fa-list"></i></a></div>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">Application Date</th>
        </tr>
      </thead>
      <tbody>
        @php $i = 0; @endphp
        @foreach($sourcing_record as $source)
          @if($i < 10)
          <tr>
            <th scope="row">{{$source->name}}</th>
            <td>{{$source->email}}</td>
            <td>{{date('M d Y', strtotime($source->created_at))}}</td>
            @php $i++; @endphp
          </tr>
          @endif
        @endforeach

      </tbody>
    </table>
  </div>
  @endif

  @if($talent_acquisition->count() >= 1)
  <div class="col-sm-6 card">
    <div style="padding-top:12px; padding-bottom: 12px;">
      <div class="float-left"><h5>Talent Acquistion Record</h5></div>
      <div class="float-right"><span class="badge level level-error">{{$talent_acquisition->count()}}</span>&nbsp;&nbsp;<a href="{{route('admin.talent-acquisition-record')}}"><i class="fas fa-list"></i></a></div>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">Application Date</th>
        </tr>
      </thead>
      <tbody>
        @php $i = 0; @endphp
        @foreach($talent_acquisition as $source)
          @if($i < 10)
          <tr>
            <th scope="row">{{$source->name}}</th>
            <td>{{$source->email}}</td>
            <td>{{date('M d Y', strtotime($source->created_at))}}</td>
            @php $i++; @endphp
          </tr>
          @endif
        @endforeach

      </tbody>
    </table>
  </div>
  @endif

  @if($reference_check->count() >= 1)
  <div class="col-sm-6 card">
    <div style="padding-top:12px; padding-bottom: 12px;">
      <div class="float-left"><h5>Reference Check Record</h5></div>
      <div class="float-right"><span class="badge level level-error">{{$reference_check->count()}}</span>&nbsp;&nbsp;<a href="{{route('admin.reference-check')}}"><i class="fas fa-list"></i></a></div>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">Application Date</th>
        </tr>
      </thead>
      <tbody>
        @php $i = 0; @endphp
        @foreach($reference_check as $source)
          @if($i < 10)
          <tr>
            <th scope="row">{{$source->name}}</th>
            <td>{{$source->email}}</td>
            <td>{{date('M d Y', strtotime($source->created_at))}}</td>
            @php $i++; @endphp
          </tr>
          @endif
        @endforeach

      </tbody>
    </table>
  </div>
  @endif

  @if($pre_training->count() >= 1)
  <div class="col-sm-6 card">
    <div style="padding-top:12px; padding-bottom: 12px;">
      <div class="float-left"><h5>Pre-Training Records</h5></div>
      <div class="float-right"><span class="badge level level-error">{{$pre_training->count()}}</span>&nbsp;&nbsp;<a href="{{route('admin.pre-training-requirements')}}"><i class="fas fa-list"></i></a></div>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">Application Date</th>
        </tr>
      </thead>
      <tbody>
        @php $i = 0; @endphp
        @foreach($pre_training as $source)
          @if($i < 10)
          <tr>
            <th scope="row">{{$source->name}}</th>
            <td>{{$source->email}}</td>
            <td>{{date('M d Y', strtotime($source->created_at))}}</td>
            @php $i++; @endphp
          </tr>
          @endif
        @endforeach

      </tbody>
    </table>
  </div>
  @endif


  @if($training)
  <div class="col-sm-6 card table-responsive">
    <div style="padding-top:12px; padding-bottom: 12px;">
      <div class="float-left"><h5>Training Records</h5></div>
      <div class="float-right"><span class="badge level level-error">{{$training->count()}}</span>&nbsp;&nbsp;<a href="{{route('admin.training-record')}}"><i class="fas fa-list"></i></a></div>
    </div>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Email</th>
          <th scope="col">Program</th>
          <th scope="col">Schedule</th>
          <th scope="col">Start Date</th>
          <th scope="col">End Date</th>
        </tr>
      </thead>
      <tbody>
        @php $i = 0; @endphp
        @foreach($training as $train)
          @if($i < 10)
          @if(isset($train->trainings[0]['program']))
          <tr>
            <th scope="row">{{$train->name ?? ''}}</th>
            <td>{{$train->email ?? ''}}</td>
            <td>{{$train->trainings[0]['program'] ?? ''}}</td>
            <td>{{$train->trainings[0]['schedule'] ?? ''}}</td>
            @php
              $startdate = explode(' ', $train->trainings[0]['start_date']);
              $enddate = explode(' ', $train->trainings[0]['end_date']);
            @endphp
            <td>{{ $train->trainings[0]['start_date']  ?? '' }}</td>
            <td>{{ $train->trainings[0]['end_date'] ?? '' }}</td>
            @php $i++; @endphp
          </tr>
          @endif
          @endif
        @endforeach

      </tbody>
    </table>
  </div>
  @endif

</div>

@endsection
