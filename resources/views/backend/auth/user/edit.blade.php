@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.edit'))

@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($user, 'PATCH', route('admin.auth.user.update', $user->id))->class('form-horizontal')->open() }}
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-5">
        <h4 class="card-title mb-0">
          @lang('labels.backend.access.users.management')
          <small class="text-muted">@lang('labels.backend.access.users.edit')</small>
        </h4>
      </div><!--col-->
    </div><!--row-->
    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
    <hr>

    <div class="row mt-4 mb-4">
      <div class="col">

        @if(Auth::user()->hasRole('administrator'))
        <div class="form-group row">
          {{ html()->label(__('User status'))->class('col-md-2 form-control-label')->for('active') }}
          <div class="col-md-10">
            <select name="active" id="active" class="form-control" style="width: 100px;">
              <option value="1" {{ $user->active == '1' ? 'selected' : '' }}>Active</option>
              <option value="0" {{ $user->active == '0' ? 'selected' : '' }}>Inactive</option>
            </select>
          </div><!--col-->
        </div><!--form-group-->
        @endif

        <div class="form-group row">
          {{ html()->label(__('validation.attributes.backend.access.users.first_name'))->class('col-md-2 form-control-label')->for('first_name') }}

          <div class="col-md-10">
            {{ html()->text('first_name')
            ->class('form-control')
            ->placeholder(__('validation.attributes.backend.access.users.first_name'))
            ->attribute('maxlength', 191)
            ->required() }}
          </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
          {{ html()->label(__('validation.attributes.backend.access.users.last_name'))->class('col-md-2 form-control-label')->for('last_name') }}

          <div class="col-md-10">
            {{ html()->text('last_name')
            ->class('form-control')
            ->placeholder(__('validation.attributes.backend.access.users.last_name'))
            ->attribute('maxlength', 191)
            ->required() }}
          </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
          {{ html()->label(__('validation.attributes.backend.access.users.email'))->class('col-md-2 form-control-label')->for('email') }}

          <div class="col-md-10">
            {{ html()->email('email')
            ->class('form-control')
            ->placeholder(__('validation.attributes.backend.access.users.email'))
            ->attribute('maxlength', 191)
            ->required() }}
          </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
          {{ html()->label(__('validation.attributes.backend.access.users.password'))->class('col-md-2 form-control-label')->for('password') }}

          <div class="col-md-10">
            {{ html()->text('password')
            ->class('form-control')
            ->placeholder(__('validation.attributes.backend.access.users.password'))
            ->attribute('maxlength', 191)
            ->required() }}
          </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
          {{ html()->label('Skype ID')->class('col-md-2 form-control-label')->for('skype_id') }}
          <div class="col-md-10">
            {{ html()->text('skype_id')
            ->class('form-control')
            ->placeholder('Skype ID')
            ->attribute('maxlength', 191)
            ->required() }}
          </div>
        </div>

        <div class="form-group row">
          @php
            $ts = unserialize($user->timesched);

          @endphp
          {{ html()->label('Work Schedule')->class('col-md-2 form-control-label')->for('timesched') }}
          <div class="col-md-2">

            <div class="input-group mb-3">
              <input type="text" id="timesched" name="timesched[]" value="{{$ts[0]}}" required class="form-control" placeholder="Time in">
              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2"><i class="far fa-clock"></i></span>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="input-group mb-3">
              <input type="text" id="timesched" name="timesched[]" value="{{$ts[1]}}" required class="form-control" placeholder="Time out">
              <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2"><i class="far fa-clock"></i></span>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group row">
          {{ html()->label('')->class('col-md-2 form-control-label')->for('timein') }}
          @php
          $dow = [];
            if(isset($user->day_of_week)){
              $dow = unserialize($user->day_of_week);
            }
          @endphp
          <div class="col-md-10">
            <div class="checkbox">
               <input value="monday" {{in_array('monday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[0]" id="day_of_week"><label class="mr-4" for="day_of_week">Monday</label>
               <input value="tuesday" {{in_array('tuesday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[1]" id="day_of_week2"><label class="mr-4" for="day_of_week2">Tuesday</label>
               <input value="wednesday" {{in_array('wednesday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[2]" id="day_of_week3"><label class="mr-4" for="day_of_week3">Wednesday</label>
               <input value="thursday" {{in_array('thursday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[3]" id="day_of_week4"><label class="mr-4" for="day_of_week4">Thrusday</label>
               <input value="friday" {{in_array('friday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[4]" id="day_of_week5"><label class="mr-4" for="day_of_week5">Friday</label>
               <input value="saturday" {{in_array('saturday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[5]" id="day_of_week6"><label class="mr-4" for="day_of_week6">Saturday</label>
               <input value="sunday" {{in_array('sunday', $dow) ? 'checked' : '' }} type="checkbox" name="day_of_week[6]" id="day_of_week7"><label class="mr-4" for="day_of_week7">Sunday</label>
            </div>
          </div>
        </div>

        @php
        $avatar = $user->avatar_location ?? 'temp.jpg';
        @endphp

        <div class="form-group row">
          <div class="col-md-2">Avatar</div>
          <div class="col-md-10">
            <img width="200" height="200" src="{{asset('/img/backend/employee_photo/avatar/' . $avatar)}}" name="preview" id="preview"> <input id="avatar" type="file" name="avatar" accept=".gif, .jpg, .jpeg, .png|image/*|" value="{{$user->avatar_location}}">
          </div>

        </div>
        @if(Auth::user()->hasRole('administrator'))
        <div class="form-group row">
          {{ html()->label('Abilities')->class('col-md-2 form-control-label') }}

          <div class="table-responsive col-md-10">
            <table class="table">
              <thead>
                <tr>
                  <th>@lang('labels.backend.access.users.table.roles')</th>
                  <th>@lang('labels.backend.access.users.table.permissions')</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    @if($roles->count())
                    @foreach($roles as $k=>$role)
                      <div class="cards">
                          <div class="checkbox">
                            <input value="{{ $role->name }}" type="checkbox" name="roles[]" id="role-{{$role->id}}" {{in_array($role->name, $userRoles) ? 'checked' : ''}}><label class="text-capitalize mr-4" for="role-{{$role->id}}">{{$role->name}}</label>
                          </div>
                      </div><!--card-->
                    @endforeach
                    @endif
                    </td>
                    <td>
                      @if($permissions->count())
                      @foreach($permissions as $permission)
                        <div class="checkbox d-flex align-items-center">
                          <input value="{{ $permission->name }}" type="checkbox" name="permissions[]" id="permission" {{in_array($permission->name, $userPermissions) ? 'checked' : ''}}><label class="text-capitalize mr-4" for="permission">{{$permission->name}}</label>
                        </div>
                      @endforeach
                      @endif
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div><!--col-->
            </div><!--form-group-->
            @else

            @if($roles->count())
            @foreach($roles as $k=>$role)

                    <input value="{{ $role->name }}" type="hidden" name="roles[]" id="role-{{$role->id}}" {{in_array($role->name, $userRoles) ? 'checked' : ''}}>
<!--card-->
            @endforeach
            @endif

            @if($permissions->count())
            @foreach($permissions as $permission)

                <input value="{{ $permission->name }}" type="hidden" name="permissions[]" id="permission" {{in_array($permission->name, $userPermissions) ? 'checked' : ''}}>

            @endforeach
            @endif



            @endif
          </div><!--col-->
        </div><!--row-->

      </div><!--card-body-->

      <div class="card-footer">
        <div class="row">
          <div class="col-sm-6 text-right">
            {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}&nbsp;&nbsp;
            {{ form_submit(__('buttons.general.crud.update')) }}
          </div><!--col-->

          <div class="col-sm-6 float-left">

          </div><!--row-->
        </div><!--row-->
      </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
    @endsection
