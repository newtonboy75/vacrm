@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col">
        <h4 class="text-muted header-underlined-wide">
          Recruiter's Hub
        </h4>
      </div><!--col-->
    </div><!--row-->

    <div class="row mt-4">
      <div class="col-md-1"></div>
      <div class="col-md-7">
        <!-- if admin -->
        <div class="mb-4" id="btns-recruiters" style="display: inline-block; width: 100%;">

        </div>
        <div style="display: inline-block; width: 100%;">
          <div class="float-left">
            <button class="btn btn-sm btn-danger" id="today">Today</button>&nbsp;&nbsp;<button class="btn btn-sm btn-secondary" id="tomorrow">Tomorrow</button>
          </div>

          <div class="float-right pr-1">
            <button class="btn btn-sm btn-light glyphicon glyphicon-chevron-left" id="prev"></button>&nbsp;&nbsp;<button class="btn btn-sm btn-light glyphicon glyphicon-chevron-right" id="next"></button>
          </div>

        </div>
        <!-- if admin -->

        <div class="timeline-wrapper">
          <ul class="timeline">
          </ul>
                  </div>
                  <br>
                  <div class="float-right pr-4"><span class="font-weight-bold mr-5">total interviews:</span><span class="total"></span></div>
                </div>
                <div class="col-md-3">

                  <div class="mt-4 mb-4">
                    <div id="datetimepicker12" style="background: #fafafa; padding: 9px; border-radius: 4px; border: 1px #f0f0f0 solid;"></div>
                    <small class="text-muted text-center">click to change date</small>
                  </div>

                  <div class="date-options">
                    <a href="javascript:;" class="float-right" id="close-option">close</a>
                    <p>Set a schedule <span id="date-to"></span></p>

                    <div class="mb-2">
                      <div class="block">RECRUITERS</div>
                      <select class="form-control" id="recruiters_opt">
                      </select>
                    </div>

                    <div id="start_time_sched" style="display: inline-block; margin-bottom: 10px !important;">
                      <div class="block">FROM</div>
                      <div class="float-left">
                        <select class="form-control" style="width: 60px !important;" id="from-hour">
                          <option value="01">1</option>
                          <option value="02">2</option>
                          <option value="03">3</option>
                          <option value="04">4</option>
                          <option value="05">5</option>
                          <option value="06">6</option>
                          <option value="07">7</option>
                          <option value="08">8</option>
                          <option value="09">9</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                        </select>
                      </div>

                      <div class="float-left pl-2 pr-2">:</div>

                      <div class="float-left">
                        <select class="form-control" style="width: 60px !important; float: left !important;" id="from-min">
                          <option value="00">00</option>
                          <option value="15">15</option>
                          <option value="30">30</option>
                          <option value="45">45</option>
                        </select>
                      </div>

                      <div class="float-left ml-1 ">
                        <select class="form-control" style="width: 70px !important; float: left !important;" id="from-tod">
                          <option value="am">am</option>
                          <option value="pm">pm</option>
                        </select>
                      </div>
                    </div>

                    <div id="end_time_sched">
                      <div class="block">TO</div>
                      <div class="float-left">
                        <select class="form-control" style="width: 60px !important;" id="to-hour">
                          <option value="01">1</option>
                          <option value="02">2</option>
                          <option value="03">3</option>
                          <option value="04">4</option>
                          <option value="05">5</option>
                          <option value="06">6</option>
                          <option value="07">7</option>
                          <option value="08">8</option>
                          <option value="09">9</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                        </select>
                      </div>

                      <div class="float-left pl-2 pr-2">:</div>

                      <div class="float-left">
                        <select class="form-control" style="width: 60px !important; float: left !important;" id="to-min">
                          <option value="00">00</option>
                          <option value="15">15</option>
                          <option value="30">30</option>
                          <option value="45">45</option>
                        </select>
                      </div>

                      <div class="float-left ml-1 ">
                        <select class="form-control" style="width: 70px !important; float: left !important;" id="to-tod">
                          <option value="am">am</option>
                          <option value="pm">pm</option>
                        </select>
                      </div>
                    </div>

                    <div style="display: inline-block"><br><br>
                      <button id="set_sked" class="btn btn-success float-right">Set Schedule</button>&nbsp;&nbsp;
                      <button id="cancel_sked" class="btn btn-danger float-left">Cancel</button>
                    </div>

                  </div>

                  <div>
                  <p class="font-weight-bold">Recruiters</p>
                  <ul class="recruiters-list">

                  </ul>
                  <p><i class="fas fa-square" style="color: #333;"></i>&nbsp;&nbsp;Unavailable</p>
                </div>
                </div>
                <div class="col-md-1"></div>
              </div><!--row-->
            </div><!--card-body-->
          </div><!--card-->

          <!---->
          <div class="modal-contents" id=exampleModal>
            <div class="modal-body">

              <div class="row" style="height: 100%";>
                <div class="col-sm-6">
                  <div id="calendar-recruiter" style="height: 460px; overflow: auto"></div>
                </div>
                <div class="col-sm-6">
                  <span class="text-muted">Set Unavailable Slots</span>
                  <div id="datetimepicker14" style="background: #fafafa; margin:4px; padding: 9px; border-radius: 4px; border: 1px #f0f0f0 solid;"></div>
                  <div class="text-muted mt-2" id="recruiter-available-slot"><span>Available timeslot</span><br>
                    <ul id="available-time"></ul>
                    <span class="text-muted">Note</span>
                    <textarea class="form-control mt-t-2" name="reason" id="tl-note" placeholder="optional note"></textarea>
                    <input type="hidden" id="caldate" value="">
                    <input type="hidden" id="recruiter_id" value="">
                    <div id="tl-result"></div>
                    <button id="set_new_sked" class="btn btn-primary mx-auto mt-2 mt-3">Set Schedule</button>
                  </div>
                </div>
              </div>

            </div>

          </div>


          @endsection
