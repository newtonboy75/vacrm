@extends('backend.layouts.app')
@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))
@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection
@section('content')

@if (Session::has('error'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  {{Session::get('error')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="card">
  <div class="card-body p-2">
    <div class="row">
      <div class="col-sm-5">
        @if(\Route::current()->getName() == 'admin.sourcing-record.edit')
        @include('backend.includes.partials.sourcing-bar')
        @elseif(\Route::current()->getName() == 'admin.talent-acquisition-record.edit')
        @include('backend.includes.partials.talent-acquisition-bar')
        @elseif(\Route::current()->getName() == 'admin.reference-check.edit')
        @include('backend.includes.partials.user-actions-reference')
        @elseif(\Route::current()->getName() == 'admin.training-record.edit')
        @include('backend.includes.partials.user-actions-training')
        @elseif(\Route::current()->getName() == 'admin.pre-training-requirements.edit')
        @include('backend.includes.partials.user-actions-pre_training')
        @elseif(\Route::current()->getName() == 'admin.placement-record.edit')
        @include('backend.includes.partials.user-actions-placement-record')
        @endif

      </div><!--col-->
      <div class="col-sm-7">
        @php
        $prev_url = explode('/', url()->previous());
        @endphp

        @if(\Route::current()->getName() == 'admin.hr-operation-record.edit')
            @include('backend.includes.partials.header-buttons')
        @elseif(\Route::current()->getName() == 'admin.employee')
            @include('backend.includes.partials.header-employees')
        @elseif($prev_url[4] == 'hr-operation-record' && $prev_url[6] == 'edit')
            @include('backend.includes.partials.header-employees')
        @endif
      </div><!--col-->
    </div><!--row-->
  </div>
</div>
<p class="alert alert-success" id="message-alert"></p>
<div class="card mt-1">
  <div class="card-body">
    <div class="row">
      <div class="col card-pad">
        @if(\Route::current()->getName() == 'admin.training-record.edit')
        @include('backend.includes.tabs.training_records')
        @elseif(\Route::current()->getName() == 'admin.placement-record.edit')
        @include('backend.includes.tabs.placement')
        @elseif(\Route::current()->getName() == 'admin.hr-operation-record.edit')
        @include('backend.includes.tabs.hr_record')
        @else
        <ul class="nav nav-tabs" id="steps" role="tablist">

          <li class="nav-item">
            <a class="nav-link" id="source-tab" data-toggle="tab" href="#source" role="tab" aria-controls="source" aria-selected="false" rel="sourcing-record">Sourcing Records</a>
          </li>


          <li class="nav-item">
            <a class="nav-link" id="talent-tab" data-toggle="tab" href="#talent" role="tab" aria-controls="talent" aria-selected="false" rel="talent-acquisition-record">Talent Acquisition</a>
          </li>


          <li class="nav-item">
            <a class="nav-link" id="reference-tab" data-toggle="tab" href="#reference" role="tab" aria-controls="reference" aria-selected="false" rel="reference-tab">Reference Check</a>
          </li>




      <li class="nav-item">
        <a class="nav-link" id="pretraining-tab" data-toggle="tab" href="#pretraining" role="tab" aria-controls="settings" aria-selected="false" rel="pre-training-requirements">Pre-training Requirements</a>
      </li>
    </ul>

    <div class="tab-content">

      <div class="tab-pane" id="source" role="tabpanel" aria-labelledby="sourcing-record">
        @include('backend.includes.tabs.sourcing')
      </div>

      <div class="tab-pane" id="talent" role="tabpanel" aria-labelledby="talent-acquisition-record">
        @include('backend.includes.tabs.talent-acquisition')
      </div>

      <div class="tab-pane" id="reference" role="tabpanel" aria-labelledby="reference-check">
        @include('backend.includes.tabs.reference-check')
      </div>





  <div class="tab-pane" id="pretraining" role="tabpanel" aria-labelledby="pre-training-requirements">
    @include('backend.includes.tabs.pre-training')
  </div>

</div>
@endif

@if(\Route::current()->getName() != 'admin.hr-operation-record.edit')
<div class="row top-underlined">
  <div class="col">
    <button class="btn btn-success" id="applicant-record-save"><i class="fas fa-save"></i> Update</button>&nbsp;&nbsp;
    <a role=button href="{{ URL::previous() }}" class="btn btn-success" id="applicant-record-save-exit"><i class="fas fa-save"></i> <i class="fas fa-list"></i> Back to list</a>&nbsp;&nbsp;<button disabled id="message" class="float-dleft" style="border: 0px !important; background: #fff;"></button>
  </div>
</div>
@else
<div class="row top-underlined">
  <div class="col">
    <button class="btn btn-success" id="hr-record-save"><i class="fas fa-save"></i> Update</button>&nbsp;&nbsp;
    <a role=button href="{{ URL::previous() }}" class="btn btn-success" id="applicant-record-save-exit"><i class="fas fa-save"></i> <i class="fas fa-list"></i> Back to list</a>&nbsp;&nbsp;<button disabled id="message" class="float-dleft" style="border: 0px !important; background: #fff;"></button>
  </div>
</div>
@endif
</div><!--col-->
</div><!--row-->

</div><!--card-body-->
</div><!--card-->
@endsection
