@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<p class="alert alert-success" id="message-alert"></p>
<div class="row">
  <div class="col">
    <!--tab-->
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="dashboard-tab" data-toggle="tab" href="#dashboard" role="tab" aria-controls="dashboard" aria-selected="false">Dashboard</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="lists-tab" data-toggle="tab" href="#lists" role="tab" aria-controls="lists" aria-selected="true">Clients List</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" id="lists-notes" data-toggle="tab" href="#notes" role="tab" aria-controls="notes" aria-selected="true">Notes</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" id="placement-tab" data-toggle="tab" href="#placement" role="tab" aria-controls="placement" aria-selected="true">Placement Record</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="cancellation-tab" data-toggle="tab" href="#cancellation" role="tab" aria-controls="cancellation" aria-selected="false">Cancellations</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="suspension-tab" data-toggle="tab" href="#suspension" role="tab" aria-controls="suspension" aria-selected="false">Suspensions</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" id="replacement-tab" data-toggle="tab" href="#replacement" role="tab" aria-controls="replacement" aria-selected="false">Replacements</a>
      </li>

    </ul>

    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="dashboard" role="tabpanel" aria-labelledby="dashboard-tab" style="width: 99% !important;">@include('backend.includes.clients.tab0')</div>

      <div class="tab-pane fade" id="lists" role="tabpanel" aria-labelledby="lists-tab">
        <div style="width: 100% !important;">@include('backend.includes.clients.tab5')</div>
      </div>

      <div class="tab-pane fade" id="placement" role="tabpanel" aria-labelledby="placement-tab">
        <div style="width: 100% !important;">@include('backend.includes.clients.tab1')</div>
      </div>

      <div class="tab-pane fade" id="notes" role="tabpanel" aria-labelledby="notes-tab">
        <div style="width: 100% !important;">@include('backend.includes.clients.tabnotes')</div>
      </div>

      <div class="tab-pane fade" id="cancellation" role="tabpanel" aria-labelledby="cancellation-tab">
        <div class="table-responsive" style="width: 99% !important;">@include('backend.includes.clients.tab2')</div>
      </div>

      <div class="tab-pane fade" id="suspension" role="tabpanel" aria-labelledby="suspension-tab" style="width: 99% !important;">@include('backend.includes.clients.tab3')</div>

      <div class="tab-pane fade" id="replacement" role="tabpanel" aria-labelledby="replacement-tab" style="width: 99% !important;">@include('backend.includes.clients.tab4')</div>

    </div>
    <!--tab-->
  </div><!--col-->
</div>


@endsection
