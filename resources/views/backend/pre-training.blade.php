@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <p class="card-title mb-0 font-weight-bold">
                  &centerdot; TRAINEES &centerdot;
              </p>
            </div><!--col-->
            <div class="col-sm-7">
              <div class="dropdown" id="custom-searchbox">
                  <button class="btn btn-orange btn-sm" id="btn-custom-search" data-toggle="dropdown"><i class="fas fa-search"></i> Search</button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                    <li role="presentation">
                      <div class="m-2" style="width: 220px;">
                        <input type="text" class="form-control" id="custom-search-input" placeholder="search name, email...">
                        <small class="text-warning">(press enter to start searching)</small><br><br>
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="all-rec">
                          <label class="form-check-label" for="exampleCheck1">Search all record</label>
                        </div>

                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="all-emp-stat">
                          <label class="form-check-label" for="exampleCheck1">Include inactive and archived applicants/employees</label>
                        </div>

                        <div id="cl-search"><a href="javascript:;"><i class="fas fa-times"></i> clear search</a></div>

                      </div>
                    </li>


                  </ul>
                </div>&nbsp;
              <button class="btn btn-green btn-sm float-right" id="btn-add-applicant" data-toggle="modal" data-target="#applicationModal"><i class="fas fa-user-plus"></i> Add New</button>

              <div class="float-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-danger dropdown-toggle mr-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-cog"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="/admin/sourcing-record/export-ref" data-toggle="modal" data-target="#popupDataExport"><i class="fas fa-file-export"></i> Export Data</a>
                    <a class="dropdown-item" role="button" href="/admin/sourcing-record/export-ref" data-toggle="modal" data-target="#popupStatsModal"><i class="far fa-chart-bar"></i> View Stats</a>
                  </div>
                </div>
              </div>

            </div><!--col-->
        </div><!--row-->

        <div class="row">
          <div class="col-sm-12">
            <ul class="nav nav-tabs mt-5" id="pretraining-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Pre-Training</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="batches-tab" data-toggle="tab" href="#batches" role="tab" aria-controls="batches" aria-selected="false">Batches</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent" class="width: 100%;">
              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="table-responsive" class="width: 100%; !important">
                <table class="table cell-border  display stripe hover order-column" id="data-pre" class="width: 100%; !important">
                   <thead>
                      <tr>
                        <th>ID</th>
                         <th>Name</th>
                         <th>Email</th>
                         <th>Application Date</th>
                         <th>Workflow Status</th>
                         <th>Recruiter Notes</th>
                         <th>Action</th>
                      </tr>
                   </thead>
                </table>
              </div></div>
              <!--batches -->
              <div class="tab-pane fade" id="batches" role="tabpanel" aria-labelledby="batches-tab">
                <div class="float-right mb-3"><button id="create-new-batch-record" class="btn btn-orange" data-toggle="modal" data-target="#batchModal">Create New Batch Record</button></div>
                <table class="table compact display row-border cell-border stripe hover order-column" id="data-batches">
                   <thead>
                      <tr>
                        <th>#</th>
                         <th>Batch #</th>
                         <th>Start Training</th>
                         <th>End Training</th>
                         <th>Numer of Trainees</th>
                         <th>Batch Email Sent</th>
                         <th>Action</th>
                      </tr>
                   </thead>
                </table>
              </div>
            </div>
          </div>
        </div>

    </div><!--card-body-->
</div><!--card-->
<div class="modal fade" id="batchModal" tabindex="-1" role="dialog" aria-labelledby="batchModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="batchModalLabel">Create New Batch Record</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-4">Batch Number</div>
          <div class="col-sm-8"><input class="form-control" type="text" name="batch_num" id="batch_num"></div>
        </div>

        <div class="row mt-2">
          <div class="col-sm-4">Start Date</div>
          <div class="col-sm-8"><input data-zdp_readonly_element="false" class="form-control dpk-batch" type="text" name="start_date" id="start_date"></div>
        </div>

        <div class="row mt-2">
          <div class="col-sm-4">End Date</div>
          <div class="col-sm-8"><input data-zdp_readonly_element="false" class="form-control dpk-batch" type="text" name="end_date" id="end_date"></div>
        </div>

        <div class="row mt-2">
          <div class="col-sm-4">Time</div>
          <div class="col-sm-8">
            <label>Start: <input data-zdp_readonly_element="false" class="form-control dpk-batch-time" type="text" name="start_time" id="start_time"><br>
            <label>End: <input data-zdp_readonly_element="false" class="form-control dpk-batch-time" type="text" name="end_time" id="end_time">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save_new_batch">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- view batch -->
<div class="modal fade" id="viewBatchModalCenter" tabindex="-1" role="dialog" aria-labelledby="viewBatchModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="viewBatchModalLongTitle">View Batch</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <input type="hidden" id="batch_id">
          <div class="col-sm-3">Batch Number:</div><div class="col-sm-9"> <input type="text" id="batch_num_p2" class="form-control" style="width: 100px"></div>
        </div>

        <div class="row  mt-2">
          <div class="col-sm-3">Start Date:</div><div class="col-sm-9"> <input type="text" id="start_date_p2" class="form-control" style="width: 300px"></div>
        </div>

        <div class="row  mt-2">
          <div class="col-sm-3">End Date:</div><div class="col-sm-9"> <input type="text" id="end_date_p2" class="form-control" style="width: 300px"></div>
        </div>

        <div class="row mt-2">
          <div class="col-sm-12 table-responsive">
            <br>
            <strong>Trainees</strong><br>
            <table class="table table-sm">
              <thead>
                 <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Program</th>
                 </tr>
              </thead>
              <tbody id="all_trainees">

              </tbody>

            </table>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <div class="text-green" id="info-bulk"></div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="send_bulk_mail"><i class="fas fa-mail-bulk"></i> Send Bulk Email</button>
      </div>
    </div>
  </div>
</div>

@include('backend.includes.tab_chart')
@include('backend.includes.new_applicant')
@endsection
