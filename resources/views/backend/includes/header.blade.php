<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{ route('admin.dashboard') }}">
        <img class="navbar-brand-full" src="https://virtudeskcrm.s3.us-west-2.amazonaws.com/images/virtudesk_logo_final.png" width="82" alt="Virtudesk Admin">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span id="tglr">
        <svg viewBox="0 0 100 80" width="14" height="14">
            <rect width="100" height="14"></rect>
            <rect y="30" width="100" height="14"></rect>
            <rect y="60" width="100" height="14"></rect>
        </svg>
    </span>
    </button>

    <ul class="nav nav-tabs d-md-down-none main-nav">
        <li class="nav-item">
            <a class="nav-link {{
              active_class(Route::is('admin.sourcing-record*'))
            }}" href="{{ route('admin.sourcing-record') }}">Sourcing<br>Record</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{
              active_class(Route::is('admin.talent-acquisition-record*'))
            }}" href="{{ route('admin.talent-acquisition-record') }}">Talent Acquisition<br>Record</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{
              active_class(Route::is('admin.reference-check*'))
            }}" href="{{ route('admin.reference-check') }}">Reference Check<br>Record</a>
        </li>
        <!--
        <li class="nav-item">
            <a class="nav-link {{
              active_class(Route::is('admin.home-evaluation-record*'))
            }}" href="{{ route('admin.home-evaluation-record') }}">Home Evaluation<br>Record</a>
        </li>
      -->

        <li class="nav-item">
            <a class="nav-link {{ active_class(Route::is('admin.pre-training-requirements*')) }}" href="{{ route('admin.pre-training-requirements') }}">Pre-training<br>Record</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{
              active_class(Route::is('admin.training-record*'))
            }}" href="{{ route('admin.training-record') }}">Training<br>Record</a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{
              active_class(Route::is('admin.placement-record*'))
            }}" href="{{ route('admin.placement-record') }}">Placement<br>Record</a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{ active_class(Route::is('admin.hr-operation-record*')) }}" href="{{ route('admin.hr-operation-record') }}">Operations<br>Record</a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{
              active_class(Route::is('admin.recruiters-hub*'))
            }}" href="{{ route('admin.recruiters-hub') }}">Recruiter's<br>Hub</a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{ active_class(Route::is('admin.clients*')) }} {{ active_class( request()->segment(3) == 'client' ) }}" href="{{ route('admin.clients') }}">Clients</a>
        </li>
    </ul>

    @php
    $avatar = $logged_in_user->avatar_location ?? 'temp.jpg';
    @endphp

    <ul class="nav navbar-nav ml-auto mr-3">
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="{{asset('/img/backend/employee_photo/avatar/' . $avatar)}}" class="img-avatar" alt="{{ $logged_in_user->email }}">

          </a>
          <div class="dropdown-menu dropdown-menu-right">

            <a class="dropdown-item" href="/admin/profile/view">
                <i class="fas fa-lock"></i> Account
            </a>


            <a class="dropdown-item" href="{{ route('frontend.auth.logout') }}">
                <i class="fas fa-lock"></i> @lang('navs.general.logout')
            </a><br>
          </div>
        </li>
    </ul>

    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
    </button>
</header>
