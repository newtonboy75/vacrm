<!-- Modal -->
@php

$fname = '';
  $lname = '';
  
if(isset($interview->interview_record)){
  $allrecs = App\Models\Auth\User::whereId($interview->interview_record[0]->recruiter_id)->first();

  $recruiter_name = '';
  

  $recruiter_id = $interview->interview_record[0]->recruiter_id;

  if($recruiter_id){
    $fname = $allrecs->first_name ?? '';
    $lname = $allrecs->last_name ?? '';
    $recruiter_name = $fname.' '.$lname;
    $skype = $allrecs->skype_id ?? '';
    $rec_email = $allrecs->email ?? '';
  }else{
    $fname = $applicant->recruiter_name->first_name ?? '';
    $lname = $applicant->recruiter_name->last_name ?? '';
    $recruiter_name = $fname.' '.$lname;
    $skype = $applicant->recruiter_name->skype_id ?? '';
    $rec_email = $applicant->recruiter_name->email ?? '';
  }

  if($recruiter_id == '12'){
    $rec_email = 'jobs@myvirtudesk.com';
    $recruiter_name = $fname;
  }
}
@endphp

<div class="modal fade" id="sendReschedBs" tabindex="-1" role="dialog" aria-labelledby="sendReschedBs" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:700px;">
      <div class="modal-header">
        <h5 class="modal-title" id="sendReschedBs">Email Editor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">To</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="to" id="to" value="{{ $applicant->email ?? ''}}">
        </div>

        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Subject</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="subject" id="subject" value="RE: {{ucwords($interview->interview_record[0]->type ?? '')}} Interview Schedule">
        </div>

        <div class="form-group">
          <label for="address" class="f-label">Body</label>
          <textarea name="email-body" id="email-body-resched-bs" class="form-control" required rows="10">

            <p style="text-align:left"><span style="color:#000000"><span style="font-size:small"><strong>Dear {{$applicant->name ?? ''}},</strong></span></span><span style="color:#000000"><span style="font-size:small">&nbsp;&nbsp;&nbsp;&nbsp;</span></span></p>

            <p style="text-align:left"><span style="font-size:small">We would like to inform you that your Interview schedule <strong>has been moved to a different time</strong></span><span style="font-size:small">&nbsp;due&nbsp;to slight changes with the&nbsp;business&nbsp;structure.&nbsp;</span></p>

            <p style="text-align:left"><span style="font-size:small">Please take note of your </span><span style="font-size:small"><strong>new {{ucwords($interview->interview_record[0]->type ?? '')}} Interview Schedule</strong></span><span style="font-size:small">:</span></p>

            <p style="text-align:left"><span style="font-size:small"><strong>Date: </strong></span><span style="color:#ff0000"><span style="font-size:small"><strong>{{ isset($interview->interview_record[0]->date) ? date('F d, Y', strtotime($interview->interview_record[0]->date)) : '' }} </strong></span></span></p>

            <p style="text-align:left"><span style="font-size:small"><strong>Time:</strong></span> <span style="color:#ff0000"><span style="font-size:small"><strong>{{ isset($interview->interview_record[0]->date )? date('h:i A', strtotime($interview->interview_record[0]->date)) : '' }} MNL TIME</strong></span></span></p>

            <p style="text-align:left"><span style="font-size:small"><strong>Interviewer:</strong></span> <span style="color:#ff0000"><span style="font-size:small"><strong>{{ $fname.' '.$lname }}</strong></span></span></p>

            <p style="text-align:left"><span style="font-size:small"><strong>Skype ID:</strong></span><span style="font-size:small">&nbsp;</span><span style="color:#0000ff"><span style="font-size:small"><strong>{{$skype ?? ''}}</strong></span></span><span style="color:#ff0000"><span style="font-size:small"><strong>&nbsp;</strong></span></span><span style="color:#000000"><span style="font-size:small"><strong>or</strong></span></span><span style="font-size:small"><strong> </strong></span><span style="color:#0000ff"><span style="font-size:small"><strong>{{ $recruiter_name ?? '' }} | Talent Acquisition </strong></span></span><span style="color:#000000"><span style="font-size:small"><strong>or</strong></span></span><span style="color:#ff0000"><span style="font-size:small"><strong>&nbsp;</strong></span></span><a href="{{$rec_email ?? ''}}" style="color:#0000ff; text-decoration:underline" target="_blank"><span style="font-size:small"><strong>{{$rec_email ?? ''}}</strong></span></a></p>

            <p style="text-align:left"><span style="font-size:small"><strong><span style="background-color:#ffff00">IMPORTANT NOTE: Please follow the same set of instructions sent on the first email.&nbsp;</span></strong></span></p>

            <p style="text-align:left"><span style="font-size:small">We would like to apologize for the change and we appreciate your flexibility.</span></p>

            <p><strong><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing &amp; Talent Acquisition Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
            <span style="font-size:small"><strong>Email: </strong></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br />
            <strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></strong></p>

          </textarea>
          <script>
          CKEDITOR.replace( 'email-body-resched-bs' );
          </script>
        </div>
      </div>
      <div class="modal-footer">
        <span class="wait-status text-warning"></span>
        <input type="hidden" name="email_type" value="resched-bs" id="email_type">
        <input type="hidden" name="training_id" value="{{ $applicant->last_training->id ?? '' }}" id="training_id">
        <input type="hidden" name="user_id" value="{{$applicant->id ?? ''}}" id="user_id">
        <input type="hidden" name="from" value="jobs@myvirtudesk.com|Sourcing &amp; Talent Acquisition Team" id="from_admin">
        <input type="hidden" name="subject" value="Re: MyVirtudesk Application" id="subject">
        <button id="email_dismiss" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="presched-bs" rel="redo">Send Email and Apply Transition</button>
      </div>
    </div>
  </div>
</div>
