<!-- Modal -->
<div class="modal fade" id="sendTaIncompleteEmailModal" tabindex="-1" role="dialog" aria-labelledby="sendTaIncompleteEmailModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:700px;">
      <div class="modal-header">
        <h5 class="modal-title" id="sendTaIncompleteEmailModal">Email Editor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">To</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="to" id="to" value="{{ $applicant->email ?? ''}}">
        </div>

        <div class="form-group">
          <label for="address" class="f-label">Body</label>
          <textarea name="email-body" id="email-body-tain" class="form-control" required rows="10">
            @include('emails.ta-incomplete')
          </textarea>
          <script>
          CKEDITOR.replace( 'email-body-tain' );
          </script>
        </div>
      </div>
      <div class="modal-footer">
        <span class="wait-status text-warning"></span>
        <input type="hidden" name="email_type" value="congratulatory" id="email_type">
        <input type="hidden" name="user_id" value="{{$applicant->id ?? ''}}" id="user_id">
        <button id="email_dismiss" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="sender4" rel="ta-inc">Send Email and Apply Transition</button>
      </div>
    </div>
  </div>
</div>
