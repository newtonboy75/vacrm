<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Virtudesk</title>
  <meta name="description" content="Virtudesk">
  <meta name="author" content="@yield('meta_author', 'Newtonboy Tugadi')">
  @yield('meta')

  {{-- See //laravel.com/docs/5.5/blade#stacks for usage --}}
  @stack('before-styles')

  <!-- Check if the language is set to RTL, so apply the RTL layouts -->
  <!-- Otherwise apply the normal LTR layouts -->
  {{ style(mix('css/backend.css')) }}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('css/bootstrap-datetimepicker-standalone.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css">
  @stack('after-styles')
  <script src="/js/ckeditor/ckeditor.js"></script>
  <link href="//cdn.jsdelivr.net/gh/StephanWagner/jBox@v1.0.5/dist/jBox.all.min.css" rel="stylesheet">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/zebra_datepicker@1.9.13/dist/css/bootstrap/zebra_datepicker.min.css">
  <link href="{{ asset('css/dt.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/fixedcolumns/3.2.1/css/fixedColumns.dataTables.min.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css"/>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500&display=swap" rel="stylesheet">

</head>

{{--
  * CoreUI BODY options, add following classes to body to change options
  * // Header options
  * 1. '.header-fixed'					- Fixed Header
  *
  * // Sidebar options
  * 1. '.sidebar-fixed'					- Fixed Sidebar
  * 2. '.sidebar-hidden'				- Hidden Sidebar
  * 3. '.sidebar-off-canvas'		    - Off Canvas Sidebar
  * 4. '.sidebar-minimized'			    - Minimized Sidebar (Only icons)
  * 5. '.sidebar-compact'			    - Compact Sidebar
  *
  * // Aside options
  * 1. '.aside-menu-fixed'			    - Fixed Aside Menu
  * 2. ''			    - Hidden Aside Menu
  * 3. '.aside-menu-off-canvas'	        - Off Canvas Aside Menu
  *
  * // Breadcrumb options
  * 1. '.breadcrumb-fixed'			    - Fixed Breadcrumb
  *
  * // Footer options
  * 1. '.footer-fixed'					- Fixed footer
  --}}
  <body class="app header-fixed sidebar-fixed aside-menu-off-canvas sidebar-fixed aside-menu-off-canvas">
    @include('backend.includes.header')

    <div class="app-body">
      @include('backend.includes.sidebar')

      <main class="main">
        @include('includes.partials.read-only')
        @include('includes.partials.logged-in-as')
        {!! Breadcrumbs::render() !!}

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="content-header">
              @yield('page-header')
            </div><!--content-header-->
            @include('includes.partials.messages')
            @yield('content')
          </div><!--animated-->
        </div><!--container-fluid-->
      </main><!--main-->

      @include('backend.includes.aside')
    </div><!--app-body-->

    @include('backend.includes.footer')

    @if(Request::is('admin/sourcing-record/*'))
    @include('backend.includes.popup_email_interview')
    @include('backend.includes.popup_email_interview_manual')
    @include('backend.includes.popup_email_interview_turn_down')
    @include('backend.includes.popup_sms_sender')
    @include('backend.includes.popup_email_reapplied')
    @include('backend.includes.popup_email_nonfilipino')
    @include('backend.includes.popup_resend_cv')
    @endif

    @if(Request::is('admin/talent-acquisition-record/*'))
    @include('backend.includes.popup_email_interview_passed')
    @include('backend.includes.popup_new_email_interview_initial')
    @include('backend.includes.popup_new_email_interview_final')
    @include('backend.includes.popup_email_interview_failed')
    @include('backend.includes.popup_resched_bs')
    @include('backend.includes.popup_email_home_eval_ref')
    @include('backend.includes.popup_email_regret')

    @endif

    @if(Request::is('admin/reference-check/*'))
    @include('backend.includes.popup_email_home_eval')
    @include('backend.includes.popup_email_regret')
    @endif

    @if(Request::is('admin/home-evaluation-record/*'))
    @include('backend.includes.popup_email_congratulatory')
    @include('backend.includes.popup_email_redo')
    @endif

    @if(Request::is('admin/pre-training-requirements/*'))
    @include('backend.includes.popup_pre_sched')
    @include('backend.includes.popup_pre_email')

    @include('backend.includes.popup_email_popup_ta_incomplete')
    @include('backend.includes.popup_email_popup_tacomplete')
    @include('backend.includes.popup_email_popup_taincomplete')
    @endif

    <!-- Scripts -->
    @stack('before-scripts')

    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://www.google.com/jsapi"></script>

    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/backend.js')) !!}
    @stack('after-scripts')


    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"></script>
    <script src="{{ asset('js/eonasdan/bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js') }}"></script>

    <script type="text/javascript" src="//cdn.datatables.net/v/dt/dt-1.10.20/af-2.3.4/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/cr-1.5.2/fc-3.3.0/kt-2.5.1/r-2.2.3/rg-1.1.1/rr-1.2.6/sc-2.0.1/sl-1.3.1/datatables.min.js"></script>
    <script src="//cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/custom_calendar.js') }}"></script>
    <script src="//cdn.jsdelivr.net/gh/StephanWagner/jBox@v1.0.5/dist/jBox.all.min.js"></script>

    <script src="//cdn.jsdelivr.net/npm/zebra_datepicker@latest/dist/zebra_datepicker.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/mammoth/1.4.9/mammoth.browser.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    @if(request()->segment(2) == 'dashboard')
    {{ $chart->script() }}
    @endif


    <script>
    $('[id="dpk-ref"]').Zebra_DatePicker({
      format: 'M d, Y h:mm A',
      direction: 0,
      show_icon: false,
      first_day_of_week : 0,
      enabled_minutes: [0, 15, 30, 45, 60]
    });

    var modal = new jBox('Modal', {
      width: 640,

      attach: '#recruiter_modal',
      title: 'Recruiter\s Calendar',
      content: $('#exampleModal'),
    });

    $('#datetimepicker14').datetimepicker({
      inline: true,
      sideBySide: false,
      format: 'DT',
    });


    $('.datetimepicker').datetimepicker({
      inline: false,
      sideBySide: true,
      format: 'MMMM DD, YYYY h:mm A'
    });

    var current_recruiter_on_pp = '';

    $(document).on('click', '#recruiter_modal', function(){
      current_recruiter_on_pp = '';
      var format = 'YYYY-MM-DD h:mm:ss';
      var cal_date = moment().format(format);
      var id = $(this).attr('rel');
      current_recruiter_on_pp = id;


      var format = 'YYYY-MM-DD';
      var cal_date = moment().format(format);
      $('#caldate').val(cal_date);
      $('#recruiter_id').val(id);
      load_recruiter_calendar(id, cal_date);
      modal.open();
    });

    /***
    function load_recruiter_calendar(id, cal_date){
      $('#calendar-recruiter').html('');
      //this
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: '/admin/get_interview_info_by_recruiter',
          type: 'post',
          data: { id: id, date: cal_date, external: 1 },
          success: function(data) {
            //console.log(data);
            if(data === '<ul class="ul-rec-tab"></ul>'){
              $('#calendar-recruiter').html('<strong>no schedule set for today</strong>');
            }else{
              $('#calendar-recruiter').html(data[0]);
              $('#available-time').html(data[1]);
            }
          }
        });
      }
      ***/

      $("#datetimepicker14").on("dp.change", function (e) {
        $('#caldate').val('');
        var date = e.date;//e.date is a moment object
        var target = $(e.target).attr('name');
        var format = 'YYYY-MM-DD h:mm:ss';
        var current = moment(date, format).format(format);
        //alert(current_recruiter_on_pp);


        var format2 = 'YYYY-MM-DD';
        var cal_date2 = moment(date, format2).format(format2)
        $('#caldate').val(cal_date2);
        load_recruiter_calendar(current_recruiter_on_pp, current);
      });

      $(document).on('click', '#ts', function(){
        var date_selected  = '';
        var mmm = $('#caldate').val().split(' ');
        var time = $(this).text();
        //$('[id="ts"]').removeClass('selected');

        date_selected = mmm[0] + ' ' + time ;
        $('#caldate').val(mmm[0]);
        $(this).toggleClass('selected');

      });

      $('#set_new_sked').click(function(){

        var final_dates = [];
        var dt = $('#caldate').val();


        $('button.selected').each(function (index, value) {
          final_dates.push(dt + ' ' + $(this).text());
        });

        //console.log(final_dates);

        var recruiter_id = $('#recruiter_id').val();
        var note = $('#tl-note').val();


        if(final_dates.length <= 0){
          alert('No time selected');
        }else{
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              url: '/admin/set_schedule_by_recruiter',
              type: 'post',
              data: {recruiter_id: recruiter_id, date: final_dates, note: note},
              success: function(data) {
                //console.log(data);
                if(data == '1'){
                  alert('New schedule has been set');
                  load_recruiter_calendar(recruiter_id, dt);
                }
              }
            });
          }
        });

        </script>

        <script>
        var cur_url = '{{ request()->segment(2) }}';
        var steps = $('#steps');

        var steps = $('#steps');
        var step_num = '50';
        switch(cur_url) {
          case 'sourcing-record':
          step_num = 0;
          break;
          case 'talent-acquisition-record':
          step_num = 1;
          break;
          case 'reference-check':
          step_num = 2;
          break;
          //case 'home-evaluation-record':
          //step_num = 3;
          //break;
          case 'pre-training-requirements':
          step_num = 3;
          break;
          case 'employee':
          step_num = 3;
          break;
        }

        $('#steps li').eq(step_num).show();
        $('#steps li').eq(step_num).find('a').addClass('active');
        $('.tab-pane').eq(step_num).addClass('active');
        $('.nav-dropdown').addClass('open');
        $('.nav-dropdown-items li').eq(step_num).find('a').addClass('active');
        $('#steps li').eq(step_num).prevAll().show();

        //remember tab
        $('a[data-toggle="tab"]').on("shown.bs.tab", function(e) {
          //alert($(this).attr('href'));
          setCookie('last_tab', '', 0);
          setCookie('last_tab', $(this).attr('id'), '1');
        });

        var lastTab = getCookie('last_tab');

        $('#'+lastTab).click();

        $('.main-nav li a').click(function(){
          setCookie('last_tab', '', 0);
        });

        </script>
        <script>
        $('[id="timesched"]').datetimepicker({
          inline: false,
          sideBySide: false,
          format: 'LT',
          stepping: 30,
        });

        $('input.dpk').Zebra_DatePicker({
          format: 'Y-m-d H:i A',
          direction: 0,
          default_position: 'below',
          show_icon: false,
          enabled_minutes: [0, 15, 30, 45, 60]
        });

        $('input.dpk-d-only').Zebra_DatePicker({
          format: 'F d, Y',
          direction: 0,
          default_position: 'below',
          show_icon: false
        });

        </script>
        @if(request()->segment(2) === 'clients')
          <script src="{{ asset('js/clients.js') }}"></script>
          @endif

          @if(request()->segment(2) === 'clients' && request()->segment(3) === 'client')

          <script>
          var url = $(location).attr('href'),
          parts = url.split("/"),
          last_part = parts[parts.length-1];

          $('#data-client-placement').DataTable({
            heightStyle: "content",
            select: 'single',
            processing: true,
            serverSide: true,
            "bDestroy": true,
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            deferRender: true,
            scroller: false,
            scrollCollapse: true,
            paging: true,
            dom: 'Blfrtip',
            ajax: {
              url: '/admin/clients/single/'+last_part,
              dataType: 'json',
              cache:false,
              type: 'GET',
            },
            "aaSorting": [],
            "order": [[ 0, "desc" ]],
            "columnDefs": [

              {
                "targets": 13,
                "data": "id",
                "orderable": false,
                "visible": true,
                "render": function ( data, type, row, meta ) {
                  return '<span id="del-client-info-s" rel="'+data+'" title="delete"><i class="fas fa-times"></i></span>';
                },
              },
            ],
            "columns": [
              { data: 'id', name: 'id' },
              { data: 'current_va_start_date', name: 'current_va_start_date' },
              { data: 'status', name: 'status' },
              { data: 'marketing_specialist', name: 'marketing_specialist' },
              { data: 'hiring_objective', name: 'hiring_objective' },
              { data: 'contract_signed', name: 'contract_signed' },
              { data: 'current_va', name: 'current_va' },
              { data: 'employment_status', name: 'employment_status' },
              { data: 'contract_tb1', name: 'contract_tb1' },
              { data: 'cancellation_date', name: 'cancellation_date' },
              { data: 'cancellation_reason', name: 'cancellation_reason' },
              { data: 'suspension_date', name: 'suspension_date' },
              { data: 'suspension_reason', name: 'suspension_reason' },
            ]
          });

          //client notes
          $('#data-client-note').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            deferRender: true,
            scroller: false,
            scrollCollapse: true,
            paging: true,
            dom: 'Blfrtip',
            ajax: {
              url: '/admin/clients/note/'+last_part,
              dataType: 'json',
              cache:false,
              type: 'GET',
            },
            "aaSorting": [],
            "order": [[ 0, "desc" ]],
            "columnDefs": [
              {
                "targets": 3,
                "data": "note",
                "orderable": false,
                "visible": true,
                "render": function ( data, type, row, meta ) {
                  return '<a href="javascript:;" id="view-note" rel="'+data[0]+'">'+data[1]+'</a>';
                },
              },
              {
                "targets": 8,
                "data": "id",
                "orderable": false,
                "visible": true,
                "render": function ( data, type, row, meta ) {
                  return '<a href="javascript:;" id="del-client-note-s" rel="'+data+'" title="delete"><i class="fas fa-times"></i></a>&nbsp;&nbsp;&nbsp;';
                },
              },
              {
                "targets": 7,
                "data": "client",
                "orderable": false,
                "visible": true,
                "render": function ( data, type, row, meta ) {
                  return '<a href="#" id="clientinfo" rel="'+data[0]+'" title="view">'+data[1]+'</a>';
                },
              },
            ],
            "columns": [
              { data: 'id', name: 'id' },
              { data: 'date', name: 'date' },
              { data: 'note_type', name: 'note_type' },
              { data: 'note', name: 'note' },
              { data: 'interaction_type', name: 'interaction_type' },
              { data: 'status', name: 'status' },
              { data: 'noted_by', name: 'noted_by' },
              { data: 'client', name: 'client' },
            ]
          });

          </script>
        @endif

        @if(request()->segment(2) === 'hr-operation-record')
          <script src="{{ asset('js/contracts.js') }}"></script>
        @endif

        @if(request()->segment(2) === 'esignature')
          <script src="{{ asset('js/doc_uploader.js') }}"></script>
        @endif

        @if(request()->segment(2) === 'auth' && request()->segment(5) === 'edit')
          <script>
          //alert('test');
          $('#avatar').change(function(event){
            var output = document.getElementById('preview');
            output.src = URL.createObjectURL(event.target.files[0]);


            var files = new FormData(); // you can consider this as 'data bag'
            files.append('avatar', event.target.files[0]); // append selected file to the bag named 'file'

            var user_id = $('#user_id').val();
            files.append('user_id', user_id);

            var first_name = $('#first_name').val();
            files.append('first_name', first_name);

            var last_name = $('#last_name').val();
            files.append('last_name', last_name);

            $.ajax({
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              type: 'post',
              url: '/admin/avatar',
              processData: false,
              contentType: false,
              data: files,
              success: function (response) {
                //console.log(response);
              },
              error: function (err) {
                //console.log(err);
              }
            });

          });
          </script>
        @endif

        @if(request()->segment(2) === 'clients' && !request()->segment(3) )

          <script type="text/javascript">
          google.charts.load("current", {packages:["corechart"]});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {

            var record={!! json_encode($pie) !!};
            console.log(record);
            // Create our data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'status');
            data.addColumn('number', 'status_num');
            for(var k in record){
              var v = record[k];

              data.addRow([k,v]);
              console.log(v);
            }
            var options = {
              title: 'Client Placement Status',
              is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
          }
          </script>

          <script type="text/javascript">
          google.charts.load("current", {packages:["corechart"]});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {

            var record={!! json_encode($dyear) !!};
            var dt = new Date();
            //console.log(record);
            // Create our data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'timezone');
            data.addColumn('number', 'status_num');
            for(var k in record){
              var v = record[k];

              data.addRow([k,v]);
              console.log(v);
            }
            var options = {
              title: 'Client Placement Status for ' + (new Date).getFullYear(),
              is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart-year'));
            chart.draw(data, options);
          }
          </script>
        @endif


        @if(request()->segment(2) === 'outbox')
          <script>
          $('.custom-vert-menu li').first().find('a').trigger('click');
          </script>
        @endif

      </body>
      </html>
