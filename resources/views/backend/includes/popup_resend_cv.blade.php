<!-- Modal -->
<div class="modal fade" id="sendResendModal" tabindex="-1" role="dialog" aria-labelledby="sendResendModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:700px;">
      <div class="modal-header">
        <h5 class="modal-title" id="sendResendModal">Email Editor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">To</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="to" id="to" value="{{ $applicant->email ?? ''}}">
        </div>

        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Subject</span>
          </div>
          <input type="text" class="form-control" placeholder="Subject" aria-label="Subject" aria-describedby="basic-addon1" name="subject" id="subject" value="Virtudesk Application">
        </div>

        <div class="form-group">
          <label for="address" class="f-label">Body</label>
          <textarea name="email-body" id="email-body-resend-cv" class="form-control" required rows="10">

            <p style="text-align:left"><span style="background-color:#ffffff"><span style="font-size:small"><strong>Dear {{$applicant->name ?? ''}},</strong></span></span></p>

            <p><span style="font-size:small">Thank you for taking an interest in being part of the Virtudesk family.</span></p>

            <p><span style="font-size:small">The application you have sent doesn&rsquo;t contain any resume. Please click <strong><a href="{{config('app.vdesk_url')}}/resubmit-cv"><u>here</u></a></strong> and upload a copy of your most updated resume in PDF format.</span></p>




            <p><strong><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing &amp; Talent Acquisition Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
            <span style="font-size:small"><strong>Email: </strong></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br />
            <strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></strong></p>





          </textarea>
          <script>
          CKEDITOR.replace( 'email-body-resend-cv' );
          </script>
        </div>
      </div>
      <div class="modal-footer">
        <span class="wait-status text-warning"></span>
        <input type="hidden" name="email_type" value="resend-cv" id="email_type">
        <input type="hidden" name="user_id" value="{{$applicant->id ?? ''}}" id="user_id">
        <input type="hidden" name="from" value="jobs@myvirtudesk.com|Sourcing and Talent Acquistion Team" id="from_admin">
        <input type="hidden" name="subject" value="Re: MyVirtudesk Application" id="subject">
        <button id="email_dismiss" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="resend_cv_email" rel="redo">Send Email</button>
      </div>
    </div>
  </div>
</div>
