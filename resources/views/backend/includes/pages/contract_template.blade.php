<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Virtudesk</title>
  <meta name="description" content="Virtudesk">
  <meta name="author" content="@yield('meta_author', 'Newtonboy Tugadi')">
  @yield('meta')
  @stack('before-styles')

  <!-- Check if the language is set to RTL, so apply the RTL layouts -->
  <!-- Otherwise apply the normal LTR layouts -->
  {{ style(mix('css/backend.css')) }}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('css/contract.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css">
  @stack('after-styles')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>

<div class="contract-wrapper mx-auto">
    @if($contract)
      <div class="contract-header"><img src="https://virtudeskpro.com/img/backend/brand/virtudesk_logo.png"></div>
      <div class="contract-body">
        @if($doc !='')
          {!!html_entity_decode($doc)!!}

          <div class="sign-area">
            <div class="button"><a class="btn btn-success btn-lg" href="{{route('sign', ['id' => $contract->id.'-'.$contract->uid, 'action' => 'signed'])}}">Sign | {{ucwords($contract->employee->name)}}</a></div>
            <div>I acknowledge that I have read the content of this document, and I'm entering into a legally binding agreement by clicking the Sign button. <br>I give my consent to the use of electronic communications and records related to this agreement.  You can <a class="text-orange" href="{{route('sign', ['id' => $contract->id.'-'.$contract->uid, 'action' => 'declined'])}}">decline here</a>.</div>
          </div>

        @else
          Contract document not found
        @endif
      </div>
    @else
    No contract found
    @endif
</div>


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="http://malsup.github.io/min/jquery.form.min.js"></script>
</body>
</html>
