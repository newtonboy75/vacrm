<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Virtudesk</title>
  <meta name="description" content="Virtudesk">
  <meta name="author" content="@yield('meta_author', 'Newtonboy Tugadi')">
  @yield('meta')
  @stack('before-styles')

  <!-- Check if the language is set to RTL, so apply the RTL layouts -->
  <!-- Otherwise apply the normal LTR layouts -->
  {{ style(mix('css/backend.css')) }}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('css/contract.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css">
  @stack('after-styles')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>

<div class="contract-wrapper mx-auto" style="height: 100vh">
      @if($recipient_action != 'none')
        <div class="contract-header"><img src="https://virtudeskpro.com/img/backend/brand/virtudesk_logo.png"></div>
      @endif

      <div class="contract-body text-center">
        @if($recipient_action == 'signed')
          <h5>Congratulations, you've signed the {{$contractType}}</h5>
          You will receive the finalized document shortly.
        @elseif($recipient_action == 'declined')
          <h5>You declined to sign this document</h5>
        @else
          @if($doc != '')
            <iframe src="/form_templates/{{$doc}}" style="width: 100%; height: 100vh;"></iframe>
          @else
            <h5>Sorry, your contract has expired.</h5>
          @endif

        @endif
      </div>
</div>


<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="http://malsup.github.io/min/jquery.form.min.js"></script>
<script>
history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function (event)
{
  history.pushState(null, document.title, location.href);
});
</script>
</body>
</html>
