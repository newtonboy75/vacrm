<footer class="app-footer">
    <div>
        <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }}
            <a href="https://virtudeskpro.com">
                @lang('strings.backend.general.boilerplate_link')
            </a>
        </strong> @lang('strings.backend.general.all_rights_reserved')
    </div>
</footer>
