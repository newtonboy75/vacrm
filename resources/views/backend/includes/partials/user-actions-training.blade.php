<div class="p-2">
  <div class="btn-group btn-group-sm" role="group">
    <button id="userActions" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application Transitions</button>
    <div class="dropdown-menu" aria-labelledby="userActions">
      <a href="javascript:;" class="dropdown-item" id="popupTrainingResched">Send Training Re-schedule</a>
      <a href="javascript:;" class="dropdown-item" data-toggle="modal" data-target="#interviewInviteModalSMS">Send SMS</a>
      <a href="/admin/auth/user/1/password/change" id="create_placement_record" class="dropdown-item" data-toggle="modal" data-target="#createPlacementRecord">Create Placement Record</a>
   </div>
 </div>
</div>
