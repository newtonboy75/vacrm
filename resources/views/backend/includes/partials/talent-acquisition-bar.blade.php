<div class="p-2">
  <div class="btn-group btn-group-sm" role="group">
    <button id="userActions" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application Transitions</button>
    <div class="dropdown-menu" aria-labelledby="userActions">
      <a href="/admin/auth/user/1/password/change" class="dropdown-item" data-toggle="modal" data-target="#sendReferenceEmailModal">Send Reference Form Email</a>
      <a href="javascript:;" class="dropdown-item" data-toggle="modal" data-target="#sendNewInitial" rel="initial">Send Initial Schedule Email</a>
      <a href="javascript:;" class="dropdown-item" data-toggle="modal" data-target="#sendNewFinal" rel="final">Send Final Schedule Email</a>
      <a href="javascript:;" class="dropdown-item" data-toggle="modal" data-target="#sendRescheduleEmailModal">Send Reschedule Acknowlegement Email</a>
      <a href="javascript:;" class="dropdown-item" data-toggle="modal" data-target="#sendReschedBs">Send Reschedule Due to Business Structure Email</a>
      <a href="javascript:;" class="dropdown-item" data-toggle="modal" data-target="#sendFailedEmailModal">Send Failed Interview Email</a>
      <a href="/admin/auth/user/1/password/change" class="dropdown-item" data-toggle="modal" data-target="#sendHomeEvalModal">Send Congratulatory Email</a>
      <a href="/admin/auth/user/1/password/change" class="dropdown-item" data-toggle="modal" data-target="#sendRegretModal">Send Regret Email</a>
      
      
   </div>
 </div>
</div>
