<div class="p-2 float-right">
  <div class="btn-group btn-group-sm" role="group">
    <button id="userActions" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
    <div class="dropdown-menu" aria-labelledby="userActions">

      @if(request()->segment(2) == 'employee')
        <a href="/admin/hr-operation-record/{{request()->segment(3)}}/edit" class="dropdown-item"><i class="fas fa-align-justify"></i> Back to list</a>
      @else
        <a href="/admin/{{ request()->segment(2) }}" class="dropdown-item"><i class="fas fa-align-justify"></i> Back to list</a>
      @endif
   </div>
 </div>
</div>
