@if($breadcrumbs)
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.dashboard') }}">Home</a> </li>

        @foreach($breadcrumbs as $breadcrumb)
            @if($breadcrumb->url && !$loop->last)
                <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @else
                @php
                  $bc = explode('/', $breadcrumb->url);
                @endphp

                @if(request()->segment(2) == 'log-viewer')
                  <li class="breadcrumb-item active"><a href="/admin/log-viewer/logs">{{ $breadcrumb->title }}</a></li>
                @elseif(request()->segment(2) == 'auth')
                  <li class="breadcrumb-item active"><a href="/admin/auth/user">{{ $breadcrumb->title }}</a></li>
                @else
                  @if(isset($bc[6]))
                    <li class="breadcrumb-item active"><a href="{{ route('admin.'.$bc[4]) }}">{{ $breadcrumb->title }}</a></li>
                  @endif

                @endif

            @endif
        @endforeach


    </ol>
@endif
