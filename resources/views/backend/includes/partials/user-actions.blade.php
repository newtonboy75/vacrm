<a href="{{route('admin.sourcing-record.edit', ['id' => $id])}}" title="@lang('buttons.general.crud.edit')" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i> Edit</a>
