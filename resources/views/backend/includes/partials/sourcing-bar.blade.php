<div class="p-2">
  <div class="btn-group btn-group-sm" role="group">
    <button id="userActions" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions &  Transitions</button>
    <div class="dropdown-menu" aria-labelledby="userActions">
      <a href="javascript:;" class="dropdown-item" data-toggle="modal" data-target="#interviewInviteModalSMS">Send SMS</a>
      <a href="javascript:;" class="dropdown-item" id="manual_send_email">Send Initial Interview Email (Manual)</a>
      <a href="javascript:;" class="dropdown-item" id="preset_send_email">Send Initial Interview Email (Calendar)</a>
      <a href="javascript:;" class="dropdown-item" id="turndown_send_email">Send Turn Down Email</a>
      <a href="javascript:;" class="dropdown-item" id="resend_email">No CV or Resend CV Email</a>
      <a href="javascript:;" class="dropdown-item" id="reapplied">Send Email for Reapplication</a>
      <a href="javascript:;" class="dropdown-item" id="nonpinoy">Send Email for Non-Filipino applicants</a>
    </div>
  </div>
</div>
