<div class="p-2">
  <div class="btn-group btn-group-sm" role="group">
    <button id="userActions" type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Application Transitions</button>
    <div class="dropdown-menu" aria-labelledby="userActions">
      <a href="#" id="create_training_record" class="dropdown-item" data-toggle="modal" data-target="#createPlacementRecord">Create Training Record</a>


      <a href="#" id="training-sched" class="dropdown-item" data-toggle="modal" data-target="#sendTrainingSched">Send Training Schedule</a>
      <a href="#" id="training-email" class="dropdown-item" data-toggle="modal" data-target="#sendTrainingEmail">Send Email (acknowledgement)</a>
      
      <a href="/admin/auth/user/1/password/change" class="dropdown-item" data-toggle="modal" data-target="#sendTaIncompleteEmailModal">Send No Requirements Email</a>
      <a href="/admin/auth/user/1/password/change" class="dropdown-item" data-toggle="modal" data-target="#sendTaCompleteSchedModal">Send Training Schedule- Complete Requirements</a>
      <a href="/admin/auth/user/1/password/change" class="dropdown-item" data-toggle="modal" data-target="#sendTaIncompleteSchedModal">Send Training Schedule - Incomplete Requirements</a>
   </div>
 </div>
</div>
