<a href="{{route('admin.talent-acquisition-record.edit', ['id' => $id])}}" title="@lang('buttons.general.crud.edit')" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i> Edit</a>
