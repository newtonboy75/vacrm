<!-- Modal -->
<div class="modal fade" id="interviewInviteModalSMS" tabindex="-1" role="dialog" aria-labelledby="interviewInviteModalSMS" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:400px;">
      <div class="modal-header">
        <h5 class="modal-title" id="interviewInviteModalSMS">SMS Sender</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">To</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="to" id="to" value="{{ $applicant->mobile_number ?? ''}}">
        </div>
        <div class="form-group">
          <label for="address" class="f-label">Body</label>
          <textarea name="email-body_" id="sms_text_body" class="form-control mb-2" required rows="5">Hi {{$applicant->name ?? ''}}, </textarea>
          <span>Characters: </span><span id="text_count">0</span>/160
        </div>
      </div>
      <div class="modal-footer">
        <span class="wait-status text-warning"></span>
        <input type="hidden" name="user_id" value="{{$applicant->id ?? ''}}" id="user_id">
        <button id="email_dismiss" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="send_sms">Send SMS</button>
      </div>
    </div>
  </div>
</div>
