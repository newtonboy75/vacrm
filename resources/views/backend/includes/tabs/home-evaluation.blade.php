 <div class="row">
  <div class="col table-responsive">
    <div class="field-box">
      <input type="hidden" name="from_tab" id="from_tab" value="home-evaluation-record">

      @foreach($applicant->home_evaluation_record as $eval)
      <input type="hidden" name="applicant_id" value="{{$applicant->id}}">
      <h5 class="text-muted header-underlined-wide mt-4">Home Office Evaluation</h5>
      <div class="mt-5 row">
        <div class="col-sm-6"><label class="top-label">What type of house/accommodation do you live in?</label></div>
        <div class="col-sm-6"><select name="accommodation_type" required class="form-control">
          <option disabled selected>Please choose</option>
          <option value="house" {{ $eval->accommodation_type == 'house' ? 'selected' : '' }}>House (owned)</option>
          <option value="apartment" {{ $eval->accommodation_type == 'apartment' ? 'selected' : '' }}>Apartment</option>
          <option value="townhouse" {{ $eval->accommodation_type == 'townhouse' ? 'selected' : '' }}>Townhouse</option>
          <option value="condo" {{ $eval->accommodation_type == 'condo' ? 'selected' : '' }}>Condominium Unit</option>
        </select></div>
      </div>

      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">What type of neighborhood/street are you living in?</label></div>
        <div class="col-sm-6">
          <div class="form-check">
            <input {{ $eval->kind_of_neighborhood == 'busy' ? 'checked' : '' }} required type="radio" class="form-check-input" id="kind_of_neighborhood" name="kind_of_neighborhood" value="busy">
            <label class="form-check-label" for="kind_of_neighborhood">Busy Street (Near the main road, tricycle or jeepney terminals, etc.)</label>
          </div>
          <div class="form-check">
            <input {{ $eval->kind_of_neighborhood == 'quiet' ? 'checked' : '' }} required type="radio" class="form-check-input" id="kind_of_neighborhood2" name="kind_of_neighborhood" value="quiet">
            <label class="form-check-label" for="kind_of_neighborhood2">Quiet</label>
          </div>
        </div>
      </div>

      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">How long have you lived in this place?</label></div>
        <div class="col-sm-6"><input required type="number" class="form-control" name="living_duration" value="{{ $eval->living_duration ?? '' }}" placeholder="Number of years/duration"></div>
      </div>

      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">How many people are you living with?</label></div>
        <div class="col-sm-6"><input required type="number" class="form-control" name="commoners_count" value="{{ $eval->commoners_count ?? '' }}"></div>
      </div>

      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">How many kids (ages 10 years old and below) are living with you?</label></div>
        <div class="col-sm-6"><input required type="number" class="form-control" name="commoners_count" value="{{ $eval->num_kids ?? '' }}"></div>
      </div>

      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">Who are taking care of the kids, primarily?</label></div>
        <div class="col-sm-6"><input type="text" class="form-control" name="kids_take_care" value="{{ $eval->kids_take_care ?? '' }}"></div>
      </div>

      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">Who will take care of the kids while you're at work?</label></div>
        <div class="col-sm-6"><input type="text" class="form-control" name="kids_take_care_work" value="{{ $eval->kids_take_care_work ?? '' }}"></div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">How often do you experience power outages in your area? Please give an estimated number per month.</label></div>
        <div class="col-sm-6"><input type="text" class="form-control" name="power_outage" value="{{ $eval->power_outage ?? '' }}"></div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">Is your area prone to getting flooded during typhoon season?</label></div>
        <div class="col-sm-6"><div class="form-check">
          <input required type="radio" class="form-check-input" id="flood1" name="bad_weather_flooded" value="yes" {{ $eval->bad_weather_flooded == 'yes' ? 'checked' : '' }}>
          <label class="form-check-label" for="flood1">Yes</label>
        </div>
        <div class="form-check">
          <input required type="radio" class="form-check-input" id="flood2" name="bad_weather_flooded" value="no" {{ $eval->bad_weather_flooded == 'no' ? 'checked' : '' }}>
          <label class="form-check-label" for="flood2">No</label>
        </div>
        <br>
        <p>If Yes, what is your contingency plan if ever your area gets flooded?</p>
        <input type="text" class="form-control" id="flood_plan" name="flood_plan" value="{{ $eval->flood_plan ?? '' }}" placeholder="If Yes, what is your contingency plan if ever your area gets flooded?"></div>
      </div>


      <h5 class="text-muted header-underlined-wide mt-4">VA HOME INFORMATION</h5>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">Where will you be setting up your home office?</label></div>
        <div class="col-sm-6"><input type="text" name="place_setup" value="{{ $eval->place_setup ?? '' }}" class="form-control"></div>
      </div>

      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">Please describe your Home Office Setup</label></div>
        <div class="col-sm-6"><textarea name="describe_space" class="form-control">{{ $eval->describe_space ?? '' }}</textarea></div>
      </div>

      <h5 class="text-muted header-underlined-wide mt-4">System information</h5>

      @php $result = $eval->result; @endphp
      @endforeach

      @foreach($applicant->home_evaluation_record as $system_check)

      <div class="mt-4 row">
        <div class="col-sm-12"><small>MAIN COMPUTER UNIT</small></div>
        <div class="col-sm-6 mt-2"><label class="top-label">Type of Computer to be used:</label></div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="type_computer" name="type_computer" value="pc" {{ $system_check->system->type_computer == 'pc' ? 'checked' : '' }}>
            <label class="form-check-label" for="type_computer">Desktop/PC</label>
          </div>
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="type_computer2" name="type_computer" value="laptop" {{ $system_check->system->type_computer == 'laptop' ? 'checked' : '' }}>
            <label class="form-check-label" for="type_computer2">Laptop</label>
          </div>
        </div>
      </div>
      <!---->
      <div class="mt-4 row">

        <div class="col-sm-6 mt-2 top-label">Processor</div><div class="col-sm-6 mt-2"><input type="text" class="form-control" required name="main_system_proc" value="{{ $system_check->system->main_system_proc ?? '' }}"></div>
        <div class="col-sm-6 mt-4 top-label">RAM (Minimum of 4GB)</div><div class="col-sm-6 mt-4"><input type="text" class="form-control" required name="main_system_ram" value="{{ $system_check->system->main_system_ram ?? '' }}"></div>

        <div class="col-sm-6 mt-4 top-label">What Operating System is it running on</div><div class="col-sm-6 mt-4"><input type="text" class="form-control" required name="main_system_os" value="{{ $system_check->system->main_system_os ?? '' }}"></div>

        <div class="col-sm-6 mt-4 top-label">What type of architecture is your main system running on?</div>
        <div class="col-sm-6 mt-4">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="main_ram_proc_type" name="main_ram_proc_type" value="32" {{ $system_check->system->main_ram_proc_type == '32' ? 'checked' : '' }}>
            <label class="form-check-label" for="main_ram_proc_type">32-bit</label>
          </div>

          <div class="form-check">
            <input required type="radio" class="form-check-input" id="main_ram_proc_type" name="main_ram_proc_type" value="64" {{ $system_check->system->main_ram_proc_type == '64' ? 'checked' : '' }}>
            <label class="form-check-label" for="main_ram_proc_type2">64-bit</label>
          </div>
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-12"><small>BACKUP SYSTEM SPECIFICATION</small></div>
        <div class="col-sm-6 mt-2 top-label">Processor</div><div class="col-sm-6 mt-2"><input type="text" class="form-control" required name="backup_system_proc" value="{{ $system_check->system->backup_system_proc ?? '' }}"></div>
        <div class="col-sm-6 mt-4 top-label">RAM (Minimum of 4GB)</div><div class="col-sm-6 mt-4"><input type="text" class="form-control"  name="backup_system_ram" value="{{ $system_check->system->backup_system_ram ?? '' }}"></div>

        <div class="col-sm-6 mt-4 top-label">What Operating System is it running on</div><div class="col-sm-6 mt-4"><input type="text" class="form-control" required name="backup_system_os" value="{{ $system_check->system->backup_system_os ?? '' }}"></div>

        <div class="col-sm-6 mt-4 top-label">What type of architecture is your backup system running on?</div>
        <div class="col-sm-6 mt-4">
          <div class="form-check">
            <input type="radio" class="form-check-input" id="backup_ram_proc_type" name="backup_ram_proc_type" value="32" {{ $system_check->system->backup_ram_proc_type == '32' ? 'checked' : '' }}>
            <label class="form-check-label" for="backup_ram_proc_type">32-bit</label>
          </div>

          <div class="form-check">
            <input type="radio" class="form-check-input" id="backup_ram_proc_type" name="backup_ram_proc_type" value="64" {{ $system_check->system->backup_ram_proc_type == '64' ? 'checked' : '' }}>
            <label class="form-check-label" for="backup_ram_proc_type">64-bit</label>
          </div>
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 mt-2 top-label">Type of Computer to be used:</div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="type_computer_backup" name="type_computer_backup" value="pc" {{ $system_check->system->type_computer_backup == 'pc' ? 'checked' : '' }}>
            <label class="form-check-label" for="type_computer_backup">Desktop/PC</label>
          </div>
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="type_computer_backup" name="type_computer_backup" value="laptop" {{ $system_check->system->type_computer_backup == 'laptop' ? 'checked' : '' }}>
            <label class="form-check-label" for="type_computer_backup">Laptop</label>
          </div>
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Is this backup system readily available for you to use 24/7 especially in cases where your main system goes down <br>in the middle of your shift which is at night?</div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="backup_availability" name="backup_availability" value="yes" {{ $system_check->system->backup_availability == 'yes' ? 'checked' : '' }}>
            <label class="form-check-label" for="backup_availability">Yes</label>
          </div>
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="backup_availability" name="backup_availability" value="no" {{ $system_check->system->backup_availability == 'no' ? 'checked' : '' }}>
            <label class="form-check-label" for="backup_availability">No</label>
          </div>
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Are you the owner of this Backup system? </div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="owner_backup" name="owner_backup" value="yes" {{ $system_check->system->owner_backup == 'yes' ? 'checked' : '' }}>
            <label class="form-check-label" for="owner_backup">Yes</label>
          </div>

          <div class="form-check">
            <input required type="radio" class="form-check-input" id="owner_backup" name="owner_backup" value="no" {{ $system_check->system->owner_backup == 'no' ? 'checked' : '' }}>
            <label class="form-check-label" for="owner_backup">No</label>
          </div>
          <br>
          <label class="top-label">If not, please state the full name of the owner and your relation to the owner:</label>
          <input type="text" name="backup_owner_name" class="form-control" value="{{ $system_check->system->backup_owner_name ?? '' }}">
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label"> Are you living with the owner of this back up system you declared?</div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="owner_backup_relation" name="owner_backup_relation" value="yes" {{ $system_check->system->owner_backup_relation == 'yes' ? 'checked' : '' }}>
            <label class="form-check-label" for="owner_backup_relation">Yes</label>
          </div>

          <div class="form-check">
            <input required type="radio" class="form-check-input" id="owner_backup_relation" name="owner_backup_relation" value="no" {{ $system_check->system->owner_backup_relation == 'no' ? 'checked' : '' }}>
            <label class="form-check-label" for="owner_backup_relation">No</label>
          </div><br>
          <label>If not, how long will it take you to go to this backup system?</label>
          <input type="text" name="how_long_backup_system" class="form-control" value="{{ $system_check->system->how_long_backup_system ?? '' }}">
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-12"><small>MAIN INTERNET SERVICE INFORMATION</small></div>
        <div class="col-sm-6 mt-2"><label class="top-label">Internet Service Provider and Plan Type</label></div>
        <div class="col-sm-6 mt-2"><input required type="text" class="form-control" name="isp" value="{{ $system_check->system->isp ?? '' }}"></div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Speed Plan</div>
        <div class="col-sm-6"><input required type="text" class="form-control" name="main_speed_plan" value="{{ $system_check->system->main_speed_plan ?? '' }}"></div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Type of Internet Connection</div>
        <div class="col-sm-6">
          <select name="type_connection_main" class="form-control" required>
            <option disabled selected>Please choose</option>
            <option value="broadband" {{ $system_check->system->type_connection_main == 'broadband' ? 'selected' : '' }}>Broadband/DSL</option>
            <option value="fiber" {{ $system_check->system->type_connection_main == 'fiber' ? 'selected' : '' }}>Fiber Internet</option>
            <option value="cable" {{ $system_check->system->type_connection_main == 'cable' ? 'selected' : '' }}>Cable Internet</option>
            <option value="mobile" {{ $system_check->system->type_connection_main == 'mobile' ? 'selected' : '' }}>Mobile (LTE)</option>
          </select>
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">Are you the account owner of this internet service?</label></div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="connection_owner" name="connection_owner" value="yes" {{ $system_check->system->connection_owner == 'yes' ? 'checked' : '' }}>
            <label class="form-check-label" for="connection_owner">Yes</label>
          </div>

          <div class="form-check">
            <input required type="radio" class="form-check-input" id="connection_owner2" name="connection_owner" value="no" {{ $system_check->system->connection_owner == 'no' ? 'checked' : '' }}>
            <label class="form-check-label" for="connection_owner2">No</label>
          </div>
          <br>
          <label class="top-label2">If not, please provide the full name of the account owner as it appears on the bill</label>
          <input type="text" class="form-control" name="other_connection_owner" value="{{ $system_check->system->other_connection_owner ?? '' }}">
          <br>
          <label class="top-label2">What is your relation to the account owner?</label>
          <input required type="text" class="form-control" name="account_owner_relation" value="{{ $system_check->system->account_owner_relation ?? '' }}">
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Are you still upgrading your internet speed?</div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="upgrade_internet" name="upgrade_internet" value="yes" {{ $system_check->system->upgrade_internet == 'yes' ? 'checked' : '' }}>
            <label class="form-check-label" for="upgrade_internet">Yes</label>
          </div>
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="upgrade_internet2" name="upgrade_internet" value="no" {{ $system_check->system->upgrade_internet == 'no' ? 'checked' : '' }}>
            <label class="form-check-label" for="upgrade_internet2">No</label>
          </div>
          <br>
          <label class="top-label2">If yes, what is the ETA?</label>
          <input type="text" class="form-control" name="eta" value="{{ $system_check->system->eta ?? '' }}">
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Please provide at least 3 speed tests of your Main ISP Connection:

          <table class="mt-3 table table-borderless">
            <tr><td><div class="float-left">DL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="dl1" value="{{ $system_check->system->dl1 ?? '' }}">&nbsp;mbps</td><td><div class="float-left pl-3">UL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="ul1" value="{{ $system_check->system->ul1 ?? '' }}">&nbsp;mbps</td></tr>
            <tr><td><div class="float-left">DL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="dl2" value="{{ $system_check->system->dl2 ?? '' }}">&nbsp;mbps</td><td><div class="float-left pl-3">UL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="ul2" value="{{ $system_check->system->ul2 ?? '' }}">&nbsp;mbps</td></tr>
            <tr><td><div class="float-left">DL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="dl3" value="{{ $system_check->system->dl3 ?? '' }}">&nbsp;mbps</td><td><div class="float-left pl-3">UL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="ul3" value="{{ $system_check->system->ul3 ?? '' }}">&nbsp;mbps</td></tr>
          </table>
        </div>
        <div class="col-sm-6">Speed Test Links
          <table class="table table-borderless" style="margin-top: 36px;">
            <tr><td><input class="form-control" type="text" name="speedtest_link1" placeholder="e.g. https://www.speedtest.net/result/8730145914" value="{{ $system_check->system->speedtest_link1 ?? '' }}"></td></tr>
            <tr><td><input class="form-control" type="text" name="speedtest_link2" placeholder="e.g. https://www.speedtest.net/result/8730145914" value="{{ $system_check->system->speedtest_link2 ?? '' }}"></td></tr>
            <tr><td><input class="form-control" type="text" name="speedtest_link3" placeholder="e.g. https://www.speedtest.net/result/8730145914" value="{{ $system_check->system->speedtest_link3 ?? '' }}"></td></tr>
          </table>
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-12"><small>BACKUP INTERNET SERVICE INFORMATION</small></div>
        <div class="col-sm-6 mt-2 top-label">What is your Backup ISP? </div>
        <div class="col-sm-6"><input type="text" class="form-control" name="backup_isp" value="{{ $system_check->system->backup_isp ?? '' }}"></div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Are you the owner of this Backup ISP?</div>
        <div class="col-sm-6">
          <div class="form-check">
            <input type="radio" class="form-check-input" id="connection_owner2" name="connection_owner2" value="yes" {{ $system_check->system->connection_owner2 == 'yes' ? 'checked' : '' }}>
            <label class="form-check-label" for="connection_owner">Yes</label>
          </div>
          <div class="form-check">
            <input type="radio" class="form-check-input" id="connection_owner2" name="connection_owner2" value="no" {{ $system_check->system->connection_owner2 == 'no' ? 'checked' : '' }}>
            <label class="form-check-label" for="connection_owner">No</label>
          </div>
          <br>
          <label class="top-label"> If not, please provide the full name of the owner and your relation:</label>
          <input type="text" class="form-control" name="isp_owner_2" value="{{ $system_check->system->isp_owner_2 ?? '' }}">
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Is this backup ISP readily available for you to use 24/7 especially in cases where your main system goes down <br>in the middle of your shift which is at night?</div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="isp_backup_availabilitiy" name="isp_backup_availabilitiy" value="yes" {{$system_check->system->isp_backup_availabilitiy == 'yes' ? 'checked' : '' }}>
            <label class="form-check-label" for="connection_owner">Yes</label>
          </div>

          <div class="form-check">
            <input required type="radio" class="form-check-input" id="isp_backup_availabilitiy" name="isp_backup_availabilitiy" value="no" {{$system_check->system->isp_backup_availabilitiy == 'no' ? 'checked' : '' }}>
            <label class="form-check-label" for="connection_owner">No</label>
          </div>
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">How long will it take you to reach your backup power source<br>(if this is not located in your own home)</div>
        <div class="col-sm-6"><input type="text" name="how_long" class="form-control" value="{{ $system_check->system->how_long ?? '' }}"></div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Please provide at least 3 speed tests of your Backup ISP Connection:

          <table class="mt-3 table table-borderless">
            <tr><td><div class="float-left">DL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="backup_dl1" value="{{ $system_check->system->backup_dl1 ?? '' }}">&nbsp;mbps</td><td><div class="float-left pl-3">UL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="backup_ul1" value="{{ $system_check->system->backup_ul1 ?? '' }}">&nbsp;mbps</td></tr>
            <tr><td><div class="float-left">DL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="backup_dl2" value="{{ $system_check->system->backup_dl2 ?? '' }}">&nbsp;mbps</td><td><div class="float-left pl-3">UL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="backup_ul2" value="{{ $system_check->system->backup_ul2 ?? '' }}">&nbsp;mbps</td></tr>
            <tr><td><div class="float-left">DL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="backup_dl3" value="{{ $system_check->system->backup_dl3 ?? '' }}">&nbsp;mbps</td><td><div class="float-left pl-3">UL:&nbsp;</div> <input type="number" class="form-control float-left" style="width: 60px;" name="backup_ul3" value="{{ $system_check->system->backup_ul3 ?? '' }}">&nbsp;mbps</td></tr>
          </table>
        </div>
        <div class="col-sm-6">Speed Test Links
          <table class="table table-borderless" style="margin-top: 36px;">
            <tr><td><input class="form-control" type="text" name="backup_speedtest_link1" placeholder="e.g. https://www.speedtest.net/result/8730145914" value="{{ $system_check->system->backup_speedtest_link1 ?? '' }}"></td></tr>
            <tr><td><input class="form-control" type="text" name="backup_speedtest_link2" placeholder="e.g. https://www.speedtest.net/result/8730145914" value="{{ $system_check->system->backup_speedtest_link2 ?? '' }}"></td></tr>
            <tr><td><input class="form-control" type="text" name="backup_speedtest_link3" placeholder="e.g. https://www.speedtest.net/result/8730145914" value="{{ $system_check->system->backup_speedtest_link3 ?? '' }}"></td></tr>
          </table>
        </div>
      </div>
      <!---->
      <h5 class="text-muted header-underlined-wide mt-4">BACKUP POWER SOURCE</h5>
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">What is your backup power source?</div>
        <div class="col-sm-6"><input type="text" name="backup_power_source" class="form-control" value="{{ $system_check->system->backup_power_source ?? '' }}"></div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">Are you the owner of this backup power source?</div>
        <div class="col-sm-6">
          <div class="form-check">
            <input type="radio" class="form-check-input" id="owner_backup_source" name="owner_backup_source" value="yes" {{ $system_check->system->owner_backup_source == 'yes' ? 'checked' : '' }}>
            <label class="form-check-label" for="owner_backup_source">Yes</label>
          </div>

          <div class="form-check">
            <input type="radio" class="form-check-input" id="owner_backup_source" name="owner_backup_source" value="no" {{ $system_check->system->owner_backup_source == 'no' ? 'checked' : '' }}>
            <label class="form-check-label" for="owner_backup_source">No</label>
          </div>
          <br>
          <label class="top-label"> If not, please provide the full name of the owner and your relation:</label>
          <input type="text" name="owner_backup_power_owner_name" class="form-control" value="{{ $system_check->system->owner_backup_power_owner_name ?? '' }}">
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label"> Is this backup power source readily available for you to use 24/7 especially in cases where your main system goes down <br>in the middle of your shift which is at night?</div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="availability_backup_source" name="availability_backup_source" value="yes" {{ $system_check->system->availability_backup_source == 'yes' ? 'checked' : '' }}>
            <label class="form-check-label" for="availability_backup_source">Yes</label>
          </div>

          <div class="form-check">
            <input required type="radio" class="form-check-input" id="availability_backup_source" name="availability_backup_source" value="no" {{ $system_check->system->availability_backup_source == 'no' ? 'checked' : '' }}>
            <label class="form-check-label" for="owner_backup_source">No</label>
          </div>
        </div>
      </div>
      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6 top-label">How long will it take you to reach your backup ISP <br>(if this is not located in your own home)</div>
        <div class="col-sm-6"><input type="text" name="backup_power_how_long" class="form-control" value="{{ $system_check->system->backup_power_how_long ?? '' }}"></div>
      </div>
      <!---->
      <h5 class="text-muted header-underlined-wide mt-5">OTHER SYSTEM INFORMATION</h5>

      <div class="mt-4 row">
        <div class="col-sm-6 top-label">What type of webcam are you using?</div>
        <div class="col-sm-6">
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="built_in" name="web_camera" value="built-in" {{ $system_check->system->web_camera == 'built-in' ? 'checked' : '' }}>
            <label class="form-check-label" for="web_camera">Built-in</label>
          </div>
          <div class="form-check">
            <input required type="radio" class="form-check-input" id="web_camera" name="web_camera" value="external" {{ $system_check->system->web_camera == 'external' ? 'checked' : '' }}>
            <label class="form-check-label" for="web_camera">External</label>
          </div>
          <br>
          <label class="top-label"> If external, what brand and model of webcam are you using?</label>
          <input type="text" name="web_camera_brand" class="form-control" value="{{ $system_check->system->web_camera_brand ?? '' }}">
        </div>


      </div>

      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">Do you have a USB noise –cancelling headset?</label></div>

        <div class="col-sm-6">
          <div class="form-check" style="width:100%">
            <input type="radio" class="form-check-input" id="headset" name="headset" value="yes" {{ isset($system_check->system->headset) ? $system_check->system->headset == 'yes' ? 'checked' : '' : '' }}>
            <label class="form-check-label" for="headset">Yes</label>
          </div>
          <div class="form-check">
            <input type="radio" class="form-check-input" id="headset2" name="headset" value="no" {{ isset($system_check->system->headset) ? $system_check->system->headset == 'no' ? 'checked' : '' : '' }}>
            <label class="form-check-label" for="headset2">No</label>
          </div>
        </div>

      </div>

      <!---->
      <div class="mt-4 row">
        <div class="col-sm-6"><label class="top-label">What brand and model is your USB noise – cancelling headset?</label></div>
        <div class="col-sm-6"><input type="text" name="headset_brand" class="form-control" value="{{ $system_check->system->headset_brand ?? '' }}"></div>
      </div>

      @endforeach

      <!-- images -->
      <h5 class="text-muted header-underlined-wide mt-5">Photo Addendum</h5>

      <div class="row mt-5">
        <!-- exterior -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="exterior"><label class="top-label">House exterior</label><br><br>
            <a target="_blank" href="{{ isset($photoAddendum->exterior) ? asset($photoAddendum->getImage($photoAddendum->exterior, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" id="exterior" src="{{ isset($photoAddendum->exterior) ? asset($photoAddendum->getImage($photoAddendum->exterior, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" width="100%" height="220"></a>
          </div>
          <div><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{isset($photoAddendum->exterior) ? $photoAddendum->exterior : '' }}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="exterior">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="exterior"/>
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
        <!-- workstation -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="workstation"><label class="top-label">Workstation</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->workstation) ? asset($photoAddendum->getImage($photoAddendum->workstation, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->workstation) ? asset($photoAddendum->getImage($photoAddendum->workstation, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="workstation"></a>
          </div>
          <div><label class="top-label">File</label><br>

            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{ isset($photoAddendum->workstation) ? $photoAddendum->workstation : '' }}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="workstation">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="workstation" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>

            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
        <!-- main pc -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="main_pc_specs"><label class="top-label">Main Computer Unit</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->main_pc_specs) ? asset($photoAddendum->getImage($photoAddendum->main_pc_specs, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->main_pc_specs) ? asset($photoAddendum->getImage($photoAddendum->main_pc_specs, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="main_pc_specs"></a>
          </div>
          <div><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{ isset($photoAddendum->main_pc_specs) ? $photoAddendum->main_pc_specs : '' }}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="main_pc_specs">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="main_pc_specs"/>
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <!-- backup pc -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="backup_pc"><label class="top-label">Backup Computer Unit</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->backup_pc) ? asset($photoAddendum->getImage($photoAddendum->backup_pc, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->backup_pc) ? asset($photoAddendum->getImage($photoAddendum->backup_pc, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="backup_pc"></a>
          </div>
          <div class="col-sm-6"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{ isset($photoAddendum->backup_pc) ? $photoAddendum->backup_pc : '' }}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="backup_pc">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="backup_pc" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
        <!-- isp main -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="main_isp"><label class="top-label">ISP Main</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->main_isp) ? asset($photoAddendum->getImage($photoAddendum->main_isp, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->main_isp) ? asset($photoAddendum->getImage($photoAddendum->main_isp, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="main_isp"></a>
          </div>
          <div class="col-sm-6" id="main_isp"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{ isset($photoAddendum->main_isp) ? $photoAddendum->main_isp : '' }}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="main_isp">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="main_isp" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>

        </div>
        <!-- isp backup -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="backup_isp"><label class="top-label">ISP Backup</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->backup_isp) ? asset($photoAddendum->getImage($photoAddendum->backup_isp, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->backup_isp) ? asset($photoAddendum->getImage($photoAddendum->backup_isp, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="main_pc_specs"></a>
          </div>
          <div class="col-sm-6"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{ isset($photoAddendum->backup_isp) ? $photoAddendum->backup_isp : '' }}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="backup_isp">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="backup_isp"/>
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <!-- headset -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="headset"><label class="top-label">Headset</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->headset) ? asset($photoAddendum->getImage($photoAddendum->headset, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->headset) ? asset($photoAddendum->getImage($photoAddendum->headset, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="headset"></a>
          </div>
          <div class="col-sm-6"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{ isset($photoAddendum->headset) ? $photoAddendum->headset : '' }}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="headset">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="headset" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
        <!-- backup power source -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="backup_power_source"><label class="top-label">Backup Power Source</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->backup_power_source) ? asset($photoAddendum->getImage($photoAddendum->backup_power_source, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->backup_power_source) ? asset($photoAddendum->getImage($photoAddendum->backup_power_source, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="backup_pc_specs"></a>
          </div>
          <div class="col-sm-6"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{isset($photoAddendum->backup_power_source) ? $photoAddendum->backup_power_source : ''}}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="backup_power_source">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="backup_power_source" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
        <!-- main pc -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="main_pc"><label class="top-label">Main Computer Specs</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->main_pc) ? asset($photoAddendum->getImage($photoAddendum->main_pc, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->main_pc) ? asset($photoAddendum->getImage($photoAddendum->main_pc, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="main_pc"></a>

          </div>
          <div class="col-sm-6" ><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{isset($photoAddendum->main_pc) ? $photoAddendum->main_pc : ''}}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="main_pc">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="main_pc" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <!-- backup computer specs -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="backup_pc_specs"><label class="top-label">Backup Computer Specs</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->backup_pc_specs) ? asset($photoAddendum->getImage($photoAddendum->backup_pc_specs, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->backup_pc_specs) ? asset($photoAddendum->getImage($photoAddendum->backup_pc_specs, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="backup_pc_specs"></a>
          </div>
          <div class="col-sm-6"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{isset($photoAddendum->backup_pc_specs) ? $photoAddendum->backup_pc_specs : '' }}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="backup_pc_specs">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="backup_pc_specs" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
        <!-- job order -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="isp_receipt"><label class="top-label">Scanned Copy Of ISP Job Order</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->isp_receipt) ? asset($photoAddendum->getImage($photoAddendum->isp_receipt, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->isp_receipt) ? asset($photoAddendum->getImage($photoAddendum->isp_receipt, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="isp_receipt"></a>
          </div>
          <div class="col-sm-6"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{isset($photoAddendum->isp_receipt) ? $photoAddendum->isp_receipt : ''}}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="isp_receipt">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="isp_receipt"/>
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
        <!-- isp bill -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="isp_bill"><label class="top-label">Scanned Copy of Current ISP Bill</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->isp_bill) ? asset($photoAddendum->getImage($photoAddendum->isp_bill, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->isp_bill) ? asset($photoAddendum->getImage($photoAddendum->isp_bill, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="isp_bill"></a>
          </div>
          <div class="col-sm-6"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{isset($photoAddendum->isp_bill) ? $photoAddendum->isp_bill : ''}}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="isp_bill">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="isp_bill" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
      </div>
      <div class="row mt-5">
        <!-- authorization letter -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="isp_authorization_letter"><label class="top-label">Scanned copy of Authorization Letter from account holder of the ISP with signature</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->isp_authorization_letter) ? asset($photoAddendum->getImage($photoAddendum->isp_authorization_letter, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->isp_authorization_letter) ? asset($photoAddendum->getImage($photoAddendum->isp_authorization_letter, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="isp_authorization_letter"></a>
          </div>
          <div><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{isset($photoAddendum->isp_authorization_letter) ? $photoAddendum->isp_authorization_letter : ''}}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="isp_authorization_letter">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="isp_authorization_letter" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
        <!-- owner id -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="isp_owner_id"><label class="top-label">Scanned ISP owner's ID with signature</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->isp_owner_id) ? asset($photoAddendum->getImage($photoAddendum->isp_owner_id, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->isp_owner_id) ? asset($photoAddendum->getImage($photoAddendum->isp_owner_id, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="isp_owner_id"></a>
          </div>
          <div class="col-sm-6"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{isset($photoAddendum->isp_owner_id) ? $photoAddendum->isp_owner_id : ''}}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="isp_owner_id">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="isp_owner_id"/>
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
        <!-- web camera -->
        <div class="col-sm-4">
          <div class="thumb-wrapper" id="webcamera"><label class="top-label">Fully Functioning web-camera</label><br>
            <a target="_blank" href="{{ isset($photoAddendum->webcamera) ? asset($photoAddendum->getImage($photoAddendum->webcamera, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}"><img class="thumbnail-he" src="{{ isset($photoAddendum->webcamera) ? asset($photoAddendum->getImage($photoAddendum->webcamera, $photoAddendum->owner)) : '/img/backend/brand/grey.png' }}" id="webcamera"></a>
          </div>
          <div class="col-sm-6"><label class="top-label">File</label><br>
            <form method="post" action="" enctype="multipart/form-data" id="file_uploader">
              <div>
                <input type="hidden" id="fileid" name="file_id" value="{{ isset($photoAddendum->id) ? $photoAddendum->id : '' }}">
                <input type="hidden" id="imageid" name="from_file" value="{{isset($photoAddendum->webcamera) ? $photoAddendum->webcamera : ''}}">
                <input type="hidden" id="user_id"  name="user_id" value="{{$applicant->id}}">
                <input type="hidden" id="from" name="from_input" value="webcamera">
                <input type="file" accept=".jpg, .gif, .png" id="file" name="file" rel="webcamera" />
                <input type="button" class="btn btn-default" value="Upload" id="uploader" style="visibility: hidden;">
              </div>
            </form>
            <span style="display:none !important" class="badge level level-alert"><i class="fa fa-fw fa-times-circle"></i>&nbsp;remove</span>
          </div>
        </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-12">
        <label class="top-label">Notes</label><br>

        @if(isset($eval->he_notes))
          @if(count($eval->he_notes) >= 1)
            @foreach($eval->he_notes as $note)
              <div style="cursor: pointer; border-radius: 3px; background: #fafafa; padding: 6px; margin-bottom: 10px;">
                <span class="text-muted">&nbsp;{{ date('M d, Y h:i', strtotime($note->date_created)) }}</span> <span class="text-muted float-right pr-2" id="hr-del"><i class="fas fa-times"></i></span>
                <div><textarea class="form-control" name="notes[]" id="hrnote">{{$note->note}}</textarea></div>
              </div>
            @endforeach
          @else
            <textarea class="form-control" name="notes[]" id="hrnote"></textarea><br>
          @endif
        @endif


        <p id="dbr"></p>
        <button class="btn btn-sm btn-green" id="btn-add-he-note">+ Add Note</button>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-12">
        <label class="top-label">Result</label><br>
        <select class="form-control" name="result">
          <option disabled value="" selected>Please choose</option>
          <option value="1" {{ isset($result) ? $result == '1' ? 'selected' : '' : '' }}>Passed Home Evaluation | Endorsed For HR Requirements</option>
          <option value="2" {{ isset($result) ? $result == '2' ? 'selected' : '' : ''  }}>Failed Home Evaluation</option>
          <option value="3" {{ isset($result) ? $result == '3' ? 'selected' : '' : ''  }}>On Hold | Needs to Red-do HEF</option>
          <option value="4" {{ isset($result) ? $result == '4' ? 'selected' : '' : ''  }}>Blacklisted</option>
          <option value="5" {{ isset($result) ? $result == '5' ? 'selected' : '' : ''  }}>Withdrawn</option>
          <option value="6" {{ isset($result) ? $result == '6' ? 'selected' : '' : ''  }}>Forfeited</option>
        </select>
      </div>
    </div>
  </div>
</div>
</div>
