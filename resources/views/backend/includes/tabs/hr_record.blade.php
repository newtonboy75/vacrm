<ul class="nav nav-tabs" id="myTab" role="tablist">

  <li class="nav-item">
    <a class="nav-link active" id="employee_info-tab" data-toggle="tab" href="#employee_info" role="tab" aria-controls="employee_info" aria-selected="true">Employee Information</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="employee_credentials-tab" data-toggle="tab" href="#employee_credentials" role="tab" aria-controls="employee_credentials" aria-selected="false">Employee Credentials</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="emmployment_info-tab" data-toggle="tab" href="#emmployment_info" role="tab" aria-controls="emmployment_info" aria-selected="false">Employment Information</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="coaching_log-tab" data-toggle="tab" href="#coaching_log" role="tab" aria-controls="coaching_log" aria-selected="false">Coaching Log</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="va_checkin-tab" data-toggle="tab" href="#va_checkin" role="tab" aria-controls="va_checkin" aria-selected="false">VA Checkin Record</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="memo-tab" data-toggle="tab" href="#memo" role="tab" aria-controls="memo" aria-selected="false">Memos & Incident Reports</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contracts-tab" data-toggle="tab" href="#contracts" role="tab" aria-controls="contracts" aria-selected="false">Forms</a>
  </li>
</ul>

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="employee_info" role="tabpanel" aria-labelledby="employee_info-tab">@include('backend.includes.tabs.hr_records.employee_info')</div>
  <div class="tab-pane fade" id="employee_credentials" role="tabpanel" aria-labelledby="employee_credentials-tab">@include('backend.includes.tabs.hr_records.credentials')</div>
  <div class="tab-pane fade" id="emmployment_info" role="tabpanel" aria-labelledby="emmployment_info-tab">@include('backend.includes.tabs.hr_records.employment_info')</div>
  <div class="tab-pane fade show" id="coaching_log" role="tabpanel" aria-labelledby="coaching_log-tab">@include('backend.includes.tabs.hr_records.coaching')</div>
  <div class="tab-pane fade" id="va_checkin" role="tabpanel" aria-labelledby="va_checkin-tab">@include('backend.includes.tabs.hr_records.va_checkin')</div>
  <div class="tab-pane fade" id="memo" role="tabpanel" aria-labelledby="memo-tab">@include('backend.includes.tabs.hr_records.memo')</div>
  <div class="tab-pane fade" id="contracts" role="tabpanel" aria-labelledby="contracts-tab">@include('backend.includes.tabs.hr_records.contracts')</div>
</div>
