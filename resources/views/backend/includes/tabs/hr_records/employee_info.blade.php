


<h5 class="text-muted header-underlined-wide">VA Matching Profile</h5>
<form id="formEmployee">
@php
$profile_pic = '';

if($applicant->profile_image_id != ''){
  $profile_pic = App\Media::whereId($applicant->profile_image_id)->first()->name;
}else{
  $profile_pic = 'placeholder123.png';
}

@endphp

<div class="row">
  <div class="col-sm-2 mt-2">
    <div id="profile_pic"><img id="img-obj" src="https://virtudeskcrm.s3.us-west-2.amazonaws.com/employee_profile_pic/{{$profile_pic}}">
      <label for="fileInput">
        <i id="icon" class="fas fa-arrow-circle-up" title="change profile pic"></i>
      </label>
      <input id="fileInput" type="file" accept="image/*" name="emp_photo" onchange="document.getElementById('img-obj').src = window.URL.createObjectURL(this.files[0])">
    </div>
  </div>

  <div class="col-sm-10">

    <div>
    <input type="hidden" name="applicant_id" value="{{$applicant->id}}">
    <input type="hidden" name="from_tab" value="emp_info">

    <label class="top-label mt-3">Employee Name</label>
    <input class="form-control" type="text" name="applicant_name" value="{{$applicant->name}}">

    <label class="top-label mt-2">Mobile Number</label>
    <input class="form-control" type="text" name="mobile_number" value="{{$applicant->mobile_number}}">

    <label class="top-label mt-2">Landline Number</label>
    <input class="form-control" type="text" name="landing_number" value="{{$applicant->landing_number}}">

    <label class="top-label mt-2">Complete Address</label>
    <input class="form-control" type="text" name="address" value="{{$applicant->address}}">

    <label class="top-label mt-2">Program/VALevel</label>
    <select name="va_level" class="form-control">
      <option value="1" {{$applicant->va_level == '1' ? 'selected' : ''}}>ISA</option>
      <option value="2" {{$applicant->va_level == '2' ? 'selected' : ''}}>GVA</option>
      <option value="3" {{$applicant->va_level == '3' ? 'selected' : ''}}>EVA</option>
    </select>

    <label class="top-label mt-2">LOB</label>
    <select name="lob" class="form-control" required>
      <option value="0" disabled selected>Please choose</option>
      <option value="1" {{isset($employeeRecord->lob) ? $employeeRecord->lob == '1' ? 'selected' : '' : ''}}>Operations - VA</option>
      <option value="2" {{isset($employeeRecord->lob) ? $employeeRecord->lob == '2' ? 'selected' : '' : ''}}>Management - Operations</option>
      <option value="3" {{isset($employeeRecord->lob) ? $employeeRecord->lob == '3' ? 'selected' : '' : ''}}>Management - L&D and Digital Content</option>
      <option value="4" {{isset($employeeRecord->lob) ? $employeeRecord->lob == '4' ? 'selected' : '' : ''}}>Management - HR</option>
      <option value="5" {{isset($employeeRecord->lob) ? $employeeRecord->lob == '5' ? 'selected' : '' : ''}}>Management - Finance</option>
      <option value="6" {{isset($employeeRecord->lob) ? $employeeRecord->lob == '6' ? 'selected' : '' : ''}}>Management - Talent Acquisition</option>
      <option value="7" {{isset($employeeRecord->lob) ? $employeeRecord->lob == '7' ? 'selected' : '' : ''}}>Management - Sourcing</option>
      <option value="8" {{isset($employeeRecord->lob) ? $employeeRecord->lob == '8' ? 'selected' : '' : ''}}>Bootcamper - Marketing</option>
      <option value="9" {{isset($employeeRecord->lob) ? $employeeRecord->lob == '9' ? 'selected' : '' : ''}}>Operations - Nexus</option>
    </select>

    <label class="top-label mt-2">VD Tenure</label>

    @php

    use Carbon\Carbon;
    $employment_duration = '';
    if($employeeRecord !== null){
      $tenure =  date('Y-m-d', strtotime($employeeRecord->created_at));
      $exp_tenure = explode('-', $tenure);
      $employment_duration = Carbon::createFromDate($exp_tenure[0], $exp_tenure[1], $exp_tenure[2])->diff(Carbon::now())->format('%y years, %m months and %d days');
    }

    @endphp
    <input class="form-control" disabled type="text" name="ve_tenure" value="{{$employment_duration ?? ''}}">

    <label class="top-label mt-2">Employee Status</label>
    <select class="form-control" name="user_status">
      <option value="1" {{$applicant->status == '1' ? 'selected' : ''}}>Active</option>
      <option value="2" {{$applicant->status == '2' ? 'selected' : ''}}>Inactive</option>
      <option value="3" {{$applicant->status == '3' ? 'selected' : ''}}>Archived</option>
    </select>
    </div>
  </div>

</div>




</form>
