<h5 class="text-muted header-underlined-wide">Employment Information</h5>
<label class="top-label mt-3">Jobs</label>
<br>

<input type="hidden" name="applicant_id" value="{{$applicant->id}}">
<input type="hidden" name="from_tab" value="empl_info">
<input type="hidden" name="assigned_client" id="assigned_client" value="{{$applicant->trainings[0]->trainer->id ?? ''}}">

<table class="table">
  <tr><td class="font-weight-bold">
    <div class="row"><div class="col-sm-1">JOB ID</div><div class="col-sm-2">DATE</div><div class="col-sm-1">STATUS</div><div class="col-sm-3">CLIENT</div></div></td></tr>

    @php
    $current_stat = ['Active', 'Canceled', 'Replaced', 'For Cancellation', 'For Replacement'];
    @endphp
    @foreach($jobs as $job)
    <tr><td class="td-collapse">
      <div class="row" id="collapse-cursor">
        <div class="col-sm-1">{{$job->job_info[0]->id ?? ''}}</div>
        <div class="col-sm-2">{{ isset($job->job_info[0]) ? date('F d, Y, g:i A', strtotime($job->job_info[0]->start_date)) : '' }}</div>
        <div class="col-sm-1">{{ isset($job->job_info[0]->status) ? $current_stat[($job->job_info[0]->status) - 1] : '' }}</div>
        <div class="col-sm-3">
          @foreach($clients as $client)
          {{ isset($job->job_info[0]) ? $job->job_info[0]->client_id == $client->id ? $client->name : '' : '' }}
          @endforeach
        </div>
      </div>
      <div class="row" id="custom_collapse">
        <div class="col">
          <div>
            <input type="hidden" name="empid[]" value="{{$job->job_info[0]->id ?? ''}}">
            <div class="row" style="border: 1px solid #f0f0f0; padding: 20px; background: #fafafa; margin-top: 16px;">
              <div class="col-sm-4">
                <label class="top-label mt-2">Client</label>
                <select class="form-control client_id" id="client_old" name="client_id[]">
                  @foreach($clients as $client)
                  <option value="{{ $client->id ?? '' }}" {{ isset($job->job_info[0]) ? $job->job_info[0]->client_id == $client->id ? 'selected' : '' : '' }}>{{$client->name}}</option>
                  @endforeach
                </select>

                <label class="top-label mt-2">Level</label>
                <select class="form-control" id="level" name="level[]">
                  <option value="1" {{ isset($job->job_info[0]) ? $job->job_info[0]->level == '1' ? 'selected' : '' : ''}}>1st VA</option>
                  <option value="2" {{ isset($job->job_info[0]) ? $job->job_info[0]->level == '2' ? 'selected' : '' : '' }}>Replacement VA</option>
                  <option value="3" {{ isset($job->job_info[0]) ? $job->job_info[0]->level == '3' ? 'selected' : '' : ''}}>Multiple VA</option>
                </select>

                <label class="top-label mt-2">Hourly Rate</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">$</span>
                  </div>
                  <input type="number" class="form-control" id="hourly_rate" name="hourly_rate[]" value="{{ isset($job->job_info[0]) ? intval($job->job_info[0]->hourly_rate) : '' }}">
                </div>

                <label class="top-label mt-2">Job Description</label>
                <textarea class="form-control" name="job_description[]" id="job_description">{{ isset($job->job_info[0]) ? $job->job_info[0]->description : ''}}</textarea>

                <label class="top-label mt-2">Schedule</label>
                <input class="form-control" type="text" name="schedule[]" id="schedule" value="{{ isset($job->job_info[0]) ? $job->job_info[0]->schedule : ''}}">

              </div>
              <div class="col-sm-4">
                <label class="top-label mt-2">Manager</label>
                <!--
                <input class="form-control" type="text" name="manager[]" id="manager" value="{{ isset($job->job_info[0]) ? $job->job_info[0]->manager : ''}}">
                -->

                <select class="form-control" name="manager[]" id="manager">
                  @foreach($allCoaches as $coach)
                  <option value="{{$coach->name}}" {{ $job->job_info[0]->manager == $coach->name ? 'selected' : ''}}>{{$coach->name}}</option>
                  @endforeach
                </select>

                <label class="top-label mt-2">Team</label>
                <select class="form-control" name="team[]" id="team">
                  @foreach($groups as $group)
                  <option value="{{$group->id}}" {{ isset($job->job_info[0]) ? $job->job_info[0]->team == $group->id ? 'selected' : '' : '' }}>{{$group->name}}</option>
                  @endforeach
                </select>

                <label class="top-label mt-2">Start Date</label>

                <div class="input-group input-group"><input data-zdp_readonly_element="false" type="text" class="form-control dpk" name="start_date[]" value="{{ isset($job->job_info[0]) ? date('F d, Y, g:i A', strtotime($job->job_info[0]->start_date)) : ''}}"></div>

                <label class="top-label mt-2">End Date</label>

                <div class="input-group input-group"><input data-zdp_readonly_element="false" type="text" class="form-control dpk" name="end_date[]" value="{{ isset($job->job_info[0]) ? date('F d, Y, g:i A', strtotime($job->job_info[0]->end_date)) : ''}}"></div>

                <label class="top-label mt-2">Status</label>
                <select class="form-control" id="status" name="empstatus[]">
                  <option value="1" {{ isset($job->job_info[0]) ? $job->job_info[0]->status == '1' ? 'selected' : '' : ''}}>Active</option>
                  <option value="2" {{ isset($job->job_info[0]) ? $job->job_info[0]->status == '2' ? 'selected' : '' : ''}}>Canceled</option>
                  <option value="3" {{ isset($job->job_info[0]) ? $job->job_info[0]->status == '3' ? 'selected' : '' : ''}}>Replaced</option>
                  <option value="4" {{ isset($job->job_info[0]) ? $job->job_info[0]->status == '4' ? 'selected' : '' : ''}}>For Cancellation</option>
                  <option value="5" {{ isset($job->job_info[0]) ? $job->job_info[0]->status == '5' ? 'selected' : '' : ''}}>For Replacement</option>
                </select>

              </div>
              <div class="col-sm-4">

                <label class="top-label mt-2">Employment Status</label>
                <select class="form-control mt-2" name="employment_status[]" id="employment_status">
                  <option {{ isset($job->job_info[0]) ? $job->job_info[0]->employment_status == '1' ? 'selected' : '' : ''}} value="1">FT</option>
                  <option {{ isset($job->job_info[0]) ? $job->job_info[0]->employment_status == '2' ? 'selected' : '' : ''}} value="2">PT</option>
                  <option {{ isset($job->job_info[0]) ? $job->job_info[0]->employment_status == '3' ? 'selected' : '' : ''}} value="3">Timeblock</option>
                  <option {{ isset($job->job_info[0]) ? $job->job_info[0]->employment_status == '4' ? 'selected' : '' : ''}} value="4">Trial - 20 hours</option>
                  <option {{ isset($job->job_info[0]) ? $job->job_info[0]->employment_status == '5' ? 'selected' : '' : ''}} value="5">Trial - 40 hours</option>
                  <option {{ isset($job->job_info[0]) ? $job->job_info[0]->employment_status == '6' ? 'selected' : '' : ''}} value="6">Trial -1 month</option>
                  <option {{ isset($job->job_info[0]) ? $job->job_info[0]->employment_status == '7' ? 'selected' : '' : ''}} value="7">Trial - 2 months</option>
                </select>

                <label class="top-label mt-2">Notes</label>
                <textarea class="form-control" id="note" name="note[]">{{isset($job->job_info[0]) ? $job->job_info[0]->notes : ''}}</textarea>

                <label class="top-label mt-2">System Tools</label>
                <textarea class="form-control" id="system_tools" name="system_tools[]">{{ isset($job->job_info[0]) ? $job->job_info[0]->tools : ''}}</textarea>

                <div title="Delete item" class="mt-3 float-right top-label del-eminfo del-item-hr" id="ref-emp-info" rel="{{ isset($job->job_info[0]) ? $job->job_info[0]->id : ''}}"><i class="fas fa-trash-alt"></i> Delete Record</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </td></tr>
    @endforeach
  </table>
  <br>
  <div class="row" id="row-empinfo"></div>
  <button class="btn btn-sm btn-success mt-3 mb-2" data-toggle="modal" id="add_new_emp_info">Add New</button>
