<!-- Modal -->
<div class="modal fade" id="checkinModal" tabindex="-1" role="dialog" aria-labelledby="checkinModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="checkinModalLabel">VA Evaluation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-3">VA Name</div>
          <input type="hidden" id="client_id" value="{{$applicant->id}}">
          <div class="col-sm-9"><input class="form-control" type="text" disable value="{{$applicant->name}}" id="vaname"></div>
        </div>

        <div class="row mt-2">
          <div class="col-sm-3">Client Name</div>
          <div class="col-sm-9"><select class="form-control" id="va-popup-client" name="client"></select></div>
        </div>

        <div class="row mt-2">
          <div class="col-sm-3">Current Status</div>
          <div class="col-sm-9">
            <select class="form-control" id="va_status" name="va_status">
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
              <option value="On-Hold">On-Hold</option>
            </select></div>
        </div>

        <div class="row mt-2">
          <div class="col-sm-3">Strength</div>
          <div class="col-sm-9"><textarea class="form-control" id="strength"></textarea></div>
        </div>

        <div class="row mt-2">
          <div class="col-sm-3">Opportunity</div>
          <div class="col-sm-9"><textarea class="form-control" id="opportunity"></textarea></div>
        </div>

        <div class="row mt-2">
          <div class="col-sm-3">Action Plan</div>
          <div class="col-sm-9"><textarea class="form-control" id="action"></textarea></div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="create_checkin_file">Save changes</button>
      </div>
    </div>
  </div>
</div>
