<h5 class="text-muted header-underlined-wide">Contracts</h5>

<div>
<table style="width: 100% !important;" class="table" id="data-contracts-signed">
  <thead>
    <tr>
      <th>ID</th>
      <th>Contract ID</th>
      <th>Contract Type</th>
      <th>Date Signed</th>
      <th>Status</th>
    </tr>
  </thead>
</table>
</div>

<!--
@include('backend.includes.tabs.contracts.contract')
-->
@include('backend.includes.tabs.contracts.popup_caf')
@include('backend.includes.tabs.contracts.popup_final')
@include('backend.includes.tabs.contracts.hold_suspension')
@include('backend.includes.tabs.contracts.popup_fte')
