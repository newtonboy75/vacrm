<h5 class="text-muted header-underlined-wide">Coaching Log</h5>
<label class="top-label mt-3">Coaching Logs</label>
<form id="formEmployee" enctype="multipart/form-data">
<input type="hidden" name="applicant_id" value="{{$applicant->id}}">
<input type="hidden" name="from_tab" value="coaching">

<table class="table table-sm table-bordered" id="coaching_tbl">
  <tr>

    <th scope="col" style="width: 180px;">Date Served</th>
    <th scope="col" style="width: 150px;">Issued By</th>
    <th scope="col" style="width: 180px;">	Date Signed</th>
    <th scope="col">Violation</th>
    <th scope="col" style="width: 120px;">Status</th>
    <th scope="col">Memo</th>
    <th scope="col"></th>
  </tr>
  @foreach($coaching as $coach)

  <tr>
    <td>
      <input type="hidden" name="coaching_id[]" value="{{$coach->coachings[0]->id}}">
      <input data-zdp_readonly_element="false" type="text" class="form-control dpk" id="dpicker" name="date_served[]" value="{{ date('F d, Y g:i', strtotime($coach->coachings[0]->date_served )) ?? '' }}"></td>
    <td>
      <select class="form-control" name="issued_by[]" id="issued_by">
        @foreach($allCoaches as $dcoach)
          <option value="{{$dcoach->id}}" {{$dcoach->id ==$coach->coachings[0]->issued_by ? 'selected' : ''}}>{{$dcoach->first_name}} {{$dcoach->last_name}}</option>
        @endforeach
      </select>
    </td>
    <td><input data-zdp_readonly_element="false" type="text" class="form-control dpk" id="dpicker" name="date_signed[]" value="{{ date('F d, Y g:i', strtotime($coach->coachings[0]->date_signed )) ?? '' }}"></td>
    <td><input type="text" class="form-control" id="violation" name="violation[]" value="{{ $coach->coachings[0]->violation ?? '' }}"></td>
    <td>
      <select class="form-control" name="status[]">

        <option value="1" {{$coach->coachings[0]->status == '1' ? 'selected' : ''}}>Active</option>
        <option value="2" {{$coach->coachings[0]->status == '2' ? 'selected' : ''}}>Inactive</option>
      </select>
    </td>
    <td><a target="_blank" href="/uploaded/coaching/{{trim($coach->coachings[0]->memo)}}">{{$coach->coachings[0]->memo}}</a><br><br>
      <input class="form-control" type="file" id="file" name="file[]" multiple></td>
    <td class="text-center"><div title="Delete item" class="mt-1 top-label del-item" id="coaching-info" rel="{{$coach->coachings[0]->id ?? ''}}"><i class="fas fa-trash-alt"></i></div></td>
  </tr>
  @endforeach

</table>
</form>
<button class="btn btn-sm btn-success mt-1 mb-2" id="add_coaching_row">Add New</button>
