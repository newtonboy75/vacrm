<h5 class="text-muted header-underlined-wide">Memo</h5>

<label class="top-label mt-3">Memo</label>
<form id="formEmployee" enctype="multipart/form-data">
<input type="hidden" name="applicant_id" value="{{$applicant->id}}">
<input type="hidden" name="from_tab" value="memo">

<table class="table table-sm table-bordered" id="memo_tbl">
  <tr>

    <th scope="col" style="width: 180px;">Date Served</th>
    <th scope="col" style="width: 150px;">Issued By</th>
    <th scope="col" style="width: 180px;">	Date Signed</th>
    <th scope="col">Violation</th>
    <th scope="col" style="width: 120px;">Status</th>
    <th scope="col">Memo</th>
    <th scope="col"></th>
  </tr>

  @foreach($memos as $memo)
  <tr>
    <td>
      <input type="hidden" name="coaching_id[]" value="{{$memo->memos[0]->id}}">
      <input data-zdp_readonly_element="false" type="text" class="form-control" id="dpicker" name="date_served[]" value="{{ date('F d, Y g:i', strtotime($memo->memos[0]->date_served)) ?? '' }}"></td>
    <td>
      <select class="form-control" name="issued_by[]" id="issued_by">
        @foreach($allCoaches as $dcoach)
          <option value="{{$dcoach->id}}" {{$dcoach->id ==$memo->memos[0]->id ? 'selected' : ''}}>{{$dcoach->first_name}} {{$dcoach->last_name}}</option>
        @endforeach
      </select>
    </td>
    <td><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpicker" name="date_signed[]" value="{{ date('F d, Y g:i', strtotime($memo->memos[0]->date_signed )) ?? '' }}"></td>
    <td><input type="text" class="form-control" id="violation" name="violation[]" value="{{ $memo->memos[0]->violation ?? '' }}"></td>
    <td>
      <select class="form-control" name="status[]">
        <option value="1" {{$memo->memos[0]->status == '1' ? 'selected' : ''}}>Active</option>
        <option value="2" {{$memo->memos[0]->status == '2' ? 'selected' : ''}}>Inactive</option>
      </select>
    </td>
    <td id="memoid" rel="{{$memo->memos[0]->id}}" class="text-center">
        @if($memo->memos[0]->memo != '')
          @php
            $contract = App\EmployeeContract::where('contract_id', '=', $memo->memos[0]->memo)->first();


            $contract_type = [
              'caf' => 'Corrective Action Form',
              'gva_quarterly' => 'GVA Quarterly',
              'final' => 'Final',
              'hold-suspension' => 'Hold Suspension',
              'isa-quarterly' => 'ISA Quarterly',
              'disclosure-form' => 'Disclosure',
              'fte-form' => 'FTE Form',
              'nte-form' => 'NTE Form',
              'termination-letter' => 'Termination Letter',
              'memorandum-form' => 'Memorandum',
              'assurance-form' => 'Assurance Form',
              'codeofconduct-form' => 'Code of Conduct',
              'ica-form' => 'ICA Form',
              'nca-form' => 'NCA Form',
            ];

          @endphp

          @if($contract)

          {{$contract_type[$contract->contract_type]}} | ID: {{$memo->memos[0]->memo}}<br>
          STATUS: <a  href="/admin/esignature/{{$contract->contract_type}}/{{$contract->employee_id}}/{{$memo->memos[0]->id}}/{{$contract->id}}">{{$contract->recipient_action}}</a>
          <input type="hidden" name="termination_file[]" value="{{$memo->memos[0]->memo}}">

          @endif

        @else
        <input type="hidden" name="termination_file[]" value="">
        <input type="hidden" id="uid" value="{{$applicant->id}}">
          <select class="form-control" id="esig_contracts" title="Please save changes to activate">
            <option>Select</option>
            <option value="nca-form">NCA</option>
            <option value="fte-form">FTE</option>
            <option value="nte-form">NTE</option>
            <option value="hold-suspension">Placement - Hold/Suspension</option>
            <option value="caf">Corrective Action Form</option>
            <option value="termination-letter">Termination Letter</option>
          </select>
        @endif

    </td>
    <td class="text-center"><div title="Delete memo" class="mt-1 top-label del-item" id="memo-info" rel="{{$memo->memos[0]->id ?? ''}}"><i class="fas fa-trash-alt"></i></div></td>
  </tr>
  @endforeach

</table>
</form>
<button class="btn btn-sm btn-success mt-1 mb-2" id="add_memo_row">Add New</button>
