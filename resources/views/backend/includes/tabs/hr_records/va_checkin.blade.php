<h5 class="text-muted header-underlined-wide">VA Checkin Record</h5>
<label class="top-label mt-3">Checkin Records</label>
<br>
<form id="formEmployee" enctype="multipart/form-data">
<input type="hidden" name="applicant_id" value="{{$applicant->id}}">
<input type="hidden" name="from_tab" value="va_checkin">

<table class="table" id="checkin_tbl">
  <tr>
    <th scope="col">Check In Date</th>
    <th scope="col">Issued By</th>
    <th scope="col">Date Signed</th>
    <th scope="col">File</th>
    <th scope="col"></th>
  </tr>
  @foreach($checkins as $checkin)
    <tr>
      <td>
        <input type="hidden" name="va_id[]" value="{{$checkin->checkins[0]->id}}">
        <input data-zdp_readonly_element="false" type="text" class="form-control dpk" id="dpicker" name="served_date[]" value="{{ date('F d, Y g:i a', strtotime($checkin->checkins[0]->served_date)) ?? '' }}"></td>
      <td>
        <select name="issued_by[]" class="form-control" id="issued_by">
        @foreach($allCoaches as $dcoach)
          <option value="{{$dcoach->id}}" {{$dcoach->id == $checkin->checkins[0]->issued_by ? 'selected' : ''}}>{{$dcoach->first_name}} {{$dcoach->last_name}}</option>
        @endforeach
      </select>
      </td>
      <td><input data-zdp_readonly_element="false" type="text" class="form-control dpk" id="dpicker" name="signed_date[]" value="{{ date('F d, Y g:i a', strtotime($checkin->checkins[0]->signed_date)) ?? '' }}"></td>
      <td style="text-align:center; padding: 10px;">
        <div class="btn-group btn-group-sm mx-auto" role="group" aria-label="File">

          <input type="hidden" name="file[]" id="file" value="{{ trim($checkin->checkins[0]->file) ?? '' }}">

          <br><br>
          @if($checkin->checkins[0]->file != '')

          <a title="view" class="btn btn-success" href="/form_templates/{{trim($checkin->checkins[0]->file)}}" target="_blank"><i class="far fa-file-pdf"></i></a>
          <a title="remove" class="btn btn-red" href="javascript:;" id="va_checkin_file-delete" rel="{{$applicant->id}}"><i class="fas fa-times-circle"></i></a>
          @else
          <a class="btn btn-primary" title="create" href="javascript:;" id="va_checkin_file" data-toggle="modal" data-target="#checkinModal"><i class="far fa-edit"></i></a>
          @endif

        </div>
        </td>
      <td class="text-center"><div title="Delete record" class="mt-1 top-label" id="checkin-info-delete" rel="{{$checkin->checkins[0]->id ?? ''}}"><i class="fas fa-times"></i></div></td>
    </tr>
  @endforeach
</table>
</form>
<button class="btn btn-sm btn-success mt-1 mb-2" id="add_checkin_row">Add New</button>

@include('backend.includes.tabs.hr_records.popups.va_checkin')
