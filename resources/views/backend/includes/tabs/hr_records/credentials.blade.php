<h5 class="text-muted header-underlined-wide">Employee Credentials</h5>
<label class="top-label mt-3">Employee Name</label>
<input type="hidden" name="applicant_id" value="{{$applicant->id}}">
<input type="hidden" name="from_tab" value="credential">
<table class="table table-sm table-bordered">
  <thead>
    <tr>
      <th>Account</th>
      <th>Login</th>
      <th>Password</th>

    </tr>
  </thead>
  <tbody id="tbl-ref">
    <tr>
      <td class="text-center"><label class="top-label mt-2">VD Skype</label></td>
      <td><input class="form-control" type="text" name="vd_skype" value="{{$applicant->vd_skype}}"></td>
      <td><input class="form-control" type="text" name="vd_skype_pass" value="{{$applicant->vd_skype_pass}}"></td>

    </tr>
    </tr>
      <td class="text-center"><label class="top-label mt-2">VD Email</label></td>
      <td><input class="form-control" type="text" name="vd_email" value="{{$applicant->vd_email}}"></td>
      <td><input class="form-control" type="text" name="vd_email_pass" value="{{$applicant->vd_email_pass}}"></td>

    </tr>
  </tbody>
</table>
