<!--contracts-->
<div class="modal fade" id="boxHoldSuspensionModal" tabindex="-1" role="dialog" aria-labelledby="boxHoldSuspensionModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="boxHoldSuspensionModalTitle">Hold or Suspension Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="table-holder">
        <form action="{{ url('admin/hr-operation-record/esig-hold') }}" method="post">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-sm-2">Requested by: </div>
            <div class="col-sm-10">
              <select class="form-control" name="client-lists" id="client-lists"></select>
              <input type="hidden" name="id" class="form-control" value="{{$applicant->id}}">
              <input type="hidden" name="recipient_email" class="form-control" value="{{$applicant->email}}"></div>
          </div>

          <div class="row mt-3">
            <div class="col-sm-2">VA Name: </div>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="recipient_name" value="{{ $applicant->name }}">
            </div>
          </div>

          <div class="row mt-3">
            <div class="col-sm-2">Program: </div>
            <div class="col-sm-10">
              <select class="form-control" name="program">
                <option value="GVA">GVA</option>
                <option value="ISA">ISA</option>
                <option value="TC">TC</option>
                <option value="EVA">EVA</option>
                <option value="TIMEBLOCK">TIMEBLOCK</option>
              </select>
            </div>
          </div>

          <div class="row mt-3">
            <div class="col-sm-2">Current Status with Client: </div>
            <div class="col-sm-10">
              <select class="form-control" name="status">
                <option value="Placements">Placements</option>
                <option value="Active">Active</option>
                <option value="Inactive">Inactive</option>
                <option value="Cancelled">Cancelled</option>
              </select>
            </div>
          </div>

          <div class="row mt-3">
            <div class="col-sm-2">Reason For Hold or Suspension (Please place detailed info on why VA is for On Hold status on Placements) </div>
            <div class="col-sm-10">
              <textarea class="form-control" name="reason"></textarea>
            </div>
          </div>

          <div class="row mt-3">
            <div class="col-sm-2">Final Hold Decision </div>
            <div class="col-sm-10">
              <select class="form-control" name="final_decision">
                <option value="Part Time Client Only">Part Time Client Only</option>
                <option value="Active">For Termination</option>
                <option value="Inactive">Released</option>
              </select>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" value="Submit and view contract" class="btn btn-primary">
        </form>
      </div>
    </div>
  </div>
</div>
