@extends('backend.layouts.app')
@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
          <div class="col-sm-8 mx-auto" >

            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{ session()->get('success') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif

            @if(session()->has('error'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
              {{ session()->get('error') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif

            <h5>Corrective Action File</h5>
            <p>&nbsp;</p>

            <form action="{{ url('admin/hr-operation-record/caf-form-send') }}" method="post" id="tf">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-sm-2">VA Name: </div>
                <div class="col-sm-10"><input type="text" id="recipient" name="recipient" class="form-control" placeholder="VA Name" value="{{$applicant->name ?? ''}}" >
                <input type="hidden" name="recipient_name" class="form-control" value="{{$applicant->name ?? ''}}">
                <input type="hidden" name="id" class="form-control" value="{{$applicant->id ?? ''}}">
                <input type="hidden" name="recipient_email" class="form-control" value="{{$applicant->email ?? ''}}"></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Client Name: </div>
                <div class="col-sm-10"><input type="text" value="" name="client_name" class="form-control" placeholder="Client Name" ></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Team: </div>
                <div class="col-sm-10"><input type="text" value="" name="team" class="form-control" placeholder="Team" ></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Assigned Team Lead: </div>
                <div class="col-sm-10"><input type="text" value="" name="team_lead" class="form-control" placeholder="Assigned Team Lead" ></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Date Filed: </div>
                <div class="col-sm-10"><input id="current_va_date" type="text" value="{{date('M d, Y')}}" name="date" class="form-control" placeholder="Date" ></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Prepared By: </div>
                <div class="col-sm-10"><input type="text" value="{{$user->first_name}} {{$user->last_name}}" name="prepared_by" class="form-control" placeholder="" ></div>
              </div>
              <hr>
              <h5>Incident Report Details</h5><br>
              <div class="row mt-2">
                <div class="col-sm-2">Date of Incident: </div>
                <div class="col-sm-10"><input id="current_va_date" type="text" value="" name="date_incident" class="form-control" placeholder="Date" ></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Description of the Offense:</div>
                <div class="col-sm-10"><textarea name="offense_desc" class="form-control" placeholder="Please describe in detail what the offense is" ></textarea></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Evidences:</div>
                <div class="col-sm-10"><textarea class="form-control" id="ebody" name="evidendes"></textarea><br>(Please provide evidences to support VAs violation. Ex. screenshots, etc.)</div>
              </div>

              <hr>
              <h5>Code of Conduct Policy Violated</h5><br>
              <div class="row mt-2">
                <div class="col-sm-2">Supervisor Remarks:</div>
                <div class="col-sm-10"><textarea class="form-control" id="ebody2" name="coc"></textarea><br>(Please copy paste on this section the exact policy on the COC that was
violated. This would be considered as a supporting argument towards the incident)</div>
              </div>

              <hr>
              <h5>HR Incident Decision</h5><br>
              <div class="row mt-2">
                <div class="col-sm-2">Decision:</div>
                <div class="col-sm-10"><textarea class="form-control" name="decision"></textarea><br>(Please provide a detailed explanation on what the sanction/decision is for this incident)</div>
              </div>


              <input type="hidden" name="contract_type" value="caf">
              <input type="hidden" name="memoid" value="{{request()->memoid}}">


              <div class="modal-footer mt-5">
                <a href="/admin/hr-operation-record/{{request()->id}}/edit" type="button" class="btn btn-secondary" data-dismiss="modal">Back</a>
                <button class="btn btn-primary" id="send-termination-letter">Crete File</button>
                </form>
              </div>

          </div>
        </div>
      </div>
</div>


@include('backend.includes.tabs.contracts.popup_termination')
@endsection
