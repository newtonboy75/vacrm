@extends('backend.layouts.app')
@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
          <div class="col-sm-8 mx-auto" >

            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{ session()->get('success') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif

            @if(session()->has('error'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
              {{ session()->get('error') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif

            <h5>Send Final Notice</h5>
            <p>&nbsp;</p>

            <form action="{{ url('admin/hr-operation-record/termination-form-send') }}" method="post" id="tf">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-sm-2">To: </div>
                <div class="col-sm-10"><input type="text" id="recipient" name="recipient" class="form-control" placeholder="VA Name" value="{{$applicant->name ?? ''}}" >
                <input type="hidden" name="recipient_name" class="form-control" value="{{$applicant->name ?? ''}}">
                <input type="hidden" name="id" class="form-control" value="{{$applicant->id ?? ''}}">
                <input type="hidden" name="recipient_email" class="form-control" value="{{$applicant->email ?? ''}}"></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">From: </div>
                <div class="col-sm-10"><input type="text" value="VirtuDesk Management Team" name="from" class="form-control" placeholder="VA Name" ></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Date: </div>
                <div class="col-sm-10"><input type="text" id="current_date" name="current_date" class="form-control" placeholder="VA Name" value="{{ date('M d, Y') }}"></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Subject: </div>
                <div class="col-sm-10">
                  <textarea id="ebody2d" class="form-control" name="subject" row="2">Termination</textarea></div>
              </div>

              <div class="row mt-2">
                <div class="col-sm-2">Body: </div>
                <div class="col-sm-10">
                  <textarea  id="ebody" class="form-control" rows="5" name="body">
                <p>We are writing this letter to inform you of your termination with the company for violating our Code of
                Conduct and the Training Policy.</p>
                <p>Please be informed that as per the report from Training, you were MIA (Missing in Action) during the first
                day of training – April 15, 2019 after lunch break. There was no notification that was received from you
                about this incident. This violation falls under two (2) offenses against the Company’s Code of Conduct
                and the Training Policy which merits a termination of contract:</p>

                <p style="font-weight: bold;">Under the Virtudesk Code of Conduct:<br>
                Offense against Conduct and Decorum – Failure to notify the company when unable to perform
                service at the agreed time.</p>

                <p style="font-weight: bold;">Under the Virtudesk Training Policy:<br>
                Attendance and Punctuality - All trainees are expected to have 100% attendance for the first 3
                months coming from their New Hire Training start date until Placements or until they start with
                their first client, whichever comes first. A perfect attendance is described as no absences, no
                tardiness, no over break and no idle time within the 4-8 hour shift for the whole duration of
                training.<p>

                <p>Please understand that the behavior you have shown is against the prescribed conduct of a contractor,
                procedure and rules and regulations of the company. We expect our contractors to act accordingly.
                Should you need any clarification, do not hesitate to contact us. We hope you will be able to make a
                new beginning career wise.<p>

                <p>Best Regards,<br>
                Virtudesk Management
                </p>
              </textarea>
                </div>
              </div>

              <input type="hidden" name="contract_type" value="{{request()->type}}">
              <input type="hidden" name="memoid" value="{{request()->memoid}}">


              <div class="modal-footer mt-5">
                <a href="/admin/hr-operation-record/{{request()->id}}/edit" type="button" class="btn btn-secondary" data-dismiss="modal">Back</a>

                <button class="btn btn-success" id="preview">Preview/Download</button>
                <button class="btn btn-primary" id="send-termination-letter">Send</button>
                </form>
              </div>

          </div>
        </div>
      </div>
</div>


@include('backend.includes.tabs.contracts.popup_termination')
@endsection
