<!--contracts-->
<div class="modal fade" id="boxModal" tabindex="-1" role="dialog" aria-labelledby="boxModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="boxModalTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="table-holder">
        <form action="{{ url('admin/hr-operation-record/load-contract') }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
        <input type="hidden" name="user_id" id="user_id" value="">
        <label class="top-label">Select client</label>
        <select class="form-control" id="all_clients" name="client">

        </select><br>
        <label class="top-label">Select screenshot to upload</label>
        <input class="form-control" type="file" name="file" id="fileToUpload" accept="image/*"><br>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" value="Upload screenshot and view contract" class="btn btn-primary">
        </form>
      </div>
    </div>
  </div>
</div>
