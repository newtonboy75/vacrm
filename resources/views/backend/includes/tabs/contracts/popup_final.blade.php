<!--contracts-->
<div class="modal fade" id="boxFinalModal" tabindex="-1" role="dialog" aria-labelledby="boxFinalModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="boxFinalModalTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="table-holder">
        <form action="{{ url('admin/hr-operation-record/fte-form') }}" method="get">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-sm-2">To: </div>
            <div class="col-sm-10"><input type="text" name="recipient" class="form-control" placeholder="VA Name" value="{{$applicant->name}}" disabled>
            <input type="hidden" name="recipient_name" class="form-control" value="{{$applicant->name}}">
            <input type="hidden" name="id" class="form-control" value="{{$applicant->id}}">
            <input type="hidden" name="recipient_email" class="form-control" value="{{$applicant->email}}"></div>
            <input type="hidden" name="memoid" class="form-control" value="{{request()->memoid}}"></div>
          </div>

          <div class="row mt-2">
            <div class="col-sm-2">From: </div>
            <div class="col-sm-10"><input type="text" value="VirtuDesk Management Team" name="from" class="form-control" placeholder="VA Name" disabled></div>
          </div>

          <div class="row mt-2">
            <div class="col-sm-2">Date: </div>
            <div class="col-sm-10"><input type="text" name="current_date" class="form-control" placeholder="VA Name" value="{{ date('M d, Y') }}"></div>
          </div>

          <div class="row mt-2">
            <div class="col-sm-2">Subject: </div>
            <div class="col-sm-10"><textarea id="ebody2" class="form-control" rows="5"  name="subject">sdfsfsfFinal Notice to Explain for Multiple attendance offenses: Tardiness, Abuse of personal privilege (over-breaks and frequent breaks), Slack time while performing service, Failure to report for work (with or without notification), Providing invalid reason for failure to report for work (with or without notification). <br>A combination of one (1) or more of the given attendance offenses above but should not exceed more than three (3) of these given offenses</textarea></div>
          </div>

          <div class="row mt-2">
            <div class="col-sm-2">Body: </div>
            <div class="col-sm-10"><textarea  id="ebody" class="form-control" rows="5" name="body">
              <p>Please be informed that as per the report from Operations and Workforce you have committed the following attendance offenses on the following dates:</p>

                <ul>
                  <li>October 2, 2019 – 11 minutes (Tardiness)</li>
                  <li>October 18, 2019 – 15 minutes (Over-break)</li>
                  <li>October 25, 2019 – 17 minutes (Tardiness)</li>
                  <li>October 25, 2019 – Undertime</li>
                  <li>October 30, 2019 – 1 hour and 39 minutes (Over-break)</li>
                </ul>

            <p>The screen shot below is the Corrective Action Form submitted by your assigned manager, Cheryll Cos on November 7, 2019 for your offense mentioned above:</p>

            [change url screenshot]

            <p>Multiple attendance offense is a Major Offense against Productivity: Tardiness, Abuse of personal privilege (over-breaks and frequent breaks), Slack time while performing service, Failure to report for work (with or without notification), Providing invalid reason for failure to report for work (with or without notification).</p>

            <ul><li>A combination of one (1) or more of the given attendance offenses above but should not exceed more than three (3) of these given offenses</li></ul>

            <p>As per the company’s Code of Conduct, if a major offense is committed the offender’s assigned manager will escalate the matter to HR. HR will send a final written warning (FTE) to offender. A monetary deduction of $0.50 per hour for two pay periods will be made from the offender’s salary. However, management deems instead to issue to you a lower monetary deduction of $.50/hour for one pay period.</p>

            <p>Submit your written explanation within “forty-eight (48) hours”. This will give you an opportunity to explain to defend yourself and provides you due process. Please include and attach all evidence you feel have weight on this matter. Failure to respond within the deadline will result in a decision made without delay and in favor of this Show Cause Memo.</p>

            <p>By signing this document, you hereby acknowledge full understanding and confirm that all the details enumerated above are accurate and true. This document must be re-sent with your affixed signature 48 hours upon receipt.</p>
          </textarea></div>
          </div>


          <div class="row mt-2">
            <div class="col-sm-2">Received by: </div>
            <div class="col-sm-10">
              <input type="text" name="recipient_" class="form-control" value="{{$applicant->name}}" disabled placeholder="VA Name">
              <input type="hidden" name="recipient_name" class="form-control" value="{{$applicant->name}}" placeholder="VA Name">
            </div>
          </div>

          <div class="row mt-2">
            <div class="col-sm-2">Position: </div>
            <div class="col-sm-10">
              <input value="{{ strtoupper($applicant->position) }}" disabled type="text" name="va_position2" class="form-control" placeholder="General Virtual Assistant">
              <input value="{{ strtoupper($applicant->position) }}" type="hidden" name="va_position" class="form-control" placeholder="General Virtual Assistant">
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sign</button>
        <input type="submit" value="Submit and view contract" class="btn btn-primary">
        </form>
      </div>
    </div>
  </div>
</div>
