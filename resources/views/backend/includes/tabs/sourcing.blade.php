<div class="row" >
  <!--applicant info -->
  <div class="col-sm-4">
    <div style="border: 1px solid #f0f0f0; padding: 20px;">
      <div class="field-box">
        <h5 class="text-muted header-underlined">Applicant Info</h5>
        <input type="hidden" name="csrf_token" value="{{ csrf_token() }}">
        <input type="hidden" name="from_tab" id="from_tab" value="sourcing-record">
        <input type="hidden" name="applicant_id" id="from_tab" value="{{$applicant->id}}">
      </div>

      <div class="field-box">
        <label class="top-label">Application ID</label>
        <input class="form-control" type="text" value="{{$applicant->id}}" disabled name="sourcing_applicant_id">
      </div>

      <div class="field-box">
        <label class="top-label">Applicant's Name</label>
        <input class="form-control" type="text" value="{{ucwords(strtolower($applicant->name))}}" name="sourcing_applicant_name">
      </div>

      <div class="field-box">
        <label class="top-label">Email Address</label>
        <input class="form-control" type="text" value="{{$applicant->email}}" name="sourcing_applicant_email">
      </div>

      <div class="field-box">
        <label class="top-label">Landline Number</label>
        <input class="form-control" type="text" value="{{$applicant->landing_number == '0' ? '' : $applicant->landing_number }}" name="sourcing_landline_number">
      </div>

      <div class="field-box">
        <label class="top-label">Mobile Number</label>
        <input class="form-control" type="text" value="{{$applicant->mobile_number}}" name="sourcing_mobile_number">
      </div>

      <div class="field-box">
        <label class="top-label">Complete Address</label>
        <input class="form-control" type="text" value="{{ucwords(strtolower($applicant->address))}}" name="sourcing_address">
      </div>

      <div class="field-box">
        <label class="top-label">Skype ID</label>
        <input class="form-control" type="text" value="{{$applicant->skype_id}}" name="sourcing_skype_id">
      </div>

      <div class="field-box">
        <label class="top-label">Blogs/Social Media/Portfolio URL</label>
        <input class="form-control" type="text" value="{{$applicant->vats->blog ?? ''}}" name="blog">
      </div>

      <div class="field-box">
        <label class="top-label">With 6 months of Real Estate Virtual Assistant Experience?</label><br>
        <select class="form-control" name="has_real_estate_exp">
          <option value="" disabled selected>Please choose</option>
          <option value="yes" {{$applicant->has_real_estate_exp == 'yes' ? 'selected' : ''}}>Yes</option>
          <option value="no" {{$applicant->has_real_estate_exp == 'no' ? 'selected' : ''}}>No</option>
        </select>
      </div>

      <div class="field-box">
        <label class="top-label">How did you hear about Virtudesk</label>
        <input class="form-control" type="text" value="{{$applicant->come_from ?? '' }}" name="sourcing_come_from">
      </div>

      <div class="field-box" style="display: none; ">
        <label class="top-label">Upload CV</label>

        <input disabled style="background: none; border: none;" class="form-control" type="file" id="change-cv" name="cv" accept="application/pdf"><br>
        &nbsp;&nbsp;&nbsp;<span class="text-red mt-2" id="enable-upload">[ Click to enable upload ]</span><br>

      </div>

      <br><br>
      <a href="javascript:;" class="btn btn-info dropdown-toggle btn-sm" id="rating_toggler">User Skills Ratings</a><br>

      @if(isset($applicant->ratings))
      @php
      $allratings = unserialize($applicant->ratings->app_ratings);
      $colors[0] = 'white';
      $colors[1] = 'red';
      $colors[2] =  'orange';
      $colors[3] = 'yellow';
      $colors[4] = 'green';
      $colors[5] = 'blue';
      @endphp
      <div class="field-box" id="toggle_ratings">

        <table class="table table-sm table-striped">
          <thead><tr><td>&nbsp;</td><td>Rating</td></tr></thead>
          <tbody>
            @foreach($allratings as $ar=>$r)

            <tr><td><strong>{{ ucwords(str_replace('_', ' ', $ar)) }}</strong></td><td>
              <span style="color: {{ $colors[$r] ?? '' }}">
                @for($i=0; $i<$r; $i++)
                &#9679;
                @endfor
              </span>
              <span class="float-right">({{$r}})</span>
            </td></tr>

            @endforeach
          </tbody>
        </table>
      </div>
      @endif

    </div>
  </div>

  <!-- source -->
  <div class="col-sm-4">

    <div style="border: 1px solid #f0f0f0; padding: 20px;">
      <div class="field-box">
        <h5 class="text-muted header-underlined">Source</h5>
      </div>

      @if(count($applicant->cv) > 0)
      <label class="top-label">Attached CV</label><br>
      <div class="attached-cv" style="width: 100% !important;">
        <a title="{{$applicant->cv[0]['file_name']}}" class="" href="{{URL::to('admin/download-file/resumes/')}}/{{$applicant->cv[0]['file_name']}}" target="_blank">
          <img src="https://virtudeskcrm.s3.us-west-2.amazonaws.com/images/resume_img.jpg" width="150"><br><br>{{$applicant->cv[0]['file_name']}}</a>
        </div>

        @endif

        <div class="field-box">
          <label class="top-label">Position Applying For</label>

          <select class="form-control" name="sourcing_position_applying_for">
            <option disabled selected>Select Position</option>
            <option value="eva" {{$applicant->position_applying_for == 'eva' ? 'selected' : ''}}>Executive Virtual Assistant (EVA)</option>
            <option value="isa" {{$applicant->position_applying_for == 'isa' ? 'selected' : ''}}>Inside Sales Assistant (ISA)</option>
            <option value="gva" {{$applicant->position_applying_for == 'gva' ? 'selected' : ''}}>General Virtual Assistant (GVA)</option>
          </select>
        </div>

        <div class="field-box">

          <label class="top-label">Employment History</label><br>
          @if(isset($applicant->vats->employment_history))

          @php

          $emp_histories = unserialize($applicant->vats->employment_history);

            $employer = $emp_histories['employer'];
            $date_employment = $emp_histories['date_employment'];
            $position = $emp_histories['position'] ?? ['eva'];
            $responsibilities = $emp_histories['responsibilities'] ?? ['na'];
            $sample_works = $emp_histories['sample_works'] ?? ['na'];

          @endphp

          @if(isset($employer))
          @for($i=0; $i < count($employer); $i++)
          <div class="field-box" style="border: 1px solid #f0f0f0; padding: 20px;">
            <label class="top-label">Company/Employer</label>
            <input class="form-control" type="text" value="{{$employer[$i]}}" name="emp_history_employer[]">

            <label class="top-label">Date Employment</label>

            @if(is_array($date_employment))

            @php

            if(is_array($date_employment[0])){
              $date_emp_start = $date_employment[0][$i];
              $date_emp_end = $date_employment[1][$i];

            }else{
              $date_emp_start = isset($date_employment[$i][0]) ? $date_employment[$i][0] : '';
              $date_emp_end = isset($date_employment[$i][1]) ? $date_employment[$i][1] : '';
            }

            @endphp

            <div class="input-group">
              <label for="date_employment_from"><small>FROM</small>
                <input type="month" class="form-control" name="date_employment_from[]" id="date_employment_from" value="{{ $date_emp_start ?? '' }}" min="1970-01-01" max="2021-12-31" required>
              </label>&nbsp;&nbsp;
              <label for="date_employment_to"><small>TO</small>
                <input type="month" class="form-control" name="date_employment_to[]" id="date_employment_to"  value="{{ $date_emp_end ?? '' }}" min="1970-01-01" max="2021-12-31" required>
              </label>
            </div>

            @elseif(strstr($date_employment[$i], '_'))
            @php
            $date_emp = explode('_', $date_employment[$i]);
            @endphp
            <div class="input-group">
              <label for="date_employment_from"><small>FROM</small>
                <input type="month" class="form-control" name="date_employment_from[]" id="date_employment_from" value="{{$date_emp[0][0]}}" min="1970-01-01" max="2021-12-31" required>
              </label>&nbsp;&nbsp;
              <label for="date_employment_to"><small>TO</small>
                <input type="month" class="form-control" name="date_employment_to[]" id="date_employment_to"  value="{{$date_emp[0][1]}}" min="1970-01-01" max="2021-12-31" required>
              </label>
            </div>
            @else
            <div class="input-group">
              <label for="date_employment_from"><small>FROM</small>
                <input type="month" class="form-control" name="date_employment_from[]" id="date_employment_from" value="{{$date_employment[0]}}" min="1970-01-01" max="2021-12-31" required>
              </label>&nbsp;&nbsp;
              <label for="date_employment_to"><small>TO</small>
                <input type="month" class="form-control" name="date_employment_to[]" id="date_employment_to"  value="{{$date_employment[0]}}" min="1970-01-01" max="2021-12-31" required>
              </label>
            </div>
            @endif

            <label class="top-label">Position</label>
            <input class="form-control" type="text" value="{{$position[$i]}}" name="emp_position[]">
            <label class="top-label">Responsibilities</label>
            <textarea class="form-control" name="emp_responsibilities[]" style="max-height: 200px; min-height: 100px;">{{$responsibilities[$i] ?? ''}}</textarea>
            <label class="top-label">Sample works/blog/portfolio URL</label>
            <input type="text" class="form-control" name="sample_works[]" value="{{$sample_works[$i] ?? ''}}">
          </div>
          @endfor
          @endif
          @endif
        </div>
      </div>

    </div>
    <!-- status and updates -->
    <div class="col-sm-4">

      <div style="border: 1px solid #f0f0f0; padding: 20px;">
        <div class="field-box">
          <h5 class="text-muted header-underlined">Initial Interview & Status</h5>
        </div>
        <div class="field-box">
          <label class="top-label">Application Date</label>
          <div class="input-group input-group mb-3" style="width: 300px;">
            <input class="form-control" type="text" value="{{date('M d, Y', strtotime($applicant->created_at))}}" id="datepicker" disabled name="sourcing_application_date">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
            </div>
          </div>
        </div>

        <div class="field-box">
          <label class="top-label">Status</label><br>
          <select class="form-control" name="sourcing_application_status">
            <option value="1" {{$applicant->status == '1' ? 'selected' : ''}}>Submitted Application Form</option>
            <option value="2" {{$applicant->status == '2' ? 'selected' : ''}}>Passed Paper Screening</option>
            <option value="3" {{$applicant->status == '3' ? 'selected' : ''}}>Failed Paper Screening</option>
            <option value="8" {{$applicant->status == '8' ? 'selected' : ''}}>Pending / On Hold</option>
          </select>
        </div>

        <div class="field-box">
          <label class="top-label">Notes</label>
          <textarea class="form-control" name="sourcing_notes">{{$applicant->notes}}</textarea>
        </div>

        <div class="field-box">
          <label class="top-label"><input type="checkbox" name="sourcing_red_flag" {{ $applicant->red_flag ? 'checked' : '' }}> Red Flag</label><br>
          <label class="top-label">Red Flag Notes</label>
          <textarea class="form-control" name="sourcing_red_flag_notes">{{$applicant->red_flag_notes}}</textarea>
        </div>

        <div class="field-box">
          <label class="top-label mt-2">Recruiter</label><br>
          <select class="form-control" name="sourcing_source_id" id="sourcing_source_id">
            <option value="0">Please choose</option>
            @if(count($allrecruiters) >= 1)
            @foreach($allrecruiters as $recruiter)
            <option rel="{{$recruiter->email ?? ''}}|{{$recruiter->skype_id ?? ''}}" value="{{$recruiter->id ?? ''}}" {{$applicant->recruiter_id == $recruiter->id ? 'selected' : ''}}>{{$recruiter->first_name ?? ''}} {{$recruiter->last_name ?? ''}}</option>
            @endforeach
            @endif
          </select><br>
          <label class="top-label mt-3">Initial Interview Date</label><br>
          <input data-zdp_readonly_element="false" style="width: 300px" class="form-control dpk" type="text" value="{{isset($interview->interview_record) ? date('M d, Y, g:i A', strtotime($interview->interview_record[0]->date)) : date('M d, Y, g:i A')}}" name="sourcing_initial_interview_date">

          <div class="field-box">
            <div class="float-right"><a href="/admin/sourcing-record/{{$applicant->id}}/delete" id="del-app-rec" rel="{{$applicant->id}}" title="Delete record" class="btn btn-sm text-red ml-1"><i class="fas fa-trash-alt"></i> Delete Applicant Record</a></div>
            <p>&nbsp;</p>
          </div>
        </div>
      </div>

    </div>
  </div>
