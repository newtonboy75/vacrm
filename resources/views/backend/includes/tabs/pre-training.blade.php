<div class="row">
  <div class="col table-responsive">
    <div class="field-box">
      <form id="pretraining-form">
        @php
        $nbi_img = [];
        $profile_pic = '';

        if($applicant->profile_image_id != ''){
          $profile_pic = App\Media::whereId($applicant->profile_image_id)->first()->name;
        }else{
          $profile_pic = 'placeholder123.png';
        }

        @endphp

        <h5 class="text-muted header-underlined-wide">Pre Training Requirements</h5>
        <input type="hidden" name="csrf_token" value="{{ csrf_token() }}">
        <input type="hidden" name="from_tab" id="from_tab" value="pre-training-requirements">
        <input type="hidden" name="applicant_id" id="applicant_id" value="{{$applicant->id}}">

        <label>2x2 ID Photo</label><br>

        <div id="profile_pic">
          <img id="img-obj" src="https://virtudeskcrm.s3.us-west-2.amazonaws.com/employee_profile_pic/{{$profile_pic}}">
          <label for="fileInput">
            <i id="icon" class="fas fa-arrow-circle-up" title="change profile pic"></i>
          </label>
          <input id="fileInput" type="file" accept="image/*" name="id_image" onchange="document.getElementById('img-obj').src = window.URL.createObjectURL(this.files[0])">
        </div><br>

        <label class="top-label">VD Skype ID</label>
        <input type="text" class="form-control" name="vd_skype" value="{{$applicant->vd_skype}}"><br>

        <label class="top-label">VD Skype Password</label>
        <input type="text" class="form-control" name="vd_skype_pass" value="{{$applicant->vd_skype_pass}}"><br>

        <label class="top-label">VD Email</label>
        <input type="text" class="form-control" name="vd_email" value="{{$applicant->vd_email}}"><br>

        <label class="top-label">VD Email Password</label>
        <input type="text" class="form-control" name="vd_email_pass" value="{{$applicant->vd_email_pass}}"><br>

        <label class="top-label mt-2">NBI Certificate</label><br>


        <div id="profile_pic2">
          @if(isset($applicant->nbi_image))
            @php
            $nbi_img = explode('/', $applicant->nbi_image->name);
            @endphp
          <a target="_blank" href="https://virtudeskcrm.s3.us-west-2.amazonaws.com/home_eval/user_{{$applicant->id}}/{{last($nbi_img)}}">
            <img id="img-obj2" class="nbi-image" src="https://virtudeskcrm.s3.us-west-2.amazonaws.com/home_eval/user_{{$applicant->id}}/{{last($nbi_img)}}">
          </a>
          @else
          <a target="_blank" href="https://virtudeskpro.com/uploaded/employee_profile_pic/placeholder123.png">
            <img id="pou-obj2" class="pou-image" src="https://virtudeskpro.com/uploaded/employee_profile_pic/placeholder123.png">
          </a>
          @endif

          <label for="fileInput2">
            <i id="icon" class="fas fa-arrow-circle-up" title="change NBI photo"></i>
          </label>
          <input id="fileInput2" type="file" accept="image/*" name="nbi_file" value="" onchange="document.getElementById('img-obj2').src = window.URL.createObjectURL(this.files[0])">
        </div>

        <br>
        <label class="top-label mt-2">Proof of Unemployment from last employment</label><br>
        <div id="profile_pic2" style="display:inline-block;">

          @if(isset($applicant->pou_file))
          <a target="_blank" href="https://virtudeskcrm.s3.us-west-2.amazonaws.com/employee_pou/{{$applicant->pou_file->name}}">
            <img id="pou-obj2" class="pou-image" src="https://virtudeskcrm.s3.us-west-2.amazonaws.com/employee_pou/{{$applicant->pou_file->name}}">
          </a>
          @else
          <a target="_blank" href="#">
            <img id="pou-obj2" class="pou-image" src="https://virtudeskpro.com/uploaded/employee_profile_pic/placeholder123.png">
          </a>
          @endif

          <label for="fileInput3">
            <i id="icon" class="fas fa-arrow-circle-up" title="change"></i>
          </label>
          <input id="fileInput2" type="file" accept="image/*" name="pou_file" value="" onchange="document.getElementById('pou-obj2').src = window.URL.createObjectURL(this.files[0])">
        </div>

        <br>
        <label class="top-label mt-3">NBI Not Available Comment</label><br>
        <textarea class="form-control" name="no_nbi_comment">{{$applicant->no_nbi ?? ''}}</textarea>

        <br>
        <label class="top-label">Emergency Contact Person</label>
        <input type="text" name="contact_person" class="form-control" value="{{ $applicant->employee_emergency_contact->contact_person_name ?? '' }}">

        <br>
        <label class="top-label">Relation to Emergency Contact Person</label>
        <input type="text" name="contact_person_relation" class="form-control" value="{{ $applicant->employee_emergency_contact->contact_person_relation ?? '' }}">

        <br>
        <label class="top-label">Contact Number of Emergency Contact Person</label>
        <input type="number" name="contact_person_number" class="form-control" value="{{ $applicant->employee_emergency_contact->contact_person_number ?? '' }}">

        <br>
        <label class="top-label mt-3">Status&nbsp;&nbsp</label><span class="text-green">*** Setting status to Endorsed to training will send training schedule to trainee upon clicking update</span><br>
        <select class="form-control" name="pre_training_requirements_status">
          <option value="1" {{$applicant->pre_training_requirements_status == '1' ? 'selected' : '' }}>Incomplete requirements</option>
          <option value="2" {{$applicant->pre_training_requirements_status == '2' ? 'selected' : '' }}>Endorsed to training </option>
          <option value="3" {{$applicant->pre_training_requirements_status == '3' ? 'selected' : '' }}>Fortified - No requirements submitted</option>
        </select>
        <br>
        <label class="top-label">Recruiter's Notes</label><br>
        <textarea class="form-control" name="pre_training_notes">{{ $applicant->pre_training_notes['notes'] }}</textarea>
        <br>
        <label class="top-label">Rate</label>
        <input type="number" name="user_rate" class="form-control" value="{{ $applicant->rate ?? '' }}">
        <br>
        <label class="top-label mt-3">Contracts</label><br>
        <div class="row mb-3">
          @php
          $memorandum = App\EmployeeContract::where('employee_id', '=', $applicant->id)->where('contract_type', '=', 'memorandum-form')->orderBy('id', 'desc')->first();
          $ica = App\EmployeeContract::where('employee_id', '=', $applicant->id)->where('contract_type', '=', 'ica-form')->orderBy('id', 'desc')->first();
          $coc = App\EmployeeContract::where('employee_id', '=', $applicant->id)->where('contract_type', '=', 'codeofconduct-form')->orderBy('id', 'desc')->first();
          $assurance = App\EmployeeContract::where('employee_id', '=', $applicant->id)->where('contract_type', '=', 'assurance-form')->orderBy('id', 'desc')->first();
          $disclosure = App\EmployeeContract::where('employee_id', '=', $applicant->id)->where('contract_type', '=', 'disclosure-form')->orderBy('id', 'desc')->first();
          $nca = App\EmployeeContract::where('employee_id', '=', $applicant->id)->where('contract_type', '=', 'nca-form')->orderBy('id', 'desc')->first();
          $ica_eva = App\EmployeeContract::where('employee_id', '=', $applicant->id)->where('contract_type', '=', 'ica-eva')->orderBy('id', 'desc')->first();
          $ica_isa = App\EmployeeContract::where('employee_id', '=', $applicant->id)->where('contract_type', '=', 'ica-isa')->orderBy('id', 'desc')->first();
          $ica_gva= App\EmployeeContract::where('employee_id', '=', $applicant->id)->where('contract_type', '=', 'ica-gva')->orderBy('id', 'desc')->first();
          @endphp

         <div class="img-container">
            <p>ICA EVA</p>
            <img src="/img/backend/contracts.png">
            <div class="btn-group btn-group-sm" role="group" aria-label="Log Viewer Actions">
              @if($ica_eva)
                @if($ica_eva->recipient_action == 'signed')
                <a target="_blank"  class="btn-success btn signed-contract" href="https://virtudeskpro.com/view/{{$ica_eva->contract_id}}_{{$ica_eva->uid}}"><i class="fas fa-check"></i> Signed</a>
                @elseif($ica_eva->recipient_action == 'pending')
                <a href="/admin/esignature/ica-eva/{{$applicant->id}}" class="btn btn-secondary"> Pending
                @elseif($ica_eva->recipient_action == 'declined')
                <a href="/admin/esignature/ica-eva/{{$applicant->id}}" class="btn btn-danger"> !Declined
                @else
                Send Contract
                @endif
              </a>
              @else
              <a href="/admin/esignature/ica-eva/{{$applicant->id}}" class="btn btn-primary">Send contract</a>
              @endif
            </div>
        </div>

        <div class="img-container">
            <p>ICA ISA</p>
            <img src="/img/backend/contracts.png">
            <div class="btn-group btn-group-sm" role="group" aria-label="Log Viewer Actions">
              @if($ica_isa)
                @if($ica_isa->recipient_action == 'signed')
                <a target="_blank"  class="btn-success btn signed-contract" href="https://virtudeskpro.com/view/{{$ica_isa->contract_id}}_{{$ica_isa->uid}}"><i class="fas fa-check"></i> Signed</a>
                @elseif($ica_isa->recipient_action == 'pending')
                <a href="/admin/esignature/ica-isa/{{$applicant->id}}" class="btn btn-secondary"> Pending
                @elseif($ica_isa->recipient_action == 'declined')
                <a href="/admin/esignature/ica-isa/{{$applicant->id}}" class="btn btn-danger"> !Declined
                @else
                Send Contract
                @endif
              </a>
              @else
              <a href="/admin/esignature/ica-isa/{{$applicant->id}}" class="btn btn-primary">Send contract</a>
              @endif
            </div>
        </div>

        <div class="img-container">
            <p>ICA GVA</p>
            <img src="/img/backend/contracts.png">
            <div class="btn-group btn-group-sm" role="group" aria-label="Log Viewer Actions">
              @if($ica_gva)
                @if($ica_gva->recipient_action == 'signed')
                <a target="_blank"  class="btn-success btn signed-contract" href="https://virtudeskpro.com/view/{{$ica_gva->contract_id}}_{{$ica_gva->uid}}"><i class="fas fa-check"></i> Signed</a>
                @elseif($ica_gva->recipient_action == 'pending')
                <a href="/admin/esignature/ica-gva/{{$applicant->id}}" class="btn btn-secondary"> Pending
                @elseif($ica_gva->recipient_action == 'declined')
                <a href="/admin/esignature/ica-gva/{{$applicant->id}}" class="btn btn-danger"> !Declined
                @else
                Send Contract
                @endif
              </a>
              @else
              <a href="/admin/esignature/ica-gva/{{$applicant->id}}" class="btn btn-primary">Send contract</a>
              @endif
            </div>
        </div>

         <div class="img-container">
            <p>Non-compete Agreement</p>
            <img src="/img/backend/contracts.png">
            <div class="btn-group btn-group-sm" role="group" aria-label="Log Viewer Actions">
              @if($nca)
                @if($nca->recipient_action == 'signed')
                <a target="_blank"  class="btn-success btn signed-contract" href="https://virtudeskpro.com/view/{{$nca->contract_id}}_{{$nca->uid}}"><i class="fas fa-check"></i> Signed</a>
                @elseif($nca->recipient_action == 'pending')
                <a href="/admin/esignature/nca-form/{{$applicant->id}}" class="btn btn-secondary"> Pending
                @elseif($nca->recipient_action == 'declined')
                <a href="/admin/esignature/nca-form/{{$applicant->id}}" class="btn btn-danger"> !Declined
                @else
                Send Contract
                @endif
              </a>
              @else
              <a href="/admin/esignature/nca-form/{{$applicant->id}}" class="btn btn-primary">Send contract</a>
              @endif
            </div>
        </div>

        <div class="img-container">
            <p>Independent Contract</p>
            <img src="/img/backend/contracts.png">
            <div class="btn-group btn-group-sm" role="group" aria-label="Log Viewer Actions">
              @if($ica)
                @if($ica->recipient_action == 'signed')
                <a target="_blank"  class="btn-success btn signed-contract" href="https://virtudeskpro.com/view/{{$ica->contract_id}}_{{$ica->uid}}"><i class="fas fa-check"></i> Signed</a>
                @elseif($ica->recipient_action == 'pending')
                <a href="/admin/esignature/ica-form/{{$applicant->id}}" class="btn btn-secondary"> Pending
                @elseif($ica->recipient_action == 'declined')
                <a href="/admin/esignature/ica-form/{{$applicant->id}}" class="btn btn-danger"> !Declined
                @else
                Send Contract
                @endif
              </a>
              @else
              <a href="/admin/esignature/ica-form/{{$applicant->id}}" class="btn btn-primary">Send contract</a>
              @endif
            </div>
        </div>

        <div class="img-container">
            <p>Leave Memorandum</p>
            <img src="/img/backend/contracts.png">
            <div class="btn-group btn-group-sm" role="group" aria-label="Log Viewer Actions">
              @if($memorandum)
                @if($memorandum->recipient_action == 'signed')
                <a target="_blank"  class="btn-success btn signed-contract" href="https://virtudeskpro.com/view/{{$memorandum->contract_id}}_{{$memorandum->uid}}"><i class="fas fa-check"></i> Signed</a>
                @elseif($memorandum->recipient_action == 'pending')
                <a href="/admin/esignature/memorandum-form/{{$applicant->id}}" class="btn btn-secondary"> Pending
                @elseif($memorandum->recipient_action == 'declined')
                <a href="/admin/esignature/memorandum-form/{{$applicant->id}}" class="btn btn-danger"> !Declined
                @else
                Send Contract
                @endif
              </a>
              @else
              <a href="/admin/esignature/memorandum-form/{{$applicant->id}}" class="btn btn-primary">Send contract</a>
              @endif
            </div>
        </div>

        <div class="img-container">
            <p>Disclosure Form</p>
            <img src="/img/backend/contracts.png">
            <div class="btn-group btn-group-sm" role="group" aria-label="Log Viewer Actions">
              @if($disclosure)
                @if($disclosure->recipient_action == 'signed')
                <a target="_blank"  class="btn-success btn signed-contract" href="https://virtudeskpro.com/view/{{$disclosure->contract_id}}_{{$disclosure->uid}}"><i class="fas fa-check"></i> Signed</a>
                @elseif($disclosure->recipient_action == 'pending')
                <a href="/admin/esignature/disclosure-form/{{$applicant->id}}" class="btn btn-secondary"> Pending
                @elseif($disclosure->recipient_action == 'declined')
                <a href="/admin/esignature/disclosure-form/{{$applicant->id}}" class="btn btn-danger"> !Declined
                @else
                Send Contract
                @endif
              </a>
              @else
              <a href="/admin/esignature/disclosure-form/{{$applicant->id}}" class="btn btn-primary">Send contract</a>
              @endif
            </div>
        </div>

        <div class="img-container">
            <p>Assurance Form</p>
            <img src="/img/backend/contracts.png">
            <div class="btn-group btn-group-sm" role="group" aria-label="Log Viewer Actions">
              @if($assurance)
                @if($assurance->recipient_action == 'signed')
                <a target="_blank"  class="btn-success btn signed-contract" href="https://virtudeskpro.com/view/{{$assurance->contract_id}}_{{$assurance->uid}}"><i class="fas fa-check"></i> Signed</a>
                @elseif($assurance->recipient_action == 'pending')
                <a href="/admin/esignature/assurance-form/{{$applicant->id}}" class="btn btn-secondary"> Pending
                @elseif($assurance->recipient_action == 'declined')
                <a href="/admin/esignature/assurance-form/{{$applicant->id}}" class="btn btn-danger"> !Declined
                @else
                Send Contract
                @endif
              </a>
              @else
              <a href="/admin/esignature/assurance-form/{{$applicant->id}}" class="btn btn-primary">Send contract</a>
              @endif
            </div>
        </div>

         <div class="img-container">
            <p>CODE OF CONDUCT</p>
            <img src="/img/backend/contracts.png">
            <div class="btn-group btn-group-sm" role="group" aria-label="Log Viewer Actions">
              @if($coc)
                @if($coc->recipient_action == 'signed')
                <a target="_blank"  class="btn-success btn signed-contract" href="https://virtudeskpro.com/view/{{$coc->contract_id}}_{{$coc->uid}}"><i class="fas fa-check"></i> Signed</a>
                @elseif($coc->recipient_action == 'pending')
                <a href="/admin/esignature/codeofconduct-form/{{$applicant->id}}" class="btn btn-secondary"> Pending
                @elseif($coc->recipient_action == 'declined')
                <a href="/admin/esignature/codeofconduct-form/{{$applicant->id}}" class="btn btn-danger"> !Declined
                @else
                Send Contract
                @endif
              </a>
              @else
              <a href="/admin/esignature/codeofconduct-form/{{$applicant->id}}" class="btn btn-primary">Send contract</a>
              @endif
            </div>
        </div>

        </div>
        <br>

        <label class="top-label">Trainings</label><br>
        <table class="table">
          <thead>
            <tr>
              <th>Batch #</th>
              <th>Program</th>
              <th>Training Schedule</th>
              <th>Trainer</th>
              <th>Training Start Date</th>
              <th>Training End Date</th>
              <th></th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody id="tbl-training">
          <?php $all_training = []; ?>
            @if(count($applicant->trainings) > 0)
            @foreach($applicant->trainings as $training)
                <?php $all_training[] = $training; ?>

            <tr>
              <input type="hidden" name="training_id[]" value="{{$training->id}}">
              <td><input type="text" class="form-control" name="batch[]" value="{{$training['batch']}}" style="width:50px !important;"></td>
              <td><input type="text" class="form-control" name="program[]" value="{{$training['program']}}"></td>
              <td><input type="text" class="form-control" name="schedule[]" value="{{$training['schedule']}}"></td>
              <td>
                <select name="trainer_id[]" class="form-control" id="trainers">
                @if(count($alltrainers) >= 1)
                  @foreach($alltrainers as $trainer)
                  <option value="{{$trainer->id}}" {{$trainer->id == $training->trainer_id ? 'selected' : ''}}>{{$trainer->name}}</option>
                  @endforeach
                @endif
                </select>
                <td>
                  <input type="text" class="form-control dpk_" name="start_date[]" value="{{ $training['start_date'] }}" title="{{ $training['start_date'] }}" required>
                </td>
                <td><input type="text" class="form-control dpk_" name="end_date[]" value="{{ $training['end_date'] }}" title="{{ $training['end_date'] }}"></td>
                <td>@if($training['email_sent'] == 'yes') <i class="fas fa-paper-plane" title="Email sent"></i> @else <i class="far fa-paper-plane" title="Email not sent"></i> @endif</td>
                <td class="text-center"><div id="del-pre" rel="{{$training['id']}}"><i class="fas fa-trash-alt"></i></div></td>
              </tr>
              @endforeach
              @endif

            </tbody>
          </table>
          <div><button class="btn btn-sm btn-primary" id="training-add-new"><i class="fas fa-plus-circle"></i> Add New</button></div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="popupContractModalCenter" tabindex="-1" role="dialog" aria-labelledby="popupContractModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="popupContractModalLongTitle">Leave Memorandum</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
