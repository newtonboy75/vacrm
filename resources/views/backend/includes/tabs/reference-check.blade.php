<div class="row">
  <div class="col table-responsive">
    <div class="field-box">
      <h5 class="text-muted header-underlined-wide">Reference Check Information</h5>
      <label class="top-label">References</label>
      <input type="hidden" name="csrf_token" value="{{ csrf_token() }}">
      <input type="hidden" name="from_tab" id="from_tab" value="reference-check">
      <input type="hidden" name="applicant_id" id="from_tab" value="{{$applicant->id}}">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Referencer</th>
            <th>Company</th>
            <th>Position</th>
            <th>Mobile Number</th>
            <th>Landline Number</th>
            <th>Email</th>
            <th>Best time to contact</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody id="tbl-ref">
          @if(count($references) >= 1)
              @foreach($references as $ref)
                <tr><input type="hidden" name="ref_id[]" value="{{$ref['reference_record'][0]['id']}}">
                  <td><input type="text" name="referencer[]" class="form-control" value="{{$ref['reference_record'][0]['referencer']}}"></td>
                  <td><input type="text" name="company[]" class="form-control" value="{{$ref['reference_record'][0]['company']}}"></td>
                  <td><input type="text" name="position[]" class="form-control" value="{{$ref['reference_record'][0]['position']}}"></td>
                  <td><input type="text" name="mobile_number[]" class="form-control" value="{{$ref['reference_record'][0]['mobile_number']}}"></td>
                  <td><input type="text" name="landing_number[]" class="form-control" value="{{$ref['reference_record'][0]['landing_number']}}"></td>
                  <td><input type="text" name="email[]" class="form-control" value="{{$ref['reference_record'][0]['email']}}"></td>
                  <td><input type="text" name="best_time_to_contact[]" class="form-control" value="{{$ref['reference_record'][0]['best_time_to_contact']}}"></td>
                  <td class="text-center"><div id="ref-int" rel="{{$ref['reference_record'][0]['id']}}"><i class="fas fa-trash-alt"></i></div></td>
                </tr>
                <tr>
    
                  <td colspan="8">
                    <div class="mr-5" style="width: 300px;"><label class="label-info">Date Contacted</label>
                      <input type="text" name="date_contacted[]" class="form-control" id="dpk-ref" value="{{$ref['reference_record'][0]['date_contacted']}}"></div>
                    </div><br>
                    <div style="width: 300px;"><label class="label-info"><i class="far fa-sticky-note"></i> Recruiter's Note &nbsp;&nbsp;</label>
                      <textarea class="form-control" name="note[]">{{$ref['reference_record'][0]['note']}}</textarea>
                    </div>
                  </td>
    
                </tr>
            @endforeach
        @endif
        </tbody>
      </table>
      <div><button class="btn btn-sm btn-primary" id="ref-add-new"><i class="fas fa-plus-circle"></i> Add New</button></div>
    </div>
  </div>
</div>
