<h5 class="text-muted header-underlined-wide">VA Matching Profile</h5>
<input type="hidden" name="from_tab" id="from_tab" value="placement-record">
<label class="top-label">Employee Name</label>
<input class="form-control" type="text" disabled name="applicant_name" value="{{$applicant->name}}">
<input type="hidden"  id="applicant_id" name="applicant_id" value="{{$applicant->id}}">
<label class="top-label mt-2">VAM #</label>
<input class="form-control" type="text" name="vam_number" value="">
<input type="hidden" name="csrf_token" value="{{ csrf_token() }}">
<label class="top-label mt-2">VA Level</label><br>
<select name="va_level" class="form-control">
  <option value="1" {{$applicant->va_level == '1' ? 'selected' : ''}}>ISA</option>
  <option value="2" {{$applicant->va_level == '2' ? 'selected' : ''}}>GVA</option>
  <option value="3" {{$applicant->va_level == '3' ? 'selected' : ''}}>EVA</option>
</select>
<br>
<label class="top-label mt-2">Batch #</label>
<input class="form-control" type="text" name="batch_number" disabled value="{{$applicant->trainings[0]->batch ?? ''}}">

<label class="top-label mt-2">Mobile Number</label>
<input class="form-control" type="text" name="mobile_number" value="{{$applicant->mobile_number}}">

<label class="top-label mt-2">Landline Number</label>
<input class="form-control" type="text" name="landline_number" value="{{$applicant->landing_number}}">

<label class="top-label mt-2">Email Address</label>
<input class="form-control" type="text" name="email" value="{{$applicant->vd_email}}">

<label class="top-label mt-2">Status</label><br>
<select name="applicant_status" class="form-control">
  <option value="4" {{$applicant->status == '4' ? 'selected' : ''}}>Scheduled</option>
  <option value="5" {{$applicant->status == '5' ? 'selected' : ''}}>Waiting for Client's Decision</option>
  <option value="6" {{$applicant->status == '6' ? 'selected' : ''}}>Hired</option>
  <option value="7" {{$applicant->status == '7' ? 'selected' : ''}}>Available</option>
  <option value="8" {{$applicant->status == '8' ? 'selected' : ''}}>On Hold</option>
  <option value="9" {{$applicant->status == '9' ? 'selected' : ''}}>Declined</option>
</select><br>

<label class="top-label mt-2">Complete Address</label>
<input class="form-control" type="text" name="address" value="{{$applicant->address}}">

<br><br>
<h5 class="text-muted header-underlined-wide">Client Interview Information</h5>
<label class="top-label mt-2">Client Interviews</label>

<div class="row table-responsive">
  <table class="table table-sm table-bordered table-striped" id="client-interview-tbl">
    <tr>
      <th scope="col"></th>
      <th scope="col">Va Pool</th>
      <th scope="col">Va Status</th>
      <th scope="col">Interview Date & Time</th>
      <th scope="col">Client Interview Status</th>
      <th scope="col">Client Interview Notes</th>
      <th scope="col">Action</th>
    </tr>

    @foreach($clientInterviews as $k=>$c)

    <tr>
      <td>
        <input type="hidden" name="client_interview_id[]" value="{{$c->interviews[0]['id']}}">
        <label class="top-label mt-1">Client Interviews</label><br>
        <select class="form-control client_id" id="client_old" name="client_id[]">
          @foreach($clients as $client)
            <option value="{{$client->id}}" {{ $c->interviews[0]['client_id'] == $client->id ? 'selected' : '' }}>{{$client->name}}</option>
          @endforeach
        </select><br>
        <button class="btn btn-sm btn-success mt-1" data-toggle="modal" data-target="#addClientModal">Add New</button>
        <br>
        <label class="top-label mt-3">Job Description</label>
        <textarea class="form-control mb-3" name="interview_description[]">{{$c->interviews[0]['jobs'][0]['description'] ?? ''}}</textarea>
      </td>
      <td>
        <select class="form-control mt-2" style="width:80px !important;" name="client_va_pool[]" id="client_va_pool">
          <option value="1" {{ $c->interviews[0]['va_pool'] == '1' ? 'selected' : '' }}>ISA</option>
          <option value="2" {{ $c->interviews[0]['va_pool'] == '2' ? 'selected' : '' }}>GVA - TC</option>
          <option value="3" {{ $c->interviews[0]['va_pool'] == '3' ? 'selected' : '' }}>GVA - General</option>
          <option value="4" {{ $c->interviews[0]['va_pool'] == '4' ? 'selected' : '' }}>GVA - Marketing</option>
          <option value="5" {{ $c->interviews[0]['va_pool'] == '5' ? 'selected' : '' }}>EVA</option>
        </select>
      </td>
      <td>
        <select class="form-control mt-2" style="width:80px !important;" name="client_va_status[]">
          <option value="1" {{ $c->interviews[0]['va_status'] == '1' ? 'selected' : '' }}>FT</option>
          <option value="2" {{ $c->interviews[0]['va_status'] == '2' ? 'selected' : '' }}>PT</option>
          <option value="3" {{ $c->interviews[0]['va_status'] == '3' ? 'selected' : '' }}>Timeblock</option>
          <option value="4" {{ $c->interviews[0]['va_status'] == '4' ? 'selected' : '' }}>Trial - 20 hours</option>
          <option value="5" {{ $c->interviews[0]['va_status'] == '5' ? 'selected' : '' }}>Trial - 40 hours</option>
          <option value="6" {{ $c->interviews[0]['va_status'] == '6' ? 'selected' : '' }}>Trial -1 month</option>
          <option value="7" {{ $c->interviews[0]['va_status'] == '7' ? 'selected' : '' }}>Trial - 2 months</option>
        </select>
      </td>
      <td>
        <input data-zdp_readonly_element="false" style="width: 100%;" class="form-control mt-2 dpk" type="text" value="{{date('M d, Y, h:i A', strtotime($c->interviews[0]['date'] )) ?? ''}}" id="client_interview_sched" name="client_interview_sched[]">
      </td>
      <td>
        <select id="client_interview_status" name="client_interview_status[]" class="form-control mt-2" style="width:120px !important">
          <option value="5" {{ $c->interviews[0]['status'] == '5' ? 'selected' : '' }}>Scheduled</option>
          <option value="6" {{ $c->interviews[0]['status'] == '6' ? 'selected' : '' }}>Waiting For Client's Decision</option>
          <option value="7" {{ $c->interviews[0]['status'] == '7' ? 'selected' : '' }}>Hired</option>
          <option value="8" {{ $c->interviews[0]['status'] == '8' ? 'selected' : '' }}>Available</option>
          <option value="9" {{ $c->interviews[0]['status'] == '9' ? 'selected' : '' }}>On Hold</option>
          <option value="10" {{ $c->interviews[0]['status'] == '10' ? 'selected' : '' }}>Declined</option>
        </select>
      </td>
      <td>
        <textarea name="client_interview_note[]" class="form-control mt-2">{{$c->interviews[0]['note']}}</textarea>
      </td>
      <td class="text-center">
        <button class="btn btn-sm btn-success mt-2" id="client_send_email" rel="{{$applicant->id}}">Send email</button>
        <div class="position-relative" style="margin-top: 140px;" title="delete record" id="del-placement" rel="{{$c->interviews[0]['id']}}"><i class="fas fa-trash-alt"></i> Delete</div>
      </td>
    </tr>
    @endforeach

  </table>
</div>
<div><button class="btn btn-sm btn-primary" id="client-add-new"><i class="fas fa-plus-circle"></i> Add New</button></div>

<br>
<br>

<!-- Modal -->
<div class="modal fade" id="addClientModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label class="top-label mt-1">Client's Name</label>
        <input type="text" class="form-control" name="client_name" id="client_name" required>
        <label class="top-label mt-1">State</label><br>
        <select class="form-control" name="state" id="client_state">
        	<option value="AL">Alabama</option>
        	<option value="AK">Alaska</option>
        	<option value="AZ">Arizona</option>
        	<option value="AR">Arkansas</option>
        	<option value="CA">California</option>
        	<option value="CO">Colorado</option>
        	<option value="CT">Connecticut</option>
        	<option value="DE">Delaware</option>
        	<option value="DC">District Of Columbia</option>
        	<option value="FL">Florida</option>
        	<option value="GA">Georgia</option>
        	<option value="HI">Hawaii</option>
        	<option value="ID">Idaho</option>
        	<option value="IL">Illinois</option>
        	<option value="IN">Indiana</option>
        	<option value="IA">Iowa</option>
        	<option value="KS">Kansas</option>
        	<option value="KY">Kentucky</option>
        	<option value="LA">Louisiana</option>
        	<option value="ME">Maine</option>
        	<option value="MD">Maryland</option>
        	<option value="MA">Massachusetts</option>
        	<option value="MI">Michigan</option>
        	<option value="MN">Minnesota</option>
        	<option value="MS">Mississippi</option>
        	<option value="MO">Missouri</option>
        	<option value="MT">Montana</option>
        	<option value="NE">Nebraska</option>
        	<option value="NV">Nevada</option>
        	<option value="NH">New Hampshire</option>
        	<option value="NJ">New Jersey</option>
        	<option value="NM">New Mexico</option>
        	<option value="NY">New York</option>
        	<option value="NC">North Carolina</option>
        	<option value="ND">North Dakota</option>
        	<option value="OH">Ohio</option>
        	<option value="OK">Oklahoma</option>
        	<option value="OR">Oregon</option>
        	<option value="PA">Pennsylvania</option>
        	<option value="RI">Rhode Island</option>
        	<option value="SC">South Carolina</option>
        	<option value="SD">South Dakota</option>
        	<option value="TN">Tennessee</option>
        	<option value="TX">Texas</option>
        	<option value="UT">Utah</option>
        	<option value="VT">Vermont</option>
        	<option value="VA">Virginia</option>
        	<option value="WA">Washington</option>
        	<option value="WV">West Virginia</option>
        	<option value="WI">Wisconsin</option>
        	<option value="WY">Wyoming</option>
        </select><br>
        <label class="top-label mt-1">Timezone</label><br>
        <select class="form-control" name="timezone" id="client_tz">
          <option value="PST">Pacific Standard Time Zone</option>
          <option value="MST">Mountain Standard Time Zone</option>
          <option value="CST">Central Standard Time Zone</option>
          <option value="EST">Eastern Standard Time Zone</option>
          <option value="HAWAII">Hawaii</option>
        </select>
        <br><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-add-client">Save changes</button>
      </div>
    </div>
  </div>
</div
