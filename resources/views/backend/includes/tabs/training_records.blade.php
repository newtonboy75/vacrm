<h5 class="text-muted header-underlined-wide">Training Records</h5>
<br>
<div class="text-red font-weight-bold">TRAINING IDENTIFICATION INFO</div>
<hr>
<label class="top-label">ID</label>
<input type="hidden" name="from_tab" id="from_tab" value="training-record">
<input class="form-control" type="text" disabled name="applicantid" value="{{$applicant->id}}">
<input type="hidden" id="applicant_id" name="applicant_id" value="{{$applicant->id}}">
<br>

<label class="top-label">VA Level</label>
<select class="form-control" name="va_level" id="va_level">
  @if(isset($applicant->trainings[0]['program']))
  <option value="1" {{ strtolower($applicant->last_training->program) == 'isa' ? 'selected' : '' }}>ISA</option>
  <option value="2" {{ strtolower($applicant->last_training->program) == 'gva' ? 'selected' : '' }}>GVA</option>
  <option value="3" {{ strtolower($applicant->last_training->program) == 'eva' ? 'selected' : '' }}>EVA</option>
  <option value="4" {{ strtolower($applicant->last_training->program) == 'tc' ? 'selected' : '' }}>TC</option>
  @endif
</select>
<br>
<label class="top-label">Batch #</label>
@php
$mybatch = '';
if(isset($applicant->trainings[0]['batch'])){
  $mybatch = preg_replace('/[^a-zA-Z]/', '', $applicant->last_training->batch);
}

@endphp
<select class="form-control" name="batch" id="batch_id">
@php
$batches = \App\PreTrainingBatch::where('batch_number', 'REGEXP', $mybatch)->get();
foreach($batches as $batch){
  if($batch){
@endphp
<option rel="{{$batch->start_date}}|{{$batch->end_date}}" value="{{ $batch->id }}" {{ $applicant->last_training->batch == $batch->batch_number ? 'selected' : '' }}>{{ $batch->batch_number }}</option>
@php
  }
}
@endphp
</select>

<br>
<label class="top-label">Trainer</label>
<select name="trainer" class="form-control">
@foreach($alltrainers as $trainer)

@if($trainer->id >= 1)
@if(isset($applicant->last_training->trainer_id))
<option value="{{$trainer->id}}" {{ $trainer->id == $applicant->last_training->trainer_id ? 'selected' : '' }}>{{$trainer->first_name}} {{$trainer->last_name}}</option>
@endif
@endif
@endforeach

@if($trainer->id >= 1)
@if(isset($applicant->last_training->trainer_id))
<option value="3285" {{ $applicant->last_training->trainer_id ==  '3285' ? 'selected' : '' }}>Kylix Filamor IV & Michael Angelo Pascual</option>
@endif
@endif
</select>

<br>
<label class="top-label">Training Start Date</label>
<input id="tr-start-date" class="form-control" type="text" disabled name="trstart_date" value="{{ $applicant->last_training->start_date ?? '' }}">
<input id="trstart-date" class="form-control" type="hidden"  name="start_date" value="{{ $applicant->last_training->start_date ?? '' }}">

<br>
<label class="top-label">Training End Date</label>
<input id="tr-end-date" class="form-control" type="text" disabled name="trend_date" value="{{ $applicant->last_training->end_date ?? '' }}">
<input id="trend-date" class="form-control" type="hidden"  name="end_date" value="{{ $applicant->last_training->end_date ?? '' }}">
<!--
<input class="form-control" type="text" disabled  name="batch" value="{{$applicant->last_training->batch ?? ''}}">
-->
<p>&nbsp;</p><br>
<div class="text-red font-weight-bold">TRAINEE INFORMATION</div>
<hr>
<label class="top-label">Trainee Name</label>
<input class="form-control" type="text" disabled name="name" value="{{$applicant->name}}">
<br>
<label class="top-label">Mobile Number</label>
<input class="form-control" type="text" disabled name="mobile_number" value="{{$applicant->mobile_number}}">
<br>
<label class="top-label">Email</label>
<input class="form-control" type="text" disabled name="email" value="{{$applicant->email}}">
<br>
<label class="top-label">Complete Address</label>
<input class="form-control" type="text" disabled name="address" value="{{$applicant->address}}">

<br>
<label class="top-label">Recruiter</label>
<input class="form-control" type="text" disabled name="recruiter" value="{{$applicant->recruiter_name->first_name ?? ''}} {{$applicant->recruiter_name->last_name ?? ''}}">

<br>
<label class="top-label">How did your hear about facebook?</label>
<input class="form-control" type="text" disabled name="come_from" value="{{$applicant->come_from ?? ''}}">

<br>
<label class="top-label">Emergency Contact Person</label>
<input type="text" name="contact_person" class="form-control" value="{{ $applicant->employee_emergency_contact->contact_person_name ?? '' }}">

<br>
<label class="top-label">Relation to Emergency Contact Person</label>
<input type="text" name="contact_person_relation" class="form-control" value="{{ $applicant->employee_emergency_contact->contact_person_relation ?? '' }}">

<br>
<label class="top-label">Contact Number of Emergency Contact Person</label>
<input type="number" name="contact_person_number" class="form-control" value="{{ $applicant->employee_emergency_contact->contact_person_number ?? '' }}">


<p>&nbsp;</p><br>
<div class="text-red font-weight-bold">TRAINEE EXPERIENCE</div>
<hr>
<label class="top-label">With VA Experience?</label>
<select class="form-control" name="va_experience">
  <option value="0" disabled selected>Please choose</option>

  <option value="1" {{ $applicant->last_training->va_experience == '1' ? 'selected' : '' }}>Shifter (No Experience)</option>
  <option value="2" {{ $applicant->last_training->va_experience == '2' ? 'selected' : '' }}>Adapter I (with 6 months - 11 months experience)</option>
  <option value="3" {{ $applicant->last_training->va_experience == '3' ? 'selected' : '' }}>Adapter II (with 1 year - 1 year and 5 months experience)</option>
  <option value="4" {{ $applicant->last_training->va_experience == '4' ? 'selected' : '' }}>Adapter III (with 1 year and 6 months to above experience)</option>
  <option value="5" {{ $applicant->last_training->va_experience == '5' ? 'selected' : '' }}>TC I (with 6 months - 11 months experience)</option>
  <option value="6" {{ $applicant->last_training->va_experience == '6' ? 'selected' : '' }}>TC II (with 1 year to 2 years and 11 months experience)</option>
  <option value="7" {{ $applicant->last_training->va_experience == '7' ? 'selected' : '' }}>TC III (with 3 years and above experience)</option>

</select>
<br>
<label class="top-label">Last Work Experience</label>
<select class="form-control" name="last_work_experience">
  <option value="0" disabled selected>Please choose</option>

  <option value="1" {{ $applicant->last_training->last_work_experience == '1' ? 'selected' : '' }}>BPO - CSR only</option>
  <option value="2" {{ $applicant->last_training->last_work_experience == '2' ? 'selected' : '' }}>BPO - ATL (CSR only)</option>
  <option value="3" {{ $applicant->last_training->last_work_experience == '3' ? 'selected' : '' }}>BPO - TL (CSR only)</option>
  <option value="4" {{ $applicant->last_training->last_work_experience == '4' ? 'selected' : '' }}>BPO - CSR (with Upselling)</option>
  <option value="5" {{ $applicant->last_training->last_work_experience == '5' ? 'selected' : '' }}>BPO - CSR ATL (with Upselling)</option>
  <option value="6" {{ $applicant->last_training->last_work_experience == '6' ? 'selected' : '' }}>BPO - CSR TL (with Upselling)</option>
  <option value="7" {{ $applicant->last_training->last_work_experience == '7' ? 'selected' : '' }}>BPO - CSR (Sales)</option>
  <option value="8" {{ $applicant->last_training->last_work_experience == '8' ? 'selected' : '' }}>BPO - CSR ATL (Sales)</option>
  <option value="9" {{ $applicant->last_training->last_work_experience == '9' ? 'selected' : '' }}>BPO - CSR TL (Sales)</option>
  <option value="10" {{ $applicant->last_training->last_work_experience == '10' ? 'selected' : '' }}>BPO - CSR (Non-Voice)</option>
  <option value="11" {{ $applicant->last_training->last_work_experience == '11' ? 'selected' : '' }}>BPO - CSR (ATL - Non-Voice)</option>
  <option value="12" {{ $applicant->last_training->last_work_experience == '12' ? 'selected' : '' }}>BPO - CSR (TL - Non-Voice)</option>
  <option value="13" {{ $applicant->last_training->last_work_experience == '13' ? 'selected' : '' }}>BPO - OM (CSR only)</option>
  <option value="14" {{ $applicant->last_training->last_work_experience == '14' ? 'selected' : '' }}>BPO - OM (with Upselling)</option>
  <option value="15" {{ $applicant->last_training->last_work_experience == '15' ? 'selected' : '' }}>BPO - OM (Sales)</option>
  <option value="16" {{ $applicant->last_training->last_work_experience == '16' ? 'selected' : '' }}>BPO - OM (Non-Voice)</option>
  <option value="17" {{ $applicant->last_training->last_work_experience == '17' ? 'selected' : '' }}>BPO - AOM (CSR only)</option>
  <option value="18" {{ $applicant->last_training->last_work_experience == '18' ? 'selected' : '' }}>BPO - AOM (with Upselling)</option>
  <option value="19" {{ $applicant->last_training->last_work_experience == '19' ? 'selected' : '' }}>BPO - AOM (Sales)</option>
  <option value="20" {{ $applicant->last_training->last_work_experience == '20' ? 'selected' : '' }}>BPO - AOM (Non-Voice)</option>
  <option value="21" {{ $applicant->last_training->last_work_experience == '21' ? 'selected' : '' }}>BPO - SME (CSR only)</option>
  <option value="22" {{ $applicant->last_training->last_work_experience == '22' ? 'selected' : '' }}>BPO - SME (CSR with Upselling)</option>
  <option value="23" {{ $applicant->last_training->last_work_experience == '23' ? 'selected' : '' }}>BPO - SME (Sales)</option>
  <option value="24" {{ $applicant->last_training->last_work_experience == '24' ? 'selected' : '' }}>BPO - SME (Non-Voice)</option>
  <option value="25" {{ $applicant->last_training->last_work_experience == '25' ? 'selected' : '' }}>BPO - TSR only</option>
  <option value="26" {{ $applicant->last_training->last_work_experience == '26' ? 'selected' : '' }}>BPO - ATL (TSR only)</option>
  <option value="27" {{ $applicant->last_training->last_work_experience == '27' ? 'selected' : '' }}>BPO - TL (TSR only)</option>
  <option value="28" {{ $applicant->last_training->last_work_experience == '28' ? 'selected' : '' }}>BPO - OM (TSR only)</option>
  <option value="29" {{ $applicant->last_training->last_work_experience == '29' ? 'selected' : '' }}>BPO - AOM (TSR only)</option>
  <option value="30" {{ $applicant->last_training->last_work_experience == '30' ? 'selected' : '' }}>BPO - TSR (with Upselling)</option>
  <option value="31" {{ $applicant->last_training->last_work_experience == '31' ? 'selected' : '' }}>BPO - ATL (TSR with Upselling)</option>
  <option value="32" {{ $applicant->last_training->last_work_experience == '32' ? 'selected' : '' }}>BPO - TL (TSR with Upselling)</option>
  <option value="33" {{ $applicant->last_training->last_work_experience == '33' ? 'selected' : '' }}>BPO - OM (TSR with Upselling)</option>
  <option value="34" {{ $applicant->last_training->last_work_experience == '34' ? 'selected' : '' }}>BPO - AOM (TSR with Upselling)</option>
  <option value="35" {{ $applicant->last_training->last_work_experience == '35' ? 'selected' : '' }}>BPO - SME (TSR with Upselling)</option>
  <option value="36" {{ $applicant->last_training->last_work_experience == '36' ? 'selected' : '' }}>BPO - IT</option>
  <option value="37" {{ $applicant->last_training->last_work_experience == '37' ? 'selected' : '' }}>BPO - CSM</option>
  <option value="38" {{ $applicant->last_training->last_work_experience == '38' ? 'selected' : '' }}>BPO - Recruiter</option>
  <option value="39" {{ $applicant->last_training->last_work_experience == '39' ? 'selected' : '' }}>BPO - RM</option>
  <option value="40" {{ $applicant->last_training->last_work_experience == '40' ? 'selected' : '' }}>BPO - TM</option>
  <option value="41" {{ $applicant->last_training->last_work_experience == '41' ? 'selected' : '' }}>BPO - Trainer</option>
  <option value="42" {{ $applicant->last_training->last_work_experience == '42' ? 'selected' : '' }}>BPO - Asst. Trainer/Interim Trainer</option>
  <option value="43" {{ $applicant->last_training->last_work_experience == '43' ? 'selected' : '' }}>BPO - Coach (Sales)</option>
  <option value="44" {{ $applicant->last_training->last_work_experience == '44' ? 'selected' : '' }}>BPO - Coach (Comms)</option>
  <option value="45" {{ $applicant->last_training->last_work_experience == '45' ? 'selected' : '' }}>VA - Telemarketer</option>
  <option value="46" {{ $applicant->last_training->last_work_experience == '46' ? 'selected' : '' }}>VA - ISA (Non-RE)</option>
  <option value="47" {{ $applicant->last_training->last_work_experience == '47' ? 'selected' : '' }}>VA - GVA (Non-RE)</option>
  <option value="48" {{ $applicant->last_training->last_work_experience == '48' ? 'selected' : '' }}>VA - ISA (RE)</option>
  <option value="49" {{ $applicant->last_training->last_work_experience == '49' ? 'selected' : '' }}>VA - GVA (RE)</option>
  <option value="50" {{ $applicant->last_training->last_work_experience == '50' ? 'selected' : '' }}>VA - TL (Telemarketer)</option>
  <option value="51" {{ $applicant->last_training->last_work_experience == '51' ? 'selected' : '' }}>VA - TL (ISA Non-RE)</option>
  <option value="52" {{ $applicant->last_training->last_work_experience == '52' ? 'selected' : '' }}>VA - TL (GVA Non-RE)</option>
  <option value="53" {{ $applicant->last_training->last_work_experience == '53' ? 'selected' : '' }}>VA - TL (ISA)</option>
  <option value="54" {{ $applicant->last_training->last_work_experience == '54' ? 'selected' : '' }}>VA - TL (GVA)</option>
  <option value="55" {{ $applicant->last_training->last_work_experience == '55' ? 'selected' : '' }}>VA - OM</option>
  <option value="56" {{ $applicant->last_training->last_work_experience == '56' ? 'selected' : '' }}>VA - CSM</option>
  <option value="57" {{ $applicant->last_training->last_work_experience == '57' ? 'selected' : '' }}>VA - TM</option>
  <option value="58" {{ $applicant->last_training->last_work_experience == '58' ? 'selected' : '' }}>VA - Trainer</option>
  <option value="59" {{ $applicant->last_training->last_work_experience == '59' ? 'selected' : '' }}>VA - RM</option>
  <option value="60" {{ $applicant->last_training->last_work_experience == '60' ? 'selected' : '' }}>VA - Recruiter</option>
  <option value="61" {{ $applicant->last_training->last_work_experience == '61' ? 'selected' : '' }}>VA - AOM</option>
  <option value="62" {{ $applicant->last_training->last_work_experience == '62' ? 'selected' : '' }}>VA - HR</option>
  <option value="63" {{ $applicant->last_training->last_work_experience == '63' ? 'selected' : '' }}>VA - ESL</option>
  <option value="64" {{ $applicant->last_training->last_work_experience == '64' ? 'selected' : '' }}>VA - CSR</option>
  <option value="65" {{ $applicant->last_training->last_work_experience == '65' ? 'selected' : '' }}>VA - TSR</option>

</select>
<br>

<label class="top-label">Work Experience</label>
<textarea class="form-control" name="work_experience_notes">{{$applicant->last_training->work_experience_notes ?? ''}}</textarea>

<p>&nbsp;</p><br>
<div class="text-red font-weight-bold">TRAINING GLIDE PATH</div>
<hr>
<label class="top-label">Status</label>
<select class="form-control" name="training_status">
  <option value="0" selected disabled>Please set status</option>

  <option value="1" {{ $applicant->last_training->training_status == '1' ? 'selected' : '' }}>Present</option>
  <option value="3" {{ $applicant->last_training->training_status == '3' ? 'selected' : '' }}>NCNS</option>
  <option value="20" {{ $applicant->last_training->training_status == '20' ? 'selected' : '' }}>Late</option>
  <option value="21" {{ $applicant->last_training->training_status == '21' ? 'selected' : '' }}>Transferred to ISA</option>
  <option value="22" {{ $applicant->last_training->training_status == '22' ? 'selected' : '' }}>Transferred to GVA</option>
  <option value="37" {{ $applicant->last_training->training_status == '37' ? 'selected' : '' }}>Transferred to EVA</option>
  <option value="36" {{ $applicant->last_training->training_status == '36' ? 'selected' : '' }}>Transferred to TC</option>
  <option value="38" {{ $applicant->last_training->training_status == '38' ? 'selected' : '' }}>Pass</option>
  <option value="39" {{ $applicant->last_training->training_status == '39' ? 'selected' : '' }}>Fail</option>
  <option value="40" {{ $applicant->last_training->training_status == '40' ? 'selected' : '' }}>Absent - Invalid</option>
  <option value="2" {{ $applicant->last_training->training_status == '2' ? 'selected' : '' }}>Absent - Valid</option>
  <option value="4" {{ $applicant->last_training->training_status == '4' ? 'selected' : '' }}>Transferred to next batch - Family Emergency</option>
  <option value="5" {{ $applicant->last_training->training_status == '5' ? 'selected' : '' }}>Transferred to next batch - System Requirements</option>
  <option value="6" {{ $applicant->last_training->training_status == '6' ? 'selected' : '' }}>Transferred to next batch - Force Majeure</option>
  <option value="7" {{ $applicant->last_training->training_status == '7' ? 'selected' : '' }}>Transferred to next batch - Medical</option>
  <option value="8" {{ $applicant->last_training->training_status == '8' ? 'selected' : '' }}>Transferred to next batch - Did not see email</option>
  <option value="9" {{ $applicant->last_training->training_status == '9' ? 'selected' : '' }}>Transferred to next batch - Personal</option>
  <option value="10" {{ $applicant->last_training->training_status == '10' ? 'selected' : '' }}>Transferred to next batch - Employed (Rendering Resignation)</option>

  <option value="15" {{ $applicant->last_training->training_status == '15' ? 'selected' : '' }}>Resigned - Personal</option>
  <option value="16" {{ $applicant->last_training->training_status == '16' ? 'selected' : '' }}>Resigned - Business Structure</option>
  <option value="17" {{ $applicant->last_training->training_status == '17' ? 'selected' : '' }}>Resigned - Health Issues</option>
  <option value="18" {{ $applicant->last_training->training_status == '18' ? 'selected' : '' }}>Resigned - Family Emergency</option>
  <option value="19" {{ $applicant->last_training->training_status == '19' ? 'selected' : '' }}>Resigned - System Requirements</option>
  <option value="41" {{ $applicant->last_training->training_status == '41' ? 'selected' : '' }}>Resigned - Employment Status</option>

  <option value="11" {{ $applicant->last_training->training_status == '11' ? 'selected' : '' }}>Terminated - Attitude/Behavior</option>
  <option value="12" {{ $applicant->last_training->training_status == '12' ? 'selected' : '' }}>Terminated - Attendance</option>
  <option value="13" {{ $applicant->last_training->training_status == '13' ? 'selected' : '' }}>Terminated - Employment Status</option>
  <option value="42" {{ $applicant->last_training->training_status == '42' ? 'selected' : '' }}>Terminated - Policy Violation</option>
  <option value="14" {{ $applicant->last_training->training_status == '14' ? 'selected' : '' }}>Terminated - Performance</option>
  <option value="23" {{ $applicant->last_training->training_status == '23' ? 'selected' : '' }}>Re-Application - System Requirements</option>
  <option value="24" {{ $applicant->last_training->training_status == '24' ? 'selected' : '' }}>Re-Application - Health Issues</option>
  <option value="25" {{ $applicant->last_training->training_status == '25' ? 'selected' : '' }}>Re-Application - Currently Employed</option>
  <option value="26" {{ $applicant->last_training->training_status == '26' ? 'selected' : '' }}>Re-Application - Family Matters</option>

  <option value="43" {{ $applicant->last_training->training_status == '43' ? 'selected' : '' }}>MIA - After NHO</option>
  <option value="44" {{ $applicant->last_training->training_status == '44' ? 'selected' : '' }}>MIA - After 1st Break</option>
  <option value="45" {{ $applicant->last_training->training_status == '45' ? 'selected' : '' }}>MIA - After Lunch</option>
  <option value="46" {{ $applicant->last_training->training_status == '46' ? 'selected' : '' }}>MIA - After 2nd Break</option>
  <option value="47" {{ $applicant->last_training->training_status == '47' ? 'selected' : '' }}>Early Out - Personal</option>
  <option value="48" {{ $applicant->last_training->training_status == '48' ? 'selected' : '' }}>Early Out - Medical</option>

  <option value="49" {{ $applicant->last_training->training_status == '49' ? 'selected' : '' }}>On Hold - Family Emergency</option>
  <option value="50" {{ $applicant->last_training->training_status == '50' ? 'selected' : '' }}>On Hold - System Requirements</option>
  <option value="51" {{ $applicant->last_training->training_status == '51' ? 'selected' : '' }}>On Hold - Force Majeure</option>
  <option value="52" {{ $applicant->last_training->training_status == '52' ? 'selected' : '' }}>On Hold - Medical</option>
  <option value="53" {{ $applicant->last_training->training_status == '53' ? 'selected' : '' }}>On Hold - Did not see email</option>
  <option value="54" {{ $applicant->last_training->training_status == '54' ? 'selected' : '' }}>On Hold - Personal</option>
  <option value="55" {{ $applicant->last_training->training_status == '55' ? 'selected' : '' }}>On Hold - Employed (Rendering Resignation)</option>

  <option value="31" {{ $applicant->last_training->training_status == '31' ? 'selected' : '' }}>Blacklisted</option>

</select>
<br>
<label class="top-label">Red Flag Notes</label>
<textarea class="form-control" name="red_flag_notes">{{$applicant->red_flag_notes ?? ''}}</textarea>
<br>
<label class="top-label">Trainer's Notes</label>
<textarea class="form-control" id="comments" name="notes">{{$applicant->last_training->notes ?? ''}}</textarea>

<p>&nbsp;</p><br>
<div class="text-red font-weight-bold">LOGIN CREDENTIALS</div>
<hr>
<label class="top-label">VD Skype</label>
<input class="form-control" type="text"  name="vd_skype" value="{{$applicant->vd_skype ?? '' }}">

<br>
<label class="top-label">Password</label>
<input class="form-control" type="text"  name="vd_skype_pass" value="{{$applicant->vd_skype_pass ?? ''}}">

<br>
<label class="top-label">VD Email</label>
<input class="form-control" type="text"  name="vd_email" value="{{$applicant->vd_email ?? ''}}">

<br>
<label class="top-label">Password</label>
<input class="form-control" type="text" name="vd_email_pass" value="{{$applicant->vd_email_pass ?? ''}}">

<br>
<label class="top-label">VD Academy</label>
<input class="form-control" type="text" name="vd_academy" value="{{$applicant->vd_academy ?? ''}}">

<br>
<label class="top-label">Password</label>
<input class="form-control" type="text"  name="vd_academy_pass" value="{{$applicant->vd_academy_pass ?? '' }}">

<p>&nbsp;</p>
<!-- Modal -->
<div class="modal fade" id="popupTrainingReschedModal" tabindex="-1" role="dialog" aria-labelledby="popupTrainingReschedModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:700px;">
      <div class="modal-header">
        <h5 class="modal-title" id="popupTrainingReschedModal">Send Training Re-schedule</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">To</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="to" id="to" value="{{ $applicant->email ?? ''}}">
        </div>

        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Subject</span>
          </div>
          <input type="text" class="form-control" placeholder="Subject" aria-label="Subject" aria-describedby="basic-addon1" name="subject" id="subject" value="RE: Training Re-schedule">
        </div>

        <div class="form-group">
          <label for="address" class="f-label">Body</label>
          <textarea name="email-body" id="email_body_resend_resched" class="form-control" required rows="10">
            <p style="text-align:left"><strong>Dear {{$applicant->name ?? ''}},</strong></p>

            <p style="text-align:left">This is to inform you that your {{ $applicant->trainings[0]['program'] ?? '' }} Training schedule/s have been moved. Please do take note of the following details regarding your new training schedule:</p>
            <p><strong>Program:</strong> <span id="tr-program">{{ $applicant->trainings[0]['program'] ?? '' }}</span><br>
            <strong>Start Date:</strong> <span id="tr-start">{{ $applicant->trainings[0]['start_date'] ?? '' }}</span><br>
            <strong>End Date:</strong> <span id="tr-end">{{ $applicant->trainings[0]['end_date'] ?? '' }}</span>
            </p>

            <p>Should you have queries, do not hesitate to contact us.</p>

            <p><span style="color:#4f7a28"><span style="font-size:small"><strong>Virtudesk Requirements Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br /><span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:virtudesk.referenceteam@gmail.com"><span style="color:#1155cc"><span style="font-size:small"><u>virtudesk.requirements@gmail.com</u></span></span></a><br /><strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>

          </textarea>
          <script>
          CKEDITOR.replace( 'email_body_resend_resched' );
          //var editor = CKEDITOR.replace( 'email_body_resend_resched', {
              //allowedContent: '*[id]'
          //} );
          </script>
        </div>
      </div>
      <div class="modal-footer">
        <span class="wait-status text-warning"></span>
        <input type="hidden" name="email_type" value="training-resched" id="email_type">
        <input type="hidden" name="user_id" value="{{$applicant->id ?? ''}}" id="user_id">
        <input type="hidden" name="from" value="virtudesk.requirements@gmail.com|Virtudesk Requirements Team" id="from_admin">
        <input type="hidden" name="subject" value="RE: Training Re-schedule" id="subject">
        <button id="email_dismiss" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="training_resched" rel="redo">Send Email</button>
      </div>
    </div>
  </div>
</div>
