
<label class="top-label">Experience</label><br>
<div id="source-experience" style="display: {{ count($experience) >= 1 ? 'block' : 'none' }}">
  <input type="hidden" name="applicant_id" id="from_tab" value="{{$applicant->id}}">
<table class="table table-sm table-bordered">
  <thead>
    <tr>
      <th scope="col">Company Name</th>
      <th scope="col">Job Position</th>
      <th scope="col">Start Date</th>
      <th scope="col">End Date</th>
      <th scope="col">Job Responsibilities</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody id="tbl-exp">
    @foreach($experience as $exp)
      @if(isset($exp['experience_record'][0]))
            <tr class="partial-experience">

              <input type="hidden" name="exp_id[]" value="{{$exp['experience_record'][0]['id']}}">
              <td class="mx-auto"><input class="form-control" type="text" name="exp_company_name[]" value="{{$exp['experience_record'][0]['company_name']}}"></td>
              <td><input class="form-control" type="text" name="exp_job_pos[]" value="{{$exp['experience_record'][0]['position']}}"></td>
              <td><div class="input-group input-group"><input type="text" class="form-control" id="dateyear" name="exp_date_start[]" value="{{$exp['experience_record'][0]['start_date']}}"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div></div></td>
              <td><div class="input-group input-group"><input type="text" class="form-control" id="dateyear" name="exp_date_end[]" value="{{$exp['experience_record'][0]['end_date']}}"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div></div></td>
              <td><textarea class="form-control" name="exp_job_resp[]">{{$exp['experience_record'][0]['job_responsibilities']}}</textarea></td>
              <td scope="row" class="text-center"><div id="del-exp" rel="{{$exp['experience_record'][0]['id']}}"><i class="fas fa-trash-alt"></i></div></td></tr>
      @endif
    @endforeach
  </tbody>
</table>
</div>
