@php
$fintv = 0;
$lintv = 0;
@endphp
@if(count($interviews) >= 1)
@foreach($interviews as $intvs)
  @if($intvs['interview_record'][0]['type'] == 'initial')
    @php $fintv += 1; @endphp
  @elseif($intvs['interview_record'][0]['type'] == 'final')
    @php $lintv += 1; @endphp
  @endif
@endforeach
<div class="row">
  <div class="col">

    <div class="field-box">
      <h5 class="text-muted header-underlined-wide">Initial Interview</h5>
      <label class="top-label">Initial Interviews</label>
      <input type="hidden" name="csrf_token" value="{{ csrf_token() }}">
      <input type="hidden" name="from_tab" id="from_tab" value="talent-acquisition-record">
      <input type="hidden" name="applicant_id" id="from_tab" value="{{$applicant->id}}">
      <div style="display: {{ $fintv >= 1 ? 'block' : 'none'}};" id="i_interview">
        <table class="table table-bordered">
        <thead>
          <tr>
            <th>Status</th>
            <th>Note</th>
            <th>Date</th>
            <th>Rescheduled Reason</th>
            <th>Recruiter</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody id="tbl-interview">
          @foreach($interviews as $intv)
            @if($intv['interview_record'][0]['type'] == 'initial')
              <tr>
                <td>
                  <input type="hidden" name="talent_application_id_first[]" value="{{$intv['interview_record'][0]['id']}}">
                  <input type="hidden" name="talent_application_type_first[]" value="initial" required>
                <select class="form-control" name="talent_application_status_first[]" required>

                  <option value="0" selected>Please select</option>
                  <option value="1" {{$intv['interview_record'][0]['status'] == '1' ? 'selected' : ''}}>Passed Initial Interview</option>
                  <option value="2" {{$intv['interview_record'][0]['status'] == '2' ? 'selected' : ''}}>Failed Initial Interview</option>
                  <option value="3" {{$intv['interview_record'][0]['status'] == '3' ? 'selected' : ''}}>No Show</option>
                  <option value="5" {{$intv['interview_record'][0]['status'] == '5' ? 'selected' : ''}}>Pending Interview</option>
                  <option value="4" {{$intv['interview_record'][0]['status'] == '4' ? 'selected' : ''}}>Blacklisted</option>
                  <option value="6" {{$intv['interview_record'][0]['status'] == '6' ? 'selected' : ''}}>Re-scheduled | Health Issues</option>
                  <option value="7" {{$intv['interview_record'][0]['status'] == '7' ? 'selected' : ''}}>Re-scheduled | System Requirements</option>
                  <option value="8" {{$intv['interview_record'][0]['status'] == '8' ? 'selected' : ''}}>Re-scheduled | Personal Matters</option>
                  <option value="9" {{$intv['interview_record'][0]['status'] == '9' ? 'selected' : ''}}>Re-scheduled | Employment</option>
                  <option value="10" {{$intv['interview_record'][0]['status'] == '10' ? 'selected' : ''}}>Re-scheduled | Family Emergency</option>
                  <option value="11" {{$intv['interview_record'][0]['status'] == '11' ? 'selected' : ''}}>On Hold | System Requirements</option>
                  <option value="12" {{$intv['interview_record'][0]['status'] == '12' ? 'selected' : ''}}>On Hold | Undecided</option>
                  <option value="13" {{$intv['interview_record'][0]['status'] == '13' ? 'selected' : ''}}>Re-Application | System Requirements</option>
                  <option value="14" {{$intv['interview_record'][0]['status'] == '14' ? 'selected' : ''}}>Re-Application | Undecided</option>
                  <option value="15" {{$intv['interview_record'][0]['status'] == '15' ? 'selected' : ''}}>Re-Application | Health Issues</option>
                  <option value="16" {{$intv['interview_record'][0]['status'] == '16' ? 'selected' : ''}}>Re-Application | Re-schedule issues</option>
                  <option value="17" {{$intv['interview_record'][0]['status'] == '17' ? 'selected' : ''}}>Re-Application | Employed</option>
                  <option value="18" {{$intv['interview_record'][0]['status'] == '18' ? 'selected' : ''}}>Re-Application | Personal Matters</option>
                  <option value="19" {{$intv['interview_record'][0]['status'] == '19' ? 'selected' : ''}}>Re-Application | Emergency</option>
                  <option value="20" {{$intv['interview_record'][0]['status'] == '20' ? 'selected' : ''}}>Re-Application | Business Structure</option>
                  <option value="21" {{$intv['interview_record'][0]['status'] == '21' ? 'selected' : ''}}>Withdrawn | System Requirements</option>
                  <option value="22" {{$intv['interview_record'][0]['status'] == '22' ? 'selected' : ''}}>Withdrawn | Undecided</option>
                  <option value="23" {{$intv['interview_record'][0]['status'] == '23' ? 'selected' : ''}}>Withdrawn| Health Issues</option>
                  <option value="24" {{$intv['interview_record'][0]['status'] == '24' ? 'selected' : ''}}>Withdrawn | Re-schedule issues</option>
                  <option value="25" {{$intv['interview_record'][0]['status'] == '25' ? 'selected' : ''}}>Withdrawn | Employed</option>
                  <option value="26" {{$intv['interview_record'][0]['status'] == '26' ? 'selected' : ''}}>Withdrawn| Personal Matters</option>
                  <option value="27" {{$intv['interview_record'][0]['status'] == '27' ? 'selected' : ''}}>Withdrawn | Emergency</option>
                  <option value="28" {{$intv['interview_record'][0]['status'] == '28' ? 'selected' : ''}}>Withdrawn | Business Structure</option>
                  <option value="29" {{$intv['interview_record'][0]['status'] == '29' ? 'selected' : ''}}>Pending Exam</option>
                  <option value="30" {{$intv['interview_record'][0]['status'] == '30' ? 'selected' : ''}}>Pending Systems Check</option>
                  <option value="31" {{$intv['interview_record'][0]['status'] == '31' ? 'selected' : ''}}>Failed Systems Check</option>
                  <option value="32" {{$intv['interview_record'][0]['status'] == '32' ? 'selected' : ''}}>Failed Exam</option>
                  <option value="33" {{$intv['interview_record'][0]['status'] == '33' ? 'selected' : ''}}>Rescheduled</option>

                </select></td>
                <td><textarea name="talent_note_first[]" class="form-control" value="{{$intv['interview_record'][0]['note']}}" required>{{$intv['interview_record'][0]['note']}}</textarea></td>
                <td><input data-zdp_readonly_element="false" type="text" class="form-control dpk" name="talent_exp_date_end_first[]" value="{{date('M d, Y, h:i:s A', strtotime($intv['interview_record'][0]['date']))}}" required style="width: 220px;">
                </td>
                <td><textarea class="form-control" required name="talent_rescheduled_first[]">{{$intv['interview_record'][0]['re_schedule_reason']}}</textarea></td>
                <td>
                  <select class="form-control" id="tl_recruiter" name="tl_recruiter[]">
                    <option value="" disabled selected>Select Recruiter</option>
                    @foreach($allrecruiters as $recruiter)
                    @php
                    $recruiter_id = isset($intv['interview_record'][0]['recruiter_id']) ? $intv['interview_record'][0]['recruiter_id'] : $applicant->recruiter_id;
                    @endphp
                      <option value="{{ $recruiter->id }}" {{ $recruiter->id ==  $recruiter_id ? 'selected' : '' }}>{{ $recruiter->first_name }} {{ $recruiter->last_name }}</option>
                    @endforeach
                  </select>
                </td>
                <td scope="row" class="text-center" rel="initial"><div id="del-int" rel="{{$intv['interview_record'][0]['id']}}"><i class="fas fa-trash-alt"></i></div></td>
              </tr>
            @endif
          @endforeach
        </tbody>
      </table>
      </div>
      <div><button class="btn btn-sm btn-primary" id="interview1-add-new"><i class="fas fa-plus-circle"></i> Add New</button></div>
    </div>
    <p>&nbsp;</p>

    <!-- final interviews -->
    <div class="field-box">
      <h5 class="text-muted header-underlined-wide">Final Interview</h5>
      <label class="top-label">Final Interviews</label>
      <div style="display: {{ $lintv >= 1 ? 'block' : 'none' }}" id="int-final">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Status</th>
            <th>Note</th>
            <th>Date</th>
            <th>Rescheduled Reason</th>
            <th>Recruiter</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody id="tbl-finterview">
          @foreach($interviews as $intv2)
            @if($intv2['interview_record'][0]['type'] == 'final')
              <tr>
                <td>
                  <input type="hidden" name="talent_application_id_final[]" value="{{$intv2['interview_record'][0]['id']}}">
                  <input type="hidden" name="talent_application_type_final[]" value="final" required>
                <select class="form-control" name="talent_application_status_final[]" required>
                  <option value=""></option>
                  <option value="1" {{$intv2['interview_record'][0]['status'] == '1' ? 'selected' : ''}}>Passed Final Interview</option>
                  <option value="2" {{$intv2['interview_record'][0]['status'] == '2' ? 'selected' : ''}}>Failed Final Interview</option>
                  <option value="3" {{$intv2['interview_record'][0]['status'] == '3' ? 'selected' : ''}}>No Show</option>
                  <option value="5" {{$intv2['interview_record'][0]['status'] == '5' ? 'selected' : ''}}>Pending Interview</option>
                  <option value="4" {{$intv2['interview_record'][0]['status'] == '4' ? 'selected' : ''}}>Blacklisted</option>
                  <option value="6" {{$intv2['interview_record'][0]['status'] == '6' ? 'selected' : ''}}>Re-scheduled | Health Issues</option>
                  <option value="7" {{$intv2['interview_record'][0]['status'] == '7' ? 'selected' : ''}}>Re-scheduled | System Requirements</option>
                  <option value="8" {{$intv2['interview_record'][0]['status'] == '8' ? 'selected' : ''}}>Re-scheduled | Personal Matters</option>
                  <option value="9" {{$intv2['interview_record'][0]['status'] == '9' ? 'selected' : ''}}>Re-scheduled | Employment</option>
                  <option value="10" {{$intv2['interview_record'][0]['status'] == '10' ? 'selected' : ''}}>Re-scheduled | Family Emergency</option>
                  <option value="11" {{$intv2['interview_record'][0]['status'] == '11' ? 'selected' : ''}}>On Hold | System Requirements</option>
                  <option value="12" {{$intv2['interview_record'][0]['status'] == '12' ? 'selected' : ''}}>On Hold | Undecided</option>
                  <option value="13" {{$intv2['interview_record'][0]['status'] == '13' ? 'selected' : ''}}>Re-Application | System Requirements</option>
                  <option value="14" {{$intv2['interview_record'][0]['status'] == '14' ? 'selected' : ''}}>Re-Application | Undecided</option>
                  <option value="15" {{$intv2['interview_record'][0]['status'] == '15' ? 'selected' : ''}}>Re-Application | Health Issues</option>
                  <option value="16" {{$intv2['interview_record'][0]['status'] == '16' ? 'selected' : ''}}>Re-Application | Re-schedule issues</option>
                  <option value="17" {{$intv2['interview_record'][0]['status'] == '17' ? 'selected' : ''}}>Re-Application | Employed</option>
                  <option value="18" {{$intv2['interview_record'][0]['status'] == '18' ? 'selected' : ''}}>Re-Application | Personal Matters</option>
                  <option value="19" {{$intv2['interview_record'][0]['status'] == '19' ? 'selected' : ''}}>Re-Application | Emergency</option>
                  <option value="20" {{$intv2['interview_record'][0]['status'] == '20' ? 'selected' : ''}}>Re-Application | Business Structure</option>
                  <option value="21" {{$intv2['interview_record'][0]['status'] == '21' ? 'selected' : ''}}>Withdrawn | System Requirements</option>
                  <option value="22" {{$intv2['interview_record'][0]['status'] == '22' ? 'selected' : ''}}>Withdrawn | Undecided</option>
                  <option value="23" {{$intv2['interview_record'][0]['status'] == '23' ? 'selected' : ''}}>Withdrawn| Health Issues</option>
                  <option value="24" {{$intv2['interview_record'][0]['status'] == '24' ? 'selected' : ''}}>Withdrawn | Re-schedule issues</option>
                  <option value="25" {{$intv2['interview_record'][0]['status'] == '25' ? 'selected' : ''}}>Withdrawn | Employed</option>
                  <option value="26" {{$intv2['interview_record'][0]['status'] == '26' ? 'selected' : ''}}>Withdrawn| Personal Matters</option>
                  <option value="27" {{$intv2['interview_record'][0]['status'] == '27' ? 'selected' : ''}}>Withdrawn | Emergency</option>
                  <option value="28" {{$intv2['interview_record'][0]['status'] == '28' ? 'selected' : ''}}>Withdrawn | Business Structure</option>
                  <option value="29" {{$intv2['interview_record'][0]['status'] == '29' ? 'selected' : ''}}>Pending Exam</option>
                  <option value="30" {{$intv2['interview_record'][0]['status'] == '30' ? 'selected' : ''}}>Pending Systems Check</option>
                  <option value="31" {{$intv2['interview_record'][0]['status'] == '31' ? 'selected' : ''}}>Failed Systems Check</option>
                  <option value="32" {{$intv2['interview_record'][0]['status'] == '32' ? 'selected' : ''}}>Failed Exam</option>

                </select></td>
                <td><textarea name="talent_note_final[]" class="form-control"required>{{$intv2['interview_record'][0]['note']}}</textarea></td>
                <td><input type="text" class="form-control dpk" name="talent_exp_date_end_final[]" value="{{date('M d, Y, h:i:s A', strtotime($intv2['interview_record'][0]['date']))}}" required></td>
                <td><textarea class="form-control" required name="talent_rescheduled_final[]">{{$intv2['interview_record'][0]['re_schedule_reason']}}</textarea></td>
                <td>
                  <select class="form-control" id="tl_recruiter" name="tl_recruiter_final[]">
                    <option value="" disabled selected>Select Recruiter</option>
                    @foreach($allrecruiters as $recruiter)
                    @php
                    $recruiter_id = isset($intv2['interview_record'][0]['recruiter_id']) ? $intv2['interview_record'][0]['recruiter_id'] : $applicant->recruiter_id;
                    @endphp
                      <option value="{{ $recruiter->id }}" {{ $recruiter->id ==  $recruiter_id ? 'selected' : '' }}>{{ $recruiter->first_name }} {{ $recruiter->last_name }}</option>
                    @endforeach
                  </select>
                </td>

                <td scope="row" class="text-center" rel="final"><div id="del-int" rel="{{$intv2['interview_record'][0]['id']}}"><i class="fas fa-trash-alt"></i></div></td>
              </tr>
            @endif
          @endforeach
        </tbody>
      </table>
    </div>
      <div><button class="btn btn-primary  btn-sm" id="interview2-add-new"><i class="fas fa-plus-circle"></i> Add New</button></div>
    </div>
  </div>
</div>

<select id="nodisp" style="display:none">
  @foreach($allrecruiters as $recruiter)
  @php
  $recruiter_id = isset($intv['interview_record'][0]['recruiter_id']) ? $intv['interview_record'][0]['recruiter_id'] : $applicant->recruiter_id;
  @endphp
    <option value="{{ $recruiter->id }}" {{ $recruiter->id ==  $recruiter_id ? 'selected' : '' }}>{{ $recruiter->first_name }} {{ $recruiter->last_name }}</option>
  @endforeach
</select>

<div class="modal fade" id="sendRescheduleEmailModal" tabindex="-1" role="dialog" aria-labelledby="sendRescheduleEmailModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="sendRescheduleEmailModalLabel">Reschedule</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">


        @php
        $allrecs = App\Models\Auth\User::whereId($interview->interview_record[0]->recruiter_id)->first();

        $recruiter_name = '';
        $fname = '';
        $lname = '';

        $recruiter_id = $interview->interview_record[0]->recruiter_id;

        if($recruiter_id){
          $fname = $allrecs->first_name ?? '';
          $lname = $allrecs->last_name ?? '';
          $recruiter_name = $fname.' '.$lname;
          $skype = $allrecs->skype_id ?? '';
          $rec_email = $allrecs->email ?? '';
        }else{
          $fname = $applicant->recruiter_name->first_name ?? '';
          $lname = $applicant->recruiter_name->last_name ?? '';
          $recruiter_name = $fname.' '.$lname;
          $skype = $applicant->recruiter_name->skype_id ?? '';
          $rec_email = $applicant->recruiter_name->email ?? '';
        }

        if($recruiter_id == '12'){
          $rec_email = 'jobs@myvirtudesk.com';
          $recruiter_name = $fname;
        }
        @endphp

        To:<br>
        <input class="form-control" type="text" id="app_to" value="{{$applicant->email}}">

        Message:
        <textarea class="form-control resched_body" id="email-body3">
          <p><strong>Hello {{$applicant->name}},</strong></p>

          <p style="text-align:left"><span style="color:#000000"><span style="font-size:small">Here are the details regarding your interview:</span></span></p>

          <p style="text-align:left"><span style="color:#000000"><span style="font-size:small"><strong>Interview Schedule (MNL): </strong></span></span><span style="color:#ff0000"><span style="font-size:small"><strong>{{ date('F d, Y h:i A', strtotime($interview->interview_record[0]->date)) }} MANILA TIME</strong></span></span></p>

          <p style="text-align:left"><span style="color:#000000"><span style="font-size:small"><strong>Interviewer:</strong></span></span><span style="color:#ff0000"><span style="font-size:small"><strong>&nbsp;{{ $fname ?? '' }} {{ $lname ?? '' }}</strong></span></span></p>

          <p style="text-align:left"><span style="color:#000000"><span style="font-size:small"><strong>Skype ID account</strong></span></span><span style="color:#313131"><span style="font-size:small"><strong>:</strong></span></span><span style="color:#0000ff"><span style="font-size:small"><strong> </strong></span></span><span style="color:#ff0000"><span style="font-size:small"><strong>{{$skype ?? ''}}&nbsp;</strong></span></span><span style="color:#000000"><span style="font-size:small"><strong>or</strong></span></span><span style="font-size:small"><strong> </strong></span><span style="color:#ff0000"><span style="font-size:small"><strong>{{ $recruiter_name }} | Talent Acquisition </strong></span></span><span style="color:#000000"><span style="font-size:small"><strong>or</strong></span></span><span style="color:#ff0000"><span style="font-size:small"><strong>&nbsp;</strong></span></span><a href="mailto:{{ $rec_email ?? ''}}" style="color:#0000ff; text-decoration:underline" target="_blank"><span style="color:#0000ff"><span style="font-size:small"><u><strong>{{$rec_email ?? ''}}</strong></u></span></span></a></p>

          <p style="text-align:left"><span style="color:#000000"><span style="font-size:small">Please be informed that this is already your <strong>RESCHEDULED</strong> date. Please take note of it as we can only reschedule you <strong>ONCE</strong>.</span></p>

          <p><span style="color:#000000"><span style="font-size:14px"><strong><span style="background-color:#f1c40f">IMPORTANT NOTE: Follow the same instructions provided to you on the first email.</span></strong></span></span></p>

          <p><strong><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing &amp; Talent Acquisition Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
          <span style="font-size:small"><strong>Email: </strong></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br />
          <strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></strong></p>

        </textarea>
      </div>
      <div class="modal-footer"><span id="resmsg"></span>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="send_resched" class="btn btn-primary">Send</button>
      </div>
    </div>
  </div>
</div>
@endif
