<!-- Modal -->
<div class="modal fade" id="applicationModal" tabindex="-1" role="dialog" aria-labelledby="applicationModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="applicationModalLabel">New Application</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body row">

        <div class="col-sm-6"><label class="top-label">Position Applying For</label><br>
        <select class="form-control" name="position_applying_for">
          <option value="isa">ISA</option>
          <option value="gva">GVA</option>
          <option value="eva">EVA</option>
        </select><br>
        </div>
        <div class="col-sm-6">
          <input type="hidden" name="come_from" value="Manual">
          <input type="hidden" name="from" value="dashboard">
          <input type="hidden" name="sourcer" value="{{\Auth::id()}}">
        </div>

        <div class="col-sm-6"><label class="top-label">Name</label><br>
        <input type="text" name="full_name" id="full_name" class="form-control"><br></div>
        <div class="col-sm-6"><label class="top-label">Emaill Address</label><br>
        <input type="email" name="email" id="email" class="form-control"><br></div>

        <div class="col-sm-6"><label class="top-label">Mobile Number</label><br>
        <input type="tel" name="mobile_number" id="mobile_number" class="form-control"><br></div>
        <div class="col-sm-6"><label class="top-label">Landline Number</label><br>
        <input type="tel" name="landline" id="landline" class="form-control"><br></div>

        <div class="col-sm-6"><label class="top-label">Address</label><br>
        <input type="text" name="address" id="address" class="form-control"><br></div>
        <div class="col-sm-6"><label class="top-label">Skype</label><br>
        <input type="text" name="skype_id" id="skype_id" class="form-control"><br></div>

        <div class="col-sm-6">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="yes" id="defaultCheck1" name="send_thank">
            <label class="form-check-label" for="defaultCheck1">
              Send applicant 'Thank You' email (*Sends login info to the portal)
            </label>
          </div>
        </div>
        <div class="col-sm-6"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-new-application">Save changes</button>
      </div>
    </div>
  </div>
</div
