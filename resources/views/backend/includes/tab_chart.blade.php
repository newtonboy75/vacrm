<!-- Modal  Charts and stats-->
<div class="modal fade" id="popupStatsModal" tabindex="-1" role="dialog" aria-labelledby="popupStatsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width:100%; max-width:1250px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="popupStatsModalLabel">Stats</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="row">
          <div class="col-sm-12"><div id="temps_div"></div>
          <?= $lava->render('BarChart', 'AllStat', 'temps_div') ?></div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
