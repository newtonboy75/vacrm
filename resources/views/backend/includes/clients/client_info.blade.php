<div class="row">
  <div class="col-sm-5">
    <h5 class="mt-2">Client Information</h5>
    @if(Session::has('saved'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      {{ Session::get('saved') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif

    @if(Session::has('error_save'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      {{ Session::get('error_save') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif

    {{ Form::open(array('url' => '/admin/clients/update_client_info')) }}
    <input type="hidden" name="client_id" value="{{$client->id}}">
    <table class="table table-borderless table-sm ml-4 mt-4 mr-4">
      <tr>
        <td><label class="top-label">Name</label></td><td><input class="form-control" type="text" name="name" value="{{$client->name}}"></td>
      </tr>

      <tr>
        <td><label class="top-label">State</label></td><td>
          <select id="state" name="state" class="form-control">
            <option {{$client->state == 'AL' ? 'selected' : '' }} value="AL">Alabama</option>
            <option {{$client->state == 'AK' ? 'selected' : '' }} value="AK">Alaska</option>
            <option {{$client->state == 'AZ' ? 'selected' : '' }} value="AZ">Arizona</option>
            <option {{$client->state == 'AR' ? 'selected' : '' }} value="AR">Arkansas</option>
            <option {{$client->state == 'CA' ? 'selected' : '' }} value="CA">California</option>
            <option {{$client->state == 'CO' ? 'selected' : '' }} value="CO">Colorado</option>
            <option {{$client->state == 'CT' ? 'selected' : '' }} value="CT">Connecticut</option>
            <option {{$client->state == 'DE' ? 'selected' : '' }} value="DE">Delaware</option>
            <option {{$client->state == 'DC' ? 'selected' : '' }} value="DC">District Of Columbia</option>
            <option {{$client->state == 'FL' ? 'selected' : '' }} value="FL">Florida</option>
            <option {{$client->state == 'GA' ? 'selected' : '' }} value="GA">Georgia</option>
            <option {{$client->state == 'HI' ? 'selected' : '' }} value="HI">Hawaii</option>
            <option {{$client->state == 'ID' ? 'selected' : '' }} value="ID">Idaho</option>
            <option {{$client->state == 'IL' ? 'selected' : '' }} value="IL">Illinois</option>
            <option {{$client->state == 'IN' ? 'selected' : '' }} value="IN">Indiana</option>
            <option {{$client->state == 'IA' ? 'selected' : '' }} value="IA">Iowa</option>
            <option {{$client->state == 'KS' ? 'selected' : '' }} value="KS">Kansas</option>
            <option {{$client->state == 'KY' ? 'selected' : '' }} value="KY">Kentucky</option>
            <option {{$client->state == 'LA' ? 'selected' : '' }} value="LA">Louisiana</option>
            <option {{$client->state == 'ME' ? 'selected' : '' }} value="ME">Maine</option>
            <option {{$client->state == 'MD' ? 'selected' : '' }} value="MD">Maryland</option>
            <option {{$client->state == 'MA' ? 'selected' : '' }} value="MA">Massachusetts</option>
            <option {{$client->state == 'MI' ? 'selected' : '' }} value="MI">Michigan</option>
            <option {{$client->state == 'MN' ? 'selected' : '' }} value="MN">Minnesota</option>
            <option {{$client->state == 'MS' ? 'selected' : '' }} value="MS">Mississippi</option>
            <option {{$client->state == 'MO' ? 'selected' : '' }} value="MO">Missouri</option>
            <option {{$client->state == 'MT' ? 'selected' : '' }} value="MT">Montana</option>
            <option {{$client->state == 'NE' ? 'selected' : '' }} value="NE">Nebraska</option>
            <option {{$client->state == 'NV' ? 'selected' : '' }} value="NV">Nevada</option>
            <option {{$client->state == 'NH' ? 'selected' : '' }} value="NH">New Hampshire</option>
            <option {{$client->state == 'NJ' ? 'selected' : '' }} value="NJ">New Jersey</option>
            <option {{$client->state == 'NM' ? 'selected' : '' }} value="NM">New Mexico</option>
            <option {{$client->state == 'NY' ? 'selected' : '' }} value="NY">New York</option>
            <option {{$client->state == 'NC' ? 'selected' : '' }} value="NC">North Carolina</option>
            <option {{$client->state == 'ND' ? 'selected' : '' }} value="ND">North Dakota</option>
            <option {{$client->state == 'OH' ? 'selected' : '' }} value="OH">Ohio</option>
            <option {{$client->state == 'OK' ? 'selected' : '' }} value="OK">Oklahoma</option>
            <option {{$client->state == 'OR' ? 'selected' : '' }} value="OR">Oregon</option>
            <option {{$client->state == 'PA' ? 'selected' : '' }} value="PA">Pennsylvania</option>
            <option {{$client->state == 'RI' ? 'selected' : '' }} value="RI">Rhode Island</option>
            <option {{$client->state == 'SC' ? 'selected' : '' }} value="SC">South Carolina</option>
            <option {{$client->state == 'SD' ? 'selected' : '' }} value="SD">South Dakota</option>
            <option {{$client->state == 'TN' ? 'selected' : '' }} value="TN">Tennessee</option>
            <option {{$client->state == 'TX' ? 'selected' : '' }} value="TX">Texas</option>
            <option {{$client->state == 'UT' ? 'selected' : '' }} value="UT">Utah</option>
            <option {{$client->state == 'VT' ? 'selected' : '' }} value="VT">Vermont</option>
            <option {{$client->state == 'VA' ? 'selected' : '' }} value="VA">Virginia</option>
            <option {{$client->state == 'WA' ? 'selected' : '' }} value="WA">Washington</option>
            <option {{$client->state == 'WV' ? 'selected' : '' }} value="WV">West Virginia</option>
            <option {{$client->state == 'WI' ? 'selected' : '' }} value="WI">Wisconsin</option>
            <option {{$client->state == 'WY' ? 'selected' : '' }} value="WY">Wyoming</option>
          </select>
        </td>
      </tr>

      <tr>
        <td><label class="top-label">Timezone</label></td>
        <td>
          <select id="timezone" name="timezone" class="form-control">
            <option {{$client->timezone == 'PST' ? 'selected' : ''}} value="PST">PST	- Pacific Standard Time</option>
            <option {{$client->timezone == 'MST' ? 'selected' : ''}} value="MST">MST	- Mountain Standard Time</option>
            <option {{$client->timezone == 'CST' ? 'selected' : ''}} value="CST">CST	- Central Standard Time</option>
            <option {{$client->timezone == 'EST' ? 'selected' : ''}} value="EST">EST	- Eastern Standard Time</option>
            <option {{$client->timezone == 'HST' ? 'selected' : ''}} value="HST">HST	- Hawaii Standard Time</option>
            <option {{$client->timezone == 'AKST' ? 'selected' : ''}} value="AKST">AKST - Alaska Standard Time</option>
          </select>
        </td>
      </tr>

      <tr>
        <td><label class="top-label">Phone Number</label></td><td><input class="form-control" type="text" name="phone_number" value="{{$client->phone_number}}"></td>
      </tr>

      <tr>
        <td><label class="top-label">Email</label></td><td><input class="form-control" type="text" name="email" value="{{$client->email}}"></td>
      </tr>

      <tr>
        <td><label class="top-label">Status</label></td>
        <td>
          <select class="form-control" name="status">
            <option {{$client->status == 'active' ? 'selected' : '' }} value="active">Active</option>
            <option {{$client->status == 'inactive' ? 'selected' : '' }} value="inactive">Cancelled</option>
          </select>
        </td>
      </tr>

      <tr>
        <td><label class="top-label">Date Registered</label></td>
        <td><input class="form-control dpk-d-only" type="text" name="date_registered" value="{{date('F d, Y', strtotime($client->register_at)) ?? ''}}" style="background: #fff !important;"></td>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td><td><input type="submit" class="btn btn-primary mt-3" value="Save changes"></td>
      </tr>
      <tr>
        <td>&nbsp;</td><td></td>
      </tr>

      @if(isset($client->cancel_at))
      <tr>
        <td><strong>Date Cancelled:</strong> {{ date('M d, Y', strtotime($client->cancel_at)) }}</td></td></td>
      </tr>
      @endif

    </table>
    {{ Form::close() }}
  </div>
  <div class="col-sm-1"></div>


  <div class="col-sm-6">
    @if($client_info)
    <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#newCInfo" title="Add New Placement Info">+</button>
    @endif
    <h5 class="mt-2">Recent Placement</h5>
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      {{ Session::get('success') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif

    @if(Session::has('error_success'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      {{ Session::get('error_success') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif

<!--here -->

<div id="current_record">
  @if($client_info)
  {{ Form::open(array('url' => '/admin/clients/save_info')) }}
  <input type="hidden" name="client_id" value="{{$client->id ?? ''}}">
  <input type="hidden" name="id" value="{{ $client_info->id ?? '' }}">
  <div class="row mt-4">
    <div class="col-sm-6">
      <label class="top-label">Marketing Specialist</label>
      <input type="text" id="marketing_specialist" name="marketing_specialist" class="form-control" value="{{$client_info->marketing_specialist ?? ''}}">
    </div>
    <div class="col-sm-6">
      <label class="top-label">Status</label><br>
      <select class="form-control" id="pl_status" name="status">
        <option disabled value="" selected>Please choose</option>
        <option {{ isset($client_info->status) ? $client_info->status == '1' ? 'selected' : '' : '' }} value="1">Active</option>
        <option {{ isset($client_info->status) ? $client_info->status == '2' ? 'selected' : '' : '' }} value="2">Inactive</option>
        <option {{ isset($client_info->status) ? $client_info->status == '3' ? 'selected' : '' : '' }} value="3">Cancelled</option>
        <option {{ isset($client_info->status) ? $client_info->status == '4' ? 'selected' : '' : '' }} value="4">Placement</option>
        <option {{ isset($client_info->status) ? $client_info->status == '5' ? 'selected' : '' : '' }} value="5">Suspended</option>
        <option {{ isset($client_info->status) ? $client_info->status == '6' ? 'selected' : '' : '' }} value="6">Completed</option>
        <option {{ isset($client_info->status) ? $client_info->status == '7' ? 'selected' : '' : '' }} value="7">Incomplete</option>
        <option {{ isset($client_info->status) ? $client_info->status == '8' ? 'selected' : '' : '' }} value="8">Probono</option>
      </select>


      <div class="mt-2 cancelled_reason">
        <label class="top-label">Reason for cancelling</label>
        <textarea value="" name="cancellation_reason" class="form-control">{{$client_info->cancellation_reason}}</textarea>
      </div>



      <div class="mt-2 suspended_reason">
        <label class="top-label">Reason for suspension</label>
        <textarea value="" name="suspension_reason" class="form-control">{{$client_info->suspension_reason}}</textarea>
      </div>


    </div>

    <div class="col-sm-6 mt-3">
      <label class="top-label">What they hired for</label>
      <input type="text" id="hiring_objective" name="hiring_objective" class="form-control" value="{{ $client_info->hiring_objective ?? '' }}">
    </div>

    <div class="col-sm-6 mt-3">
      <label class="top-label">Signed Contract?</label><br>
      <select class="form-control" id="contract_signed" name="contract_signed">
        <option disabled value="" selected>Please choose</option>
        <option {{ isset($client_info->contract_signed) ? $client_info->contract_signed == 'yes' ? 'selected' : '' : '' }} value="yes">Yes</option>
        <option {{ isset($client_info->contract_signed) ? $client_info->contract_signed == 'no' ? 'selected' : '' : '' }} value="no">No</option>
      </select>
    </div>


    <div class="col-sm-6 mt-3">
      <label class="top-label">Employment Status</label>
      <select class="form-control" id="employment_status" name="employment_status">
        <option disabled value="" selected>Please choose</option>
        <option {{ isset($client_info->employment_status) ? $client_info->employment_status == '1' ? 'selected' : '' : '' }} value="1">Full-time</option>
        <option {{ isset($client_info->employment_status) ? $client_info->employment_status == '2' ? 'selected' : '' : '' }}  value="2">Part-time</option>
        <option {{ isset($client_info->employment_status) ? $client_info->employment_status == '3' ? 'selected' : '' : '' }}  value="3">Time Block</option>
        <option {{ isset($client_info->employment_status) ? $client_info->employment_status == '4' ? 'selected' : '' : '' }}  value="4">1 month</option>
      </select>
    </div>

    <div class="col-sm-6 mt-3">
      <label class="top-label">Type of Contract</label>
      <input type="text" id="contract_tb1" name="contract_tb1" class="form-control" value="{{ $client_info->contract_tb1 ?? '' }}">
    </div>

    <div class="col-sm-6 mt-3 mb-1">
      <label class="top-label">Current VA</label>

      <div class="input-group mb-3">
        <input type="hidden" name="oldva" id="oldva" value="{{ trim($client_info->current_va_old) ?? '' }}">
        <input type="text" id="current_va" name="current_va" class="form-control" value="{{ $client_info->current_va_old ?? '' }}">
        <div class="input-group-append" id="btn-replace-va">
          <a href="javascript:;" style="text-decoration: none" title="Replace VA" class="input-group-text" id="btn-replace-va"><i class="fas fa-exchange-alt"></i></a>
        </div>
      </div>

      <div class="replacement_reason">
        <label class="top-label">Replacement Reason</label>
        <textarea class="form-control" name="replacement_reason" id="replacement_reason"></textarea>
      </div>

    </div>

    <div class="col-sm-6 mt-3">
      <label class="top-label">Start Date with Current VA</label>
      @php
      $year = date('Y', strtotime($client_info->current_va_start_date));
      $va_dte = '';
      if($year > '1970'){
        $va_dte = date('M d, Y', strtotime($client_info->current_va_start_date));
      }
      @endphp
      <input data-zdp_readonly_element="false" type="text" id="current_va_date" name="current_va_date" class="form-control" value=" {{ $va_dte }}">
    </div>

    <div class="col-sm-6 mt-3">
      <input type="submit" class="btn btn-secondary" value="Save changes">

    </div>
    <div class="col-sm-6 mt-3" style="border-top: 1px solid #f0f0f0; padding-top: 5px; background: #fafafa; max-height: 200px; overflow: auto;">
      <strong>History:</strong><br><br>

      @if(isset($client_info->suspensions))
      @foreach($client_info->suspensions as $suspension)
      <p><strong>{{date('M d, Y', strtotime($suspension->suspension_date))}}:</strong> Status changed to Suspended <br>
        <strong>Reason:</strong> {{$suspension->suspension_reason}}
      </p>
      @endforeach
      @endif

      @if(isset($client_info->cancellations))
      @foreach($client_info->cancellations as $cancellation)
      <p><strong>{{date('M d, Y', strtotime($cancellation->cancellation_date))}}:</strong> Status changed to Cancelled<br>
        <strong>Reason:</strong> {{$cancellation->reason}}
      </p>
      @endforeach
      @endif

      @if(isset($client_info->replacements))
      @foreach($client_info->replacements as $replacement)
      @php
      $replaced_by = App\ApplicantRecord::whereId($replacement->replaced_by)->first();
      $replaced = App\ApplicantRecord::whereId($replacement->replacement_id)->first();
      @endphp
      <p><strong>{{date('M d, Y', strtotime($replacement->date))}}</strong>: VA {{$replaced->name ?? ''}} was replaced by {{$replaced_by->name ?? ''}}<br>
        <strong>Replacement Reason:</strong> {{$replacement->replacement_reason}}
      </p>
      @endforeach
      @endif

    </div>
  </div>
  {{ Form::close() }}

  @else
  <div style="width: 100%; display: inline-block">
    <p><br><button type="button" class="btn btn-success float-left" data-toggle="modal" data-target="#newCInfo" title="Create New Placement Info">Create New Placement</button></p>
  </div>
  @endif
  </div>

  <div class="modal fade" id="newCInfo" tabindex="-1" role="dialog" aria-labelledby="newCInfoLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="newCInfoLabel">Create New Placement Info</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="new_record">

            <input type="hidden" name="client_id" value="{{$client->id ?? ''}}">
            <input type="hidden" name="id" value="{{ $client_info->id ?? '' }}">
            <div class="row mt-4">
              <div class="col-sm-6">
                <label class="top-label">Marketing Specialist</label>
                <input type="text" id="marketing_specialist" name="marketing_specialist" class="form-control" value="">
              </div>
              <div class="col-sm-6">
                <label class="top-label">Status</label><br>
                <select class="form-control" id="pl_status" name="status">
                  <option disabled value="" selected>Please choose</option>
                  <option value="1">Active</option>
                  <option value="2">Inactive</option>
                  <option value="3">Cancelled</option>
                  <option value="4">Placement</option>
                  <option value="5">Suspended</option>
                  <option value="6">Completed</option>
                  <option value="7">Incomplete</option>
                  <option value="8">Probono</option>
                </select>

              </div>


              <div class="col-sm-6 mt-3">
                <label class="top-label">What they hired for</label>
                <input type="text" id="hiring_objective" name="hiring_objective" class="form-control" value="" placeholder="Admin/Marketing/Prospecting">
              </div>

              <div class="col-sm-6 mt-3">
                <label class="top-label">Signed Contract?</label><br>
                <select class="form-control" id="contract_signed" name="contract_signed">
                  <option disabled value="" selected>Please choose</option>
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
                </select>
              </div>

              <div class="col-sm-6 mt-3">
                <label class="top-label">Employment Status</label><br>
                <select class="form-control" id="employment_status" name="employment_status">
                  <option disabled value="" selected>Please choose</option>
                  <option value="1">Full-time</option>
                  <option value="2">Part-time</option>
                  <option value="3">Time Block</option>
                  <option value="4">1 month</option>
                </select>
              </div>

              <div class="col-sm-6 mt-3">
                <label class="top-label">Type of Contract</label>
                <input type="text" id="contract_tb1" name="contract_tb1" class="form-control" value="">
              </div>

              <div class="col-sm-6 mt-3 mb-1">
                <label class="top-label">Current VA</label>

                <div class="input-group mb-3">
                  <input type="text" id="current_va_pp" name="current_va" class="form-control ui-autocomplete-input" value="" autocomplete="on" placeholder="Type name of VA">
                </div>

              </div>

              <div class="col-sm-6 mt-3">
                <label class="top-label">Start Date with Current VA</label>
                <input data-zdp_readonly_element="false" type="text" id="current_va_date" name="current_va_date" class="form-control" value="{{ date('M d, Y') }}">
              </div>

              <div class="col-sm-6 mt-3">
              </div>
            </div>
            <div class="alert alert-success fade show" role="alert" id="new_alert">
            </div>

            <div class="alert alert-warning fade show" role="alert" id="new_error">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="save_new_info_client">Save changes</button>
        </div>
      </div>
    </div>
  </div>

</div>

<!-- end here -->
</div>

<!-- floating note -->
<div class="floating-note">
  <div class="notes-title"><i class="far fa-sticky-note"></i> Note
    <div class="actn float-right"><i id="c-up" class="fas fa-caret-up"></i><i id="c-down" class="fas fa-caret-down"></i></div>
  </div>
  <form>
    <div class="notes-data">
    <small class="top-label3">Note Type</small><br>
    <select name="note_type" required class="form-control" id="note-type">
      <option value="" selected disabled>Please select Note type</option>
      <option value="consultation">Consultation</option>
      <option value="touchbase">Touchbase</option>
      <option value="followup">Follow Up</option>
      <option value="notification">Notification</option>
      <option value="collection">Collection</option>
      <option value="others">Others (specify)</option>
    </select>
    <br>
    <input class="form-control" type="text" id="note-type-others" value="" placeholder="Please specify note type (max 256 chars)">
    <br>
    <small class="top-label3">Notes</small>
    <textarea id="client-notes-pp" class="form-control"></textarea>
    <br>
    <small class="top-label3">Interaction Type</small><br>
    <select id="interaction_type" class="form-control">
      <option value="" selected disabled>Please select Interaction type</option>
      <option value="call">Call</option>
      <option value="email">Email</option>
      <option value="text">Text</option>
      <option value="others">Others (specify)</option>
    </select>
    <br>
    <input class="form-control" type="text" id="note-interaction-others" value="" placeholder="Please specify interaction type (max 256 chars)">
    <br>
    <small class="top-label3">Status</small><br>
    <select id="note_status" class="form-control">
      <option value="" selected disabled>Please select Status</option>
      <option value="open">Open</option>
      <option value="ongoing">On Going</option>
      <option value="pending">Pending</option>
      <option value="acknowleged">Acknowleged</option>
      <option value="done">Done</option>
    </select>

    <input type="hidden" id="pp_client_id" value="{{$client->id}}">
    <input type="hidden" id="note_id" value="">

    <br>
    <span id="msg-note" class="float-left"></span>
    <button id="save-note-floating" class="btn btn-blue btn-sm float-right">Save Note</button>
    <br><br>

  </div>
  </form>
</div>
<!-- floating note -->
