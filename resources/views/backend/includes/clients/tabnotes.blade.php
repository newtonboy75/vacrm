<div class="table-responsive">
<table style="width: 100% !important;"  class="table table-striped ataTable display cell-border compact hover order-column row-border stripe" id="data-client-note">
  <thead>
    <tr>
      <th>ID</th>
      <th>Date</th>
      <th>Note Type</th>
      <th>Note</th>
      <th>Interaction Type</th>
      <th>Status</th>
      <th>Noted By</th>
      <th>Client</th>
      <th>*</th>
    </tr>
  </thead>
</table>

<!-- Modal -->
<div class="modal fade" id="singleNoteModal" tabindex="-1" role="dialog" aria-labelledby="singleNoteModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="singleNoteModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="notes-data-single">
        <small class="top-label3">Note Type</small><br>
        <select name="note_type" required class="form-control" id="note-type">
          <option value="" selected disabled>Please select Note type</option>
          <option value="consultation">Consultation</option>
          <option value="touchbase">Touchbase</option>
          <option value="followup">Follow Up</option>
          <option value="notification">Notification</option>
          <option value="collection">Collection</option>
          <option value="others">Others (specify)</option>
        </select>
        <br>
        <input class="form-control" type="text" id="note-type-others" value="" placeholder="Please specify note type (max 256 chars)">
        <br>
        <small class="top-label3">Notes</small>
        <textarea id="client_notes_pp_single" class="form-control"></textarea>
        <br>
        <small class="top-label3">Interaction Type</small><br>
        <select id="interaction_type" class="form-control">
          <option value="" selected disabled>Please select Interaction type</option>
          <option value="call">Call</option>
          <option value="email">Email</option>
          <option value="text">Text</option>
          <option value="others">Others (specify)</option>
        </select>
        <br>
        <input class="form-control" type="text" id="note-interaction-others" value="" placeholder="Please specify interaction type (max 256 chars)">
        <br>
        <small class="top-label3">Status</small><br>
        <select id="note_status" class="form-control">
          <option value="" selected disabled>Please select Status</option>
          <option value="open">Open</option>
          <option value="ongoing">On Going</option>
          <option value="pending">Pending</option>
          <option value="acknowleged">Acknowleged</option>
          <option value="done">Done</option>
        </select>
        <br>
        <input class="form-control" type="text" id="note-status-others" value="Please specify status type">

        <input type="hidden" id="pp_client_id" value="">
        <input type="hidden" id="note_id" value="">

      </div>
      </div>
      <div class="modal-footer">
        <div id="msg-note"></div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit-note-floating">Save changes</button>
      </div>
    </div>
  </div>
</div>
</div>
