@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')

<div class="card">
  <div class="card-body">
    <div class="row">

      <div class="col-sm-12">
        <ul class="nav nav-tabs" id="clientTabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="client-info-tab" data-toggle="tab" href="#client-info" role="tab" aria-controls="client-info" aria-selected="false">Client Information</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="placement-info-tab" data-toggle="tab" href="#placement-info" role="tab" aria-controls="placement-info" aria-selected="true">Placement Info Records</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="placement-note-tab" data-toggle="tab" href="#placement-note" role="tab" aria-controls="placement-note" aria-selected="true">Notes</a>
          </li>
        </ul>
        <div class="tab-content" id="clientTabContent">
          <div class="tab-pane fade show active" id="client-info" role="tabpanel" aria-labelledby="client-info-tab" style="width: 99% !important;">@include('backend.includes.clients.client_info')
          </div>
          <div class="tab-pane fade" id="placement-info" role="tabpanel" aria-labelledby="placement-info-tab">
            <div style="width: 100% !important;">
              @include('backend.includes.clients.client_info_table')
            </div>
          </div>
          <div class="tab-pane fade" id="placement-note" role="tabpanel" aria-labelledby="placement-note-tab">
            <div style="width: 100% !important;">
              @include('backend.includes.clients.client_notes_table')
            </div>
          </div>

        </div>
      </div>

    </div>
  </div>
</div>






@endsection
