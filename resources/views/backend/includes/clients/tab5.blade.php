<button class="btn btn-orange btn-sm float-right mb-2" id="btn-add-client">Add New Client</button>

<table style="width: 100% !important;" class="table table-striped ataTable display cell-border compact hover order-column row-border stripe" id="data-client-lists-all">
  <thead>
    <tr>
      <th>ID</th>
      <th>Client Name</th>
      <th>Timezone</th>
      <th>State</th>
      <th>Phone Number</th>
      <th>Email</th>
      <th>Status</th>
      <th>Date Joined</th>
      <th></th>
    </tr>
  </thead>
</table>

<!-- Modal -->
<div class="modal fade" id="clientPlacementModal" tabindex="-1" role="dialog" aria-labelledby="clientPlacementModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="clientPlacementModalLongTitle">Add New Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{ Form::open(array('url' => '/admin/clients/create_client')) }}
        <input type="hidden" name="client_id" value="">
        <table class="table table-borderless">
          <tr>
            <td><label class="top-label">Name</label></td><td><input class="form-control" type="text" id="name" name="name" value=""></td>
          </tr>

          <tr>
            <td><label class="top-label">State</label></td><td>
              <select id="state" name="state" class="form-control">
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="DC">District Of Columbia</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
              </select>
            </td>
          </tr>

          <tr>
            <td><label class="top-label">Timezone</label></td>
            <td>
              <select id="timezone" name="timezone" class="form-control">
                <option value="PST">PST	- Pacific Standard Time</option>
                <option value="MST">MST	- Mountain Standard Time</option>
                <option value="CST">CST	- Central Standard Time</option>
                <option value="EST">EST	- Eastern Standard Time</option>
                <option value="HST">HST	- Hawaii Standard Time</option>
                <option value="AKST">AKST - Alaska Standard Time</option>
              </select>
            </td>
          </tr>

          <tr>
            <td><label class="top-label">Phone Number</label></td><td><input class="form-control" type="text" id="phone_number" name="phone_number" value=""></td>
          </tr>

          <tr>
            <td><label class="top-label">Email</label></td><td><input class="form-control" type="text" id="email" name="email" value=""></td>
          </tr>

        </table>
        {{ Form::close() }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-new-client">Save changes</button>
      </div>
    </div>
  </div>
</div>
