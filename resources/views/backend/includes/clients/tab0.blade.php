<div class="row">
  <div class="col-sm-6 text-left">
    <div id="piechart" style="width: 700px !important; height: 400px !important; padding: 0px;"></div>
    <p>&nbsp;</p>
    <div id="piechart-year" style="width: 700px !important; height: 400px !important; padding: 0px;"></div>
  </div>

  <div class="col-sm-6">
    <div class="row ml-1">
      <div class="col-sm-12 mt-5">
        <p class="text-green font-weight-bold">Client Demographics</p>
        <table class="table-striped compact row-border cell-border stripe hover" id="data-client-stats">
          <thead>
            <tr><th>Year</th><th>PST</th><th>EST</th><th>CST</th><th>MST</th><th>HST</th><th>TOTAL</th></tr>
          </thead>
        </table>
        <div class="float-right mt-3 mr-3 font-weight-bold" id="stat_total"></div>
      </div>
      <div class="col-sm-12 mt-5">
        <p class="text-green font-weight-bold">Year-to-date Status</p>
        <table class="table-striped display row-border cell-border stripe hover order-column" id="data-client-stats-year">
          <thead>
            <tr><th>Year</th><th>Active</th><th>Inactive</th><th>Cancelled</th><th>Suspended</th><th>Completed</th><th>Incomplete</th><th>Placement</th><th>Probono</th><th>Timeblock - Active</th><th>Total</th></tr>
          </thead>
        </table>
        <div class="float-right mt-3 mr-3 font-weight-bold" id="stat_total_year"></div>
      </div>
      <p>&nbsp;</p>
    </div>
  </div>

</div>
