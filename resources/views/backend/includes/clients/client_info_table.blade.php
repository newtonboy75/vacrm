<div class="table-responsive">
<table style="width: 100% !important;"  class="table table-striped ataTable display cell-border compact hover order-column row-border stripe" id="data-client-placement">
  <thead>
    <tr>
      <th>ID</th>
      <th>Start Date with VA</th>
      <th>Status</th>
      <th>Marketing Specialist</th>
      <th>What They Hired For</th>
      <th>Signed Contract?</th>
      <th>VA name</th>
      <th>Full Time/Part Time</th>
      <th>Type of Contract</th>
      <th>Cancellation Date</th>
      <th>Reason for Cancellation</th>
      <th>Suspension Date</th>
      <th>Reason for Suspension</th>
      <th></th>
    </tr>
  </thead>
</table>
</div>
