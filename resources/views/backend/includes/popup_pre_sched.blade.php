<!-- Modal -->
<div class="modal fade" id="sendTrainingSched" tabindex="-1" role="dialog" aria-labelledby="sendTrainingSched" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:700px;">
      <div class="modal-header">
        <h5 class="modal-title" id="sendTrainingSched">Email Editor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">To</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="to" id="to" value="{{ $applicant->email ?? ''}}">
        </div>

        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Subject</span>
          </div>
          <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="subject" id="subject" value="Pre-training Requirements | Virtudesk Requirements Team">
        </div>

        <div class="form-group">
          <label for="address" class="f-label">Body</label>
          <textarea name="email-body" id="email-body-presched" class="form-control" required rows="10">
            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>Dear {{$applicant->name}},</strong></span></span></span></p>

            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>Good day!</strong></span></span></span></p>

            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">We would like to thank you for submitting your requirements.</span></span></span></p>

            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong><em>Congratulations! </em></strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#38761d"><strong><em>You will now be endorsed to Training!</em></strong></span></span></span></p>

            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Please do take note of the following details regarding your training:</span></span></span></p>
<?php 
$end_date = ''; 
$start_date = '';

if(isset($applicant->last_training)){
$training_date = explode(' ', $applicant->last_training->start_date); 
$start_date = $training_date[0].' '.$training_date[1].' '.$training_date[2];
$end_date = $training_date[3].' '.$training_date[4].' '.$training_date[5].' '.strtoupper($training_date[6]);
}
?>


            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#2980b9"><strong>Training Date (Manila):</strong> {{ $start_date}}</span></span></span></p>

            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#2980b9"><strong>Training Time (Manila):</strong> {{ $end_date }}</span></span></span></p>

            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Our Learning &amp; Communications Team will be sending you another email a couple of days OR a day before your scheduled training date for specific instructions.</span></span></span></p>

            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">You will also have your New Hire Orientation a day before your training date stated above so please mark your calendars. You will receive another email in the next coming days regarding your New Hire Orientation &amp; Training Proper. Kindly check your personal and virtudesk email accounts&#39; inboxes and spam folders.</span></span></span></p>

            <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Should you have queries, do not hesitate to contact us at </span></span></span><a href="mailto:requirements@myvirtudesk.com" style="text-decoration:none"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#1155cc"><u>requirements@myvirtudesk.com</u></span></span></span></a></p>

            <p><span style="color:#4f7a28"><span style="font-size:small"><strong>Virtudesk Requirements Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
              <span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:requirements@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>requirements@myvirtudesk.com</u></span></span></a><br />
              <strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>

            </textarea>
            <script>
            CKEDITOR.replace( 'email-body-presched' );
            </script>
          </div>
        </div>
        <div class="modal-footer">
          <span class="wait-status text-warning"></span>
          <input type="hidden" name="email_type" value="pretrain" id="email_type">
          <input type="hidden" name="training_id" value="{{ $applicant->last_training->id ?? '' }}" id="training_id">
          <input type="hidden" name="user_id" value="{{$applicant->id ?? ''}}" id="user_id">
          <input type="hidden" name="from" value="requirements@myvirtudesk.com|Virtudesk Requirements Team" id="from_admin">
          <input type="hidden" name="subject" value="Re: MyVirtudesk Application" id="subject">
          <button id="email_dismiss" type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary" id="presched" rel="redo">Send Email and Apply Transition</button>
        </div>
      </div>
    </div>
  </div>
