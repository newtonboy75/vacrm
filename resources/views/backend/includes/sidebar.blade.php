<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link {{
          active_class(Route::is('admin/dashboard'))
        }}" href="{{ route('admin.dashboard') }}">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        @lang('menus.backend.sidebar.dashboard')
      </a>
    </li>

    @if($logged_in_user->isAdmin())

    <li class="divider"></li>

    <li class="nav-item nav-dropdown {{
      active_class(Route::is('admin/log-viewer*'), 'open')
    }}">
    <a class="nav-link nav-dropdown-toggle {{
      active_class(Route::is('admin/log-viewer*'))
    }}" href="#">
    <i class="nav-icon far fa-user"></i> Applicants
  </a>

  <ul class="nav-dropdown-items">
    <li class="nav-item">
      <a class="nav-link {{
        active_class(Route::is('admin.sourcing-record*'))
      }}" href="{{ route('admin.sourcing-record') }}">
      Sourcing Record
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{
      active_class(Route::is('admin.talent-acquisition-record*'))
    }}" href="{{ route('admin.talent-acquisition-record') }}">
    Talent Acquisition Record
  </a>
</li>
<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin.reference-check*'))
  }}" href="{{ route('admin.reference-check') }}">
  Reference Check
</a>
</li>

<!--
<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin.home-evaluation-record*'))
  }}" href="{{ route('admin.home-evaluation-record') }}">
  Home Evaluation Record
</a>
</li>
-->

<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin.pre-training-requirements*'))
  }}" href="{{ route('admin.pre-training-requirements') }}">
  Pre Training Requirements
</a>
</li>

<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin.training-record*'))
  }}" href="{{ route('admin.training-record') }}">
  Training Record
</a>
</li>

<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin.placement-record*'))
  }}" href="{{ route('admin.placement-record') }}">
  Placement Record
</a>
</li>

<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin.hr-operation-record*'))
  }}" href="{{ route('admin.hr-operation-record') }}">
  Operation Record
</a>
</li>

<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin/recruiters-hub*'))
  }}" href="{{ route('admin.recruiters-hub') }}">
  Recruiter's Hub
</a>
</li>

<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin/clients*'))
  }}" href="{{ route('admin.clients') }}">
  Clients
</a>
</li>

<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin/admin-log*'))
  }}" href="{{ route('admin.admin-log') }}">
  Admin Logs
</a>
</li>



</ul>
</li>


<li class="nav-title">
  @lang('menus.backend.sidebar.system')
</li>
<li class="nav-item nav-dropdown {{ active_class(Route::is('admin/auth*'), 'open') }}">
  <a class="nav-link nav-dropdown-toggle {{ active_class(Route::is('admin/auth*')) }}" href="#">
  <i class="nav-icon fa fa-cog" aria-hidden="true"></i> @lang('menus.backend.access.title')
    @if ($pending_approval > 0)
    <span class="badge badge-danger">{{ $pending_approval }}</span>
    @endif
  </a>
<ul class="nav-dropdown-items">
  <li class="nav-item">
    <a class="nav-link {{
      active_class(Route::is('admin/auth/user*'))
    }}" href="{{ route('admin.auth.user.index') }}">
    @lang('labels.backend.access.users.management')

    @if ($pending_approval > 0)
    <span class="badge badge-danger">{{ $pending_approval }}</span>
    @endif
  </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{
      active_class(Route::is('admin/auth/role*'))
    }}" href="{{ route('admin.auth.role.index') }}">
    @lang('labels.backend.access.roles.management')
  </a>
  </li>
</ul>
</li>
@if(\Auth::user()->hasRole('dev'))
<li class="divider"></li>

<li class="nav-item nav-dropdown {{ active_class(Route::is('admin/log-viewer*'), 'open') }}">
  <a class="nav-link nav-dropdown-toggle {{
    active_class(Route::is('admin/log-viewer*'))
  }}" href="#">
  <i class="nav-icon fas fa-list"></i> @lang('menus.backend.log-viewer.main')
  </a>

  <ul class="nav-dropdown-items">
    <li class="nav-item">
      <a class="nav-link {{
        active_class(Route::is('admin/log-viewer'))
      }}" href="{{ route('log-viewer::dashboard') }}">
      @lang('menus.backend.log-viewer.dashboard')
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{
      active_class(Route::is('admin/log-viewer/logs*'))
    }}" href="{{ route('log-viewer::logs.list') }}">
    @lang('menus.backend.log-viewer.logs')
  </a>
  </li>
  </ul>
</li>
@endif
@endif

<li class="nav-item">
  <a class="nav-link {{
    active_class(Route::is('admin/outbox*'))
  }}" href="{{ route('admin.outbox') }}">
  Oubox
</a>
</li>

</ul>
</nav>

<button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
