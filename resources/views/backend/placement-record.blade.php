@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <p class="card-title mb-0 font-weight-bold">
                  &centerdot; EMPLOYEES &centerdot;
              </p>
            </div><!--col-->

            <div class="col-sm-7">
              <div class="dropdown" id="custom-searchbox">
                  <button class="btn btn-orange btn-sm" id="btn-custom-search" data-toggle="dropdown"><i class="fas fa-search"></i> Search</button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                    <li role="presentation">
                      <div class="m-2" style="width: 220px;">
                        <input type="text" class="form-control" id="custom-search-input" placeholder="search name, email...">
                        <small class="text-warning">(press enter to start searching)</small><br><br>
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="all-rec">
                          <label class="form-check-label" for="exampleCheck1">Search all record</label>
                        </div>

                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="all-emp-stat">
                          <label class="form-check-label" for="exampleCheck1">Include inactive and archived applicants/employees</label>
                        </div>

                        <div id="cl-search"><a href="javascript:;"><i class="fas fa-times"></i> clear search</a></div>

                      </div>
                    </li>


                  </ul><br><br>
                  <div><select class="form-control" id="placement-type">
                    <option value="all">All Placement Records</option>
                    <option selected value="active">Active Employees Only</option>
                    <option value="inactive">Archived Employees Only</option>
                  </select></div>
                </div>&nbsp;
              <button class="btn btn-green btn-sm float-right" id="btn-add-applicant" data-toggle="modal" data-target="#applicationModal"><i class="fas fa-user-plus"></i> Add New</button>

              <div class="float-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-danger dropdown-toggle mr-3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-cog"></i>
                  </button>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="/admin/sourcing-record/export-ref" data-toggle="modal" data-target="#popupDataExport"><i class="fas fa-file-export"></i> Export Data</a>
                    <a class="dropdown-item" role="button" href="/admin/sourcing-record/export-ref" data-toggle="modal" data-target="#popupStatsModal"><i class="far fa-chart-bar"></i> View Stats</a>
                  </div>
                </div>
              </div>

            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
              <div class="table-responsive">
                <table class="table cell-border  display stripe hover order-column" id="data-placement">
                   <thead>
                      <tr>
                        <th>ID</th>
                         <th>Name</th>
                         <th>Program</th>
                         <th>Status</th>
                         <th>Client Interview Status</th>
                         <th>Action</th>
                      </tr>
                   </thead>
                </table>
              </div>
            </div><!--col-->
        </div><!--row-->

    </div><!--card-body-->
</div><!--card-->

@include('backend.includes.tab_chart')
@include('backend.includes.new_applicant')
@endsection
