<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Tymbl Team</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="Tymbl Team">
  <style>
  	body {font-family: arial; padding: 10px 10px; font-size: 16px;}
  	.footer-text{font-size: 15px; margin-top:70px; text-align:center;  width: 90%; margin-bottom: 30px;}

    .listing-link{font-size: 17px !important; display: block; padding:8px; margin-left:auto; margin-right: auto; text-align: center;     color: rgb(255, 255, 255);
    background-color: rgb(0, 123, 255); border-color: rgb(0, 123, 255); font-weight: 600; text-decoration:none; width: 280px; margin-bottom: 20px; margin-top: 5px;border-radius: 2px!important;}
    .list_img{width:100%;margin-top:10px;margin-bottom:10px;}
  </style>
</head>
<body>
<div><br>
  <p><strong>Hi {{ucfirst($applicant)}}!</strong></p>
  <p>We would like to inform you that your profile has been endorsed to one of our clients. You may have a scheduled Client interview anytime with this Client. With this, kindly take note of the proposed Interview Schedule below:</p>

  <strong>Date and Time ({{$timezone}}):</strong> {{$date}}<br>
  <strong>Client's Name:</strong> {{$client}}<br>
  <strong>Position:</strong> {{$position}}

  <p>Please do take note of this schedule. We have also added you to the Interview room for this Client on Skype. If you have any questions or clarifications, don't hesitate to reach out on the Interview Room for this client on Skype or send our Client Services Manager - Ms. Lalla Tan, a message on Skype.</p>

  <p>Don't forget to research something about the client, the client's company and practice on how to answer interview questions.</p>

  <p>We wish you all the best on this interview and go nail that CI!</p>


  <p><span style="color:#4f7a28"><span style="font-size:small"><strong>Placements Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br>
  <span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:placements@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>placements@myvirtudesk.com</u></span></span></a><br>
  <span style="color: #0b5397; font-weight:bold">Website:</span> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>
    <br>
</div>
</body>
</html>
