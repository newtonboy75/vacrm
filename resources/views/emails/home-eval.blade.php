<html>
<head><title></title></head>
<body>
<p><span style="font-size:9pt"><span style="font-family:Arial"><span><strong>Hi {{$applicant->name ?? ''}},&nbsp;</strong></span></span></span></p>


<p><span style="font-size:9pt"><span style="font-family:Arial">We&rsquo;re writing to inform you of the result of the recently conducted reference check.</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">After careful assessment and background checking, we would like to inform you:</span></span></p>

<p style="text-align:center"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CONGRATULATIONS!&nbsp;</strong></span></span></span></p>

<p style="margin-left:48px; text-align:center"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#38761d"><strong>You Passed the Background Screening Process!</strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">To jumpstart your career with us, we have provided some instructions and a checklist of requirements that you would need to accomplish within </span></span><span style="font-size:9pt"><span style="font-family:Arial"><strong>24 hours</strong></span></span><span style="font-size:9pt"><span style="font-family:Arial"> from the receipt of this email.&nbsp;</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">Please read and follow the instructions carefully. </span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff2600"><em>At this point, your ability to follow and comprehend instructions are already being assessed.&nbsp;</em></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">Here&rsquo;s a list of requirements to be completed within 24 hours from the time this email has been sent, so we can include you in the next available training class:</span></span></p>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><strong><em>Immediate Requirements (To be passed within 24 hours)</em></strong></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Add our Facebook Account and our official Facebook Page &amp; send us a screenshot that you have accomplished this:</span></span>

	<ul>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial">Marcelina Pablo (Facebook Account) </span></span><a href="https://www.facebook.com/marcelina.pablo.946" style="text-decoration:none"><span style="font-size:9pt"><span style="font-family:Arial"><u>https://www.facebook.com/marcelina.pablo.946</u></span></span></a></li>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial">Virtudesk Recruitment - PH (Facebook Page) </span></span><a href="https://www.facebook.com/virtudeskph/" style="text-decoration:none"><span style="font-size:9pt"><span style="font-family:Arial"><span style="background-color:#ffffff"><u>https://www.facebook.com/virtudeskph/</u></span></span></span></a></li>
	</ul>
	</li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Create your Official Virtudesk Skype &amp; Gmail Accounts.&nbsp;</span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><strong>Virtudesk Gmail</strong></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Go to Gmail and create a Virtudesk Gmail account.&nbsp;</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">For your Gmail address please follow this format:</span></span>
	<ul>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial"><u>virtudesk.firstname.lastname@gmail.com</u></span></span></li>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial">Example: </span></span><span style="font-size:9pt"><span style="font-family:Arial"><u>virtudesk.sarah.fuller@gmail.com</u></span></span></li>
	</ul>
	</li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><strong>Virtudesk Skype</strong></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Go to skype.com and create a Virtudesk Skype Account.</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">When creating your Skype account, please use the option &ldquo;Use your email instead&rdquo;.</span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9"><img src="https://lh3.googleusercontent.com/YA47K6p_gjEdzQ1ZiKk1G7clGqZpC2ieUk0od82kQMmpumYfmkXe9clhOdApnBQJYmzFTh2NIyEyxRecMFPJuXxtbDcx_9Y4CNDFLI_v3yarjxtucbxw6QOsafv4WhGGhGMVeEah" style="height:499px; width:613px" /></span></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Use the Virtudesk Gmail account you created as your email for Skype. Then create your password.</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Fill up this Virtudesk Applicant Information Sheet:&nbsp;</span></span>
	<ul>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#1155cc"><u>https://docs.google.com/forms/d/1LVLIzU7b-S78opQzax88vt675ig68Z4Snk3BGIELRto/edit?usp=sharing</u></span></span></span></li>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial">Please make sure to provide all the necessary details being asked on the form.</span></span></li>
	</ul>
	</li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Recent 2x2 ID Picture</span></span></li>
</ul>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff2600"><strong><em>Submit all of these requirements by logging in </em></strong></span></span></span><a href="https://careers.myvirtudesk.ph/" ><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#1155cc"><strong><em><u>HERE</u></em></strong></span></span></span></a><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff2600"><strong><em>.&nbsp;Choose the returning applicant , log in here option. Take note that your login credential was previously given when you initially submitted your application to us.</em></strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff2600"><strong><em>Proof of unemployment from the previous employer prior to your application here in Virtudesk, like COE , proof of conversation with dates or approved resignation letter that should be received from the email domain of your previous employer.</em></strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong><em>Requirements should be sent within 24 hours upon receipt of this email. Failure to submit these requirements on time may lead to your application being forfeited.</em></strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><em>Once you have submitted all the immediate requirements, please wait for our requirement team&rsquo;s email regarding your training schedule</em></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff2600"><strong><em>NOTE: Please do note that you will only be given a training schedule and be endorsed to a training class if you have completed ALL the Immediate Requirements on or before the deadline. Failure to submit these requirements on time will lead to your application being forfeited.</em></strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><strong><em>To Follow Requirements (May be sent beyond the 24-hour time frame)</em></strong></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Security Bank Account (This can be submitted once you get your client)</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">NBI Clearance</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Home Evaluation Form. (Specific instructions for this are provided on this email. Please continue reading the whole email)</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Signed Virtudesk Disclosure Form -<span style="color:#2980b9"> </span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong><em>this will be sent to your personal email account after the new hire orientation.</em></strong></span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Signed Non-Compete Clause Form -&nbsp;<span style="color:#2980b9">&nbsp;</span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong><em>this will be sent to your personal email account after the new hire orientation.</em></strong></span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Signed Virtudesk Contract -<span style="color:#2980b9"> </span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong><em>this will be sent to your personal email account after the new hire orientation.</em></strong></span></span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9">==================================================================</span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><strong>SYSTEM REQUIREMENTS CHECKLIST &amp; HOME EVALUATION FORM:</strong></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">One of our crucial requirements is for applicants to complete the Home Evaluation Form.&nbsp;</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">Home Evaluation is done for us to make sure that all of our applicants are </span></span><span style="font-size:9pt"><span style="font-family:Arial"><strong>system ready </strong></span></span><span style="font-size:9pt"><span style="font-family:Arial">and that their location/working environment is </span></span><span style="font-size:9pt"><span style="font-family:Arial"><strong>conducive </strong></span></span><span style="font-size:9pt"><span style="font-family:Arial">to starting a work from home career. This helps ensure that once you start training and get placed with a client, we wouldn&rsquo;t have any problems in terms of your system and work location.</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">Here are our Technical Requirements:</span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">A fully functioning laptop/PC running on Windows 10 or higher or Mac OS High Sierra 10.13.4 or lower or Mac OS Catalina. (Mojave &amp; Big Sur are not accepted)</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">4GB of RAM or Higher&nbsp; for both main and backup computers</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">A fully functioning laptop/PC of at least Core i3 Processor or higher</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">64-bit Architecture for both main &amp; backup computers</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">A backup computer that meets the specifications above</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">A USB Noise-Cancelling headset</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Wired internet connection of at least 5mbps total speed</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">A backup internet connection of at least 5mbps total speed.</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Workstation free from any noise and distractions</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">A Backup Power Source</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Webcam of at least 5 megapixels</span></span></li>
</ul>

<p><span style="font-size:9pt"><span style="font-family:Arial">To further understand our technical requirements, kindly read and understand the instructions below.</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">We have attached the Home Evaluation Form on this email. To accomplish this form, kindly read and follow the instructions &amp; guidelines on what we can accept as your systems.&nbsp;</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><strong>GUIDELINES IN FILLING UP THE HOME EVALUATION FORM: (Please read these carefully)</strong></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial">Make sure to fill out the form with all ACCURATE DETAILS as this would be placed under your application file and will serve as proof that you are system ready for this job.</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>Main System.</strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9"><strong>&nbsp;</strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial">Main system is a<span style="color:#2980b9"> </span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff0000">non-negotiable </span></span></span><span style="font-size:9pt"><span style="font-family:Arial">technical requirement. All applicants are required to have their own PC/laptop.&nbsp;</span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">The main system that you are required to declare is a system that meets our technical requirements and needs to be owned by the applicant. This means, the main system you will declare should be owned by you - not your brother, sister, mother, cousin, friend or anyone else. We do not allow applicants to use other people&rsquo;s systems and declare it as their own. The use of mobile phones, tablets, and/or alike as the main system is not allowed.&nbsp;</span></span></p>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">Your Main System should be running on a 64-bit architecture and should be Windows 10 or higher or Mac OS High Sierra 10.13.4 or lower or Mac OS Catalina. (Mojave &amp; Big Sur are not accepted)</span></span></p>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">Your main system should be ready to use on the day of your New Hire Orientation.</span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>Main Internet System. </strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial">The main internet system is a<span style="color:#2980b9"> </span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff0000">non-negotiable</span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9"> </span>technical requirement. All applicants are required to have their own internet system.</span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">Your main internet System may be owned by a different person as long as they are living in the same household unit as yours and you need to have access to troubleshoot the modem/unit in the middle of the night during your shift to ensure that you can get back online immediately. The main system should also be physically accessible for you to connect via LAN cable or wired internet connection. </span></span><br />
<br />
<span style="font-size:9pt"><span style="font-family:Arial">Main internet system must be at least </span></span><span style="font-size:9pt"><span style="font-family:Arial"><strong>5 mbps</strong></span></span><span style="font-size:9pt"><span style="font-family:Arial"> of total speed or higher. If you are subscribed to a 5 mbps plan this will not be considered. A 5 mbps plan does not assure a total of 5 mbps total speed.</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">Your main internet system should be ready to use on the day of your New Hire Orientation.</span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>Backup System. </strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial">All trainees are expected to have a backup system or at least have a contingency plan/location should their main system encounter any problem anytime within their work hours.</span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">The backup system that you will declare should also meet the system specifications that we require. Your backup system </span></span><span style="font-size:9pt"><span style="font-family:Arial"><strong>can be</strong></span></span><span style="font-size:9pt"><span style="font-family:Arial"> owned by someone else as long as this is readily available and accessible for you to use in the middle of the night and should not impose any problems in terms of getting to the location of this backup system. We prefer that your backup system is in the same household as yours. But if not, if this will be in another location, it should meet the requirements of the travel time for backup/contingencies - which is</span></span><span style="font-size:9pt"><span style="font-family:Arial"><strong> not MORE THAN</strong></span></span><span style="font-size:9pt"><span style="font-family:Arial"> one (1) hour from where you reside.</span></span></p>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">Your Backup System should be running on a 64-bit architecture and should be Windows 10 or higher or Mac OS High Sierra 10.13.4 or lower Mac OS Catalina. (Mojave &amp; Big Sur are not accepted)</span></span></p>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong><em>NOTE: Your backup system could not be the main system/backup system of another person you know who is also applying here in Virtudesk or is working as a Virtual Assistant for another company or Client.</em></strong></span></span></span></p>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">If you don&rsquo;t have any backup system or contingency plans yet, that&rsquo;s okay. You may still proceed with training and look for options during the training period. However, you must declare a backup system or contingency plan before your expected graduation date in training.</span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>Headset.</strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9"> </span>A </span></span><span style="font-size:9pt"><span style="font-family:Arial"><strong>USB</strong></span></span><span style="font-size:9pt"><span style="font-family:Arial"> Noise Cancelling Headset is required for all applicants regardless if you are applying for a voice or a non-voice program. However, if you don&rsquo;t have one yet, you may still proceed to training however, you need to make sure that you will be able to provide one before you graduate from training</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>Web camera</strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9">. </span>A fully functioning, clear webcam is needed for this position and is </span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff0000">non-negotiable</span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9">. </span>The webcamera should be ready to use before the New Hire Orientation.</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>Workstation/Work Area</strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9">. </span>Your workstation should be free from any distractions - people passing by, background noises (vehicles, roosters, dogs and/or any background noise). Your workstation must not be your bed or even outside of your household (garage, garden, etc.). A simple chair and table inside a room would suffice as long as the area is free from any distractions and background noise.</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>Backup ISP and Power Source. </strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial">Kindly place the picture of your back up internet connection and a backup power source. If you don&rsquo;t have any backup power source and backup internet yet or if you don&rsquo;t have the monetary capacity to provide one now, that&rsquo;s ok. You don&rsquo;t need to purchase one right away, however,&nbsp; kindly place the description on what your contingency plan /location is. You would need to place a contingency location in which you can go for the time being if you don&rsquo;t have a physical unit for your backup ISP and a backup power source.&nbsp;</span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">For example, if your contingency plan is your sister&rsquo;s house then kindly explain on the back-up box that you will be using your sister&rsquo;s house. Indicate how long it will take for you to go to your backup location. If you have any other contingency plans or locations, please make sure to place the description of that location on the form.</span></span></p>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">Your contingency locations should </span></span><span style="font-size:9pt"><span style="font-family:Arial"><strong>not be MORE THAN one (1) hour </strong></span></span><span style="font-size:9pt"><span style="font-family:Arial">from where you are and should not also be affected by a power or internet outage if your area is affected by one. This means that the backup power supply and internet that you will declare on this Home Evaluation Form should not have any interruptions at the same time as yours and should be of a different service provider than yours.</span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9"><strong><em> </em></strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong><em>We prefer that your backup ISP and backup power source be different from your declared main ISP and main power source.</em></strong></span></span></span></p>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial">If you don&rsquo;t have any contingency plans yet, that&rsquo;s okay. You may still proceed with training and look for options during the training period. However, you must declare a backup power source &amp; internet or contingency plan before your expected graduation date in training.</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong>JUST TAKE NOTE: That your backup system (ISP and PC/laptop and your backup power source) and or your contingency plans should ALWAYS be readily available. There is a reason why they are called BACKUP/CONTINGENCY PLANS. This is if your main system and/or connection fails, you should be able to resort to them immediately.</strong></span></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>For the ISP Bill</strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9">, </span>please upload the MOST RECENT Bill of the ISP that you will be using and declaring as your main ISP. If the ISP Account is not under your name, you would need to secure a SIGNED AUTHORIZATION LETTER coming from the person or the account owner then have it scanned and send it as an attachment together with the Home Evaluation Form. You would also need to scan one (1) valid ID of the account owner. The ID should be a valid ID WITH SIGNATURE. Scan the ID then paste it on the Home Evaluation form.</span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>For the webcam picture</strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9">, </span>show us that your webcam is working by taking a photo of your webcam (turned on) showing yourself on the monitor.&nbsp;</span></span></li>
</ul>

<p><span style="font-size:9pt"><span style="font-family:Arial">Kindly complete the attached home evaluation form and save it on your computer as a PDF file. This PDF File will be checked on your first day of training. That is why as early as now, we are giving you ample amount of time to complete the requirements. Your assigned Trainer will be checking the home evaluation form on the first day of training.</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">But again, if you don&rsquo;t have any backup power source, backup internet, backup system &amp; USB Noise Cancelling headset - you may complete these during your training.</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial">Your training schedule will be provided to you by the Employee Compliance Team as soon as you have submitted the </span></span><span style="font-size:9pt"><span style="font-family:Arial"><strong>immediate requirements </strong></span></span><span style="font-size:9pt"><span style="font-family:Arial">mentioned above. (Requirements that are to be submitted within 24-hours)</span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><strong>If you have any concerns regarding the requirements, please direct all your queries to our Requirements Team at</strong><span style="color:#2980b9"><strong> </strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#1155cc"><strong><u>requirements@myvirtudesk.com</u></strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#2980b9"><strong> </strong></span><strong>so they can assist you.</strong></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><em>Kindly make sure to observe the deadline provided to you and acknowledge this email.&nbsp;</em></span></span></p>

<p>&nbsp;</p>

<p><span style="font-size:12px"><span style="font-family:Arial"><span style="color:#4f7a28"><strong>Reference Verification</strong></span><span style="color:#2980b9"><strong> | </strong></span><span style="color:#ff9300"><strong>Employee Compliance Team</strong></span></span></span></p>

<p><span style="font-size:12px"><span style="font-family:Arial"><strong>Email</strong><span style="color:#0b5394"><strong>: </strong></span><span style="color:#1155cc"><u>virtudesk.referenceteam@gmail.com</u></span></span></span></p>

<p><span style="font-size:12px"><span style="font-family:Arial"><strong>Website:</strong> myvirtudesk.ph</span></span></p>

<p>&nbsp;</p>

<hr />
<p><span style="font-size:12px"><span style="font-family:Arial"><span style="color:#c0392b"><strong>Note:</strong></span><span style="color:#2980b9"><strong> </strong>Please be guided by our Recruitment Office Hours: Monday thru Friday - 9:00 PM to 6:00 AM Manila Standard Time. All emails received beyond working hours will be answered on the next business day</span></span></span></p>

</body>
</html>
