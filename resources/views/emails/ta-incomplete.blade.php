<p style="margin-bottom: 0cm"><font size="2" style="font-size: 13px"><b>Dear {{$applicant->name ?? ''}}, </b></font>
</p>
<p>We are sending this email to remind you that you have not completed or submitted your requirements for us to complete your application.</p>

<p>Previously, we have sent you an email with the list of requirements that you need to submit for us to be able to endorse you to the next step of the hiring process however we failed to receive a response from you.</p>

<p>With this, we would like to follow up on the said requirements so we can endorse you to training. Kindly complete your requirements on or before</p>

<p>We look forward to hearing from you soon!</p>

<p><span style="color:#4f7a28"><span style="font-size:small"><strong>Virtudesk Requirements Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
<span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:requirements@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>requirements@myvirtudesk.com</u></span></span></a><br />
<strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>
<hr />
<p><strong><span style="color:#c0392b"><strong>Note</strong></span>: </strong><span style="color:#2980b9">Please be guided by our Recruitment Office Hours: Tuesdays thru Saturdays - 7:00 AM to 3:00 PM Manila Standard Time. Initial interviews are conducted from 6:00 AM- 6:00 PM Manila Standard Time. All emails received beyond working hours will be answered on the next business day.</span></p>

<p>&nbsp;</p>