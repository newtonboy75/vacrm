<html>
<head><title></title></head>
<body>
<p><span style="font-size:13px"><strong>Dear {{$applicant->name ?? ''}}, </strong></span></p>
<p><span style="background-color:#ffffff"><span style="font-size:small">Thank you for your interest in seeking employment with our company and for giving us the opportunity to review your resume details.</span></span></p>

<p><span style="background-color:#ffffff"><span style="font-size:small">Although your experience and qualifications are impressive, we have identified that your experiences are currently not a fit for our requirements. Therefore, we will not be proceeding further with your application at this stage.&nbsp;</span></span></p>

<p><span style="background-color:#ffffff"><span style="font-size:small">However, we are keeping your details in our file and will refer to your application again should a suitable job opportunity arise.</span></span></p>

<p><span style="background-color:#ffffff"><span style="font-size:small">Once again, we thank you for your time and effort and we wish you every success in your future career endeavors.</span></span></p>

<p><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing & Talent Acquisition Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br>
<span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br>
<span style="color: #0b5397; font-weight:bold">Website:</span> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>
</body>
</html>
