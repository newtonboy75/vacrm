<p style="margin-bottom: 0cm"><font size="2" style="font-size: 13px"><b>Hi {{$applicant->name ?? ''}}, </b></font>
</p>
<p><span style="font-size:small">Before anything else, We&rsquo;d like to thank you for taking time to apply to us.</span></p>

<p><span style="font-size:small">In line with the recent Interview that you have had to go through, we are sending you this email to let you know of the result. </span></p>

<p><span style="font-size:small">After careful assessment of your application, we regret to inform you that you did not pass the Interview. With this, we will no longer take your application any further.</span></p>

<p><span style="font-size:small">However, if you wish to re-apply after 3 months, we will be glad to process your application.</span></p>

<p>&nbsp;</p>


<p><strong><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing & Talent Acquisition Team</strong></span></span><span style="font-size:small"><strong> | </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk</strong></span></span><br />
<span style="font-size:small"><strong>Email: </strong></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br />
<strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></strong></p>
