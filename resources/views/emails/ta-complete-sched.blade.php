<p><span style="font-size:13px"><b>Dear {{$applicant->name ?? ''}},</b></span></p>

<p><span style="font-size:small">Good day!</span></p>

<p><span style="font-size:small">We would like to thank you for submitting your requirements.</span></p>

<p><span style="font-size:small">Congratulations! You will now be endorsed to Training!</span></p>

<p><span style="font-size:small">Please do take note of the following details regarding your training:</span></p>

<p><span style="color:#d35400"><strong><span style="font-size:small">Training Start Date (Manila):</span></strong></span> <span style="color: #000 !important; font-weight: bold;">{{ $applicant->last_training->start_date ?? '' }}</span></p>

<p><span style="color:#d35400"><strong><span style="font-size:small">Training End Date (Manila):</span></strong></span> <span style="color: #000 !important; font-weight: bold;">{{ $applicant->last_training->end_date ?? '' }}</span></p>

<p><span style="font-size:small">Our Learning &amp; Communications Team will be sending you another email a couple of days OR a day before your scheduled training date for specific instructions.</span></p>

<p><span style="font-size:small">You will also have your New Hire Orientation on your first day of your training so please mark your calendars. You will receive another email in the next coming days regarding your New Hire Orientation & Training Proper. Kindly check your personal and virtudesk email accounts' inboxes and spam folders.</span></p>

<p><span style="font-size:small">Please visit and study the following links before your training:<br />
&nbsp;<br />
Introduction to Real Estate - https://www.youtube.com/watch?v=w3a6WNLKZts<br />
Real Estate Terms - https://www.youtube.com/watch?v=9E2AIlMyyCg<br />
Real Estate Vocabulary - https://www.youtube.com/watch?v=O5cROx7Thyc</span></p>

<p><br />
<span style="font-size:small">Should you have queries, do not hesitate to contact us at requirements@myvirtudesk.com.</span></p>

<p><span style="color:#4f7a28"><span style="font-size:small"><strong>Virtudesk Requirements Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
<span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:requirements@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>requirements@myvirtudesk.com</u></span></span></a><br />
<strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>
<hr />
<p><strong><span style="color:#c0392b"><strong>Note</strong></span>: </strong><span style="color:#2980b9">Please be guided by our Recruitment Office Hours: Tuesdays thru Saturdays - 7:00 AM to 3:00 PM Manila Standard Time. Initial interviews are conducted from 6:00 AM- 6:00 PM Manila Standard Time. All emails received beyond working hours will be answered on the next business day.</span></p>

<p>&nbsp;</p>
