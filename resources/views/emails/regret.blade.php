<html>
<head><title></title></head>
<body>
  <p style="font-weight: bold;">Good day {{$applicant->name ?? ''}},</p>

  <p><span style="font-size:13px;">We would like to inform you of the result of the background screening our team has conducted in regard to your application.</span></p>

  <p>After conducting the reference check and after communicating with your declared references, we would like to inform you that you did not pass the background screening process</p>

  <p>At this point in the process, we will no longer be processing your application, however, if you wish to re-apply after 3 months, we will be glad to process your application.</p>


  <p>&nbsp;</p>

  <p><span style="color:#4f7a28"><span style="font-size:small"><strong>Reference Verification</strong></span></span><span style="font-size:small"><strong> | </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Employee Compliance Team</strong></span></span><br>
  <span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:virtudesk.referenceteam@gmail.com"><span style="color:#1155cc"><span style="font-size:small"><u>virtudesk.referenceteam@gmail.com</u></span></span></a><br>
  <span style="color: #0b5397; font-weight:bold">Website:</span> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>

  </body>
  </html>
