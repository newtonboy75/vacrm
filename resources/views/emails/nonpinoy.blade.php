<html>
<head><title></title></head>
<body>
  <p style="text-align:left"><span style="background-color:#ffffff"><span style="font-size:small"><strong>Dear {{$applicant->name ?? ''}},</strong></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-size:small">Thank you for your interest in seeking employment with our company and for giving us the opportunity to review your resume details.</span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-size:small">Although your experience and qualifications are impressive, Virtudesk job vacancies are exclusively offered to Filipino citizens and must currently reside anywhere in the Philippines. </span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-size:small">Once again, we thank you for your time and effort and we wish you every success in your future career endeavors.</span></span></p>

  <p>&nbsp;</p><p><strong><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing &amp; Talent Acquisition Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
    <span style="font-size:small"><strong>Email: </strong></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br />
    <strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></strong></p>
  </body>
  </html>
