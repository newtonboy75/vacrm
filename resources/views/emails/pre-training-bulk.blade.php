<html>
<head><title></title></head>
<body>

  <p>Good Day Applicant!</p>

  <p style="color: #ff9900; font-weight: 600;">CONGRATULATIONS!</p>

  <p>We’re writing to welcome you to Virtudesk and to tell you how much we are looking forward to you joining our team.</p>
  <p>To jumpstart your career with us, we will be providing your with your Training Schedule. Please refer to the below information:</p>

  <p style="color:#ff2600; font-weight: 600">
    Training Date (Manila): {{ $start_date }} - {{ $end_date }}<br>
    Time (Manila): {{ $start_time }} - {{ $end_time }}</p>

    <p>Please do take note of your training schedule. Also, please make sure that you have submitted the following requirements already:</p>

    <p style="font-weight: 600">Immediate Requirements</p>
    <ul>
      <li>Add our Facebook Account and our official Facebook Page:<br>
        Marcelina Pablo (Facebook Account) https://www.facebook.com/profile.php?id=100014544690756<br>
        Virtudesk Recruitment - PH (Facebook Page) https://www.facebook.com/virtudeskph/</li>

      <li>Official Virtudesk Skype & Gmail Accounts. <span style="color: #ff9900;">(If you didn’t create one yet, please refer to the Congratulatory email sent to you previously with instructions on how to create your Official Virtudesk accounts.)</span></li>
      <li>Filled Out the Applicant Information Sheet<br>https://drive.google.com/open?id=1LVLIzU7b-S78opQzax88vt675ig68Z4Snk3BGIELRto</li>
      <li>Recent 2x2 ID Picture</li>
      <li>NBI Clearance <span style="color: #ff9900;">(If you can’t provide this yet, please inform our Requirements team via email))</span></li>

        </ul>
        <p style="font-weight: 600">To Follow Requirements (May be sent during training.)</p>
        <ul>
          <li>Certificate of Employment from the previous employer prior to your application here in Virtudesk.<br>For Freelancers; please submit a Recommendation Letter coming from your previous Client.</li>
          <li>Security Bank Account (This can be passed once you get your client)</li>
          <li>Signed Virtudesk Disclosure Form - <span style="color: #ff9900; font-weight: 500;">this will be sent to your personal email account before the start of your training.</span></li>
          <li>Signed Non-Compete Clause Form- <span style="color: #ff9900; font-weight: 500;">this will be sent to your personal email account before the start of your training.</pan></li>
            <li>Signed Virtudesk Contract - <span style="color: #ff9900; font-weight: 500;">this will be sent to your personal email account before the start of your training.</span></li>
          </ul>

          <hr style="border-top: 3px double #8c8b8b;">

          <p>If you have submitted these requirements already, no need to send them again. Another email will be sent to you with instructions regarding your training.</p>

          <p>NOTE: If you have not submitted any of the above mentioned immediate requirements, please make sure to do so before {{ $start_date }} at {{ $start_time }} Manila.</p>

          <p>If you have any concerns regarding the requirements, please direct all your queries to our Requirements Team at requirements@myvirtudesk.com so they can assist you. And kindly confirm the receipt of this email.</p>

          <p>&nbsp;</p>

          <p><span style="color:#4f7a28"><span style="font-size:small"><strong>Virtudesk Requirements Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br>
            <span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:requirements@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>requirements@myvirtudesk.com</u></span></span></a><br>
            <span style="color: #0b5397; font-weight:bold">Website:</span> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>
          </body>
          </html>
