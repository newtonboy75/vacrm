<html>
<head><title></title></head>
<body>
<p><span style="font-size:13px"><strong>Dear {{$applicant->name ?? ''}}, </strong></span></p>

<p><span style="background-color:#ffffff"><span style="font-size:small">Thank you for sending your application.</span></span></p>


@if(count($applicant->initial_interview) > 0)

  @if($applicant->initial_interview[0]->interview_record[0]->status == '3')
    <p>Upon reviewing our records, it shows that you recently applied and was scheduled for an initial interview last <strong>{{ date('F d, Y', strtotime($applicant->initial_interview[0]->interview_record[0]->date)) }}</strong>. However, you did not show up and the recruiter has tagged you as <strong>no show</strong>.</p>
    <p>Here in Virtudesk, we value professionalism and the time of our applicants that's why we process them in a timely manner. With an applicant not showing up on a scheduled interview with no notifications that they cannot make it already shows a lot about their interest in the position.</p>
    <p>With this, we're afraid that we won't be able to process your application or take it any further at this point. You may reapply after 3 months from the date of your interview. We wish you all the best of luck in your next career move!</p>
  @else
    <p><span style="background-color:#ffffff"><span style="font-size:small">Upon reviewing our records, it shows that you recently applied and had your initial interview last <strong>{{isset($applicant->initial_interview[0]->interview_record[0]->date) ? date('F d, Y', strtotime($applicant->initial_interview[0]->interview_record[0]->date)) : '[no interview record yet]'}}</strong> and unfortunately, you did not pass the interview.&nbsp;</span></span></p>
    <p><span style="background-color:#ffffff"><span style="font-size:small">With this, we&#39;re afraid that we won&#39;t be able to process your application or take it any further as you have recently been processed. You may reapply after 3 months from the date of your initial interview.</span></span></p>
    <p><span style="background-color:#ffffff"><span style="font-size:small">Best of luck on your next career move!</span></span></p>
  @endif

@else
  <p><span style="background-color:#ffffff"><span style="font-size:small">Upon reviewing our records, it shows that you recently applied and had your initial interview last <strong>{{isset($applicant->initial_interview[0]->interview_record[0]->date) ? date('F d, Y', strtotime($applicant->initial_interview[0]->interview_record[0]->date)) : '[no interview record yet]'}}</strong> and unfortunately, you did not pass the interview.&nbsp;</span></span></p>
  <p><span style="background-color:#ffffff"><span style="font-size:small">With this, we&#39;re afraid that we won&#39;t be able to process your application or take it any further as you have recently been processed. You may reapply after 3 months from the date of your initial interview.</span></span></p>
  <p><span style="background-color:#ffffff"><span style="font-size:small">Best of luck on your next career move!</span></span></p>
  @endif

  <p>&nbsp;</p><p><strong><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing &amp; Talent Acquisition Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
  <span style="font-size:small"><strong>Email: </strong></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br />
  <strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></strong></p>
</body>
</html>
