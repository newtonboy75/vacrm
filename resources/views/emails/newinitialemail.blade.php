<html>
<head><title></title></head>
<body>
  <p style="text-align:left"><span style="background-color:#ffffff"><span style="font-size:small"><strong>Dear Nonito Tester,</strong></span></span></p>

  <div id="initial-invite-interview">

    <p><span style="color:#ff9900"><span style="font-size:small"><strong>CONGRATULATIONS!</strong></span></span></p>

    <p>We&rsquo;re writing to inform you that we have reviewed your profile and we are pleased to inform you that we would like to get to know you better.&nbsp;</p>

    <p>With this, please login on our Recruitment Portal to view your interview schedule. You may login <a href="{{config('app.vdesk_url')}}/application_progress">here</a>.</p>

    <p>Once again, congratulations and we look forward to speaking with you.</p>

  </div>

  <p><strong><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing &amp; Talent Acquisition Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
    <span style="font-size:small"><strong>Email: </strong></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br />
    <strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></strong></p>
  </body>
  </html>
