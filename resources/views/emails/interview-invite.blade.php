<html>
<head><title></title></head>
<body>

<p><span style="font-size:13px">Hi <strong>{{$applicant->name ?? ''}}, </strong></span></p>

<p><span style="font-size:small">We hope you are well and thank you for sending your application.</span></p>

<p><span style="font-size:small">We are happy to inform you that you&rsquo;ve been shortlisted as one of our candidates for the Virtual Assistant Position and we would like to invite you for an interview.</span></p>

<p><span style="font-size:small">Interview details are provided below together with some instructions we would need you to follow. Follow the steps we have provided below for your interview. Don&rsquo;t worry, we will guide you every step of the way and we promise, you won&rsquo;t get lost. </span></p>

<p><span style="font-size:small">-----------------------------------------------</span></p>

<p><span style="color:#6aa84f"><span style="font-size:small"><em><strong>Interview Instructions: (Please read carefully)</strong></em></span></span></p>

<p><span style="font-size:small"><strong>Step 1: </strong></span><span style="font-size:small">You will enter the first level of our recruitment process which is the one on one </span><span style="font-size:small"><strong>SVI </strong></span><span style="font-size:small">or </span><span style="font-size:small"><em><strong>Skype Video Interview.</strong></em></span><span style="font-size:small"> Please take note that for this interview a working web camera and headset is </span><span style="font-size:small"><strong>required</strong></span><span style="font-size:small"> as stated on our system requirements on our website. </span></p>

<p><span style="font-size:small"><strong>Please select your preferred interview date and time here: <a href="{{config('app.vdesk_url')}}/interview_schedule/{{$applicant->id ?? ''}}/{{$applicant->applicants_hash ?? ''}}">{{config('app.vdesk_url')}}/interview_schedule/{{$applicant->id ?? ''}}/{{$applicant->applicants_hash ?? ''}}</a></strong></span></p>

<p><span style="font-size:small"><strong>Step 2:</strong></span><span style="font-size:small"> Kindly use your personal computer or laptop during the interview. Using another person&#39;s pc/laptop and/or having your interview at a computer shop/internet cafe is not allowed.</span></p>

<p><span style="font-size:small"><strong>Step 3 : </strong></span><span style="font-size:small">Please </span><span style="color:#ff9900"><span style="font-size:small"><em><strong>add your interviewer on Skype on the day of your scheduled interview.</strong></em></span></span></p>

<p><span style="font-size:small">Skype ID account is</span><span style="font-size:small"> <strong><span id="pop_rec_skype">{{$applicant->recruiter_name->skype_id ?? '' }}</span></strong> and <strong><span id="pop_rec_email">{{$applicant->recruiter_name->email ?? '' }}</span></strong></span></p>

<p><span style="color:#ff9900"><span style="font-size:small"><em><strong>NOTE: ONLY ADD THE RECRUITER ON THE DAY OF YOUR INTERVIEW. </strong></em></span></span></p>

<p><span style="font-size:small"><strong>Step 4:</strong></span><span style="font-size:small"> Please make sure to be at home at your workstation and use the system that you will be using if ever you get hired. A systems check will be conducted during the initial stages of the recruitment process.</span></p>

<p><span style="font-size:small"><strong>Step 5:</strong></span><span style="font-size:small"> </span><span style="color:#ff9900"><span style="font-size:small"><strong>Crucial part of all,</strong></span></span><span style="font-size:small"> please be online at least </span><span style="color:#ff0000"><span style="font-size:small"><strong>3 HOURS BEFORE</strong></span></span><span style="font-size:small"> your scheduled interview and message your Interviewer as soon as you are online. On the day of the interview, once you have added your Interviewer, </span><span style="color:#ff0000"><span style="font-size:small"><strong>immediately message</strong></span></span><span style="color:#ff0000"><span style="font-size:small"> </span></span><span style="font-size:small">her with your full name, email address and your interview schedule. She will send you a message with the initial interview instructions.</span></p>

<p><span style="color:#ff0000"><span style="font-size:small"><strong>If you don&rsquo;t send your Interviewer a message once you are online, they will not send you any details and will not call you for the interview. So please make sure to send them a message that you are already online.</strong></span></span><br />
<br />
<span style="font-size:small"><strong>Step 6: </strong></span><span style="color:#ff0000"><span style="font-size:small"><u>VERY IMPORTANT</u></span></span><span style="font-size:small">, prepare 3 contacts of your character references before your initial interview. Inform your character references ahead of time that we will be calling them once you have passed the initial interview and examination. We can only accept your Direct Manager/ HR/ Team Lead as your reference.</span></p>

<p><span style="font-size:small">Below are the information needed:</span></p>

<p><span style="font-size:small">Name :</span></p>

<p><span style="font-size:small">Position:</span></p>

<p><span style="font-size:small">Company:</span></p>

<p><span style="font-size:small">Contact Number:</span></p>

<p><span style="font-size:small">Email Address: </span></p>

<p><span style="font-size:small">-------------------------------------------------</span></p>

<p><span style="color:#ff0000"><span style="font-size:small"><strong>Something to remember:</strong></span></span><span style="font-size:small"> If you will be qualified and will be scheduled for the training, you should secure a location conducive for work without any distractions and should meet the company&rsquo;s system requirements:</span></p>

<ul>
	<li><span style="font-size:small">Computer Processor: at least i3 (for both main and backup system)</span></li>
	<li><span style="font-size:small">Computer memory/RAM: at least running at 4.00 GB (for both main and backup system</span></li>
	<li><span style="font-size:small">Computer OS: at least Windows XP</span></li>
	<li><span style="font-size:small">Primary Internet Connection: running on a 3mbps plan or higher. Preferably wired connection.</span></li>
	<li><span style="font-size:small">Backup Internet Connection: running on 3mbps. Broadband sticks and such.</span></li>
</ul>

<p><span style="color:#ff0000"><span style="font-size:small"><em>If your system does not meet the requirements above, please let your assigned Recruiter know immediately. </em></span></span></p>

<p><span style="color:#ff0000"><span style="font-size:small"><strong>Final Reminders:</strong></span></span></p>

<p><span style="font-size:small">This is still a job interview and we want to make sure that you are on your best. Kindly follow these guidelines and make sure that you are all set before the recruiter calls you:</span></p>

<ul>
	<li><span style="font-size:small"><strong>Location.</strong></span><span style="font-size:small"> Make sure to have a clear background. Make sure you are in a place where no one can walk behind you and no distracting backgrounds can be seen (ex. kitchen, dining tables, sofa, bed, people sleeping, restroom, etc.) </span><span style="font-size:small"><em><strong>Most importantly, make sure that you are at home and you are using the system that you will be using if ever you will work for us.</strong></em></span></li>
	<li><span style="font-size:small"><strong>Dress for success.</strong></span><span style="font-size:small"> When applying for a job you have to present yourself well - you have to look professional and be professional. Wearing of sandos, tubes, sleep wear, nighties, tshirts, and other revealing clothes are not professional and are not suitable for job interviews. Polo, Polo Shirts, collared blouse, blazers, suits, long sleeves are the acceptable. Make sure to wear something decent and formal, just like going to a job interview. Dressing up for the occasion is a big plus as this represents your personality and your willingness for the job.</span></li>
	<li><span style="font-size:small"><strong>Appearance. </strong></span><span style="font-size:small">make sure to look professional on your video. Comb your hair and put some light makeup for the ladies.</span></li>
	<li><span style="font-size:small"><strong>Expression.</strong></span><span style="font-size:small"> Smile at the camera and always make sure to look straight to the camera all throughout the interview. Remember, this is still a person you are talking to so you need to look straight to the camera as if you&#39;re talking and looking to the eyes of the person your talking to.</span></li>
	<li><span style="font-size:small"><strong>Lighting.</strong></span><span style="font-size:small"> Make sure that you have adequate lighting during your interview. You want to make sure that the interviewer would see your face and expression while talking. A poorly lighted environment is not recommended for your interview. Also, make sure that your cameras setting is adjusted so that your video will not blurred and make sure that you are at the center of the video rather than on the side.</span></li>
</ul>

<p><span style="color:#ff0000"><span style="font-size:small"><em><strong>NOTE: AT THIS POINT, YOU ARE NOW BEING ASSESSED ON HOW YOU FOLLOW INSTRUCTIONS</strong></em></span></span></p>

<p><span style="font-size:small">Thank you and kindly acknowledge the receipt of this email and your confirmation. We are looking forward to your Interview.</span></p>

<p><span style="font-size:small">Be Yourself and Just Enjoy Your Interview. Smile!</span></p>

<p><span style="color:#4f7a28"><span style="font-size:small"><strong>Talent Acquisition Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br>
<span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br>
<span style="color: #0b5397; font-weight:bold">Website:</span> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>



</body>
</html>
