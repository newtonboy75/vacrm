<html>
<head><title>Thank You Email</title>
<style type="text/css" rel="stylesheet">
.wrapper{
margin: 50px;

text-align:center;
font-family: arial;
}

.button{
text-align: center;
width: 30%;
padding: 20px 40px;
background-color: #f0f0f0;
text-decoration: none;
color: #fff;
background-color: #20a8d8;
border-color: #20a8d8;
}
</style>
</head>
<body>

<div class="wrapper">
<img src="https://virtudeskpro.com/img/backend/brand/virtudesk_logo.png"><p>&nbsp;</p>
<h3>Hi {{$applicant_name}}</h3>
<p>{{$contract_type}} (#{{$id}}) has been signed and ready to download. Please see file attachment.</p>
<p>&nbsp;</p><p>&nbsp;</p>
You can also go to <a href="{{$landing_url}}">{{$landing_url}}</a> to view the contract.
</div>
  <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
  <p><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br>
  <span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br>
  <span style="color: #0b5397; font-weight:bold">Website:</span> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>
</body>
</html>
