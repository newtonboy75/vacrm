<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><strong>Good Day Applicant!</strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">We&rsquo;re writing to inform you of the result of the recently conducted home evaluation.&nbsp;</span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">After careful assessment of your declared systems and information, we would like to say:</span></span></span></p>

<p style="text-align:center"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>CONGRATULATIONS!&nbsp;</strong></span></span></span></p>

<p style="text-align:center"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#38761d"><strong>You Passed the Home Evaluation Process!</strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">To jumpstart your career with us, we have provided some instructions and a list of requirements on this email that you would need to accomplish within </span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><strong>24 hours </strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">from the receipt of this email.&nbsp;</span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Please read and follow the instructions carefully. </span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff2600"><em>At this point, your ability to follow and comprehend instructions are already being assessed.&nbsp;</em></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Here&rsquo;s a list of requirements to be completed within 24 hours from the time this email has been submitted, so we can include you in the next available training class:</span></span></span></p>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><strong><em>Immediate Requirements (To be passed within 24 hours)</em></strong></span></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Add our Facebook Account and our official Facebook Page &amp; send us a screenshot that you have accomplished this:</span></span></span>

	<ul>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Marcelina Pablo (Facebook Account) </span></span></span><a href="https://www.facebook.com/marcelina.pablo.946" style="text-decoration:none"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#1155cc"><u>https://www.facebook.com/marcelina.pablo.946</u></span></span></span></a></li>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Virtudesk Recruitment - PH (Facebook Page) </span></span></span><a href="https://www.facebook.com/virtudeskph/" style="text-decoration:none"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#1155cc"><span style="background-color:#ffffff"><u>https://www.facebook.com/virtudeskph/</u></span></span></span></span></a></li>
	</ul>
	</li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Create your Official Virtudesk Skype &amp; Gmail Accounts.&nbsp;</span></span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><strong>Virtudesk Gmail</strong></span></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Go to Gmail and create a Virtudesk Gmail account.&nbsp;</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">For your Gmail address please follow this format:</span></span></span>
	<ul>
		<li style="list-style-type:circle"><a href="mailto:virtudesk.firstname.lastname@gmail.com" style="text-decoration:none"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#1155cc"><u>virtudesk.firstname.lastname@gmail.com</u></span></span></span></a></li>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Example: </span></span></span><a href="mailto:virtudesk.sarah.fuller@gmail.com" style="text-decoration:none"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#1155cc"><u>virtudesk.sarah.fuller@gmail.com</u></span></span></span></a></li>
	</ul>
	</li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><strong>Virtudesk Skype</strong></span></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Go to skype.com and create a Virtudesk Skype Account.</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">When creating your Skype account, please use the option &ldquo;Use your email instead&rdquo;.</span></span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><img src="https://lh3.googleusercontent.com/99ejksD7Em4J_nxAjAemPK-Zd5W85o8cxaWB8cKh7fJUnf_2tm1c2uVl5t6Hvx3jdxj1dGZUkJt-i3P6i1iRV8eWaMluEmKf6_rV997KnThv1jCbnqNaaFxp4DNml6e3QVr6P1aH" style="width:100%" /></span></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Use the Virtudesk Gmail account you created as your email for Skype. Then create your password.</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Fill up this Virtudesk Applicant Information Sheet:&nbsp;</span></span></span>
	<ul>
		<li style="list-style-type:circle"><a href="https://drive.google.com/open?id=1LVLIzU7b-S78opQzax88vt675ig68Z4Snk3BGIELRto" style="text-decoration:none"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#1155cc"><u>https://drive.google.com/open?id=1LVLIzU7b-S78opQzax88vt675ig68Z4Snk3BGIELRto</u></span></span></span></a></li>
		<li style="list-style-type:circle"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Please make sure to provide all the necessary details being asked on the form.</span></span></span></li>
	</ul>
	</li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Recent 2x2 ID Picture</span></span></span></li>
</ul>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff2600"><strong><em>Submit all of these requirements by clicking </em></strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#0000ff"><strong><em><a href="{{config('app.vdesk_url')}}/application_progress"><u>here</u></a>. </em></strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong><em>Requirements should be sent </em></strong></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff2600"><strong><em>within 24 hours upon receipt of this email. Failure to submit these requirements on time may lead to your application being forfeited.</em></strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><strong><em>To Follow Requirements (May be sent beyond the 24-hour time frame)</em></strong></span></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Security Bank Account (This can be passed once you get your client)</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">NBI Clearance</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Signed Virtudesk Disclosure Form - </span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong><em>this will be sent to your personal email account before the start of your training.</em></strong></span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Signed Non-Compete Clause Form- </span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong><em>this will be sent to your personal email account before the start of your training.</em></strong></span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">Signed Virtudesk Contract - </span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong><em>this will be sent to your personal email account before the start of your training.</em></strong></span></span></span></li>
</ul>

<p style="margin-left:48px"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000">==================================================================</span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><em>Once you have submitted all the immediate requirements, please wait for our requirement team&rsquo;s email regarding your training schedule. </em></span></span></span><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><strong><em>This is normally sent a day prior to the training start date.&nbsp;</em></strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#ff2600"><strong><em>NOTE: Please do note that you will only be given a training schedule and be endorsed to a training class if you have completed ALL the Immediate Requirements on or before the deadline. Failure to submit these requirements on time will lead to your application being forfeited.</em></strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><strong>If you have any concerns regarding the requirements, please direct all your queries to our Requirements Team at </strong></span></span></span><a href="mailto:requirements@myvirtudesk.com" style="text-decoration:none"><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#1155cc"><strong><u>requirements@myvirtudesk.com</u></strong></span></span></span></a><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><strong> so they can assist you.</strong></span></span></span></p>

<p><span style="font-size:9pt"><span style="font-family:Arial"><span style="color:#000000"><em>Kindly make sure to observe the deadline provided to you and acknowledge this email.&nbsp;</em></span></span></span></p>
