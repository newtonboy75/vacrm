<html>
<head><title>Thank You Email</title></head>
<body>
  <p style="font-weight: bold;">Dear {{$new_applicant->name}},</p>

  <p>Thank you for your interest in seeking employment with our company. </p>

  <p>We have received your application successfully. Our Talent Acquisition Team will be reviewing your submitted resume and if your qualifications meet our requirements, we will be sending you an email with further instructions. </p>

  <p>You may also check the status of your application on our Recruitment Portal. To login, please use the credentials below:</p>

  <p><strong>Username:</strong> {{$new_applicant->email}}<br>
  <strong>Password:</strong> {{$new_applicant->temp_pass}}<br>
  <strong>Login Page:<a href="{{config('app.vdesk_url')}}">http://careers.myvirtudesk.ph</a></strong> </p>

  <p style="font-weight: bold;">Note: Please do not change your assigned password.</p>

  <p>We wish you the best of luck on your application!</p>

  <p><span style="color:#4f7a28"><span style="font-size:small"><strong>Sourcing Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br>
  <span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:jobs@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>jobs@myvirtudesk.com</u></span></span></a><br>
  <span style="color: #0b5397; font-weight:bold">Website:</span> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>
</body>
</html>
