<html>
<head><title></title></head>
<body>

  <p style="text-align:left"><span style="color:#0000ff"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Dear </strong></span></span></span><span style="color:#0000ff"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>{{$applicant->name ?? ''}}</strong></span></span></span><span style="color:#0000ff"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>,</strong></span></span></span>
  &nbsp;</p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">We hope you are well and thank you for sending your application.</span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">We are happy to inform you that you&rsquo;ve been shortlisted as one of our candidates for the Virtual Assistant position and we would like to invite you for an interview.</span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Interview details are provided below together with some instructions we would need you to follow. </span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Follow the steps below</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> and don&rsquo;t worry, we will guide you every step of the way and we promise, you won&rsquo;t get lost. </span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">-----------------------------------------------</span></span></span></p>

  <p style="text-align:left"><span style="color:#6aa84f"><span style="font-family:Arial,serif"><span style="font-size:medium"><em><strong>Interview Instructions: (Please read carefully)</strong></em></span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Step 1: </strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">You will enter the first level of our recruitment process which is the one on one </span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>SVI </strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">or </span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><em><strong>Skype Video Interview.</strong></em></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> Please take note that for this interview, </span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>a working web camera and headset are required</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> as stated on the system requirements on our website. </span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Take note of your interview schedule below:</span></span></span><br><br>

  <span style="color:#ff0000"><span style="font-family:Arial,serif; text-transform: uppercase !important;"><span style="font-size:medium"><strong>Interview Date (MNL): {{isset($interview) ? strtoupper(date('F d, Y', strtotime($interview->interview_record[0]->date))) : 'please save changes and refresh page'}} </strong></span></span></span><br>
  <span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:medium"><strong>Time: {{isset($interview) ? date('g:i A', strtotime($interview->interview_record[0]->date)) : 'please save changes and refresh page'}} MANILA TIME </strong></span></span></span><br>
  <span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:medium"><strong>Interviewer: </strong></span></span></span><span style="color:#ff0000"><span style="font-family:Arial,serif">
    @php
    $recruiter_name = '';
    $recruiter_email = '';
    if($applicant->recruiter_name){
      if($applicant->recruiter_name->first_name != ''){
        $recruiter_name = $applicant->recruiter_name->first_name;
        $recruiter_email = $applicant->recruiter_name->email;
      }

      if($applicant->recruiter_name->last_name != ''){
        $recruiter_name .= ' '.$applicant->recruiter_name->last_name;
        $recruiter_email = $applicant->recruiter_name->email;
      }

      if($applicant->recruiter_name->id == '12'){
        $recruiter_name = $applicant->recruiter_name->first_name;
        $recruiter_email = 'jobs@myvirtudesk.com';
      }
    }

    @endphp
  <span style="font-size:medium"><strong>{{$applicant->recruiter_name->first_name ?? 'please choose a recruiter' }} {{$applicant->recruiter_name->last_name ?? 'please choose a recruiter' }}</strong></span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Step 2:</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> Kindly use your personal computer or laptop during the interview. Using another person&#39;s pc/laptop and/or having your interview at a computer shop/internet cafe </span></span></span><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:small"><u><strong>are not allowed.</strong></u></span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Step 3: </strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Please </span></span></span><span style="color:#ff9900"><span style="font-family:Arial,serif"><span style="font-size:small"><em><strong>add your interviewer on Skype on the day of your scheduled interview.</strong></em></span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:medium"><strong>Skype ID account:</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:medium">&nbsp;</span></span></span><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:medium"><strong>{{$applicant->recruiter_name->skype_id ?? 'please choose a recruiter' }} or </strong></span></span></span><a href="mailto:{{$recruiter_email ?? 'please choose a recruiter' }}" style="color:#0000ff; text-decoration:underline" target="_blank"><span style="color:#0000ff"><span style="font-family:Arial,serif"><span style="font-size:medium"><u><strong>{{ $recruiter_email ?? 'please choose a recruiter' }}</strong></u></span></span></span></a><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:medium"><strong> </strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:medium"><strong>or</strong></span></span></span><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:medium"><strong> {{ $recruiter_name ?? 'please choose a recruiter' }} | Talent Acquisition</strong></span></span></span></p>

  <p style="text-align:left"><span style="color:#ff9900"><span style="font-family:Arial,serif"><span style="font-size:small"><em><strong>NOTE: ONLY ADD THE RECRUITER ON THE DAY OF YOUR INTERVIEW</strong></em></span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Step 4:</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> Please make sure to be at home on your workstation and use the system that you will be using if ever you get hired. A systems&#39; check will be conducted during the initial stages of the recruitment process.</span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Helvetica,serif"><span style="font-size:small"><strong>Step 5</strong></span></span></span><span style="color:#000000"><span style="font-family:Helvetica,serif"><span style="font-size:small">: </span></span></span><span style="color:#ff0000"><span style="font-family:Helvetica,serif"><span style="font-size:small"><u><strong>VERY IMPORTANT</strong></u></span></span></span><span style="color:#000000"><span style="font-family:Helvetica,serif"><span style="font-size:small">, prepare 3 contacts of your character references before your initial interview. Inform your character references ahead of time that we will call them once you passed the initial interview and examination. We can only accept your Direct Manager/ HR/Team Lead as your references. </span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Helvetica,serif"><span style="font-size:small">Below are the information needed: </span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Helvetica,serif"><span style="font-size:small">Name: </span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Helvetica,serif"><span style="font-size:small">Position: </span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Helvetica,serif"><span style="font-size:small">Company: </span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Helvetica,serif"><span style="font-size:small">Contact Number: </span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Helvetica,serif"><span style="font-size:small">Email Address:</span></span></span><span style="color:#000000"><span style="font-family:Times New Roman,serif"><span style="font-size:medium">&nbsp;&nbsp;</span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Step 6:</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"> </span></span><span style="color:#ff9900"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Crucial part of all,</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> please </span></span></span><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>add your interviewer 3 hours BEFORE</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> your scheduled time. On the day of the interview, once you have added your interviewer, </span></span></span><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>immediately message</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> him/her with your full name, email address and your interview schedule. He/she will send you a message with the initial interview instructions.</span></span></span><span style="font-family:Times New Roman,serif"><span style="font-size:medium">&nbsp;&nbsp;</span></span></p>

  <p style="text-align:left"><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>If you don&rsquo;t send your interviewer a message once you are online, he/she will not send you any details and will not call you for the interview. Please make sure to send him/her a message that you are already online.</strong></span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">-------------------------------------------------</span></span></span></p>

  <p style="text-align:left"><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Something to remember:</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> During the initial interview and while working with us, you should secure a location conducive for work without any distractions and should meet the company’s system requirements:</span></span></span></p>

  <ul>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Computer Processor: at least i3, 64 bit OS (for both main and backup system)</span></span></span></p>
  	</li>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Computer memory/RAM: at least running 4.00 GB (for both main and backup system)</span></span></span></p>
  	</li>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Computer OS: Windows 10</span></span></span></p>
  	</li>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Primary Internet Connection: running on a 5mbps plan or higher. Preferably wired connection. <strong>(Prepaid Wifi/LTE connections are not allowed)</strong></span></span></span></p>
  	</li>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Backup Internet Connection: running on 5mbps. Broadband sticks and such</span></span></span></p>
    </li>
  </ul>

<ul>
	<li>
	<p style="text-align:left"><strong><span style="color:red">Additional Requirements for Mac Users</span></strong><br />
	<br />
	<strong>&nbsp; A.</strong>&nbsp;Accepted MAC OS - High Sierra, Catalina , and Big Sur <strong>ONLY</strong><strong>.</strong></p>

	<p style="text-align:left"><strong>&nbsp; </strong><strong>B.</strong>&nbsp;Those running on High Sierra - version 10.13.4 is ONLY accepted.</p>

	<p style="text-align:left"><strong>&nbsp; C.</strong>&nbsp;Those running on Catalina- version 10.15.5 is ONLY accepted.</p>

	<p style="text-align:left"><strong>&nbsp; D.</strong>&nbsp;Those running on Big Sur - version 11.1 is ONLY accepted.</p>

	<p style="text-align:left"><strong>&nbsp; E.</strong>&nbsp;Those running on any other OS or Versions other than mentioned above - please update your OS to any of the above mentioned accepted OS &amp; Versions.</p>
	</li>
</ul>

  <p style="text-align:left"><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:small"><em>If your system does not meet the requirements above, please let your assigned Recruiter know immediately. </em></span></span></span></p>

  <p style="text-align:left"><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Final Reminders:</strong></span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">This is still a job interview and we want to make sure that you are on your best. Kindly follow these guidelines and make sure that you are all set before the recruiter calls you:</span></span></span></p>

  <ul>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Location.</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> Make sure to have a clear background. Make sure you are in a place where no one can walk behind you and no distracting backgrounds can be seen (ex. kitchen, dining tables, sofa, bed, people sleeping, restroom, etc.) </span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><em><strong>Most importantly, make sure that you are at home and you are using the system that you will be using if ever you will work for us.</strong></em></span></span></span></p>
  	</li>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Dress for success.</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> When applying for a job, you have to present yourself well. You have to look professional and be professional. Wearing of sandos, tubes, sleep wear, nighties, t-shirts and other revealing clothes are not professional and are not suitable for job interviews. Polo, polo shirts, collared blouse, blazers, suits, long sleeves are acceptable. Make sure to wear something decent and formal, just like going to a personal job interview. Dressing up for the occasion is a big plus as this represents your personality and your willingness for the job.</span></span></span></p>
  	</li>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Appearance. </strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Make sure to look professional on your video. Comb your hair and put some light makeup for the ladies.</span></span></span></p>
  	</li>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Expression.</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> Smile at the camera and always make sure to look straight into the camera all throughout the interview. Remember, this is still a person you are talking to so you need to look straight into the camera as if you&#39;re talking and looking to the eyes of your interviewer in person.</span></span></span></p>
  	</li>
  	<li>
  	<p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Lighting.</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> Make sure that you have adequate lighting during your interview. You want to make sure that the interviewer would see your face and expression while talking. A poorly lighted environment is not recommended for your interview. Also, make sure that your camera&#39;s setting is adjusted so that your video will not be blurred and make sure that you are at the center of the video rather than on the side.</span></span></span></p>
  	</li>
  </ul>

  <p style="text-align:left"><span style="color:#ff0000"><span style="font-family:Arial,serif"><span style="font-size:small"><em><strong>NOTE: AT THIS POINT, YOU ARE NOW BEING ASSESSED ON HOW YOU FOLLOW INSTRUCTIONS.</strong></em></span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Thank you and kindly acknowledge receipt of this email.</strong></span></span></span><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"> We are looking forward to your interview.</span></span></span></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small">Be yourself and just enjoy your interview. Smile!</span></span></span></p>

  <p style="text-align:left"><span style="font-family:Arial,serif"><span style="font-size:small">Have a great day ahead!</span></span></p>

  <p style="text-align:left">&nbsp;</p>

  <p style="text-align:left"><span style="color:#ff9900"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Sourcing &amp; Talent Acquisition Team&nbsp;</strong></span></span></span><span style="color:#38761d"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>| Virtudesk</strong></span></span></span></p>

  <p style="text-align:left"><span style="color:#0b5394"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Email:&nbsp;</strong></span></span></span><span style="color:#0000ff"><span style="font-family:Arial,serif"><span style="font-size:small"><u><a href="mailto:jobs@myvirtudesk.ph" style="color:#0000ff; text-decoration:underline" target="_blank">jobs@myvirtudes</a><a href="http://k.com/" style="color:#0000ff; text-decoration:underline" target="_blank">k.com</a></u></span></span></span></p>

  <p style="text-align:left"><span style="color:#0b5394"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>Website:</strong></span></span></span><span style="color:#0b5394"><span style="font-family:Arial,serif"><span style="font-size:small">&nbsp;</span></span></span><a href="https://myvirtudesk.ph/" style="color:#0000ff; text-decoration:underline" target="_blank"><span style="color:#1155cc"><span style="font-family:Arial,serif"><span style="font-size:small"><u>https://myvirtudesk.ph</u></span></span></span></a></p>

  <p style="text-align:left"><span style="color:#000000"><span style="font-family:Arial,serif"><span style="font-size:small"><strong>-------------------------------------------------------------------------</strong></span></span></span></p>

  <p style="text-align:left"><strong><span style="color:#e74c3c">Note:</span><span style="color:#3498db">&nbsp;</span><span style="color:#0000ff">Please be guided by our Office Hours: Tuesdays thru Saturdays 8:00 AM to 5:00 PM Manila Standard Time. All emails and Skype messages received beyond working hours will be answered on the next business day. Initial interviews are conducted Tuesdays thru Saturdays 09:00 AM to 06:00 PM Manila Standard Time. Sunday &amp; Monday 09:00 AM to 9:00 PM Manila Standard Time.</span></strong></p>

</body>
</html>
