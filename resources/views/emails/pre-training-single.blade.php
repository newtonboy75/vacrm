<html>
<head><title></title></head>
<body>
  <p style="font-weight: bold;">Dear {{$applicant->name ?? ''}}, </p>

  <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>Good day!</strong></span></span></span></p>

  <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">We would like to thank you for submitting your requirements.</span></span></span></p>

  <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong><em>Congratulations! </em></strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#38761d"><strong><em>You will now be endorsed to Training!</em></strong></span></span></span></p>

  <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Please do take note of the following details regarding your training:</span></span></span></p>

  <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#2980b9"><strong>Training Date (Manila):</strong> {{ $start_date }} </span></span></span></p>

  <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#2980b9"><strong>Training Time (Manila):</strong> {{ $end_date }}</span></span></span></p>

  <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Our Learning &amp; Communications Team will be sending you another email a couple of days OR a day before your scheduled training date for specific instructions.</span></span></span></p>

  <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">You will also have your New Hire Orientation a day before your training date stated above so please mark your calendars. You will receive another email in the next coming days regarding your New Hire Orientation &amp; Training Proper. Kindly check your personal and virtudesk email accounts&#39; inboxes and spam folders.</span></span></span></p>

  <p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Should you have queries, do not hesitate to contact us at </span></span></span><a href="mailto:requirements@myvirtudesk.com" style="text-decoration:none"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#1155cc"><u>requirements@myvirtudesk.com</u></span></span></span></a></p>

  <p><span style="color:#4f7a28"><span style="font-size:small"><strong>Virtudesk Requirements Team </strong></span></span><span style="font-size:small"><strong>| </strong></span><span style="color:#ff9300"><span style="font-size:small"><strong>Virtudesk PH</strong></span></span><br />
    <span style="color:#0b5394"><span style="font-size:small"><strong>Email: </strong></span></span><a href="mailto:requirements@myvirtudesk.com"><span style="color:#1155cc"><span style="font-size:small"><u>requirements@myvirtudesk.com</u></span></span></a><br />
    <strong>Website:</strong> <a href="myvirtudesk.ph">myvirtudesk.ph</a></p>
