<p><span style="font-size:13px"><strong>Hi {{$applicant->name ?? ''}}, </strong></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Before anything else, We&rsquo;d like to thank you for taking time to apply to us.In line with the recent interview you have had, we are sending you this email to let you know of the result.&nbsp;</span></span></span></p>

<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#ff9900"><strong>CONGRATULATIONS! </strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#6aa84f"><strong>You passed the interview!&nbsp;</strong></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">The Next part of the Recruitment process would now be the Background &amp; Reference Check.&nbsp;</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Here at Virtudesk, we want to make sure that all of our employees have the highest level of integrity, work ethics and we need to make sure that they are ready and well-equipped for the job. We need to make sure that all of our employees will uphold privacy and give their 100% commitment to the company.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">In this next step of the Recruitment Process we will be validating all the information you have provided to use on your Resume and during the Interviews you have had with us. This means, </span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>all information on your application and resume</strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"> will be validated for authenticity as part of our Employee Compliance. This includes but is not limited to contacting your previous employers/company/Clients, validating your declared personal &amp; professional information, and such.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Apart from validating the information you have provided us, we will also be asking for 3 character references from you as part of the process. These 3 Character References will be contacted separately to validate and authenticate any of your declared information. Instructions &amp; guidelines are posted below.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Please click </span></span></span><a href="https://careers.myvirtudesk.ph/application_progress" style="text-decoration:none"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#1155cc"><strong><u>here</u></strong></span></span></span></a><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"> to be directed to the Reference Check Form.</span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong> </strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Kindly complete the form and click submit once done. Choose a returning applicant. Use your login credentials. Login credentials were given to all the applicants when they initially submitted their application.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong>NOTE: At this point, your ability to follow instructions and to meet deadlines are already being assessed.</strong></span></span></span></p>

<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Deadline for submission of this form would be </span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>within 2 hours </strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">from the time this email was sent. </span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong>Failure to submit this form on time will&nbsp; lead to your application being forfeited.</strong></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>GUIDELINES IN FILLING UP THE REFERENCE FORM: (Please read the guidelines carefully before filling out the form)</strong></span></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">We need a total of 3 references from the applicant.</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong>IMPORTANT:</strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"> The first reference should be someone from the </span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong>last company you were employed with that you declared on your CV</strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"> upon application with Virtudesk. Failure to provide any reference from your last declared employer on your application and on the interview will already lead for your application to be forfeited.</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">The other 2 references can be from the other companies you&rsquo;ve been with in the past.</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">The email address of your reference could either be their work email or their personal email address.</span></span></span></li>
</ul>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">The only references we consider to be as valid would be the following:</span></span></span></p>

<ul>
	<li style="list-style-type:disc"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Previous Supervisor/TL or Manager that you have worked with directly.</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Previous Client that you have worked for directly. (This is for those who have freelancing experience)</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">HR Managers/HR Personnel from their previous company.</span></span></span></li>
	<li style="list-style-type:disc"><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Friends, relatives, teammates, colleagues </span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>are not allowed</strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"> to be declared as a reference.</span></span></span></li>
</ul>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">To make the reference check faster and more seamless, please make sure to give your listed references a heads up that we will be contacting them either via call or email during the preferred time you have indicated on the form.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>DISCLAIMER: By submitting your References on the form, you are accepting all the terms and conditions placed on this email. Again, failure to submit your references within 2 hours from the receipt of this email will already lead for your application to be forfeited.</strong></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">Once we have received your References, our Background Screening Team will conduct the background &amp; reference check.&nbsp;</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">The whole process of background check and verification takes approximately 2-4 days to complete; however, </span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#ff0000"><strong><em>this may vary depending if your reference and your previous client/company&nbsp; are answering our calls or emails.</em></strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"> We will notify you via email if we weren&rsquo;t able to contact them in any means possible and we would appreciate your response so we can move forward with the process smoothly.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000">We will ask for your patience as we conduct our company process.&nbsp;</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#4f7a28"><strong>Sourcing &amp; Talent Acquisition Team</strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong> | </strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#ff9300"><strong>Virtudesk</strong></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>Email: </strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#1155cc"><strong><u>jobs@myvirtudesk.com</u></strong></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>Website: <a href="https://myvirtudesk.ph">myvirtudesk.ph</a></strong></span></span></span></p>

<p>&nbsp;</p>

<hr />
<p><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#c0392b"><strong>Note</strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#000000"><strong>: </strong></span></span></span><span style="font-size:11pt"><span style="font-family:Arial"><span style="color:#2980b9">Please be guided by our Recruitment Office Hours: Tuesdays thru Saturdays - 7:00 AM to 3:00 PM Manila Standard Time. Initial interviews are conducted from 6:00 AM- 6:00 PM Manila Standard Time. All emails received beyond working hours will be answered on the next business day.</span></span></span></p>

<p>&nbsp;</p>

<p>&nbsp;</p>
