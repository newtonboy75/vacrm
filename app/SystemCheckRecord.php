<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SystemCheckRecord extends Model
{
    protected $table = 'system_check';
    protected $fillable = ['home_eval_id', 'type_computer', 'power_source_main', 'power_source_backup', 'main_pc_processor', 'backup_pc_ram', 'main_ram_proc_type', 'backup_ram_proc_type', 'isp', 'connection_owner', 'other_connection_owner', 'type_connection', 'speedtest_main', 'speedtest_backup','power_source_backup_length', 'internet_backup', 'main_power_transition', 'main_connection_transition', 'num_internet_shared', 'web_camera', 'web_camera_brand', 'backup_owner_name', 'how_long_backup_system', 'main_speed_plan', 'eta', 'dl1', 'dl2', 'dl3', 'ul1', 'ul2', 'ul3', 'speedtest_link1', 'speedtest_link2', 'speedtest_link3', 'backup_dl1', 'backup_dl2', 'backup_dl3', 'backup_ul1', 'backup_ul2', 'backup_ul3', 'backup_speedtest_link1', 'backup_speedtest_link2', 'backup_speedtest_link3', 'backup_isp', 'how_long', 'isp_owner_2', 'backup_power_source', 'owner_backup_power_owner_name', 'backup_power_how_long', 'backup_system_proc', 'main_system_proc', 'main_system_ram', 'backup_system_ram', 'account_owner_relation', 'type_connection_main', 'availability_backup_source', 'owner_backup_source', 'isp_backup_availabilitiy', 'connection_owner2', 'upgrade_internet', 'owner_backup_relation', 'owner_backup', 'backup_availability', 'type_computer_backup', 'headset', 'headset_brand', 'backup_system_os', 'main_system_os'];
    public $timestamps = false;


}
