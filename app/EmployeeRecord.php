<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmployeeRecord extends Model
{
    protected $table = 'employee';
    protected $fillable = ['applicant_id', 'lob', 'created_at'];
    public $timestamps = false;

    public function info(){
      return  $this->hasOne('App\ApplicantRecord', 'id', 'applicant_id');
    }

    public function group(){
      return  $this->hasMany('App\FosUserGroup', 'user_id', 'applicant_id');
    }

}
