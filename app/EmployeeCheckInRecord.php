<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmployeeCheckInRecord extends Model
{
    protected $table = 'employee_checkin_records';
    protected $fillable = ['employee_id', 'checkin_id'];
    public $timestamps = false;

}
