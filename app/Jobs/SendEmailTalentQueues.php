<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueSourcing as EmailQueueSourcing;
use Mail;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SendEmailTalentQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $new_request;
    protected $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($new_request, $type)
    {
        //
        $this->new_request = $new_request;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

      $request = $this->new_request;
      $to = $request['to'];


      if($this->type == 'interview_passed'){
        $body = $request['email_body'];
        $subject = 'IMPORTANT: PLEASE READ THE WHOLE EMAIL - Virtudesk - Interview Result';
        $from = 'virtudesk.referenceteam@gmail.com';
        
        try{
        Mail::send([], [], function ($message) use($to, $body, $from, $subject){
          $message->to($to)
          ->from($from, 'MyVirtudesk')
          ->cc($from, 'MyVirtudesk')
          ->subject($subject)
          ->replyTo('virtudesk.referenceteam@gmail.com')
          ->setBody($body, 'text/html');
        });

        if (count(Mail::failures()) > 0 ) {
          $success = '0';
          $status = 'failed';
        }
        }catch(\Exception $e){
            $status = 'failed';
        }

        $applicant_record = ApplicantRecord::whereId($request['user_id'])->first();

        if($applicant_record){
          $applicant_record->workflow_status = 'reference_check';
          $applicant_record->save();
          $success = '1';
          unset($applicant_record);
        }


      }elseif($this->type == 'resched'){
        $body = $request['body'];
        $subject = 'Applicant Interview Date Rescheduled';
        
        try{
        Mail::send([], [], function ($message) use($to, $body, $subject){
          $message->to($to)
            ->subject($subject)
            ->from('jobs@myvirtudesk.com', 'MyVirtudesk Talent Acquisition')
            ->cc('jobs@myvirtudesk.com')
            ->replyTo('jobs@myvirtudesk.com')
            ->setBody($body, 'text/html');
        });
        
        }catch(\Exception $e){
            $status = 'failed';
        }

      }else{
        $body = $request['email_body'];
        $subject = 'Virtudesk Application | Application Result';
        
        try{
        Mail::send([], [], function ($message) use($to, $body, $subject){
          $message->to($to)
          ->from('jobs@myvirtudesk.com', 'MyVirtudesk')
          ->cc('jobs@myvirtudesk.com', 'MyVirtudesk')
          ->subject($subject)
          ->setBody($body, 'text/html');
        });

        if (count(Mail::failures()) > 0 ) {
          $success = '0';
        }
        
        }catch(\Exception $e){
            $status = 'failed';
        }

        $applicant_record = ApplicantRecord::whereId($request['user_id'])->first();

        if($applicant_record){
          $applicant_record->archived = '1';
          $applicant_record->save();
          $success = '1';
          unset($applicant_record);
        }

      }
      
      unset($applicant_record);
      unset($this->new_request);
      
      app('queue.worker')->shouldQuit  = 1;


    }
}
