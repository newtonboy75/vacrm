<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueContracts as EmailQueueContracts;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SendEmailEmployerContractsQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $landing_url;
    protected $employee_name;
    protected $contract_types;
    protected $contract_id;
    protected $email_use;
    protected $pathToFile;
    protected $display;
    protected $from;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($landing_url, $employee_name, $contract_types, $email_use, $contract_id, $pathToFile, $display, $from)
    {
        $this->landing_url = $landing_url;
        $this->employee_name = $employee_name;
        $this->contract_types = $contract_types;
        $this->email_use = $email_use;
        $this->contract_id = $contract_id;
        $this->pathToFile = $pathToFile;
        $this->display = $display;
        $this->from = $from;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $landing_url = $this->landing_url;
        $employee_name = $this->employee_name;
        $contract_types = $this->contract_types;
        $email_use = $this->email_use;
        $contract_id = $this->contract_id;
        $pathToFile = $this->pathToFile;
        $display = $this->display;
        $from = $this->from;
        $to_emails = [trim($email_use), 'talentacquisition@myvirtudesk.com'];
        //$to_emails = 'talentacquisition@myvirtudesk.com';

       try{
       Mail::send('emails.esig_user_copy', ['landing_url' => $landing_url, 'applicant_name' => ucwords($employee_name), 'id' => $contract_id, 'contract_type' => $contract_types], function ($m) use ($contract_types, $employee_name, $pathToFile, $display, $email_use, $from, $to_emails) {
        $m->from($from, 'MyVirtudesk');
        $m->to($to_emails)
        ->attach($pathToFile, ['as' => $display, 'mime' => 'application/pdf'])
        ->subject($contract_types.' has been signed');
      });
      
       if (count(Mail::failures()) > 0 ) {
        $success = '0';
        $status = 'failed';
        exit;
      }

      }catch(\Exception $e){
          $status = '1';
          exit;
     }
     
        unset($landing_url);
        unset($employee_name);
        unset($contract_types);
        unset($email_use);
        unset($contract_id);
        unset($pathToFile);
        unset($display);
        unset($from);
       //gc_collect_cycles();
        app('queue.worker')->shouldQuit  = 1;
        exit;
    }
}
