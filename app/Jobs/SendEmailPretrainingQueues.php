<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueSourcing as EmailQueueSourcing;
use Mail;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;


class SendEmailPretrainingQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $new_request;
    protected $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($applicant, $type_training)
    {
        //
        $this->applicant = $applicant;
        $this->type_training = $type_training;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

      $applicant = $this->applicant;
      $type_training = $this->type_training;
      
      try{
      Mail::send('emails.pre-training-single', ['applicant' => $applicant, 'date' => $date, 'training' => $type_training], function ($m) use ($applicant) {
        $m->from('requirements@myvirtudesk.com', 'MyVirtudesk');
        $m->cc('requirements@myvirtudesk.com', 'MyVirtudesk');
        $m->replyTo('requirements@myvirtudesk.com');
        $m->to($applicant->email, $applicant->name)
        ->subject('Pre-training Requirements | Virtudesk Requirements Team');
      });

      if (count(Mail::failures()) > 0 ) {
        $success = '0';
        $status = 'failed';
      }

    }catch(\Exception $e){
          $status = 'failed';
     }
     
     unset($applicant);
     unset($type_training);
     
     app('queue.worker')->shouldQuit  = 1;


    }
}
