<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueNewApplicant as EmailQueueNewApplicant;
use Mail;

class SendEmailQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $new_applicant;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($new_applicant)
    {
        //
        $this->new_applicant = $new_applicant;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $html = 'A new application (ID#'.$this->new_applicant->id.') was submitted. Please login to CRM to check the status';
      $to_ = 'jobs@myvirtudesk.com';
      $newapplicant = $this->new_applicant;
    
    
        try{
        Mail::send('emails.thank-you-email', ['new_applicant' => $newapplicant], function ($m) use ($newapplicant) {
        $m->from('jobs@myvirtudesk.com', 'MyVirtudesk');
        $m->to($newapplicant->email, $newapplicant->name)->subject('Virtudesk Application | We’ve received your application');
        $m->replyTo('jobs@myvirtudesk.com');
        });
       }catch(\Exception $e){
          $status = 'failed';
          exit;
     }
        
        try{
      Mail::send([], [], function ($message) use($to_, $html){
        $message->to($to_)
          ->subject('New job application submitted')
          ->from('jobs@myvirtudesk.com', 'MyVirtudesk Talent Acquisition')
          ->setBody($html, 'text/html');
      });
       }catch(\Exception $e){
          $status = 'failed';
          exit;
     }
      app('queue.worker')->shouldQuit  = 1;
      unset($this->new_applicant);
      //gc_collect_cycles();
      exit;
      

    }
}
