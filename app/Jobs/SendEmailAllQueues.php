<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueSourcing as EmailQueueSourcing;
use Mail;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;


class SendEmailAllQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $new_request;
    protected $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($new_request)
    {
        //
        $this->new_request = $new_request;
        //$this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      //$email = new EmailQueueNewApplicant();
      //Mail::to($this->new_applicant->email)->send($email);
      $request = $this->new_request;
      //Log::info($request);
      $success = '0';
      $to = $request['to'];
      $html = $request['body'];
      $subject = $request['subject'];
      $from = explode('|', $request['from']);

       try{
      Mail::send([], [], function ($message) use($to, $html, $subject, $from){
        $message->to($to)
        ->subject($subject)
        ->cc($from[0])
        ->from($from[0], $from[1])
        ->replyTo($from[0])
        ->setBody($html, 'text/html');
      });

      if (count(Mail::failures()) > 0 ) {
        $success = '0';
        $status = 'failed';
        exit;
      }

      }catch(\Exception $e){
          $status = 'failed';
          exit;
     }

     unset($html);
     unset($this->new_request);
     unset($request);
     //gc_collect_cycles();
     app('queue.worker')->shouldQuit  = 1;

    }
}
