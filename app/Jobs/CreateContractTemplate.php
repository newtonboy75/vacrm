<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

use Illuminate\Contracts\Queue\ShouldQueue;
use App\EmployeeContract;
use App\EmployeeContractActions;
use App\ApplicantRecord;
use PDF;

class CreateContractTemplate implements  ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $fname;
    private $template_type;


    public function __construct($fname, $template_type)
    {
        $this->fname = $fname;
        $this->template_type = $template_type;
    }

    public function handle()
    {
        $doc = null;

    	if(null !== $this->fname){
      		$file = public_path().'/form_templates/'.$this->fname;
    	}else{
      		$file = public_path().'/form_templates/templates/'.$this->template_type;
    	}

    	if(file_exists($file)){
      		if ($fh = fopen($file, 'r')) {
        		while (!feof($fh)) {
          			$doc .= fgets($fh);
        		}
        		fclose($fh);
        		
        		unset($this->fname);
                unset($this->template_type);
                unset($file);
        	    //gc_collect_cycles();
        		return $doc;
      		}
    	}
    }

}