<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueSourcing as EmailQueueSourcing;
use Mail;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SendEmailSourcingQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $new_request;
    protected $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($new_request, $type)
    {
        //
        $this->new_request = $new_request;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $request = $this->new_request;
      //Log::info($request);
      $success = '0';
      $to = $request['to'];
      $body = $request['email_body'];
      $ubject = '';
      $dtype = '';
      $sent = 'sent';
      $subject = $this->type == 'invitation' ? 'Virtudesk Application | Initial Interview Invitation' : 'Virtudesk Application | Application Result';

      try{
      Mail::send([], [], function ($message) use($to, $body, $subject){
        $message->to($to)
          ->from('jobs@myvirtudesk.com', 'MyVirtudesk')
          ->cc('jobs@myvirtudesk.com', 'MyVirtudesk')
          ->subject($subject)
          ->replyTo('jobs@myvirtudesk.com')
          ->setBody($body, 'text/html');
      });

      if (count(Mail::failures()) > 0 ) {
        $success = '0';
        $sent = 'failed';
      }
      
       }catch(\Exception $e){
          $sent = 'failed';
     }
      
      app('queue.worker')->shouldQuit  = 1;

    }
}
