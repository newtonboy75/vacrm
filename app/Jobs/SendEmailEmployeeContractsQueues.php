<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueContracts as EmailQueueContracts;
use Mail;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SendEmailEmployeeContractsQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $employee_name;
    protected $contract_id;
    protected $contract_types;
    protected $who;
    protected $to;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employee_name, $contract_id, $contract_types, $who, $to, $pathToFile)
    {
        //$applicant, $applicant_name, $contract_type, $new_request, $email_use, $landing_url
        $this->employee_name = $employee_name;
        $this->contract_id = $contract_id;
        $this->contract_types = $contract_types;
        $this->who = $who;
        $this->to = $to;
        $this->pathToFile = $pathToFile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

      $employee_name = $this->employee_name;
      $contract_id = $this->contract_id;
      $contract_types = $this->contract_types;
      $who = $this->who;
      $to = $this->to;
      $pathToFile = $this->pathToFile;
      
       try{
           
           Mail::send('emails.esig_admin_copy', ['applicant_name' => ucwords($employee_name), 'id' => $contract_id, 'contract_type' => $contract_types, 'who' => $who], function ($m) use ($contract_types, $to) {
            $m->from('talentacquisition@myvirtudesk.com', 'MyVirtudesk')
            ->to($to)
            ->subject($contract_types.' has been signed')
            ->attach($pathToFile, ['as' => $display, 'mime' => 'application/pdf']);
          });
          
          if (count(Mail::failures()) > 0 ) {
            $success = '0';
            $status = 'failed';
            exit;
          }

      }catch(\Exception $e){
          $status = '1';
          exit;
     }
     
        unset($employee_name);
        unset($contract_id);
        unset($contract_types);
        unset($who);
        unset($to);
        //gc_collect_cycles();
      app('queue.worker')->shouldQuit  = 1;
      exit;
    }
}
