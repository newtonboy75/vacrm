<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueSourcing as EmailQueueSourcing;
use Mail;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SendEmailTretrainingQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $new_request;
    protected $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($new_request, $type)
    {
        //
        $this->new_request = $new_request;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

      $request = $this->new_request;
      $to = $request['to'];
      $body = $request['email_body'];

      if($this->type == 'home_eval'){
          
        try{
        $subject = 'MPORTANT: PLEASE READ THE WHOLE EMAIL - Virtudesk - Background Screening  Result';
        Mail::send([], [], function ($message) use($to, $body, $subject){
          $message->to($to)
            ->from('virtudesk.referenceteam@gmail.com', 'MyVirtudesk')
            ->cc('virtudesk.referenceteam@gmail.com', 'MyVirtudesk')
            ->replyTo('virtudesk.referenceteam@gmail.com')
            ->subject($subject)
            ->setBody($body, 'text/html')
            ->attach(\Swift_Attachment::fromPath(public_path('pdf/Home_Evaluation_Form _2019V.pdf'))
            ->setFilename('Home_Evaluation_Form _2019V.pdf')
            ->setContentType('application/pdf'));
        });
        
        }catch(\Exception $e){
          $status = '1';
        }
      }else{

        $subject = 'IMPORTANT: PLEASE READ THE WHOLE EMAIL - Virtudesk - Reference Check Result';
        
        try{
        Mail::send([], [], function ($message) use($to, $body, $subject){
          $message->to($to)
            ->from('virtudesk.referenceteam@gmail.com', 'MyVirtudesk')
            ->cc('virtudesk.referenceteam@gmail.com', 'MyVirtudesk')
            ->replyTo('virtudesk.referenceteam@gmail.com')
            ->subject($subject)
            ->setBody($body, 'text/html');
        });
        
        }catch(\Exception $e){
          $status = '1';
        }
      }

      if (count(Mail::failures()) > 0 ) {
        $success = '0';
        $status = 'failed';
      }


app('queue.worker')->shouldQuit  = 1;



    }
}
