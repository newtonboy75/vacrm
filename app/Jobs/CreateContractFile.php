<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Log;


class CreateContractFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $doc;
    protected $uid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($doc, $uid)
    {

        $this->doc = $doc;
        $this->uid = $uid;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		$newfile = public_path().'/form_templates/'.$this->uid;
    	$html = $this->doc;
    	$src = '';
    	$imgData = '';

    	if(!file_exists($newfile)){
      		fopen($newfile, "a");
    	}

   		preg_match_all('/<img[^>]+>/i', $html, $imgTags);

    	for ($i = 0; $i < count($imgTags[0]); $i++) {
      		preg_match('/src="([^"]+)/i',$imgTags[0][$i], $imgage);
      		$origImageSrc[] = str_ireplace( 'src="', '',  $imgage[0]);
      		$url =  str_ireplace( 'src="', '',  $imgage[0]);

     		if(!strstr($url, 'data:image')){
        	$path =  public_path().$url;
        	$imgData = base64_encode(file_get_contents($path));

        	//convert img to base64
        	$src = 'data:'.mime_content_type($path).';base64,'.$imgData;
        	$html = str_replace($url, $src, $this->doc);
      		}
    	}

    file_put_contents($newfile, $html);
    
    unset($newfile);
    unset($html);
    unset($imgData);
    unset($src);
    unset($imgTags);
    //gc_collect_cycles();
    app('queue.worker')->shouldQuit  = 1;
    }
      
}
