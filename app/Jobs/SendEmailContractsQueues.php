<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueContracts as EmailQueueContracts;
use Mail;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class SendEmailContractsQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $applicant;
    protected $applicant_name;
    protected $new_request;
    protected $email_use;
    protected $landing_url;
    protected $mycontract;
    protected $contract1;
    protected $contract2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($applicant, $applicant_name, $new_request, $email_use, $landing_url, $mycontract, $contract1, $contract2)
    {
        //$applicant, $applicant_name, $contract_type, $new_request, $email_use, $landing_url
        $this->applicant = $applicant;
        $this->applicant_name = $applicant_name;
        $this->new_request = $new_request;
        $this->email_use = $email_use;
        $this->landing_url = $landing_url;
        $this->mycontract = $mycontract;
        $this->contract1 = $contract1;
        $this->contract2 = $contract2;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      //$email = new EmailQueueNewApplicant();
      //Mail::to($this->new_applicant->email)->send($email);
      
      

      $applicant = $this->applicant;
      $applicant_name = $this->applicant_name;
      $new_request = $this->new_request;
      $contract = $this->mycontract;
      $email_use = $this->email_use;
      $landing_url = $this->landing_url;
      $contract1 = $this->contract1;
      $contract2 = $this->contract2;

      try{
        Mail::send('emails.esig', ['applicant_name' => $applicant_name, 'contract_type' => $contract, 'landing_url' => $landing_url], function ($m) use ($applicant, $contract1, $contract2, $new_request, $email_use) {
            $m->from($contract1, 'MyVirtudesk');
            $m->replyTo($contract1);
            $m->to($email_use, $applicant->name)
            ->subject($contract2);
        });
        
        unset($this->applicant);
        unset($this->applicant_name);
        unset($this->new_request);
        unset($this->email_use);
        unset($this->landing_url);
        unset($this->mycontract);
        unset($this->contract1);
        unset($this->contract2);

      }catch(\Exception $e){
          $status = 1;
     }
     
app('queue.worker')->shouldQuit  = 1;
      
    }
}
