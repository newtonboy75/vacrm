<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\EmailQueueSourcing as EmailQueueSourcing;
use Mail;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Swift_Mailer;
use Swift_MailTransport;
use Swift_Message;


class SendEmailReferenceQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $new_request;
    protected $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($new_request, $type)
    {
        //
        $this->new_request = $new_request;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

      $request = $this->new_request;
      $to = $request['to'];
      $body = $request['email_body'];
      //$s3path = 'pdf/Home_Evaluation_Form _2019V.pdf';
      //$s3file = Storage::disk('s3')->get($s3path);

      if($this->type == 'home_eval'){
        $subject = 'MPORTANT: PLEASE READ THE WHOLE EMAIL - Virtudesk - Background Screening  Result';
        try{
        Mail::send([], [], function ($message) use($to, $body, $subject){
          $message->to($to)
            ->from('virtudesk.referenceteam@gmail.com', 'MyVirtudesk')
            ->cc('virtudesk.referenceteam@gmail.com', 'MyVirtudesk')
            ->replyTo('virtudesk.referenceteam@gmail.com')
            ->subject($subject)
            ->setBody($body, 'text/html')
            ->attach(\Swift_Attachment::fromPath('https://virtudeskcrm.s3-us-west-2.amazonaws.com/pdf/Home_Evaluation_Form+_2019V.pdf'));
            //->attach(\Swift_Attachment::fromPath(public_path('pdf/Home_Evaluation_Form _2019V.pdf'))
            //->setFilename('Home_Evaluation_Form _2019V.pdf')
            //->setContentType('application/pdf');
        });
        
         $this->new_request = null;
        app('queue.worker')->shouldQuit  = 1;        
        }catch(\Exception $e){
          $status = '1';
     }
      }elseif($this->type == 'send_ref'){

        //$applicant = ApplicantRecord::whereId($applicant_id)->first();

        $from = 'jobs@myvirtudesk.com';
        $to = 'virtudesk.referenceteam@gmail.com';
        $subject = 'New Reference Form Submitted | '.$request['name'];
        $fromName = 'Virtudesk';

        $html = '<html><head><title>Home Evaluation Send</title></head><body><p><strong>Team,</strong></p>'.$request['name'].' (' .$request['email'].') has completely submitted the Reference Check Form. <br>Please login to your account to check the details.</body></html>';
        
        try{
        Mail::send([], [], function ($message) use ($to, $from, $fromName, $html, $subject) {
         $message->to($to)
           ->subject($subject)
           ->from($from, $fromName)
           ->setBody($html, 'text/html'); // assuming text/plain
       });

       if (count(Mail::failures()) > 0 ) {
         $success = '0';
         $status = 'failed';
       }
       }catch(\Exception $e){
          $status = 'failed';
     }
      
      }else{

        $subject = 'Virtudesk Application | Application Result';
        
        try{
        Mail::send([], [], function ($message) use($to, $body, $subject){
          $message->to($to)
            ->from('virtudesk.referenceteam@gmail.com', 'MyVirtudesk')
            ->cc('virtudesk.referenceteam@gmail.com', 'MyVirtudesk')
            ->replyTo('virtudesk.referenceteam@gmail.com')
            ->subject($subject)
            ->setBody($body, 'text/html');
        });
        }catch(\Exception $e){
          $status = 'failed';
     }
        
      }

      if (count(Mail::failures()) > 0 ) {
        $success = '0';
        $status = 'failed';
      }
      
        unset($this->new_request);
        unset($this->type);
        
        app('queue.worker')->shouldQuit  = 1;



    }
}
