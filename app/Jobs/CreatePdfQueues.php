<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\EmployeeContract;
use App\EmployeeContractActions;
use App\ApplicantRecord;
use PDF;
use App\Http\Controllers\EsigSignController;

class CreatePdfQueues implements  ShouldQueue
{
   use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $contract;


    public function __construct(EmployeeContract $contract)
    {
        $this->contract = $contract; 
    }

    public function handle()
    {
        $create_pdf = $this->createPdf($this->contract);
        unset($create_pdf);
        
    }

    private function createPdf($contract)
    {
    
      $employee = ApplicantRecord::whereId($contract->employee_id)->first();
      $audit = $contract->actions;
      $esig = null;

      $hr_email = 'hr@myvirtudesk.com';
      $requirements_email = 'requirements@myvirtudesk.com';
      $hr_subject = 'Human Resource | Virtudesk';
      $requirements_subject = 'Pre-training Requirements | Virtudesk Requirements Team';

      $contract_type = [
        'caf' =>  array('Corrected Action Form', $hr_email, $hr_subject, 'HR Team'),
        'gva_quarterly' => array('GVA Quarterly', $hr_email, $hr_subject, 'HR Team'),
        'final' => array('Final', $hr_email, $hr_subject, 'HR Team'),
        'hold-suspension' => array('Hold Suspension', $hr_email, $hr_subject, 'HR Team'),
        'isa-quarterly' => array('ISA Quarterly', $hr_email, $hr_subject, 'HR Team'),
        'disclosure-form' => array('Disclosure', $hr_email, $hr_subject, 'HR Team'),
        'fte-form' => array('FTE Form', $hr_email, $hr_subject, 'HR Team'),
        'nte-form' => array('NTE Form', $hr_email, $hr_subject, 'HR Team'),
        'termination-letter' => array('Termination Letter', $hr_email, $hr_subject, 'HR Team'),
        'memorandum-form' => array('Memorandum', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'assurance-form' => array('Assurance Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'codeofconduct-form' => array('Code of Conduct', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-form' => array('ICA Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'nca-form' => array('NCA Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-eva' => array('ICA EVA', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-isa' => array('ICA ISA', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-gva' => array('ICA GVA', $requirements_email, $requirements_subject, 'Recruitment Team'),
      ];

      $doc = '<html><head><title>Contract</title>';
      $doc .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
      $doc .= '<link href="https://fonts.googleapis.com/css?family=Satisfy&display=swap" rel="stylesheet"><style>.page-break { page-break-after: always; } body{font-family: sans-serif; arial; font-size: 13px; color: #333;} #sig{ font-family: \'Satisfy\', cursive !important; font-size: 22px;}</style></head><body>';
      $doc .= '<div style="float: right;"><small>'.date('F d, Y').'</small></div><br>';
      $doc .= '<div style="width: 100%; text-align:center;"><img style="width: 18%;" src="https://virtudeskpro.com/img/backend/brand/virtudesk_logo.png"></div><p>&nbsp;</p>';

      if($contract){
        $file = public_path().'/form_templates/'.$contract->uid;
        if(file_exists($file)){
          if ($fh = fopen($file, 'r')) {
            while (!feof($fh)) {
              $doc .= fgets($fh);
            }
            fclose($fh);
          }
        }
      }

      $doc .= '<div class="page-break"></div>';
      $doc .= '<div style="width: 100%; text-align:right;"><img style="width: 6%;" src="https://virtudeskpro.com/img/backend/brand/virtudesk_logo.png"></div><p><hr style="background: gray;"></p>';
      $doc .= '<div style="font-size: 12px;"><br><strong>Electronic Signatures</strong><br><br>';
      $doc .= '<div id="sig">'.$contract_type[$contract->contract_type][3].'<br><span style="font-size: 10px; color: gray; font-family: sans-serif !important;">(digital representation of the signature)</span></div>';
      $doc .= '<br><div><strong>'.$contract_type[$contract->contract_type][3].'</strong></div>';
      $doc .= '<div style="font-size: 10px; color: gray">'.date('F d, Y h:i', strtotime($contract->sent_date)).'</div>';
      $doc .= '<div style="font-size: 10px; color: gray"><span style="font-weight: bold;">Email: </span>'.$contract_type[$contract->contract_type][1].'</div>';
      $doc .= '<br><br><div id="sig">'.ucwords($employee->name).'<br><span style="font-size: 10px; color: gray; font-family: sans-serif !important;">(digital representation of the signature)</span></div>';
      $doc .= '<br><div><strong>'.ucwords($employee->name).'</strong></div>';
      $doc .= '<div style="font-size: 10px; color: gray">'.date('F d, Y h:i', strtotime($contract->recipient_signed_date)).'</div>';
      $doc .= '<div style="font-size: 10px; color: gray"><span style="font-weight: bold;">Email: </span>'.$employee->email.'</div></div>';

      $doc .='<p>&nbsp;</p>';
      $doc .= '<div style="font-size: 12px;"><br><strong>Audit Trail</strong><br><br>';

      foreach($contract->actions as $action){

        $doc .= '<div style="font-size: 10px; font-weight: bold">'.date('F d, Y h:i a', strtotime($action->date_action)).'</div>';
        if($action->action == 'signed'){
          $doc .= '<div style="font-size: 10px;">'.ucfirst($action->action).' by '.ucwords($employee->name).'<br>Contract ID: '.$contract->contract_id.'</div><br>';
        }else{
          $doc .= '<div style="font-size: 10px;">'.ucfirst($action->action).' by '.ucwords($employee->name).'</div><br>';
        }

      }

      $doc .= '<div style="font-size: 10px; font-weight: bold">'.date('F d, Y h:i a', strtotime($contract->sent_date)).'</div>';
      $doc .= '<div style="font-size: 10px;">Contract sent by '.$contract_type[$contract->contract_type][3].'</div>';
      $doc .= '</body></html>';

      \PDF::loadHTML($doc)->save(public_path().'/form_templates/'.$contract->uid.'.pdf')->stream('download.pdf');
      
      exec('aws s3 cp '.public_path().'/form_templates/'.$contract->uid.'.pdf'.'  s3://virtudeskcrm/pdf/'.$contract->uid.'.pdf');
      
      //sleep(3);
      //if(file_exists(public_path().'/form_templates/'.$contract->uid.'.pdf')){
      //$esig = new EsigSignController();
      //$esig->send_admin_contract_confirm($contract);
      //$esig->send_employee_contract_confirm($contract);
      // unset($esig);
        //}
      
      $contract = null;
     
      unset($contract);
      unset($doc);
      unset($contract_type);
      //gc_collect_cycles();
       app('queue.worker')->shouldQuit  = 1;
    }
}