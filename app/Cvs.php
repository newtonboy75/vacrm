<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cvs extends Model
{
    protected $table = 'cvs';
    protected $fillable = ['applicant_id', 'file_name'];
    public $timestamps = false;

}
