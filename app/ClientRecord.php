<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClientRecord extends Model
{
    protected $table = 'client';
    protected $fillable = ['name', 'timezone', 'state', 'phone_number', 'email', 'register_at'];
    public $timestamps = false;

}
