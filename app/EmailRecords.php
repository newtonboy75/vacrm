<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmailRecords extends Model
{
    protected $table = 'email_records';
    protected $fillable = ['recipient', 'sender', 'sent_by', 'subject', 'body', 'status', 'type', 'sub_type'];
    public $timestamps = false;

    public function admin_sender(){
      return  $this->hasOne('App\Models\Auth\User', 'id', 'sent_by')->orderBy('id', 'desc');
    }

    public function sender_name(){
      return  $this->hasOne('App\Models\Auth\User', 'id', 'sent_by');
    }

    public function sent_to(){
      return  $this->hasOne('App\ApplicantRecord', 'email', 'recipient');
    }



}
