<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantReference extends Model
{
    protected $table = 'applicants_references';
    protected $fillable = ['applicant_id', 'reference_id'];
    public $timestamps = false;

    public function reference_record(){
      return  $this->hasMany('App\ReferenceRecord', 'id', 'reference_id');
    }

}
