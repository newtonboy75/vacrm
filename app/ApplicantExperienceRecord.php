<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantExperienceRecord extends Model
{
    protected $table = 'experience';
    protected $fillable = ['company_name', 'position', 'start_date', 'end_date', 'job_responsibilities'];
    public $timestamps = false;


}
