<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class InterviewRecord extends Model
{
    protected $table = 'interview';
    protected $fillable = ['date', 'status', 'note', 'type', 'va_status', 'va_pool', 'job_id', 're_schedule_reason', 'client_id', 'recruiter_id'];
    public $timestamps = false;

    public function jobs(){
      return  $this->hasMany('App\JobRecord', 'id', 'job_id');
    }

    public function client(){
      return  $this->hasMany('App\ClientRecord', 'id', 'client_id');
    }

}
