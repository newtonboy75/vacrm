<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantEmergencyContact extends Model
{
    protected $table = 'employee_emergency_contact';
    protected $fillable = ['applicant_id', 'contact_person_name', 'contact_person_relation', 'contact_person_number'];
    public $timestamps = false;
}
