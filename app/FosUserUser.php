<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FosUserUser extends Model
{
    protected $table = 'fos_user_user';
    protected $fillable = ['user_id', 'group_id'];
    public $timestamps = false;

}
