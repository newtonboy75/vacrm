<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CoachingRecord extends Model
{
    protected $table = 'coaching';
    protected $fillable = ['date_served', 'issued_by', 'date_signed', 'status', 'memo', 'violation'];
    public $timestamps = false;

}
