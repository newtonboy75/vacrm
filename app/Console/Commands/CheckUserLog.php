<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Auth\User;
use App\UserLog;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class CheckUserLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check user daily activities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        $logs = '';
        $today = date('Y-m-d');
        foreach($users as $user){
          $userlogs = UserLog::where('user_id', '=', $user->id)->get();

          if(count($userlogs) >= 1){
            foreach($userlogs as $log){
              if(date('Y-m-d', strtotime($log->date_created)) == $today){
                $logs .= date('H:i A', strtotime($log->date_created)).' - '.$log->activity.'<br>';
              }
            }

            $to = 'newtonboy@gmail.com';
            $body = 'Daily Activity Report for '.$user->first_name.' '.$user->last_name.'<br>';
            $body .= 'Date: '.$today.'<br>';
            $body .= $logs;

            Mail::send([], [], function ($message) use($to, $body){
              $message->to($to)
                ->from('jobs@myvirtudesk.com', 'MyVirtudesk')
                ->subject('User Daily Activity Report')
                ->setBody($body, 'text/html');
            });

          }
        //end foreach
       }
    }


}
