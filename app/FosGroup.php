<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FosGroup extends Model
{
    protected $table = 'fos_user_group';
    protected $fillable = ['name', 'roles'];
    public $timestamps = false;

}
