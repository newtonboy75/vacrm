<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantFinalInterview extends Model
{
    protected $table = 'applicants_final_interviews';
    protected $fillable = ['applicant_id', 'interview_id'];
    public $timestamps = false;

    public function interview_record(){
      return  $this->hasMany('App\InterviewRecord', 'id', 'interview_id')->orderBy('id', 'desc');
    }
}
