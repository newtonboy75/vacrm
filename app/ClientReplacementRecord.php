<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClientReplacementRecord extends Model
{
    protected $table = 'client_replacement';
    protected $fillable = ['client_info_id', 'date', 'replacement_reason', 'replacement_id', 'replaced_by'];
    public $timestamps = false;

    public function replaced(){
      return  $this->hasOne('App\ApplicantRecord', 'id', 'replacement_id');
    }

    public function replacedby(){
      return  $this->hasOne('App\ApplicantRecord', 'id', 'replaced_by');
    }

    public function placement_info(){
      return  $this->hasOne('App\ClientPlacementRecord', 'id', 'client_info_id');
    }

    public function placement(){
      return  $this->hasOne('App\ClientPlacementRecord', 'current_va', 'replacement_id');
    }

}
