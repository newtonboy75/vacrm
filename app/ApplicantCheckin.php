<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantCheckin extends Model
{
    protected $table = 'employee_checkin_records';
    protected $fillable = ['employee_id', 'checkin_id'];
    public $timestamps = false;

    public function checkins(){
      return $this->hasMany('App\CheckInRecord', 'id', 'checkin_id');
    }
}
