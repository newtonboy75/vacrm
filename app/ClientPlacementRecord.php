<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClientPlacementRecord extends Model
{
    protected $table = 'client_placement_info';
    protected $fillable = ['client_id',	'marketing_specialist',	'status',	'month',	'year',	'start_date_with_va',	'three_month_contract',	'hiring_objective',	'contract_signed',	'current_va',	'employment_status',	'contract_tb1',	'current_va_start_date',	'cancellation_id', 'current_va_old'];
    public $timestamps = false;

    public function client_info(){
      return $this->hasMany('App\ClientRecord', 'id', 'client_id');
    }

    public function cancellations(){
      return $this->hasMany('App\ClientCancellationRecord', 'client_info_id', 'id');
    }

    public function suspensions(){
      return $this->hasMany('App\ClientSuspensionRecord', 'client_info_id', 'id');
    }

    public function replacements(){
      return $this->hasMany('App\ClientReplacementRecord', 'client_info_id', 'id');
    }

    public function curr_va(){
      return  $this->hasOne('App\ApplicantRecord', 'id', 'current_va');
    }

}
