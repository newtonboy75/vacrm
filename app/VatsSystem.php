<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class VatsSystem extends Model
{
  use SoftDeletes;

  protected $table = 'vats_info';
  protected $fillable = ['user_id', 'position', 'keywords_found', 'keywords_count', 'status', 'employment_history'];
  public $timestamps = false;

  public function getEmploymentCountAttribute(){
    $employment = $this->employment_history;
    return count($employment['employer']);
  }

  public function empCount(){
    return $this->employment_history;
  }
}
