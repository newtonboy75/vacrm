<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PreTrainingNotes extends Model
{
    protected $table = 'pre_training_notes';
    protected $fillable = ['applicant_id', 'notes', 'updated_at'];
    public $timestamps = false;
}
