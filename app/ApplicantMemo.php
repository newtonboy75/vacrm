<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantMemo extends Model
{
    protected $table = 'employee_memos';
    protected $fillable = ['employee_id', 'memo_id'];
    public $timestamps = false;

    public function memos(){
      return $this->hasMany('App\MemoRecord', 'id', 'memo_id');
    }
}
