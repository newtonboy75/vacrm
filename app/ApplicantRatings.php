<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantRatings extends Model
{
    protected $table = 'applicant_rating';
    protected $fillable = ['app_id', 'app_ratings'];
    public $timestamps = false;

}
