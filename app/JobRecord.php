<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class JobRecord extends Model
{
    protected $table = 'job';
    protected $fillable = ['description', 'hourly_rate', 'schedule', 'manager', 'start_date', 'end_date', 'status', 'employment_status', 'notes', 'tools', 'team', 'client_id', 'level'];
    public $timestamps = false;

    public function myteam(){
      return $this->hasOne('App\FosGroup', 'id', 'team');
    }
}
