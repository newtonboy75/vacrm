<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClientInterviewRecord extends Model
{
    protected $table = 'applicants_client_interviews';
    protected $fillable = ['applicant_id', 'interview_id'];
    public $timestamps = false;


    public function interviews(){
      return  $this->hasMany('App\InterviewRecord', 'id', 'interview_id');
    }

}
