<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class HomeEvaluationRecord extends Model
{
    protected $table = 'home_evaluation';
    protected $fillable = ['id', 'accommodation_type', 'living_duration', 'commoners_count', 'kids', 'kids_take_care', 'kind_of_neighborhood', 'power_outage', 'bad_weather_flooded', 'family_opinion', 'dedicate_space', 'free_distractions', 'lighting_and_ventilation', 'organized_workplace','system_check', 'result', 'photo_addendum', 'flood_plan', 'place_setup', 'describe_space', 'account_owner_relation', 'num_kids', 'kids_take_care_work'];
    public $timestamps = false;


    public function system(){
      return  $this->hasOne('App\SystemCheckRecord', 'id', 'system_check');
    }

    public function he_status(){
      $res = '';
      $result = ['Passed Home Evaluation', 'Failed Home Evaluation', 'On Hold', 'Blacklisted', 'Withdrawn', 'Forfeited'];
      $res = isset($this->result ) ?  $result[($this->result - 1)] : '';
      return $res;
    }

    public function he_notes(){
      return  $this->hasMany('App\HomeEvalNotes', 'home_eval_id', 'id');
    }

}
