<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Media extends Model
{
    protected $table = 'media_image_addendum';
    protected $fillable = ['id', 'name'];
    public $timestamps = false;

}
