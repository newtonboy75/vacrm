<?php

namespace App\Exports;

use App\ApplicantRecord;
use App\VatsSystem;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersReferrerExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $req;

    public function __construct($req)
    {
       $this->req = $req;
    }

    public function collection()
    {

      $reqs[] = $this->req['sourcing-field'];

      //$kw_count = '0'; //optional feature
      $applicants = null;

      //dd($reqs);

      $from = $reqs[0]['from'] ?? 'sourcing-record';
      unset($reqs[0]['from']);

      if($from == 'sourcing-record'){

        $priority_lists = VatsSystem::where('status', '=', '0')->pluck('user_id');
        $applicants = ApplicantRecord::where('workflow_status', '=', $from)->where('archived', '=', '0')->whereIn('id', $priority_lists)->select($reqs[0])->get();
      }elseif($from == 'talent_acquisition'){

        $status = $reqs[0]['status'];
        $initial_interview = $reqs[0]['created_at'];

        unset($reqs[0]['status']);



        $applicants = ApplicantRecord::where('workflow_status', '=', $from)->whereDate('created_at', date('Y-m-d', strtotime('yesterday')))->where('archived', '=', '0')->select($reqs[0])->orderBy('id', 'desc')->get();

        $current_status = [
          "0" => "Pending Initial Interview",
          "1" => "Passed Initial Interview",
          "2" => "Failed Initial Interview",
          "3" => "No Show",
          "5" => "Pending Interview",
          "4" => "Blacklisted",
          "6" => "Re-scheduled | Health Issues",
          "7" => "Re-scheduled | System Requirements",
          "8" => "Re-scheduled | Personal Matters",
          "9" => "Re-scheduled | Employment",
          "10" => "Re-scheduled | Family Emergency",
          "11" => "On Hold | System Requirements",
          "12" => "On Hold | Undecided",
          "13" => "Re-Application | System Requirements",
          "14" => "Re-Application | Undecided",
          "15" => "Re-Application | Health Issues",
          "16" => "Re-Application | Re-schedule issues",
          "17" => "Re-Application | Employed",
          "18" => "Re-Application | Personal Matters",
          "19" => "Re-Application | Emergency",
          "20" => "Re-Application | Business Structure",
          "21" => "Withdrawn | System Requirements",
          "22" => "Withdrawn | Undecided",
          "23" => "Withdrawn| Health Issues",
          "24" => "Withdrawn | Re-schedule issues",
          "25" => "Withdrawn | Employed",
          "26" => "Withdrawn| Personal Matters",
          "27" => "Withdrawn | Emergency",
          "28" => "Withdrawn | Business Structure",
          "29" => "Pending Exam",
          "30" => "Pending Systems Check",
          "31" => "Failed Systems Check",
          "32" => "Failed Exam",
          "33" => "Rescheduled"
        ];

        foreach($applicants as $applicant){
          $applicant->status = $current_status[$applicant->initial_interview[0]->interview_record[0]->status];
          $applicant->recruiter_id = $applicant->recruiter_name['first_name'].' '.$applicant->recruiter_name['last_name'];
          $applicant->created_at = $applicant->initial_interview[0]->interview_record[0]->date;
        }
      }


      return $applicants;

    }
}
