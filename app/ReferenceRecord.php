<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ReferenceRecord extends Model
{
    protected $table = 'reference';
    protected $fillable = ['referencer', 'company', 'position', 'mobile_number', 'landing_number', 'email', 'best_time_to_contact', 'note', 'date_contacted'];
    public $timestamps = false;

}
