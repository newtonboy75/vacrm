<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BlockSchedule extends Model
{
    protected $table = 'block_schedule';
    protected $fillable = ['recruiter_id', 'interview_id'];
    public $timestamps = false;

    public function recruiter(){
      return  $this->hasMany('App\Models\Auth\User', 'id', 'recruiter_id');
    }

    public function block_interview(){
      return  $this->hasMany('App\InterviewRecord', 'id', 'interview_id');
    }


}
