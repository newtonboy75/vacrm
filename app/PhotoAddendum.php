<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PhotoAddendum extends Model
{
    protected $table = 'photo_addendum';
    protected $fillable = ['exterior',	'headset',	'workstation',	'backup_power_source',	'main_pc',	'main_pc_specs',	'backup_pc',	'backup_pc_specs',	'main_isp',	'main_isp_speed_test',	'backup_isp',	'backup_isp_speed_test',	'isp_receipt',	'isp_bill',	'isp_authorization_letter',	'isp_owner_id',	'webcamera',	'owner'];

    public $timestamps = false;

    public function getImage($id, $user_id){
      $image = \App\Media::whereId($id)->orderBy('id', 'asc')->first();
      if($image){
        $filename = 'uploaded/home_eval/user_'.$user_id.'/'.$image->name;
        if (!file_exists($filename)) {
          $filename = 'img/backend/brand/grey.png';
        }
      }else{
        $filename = 'img/backend/brand/grey.png';
      }

      return $filename;
    }

}
