<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\EmailRecords;

class EmailRecordController extends Controller
{
    public function save($recipient,	$sender,	$sent_by,	$subject,	$body,	$status, $type, $sub_type){

      $data = [
        'recipient' => $recipient,
        'sender' => $sender,
        'sent_by' => $sent_by,
        'subject' => $subject,
        'body' => $body,
        'status' => $status,
        'type' => $type,
        'sub_type' => $sub_type
      ];

      $email = EmailRecords::create($data);
      return $email;

    }
}
