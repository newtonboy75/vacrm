<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserLog;

class UserLogController extends Controller
{
    public function save($user_id, $action){
      $data = [
        'user_id' => $user_id,
        'activity' => $action,
      ];
      $userlog = new UserLog;
      $userlog->create($data);
    }
}
