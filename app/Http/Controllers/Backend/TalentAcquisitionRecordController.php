<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use App\InterviewRecord;
use App\ApplicantFirstInterview;
use App\ApplicantExperienceRecord;
use App\ApplicantExperience;
use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;
use App\ApplicantFinalInterview;
use App\Http\Controllers\Backend\ApplicantRecordController;
use App\Http\Controllers\Backend\AdministrativeController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Backend\EmailRecordController;
use App\Jobs\SendEmailTalentQueues;
use GuzzleHttp\Client;
//use Khill\Lavacharts\Lavacharts;

/**
* Class SourcingRecordController.
*/
class TalentAcquisitionRecordController extends Controller
{
  /**
  * @return \Illuminate\View\View
  */
  public function index(){
    $chart = new AdministrativeController;
    $lava = $chart->getChartAll();
    return view('backend.talent-acquisition-record', compact('lava'));
  }

  public function load(){
    DB::connection()->disableQueryLog();
    $all_records = null;

    $tabs = request()->tab;
    $st = count(request()->archived) > 1 ? 3 : 0;

    $time = strtotime("-6 month", time());
    $more_than_a_year = strtotime("-18 month", time());
    $date = date("Y-m-d", $time);

    if(request()->search['value'] != ''){

      if(is_array($tabs)){
        $all_records = ApplicantRecord::where('name', 'REGEXP', request()->search['value'])
        ->orWhere('email', request()->search['value'])
        ->whereIn('workflow_status', $tabs)->where('archived', '<=', '3')
        ->where('recruiter_id', '!=', '')
        ->whereDate('created_at', '>=', $more_than_a_year)
        ->select('id', 'status', 'workflow_status', 'created_at', 'name', 'recruiter_id')
        ->orderBy('created_at', 'desc')->limit(100);
      }else{
        $all_records = ApplicantRecord::where('workflow_status', 'talent_acquisition')
        ->where('name', 'REGEXP', request()->search['value'])
        ->orWhere('email', request()->search['value'])
        ->where('archived', '<=', '3')->where('recruiter_id', '!=', '')
        ->whereDate('created_at', '>=', $more_than_a_year)
        ->select('id', 'status', 'workflow_status', 'created_at', 'name', 'recruiter_id')
        ->orderBy('created_at', 'desc')->limit(100);
      }

    }else{
      $all_records = ApplicantRecord::select('id', 'status', 'workflow_status', 'created_at', 'name', 'recruiter_id')
      ->where('workflow_status', 'talent_acquisition')
      ->where('archived', '<=', $st)
      ->where('recruiter_id', '!=', '')
      ->whereDate('created_at', '>=', $date)
      ->orderBy('updated_at', 'desc');
    }



    return datatables()->of($all_records)

    ->editColumn('created_at', function($all_records){
      $html = '';
      $first_inteview_id = $all_records->initial_interview[0]['interview_id'] ?? '';

      if($first_inteview_id != ''){

        $html =  date('M d Y, h:i', strtotime($all_records->initial_interview[0]['interview_record'][0]['date']));

      }

      return $html;
    })

    ->editColumn('workflow_status', function($all_records){
      return $all_records->status_context();
    })

    ->editColumn('name', function($all_records){
      return '<a style="color: #1976d2; margin-top: 10px !important; margin-bottom: 10px !important;" href="/admin/talent-acquisition-record/'.$all_records->id.'/edit"><strong>'.$all_records->name.'</strong></a>';
    })

    ->editColumn('status', function($all_records){

      $status = 0;
      $cur_status = null;

      if(count($all_records->initial_interview) >= 1){
        $status = $all_records->initial_interview[0]->interview_record[0]['status'];
        $type = $all_records->initial_interview[0]->interview_record[0]['type'];
      }

      $current_status = [
        "0" => "Pending Initial Interview",
        "1" => "Passed Initial Interview",
        "2" => "Failed Initial Interview",
        "3" => "No Show",
        "5" => "Pending Interview",
        "4" => "Blacklisted",
        "6" => "Re-scheduled | Health Issues",
        "7" => "Re-scheduled | System Requirements",
        "8" => "Re-scheduled | Personal Matters",
        "9" => "Re-scheduled | Employment",
        "10" => "Re-scheduled | Family Emergency",
        "11" => "On Hold | System Requirements",
        "12" => "On Hold | Undecided",
        "13" => "Re-Application | System Requirements",
        "14" => "Re-Application | Undecided",
        "15" => "Re-Application | Health Issues",
        "16" => "Re-Application | Re-schedule issues",
        "17" => "Re-Application | Employed",
        "18" => "Re-Application | Personal Matters",
        "19" => "Re-Application | Emergency",
        "20" => "Re-Application | Business Structure",
        "21" => "Withdrawn | System Requirements",
        "22" => "Withdrawn | Undecided",
        "23" => "Withdrawn| Health Issues",
        "24" => "Withdrawn | Re-schedule issues",
        "25" => "Withdrawn | Employed",
        "26" => "Withdrawn| Personal Matters",
        "27" => "Withdrawn | Emergency",
        "28" => "Withdrawn | Business Structure",
        "29" => "Pending Exam",
        "30" => "Pending Systems Check",
        "31" => "Failed Systems Check",
        "32" => "Failed Exam",
        "33" => "Rescheduled"
      ];

      return $status ? $current_status[$status]  : 'Pending Interview';
    })

    ->editColumn('recruiter_id', function($all_records){

      $html = '';

      if(isset($all_records->recruiter_name)){
        $html = $all_records->recruiter_name['first_name'].' '.$all_records->recruiter_name['last_name'];
      }

      return $html;
    })
    ->rawColumns(['name'])
    ->make(true);
  }

  public function edit(Request $request){
    $edit = new ApplicantRecordController;
    return $edit->edit($request);
  }

  public function update(Request $request, ApplicantRecord $applicant, ApplicantFirstInterview $interview_initial, InterviewRecord $interview, ApplicantFinalInterview $interview_final){
    DB::connection()->disableQueryLog();
    foreach($request->talent_application_status_first as $k=>$v){

      if(isset($request->talent_application_id_first[$k] )){
        $interviews = $interview->where('id', '=', $request->talent_application_id_first[$k])->first();

        $data = [
          'note' => $request->talent_note_first[$k],
          'date' => date('Y-m-d H:i:s', strtotime($request->talent_exp_date_end_first[$k])),
          're_schedule_reason' => $request->talent_rescheduled_first[$k],
          'status' => $request->talent_application_status_first[$k],
          'type' => $request->talent_application_type_first[$k],
          'recruiter_id' => $request->tl_recruiter[$k]
        ];

        $interviews->update($data);
        //unset($data);
      }else{
        $data = [
          'note' => $request->talent_note_first[$k],
          'date' => date('Y-m-d H:i:s', strtotime($request->talent_exp_date_end_first[$k])),
          're_schedule_reason' => $request->talent_rescheduled_first[$k],
          'status' => $request->talent_application_status_first[$k],
          'type' => $request->talent_application_type_first[$k],
          'recruiter_id' => $request->tl_recruiter[$k]
        ];

        $new_id = InterviewRecord::create($data);
        $interview_initial->create(['applicant_id' => $request->applicant_id, 'interview_id' => $new_id->id]);
        //unset($data);
      }
    }

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Updated/created Inteview Record record for: '.$request->applicant_id;
      $userlog->save($logged_user_id, $action);
      unset($userlog);
    }


    if(isset($request->talent_application_status_final)){
      foreach($request->talent_application_status_final as $k=>$v){

        if(isset($request->talent_application_id_final[$k])){
          $interviews = $interview->where('id', '=', $request->talent_application_id_final[$k])->first();

          $data = [
            'note' => $request->talent_note_final[$k],
            'date' => date('Y-m-d H:i:s', strtotime($request->talent_exp_date_end_final[$k])),
            're_schedule_reason' => $request->talent_rescheduled_final[$k],
            'status' => $request->talent_application_status_final[$k],
            'type' => $request->talent_application_type_final[$k],
            'recruiter_id' => $request->tl_recruiter_final[$k]
          ];

          $interviews->update($data);
          //unset($data);
        }else{

          $data = [
            'note' => $request->talent_note_final[$k],
            'date' => date('Y-m-d H:i:s', strtotime($request->talent_exp_date_end_final[$k])),
            're_schedule_reason' => $request->talent_rescheduled_final[$k],
            'status' => $request->talent_application_status_final[$k],
            'type' => $request->talent_application_type_final[$k],
            'recruiter_id' => $request->tl_recruiter_final[$k]
          ];

          $new_id = InterviewRecord::create($data);
          $interview_initial->create(['applicant_id' => $request->applicant_id, 'interview_id' => $new_id->id]);
          //unset($data);
        }
      }
    }

    $archivable_status = ['2', '3', '4', '21', '22', '23', '24', '25', '26', '27', '28', '31', '32'];

    $last_first_interview = ApplicantFirstInterview::where('applicant_id', '=', $request->applicant_id)->with('interview_record')->orderBy('id', 'desc')->first();

    $workflow_status = 'talent_acquisition';
    $archived = '0';

    if($last_first_interview){
      $last_interview_rec = last($last_first_interview->interview_record);
      //return $last_interview_rec[0]->status;

      if(in_array($last_interview_rec[0]->status, $archivable_status)){
        $archived = '1';
      }
    }

    $applicant_update = $applicant->whereId($request->applicant_id)->first();
    if($applicant_update){
      $applicant_update->archived = $archived;
      $applicant_update->save();
      unset($request);
      unset($applicant_update);
      unset($last_first_interview);
      return '1';
    }
    unset($request);
    unset($applicant_update);
    unset($last_first_interview);
    unset($archivable_status);
    //gc_collect_cycles();
    return '0';
  }

  public function delete(Request $request){

    $id = InterviewRecord::whereId($request->id)->delete();
    if($request->type="initial"){
      $app_int_id = ApplicantFirstInterview::where('interview_id', '=', $request->id)->delete();
    }else{
      $app_int_id = ApplicantFinalInterview::where('interview_id', '=', $request->id)->delete();
    }

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Deleted Talent Acquisiton record : '.$request->id;
      $userlog->save($logged_user_id, $action);
      //unset($userlog);
    }

    //unset($request);
    return $app_int_id;
  }

  public function sendEmail(Request $request){

    $success = '0';
    $to = $request->to;
    $body = $request->email_body;
    $from = '';
    $subject = '';
    $type = 'talent_acquisition';
    $status = 'sent';
    $sub_type = '';
    $new_request = $request->all();
    $disposition = '';

    if($request->type == 'interview_passed'){

      $from = 'virtudesk.referenceteam@gmail.com';
      $subject = 'IMPORTANT: PLEASE READ THE WHOLE EMAIL - Virtudesk - Interview Result';
      $sub_type = 'passed_interview';
      $disposition = 'interview_passed';

      //old implementation
      //SendEmailTalentQueues::dispatchNow($new_request, 'interview_passed');

      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Sent interview passed email for user: '.$request->user_id;
        $userlog->save($logged_user_id, $action);
        //unset($userlog);
      }

    }else{

      $sub_type = 'failed_interview';
      $from = 'virtudesk.referenceteam@gmail.com';
      $subject = 'Virtudesk Application | Application Result';
      $disposition = 'failed_interview';

      //SendEmailTalentQueues::dispatchNow($new_request, 'failed_interview');

      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Sent interview failed email for user: '.$request->user_id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }

      $success = '1';
    }

    $save_email = new EmailRecordController();
    $save_email->save($to,	$from,	$logged_user_id,	$subject,	$body,	$status, $type, $sub_type);

    //SendEmailTalentQueues::dispatchNow($new_request, $disposition);
    //old implementation

    $this->sendTalentEmail($new_request, $disposition);

    unset($new_request);
    unset($request);
    unset($save_email);
    //gc_collect_cycles();
    return '1';

  }


  public function sendTalentEmail($request, $disposition){

    $url = 'https://services.myvirtudesk.com/api/send_talent_email';

    $client = new Client(); //GuzzleHttp\Client
    $result = $client->post($url, [
      'form_params' => [
        'email' => 'newtonboy@gmail.com',
        'password' => 'renton75',
        'allrec' => $request,
        'disposition' => $disposition
      ]
    ]);

    //$r = json_encode($result->getBody()->getContents());
    //unset($r);
  }

  public function resched(Request $request){
    $to_app = $request->to;
    $html = $request->body;
    $from = '';
    $subject = 'Applicant Interview Date Rescheduled';
    $status = 'sent';

    $new_request = $request->all();

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Sent resched failed email for user: '.$request->user_id;
      $userlog->save($logged_user_id, $action);
      unset($userlog);
    }

    $save_email = new EmailRecordController();
    $save_email->save($to_app,	$from,	$logged_user_id,	$subject,	$html,	$status, 'talent', 'resched');

    //SendEmailTalentQueues::dispatchNow($new_request, 'resched');

    $this->sendTalentEmail($new_request, 'resched');

    unset($request);
    unset($save_email);
    unset($new_request);
    //gc_collect_cycles();
    return '1';

  }

}
