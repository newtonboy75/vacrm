<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Zenapply\Calendly\Calendly;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\ApplicantRecord;
use App\InterviewRecord;
use App\ApplicantFirstInterview;
use Illuminate\Support\Facades\Mail;
use App\BlockSchedule;
use Carbon\Carbon;
use App\Models\Auth\User;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\EmailRecords;
use App\ClientRecord;
use App\ClientPlacementRecord;
use GuzzleHttp\Client;
use Khill\Lavacharts\Lavacharts;
use App\VatsSystem;
use App\Exports\UsersReferrerExport;
use Excel;
use Illuminate\Support\Facades\DB;

class AdministrativeController extends Controller
{

  public function invitee_created(Request $request){

    if($request){
      Log::info('invitee created');
      Log::info($request);
    }
  }

  public function invitee_cancelled(Request $request){
    if($request){
      Log::info('invitee cancelled');
      Log::info($request);
    }
  }

  public function get_interview_info(Request $request){

    $return_date = [];
    $current_skeds = [];
    $current_skeds2 = [];

    $applicant = ApplicantRecord::whereId($request->id)->with('recruiter_name', 'initial_interview')->first();
    $check_schedules = ApplicantRecord::where('recruiter_id', '=', $applicant->recruiter_id)->with('initial_interview')->get();
    $block_interviews = BlockSchedule::where('recruiter_id', '=', $applicant->recruiter_id)->with('block_interview')->get();

    foreach($check_schedules as $skeds){
      $current_skeds[] = $skeds->initial_interview;
    }

    foreach($block_interviews as $block){
      $current_skeds2[] = $block->block_interview;
    }

    $applicant->interview_schedule = $current_skeds;
    $applicant->block_schedule = $current_skeds2;

    return $applicant;

  }

  public function set_sched(Request $request){

    $data = [
      'date' => date('Y-m-d H:i:s', strtotime($request->datetime)),
      'type' => 'initial',
      'status' => '5'
    ];

    $new_id = InterviewRecord::create($data);
    $interview_initial = ApplicantFirstInterview::create(['applicant_id' => $request->id, 'interview_id' => $new_id->id]);

    $applicant = ApplicantRecord::whereId($request->id)->with('recruiter_name')->first();

    $to = $applicant->recruiter_name->vd_email;
    //$to = 'newtonboy@gmail.com';

    $body = '<p><strong>Hello Team,</strong></p>'.$applicant->name.' just set a date for an interview.';
    Mail::send([], [], function ($message) use($to, $body){
      $message->to($to)
        ->from('jobs@myvirtudesk.com', 'MyVirtudesk')
        ->subject('Virtudesk Application | Application Result')
        ->setBody($body, 'text/html');
    });

    return '1';
  }

  public function set_sched_by_recruiter(Request $request){

    $to = strtotime($request->to);
    $from = strtotime($request->from);
    $res = (round(abs($to - $from) / 60,2)/15);
    $ntime = [];
    $newtime = '';

    if($res <= 0){
      $dte = date('Y-m-d', strtotime($request->date_to)).' '.$request->from;

      $data = [
        'date' => date('Y-m-d H:i:s', strtotime($dte)),
        'type' => 'initial',
        'status' => '5'
      ];

      $new_id = InterviewRecord::create($data);
      $interview_initial = ApplicantFirstInterview::create(['applicant_id' => null, 'interview_id' => $new_id->id]);
      BlockSchedule::create(['interview_id' => $new_id->id, 'recruiter_id' => $request->recruiter_id]);

      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Set blocked schedule for Recruiter ID: '.$request->recruiter_id;
        $userlog->save($logged_user_id, $action);
      }

    }else{
      for($i=0; $i<$res; $i++){
        //$x[] = floor(15 % 60)*$i;
        $nt = floor(15 % 60)*($i);
        $ntime[] = date('H:i', strtotime('+'.$nt.' minutes', $from));
      }

      array_push($ntime, date('H:i', $to));

      foreach($ntime as $nt){
        $dte = date('Y-m-d', strtotime($request->date_to)).' '.$nt;

        $data = [
          'date' => date('Y-m-d H:i:s', strtotime($dte)),
          'type' => 'initial',
          'status' => '5'
        ];

        $new_id = InterviewRecord::create($data);
        $interview_initial = ApplicantFirstInterview::create(['applicant_id' => null, 'interview_id' => $new_id->id]);
        BlockSchedule::create(['interview_id' => $new_id->id, 'recruiter_id' => $request->recruiter_id]);

        if(Auth::check()){
          $userlog = new UserLogController;
          $logged_user_id = Auth::id();
          $action = 'Set blocked schedule for Recruiter ID: '.$request->recruiter_id;
          $userlog->save($logged_user_id, $action);
        }
      }
    }

    return '1';
  }

  public function delete_schedule(Request $request){

    $interview_id = ApplicantFirstInterview::where('interview_id', '=', $request->id)->delete();
    $block_id = BlockSchedule::where('interview_id', '=', $request->id)->delete();

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Deleted blocked schedule #: '.$request->id;
      $userlog->save($logged_user_id, $action);
    }

    return $interview_id;
  }

  public function get_interview_info_by_recruiter(Request $request){

    $current_skeds = [];
    $current_skeds2 = [];
    $sked = collect([]);
    $all_skeds = [];
    $contents = [];

    //$applicant = ApplicantRecord::whereId($request->id)->with('recruiter_name', 'initial_interview')->first();
    $check_schedules = ApplicantRecord::where('recruiter_id', '=', $request->id)->orderBy('id', 'desc')->with('initial_interview')->get();
    $block_interviews = BlockSchedule::where('recruiter_id', '=', $request->id)->orderBy('id', 'desc')->with('block_interview')->get();
    $interviewers = User::whereId($request->id)->first();
    $time_scheds =  unserialize($interviewers->timesched);
    $day_of_week = unserialize($interviewers->day_of_week);

    $selected_date = date('l', strtotime($request->date));

    if($day_of_week == ''){
      $contents[0] = '';
      $contents[1] = '<li>No schedule</li>';
      return $contents;
      exit;
    }

    if(!in_array(strtolower($selected_date), $day_of_week)){
      $contents[0] = '';
      $contents[1] = '<li>No schedule</li>';
      return $contents;
      exit;
    }

    foreach($check_schedules as $s=>$skeds){
      //$current_skeds[] = $skeds->initial_interview;
      $name = $skeds->name;
      $id = $skeds->id;
      foreach($skeds->initial_interview as $i=>$ii){
        $current_skeds[strtotime($ii->interview_record[0]->date)] = [$name, $ii->interview_record[0]->date];
      }
    }

    foreach($block_interviews as $block){
      foreach($block->block_interview as $l=>$bi){
        $current_skeds2[strtotime($bi->date)] = [$bi->note, $bi->date];
      }
    }

    asort($current_skeds2, SORT_NUMERIC);
    asort($current_skeds, SORT_NUMERIC);

    $new_skeds = array_merge($current_skeds2, $current_skeds);

    $lis = '';

    $excluded_hours = [];
    $selected_date = date('Y-m-d', strtotime($request->date));
    foreach($new_skeds as $v){
      $interviewee = $v[0] != '' ? $v[0] : 'Recruiter not available';
      $ndte = date('Y-m-d', strtotime($v[1]));
      if(strtotime($ndte)==strtotime($selected_date)){
          $excluded_hours[] = date('g:i a', strtotime($v[1]));
          $lis .= '<li rel="'.date('Y-m-d g:i a', strtotime($v[1])).'"><strong>'.date('g:i a', strtotime($v[1])).'</strong> :&nbsp;&nbsp;'.$interviewee.'</li>';
      }
    }

    $from = strtotime($time_scheds[0]); //strtotime(date('Y-m-d 00:00:00'));
    $to = strtotime($time_scheds[1]); //strtotime(date('Y-m-d 23:59:00'));


    $res = (round(abs($to - $from) / 60,2)/15);
    for($i=0; $i<$res; $i++){
        $nt = floor(15 % 60)*($i);
        $ntime[] = date('g:i a', strtotime('+'.$nt.' minutes', $from));
      }

      array_push($ntime, date('g:i a', strtotime($time_scheds[1])));

    $html = '<ul class="ul-rec-tab">';
    $html .= $lis;
    $html .= '</ul>';

    $html2 = '';


    foreach($ntime as $available_time){
      if(!in_array($available_time, $excluded_hours)){
        $html2 .= '<li class="available-recruiter-ts"><button class="btn btn-sm btn-green mx-auto" id="ts">'.$available_time.'</button></li>';
      }else{
        $html2 .= '<li><button title="Unavailable" class="btn btn-sm mx-auto selected" id="ts-o">'.$available_time.'</button></li>';
      }
    }

    $contents[0] = $html;
    $contents[1] = $html2;

    return $contents;
  }

  public function set_schedule_by_recruiter(Request $request){

    foreach($request->date as $dte){
      $data = [
        'date' => date('Y-m-d H:i:s', strtotime($dte)),
        'type' => 'block',
        'status' => '5',
        'note' => $request->note
      ];

      $new_id = InterviewRecord::create($data);
      $interview_initial = ApplicantFirstInterview::create(['applicant_id' => null, 'interview_id' => $new_id->id]);
      BlockSchedule::create(['interview_id' => $new_id->id, 'recruiter_id' => $request->recruiter_id]);
    }

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Set blocked schedule #: '.$request->recruiter_id;
      $userlog->save($logged_user_id, $action);
    }
    return '1';
  }

  public function outbox(){
    $title = 'Outbox';
    return view('backend.oubox', compact('title'));
  }

  public function outboxType(Request $request){
    $type = [$request->type];

    if($request->type == 'home_eval'){
      $type = ['home_eval', 'reference'];
    }elseif($request->type == 'talent'){
      $type = ['talent', 'talent_acquisition'];
    }

    $email_recs = EmailRecords::whereIn('type', $type)->with('sent_to')->orderBy('id', 'desc')->paginate(10);
    return $email_recs;
  }

  public function outboxBody(Request $request){
    $body = EmailRecords::whereId($request->id)->with('sender_name', 'sent_to')->first();
    return $body;
  }

  public function performServerRefresh(){
    //exec("sudo service mysql restart");
    exec("sudo service apache2 restart");
    echo 'server restarted. please go back.';
  }

  public function read_csv(){
    $allclients = ClientRecord::where('id', '>=', 1038)->get();
    //$clients = array_map('trim', $allclients);


    //dd($clients);

    $csv = fopen('/var/www/html/virtudesk_v2/virtudesk/public/temp_uploads/clists.csv', 'r');
    //$row = fgetcsv($csv);


    $names[] = [];
    while($row = fgetcsv($csv)){
        $status = '';

        if($row[0] != ''){

            if($row[2] == 'On Hold'){
                $status = 'on_hold';
            }elseif(strstr($row[2], 'Placement')){
                $status = str_replace('Placement - ', '', $row[2]);
            }elseif(strstr($row[2], 'Timeblock')){
                $status = str_replace('Timeblock - ', '', $row[2]);
            }elseif(strstr($row[2], 'Sales')){
                $status = str_replace('Sales - ', '', $row[2]);
            }elseif($row[2] == 'Active - Cancelled'){
                $status = 'cancelled';
            }else{
                $status = $row[2];
            }

             switch(strtolower($status)){
                case 'active':
                $stat = '1';
                break;
                case 'inactive':
                $stat = '2';
                break;
                case 'cancelled':
                $stat = '3';
                break;
                case 'placement':
                $stat = '4';
                break;
                case 'suspended':
                $stat = '5';
                break;
                case 'completed':
                $stat = '6';
                break;
                case 'incomplete':
                $stat = '7';
                break;
                case 'probono':
                $stat = '8';
                break;
                case 'on_hold':
                $stat = '9';
                break;
                default:
                $stat = '';
              }
            //if(trim($row[0]) == $clients)){
                foreach($allclients as $cl){
                    if(trim($cl->name) == trim($row[0])){
                        $names[] = array('client_id' => $cl->id, 'status' => $stat, 'contract_signed' => $row[14], 'current_va_old' => $row[15], 'year' => $row[6], 'contract_tb1' => $row[18], 'marketing_specialist' => $row[1], 'cancellation_date' => $row[21], 'cancellation_reason' => trim($row[25]), 'current_va_start_date' => date('Y-m-d h:i:s', strtotime($row[7]))) ;
                    }

                }

            //}
        }
    }

    unset($names[0]);
    //unset($names[1]);

    foreach($names as $name){
        ClientPlacementRecord::create($name);
    }

    //dd('done all');

    $client_recs = ClientPlacementRecord::where('id', '>=', 1038)->get();
    unset($client_recs);

   // dd($client_recs);

  }

  public function process_csv($filename){

     // dd($filename);

    $client = new \GuzzleHttp\Client();
    $url = 'https://services.myvirtudesk.com/api/vat';

    $client = new Client(); //GuzzleHttp\Client
    $result = $client->post($url, [
        'form_params' => [
        'email' => 'newtonboy@gmail.com',
        'password' => 'renton75',
        'filename' => $filename,
        'id' => '1'
          ]
    ]);
    $r = json_decode($result->getBody()->getContents());
    dd($r);
  }

  public function getChart($date_from, $date_to, $url){

    if($url == 'sourcing-record'){
      $priority_lists = VatsSystem::where('created_at', '>=', $date_from)->where('created_at', '<=', $date_to)->get();

      $low = [];
      $medium = [];
      $high = [];
      $highest = [];

      foreach($priority_lists as $list){
        if($list->keywords_count < 5){
          $low[] = $list->id;
        }elseif($list->keywords_count >= 5 && $list->keywords_count <= 7){
          $medium[] = $list->id;
        }elseif($list->keywords_count >= 8 && $list->keywords_count <= 14){
          $high[] = $list->id;
        }else{
          $highest[] = $list->id;
        }
      }


      $lava = new Lavacharts; // See note below for Laravel

      $datatable = $lava->DataTable();
      $datatable->addNumberColumn('Lowest')->addNumberColumn('Low')->addNumberColumn('Medium')->addNumberColumn('High')->addNumberColumn('Highest');
      $datatable->addRow([count($low), count($medium), count($high), count($highest)]);

      return $datatable;
    }

  }

  public function exportReferrer(Request $request){
    return Excel::download(new UsersReferrerExport($request), 'users_referrer.xlsx');
  }

  public function getChartAll(){
    $applicants = ApplicantRecord::where('archived', '=', '0')->where('workflow_status', '!=', null)->groupBy('workflow_status')->select('workflow_status', DB::raw('count(*) as total'))->get();
//dd($applicants);
    $lava = new Lavacharts;
    $datatable = $lava->DataTable();

    $sourcing = '';
    $talent = '';
    $reference = '';
    $pretraining = '';
    $training = '';
    $placement = '';
    $operation = '';

    foreach($applicants as $applicant){
      if($applicant->workflow_status == 'sourcing_record'){
        $sourcing = $applicant->total;
      }elseif($applicant->workflow_status == 'talent_acquisition'){
        $talent = $applicant->total;
      }elseif($applicant->workflow_status == 'pre_training'){
        $pretraining = $applicant->total;
      }elseif($applicant->workflow_status == 'training_record'){
        $training = $applicant->total;
      }elseif($applicant->workflow_status == 'placement_record'){
        $placement = $applicant->total;
      }elseif($applicant->workflow_status == 'hr_record'){
        $operation = $applicant->total;
      }elseif($applicant->workflow_status == 'reference_check'){
        $reference = $applicant->total;
      }
    }

    $datatable->addNumberColumn('')->addNumberColumn('Sourcing Record')->addNumberColumn('Talent Acquisiton')->addNumberColumn('Reference Check')->addNumberColumn('Pre-training')->addNumberColumn('Training')->addNumberColumn('Placement')->addNumberColumn('HR Record');
    $datatable->addRow(['0', $sourcing, $talent, $reference, $pretraining, $training, $placement, $operation]);

    $lava->BarChart('AllStat', $datatable, [

        'vAxis' => [
            'title' => 'Tabs'
        ],
        'hAxis' => [
            'title' => 'Number of Applicants in each tab'
        ],
        'height' => 600,
        'width' => 800
    ]);

    return $lava;

  }


}
