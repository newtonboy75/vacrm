<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ApplicantRecord;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\InterviewRecord;
use App\JobRecord;
use App\ApplicantClientInterview;
use App\ClientRecord;
use Illuminate\Support\Facades\Mail;
use App\EmployeeJob;
use App\EmployeeRecord;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Backend\AdministrativeController;

/**
 * Class SourcingRecordController.
 */
class PlacementRecordController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
     public function index(){
       $chart = new AdministrativeController;
       $lava = $chart->getChartAll();
       return view('backend.placement-record', compact('lava'));
     }

     public function load(){


       $tab = request()->tab;
       $stats = request()->archived;
       $type = request()->placement_type;


       if(request()->search['value'] != ''){
         $seach_val = request()->search['value'];
         //Log::info(count($stats));
         if(count($stats) == '1'){
           //Log::info('here '.implode(',', $tab).' '.implode(',', $stats).' '.$type.' search '.request()->search['value']);
           $all_records = ApplicantRecord::where('name', 'REGEXP', $seach_val)->orWhere('email', $seach_val)->whereIn('workflow_status', array('hr_record', 'placement_record'))->where('archived', '=', '0')->groupBy('name')->orderBy('id', 'desc');

         }else{
           //Log::info('this '.implode(',', $tab).' '.implode(',', $stats).' '.$type.' search '.request()->search['value']);
           $all_records = ApplicantRecord::where('name', 'REGEXP', $seach_val)->orWhere('email', $seach_val)->whereIn('workflow_status', array('hr_record', 'placement_record'))->groupBy('name')->orderBy('id', 'desc');
         }
       }else{
         //Log::info('heto '.implode(',', $tab).' '.implode(',', $stats).' '.$type.' search '.request()->search['value']);
         if($type == 'all'){
           $all_records = ApplicantRecord::whereIn('workflow_status', array('hr_record', 'placement_record'))->groupBy('name')->orderBy('id', 'desc');
         }elseif($type == 'active'){
           $all_records = ApplicantRecord::whereIn('workflow_status', array('hr_record', 'placement_record'))->where('archived', '0')->groupBy('name')->orderBy('id', 'desc');
         }else{
           $all_records = ApplicantRecord::whereIn('workflow_status', array('hr_record', 'placement_record'))->where('archived', '>=', '1')->groupBy('name')->orderBy('id', 'desc');
         }
       }



       //Log::info(implode(',', $tab).' '.implode(',', $stats).' '.$type.' search '.request()->search['value']);

       return datatables()->of($all_records)

       ->editColumn('name', function($all_records){
         $html = $all_records->name ?? '';
         return $html;
       })

       ->addColumn('program', function($all_records){
         $html = '';
        if(count($all_records->trainings) >= 1){
             $html = $all_records->trainings[0]['program'] ?? '';
         }

         return $html;
       })

       ->editColumn('status', function($all_records){
         $html = $all_records->status < 4 ? '' : $all_records->application_status();
         return $html;
       })

       ->addColumn('client_interview_status', function($all_records){
         $html = !isset($all_records->placements_status) ? '' : $all_records->placements_status();
         return $html;
       })
       ->make(true);

     }

     public function edit(Request $request){
       $edit = new ApplicantRecordController;
       return $edit->edit($request);
     }

     public function update(Request $request){

       $applicant = ApplicantRecord::whereId($request->applicant_id)->first();
       $applicant->va_level = $request->va_level;
       $applicant->status = $request->applicant_status;
       $applicant->address = $request->address;
       $applicant->mobile_number = $request->mobile_number;
       $applicant->landing_number = $request->landline_number;
       $applicant->save();

       if($request->client_interview_id){
         foreach($request->client_interview_id as $k=>$cin){

          $data = [
            'note' => $request->client_interview_note[$k],
            'client_id' => $request->client_id[$k],
            'va_status' => $request->client_va_status[$k],
            'va_pool' => $request->client_va_pool[$k],
            'status' => $request->client_interview_status[$k],
            'date' => date('Y-m-d h:i:s', strtotime($request->client_interview_sched[$k]))
          ];

          $new_interview_record = InterviewRecord::updateOrCreate(['id' => $cin], $data);
          $applicant->placements_status = $request->client_interview_status[$k];
          $applicant->save();

          $data2 = [
            'description' => $request->interview_description[$k]
          ];

          $job = JobRecord::updateOrCreate(['id' => $new_interview_record->job_id], $data2);
          $new_interview_record->update(['job_id' => $job->id]);

          $employee_job = EmployeeJob::updateOrCreate(['job_id' => $job->id], ['employee_id' => $request->applicant_id, 'job_id' => $job->id ]);

          $employee_record = EmployeeRecord::where('applicant_id', '=', $request->applicant_id)->first();
          if(!$employee_record){
            $new_employee = EmployeeRecord::create(['applicant_id' => $request->applicant_id, 'created_at' => date('Y-m-d h:i:s')]);
          }

          $client_applicant = ApplicantClientInterview::where('interview_id', '=', $new_interview_record->id)->first();

          if(!$client_applicant){
            ApplicantClientInterview::create(['applicant_id' => $request->applicant_id, 'interview_id' => $new_interview_record->id]);
          }
         }
       }


       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Updated Placement Record for Applicant ID: '.$applicant->name;
         $userlog->save($logged_user_id, $action);
       }

       return '1';
     }

     public function delete(Request $request){
       $id = $request->id;

       $applicant_client = ApplicantClientInterview::where('interview_id', '=', $id)->delete();
       $interview = InterviewRecord::whereId($id)->first();
       $job_id = $interview->job_id;
       $job = JobRecord::whereId($job_id)->delete();
       $interview->delete();

       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Deleted Job Record ID: '.$interview->id;
         $userlog->save($logged_user_id, $action);
       }

       return '1';
     }

     public function add_new_client(Request $request){
       $response = [];
       $data = [
         'name' => $request->client_name,
         'state' => $request->state,
         'timezone' => $request->tz
       ];
       $new_client = ClientRecord::updateOrCreate(['name'=> $request->client_name], $data);

       if($new_client){
         $response[0] = '1';
         $response[1] = $new_client;
       }

       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Added new client';
         $userlog->save($logged_user_id, $action);
       }

       return $response;
     }

     public function get_clients(Request $request){
       $clients = ClientRecord::where('name', '!=', '')->where('status', '=', 'active')->orderBy('name', 'asc')->get();
       $html = '';
       foreach($clients as $client){
         $html .= '<option value="'.$client->id.'">'.$client->name.'</option>';
       }

       return $html;
     }

     public function hired(Request $request){

       //return $request;

       $applicant = ApplicantRecord::whereId($request->id)->first();
       $client = ClientRecord::whereId($request->client)->first();
       $timezone = '';

       if($applicant->vd_email == ''){
         return 'VD Gmail email not found';
       }

       switch ($client->timezone){
         case 'PST':
         $timezone = 'Pacific Standard Time Zone';
         break;
         case 'MST':
         $timezone = 'Mountain Standard Time Zone';
         break;
         case 'CST':
         $timezone = 'Central Standard Time Zone';
         break;
         case 'EST':
         $timezone = 'Eastern Standard Time Zone';
         break;
         case 'HAWAII':
         $timezone = 'Hawaii Timezone';
         break;
       }

       Mail::send('emails.client-hired-email', ['applicant' => $applicant->name, 'timezone' => $timezone, 'position' => $request->position, 'date' => $request->date, 'client' => ucfirst($client->name)], function ($m) use ($applicant) {
         $m->from('placements@myvirtudesk.com', 'Placements Team | Virtudesk PH');
         $m->cc('placements@myvirtudesk.com', 'Placements Team | Virtudesk PH');
         $m->replyTo('placements@myvirtudesk.com');
         $m->to($applicant->vd_email, $applicant->name)->subject('Virtudesk Client Interview Invitation');
       });

       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Sent hired email to: '.$applicant->name;
         $userlog->save($logged_user_id, $action);
       }

       return '1';
     }

     public function create_hr_record(Request $request){
       $applicant = ApplicantRecord::whereId($request->id)->first();
       $applicant->workflow_status = 'hr_record';
       $applicant->save();

       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created HR Record for employee: '.$applicant->name;
         $userlog->save($logged_user_id, $action);
       }

       return '1';
     }
}
