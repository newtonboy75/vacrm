<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use App\InterviewRecord;
use App\ApplicantFirstInterview;
use App\ApplicantExperienceRecord;
use App\ApplicantExperience;
use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;
use App\ApplicantFinalInterview;
use App\ApplicantReference;
use App\ReferenceRecord;
use App\HomeEvaluationRecord;
use App\PhotoAddendum;
use App\Media;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\SystemCheckRecord;
use App\TrainingRecord;
use App\Http\Controllers\Backend\ApplicantRecordController;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\Cvs;
use App\Http\Controllers\Backend\EmailRecordController;
//use App\Jobs\SendEmailSourcingQueues;
use AWS;
use GuzzleHttp\Client;
use App\VatsSystem;
use Excel;
use App\Exports\UsersReferrerExport;
use Khill\Lavacharts\Lavacharts;
/**
* Class SourcingRecordController.
*/
class SourcingRecordController extends Controller
{
  /**
  * @return \Illuminate\View\View
  */
  public function index(){


    $priority_lists = VatsSystem::all();
    $lows = [];
    $medium = [];
    $high = [];
    $highest = [];

    foreach($priority_lists as $list){

      if($list->keywords_count <= 4){
        $lows[] = $list->id;
      }elseif($list->keywords_count >= 5 && $list->keywords_count <= 7){
        $medium[] = $list->id;
      }elseif($list->keywords_count >= 8 && $list->keywords_count <= 14){
        $high[] = $list->id;
      }else{
        $highest[] = $list->id;
      }

    }
    //dd(count($lows));

    $lava = new Lavacharts; // See note below for Laravel

    $datatable = $lava->DataTable();
    $datatable->addNumberColumn('Lowest')->addNumberColumn('Low')->addNumberColumn('Medium')->addNumberColumn('High')->addNumberColumn('Highest');
    $datatable->addRow([123, count($lows), count($medium), count($high), count($highest)]);

    $lava->BarChart('BcharStat', $datatable, [

        'hAxis' => [
            'title' => 'Ranks'
        ],
        'vAxis' => [
            'title' => 'Applicants'
        ],
    ])

    ->width(900);


    return view('backend.sourcing-record', compact('lava'));

  }

  public function edit(Request $request){
    $edit = new ApplicantRecordController;
    return $edit->edit($request);
  }

  public function store(Request $request){
    dd($request);
  }

  public function update(Request $request, ApplicantRecord $applicant_record, ApplicantFirstInterview $initial_interview, ApplicantExperience $experience){
    DB::connection()->disableQueryLog();

    if($request->sourcing_source_id == '0' && $request->sourcing_application_status == '2'){
      return 'Please choose a recruiter';
    }

    $workflow_status = [];

    $position_applied = ['General Virtual Assistant' => 'gva', 'Transaction Coordinator' => 'tc', 'Inside Sales Associate' => 'isa', 'Executive Virtual Assistant' => 'eva'];

    //Mark archived 3 = pending
    $archived = $request->sourcing_application_status == '8' ? '3' : '0';

    if($request->sourcing_application_status == '3' || $request->sourcing_application_status == '8'){
      $archived = '1';
      $vats = VatsSystem::where('user_id', $request->applicant_id)->update(['status' => '1']);
    }elseif($request->sourcing_application_status == '0'){
      $archived = '0';
      $vats = VatsSystem::where('user_id', $request->applicant_id)->update(['status' => '0']);
    }

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
    }

    $redflag = isset($request->sourcing_red_flag)  ?  '1' : '0';

    if($redflag == '1'){
       $archived = '1';
       $vats = VatsSystem::where('user_id', $request->applicant_id)->update(['status' => '1']);
    }

    $data = [
      'address' => ucwords($request->sourcing_address),
      'come_from' => $request->sourcing_come_from,
      'email' => $request->sourcing_applicant_email,
      'landing_number' => $request->sourcing_landline_number,
      'mobile_number' => $request->sourcing_mobile_number,
      'name' => ucwords($request->sourcing_applicant_name),
      'notes' => $request->sourcing_notes,
      'position_applying_for' => $request->sourcing_position_applying_for,
      'recruiter_id' => $request->sourcing_recruiter_id,
      'red_flag' => $redflag,
      'red_flag_notes' => $request->sourcing_red_flag_notes,
      'skype_id' => $request->sourcing_skype_id,
      'sourcer_id' => $logged_user_id,
      'recruiter_id' => $request->sourcing_source_id,
      'status' => $request->sourcing_application_status,
      'archived' => $archived,
      'has_real_estate_exp' => $request->has_real_estate_exp
    ];

    if(isset($request->sourcing_initial_interview_date) || $request->sourcing_initial_interview_date != ''){

      $initial_interview_exists = $initial_interview->where('applicant_id', '=', $request->applicant_id)->first();

      if($initial_interview_exists){

        $interview = InterviewRecord::find($initial_interview_exists->interview_id);
        $interview->date = date('Y-m-d H:i:s', strtotime($request->sourcing_initial_interview_date));
        $interview->status = '5';
        $interview->type = 'initial';
        $interview->recruiter_id = $request->sourcing_source_id;
        $interview->save();
        unset($interview);
      }else{

        $interview = new InterviewRecord();
        $interview->date = date('Y-m-d H:i:s', strtotime($request->sourcing_initial_interview_date));
        $interview->type = 'initial';
        $interview->status = '5';
        $interview->recruiter_id = $request->sourcing_source_id;
        $interview->save();
        $initial_interview->create(['interview_id' => $interview->id, 'applicant_id' => $request->applicant_id]);
        unset($interview);
      }
    }

    $save_applicant = $applicant_record->where('id', '=', $request->applicant_id)->update($data);

    if(isset($request->exp_company_name)){
      foreach($request->exp_company_name as $k=>$v){

        if(isset($request->exp_id[$k])){
          $experience_record = ApplicantExperienceRecord::whereId($request->exp_id[$k])->first();
          $experience_record->company_name = $request->exp_company_name[$k];
          $experience_record->position = $request->exp_job_pos[$k];
          $experience_record->job_responsibilities = $request->exp_job_resp[$k];
          $experience_record->start_date = $request->exp_date_start[$k];
          $experience_record->end_date = $request->exp_date_end[$k];
          $experience_record->save();
          unset($experience_record);
        }else{
          $experience_record = new ApplicantExperienceRecord();
          $experience_record->company_name = $request->exp_company_name[$k];
          $experience_record->position = $request->exp_job_pos[$k];
          $experience_record->job_responsibilities = $request->exp_job_resp[$k];
          $experience_record->start_date = $request->exp_date_start[$k];
          $experience_record->end_date = $request->exp_date_end[$k];
          $experience_record->save();
          $experience->create(['experience_id' => $experience_record->id, 'applicant_id' => $request->applicant_id]);
          unset($experience);
          unset($experience_record);
        }
      }
    }

    if(isset($request->emp_history_employer)){


      $employment_history = [
        'employer' => $request->emp_history_employer,
        'position' => $request->emp_position,
        'date_employment' => array($request->date_employment_from, $request->date_employment_to),
        'responsibilities' => $request->emp_responsibilities,
      ];

      //return $employment_history;
      //exit;

      $vats = VatsSystem::where('user_id', $request->applicant_id)->first();
      //$vats->blog = $request->blog;
      //$vats->employment_history = serialize($employment_history);
      //$vats->save();

      $emp_histories = serialize($employment_history);

      $vats = VatsSystem::where('user_id', $request->applicant_id)->update(['employment_history' => $emp_histories, 'blog' => $request->blog]);

      //return $vats;

    }

    $app = $applicant_record->whereId($request->applicant_id)->first();

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Updated/created Sourcing Record for applicant: '.$app->name;
      $userlog->save($logged_user_id, $action);
      unset($userlog);
    }
    unset($app);
    unset($request);
    unset($data);
    unset($applicant_record);
    return $save_applicant;

  }

  public function delete(Request $request){
    $ex_id = ApplicantExperienceRecord::whereId($request->id)->delete();
    $app_ex_id = ApplicantExperience::where('experience_id', '=', $request->id)->delete();
    $vats = VatsSystem::where('user_id', '=', $request->id)->delete();

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Deleted Experience record ID: '.$request->id;
      $userlog->save($logged_user_id, $action);
      unset($userlog);
    }

    unset($ex_id);
    unset($request);
    unset($vats);

    return $app_ex_id;
  }


  public function sendEmail(Request $request){

    //$userlog = new UserLogController;
    $vats = VatsSystem::where('user_id', $request->user_id)->update(['status' => '1']);


    $logged_user_id = Auth::id();

    $url = 'https://services.myvirtudesk.com/api/send_sourcing';

    $client = new Client(); //GuzzleHttp\Client
    $result = $client->post($url, [
        'form_params' => [
        'email' => 'newtonboy@gmail.com',
        'password' => 'renton75',
        'allrec' => $request->all(),
        'logged_user_id' => $logged_user_id
          ]
    ]);

    //$r = json_encode($result->getBody()->getContents());

    //return json_decode($r);

    return '1';
    exit;

    //old implementation don't delete
    /***
    DB::connection()->disableQueryLog();
    $applicant_record = ApplicantRecord::whereId($request->user_id)->first();
    $success = '0';
    $to = $request->to;
    $body = $request->email_body;

    $subject = '';
    $dtype = '';
    $sent = 'sent';
    $cvsfile = null;
    $new_request = null;
    $disposition = '';

    $new_request = $request->all();
    if($request->type == 'initial_interview'){

      //$new_request = $request->all();

      //old implementation position
      //SendEmailSourcingQueues::dispatchNow($new_request, 'invitation');

       $applicant_record = ApplicantRecord::whereId($request['user_id'])->first();

        if($applicant_record){
          $applicant_record->workflow_status = 'talent_acquisition';
          $applicant_record->save();
          $success = '1';
        }

      $cv_filename = Cvs::where('applicant_id', '=', $request->user_id)->first();

      if($cv_filename){
        $url = env('VD_URL').'/passed-app/'.$cv_filename->file_name;
            try{
                $cvsfile = file_get_contents($url);
            } catch(\Exception $e){
                $cvsfile = null;
            }
      }

      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Sent email and applied transition talent_acquisition to applicant '.$applicant_record->name;
        $userlog->save($logged_user_id, $action);
      }

      $disposition = 'invitation';
      unset($applicant_record);

    }else{

      //$subject = 'Virtudesk Application | Application Result';

      $new_request = $request->all();
      //old position
      //SendEmailSourcingQueues::dispatchNow($new_request, 'result');

      $applicant_record = ApplicantRecord::whereId($request['user_id'])->first();

       if($applicant_record){
          $applicant_record->archived = '1';
          $applicant_record->save();
          $success = '1';
        }

      $cv_filename = Cvs::where('applicant_id', '=', $request->user_id)->first();

      if($cv_filename){
        //$url = env('VD_URL').'/failed-app/'.$cv_filename->file_name; transfer cv even if applicant failed
        $url = env('VD_URL').'/passed-app/'.$cv_filename->file_name;
         try{
                $cvsfile = file_get_contents($url);
            } catch(\Exception $e){
                $cvsfile = null;
            }
      }

      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Sent failed and archived user: '.$applicant_record->name;
        $userlog->save($logged_user_id, $action);
         unset($applicant_record);
      }

      $disposition = 'result';
      //SendEmailSourcingQueues::dispatchNow($new_request, 'result');
      $success = '1';
    }

    $save_email = new EmailRecordController();
    $save_email->save($to,	'jobs@myvirtudesk.com',	$logged_user_id,	$subject,	$body,	$sent, 'sourcing', $request->type);

    SendEmailSourcingQueues::dispatchNow($new_request, $disposition);

    unset($save_email);
    unset($new_request);
    unset($cvsfile);

    return '1';
    ***/
  }

  public function deleteApplication(){

    $applicant = ApplicantRecord::where('id', '=', request()->id)->first();
    $vats = VatsSystem::where('user_id', '=', request()->id)->delete();

    if($applicant){
      $app = ApplicantRecord::find(request()->id)->delete();
      $cv_filename = Cvs::where('applicant_id', '=', $applicant->id)->first();

      if($cv_filename){
        $url = env('VD_URL').'/failed-app/'.$cv_filename->file_name;
         try{
                $cvsfile = file_get_contents($url);
            } catch(\Exception $e){
                $cvsfile = null;
            }
      }
    }

    unset($applicant);
    unset($cvsfile);
    return '1';
  }

  public function cvUpload(Request $request){

    if($request->cv_file){
      $file = $request->cv_file;
      $filename = time().'_'.$file->getClientOriginalName();
      $file_name = $filename;

      if($file){
        $uploaded =  $file->move(public_path('/uploaded/'), $filename);
        //$file_id = Cvs::updateOrCreate(['applicant_id' => $request->applicant_id], ['file_name' => $file_name]);
        $file_id = Cvs::where('applicant_id', '=', $request->applicant_id)->first();

        $dfile = public_path().'/uploaded/'.$file_name;
        $s3key = 'resumes/'.$file_name;

          $s3 = AWS::createClient('s3');
          $s3->putObject(array(
              'Bucket'     => env('AWS_BUCKET'),
              'Key'        => $s3key,
              'SourceFile' => $dfile,
          ));


        if($file_id){
          unlink($uploaded);
          $file_id->file_name = $file_name;
          $file_id->save();
        }else{
          $file_id = Cvs::create(['applicant_id' => $request->applicant_id, 'file_name' => $file_name]);
        }
      }

      unset($request);
      return $file_id;
    }
  }







//end here
}
