<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Backend\UserLogController;
use App\EmployeeCheckInRecord;
use App\CheckInRecord;

class PdfCreatorController extends Controller
{
  public function va_checkin(Request $request){

    $date = date('ymdhis');
    $html_file = public_path().'/form_templates/client_checkin_template.html';
    $newfile = public_path().'/form_templates/client_checkin_template_'.$date.'.html';

    $vaname = '$vaname';
    $client = '$client';
    $status = '$status';
    $strength = '$strength';
    $opportunity = '$opportunity';
    $action = '$action';

    if (copy($html_file, $newfile)) {
      $html = file_get_contents($newfile);
      $html = str_replace($vaname, $request->vaname, $html);
      $html = str_replace($client, $request->client, $html);
      $html = str_replace($status, $request->status, $html);
      $html = str_replace($opportunity, $request->opportunity, $html);
      $html = str_replace($strength, $request->strength, $html);
      $html = str_replace($action, $request->action, $html);

      file_put_contents($newfile, $html);
      $pdf = \PDF::loadFile($newfile)->save(public_path().'/form_templates/client_checkin_template_'.$request->id.'_'.$date.'.pdf')->stream('download.pdf');

      $db_filename = 'client_checkin_template_'.$request->id.'_'.$date.'.pdf';

      unlink($newfile);

      return $db_filename;
    }
    return '0';
  }


  public function send_final_notice(Request $request){

    //dd($request->body);
    $date = date('ymdhis');
    $html_file = public_path().'/form_templates/final_notice.html';
    $newfile = public_path().'/form_templates/final_notice'.$date.'.html';

    $datepdf = '$date';
    $body = '$body';
    $recipient = '$recipient';
    $src = 'src="/';

    if (copy($html_file, $newfile)) {
      $html = file_get_contents($newfile);
      $html = str_replace($datepdf, date('M d, Y'), $html);
      $html = str_replace($body, $request->body, $html);
      $html = str_replace($recipient, $request->recipient_name, $html);
      $html = str_replace($src, 'src="'.$_SERVER['DOCUMENT_ROOT'].'/', $html);

      file_put_contents($newfile, $html);
      $pdf = \PDF::loadFile($newfile)->save(public_path().'/form_templates/final_notice_'.$request->id.'_'.$date.'.pdf')->stream('download.pdf');

      $db_filename = 'final_notice_'.$request->id.'_'.$date.'.pdf';
      $downloadfile = public_path().'/form_templates/final_notice_'.$request->id.'_'.$date.'.pdf';

      unlink($newfile);

      //return $db_filename;
      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Created esig contract: Final Notice';
        $userlog->save($logged_user_id, $action);
      }

      $headers = array(
              'Content-Type: application/pdf',
            );

    return \Response::download($downloadfile, $db_filename, $headers);
    }
    return '0';
  }

  public function uploader(Request $request){

      move_uploaded_file($_FILES["upload"]["tmp_name"],
      public_path().'/temp_uploads/'.$_FILES["upload"]["name"]);
      return response()->json([ 'fileName' => $_FILES["upload"]["name"], 'uploaded' => true, 'url' => '/temp_uploads/'.$_FILES["upload"]["name"], ]);

  }

}
