<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use App\InterviewRecord;
use App\ApplicantFirstInterview;
use App\ApplicantExperienceRecord;
use App\ApplicantExperience;
use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;
use App\ApplicantFinalInterview;
use App\ApplicantReference;
use App\ReferenceRecord;
use App\HomeEvaluationRecord;
use App\PhotoAddendum;
use App\Media;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\SystemCheckRecord;
use App\Http\Controllers\Backend\ApplicantRecordController;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Backend\EmailRecordController;
use App\UserLog;
use App\HomeEvalNotes;

/**
* Class SourcingRecordController.
*/
class HomeEvaluationRecordController extends Controller
{
  /**
  * @return \Illuminate\View\View
  */
  public function index(ApplicantRecord $sourcingRecord){
    $all_records = $sourcingRecord->where('workflow_status', '=', 'home_evaluation')->get();
    return view('backend.home-evaluation')->withHomeEval($all_records);
  }

  public function load(){
    $tab = request()->tab;
    $stats = request()->archived;

    if(is_array($tab)){
      $all_records = ApplicantRecord::whereIn('workflow_status', $tab)->where('archived', '<=', $st)->orderBy('updated_at', 'desc')->orderBy('id', 'desc')->get();
    }else{
      $all_records = ApplicantRecord::where('workflow_status', 'home_eval')->where('archived', '<=', $st)->orderBy('updated_at', 'desc')->orderBy('id', 'desc')->get();
    }

    return datatables()->of($all_records)

    ->editColumn('home_eval_result', function($all_records){
      $html = '';

      if($all_records->home_evaluation){
        $her = HomeEvaluationRecord::whereId($all_records->home_evaluation)->orderBy('id', 'desc')->first();
        if($her){
          $html = $her->he_status();
        }
      }
      return $html;
    })

    ->editColumn('created_at', function($all_records){
      $html = date('M d Y, h:i', strtotime($all_records->created_at));
      return $html;
    })

    ->addColumn('transition_date', function($all_records){
      $html = '';
      $log = UserLog::where('activity', 'REGEXP', 'Home Evaluation')->where('activity', 'REGEXP', $all_records->id)->orderBy('id', 'desc')->first();
      if($log){
        $html = date('M d, Y h:i', strtotime($log->date_created));
      }

      return $html;
    })

    ->make(true);
  }

  public function edit(Request $request){
    $edit = new ApplicantRecordController;
    return $edit->edit($request);
  }

  public function update(Request $request, HomeEvaluationRecord $home_evals, SystemCheckRecord $syscheck){

    $applicant_id = $request->user_id;
    $applicant = ApplicantRecord::whereId($applicant_id)->first();
    $home_eval = $home_evals->whereId($applicant->home_evaluation)->first();

    $no_archive = ['2', '4', '5', '6'];

    if(in_array($request->result, $no_archive)){
      $applicant->archived = '3';
      $applicant->save();
      return 'Saved';
      exit;
    }else{
      $applicant->archived = '0';
      $applicant->save();
    }

    if(!isset($applicant->home_evaluation)){
      return 'Cannot save empty home evaluation fields';
      exit;
    }

    $new_photo_addendum = PhotoAddendum::where('owner', '=', $applicant_id)->first();
    $new_photo_addendum->owner = $applicant_id;
    $new_photo_addendum->exterior = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-exterior')->first()->id ?? null;
    $new_photo_addendum->headset = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-headset')->first()->id ?? null;
    $new_photo_addendum->workstation = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-workstation')->first()->id ?? null;

    $new_photo_addendum->backup_power_source = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-backup_power_source')->first()->id ?? null;

    $new_photo_addendum->main_pc = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-main_pc')->first()->id ?? null;
    $new_photo_addendum->main_pc_specs = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-main_pc_specs')->first()->id ?? null;
    $new_photo_addendum->backup_pc = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-backup_pc')->first()->id ?? null;
    $new_photo_addendum->backup_pc_specs = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-backup_pc_specs')->first()->id ?? null;
    //$new_photo_addendum->main_isp = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-main_isp')->first()->id;
    //$new_photo_addendum->main_isp_speed_test = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-main_isp_speed_test')->first()->id;
    //$new_photo_addendum->backup_isp = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-backup_isp')->first()->id;
    //$new_photo_addendum->backup_isp_speed_test = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-backup_isp_speed_test')->first()->id;
    $new_photo_addendum->isp_receipt = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_receipt')->first()->id ?? null;
    $new_photo_addendum->isp_bill = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_bill')->first()->id ?? null;
    $new_photo_addendum->isp_authorization_letter = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_authorization_letter')->first()->id ?? null;
    $new_photo_addendum->isp_owner_id = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_owner_id')->first()->id ?? null;
    $new_photo_addendum->webcamera = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-webcamera')->first()->id ?? null;
    $new_photo_addendum->save();
    $photo_addendum_id = $new_photo_addendum->id;

    $data2 = [
      'main_ram_proc_type' => $request->main_ram_proc_type,
      'backup_ram_proc_type' => $request->backup_ram_proc_type,
      'type_computer_backup' => $request->type_computer_backup,
      'backup_availability' => $request->backup_availability,
      'owner_backup' => $request->owner_backup,
      'backup_owner_name' => $request->backup_owner_name,
      'owner_backup_relation' => $request->owner_backup_relation,
      'how_long_backup_system' => $request->how_long_backup_system,
      'isp' => $request->isp,
      'main_speed_plan' => $request->main_speed_plan,
      'type_connection_main' => $request->type_connection_main,
      'type_computer' => $request->type_computer,
      'other_connection_owner' => $request->other_connection_owner,
      'account_owner_relation' => $request->account_owner_relation,
      'upgrade_internet' => $request->upgrade_internet,
      'eta' => $request->eta,
      'dl1' => $request->dl1,
      'dl2' => $request->dl2,
      'dl3' => $request->dl3,
      'ul1' => $request->ul1,
      'ul2' => $request->ul2,
      'ul3' => $request->ul3,
      'speedtest_link1' => $request->speedtest_link1,
      'speedtest_link2' => $request->speedtest_link2,
      'speedtest_link3' => $request->speedtest_link3,
      'backup_isp' => $request->backup_isp,
      'connection_owner2' => $request->connection_owner2,
      'isp_owner_2' => $request->isp_owner_2,
      'isp_backup_availabilitiy' => $request->isp_backup_availabilitiy,
      'how_long' => $request->how_long,
      'backup_dl1' => $request->backup_dl1,
      'backup_dl2' => $request->backup_dl2,
      'backup_dl3' => $request->backup_dl3,
      'backup_ul1' => $request->backup_ul1,
      'backup_ul2' => $request->backup_ul2,
      'backup_ul3' => $request->backup_ul3,
      'backup_speedtest_link1' => $request->backup_speedtest_link1,
      'backup_speedtest_link2' => $request->backup_speedtest_link2,
      'backup_speedtest_link3' => $request->backup_speedtest_link3,
      'backup_power_source' => $request->backup_power_source,
      'owner_backup_source' => $request->owner_backup_source,
      'owner_backup_power_owner_name' => $request->owner_backup_power_owner_name,
      'availability_backup_source' => $request->availability_backup_source,
      'backup_power_how_long' => $request->backup_power_how_long,
      'web_camera' => $request->web_camera,
      'web_camera_brand' => $request->web_camera_brand,
      'backup_system_proc' => $request->backup_system_proc,
      'backup_system_ram' => $request->backup_system_ram,
      'main_system_proc' => $request->main_system_proc,
      'main_system_ram' => $request->main_system_ram,
      'connection_owner' => $request->connection_owner,
      'home_eval_id' => $applicant->home_evaluation
    ];

    $new_system_record = SystemCheckRecord::updateOrCreate(['home_eval_id' => $home_eval->id], $data2);
    $system_record_id = $new_system_record->id;

    if($home_eval){
      //$home_eval->accommodation_type = $request->accommodation_type;
      //$home_eval->living_duration = $request->living_duration;
      //$home_eval->commoners_count = $request->commoners_count;
      //$home_eval->kids = $request->kids;
      //$home_eval->kids_take_care = $request->kids_take_care;
      //$home_eval->kind_of_neighborhood = $request->kind_of_neighborhood;
      //$home_eval->power_outage = $request->power_outage;
      //$home_eval->bad_weather_flooded = $request->bad_weather_flooded;
      //$home_eval->family_opinion = $request->family_opinion;
      //$home_eval->dedicate_space = $request->dedicate_space;
      //$home_eval->free_distractions = $request->free_distractions;
      //$home_eval->lighting_and_ventilation = $request->lighting_and_ventilation;
      //$home_eval->organized_workplace = $request->organized_workplace;
      //$home_eval->save();

      $data = [
        'accommodation_type' => $request->accommodation_type,
        'living_duration' => $request->living_duration,
        'commoners_count' => $request->commoners_count,
        'kids' => $request->kids,
        'kids_take_care' => $request->kids_take_care,
        'kind_of_neighborhood' => $request->kind_of_neighborhood,
        'power_outage' => $request->power_outage,
        'bad_weather_flooded' => $request->bad_weather_flooded,
        'flood_plan' => $request->flood_plan,
        'place_setup' => $request->place_setup,
        'family_opinion' => $request->family_opinion,
        'dedicate_space' => $request->dedicate_space,
        'free_distractions' => $request->free_distractions,
        'lighting_and_ventilation' => $request->lighting_and_ventilation,
        'organized_workplace' => $request->organized_workplace,
        'result' => $request->result,
        'system_check' => $system_record_id,
        'photo_addendum' => $photo_addendum_id,
        'describe_space' => $request->describe_space,
      ];

      $home_eval = HomeEvaluationRecord::updateOrCreate(['photo_addendum' => $photo_addendum_id], $data);

      $del_homeeval_note = HomeEvalNotes::where('home_eval_id', '=', $home_eval->id)->delete();

      foreach($request->notes as $note){
        if(isset($note)){
          $note_data = [
            'home_eval_id' => $home_eval->id,
            'note' => $note
          ];

          $he_note = HomeEvalNotes::create($note_data);
        }
      }

      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Updated/created Home Evaluation Record for: '.$request->applicant_id;
        $userlog->save($logged_user_id, $action);
      }



      /***
      $system_check = $syscheck->where('id', '=', $home_eval->system_check)->first();
      $system_check->type_computer = $request->type_computer;
      $system_check->power_source_main = $request->power_source_main;
      $system_check->power_source_backup = $request->power_source_backup;
      $system_check->main_pc_processor = $request->main_pc_processor;
      $system_check->backup_pc_processor = $request->backup_pc_processor;
      $system_check->main_pc_ram = $request->main_pc_ram;
      $system_check->backup_pc_ram = $request->backup_pc_ram;
      $system_check->main_ram_proc_type = $request->main_ram_proc_type;
      $system_check->isp = $request->isp;
      $system_check->connection_owner = $request->connection_owner;
      $system_check->other_connection_owner = $request->other_connection_owner;
      $system_check->type_connection = $request->type_connection;
      $system_check->speedtest_main = $request->speedtest_main;
      $system_check->speedtest_backup = $request->speedtest_backup;
      $system_check->power_source_backup_length = $request->power_source_backup_length;
      $system_check->internet_backup = $request->internet_backup;
      $system_check->main_power_transition = $request->main_power_transition;
      $system_check->main_connection_transition = $request->main_connection_transition;
      $system_check->num_internet_shared = $request->num_internet_shared;
      $system_check->web_camera = $request->web_camera;
      $system_check->web_cam_brand = $request->web_cam_brand;

      $system_check->save();
      ***/

      return '1';

    }
  }

  public function uploadImage(Request $request)
  {


    $img = Media::whereId($request->id)->first();

    $from = $request->from;
    $uploadedFile = $request->file('file');

    $filename = $img->name;
    $i = date("M_D_Y_h:i_a_");
    $user_dir = public_path('/uploaded/home_eval').'/user_'.$request->user_id;
    $uploaded_file = $request->file->move($user_dir, $filename);
    copy('uploaded/home_eval/user_'.$request->user_id.'/'.$filename, $user_dir.'/'.$request->user_id.'-'.$request->from.'.'.$uploadedFile->getClientOriginalExtension());

    if($img){
      $img->name = $filename;
      $img->save();
    }else{
      $img_id = Media::create(['name' => $filename]);
      $photo_addendum = PhotoAddendum::whereId($request->file_id)->first();
      if($photo_addendum){
        $photo_addendum->$from = $img_id->id;
        $photo_addendum->save();
      }
    }

    return '../../../uploaded/home_eval/user_'.$request->user_id.'/'.$request->user_id.'-'.$request->from.'.'.$uploadedFile->getClientOriginalExtension();

  }

  public function sendEmail(Request $request){

    $success = '0';
    $to = $request->to;
    $body = $request->email_body;
    $from = 'requirements@myvirtudesk.com';
    $dtype = 'home_eval';
    $status = 'sent';
    $subject = '';

    if($request->type == 'congratulatory'){
      $subject = 'IMPORTANT: PLEASE READ THE WHOLE EMAIL - Virtudesk - Application Result';
      Mail::send([], [], function ($message) use($to, $body, $from, $subject){
        $message->to($to)
        ->from($from, 'MyVirtudesk')
        ->cc($from, 'MyVirtudesk')
        ->subject($subject)
        ->replyTo('requirements@myvirtudesk.com')
        ->setBody($body, 'text/html');
      });

      if (count(Mail::failures()) > 0 ) {
        $success = '0';
        $status = 'failed';
      }

      $applicant_record = ApplicantRecord::whereId($request->user_id)->first();

      //if($applicant_record){
        //$applicant_record->workflow_status = 'pre_training';
        //$applicant_record->save();
        //$success = '1';
     // }

     if (Auth::check()){
       $userlog = new UserLogController;
       $logged_user_id = Auth::id();
      $action = 'Sent Requirements success email for: '.$request->user_id;
        $userlog->save($logged_user_id, $action);
      }
      
      $success = '1';

    }else{

      $success = '1';
      $subject = 'IMPORTANT: PLEASE READ THE WHOLE EMAIL - Virtudesk - Reference Check Result';
      Mail::send([], [], function ($message) use($to, $body, $from, $subject){
        $message->to($to)
        ->from($from, 'MyVirtudesk')
        ->cc($from, 'MyVirtudesk')
        ->replyTo('home.evaluators@myvirtudesk.com')
        ->subject($subject)
        ->setBody($body, 'text/html');
      });

      if (count(Mail::failures()) > 0 ) {
        $success = '0';
        $status = 'failed';
      }

      $applicant_record = ApplicantRecord::whereId($request->user_id)->first();
      if($applicant_record){
        $applicant_record->workflow_status = 'home_eval';
        $applicant_record->save();
        $success = '1';
      }


      if (Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Sent Home Evaluation fail email for: '.$request->user_id;
        $userlog->save($logged_user_id, $action);
      }


    }

    $save_email = new EmailRecordController();
    $save_email->save($to, $from,	$logged_user_id,	$subject,	$body,	$status, $dtype, $request->type);

    return $success;

  }

  public function create_remote_home(Request $request){

    $eval = json_decode($request->body);

    $applicant_id = $eval->applicant_id;
    //Log::info('dito po');
    $applicant_pa = ApplicantRecord::whereId($applicant_id)->first();

    $file = $request->file;
    $filename = $file->getClientOriginalName();

    //extract all images
    if($file){
      $uploaded =  $file->move(public_path('/uploaded/home_eval'), $filename);
      $user_dir = public_path('/uploaded/home_eval').'/user_'.$eval->applicant_id;
      $dfile = public_path('/uploaded/home_eval').'/'.$filename;

      if(!File::exists($user_dir)) {
        $dir = File::makeDirectory($user_dir);
      }else{
        //exec("rmdir -r $user_dir");
        $dfiles = glob($user_dir.'/*'); //get all file names
      	foreach($dfiles as $filed){
      	    if(is_file($filed))
      	    unlink($filed); //delete file
      	}
      }

      exec("cd $user_dir && unzip -o -j $dfile");
      unlink($uploaded);

      $user_files = array_diff(scandir($user_dir), array('..', '.'));
      $new_media_arr = [];

      //save all images to db
      foreach ($user_files as $imgk => $image) {

        $img = explode('.', $image);

        $media = Media::where('name', 'REGEXP', $img[0])->orderBy('id', 'asc')->first();
        if($media){
          $media->name = $image;
          $media->save();
          $new_media_arr[$media->id] = $image;
        }else{
          $new_media = Media::create(['name' => $image]);
          $new_media_arr[$new_media->id] = $image;
        }
      }


      if($applicant_pa){
        $home_evaluation = HomeEvaluationRecord::whereId($applicant_pa->home_evaluation)->first();
        //$new_photo_addendum = PhotoAddendum::where('owner', '=', $applicant_id)->first();

        $photo_data = [];
        $photo_data['owner'] = $applicant_id;

        if(null !== $applicant_id.'-exterior'){
          $photo_data['exterior'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-exterior')->orderBy('id', 'asc')->first()->id ?? '';
        }


        if(null !== $applicant_id.'-headset'){
          $photo_data['headset'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-headset')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-workstation'){
          $photo_data['workstation'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-workstation')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-backup_power_source'){
          $photo_data['backup_power_source'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-backup_power_source')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-main_pc'){
          $photo_data['main_pc'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-main_pc')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-main_pc_specs'){
          $photo_data['main_pc_specs'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-main_pc_specs')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-backup_pc'){
          $photo_data['backup_pc'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-backup_pc')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-backup_pc_specs'){
          $photo_data['backup_pc_specs'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-backup_pc_specs')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-isp_receipt'){
          $photo_data['isp_receipt'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_receipt')->orderBy('id', 'asc')->first()->id ?? '';
        }
        Log::info($photo_data);

        if(null !== $applicant_id.'-isp_bill'){
          $photo_data['isp_bill'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_bill')->whereNotNull('id')->orderBy('id', 'asc')->first()->id ?? '';
        }


        if(null !== $applicant_id.'-isp_authorization_letter'){
          $photo_data['isp_authorization_letter'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_authorization_letter')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-isp_owner_id'){
          $photo_data['isp_owner_id'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_owner_id')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-webcamera'){
          $photo_data['webcamera'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-webcamera')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-isp_main'){
          $photo_data['main_isp'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_main')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(null !== $applicant_id.'-isp_backup'){
          $photo_data['backup_isp'] = Media::select('id')->where('name', 'REGEXP', $applicant_id.'-isp_backup')->orderBy('id', 'asc')->first()->id ?? '';
        }

        if(count($photo_data) > 1){

          $final_photo_data = array_filter($photo_data, 'strlen');

          $new_photo_addendum = PhotoAddendum::updateOrCreate(['owner' => $applicant_id], $final_photo_data);
        }

        //Log::info($photo_data);
        $photo_addendum_id = $new_photo_addendum->id ?? '';

        $data2 = [
          'main_ram_proc_type' => $eval->main_ram_proc_type,
          'backup_ram_proc_type' => $eval->backup_ram_proc_type,
          'type_computer_backup' => $eval->type_computer_backup,
          'backup_availability' => $eval->backup_availability,
          'owner_backup' => $eval->owner_backup,
          'backup_owner_name' => $eval->backup_owner_name,
          'owner_backup_relation' => $eval->owner_backup_relation,
          'how_long_backup_system' => $eval->how_long_backup_system,
          'isp' => $eval->isp,
          'main_speed_plan' => $eval->main_speed_plan,
          'type_connection_main' => $eval->type_connection_main,
          'type_computer' => $eval->type_computer,
          'other_connection_owner' => $eval->other_connection_owner,
          'account_owner_relation' => $eval->account_owner_relation,
          'upgrade_internet' => $eval->upgrade_internet,
          'eta' => $eval->eta,
          'dl1' => $eval->dl1,
          'dl2' => $eval->dl2,
          'dl3' => $eval->dl3,
          'ul1' => $eval->ul1,
          'ul2' => $eval->ul2,
          'ul3' => $eval->ul3,
          'speedtest_link1' => $eval->speedtest_link1,
          'speedtest_link2' => $eval->speedtest_link2,
          'speedtest_link3' => $eval->speedtest_link3,
          'backup_isp' => $eval->backup_isp,
          'connection_owner2' => $eval->connection_owner2,
          'isp_owner_2' => $eval->isp_owner_2,
          'isp_backup_availabilitiy' => $eval->isp_backup_availabilitiy,
          'how_long' => $eval->how_long,
          'backup_dl1' => $eval->backup_dl1,
          'backup_dl2' => $eval->backup_dl2,
          'backup_dl3' => $eval->backup_dl3,
          'backup_ul1' => $eval->backup_ul1,
          'backup_ul2' => $eval->backup_ul2,
          'backup_ul3' => $eval->backup_ul3,
          'backup_speedtest_link1' => $eval->backup_speedtest_link1,
          'backup_speedtest_link2' => $eval->backup_speedtest_link2,
          'backup_speedtest_link3' => $eval->backup_speedtest_link3,
          'backup_power_source' => $eval->backup_power_source,
          'owner_backup_source' => $eval->owner_backup_source,
          'owner_backup_power_owner_name' => $eval->owner_backup_power_owner_name,
          'availability_backup_source' => $eval->availability_backup_source,
          'backup_power_how_long' => $eval->backup_power_how_long,
          'web_camera' => $eval->web_camera,
          'web_camera_brand' => $eval->web_camera_brand,
          'backup_system_proc' => $eval->backup_system_proc,
          'backup_system_ram' => $eval->backup_system_ram,
          'main_system_proc' => $eval->main_system_proc,
          'main_system_ram' => $eval->main_system_ram,
          'connection_owner' => $eval->connection_owner,
          //'home_eval_id' => $applicant_pa->home_evaluation,
          'headset' => $eval->headset,
          'headset_brand' => $eval->headset_brand,
          'backup_system_os' => $eval->backup_system_os,
          'main_system_os' => $eval->main_system_os,

        ];

        if(!$home_evaluation){
          $new_system_record = SystemCheckRecord::create($data2);
        }else{
          $new_system_record = SystemCheckRecord::where('home_eval_id', $home_evaluation->id)->first();

          if($new_system_record){
          	$new_system_record->update($data2);
          }else{
          	$new_system_record = SystemCheckRecord::create($data2);
          }



        }

        $data = [
          'accommodation_type' => $eval->accommodation_type,
          'living_duration' => $eval->living_duration,
          'commoners_count' => $eval->commoners_count,
          'kids' => $eval->kids,
          'kids_take_care' => $eval->kids_take_care,
          'kind_of_neighborhood' => $eval->kind_of_neighborhood,
          'power_outage' => $eval->power_outage,
          'bad_weather_flooded' => $eval->bad_weather_flooded,
          'flood_plan' => $eval->flood_plan,
          'place_setup' => $eval->place_setup,
          'family_opinion' => $eval->family_opinion,
          'dedicate_space' => $eval->dedicate_space,
          'free_distractions' => $eval->free_distractions,
          'lighting_and_ventilation' => $eval->lighting_and_ventilation,
          'organized_workplace' => $eval->organized_workplace,
          //'result' => '6',
          'system_check' => $new_system_record->id,
          'photo_addendum' => $photo_addendum_id,
          'describe_space' => $eval->describe_space,
          'num_kids' => $eval->num_kids,
          'kids_take_care_work' => $eval->kids_take_care_work
        ];

        $new_home_eval = HomeEvaluationRecord::updateOrCreate(['photo_addendum' => $photo_addendum_id], $data);
        $applicant_pa->home_evaluation = $new_home_eval->id;
        $applicant_pa->save();

        //$new_system_record = SystemCheckRecord::updateOrCreate(['home_eval_id' => $new_home_eval->id], $data2);

        $new_home_eval->system_check = $new_system_record->id;
        $new_home_eval->save();
        //Log::info($new_media_arr);
      }
    }

    $this->sendUserHomeEvalEmail($applicant_pa);
  }

  public function sendUserHomeEvalEmail(ApplicantRecord $applicant){
    $from = 'jobs@myvirtudesk.com';
    $to = 'home.evaluators@myvirtudesk.com';
    //$to = 'newtonboy@gmail.com';
    $subject = 'New Home Evaluation Form Submitted | '.$applicant->name;
    $fromName = 'Jobs Virtudesk';

    $html = '<html><head><title>Home Evaluation Send</title></head><body><p><strong>Team,</strong></p>'.$applicant->name.' has completely submitted the Home Evaluation Form. Please login to your account to check the details.</body></html>';

    Mail::send([], [], function ($message) use ($to, $from, $fromName, $html, $subject) {
      $message->to($to)
      ->subject($subject)
      ->from($from, $fromName)
      ->setBody($html, 'text/html'); // assuming text/plain
    });
  }

  //end here
}
