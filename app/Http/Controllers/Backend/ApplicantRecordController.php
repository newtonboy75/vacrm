<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use App\InterviewRecord;
use App\ApplicantFirstInterview;
use App\ApplicantExperienceRecord;
use App\ApplicantExperience;
use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;
use App\ApplicantFinalInterview;
use App\ApplicantReference;
use App\ReferenceRecord;
use App\HomeEvaluationRecord;
use App\PhotoAddendum;
use App\Media;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\SystemCheckRecord;
use App\TrainingRecord;
use Illuminate\Support\Facades\Log;
use App\Cvs;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\ClientRecord;
use App\ClientInterviewRecord;
use App\JobRecord;
use App\EmployeeJob;
use App\FosUserGroup;
use App\FosGroup;
use App\ApplicantCoaching;
use App\ApplicantMemo;
use App\EmployeeRecord;
use App\ApplicantCheckin;
use App\ApplicantRatings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Carbon;
use App\Jobs\SendEmailQueues;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use App\VatsSystem;
/**
* Class SourcingRecordController.
*/
class ApplicantRecordController extends Controller
{
  /**
  * @return \Illuminate\View\View
  */
  public function index(){
    return view('backend.sourcing-record');
  }

  public function create(Request $request){
    DB::connection()->disableQueryLog();
    $admin_emails = ['newtonboy@gmail.com'];

    if(!isset($request->from)){

      if(!in_array($request->email, $admin_emails)){
        $new_apppl = ApplicantRecord::where('email', '=', $request->email)->orderBy('id', 'desc')->first();
        if($new_apppl){
          $date = Carbon::parse($new_apppl->created_at);
          $now = Carbon::now();
          $diff_date = $date->diffInDays($now);
          if($diff_date <= '90'){
            return '3';
            exit;
          }
        }
      }

      $status = 'sourcing_record';
    }else{
      if($request->send_thank == 'yes'){
        $status = 'sourcing_record';
      }else{
        $status = 'hr_record';
      }
    }

    $landline = isset($request->landline) ?? 'NULL';
    $sourcer = isset($request->from) ? $request->sourcer : '1';

    $data = [
      'name' => ucwords($request->full_name),
      'email' => $request->email,
      'mobile_number' => $request->mobile_number,
      'address' => ucwords($request->address),
      'skype_id' => $request->skype_id,
      'position_applying_for' => $request->position_applying_for,
      'come_from' => $request->come_from,
      'landing_number' => $landline,
      'workflow_status' => $status,
      'applicants_hash' => md5(microtime().$request->email),
      'temp_pass' => $this->generate_password(),
      'sourcer_id' => $sourcer,
      'has_real_estate_exp' => $request->has_real_estate_exp
    ];

    $new_applicant = ApplicantRecord::create($data);

    $all_ratings = [
      'english_communication_verbal' => $request->english_communication_verbal,
      'english_communication_written' => $request->english_communication_written,
      'inbound_sales' => $request->inbound_sales,
      'outbound_prospecting' => $request->outbound_prospecting,
      'basic_seo' => $request->basic_seo,
      'setting_of_appointments' => $request->setting_of_appointments,
      'powerpoint_presentations' => $request->powerpoint_presentations,
      'reports_creation' => $request->reports_creation,
      'upselling' => $request->upselling,
      'oubound_collection' => $request->oubound_collection,
      'project_management' => $request->project_management,
      'social_media_marketing' => $request->social_media_marketing,
      'basic_graphic_design' => $request->basic_graphic_design,
      'community_management' => $request->community_management,
      'data_entry' => $request->data_entry,
      'email_handling' => $request->email_handling,
      'document_processing' => $request->document_processing,
      'photo_video_editing' => $request->photo_video_editing,
      'web_search' => $request->web_search,
      'audio_video_transition' => $request->audio_video_transition
    ];

    $ratings_data = [
      'app_id' => $new_applicant->id,
      'app_ratings' => serialize($all_ratings)
    ];

    $applicant_ratings_data =ApplicantRatings::create($ratings_data);

    $file_name = $request->file_name;
    if(!isset($request->from)){
      $file_id = Cvs::create(['applicant_id' => $new_applicant->id, 'file_name' => $file_name]);
    }

    if(isset($request->from) && $request->from == 'dashboard'){
      $userlog = new UserLogController;
      $action = 'Created new Applicant Record from dashboard: '.$request->applicant_id;
      $userlog->save($sourcer, $action);

      if($request->send_thank == 'yes'){
        $this->sendThankYouEmail($new_applicant);
      }else{
        $employee = EmployeeRecord::updateOrCreate(['applicant_id' => $new_applicant->id], ['date' => date('Y-m-d h:i:s')]);
      }

      unset($employee);
      return '1';
      exit;

    }else{
      return $new_applicant;
    }

    return '0';
  }

  public function load(){
    //Log::info(request());
    DB::connection()->disableQueryLog();
    $all_records = null;
    $sortby = request()->sortby;
    $sort_field= '';
    $tab = request()->tab;
    $st = count(request()->archived) > 1 ? 3 : 0;
    $name = '';

    $time = strtotime("-6 month", time());
    $more_than_a_year = strtotime("-18 month", time());
    $date = date("Y-m-d", $time);

    if(request()->search['value'] != ''){
      //Log::info('thissss1');
      $kw_important_found = array();
      $name = request()->search['value'];

      if(is_array($tab) !== false){
        //Log::info('sortingggs');
        //using search with options
        $all_records = ApplicantRecord::whereIn('workflow_status', $tab)
        ->where('archived', '<=', $st)
        ->select('id', 'name', 'created_at', 'workflow_status', 'status', 'sourcer_id', 'position_applying_for', 'come_from', 'has_real_estate_exp')
        ->orderBy('created_at', 'asc')
        ->orderBy('id', 'desc');
      }else{
        //Log::info('sorti');
        //using search without options
        $all_records = ApplicantRecord::where('workflow_status', 'sourcing_record')
        ->where('archived', '=', $st)
        ->select('id', 'name', 'created_at', 'workflow_status', 'status', 'sourcer_id', 'position_applying_for', 'come_from', 'has_real_estate_exp')
        ->orderBy('updated_at', 'asc')
        ->orderBy('id', 'desc');

      }
    }else{
      //sourcing page load
      //Log::info('sorting page load');
      $kw_important_found = array();
      if(isset($sortby)){
        $sort_field = $sortby[0][0];
        $sort_dest = $sortby[0][1];

        if($sort_dest == 'asc'){
          $sort_dest = 'desc';
        }else{
          $sort_dest = 'asc';
        }

        if($sort_field == '0'){
          //Log::info('sorting page load 0');
          $priority_lists = VatsSystem::where('status', '=', '0')->pluck('user_id');

          $all_records = ApplicantRecord::where('workflow_status', 'sourcing_record')
          ->whereIn('id', $priority_lists)
          ->where('status', '<=', '1')
          ->where('archived', '=', '0')
          ->select('id', 'name', 'created_at', 'workflow_status', 'status', 'sourcer_id', 'position_applying_for', 'come_from', 'has_real_estate_exp')
          ->orderBy('id', $sort_dest);
        }elseif($sort_field == '1'){
          //Log::info('sorting page load 1');
          $all_records = ApplicantRecord::where('workflow_status', 'sourcing_record')
          //->whereIn('id', $priority_lists)
          ->where('status', '<=', '1')
          ->where('archived', '=', '0')
          ->select('id', 'name', 'created_at', 'workflow_status', 'status', 'sourcer_id', 'position_applying_for', 'come_from', 'has_real_estate_exp')
          ->orderBy('name', $sort_dest);
        }elseif($sort_field == '2'){
          //Log::info('sorting page load 2');
          $all_records = ApplicantRecord::where('workflow_status', 'sourcing_record')
          //->whereIn('id', $priority_lists)
          ->where('status', '<=', '1')
          ->where('archived', '=', '0')
          ->select('id', 'name', 'created_at', 'workflow_status', 'status', 'sourcer_id', 'position_applying_for', 'come_from', 'has_real_estate_exp')
          ->orderBy('created_at', $sort_dest);
        }else{
          //Log::info('dito po');
          //Log::info('sorting page load 4');
          $all_records = ApplicantRecord::where('workflow_status', 'sourcing_record')
          //->whereIn('id', $priority_lists)
          ->where('status', '<=', '1')
          ->where('archived', '=', '0')
          ->select('id', 'name', 'created_at', 'workflow_status', 'status', 'sourcer_id', 'position_applying_for', 'come_from', 'has_real_estate_exp')
          ->orderByRaw('FIELD(id, '.implode(", " , $priority_lists->toArray()).')');
        }
      }else{
        //Log::info('thissss3');
        //$priority_lists = VatsSystem::where('status', '=', '0')->get();

        $priority_lists = VatsSystem::where('status', '=', '0')->pluck('user_id');

        $plists = array();
        $emp_histories = array();
        //$eh = array();
        $kw_important_found = array();

        $all_records = ApplicantRecord::join('vats_info', 'applicant.id', '=', 'vats_info.user_id')
        ->whereIn('applicant.id', $priority_lists)
        ->where('applicant.archived', '<=', '1')
        ->where('applicant.status', '0')
        ->select('applicant.id', 'applicant.name', 'applicant.created_at', 'applicant.workflow_status', 'applicant.status', 'applicant.sourcer_id', 'applicant.position_applying_for', 'applicant.come_from', 'applicant.has_real_estate_exp', 'vats_info.keywords_count')
        ->orderBy('vats_info.keywords_count', 'desc');
        //->orderBy('applicant.id', 'asc');
        //->orderByRaw('FIELD(id, '.implode(", " , array_keys($kw_important_found)).')');
      }
    }

    return datatables()->of($all_records)
    ->editColumn('created_at', function($all_records){
      $html = date('M d, Y,  h:i', strtotime($all_records->created_at));
      return $html;
    })
    ->editColumn('workflow_status', function($all_records){
      return $all_records->status_context();
    })
    ->editColumn('status', function($all_records){
      return $all_records->application_status();
    })
    ->editColumn('sourcer_id', function($all_records){
      $html = '';
      if($all_records->sourcer_id != ''){
        $n1 = $all_records->sourcer_name[0]['first_name'] ?? '';
        $n2 = $all_records->sourcer_name[0]['last_name'] ?? '';
        $html = $n1.' '.$n2;
      }
      return $html;
    })
    ->addColumn('kw_count', function($all_records) use ($kw_important_found){
      return $all_records->keywords_count ?? 0;
    })
    ->addColumn('position', function($all_records){
      return ucwords(strtolower($all_records->position_applying_for));
    })
    ->editColumn('name', function($all_records) use ($kw_important_found){
      $priority_status = '';

      //if(count($kw_important_found) > 0){
        if(isset($all_records->keywords_count)){
          //$years_employment = $eh[$all_records->id];
          $important_kw_counts = $all_records->keywords_count;

          if($important_kw_counts >= 15){
            $priority_status = '<span class="badge badge-red float-right">Highest</span>';
          }elseif($important_kw_counts >= 8 && $important_kw_counts <= 14){
            $priority_status = '<span class="badge badge-red float-right">High</span>';
          }elseif($important_kw_counts >= 4 && $important_kw_counts <= 7){
            $priority_status = '<span class="badge badge-green float-right">Medium</span>';
          }else{
            $priority_status = '<span class="badge badge-gray float-right">Low</span>';
          }
        }
      //}

      return '<a style="color: #1976d2; margin-top: 10px !important; margin-bottom: 10px !important;" href="/admin/sourcing-record/'.$all_records->id.'/edit"><strong>'.ucwords(strtolower($all_records->name)).'</strong></a>&nbsp;'.$priority_status;
    })
    ->rawColumns(['name'])
    ->make(true);
  }

  public function edit(Request $request){
    DB::connection()->disableQueryLog();
    $current_route = explode('/', Route::getFacadeRoot()->current()->uri());

    $applicant = null;
    $allrecruiters = [];
    $alltrainers = [];
    $interview_initial = null;
    $interviews_initial = [];
    $interviews_final = [];
    $experiences = null;
    $references = [];
    $photo_addendum = null;
    $clients = null;
    $client_interviews = null;
    $mygroups = null;
    $groups = null;
    $coachings = null;
    $allcoaches = null;
    $memos = null;
    $employee_record = null;
    $checkins = null;
    $jobs = [];

    $photo_addendum = '';
    $applicant = ApplicantRecord::whereId($request->id)->with('trainings', 'cv', 'esigs', 'ratings')->first();

    if($current_route[1] == 'sourcing-record'){
      $allrecruiters = User::role('recruiter')->where('active', '1')->orderBy('first_name', 'asc')->get();
      $interview_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->with('interview_record')->orderBy('id', 'desc')->first();
    }elseif($current_route[1] == 'talent-acquisition-record'){
      $allrecruiters = User::role('recruiter')->where('active', '1')->orderBy('first_name', 'asc')->get();
      $interview_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->with('interview_record')->orderBy('id', 'desc')->first();
      $interviews_final = ApplicantFinalInterview::where('applicant_id', '=', $request->id)->with('interview_record')->get();
      $interviews_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->latest('applicant_id')->orderBy('id', 'desc')->with('interview_record')->get();
    }elseif($current_route[1] == 'reference-check'){
      $allrecruiters = User::role('recruiter')->where('active', '1')->orderBy('first_name', 'asc')->get();
      $interview_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->with('interview_record')->orderBy('id', 'desc')->first();
      $interviews_final = ApplicantFinalInterview::where('applicant_id', '=', $request->id)->with('interview_record')->get();
      $interviews_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->latest('applicant_id')->orderBy('id', 'desc')->with('interview_record')->get();
      $references = ApplicantReference::where('applicant_id', '=', $request->id)->with('reference_record')->get();
    }elseif($current_route[1] == 'pre-training-requirements'){
      $references = ApplicantReference::where('applicant_id', '=', $request->id)->with('reference_record')->get();
      $allrecruiters = User::role('recruiter')->orderBy('first_name', 'asc')->get();
      $interview_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->with('interview_record')->orderBy('id', 'desc')->first();
      $interviews_final = ApplicantFinalInterview::where('applicant_id', '=', $request->id)->with('interview_record')->get();
      $interviews_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->latest('applicant_id')->orderBy('id', 'desc')->with('interview_record')->get();
      $alltrainers = User::role('trainer')->orderBy('first_name', 'asc')->get();
    }elseif($current_route[1] == 'training-record'){
      $references = ApplicantReference::where('applicant_id', '=', $request->id)->with('reference_record')->get();
      $allrecruiters = User::role('recruiter')->orderBy('first_name', 'asc')->get();
      $interview_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->with('interview_record')->orderBy('id', 'desc')->first();
      $interviews_final = ApplicantFinalInterview::where('applicant_id', '=', $request->id)->with('interview_record')->get();
      $interviews_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->latest('applicant_id')->orderBy('id', 'desc')->with('interview_record')->get();
      $alltrainers = User::role('trainer')->orderBy('first_name', 'asc')->get();
    }elseif(Route::current()->getName() == 'admin.employee'){
      $references = ApplicantReference::where('applicant_id', '=', $request->id)->with('reference_record')->get();
      $allrecruiters = User::role('recruiter')->orderBy('first_name', 'asc')->get();
      $interview_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->with('interview_record')->orderBy('id', 'desc')->first();
      $interviews_final = ApplicantFinalInterview::where('applicant_id', '=', $request->id)->with('interview_record')->get();
      $interviews_initial = ApplicantFirstInterview::where('applicant_id', '=', $request->id)->latest('applicant_id')->orderBy('id', 'desc')->with('interview_record')->get();
      $alltrainers = User::role('trainer')->orderBy('first_name', 'asc')->get();
    }else{

      $experiences = ApplicantExperience::where('applicant_id', '=', $request->id)->with('experience_record')->get();
      $clients = ClientRecord::where('status', '=', 'active')->orderBy('id', 'asc')->get();

      if($applicant){
        $client_interviews = ClientInterviewRecord::where('applicant_id', '=', $applicant->id)->with('interviews')->get();
      }

      $jobs = EmployeeJob::where('employee_id', '=', $request->id)->with('job_info')->get();
      $mygroups = FosUserGroup::where('user_id', '=', $request->id)->get();
      $groups = FosGroup::all();
      $allcoaches = User::role('Coaches')->where('active', '=', '1')->groupBy('first_name')->orderBy('first_name', 'asc')->get();
      $coachings = ApplicantCoaching::where('employee_id', '=', $request->id)->with('coachings')->get();
      $memos = ApplicantMemo::where('employee_id', '=', $request->id)->with('memos')->get();
      $employee_record = EmployeeRecord::where('applicant_id', '=', $request->id)->first();
      $checkins = ApplicantCheckin::where('employee_id', '=', $request->id)->get();
    }

    return view('backend.applicant-record-edit')
    ->withApplicant($applicant)
    ->withAllrecruiters($allrecruiters)
    ->withAlltrainers($alltrainers)
    ->withInterview($interview_initial)
    ->withInterviews($interviews_initial)
    ->withFinals($interviews_final)
    ->withExperience($experiences)
    ->withReferences($references)
    //->withPhotoAddendum($photo_addendum)
    ->withClients($clients)
    ->withClientInterviews($client_interviews)
    ->withMyGroups($mygroups)
    ->withGroups($groups)
    ->withCoaching($coachings)
    ->withAllCoaches($allcoaches)
    ->withMemos($memos)
    ->withEmployeeRecord($employee_record)
    ->withCheckins($checkins)
    ->withJobs($jobs);
  }

  public function update(Request $request, ApplicantFirstInterview $interview_initial, InterviewRecord $interview, ApplicantFinalInterview $interview_final){
    foreach($request->talent_application_status_first as $k=>$taf){

      $interviews = $interview->whereId($request->talent_application_id_first)->first();
      if($interviews){
        $interviews->note = $request->talent_note_first[$k];
        $interviews->date = $request->talent_exp_date_end_first[$k];
        $interviews->re_schedule_reason = $request->talent_rescheduled_first[$k];
        $interviews->save();
      }else{
        $interview->note = $request->talent_note_first[$k];
        $interview->date = $request->talent_exp_date_end_first[$k];
        $interview->re_schedule_reason = $request->talent_rescheduled_first[$k];
        $interview->save();
        $interview_initial->create(['applicant_id' => $request->applicant_id, 'interview_id' => $request->talent_application_id_first]);
      }
    }

    unset($interviews);
    return $request;
  }

  public function generate_password(){
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $hashed = substr(str_shuffle(str_repeat($pool, 8)), 0, 8);
    $pass = ApplicantRecord::where('temp_pass', '=', $hashed)->first();

    if($pass){
      $this->generate_password();
    }else{
      return $hashed;
    }
  }

  public function sendThankYouEmail($new_applicant){
    SendEmailQueues::dispatch($new_applicant);
  }

  public function login_remote(Request $request){

    $images = [];

    $applicant = ApplicantRecord::where('email', '=', $request->email)->where('temp_pass', '=', $request->password)->with('home_evaluation_record')->first();

    if(!$applicant){
      return '0';
    }

    return $applicant;

  }

  public function check_progress(Request $request){
    $applicant = ApplicantRecord::where('id', '=', $request->id)->with('home_evaluation_record', 'initial_interview', 'recruiter_name', 'cv')->first();
    return $applicant;

  }

  public function get_sched_remote(Request $request){
    $applicant = ApplicantRecord::where('applicants_hash', '=', $request->id)->with('initial_interview')->first();

    if($applicant){
      return $applicant;
    }

    return '0';
  }

  public function change_schedule_remote(Request $request){

    $url = '';
    $new_date = $request->new_sched;
    $old_date = '';
    $from = url()->previous();
    $from_exp = explode('/', $from);

    if(count($from_exp) >= 6){
      $url = substr($from, 0, -2);
    }else{
      $url = $from;
    }

    $applicant = ApplicantRecord::where('id', '=', $request->recnum)->with('initial_interview')->first();

    $interview_record = $applicant->initial_interview[0]->interview_record[0];

    if($interview_record->re_schedule_reason != ''){
      $url .= '/2';
    }else{
      $last_sched = $interview_record->date;
      $first_interview = InterviewRecord::whereId($interview_record->id)->first();

      if($first_interview){
        $old_date = date('M d, Y h:i a', strtotime($last_sched));
        $first_interview->date = date('Y-m-d h:i:s', strtotime($new_date));
        $first_interview->re_schedule_reason = $request->reason.' / From original schedule: '.$old_date;
        $first_interview->status = '33';
        $first_interview->save();

        $html = 'Applicant '.$applicant->name.' (#'.$applicant->id.') re-scheduled first interview date';
        $html .= '<p>Original Date: '.$old_date.'</p>';
        $html .= '<p>New Date: '.$new_date.'</p>';
        $html .= '<p>Reason: '.$request->reason.'</p>';
        $to_ = 'jobs@myvirtudesk.com';
        //$to_ = 'pabeboy10@gmail.com';

        Mail::send([], [], function ($message) use($to_, $html){
          $message->to($to_)
          ->subject('Applicant Interview Date Re-scheduled')
          ->replyTo('jobs@myvirtudesk.com')
          ->from('jobs@myvirtudesk.com', 'MyVirtudesk Talent Acquisition')
          ->setBody($html, 'text/html');
        });
      }

      $url .= '/1';
    }

    return Redirect::away($url);
  }

  public function downloadFromS3($path, $filename){
    $env = env('VD_URL');
    $url = $env.'/uploaded/'.$filename;

    if($this->fileurl_exists($url)){

      $headers = get_headers($url, 1);
      $type = $headers["Content-Type"];

      header('Content-disposition: inline; filename='.$filename);
      header('Content-type: '.$type);   // make sure this is the correct mime-type for the file
      readfile($url);

    }else{

      return redirect(Storage::disk('s3')->temporaryUrl(
        $path.'/'.$filename,
        now()->addHour(),
        ['ResponseContentDisposition' => 'inline']
      ));

    }
  }

  public function fileurl_exists($url){
    $headers=get_headers($url);
    return stripos($headers[0],"200 OK") ? true : false;
  }

  public function change_cv(Request $request){
    $applicant = Cvs::where('applicant_id', '=', $request->id)->first();
    $applicant->file_name = $request->filename;
    $applicant->save();
    unset($applicant);
  }

  public function reset_pass(Request $request){

    $applicant = ApplicantRecord::where('email', '=', $request->email)->orderBy('id', 'desc')->first();
    $to = $request->email;
    $body = '';

    if($applicant){
      $new_password = $this->generate_password();
      $applicant->temp_pass = $new_password;
      $applicant->save();
      $body = '<p><strong>Hello '.$applicant->name.'</strong>,</p>';
      $body .= '<p>Your new password is: '.$new_password.'</p>';
      $body .= '<p>Please login to <a href="'.config('app.vdesk_url').'">Virtudesk Portal</a> with your registered email and new password';

    }else{
      return 'We could not find an account registered with that email. Please try again or register a new account.';
    }

    Mail::send([], [], function ($message) use($to, $body){
      $message->to($to)
      ->from('jobs@myvirtudesk.com', 'MyVirtudesk')
      ->cc('jobs@myvirtudesk.com', 'MyVirtudesk')
      ->subject('Virtudesk Application | Password Reset')
      ->replyTo('jobs@myvirtudesk.com')
      ->setBody($body, 'text/html');
    });

    return 'Thank you. An email has been sent to your email address with your new password.';
  }
}
