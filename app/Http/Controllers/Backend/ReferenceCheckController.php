<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use App\InterviewRecord;
use App\ApplicantFirstInterview;
use App\ApplicantExperienceRecord;
use App\ApplicantExperience;
use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;
use App\ApplicantFinalInterview;
use App\ApplicantReference;
use App\ReferenceRecord;
use App\Http\Controllers\Backend\ApplicantRecordController;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Backend\EmailRecordController;
use App\UserLog;
use App\Jobs\SendEmailReferenceQueues;
use App\Http\Controllers\Backend\AdministrativeController;
/**
 * Class SourcingRecordController.
 */
class ReferenceCheckController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
     public function index(){
       $chart = new AdministrativeController;
       $lava = $chart->getChartAll();
       return view('backend.reference-record', compact('lava'));
     }

     public function load(){
       DB::connection()->disableQueryLog();
       $all_records = null;
       $tabs = request()->tab;
       $st = count(request()->archived) > 1 ? 3 : 0;

       $time = strtotime("-6 month", time());
       $date = date("Y-m-d", $time);

       //$all_records = ApplicantRecord::whereIn('workflow_status', $tab)->where('archived', '<=', $st)->orderBy('updated_at', 'desc')->get();
       //if(is_array($tabs) !== false){
         //$all_records = ApplicantRecord::whereIn('workflow_status', $tabs)->where('archived', '<=', $st)->whereDate('created_at', '>=', $date)->select('workflow_status', 'id', 'name', 'email', 'created_at')->orderBy('updated_at', 'desc');
       //}else{
         //$all_records = ApplicantRecord::where('workflow_status',  'reference_check')->where('archived', '<=', $st)->whereDate('created_at', '>=', $date)->select('workflow_status', 'id', 'name', 'email', 'created_at')->orderBy('updated_at', 'desc');
       //}

       if(request()->search['value'] != ''){

       if(is_array($tabs)){
           $all_records = ApplicantRecord::where('name', 'REGEXP', request()->search['value'])->orWhere('email', request()->search['value'])->whereIn('workflow_status', $tabs)->where('archived', '<=', '3')->where('recruiter_id', '!=', '')->whereDate('created_at', '>=', $date)->select('workflow_status', 'id', 'name', 'email', 'created_at')->orderBy('created_at', 'desc')->limit(100);
      }else{
          $all_records = ApplicantRecord::where('workflow_status', 'talent_acquisition')->where('name', 'REGEXP', request()->search['value'])->orWhere('email', request()->search['value'])->where('archived', '<=', '3')->where('recruiter_id', '!=', '')->whereDate('created_at', '>=', $date)->select('workflow_status', 'id', 'name', 'email', 'created_at')->orderBy('created_at', 'desc')->limit(100);
        }

    }else{
       $all_records = ApplicantRecord::select('workflow_status', 'id', 'name', 'email', 'created_at')->where('workflow_status', 'reference_check')->where('archived', '<=', $st)->where('recruiter_id', '!=', '')->whereDate('created_at', '>=', $date)->orderBy('updated_at', 'desc');
    }

       return datatables()->of($all_records)

       ->addColumn('transition_date', function($all_records){
         $html = '';
         $log = UserLog::where('activity', 'REGEXP', 'Sent interview passed email for user:')->where('activity', 'REGEXP', $all_records->id)->orderBy('id', 'desc')->first();
         if($log){
           $html = date('M d, Y h:i', strtotime($log->date_created));
         }

         return $html;
       })

       ->editColumn('workflow_status', function($all_records){
         $html = $all_records->status_context();
         return $html;
       })

       ->editColumn('created_at', function($all_records){
         $html = date('M d Y, h:i', strtotime($all_records->created_at));
         return $html;
       })

       ->editColumn('name', function($all_records){
         return '<a style="color: #1976d2; margin-top: 10px !important; margin-bottom: 10px !important;" href="/admin/reference-check/'.$all_records->id.'/edit"><strong>'.$all_records->name.'</strong></a>';
       })
       ->rawColumns(['name'])
       ->make(true);
     }

     public function edit(Request $request){
       $edit = new ApplicantRecordController;
       return $edit->edit($request);
     }

     public function update(Request $request, ApplicantReference $applicant_references, ReferenceRecord $reference){
        DB::connection()->disableQueryLog();
       foreach($request->ref_id as $k=>$v){
         if($request->ref_id[$k] != ''){
           $ref = $reference->where('id', '=', $request->ref_id[$k])->first();

           if($ref){
             $ref->referencer = $request->referencer[$k];
             $ref->company = $request->company[$k];
             $ref->position = $request->position[$k];
             $ref->mobile_number = $request->mobile_number[$k];
             $ref->landing_number = $request->landing_number[$k];
             $ref->email = $request->email[$k];
             $ref->best_time_to_contact = $request->best_time_to_contact[$k];
             $ref->note = $request->note[$k];
             $ref->date_contacted = $request->date_contacted[$k];
             $ref->save();
           }

           unset($ref);

         }else{

           $data = [
             'referencer' => $request->referencer[$k],
             'company' => $request->company[$k],
             'position' => $request->position[$k],
             'mobile_number' => $request->mobile_number[$k],
             'landing_number' => $request->landing_number[$k],
             'email' => $request->email[$k],
             'best_time_to_contact' => $request->best_time_to_contact[$k],
             'note' => $request->note[$k],
             'date_contacted' => $request->date_contacted[$k]
           ];

           $new_ref_id = $reference->create($data);
           $applicant_references->create(['applicant_id' => $request->applicant_id, 'reference_id' => $new_ref_id->id]);
           unset($data);
         }
       }
       if (Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Updated/created Reference Check Record for: '.$request->applicant_id;
         $userlog->save($logged_user_id, $action);
         unset($userlog);
       }

       unset($request);

       return '1';
     }


     public function delete(Request $request){

       $id = ReferenceRecord::whereId($request->id)->delete();
       $ref_id = ApplicantReference::where('reference_id', '=', $request->id)->delete();

       if (Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Deleted Reference Check Record ID: '.$request->id;
         $userlog->save($logged_user_id, $action);
         unset($userlog);
       }

       unset($request);

       return '1';
      // return $ref_id;
     }


     public function sendEmail(Request $request){

       $user_id = $request->user_id;
       $success = '0';
       $to = $request->to;
       $body = $request->email_body;
       $subject = '';
       $status = 'sent';
       $sub_type = '';
       $type = 'reference';
       $new_request = $request->all();
       $disposition = '';

       if($request->type == 'home_eval'){

         //dispatchNow(new SendEmailReferenceQueues($new_request, 'home_eval'));

         //old
         //SendEmailReferenceQueues::dispatchNow($new_request, 'home_eval');

         $disposition = 'home_eval';

         $applicant_record = ApplicantRecord::where('id', '=', $user_id)->first();

         if (Auth::check()){
           $userlog = new UserLogController;
           $logged_user_id = Auth::id();
           $action = 'Sent Home Evaluation email and applied transition for: '.$applicant_record->id;
           $userlog->save($logged_user_id, $action);
           unset($userlog);
         }

         //return '1';

         if($applicant_record){
           //$applicant_record->workflow_status = 'home_eval';
           $applicant_record->workflow_status = 'pre_training';
           $applicant_record->archived = '0';
           $applicant_record->save();
           $success = '1';
           unset($applicant_record);
         }

         $save_email = new EmailRecordController();
         $save_email->save($to,	'virtudesk.referenceteam@gmail.com',	$logged_user_id,	$subject,	$body,	$status, $type, $request->type);
         //unset($save_email);
         //unset($new_request);
         //unset($request);

         //return '1';
       }else{

         //dispatchNow(new SendEmailReferenceQueues($new_request, 'home_eval_failed'));

         //SendEmailReferenceQueues::dispatchNow($new_request, 'home_eval_failed');

         $disposition = 'home_eval_failed';

         $applicant_record = ApplicantRecord::whereId($request->user_id)->first();

         if (Auth::check()){
           $userlog = new UserLogController;
           $logged_user_id = Auth::id();
           $action = 'Sent Regret email and archived user: '.$applicant_record->id;
           $userlog->save($logged_user_id, $action);
           unset($userlog);
         }

         if($applicant_record){
           $applicant_record->archived = '1';
           $applicant_record->save();
           $success = '1';
           unset($applicant_record);
         }

         $save_email = new EmailRecordController();
         $save_email->save($to,	'virtudesk.referenceteam@gmail.com',	$logged_user_id,	$subject,	$body,	$status, $type, $request->type);

       }

       SendEmailReferenceQueues::dispatchNow($new_request, $disposition);
       unset($save_email);
       unset($request);
       return $success;

     }

     public function submit_form_ref(Request $request){

       foreach($request->referencer as $k=>$v){

           $data = [
             'referencer' => $request->referencer[$k],
             'company' => $request->company[$k],
             'position' => $request->position[$k],
             'mobile_number' => $request->mobile_number[$k],
             'landing_number' => $request->landing_number[$k] ?? '',
             'email' => $request->email[$k],
             'best_time_to_contact' => $request->best_time_to_contact[$k],
           ];

           if(count($data)>=1){
             $reference = new ReferenceRecord;
             $new_ref_id = $reference->create($data);

             $applicant_references =  new ApplicantReference;
             $applicant_references->create(['applicant_id' => $request->applicant_id[$k], 'reference_id' => $new_ref_id->id]);
           }

       }

       $this->sendUserRefEmail($request->applicant_id[$k]);

       unset($request);
       unset($data);

       return '1';
     }

     public function sendUserRefEmail($applicant_id){

       $applicant = ApplicantRecord::whereId($applicant_id)->first();

       if($applicant){
         $new_request = new Request([
          'name'   => $applicant->name,
          'email' => $applicant->email,
         ]);

         //dispatchNow(new SendEmailReferenceQueues($new_request, 'send_ref'));
         SendEmailReferenceQueues::dispatchNow($new_request, 'send_ref');
         unset($new_request);
       }

     }

     //end all
}
