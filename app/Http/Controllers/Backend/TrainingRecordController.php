<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ApplicantRecord;
use App\TrainingRecord;
use App\ApplicantExperienceRecord;
use App\ApplicantExperience;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\PreTrainingBatch;
use App\Http\Controllers\Backend\AdministrativeController;
/**
* Class SourcingRecordController.
*/
class TrainingRecordController extends Controller
{
  /**
  * @return \Illuminate\View\View
  */
  public function index(){
    $chart = new AdministrativeController;
    $lava = $chart->getChartAll();
    return view('backend.training-record', compact('lava'));
  }

  public function load(){
    $tab = request()->tab;
    $st = count(request()->archived) > 1 ? 3 : 0;
    $to_archive = ['41', '29', '30', '33', '35', '11', '12', '13', '14', '15', '16', '17', '18', '19', '31', '23', '24', '25', '26', '39', '42'];

    $all_records = ApplicantRecord::whereIn('workflow_status', $tab)->where('archived', '<=', $st)->whereHas('trainings', function ($query) use ($to_archive) {
      $query->where('program', '<>', '')->whereNotIn('training_status', $to_archive)->orWhere('training_status', null);
    })->orderBy('updated_at', 'desc')->get();

    unset($to_archive);

    return datatables()->of($all_records)

    ->editColumn('program', function($all_records){
      $html = $all_records->trainings[0]['program'] ?? '';
      return $html;
    })

    ->editColumn('batch', function($all_records){
      $html = $all_records->trainings[0]['batch'] ?? '';
      return $html;
    })

    ->editColumn('training_status', function($all_records){
      $html = $all_records->trainings[0]->training_status() ?? '';
      return $html;
    })

    ->editColumn('schedule', function($all_records){

      $html = '';

      if(isset($all_records->trainings[0]['start_date']) && isset($all_records->trainings[0]['end_date'])){

        $startdate = explode(' ', $all_records->trainings[0]['start_date']);
        $enddate = explode(' ', $all_records->trainings[0]['end_date']);

        if(count($startdate) > 3 && count($enddate) > 3){
          $end = Carbon::parse(date('M d Y', strtotime($startdate[0].' '.$startdate[1].', '.$startdate[2])));
          $now = Carbon::parse( date('M d Y', strtotime($enddate[0].' '.$enddate[1].', '.$enddate[2])));
          $numdays = round((strtotime($now) - strtotime($end)) / (60 * 60 * 24));

          $html = $numdays > 1 ? $numdays.' '.'Days' : $numdays.' '.'Day';
        }

      }

      return $html;
    })

    ->editColumn('start_date', function($all_records){
      $startdate = explode(' ', $all_records->trainings[0]['start_date']);
      $html = $all_records->trainings[0]['start_date'] ?? '';
      return $html;
    })

    ->editColumn('end_date', function($all_records){
      $enddate = explode(' ', $all_records->trainings[0]['end_date']);
      $html = $all_records->trainings[0]['end_date'] ?? '';
      return $html;
    })

    ->editColumn('workflow_status', function($all_records){
      $html = $all_records->status_context() ?? '';
      return $html;
    })

     ->editColumn('name', function($all_records){
         return '<a style="color: #1976d2; margin-top: 10px !important; margin-bottom: 10px !important;" href="/admin/training-record/'.$all_records->id.'/edit"><strong>'.$all_records->name.'</strong></a>';
       })
       ->rawColumns(['name'])

    ->make(true);
  }


  public function edit(Request $request){
    $edit = new ApplicantRecordController;
    return $edit->edit($request);
  }

  public function update(Request $request){

    if(!isset($request->batch)){
      unset($request);
      return 'Please choose batch number';
      exit;
    }

    $batch_id = PreTrainingBatch::whereId($request->batch)->first();
    $program = preg_replace('/[^a-zA-Z]/', '',$batch_id->batch_number);

    if(isset($request)){
      //39, 31,
      $to_archive = ['41', '29', '30', '33', '35', '11', '12', '13', '14', '15', '16', '17', '18', '19', '31', '23', '24', '25', '26', '39', '42'];

      $archive = '0';
      if(in_array($request->training_status, $to_archive)){
        $archive = '3';
      }

      $training_record = TrainingRecord::where('applicant_id', '=', $request->applicant_id)->orderBy('id', 'desc')->first();
      $training_record->notes = $request->notes;
      $training_record->improvement = $request->improvement;
      $training_record->training_status = $request->training_status;
      $training_record->strengths = $request->strengths;
      $training_record->batch = $batch_id->batch_number;
      $training_record->start_date = $batch_id->start_date;
      $training_record->end_date = $batch_id->end_date;
      $training_record->program = $program;
      $training_record->trainer_id = $request->trainer;
      $training_record->va_experience = $request->va_experience;
      $training_record->last_work_experience = $request->last_work_experience;
      $training_record->work_experience_notes = $request->work_experience_notes;
      $training_record->save();

      $applicant = ApplicantRecord::whereId($request->applicant_id)->first();
      $applicant->va_level = $request->va_level;
      $applicant->red_flag_notes = $request->red_flag_notes;
      $applicant->vd_skype = $request->vd_skype;
      $applicant->vd_skype_pass = $request->vd_skype_pass;
      $applicant->vd_email = $request->vd_email;
      $applicant->vd_email_pass = $request->vd_email_pass;
      $applicant->vd_academy = $request->vd_academy;
      $applicant->vd_academy_pass = $request->vd_academy_pass;
      $applicant->archived = $archive;
      $applicant->save();

      unset($training_record);

    }

    if(isset($request->company_name)){
      foreach($request->company_name as $i=>$v){
        $id = $request->exp_id[$i];

        $exps = [
          'company_name' => $request->company_name[$i],
          'position' => $request->exp_position[$i],
          'address' => $request->comp_address[$i],
          'start_date' => $request->start_date[$i],
          'end_date' => $request->end_date[$i],
          'reason_leaving' => $request->reason_leaving[$i],
          'job_responsibilities' => $request->job_responsibilities[$i],
        ];

        $nexp = ApplicantExperienceRecord::updateOrCreate(['id' => $id], $exps);
        $appexp = ['applicant_id' => $applicant->id, 'experience_id' => $nexp->id];
        ApplicantExperience::updateOrCreate(['experience_id' => $id], $appexp);
      }
    }

    if (Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Updated/created Training Record for: '.$request->applicant_id;
      $userlog->save($logged_user_id, $action);
    }

        unset($request);
        unset($applicant);
        unset($to_archive);
        return '1';
  }

  public function create_placement_record(Request $request){

    $applicant = ApplicantRecord::whereId($request->applicant_id)->first();
    $applicant->workflow_status = 'placement_record';
    $applicant->save();

    if (Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Created Placement Record for: '.$request->applicant_id;
      $userlog->save($logged_user_id, $action);
    }

     unset($request);

    if($applicant){
      return '1';
    }
    return '0';
  }
}
