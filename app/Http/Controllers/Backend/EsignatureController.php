<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use LaravelEsignatureWrapper;
use App\EmployeeRecord;
use App\EmployeeContract;
use App\ClientRecord;
use App\ApplicantRecord;
use App\MemoRecord;

class EsignatureController extends Controller
{

  public function loadContractTemplate($user_id, $filename, $client){

    $employee = EmployeeRecord::where('id', '=', $user_id)->first();

    $name = $employee->info->name;
    $group = $employee->group[0]['fos_group'][0]['name'];
    $sc_url = url('/').'/uploaded/hr_files/'.$filename;

    $template_id_use = config('app.esig')['caf_template_id'];
    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/hr-operation-record/success/'.$employee->applicant_id.'/'.$uid;

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        //pavel ***change email address
        ["name" => "HR Team", "email" =>  "hr@myvirtdesk.com", "auto_sign"=>  "no", "skip_signature_request_email"=>  "yes", "redirect_url" => $redirect, "uid" => $uid
        ],
        //buyer
        ["name" => $employee->info->name, "email" => $employee->info->email, "auto_sign"=>  "yes", "skip_signature_request_email"=>  "yes",
        //"redirect_url" => url('referral-thankyou')
        ],
      ),
      "custom_fields" => array(
        ["api_key" => "revised-car-name", "value" => $employee->info->name],
        ["api_key" => "revised-caf-client-name", "value" => $client],
        ["api_key" => "revised-caf-team", "value" => $group],
        ["api_key" => "revised-caf-team-lead", "value" => ''],
        ["api_key" => "revised-caf-date", "value" =>  date("M d, Y h:i A")],
        ["api_key" => "revised-caf-prepared-by", "value" => ''],
        ["api_key" => "revised-caf-date-incident", "value" => ''],
        ["api_key" => "revised-caf-description", "value" => ''],
        ["api_key" => "revised-caf-screenshot", "value" => $sc_url],
        ["api_key" => "revised-caf-cod-violated", "value" => ''],
        ["api_key" => "revise-caf-supervisor-remarks", "value" => ''],
        ["api_key" => "revised-caf-incident-decision", "value" => '']
      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $employee->applicant_id,
      'uid' => $uid,
      'contract_type' => 'caf'
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }

  public function get_contract_by_id($id){
    $contract = LaravelEsignatureWrapper::getContract($id);
    return $contract;
  }

  public function loadContractTemplateFinalNotice(Request $request){

    $name = $request->recipient_name;
    $email = $request->recipient_email;

    $template_id_use = config('app.esig')['final_template_id'];
    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/hr-operation-record/success/'.$request->id.'/'.$uid;

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        //pavel ***change email address
        ["name" => "HR Team", "email" =>  "hr@myvirtdesk.com", "auto_sign"=>  "no", "skip_signature_request_email"=>  "yes", "redirect_url" => $redirect, "uid" => $uid
        ],
        //buyer
        ["name" => $name, "email" => $email, "auto_sign"=>  "yes", "skip_signature_request_email"=>  "yes",
        //"redirect_url" => url('referral-thankyou')
        ],
      ),
      "custom_fields" => array(
        ["api_key" => "recipient-name", "value" => $name],
        ["api_key" => "date-current", "value" => $request->current_date],
        ["api_key" => "date-issued", "value" => date('M d, Y')],
        ["api_key" => "va-position", "value" =>  'Developer'],
        ["api_key" => "subject", "value" =>  strip_tags($request->subject)],
        ["api_key" => "body", "value" =>  strip_tags($request->body)],

      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $request->id,
      'uid' => $uid,
      'contract_type' => 'final'
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }

  public function loadContractTemplateHoldSuspension($request){
    $recipient = EmployeeRecord::where('applicant_id', '=', $request->id)->first();

    $name = $request->recipient_name;
    $email = $request->recipient_email;

    $template_id_use = config('app.esig')['esig_suspension_hold'];
    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/hr-operation-record/success/'.$request->id.'/'.$uid;

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        //pavel ***change email address
        ["name" => "HR Team", "email" =>  "hr@myvirtdesk.com", "auto_sign"=>  "no", "skip_signature_request_email"=>  "yes", "redirect_url" => $redirect, "uid" => $uid
        ],
        //buyer
        ["name" => $name, "email" => $email, "auto_sign"=>  "yes", "skip_signature_request_email"=>  "yes",
        //"redirect_url" => url('referral-thankyou')
        ],
      ),
      "custom_fields" => array(
        ["api_key" => "va-name", "value" => $name],
        ["api_key" => "client-lists", "value" => 'test'],
        ["api_key" => "program", "value" => $request->program],
        ["api_key" => "status", "value" => $request->status],
        ["api_key" => "reason-hold-suspension", "value" => $request->reason],
        ["api_key" => "final-decision", "value" => $request->final_decision],

      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $request->id,
      'uid' => $uid,
      'contract_type' => 'hold_suspension'
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }

  public function loadContractTemplateGvaScorecard($id, $type){

    $recipient = EmployeeRecord::where('applicant_id', '=', $id)->first();
    $template_id_use = '';

    $name = $recipient->info->name;
    $email = $recipient->info->email;

    if($type == 'isa'){
      $contract_type = 'isa_quarterly';
      $template_id_use = config('app.esig')['esig_isa_quarterly'];
    }else{
      $contract_type = 'gva_quarterly';
      $template_id_use = config('app.esig')['esig_gva_quarterly'];
    }

    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/hr-operation-record/success/'.$id.'/'.$uid;

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        //pavel ***change email address
        ["name" => "HR Team", "email" =>  "hr@myvirtdesk.com", "auto_sign"=>  "no", "skip_signature_request_email"=>  "yes", "redirect_url" => $redirect, "uid" => $uid
        ],
        //buyer
        ["name" => $name, "email" => $email, "auto_sign"=>  "yes", "skip_signature_request_email"=>  "yes",
        //"redirect_url" => url('referral-thankyou')
        ],
      ),
      "custom_fields" => array(
        ["api_key" => "va-name", "value" => $name],
        ["api_key" => "date-eval", "value" => date('M d, Y')],

      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $id,
      'uid' => $uid,
      'contract_type' => $contract_type
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }

  //send disclosure form
  public function loadContractDisclosure($id){

    $recipient = ApplicantRecord::where('id', '=', $id)->first();

    $name = $recipient->name;
    $email = $recipient->email;

    $template_id_use = config('app.esig')['esig_disclosure_form'];
    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/pre-training-requirements/'.$id.'/edit';

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        ["name" => "HR Team", "email" =>  "hr@myvirtudesk.com", "auto_sign"=>  "no", "redirect_url" => $redirect, "uid" => $uid, "embedded_sign_page"=> "yes", "embedded_redirect_iframe_only"=> "no", "signing_order"=> "1", "skip_signature_request_email" => "yes"],

        ["name" => $name, "email" => $email, "auto_sign"=>  "no", "signing_order"=> "2", "skip_signature_request_email" => "yes", "embedded_sign_page"=> "no"],
      ),
      "custom_fields" => array(
        ["api_key" => "checkbox", "value" => "✔"],
      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    LaravelEsignatureWrapper::sendContractRecipient($contract_id, $url['data']['signers'][1]['id']);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $id,
      'uid' => $uid,
      'contract_type' => 'disclosure_form'
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }

  //send memorandum form
  public function loadContractMemorandum($id){


    $recipient = ApplicantRecord::where('id', '=', $id)->first();

    $name = $recipient->name;
    $email = $recipient->email;

    $template_id_use = config('app.esig')['esig_memorandum_form'];
    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/pre-training-requirements/'.$id.'/edit';

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        ["name" => "HR Team", "email" =>  "hr@myvirtudesk.com", "auto_sign"=>  "no", "redirect_url" => $redirect, "uid" => $uid, "embedded_sign_page"=> "yes", "embedded_redirect_iframe_only"=> "no", "signing_order"=> "1", "skip_signature_request_email" => "yes"],

        ["name" => $name, "email" => $email, "auto_sign"=>  "no", "signing_order"=> "2", "skip_signature_request_email" => "yes", "embedded_sign_page"=> "no"],
      ),
      "custom_fields" => array(
        ["api_key" => "date-issued", "value" => date('M d, Y')],
      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    LaravelEsignatureWrapper::sendContractRecipient($contract_id, $url['data']['signers'][1]['id']);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $id,
      'uid' => $uid,
      'contract_type' => 'memorandum_form'
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }

  //send assurance form
  public function loadAssuranceForm($id){

    $recipient = ApplicantRecord::where('id', '=', $id)->first();

    $name = $recipient->name;
    $email = $recipient->email;

    $template_id_use = config('app.esig')['esig_assurance_form'];
    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/pre-training-requirements/'.$id.'/edit';

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (

        ["name" => "HR Team", "email" =>  "hr@myvirtudesk.com", "auto_sign"=>  "no", "redirect_url" => $redirect, "uid" => $uid, "embedded_sign_page"=> "yes", "embedded_redirect_iframe_only"=> "no", "signing_order"=> "1", "skip_signature_request_email" => "yes"],

        ["name" => $name, "email" => $email, "auto_sign"=>  "no", "signing_order"=> "2", "skip_signature_request_email" => "yes", "embedded_sign_page"=> "no"],
      ),
      "custom_fields" => array(
        ["api_key" => "bullet", "value" => '&#8227;'],
      ),
      "locale" => "en",
      "embedded" => "yes",
      "status" => "queued",
      "test" => $test,
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    LaravelEsignatureWrapper::sendContractRecipient($contract_id, $url['data']['signers'][1]['id']);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $id,
      'uid' => $uid,
      'contract_type' => 'assurance_form'
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }

  //send coc form
  public function loadCocForm($id){

    $recipient = ApplicantRecord::where('id', '=', $id)->first();

    $name = $recipient->name;
    $email = $recipient->email;

    $template_id_use = config('app.esig')['esig_coc_form'];
    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/pre-training-requirements/'.$id.'/edit';

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        ["name" => "HR Team", "email" =>  "hr@myvirtudesk.com", "auto_sign"=>  "no", "redirect_url" => $redirect, "uid" => $uid, "embedded_sign_page"=> "yes", "embedded_redirect_iframe_only"=> "no", "signing_order"=> "1", "skip_signature_request_email" => "yes"],

        ["name" => $name, "email" => $email, "auto_sign"=>  "no", "signing_order"=> "2", "skip_signature_request_email" => "yes", "embedded_sign_page"=> "no"],
      ),
      "custom_fields" => array(
        ["api_key" => "bullet", "value" => '&#8227;'],
      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    LaravelEsignatureWrapper::sendContractRecipient($contract_id, $url['data']['signers'][1]['id']);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $id,
      'uid' => $uid,
      'contract_type' => 'codeofconduct_form'
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }

  //send ica form
  public function loadIcaForm($id){
    $recipient = ApplicantRecord::where('id', '=', $id)->first();

    $name = $recipient->name;
    $email = $recipient->email;

    $template_id_use = config('app.esig')['esig_ica_form'];
    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/pre-training-requirements/'.$id.'/edit';

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        //pavel ***change email address
        ["name" => "HR Team", "email" =>  "hr@myvirtudesk.com", "auto_sign"=>  "no", "redirect_url" => $redirect, "uid" => $uid, "embedded_sign_page"=> "yes", "embedded_redirect_iframe_only"=> "no", "signing_order"=> "1", "skip_signature_request_email" => "yes"],

        ["name" => $name, "email" => $email, "auto_sign"=>  "no", "signing_order"=> "2", "skip_signature_request_email" => "yes", "embedded_sign_page"=> "no"],
      ),
      "custom_fields" => array(
        ["api_key" => "contractor", "value" => $name],
        ["api_key" => "current-date", "value" => date('M d, Y')],
      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    LaravelEsignatureWrapper::sendContractRecipient($contract_id, $url['data']['signers'][1]['id']);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $id,
      'uid' => $uid,
      'contract_type' => 'ica_form'
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }

  //send nca form
  public function loadNcaForm($id){

    $recipient = ApplicantRecord::where('id', '=', $id)->first();

    $name = $recipient->name;
    $email = $recipient->email;

    $template_id_use = config('app.esig')['esig_nc_form'];
    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/pre-training-requirements/'.$id.'/edit';

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        //pavel ***change email address
        ["name" => "HR Team", "email" =>  "hr@myvirtudesk.com", "auto_sign"=>  "no", "redirect_url" => $redirect, "uid" => $uid, "embedded_sign_page"=> "yes", "embedded_redirect_iframe_only"=> "no", "signing_order"=> "1", "skip_signature_request_email" => "yes"],

        ["name" => $name, "email" => $email, "auto_sign"=>  "no", "signing_order"=> "2", "skip_signature_request_email" => "yes", "embedded_sign_page"=> "no"],
      ),
      "custom_fields" => array(
        ["api_key" => "contractor", "value" => $name],
        ["api_key" => "current-date", "value" => date('M d, Y')],
      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    LaravelEsignatureWrapper::sendContractRecipient($contract_id, $url['data']['signers'][1]['id']);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $id,
      'uid' => $uid,
      'contract_type' => 'nca_form'
    ];

    $contract = EmployeeContract::create($data);
    return $contract_url;
  }


  //send fte form
  public function loadFteForm(Request $request){

    $recipient = ApplicantRecord::where('id', '=', $request->id)->first();

    $name = $recipient->name;
    $email = $recipient->email;
    $position_db = $recipient->position_applying_for;

    if($position_db == 'gva'){
      $position = 'General Virtual Assistant';
    }elseif($position_db == 'isa'){
      $position = 'Inside Sales Associate';
    }else{
      $position = 'Executive Virtual Assistant';
    }

    if($request->contract_type == 'fte'){
      $template_id_use = config('app.esig')['esig_fte_form'];
      $ctype = 'fte_form';
    }else{
      $template_id_use = config('app.esig')['esig_nte_form'];
      $ctype = 'nte_form';
    }

    $test = config('app.esig')['test'];

    $uid = uniqid();
    $redirect = url('/').'/admin/hr-operation-record/success/'.$request->id.'/'.$uid;

    $body = $request->body;
    $src = '[screenshot]';

    $array = array();
    preg_match( '/src="([^"]*)"/i', $body, $array ) ;
    $sc = url('/').$array[1];
    $body_final = str_replace($src, $sc, $body);

    $data = [
      "template_id" => $template_id_use,
      "signers" => array (
        //pavel ***change email address
        ["name" => "HR Team", "email" =>  "hr@myvirtudesk.com", "auto_sign"=>  "no", "redirect_url" => $redirect, "uid" => $uid, "embedded_sign_page"=> "yes", "embedded_redirect_iframe_only"=> "no", "signing_order"=> "1", "skip_signature_request_email" => "yes"],

        ["name" => $name, "email" => $email, "auto_sign"=>  "no", "signing_order"=> "2", "skip_signature_request_email" => "yes", "embedded_sign_page"=> "no"],
      ),
      "custom_fields" => array(
        ["api_key" => "recipient", "value" => $name],
        ["api_key" => "current-date", "value" => date('M d, Y')],
        ["api_key" => "position", "value" => $position],
        ["api_key" => "subject", "value" => strip_tags($request->subject)],
        ["api_key" => "body", "value" => strip_tags($body_final)],
      ),

      "locale" => "en",
      "embedded" => 'yes',
      "test" => $test,
      "status" => 'queued',
    ];

    $contract = LaravelEsignatureWrapper::createContract($data);
    $contract_id = $contract['data']['contract_id'];
    $url = LaravelEsignatureWrapper::getContract($contract_id);
    LaravelEsignatureWrapper::sendContractRecipient($contract_id, $url['data']['signers'][1]['id']);
    $contract_url = $url['data']['signers'][0]['embedded_url'];

    $data = [
      'contract_id' => $contract_id,
      'employee_id' => $request->id,
      'uid' => $uid,
      'contract_type' => $ctype
    ];

    $contract = EmployeeContract::create($data);

    $fname = MemoRecord::whereId($request->memoid)->first();
    $fname->memo = $contract->uid;
    $fname->save();

    return $contract_url;
  }
}
