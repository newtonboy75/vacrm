<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ApplicantRecord;
use Illuminate\Http\Request;
use App\InterviewRecord;
use App\ApplicantFirstInterview;
use App\ApplicantExperienceRecord;
use App\ApplicantExperience;
use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;
use App\ApplicantFinalInterview;
use App\ApplicantReference;
use App\ReferenceRecord;
use App\HomeEvaluationRecord;
use App\PhotoAddendum;
use App\Media;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\SystemCheckRecord;
use App\TrainingRecord;
use App\Http\Controllers\Backend\ApplicantRecordController;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use LaravelEsignatureWrapper;
use App\EmployeeContract;
use App\PreTrainingBatch;
use App\PreTrainingNotes;
use App\Http\Controllers\Backend\EmailRecordController;
use App\ApplicantEmergencyContact;
use App\Jobs\SendEmailPretrainingQueues;
use App\Http\Controllers\Backend\AdministrativeController;

/**
 * Class SourcingRecordController.
 */
class PreTrainingRequirementsController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
     public function index(){
       $chart = new AdministrativeController;
       $lava = $chart->getChartAll();
       return view('backend.pre-training', compact('lava'));
     }

     public function load(){
         DB::connection()->disableQueryLog();
       $all_records = null;
       $tab = request()->tab;
       $st = count(request()->archived) > 1 ? 3 : 0;

       if(is_array($tab)){
         $all_records = ApplicantRecord::whereIn('workflow_status', $tab)->where('archived', '<=', $st)->select('workflow_status', 'id', 'name', 'email', 'created_at')->orderBy('updated_at', 'desc')->orderBy('id', 'desc')->limit('20');
       }else{
         $all_records = ApplicantRecord::where('workflow_status', 'pre_training')->where('archived', '<=', $st)->select('workflow_status', 'id', 'name', 'email', 'created_at')->orderBy('updated_at', 'desc')->orderBy('id', 'desc')->limit('300');
       }

       return datatables()->of($all_records)

       ->editColumn('workflow_status', function($all_records){
         return $all_records->status_context();
       })
       ->editColumn('created_at', function($all_records){
         return date('M d Y, h:i', strtotime($all_records->created_at));
       })
       ->editColumn('notes', function($all_records){
           $html = strlen($all_records->pre_training_notes['notes']) >= 60 ? substr($all_records->pre_training_notes['notes'], 0, 59) . '...' : $all_records->pre_training_notes['notes'];
           return '<div style="position: relative; max-width: 200px;">'.$html.'</div>';
       })
       ->editColumn('name', function($all_records){
         return '<a style="color: #1976d2; margin-top: 10px !important; margin-bottom: 10px !important;" href="/admin/pre-training-requirements/'.$all_records->id.'/edit"><strong>'.$all_records->name.'</strong></a>';
       })
       ->rawColumns(['notes', 'name'])
       ->make(true);
     }

     public function edit(Request $request){
       $edit = new ApplicantRecordController;
       return $edit->edit($request);
     }

     public function update(Request $request, TrainingRecord $training_records, ApplicantRecord $applicantRecord){
        DB::connection()->disableQueryLog();
       $applicantEmergencyRecord = null;
       $media = '';

       if($request->body){
        $eval = json_decode($request->body);

        $applicant = $applicantRecord->whereId($eval->applicant_id)->first();

        $upload_path = public_path('/uploaded');

        /***
        if(isset($request->nbi_file)){
          $newpath = $upload_path.'/home_eval/user_'.$eval->applicant_id;
          $filename = $request->nbi_file->getClientOriginalName();
          $uploaded_file = $request->nbi_file->move($newpath, $filename);
        }

        if(isset($request->id_image)){
          $newpath_id = $upload_path.'/employee_profile_pic';
          $filename_id = 'id_'.$eval->applicant_id.'_'.$request->id_image->getClientOriginalName();
          $uploaded_file_id = $request->id_image->move($newpath_id, $filename_id);
        }

        if(isset($request->pou)){
          $newpath_pou = $upload_path.'/employee_pou';
          $filename_pou= 'id_'.$eval->applicant_id.'_'.$request->pou->getClientOriginalName();
          $uploaded_file_id = $request->pou->move($newpath_pou, $filename_pou);
        }
        ***/
        $filename = $eval->nbi_file;
        $filename_id = 'id_'.$eval->applicant_id.'_'.$eval->id_image;
        $filename_pou= 'id_'.$eval->applicant_id.'_'.$eval->pou;

        $applicant->vd_skype = $eval->vd_skype;
        $applicant->vd_skype_pass = $eval->vd_skype_pass;
        $applicant->vd_email = $eval->vd_email;
        $applicant->vd_email_pass = $eval->vd_email_pass;
        $applicant->no_nbi = $eval->no_nbi_comment;
        $applicant->save();

        $employee_emergency_contact = [
          'contact_person_name' => $eval->contact_person,
          'contact_person_relation' => $eval->contact_person_relation,
          'contact_person_number' => $eval->contact_person_number
        ];

        $applicantEmergencyRecord = ApplicantEmergencyContact::updateOrCreate(['applicant_id' => $applicant->id], $employee_emergency_contact);
        unset($applicantEmergencyRecord);

        $media = new Media;
        if($filename != ''){
          $nbi_image = $media->updateOrCreate(['id' => $applicant->nbi_image_id], ['name' => $filename]);
          $applicant->nbi_image_id = $nbi_image->id;
          $applicant->save();
          //unset($nbi_image);
        }

        if($filename_id != ''){
          $profile_image = $media->updateOrCreate(['id' => $applicant->profile_image_id], ['name' => $filename_id]);
          $applicant->profile_image_id = $profile_image->id;
          $applicant->save();
        }

        if($filename_pou != ''){
          $profile_pou = $media->updateOrCreate(['id' => $applicant->pou], ['name' => $filename_pou]);
          $applicant->pou = $profile_pou->id;
          $applicant->save();
        }

        unset($media);

      }else{

        $applicant = $applicantRecord->whereId($request->applicant_id)->first();

        if($applicant){
          $applicant->vd_skype = $request->vd_skype;
          $applicant->vd_skype_pass = $request->vd_skype_pass;
          $applicant->vd_email = $request->vd_email;
          $applicant->vd_email_pass = $request->vd_email_pass;
          $applicant->pre_training_requirements_status = $request->pre_training_requirements_status;
          $applicant->no_nbi = $request->no_nbi_comment;
          $applicant->rate = $request->user_rate;
          if($request->pre_training_requirements_status == '3'){
            $applicant->archived = '3';
          }
          $applicant->save();

          if(isset($request->batch)){
            foreach($request->batch as $k=>$v){
              $training = $training_records->where('id', '=', $request->training_id[$k])->first();
               if($training){

                 if($request->batch[$k] == ''){
                   return 'Batch # is required';
                 }elseif($request->program[$k] == ''){
                   return 'Program is required';
                 }elseif ($request->start_date[$k] == '') {
                   return 'Start Date is required';
                 }

                 $training->trainer_id = $request->trainer_id[$k];
                 $training->batch = $request->batch[$k];
                 $training->program = $request->program[$k];
                 $training->schedule = $request->schedule[$k];
                 $training->start_date = $request->start_date[$k];
                 $training->end_date = $request->end_date[$k];
                 $training->save();

               }else{
                 if($request->batch[$k] == ''){
                   return 'Batch number is required';
                 }elseif($request->program[$k] == ''){
                   return 'Program is required';
                 }elseif ($request->start_date[$k] == '') {
                   return 'Start Date is required';
                 }

                 $data = [
                   'trainer_id' => $request->trainer_id[$k],
                   'batch' => $request->batch[$k],
                   'program' => $request->program[$k],
                   'schedule' => $request->schedule[$k],
                   'start_date' => $request->start_date[$k],
                   'end_date' => $request->end_date[$k],
                   'applicant_id' => $request->applicant_id,
                 ];

                 $new_training = TrainingRecord::create($data);
                 $new_training->save();
                 }
               }

               $batch = PreTrainingBatch::updateOrCreate(['batch_number' => $request->batch[$k]]);
               //if(!$batch){
                 //PreTrainingBatch::create(['batch_number' => $request->batch[$k]]);
               //}


            }
          }

          if($request->pre_training_notes != ''){
            $notes = [
              'notes' => $request->pre_training_notes
            ];
            $recruiter_notes = PreTrainingNotes::updateOrCreate(['applicant_id' => $applicant->id], $notes);
          }

          $upload_path = public_path('/uploaded');
          $filename = '';
          $filename_id = '';

          if($request->nbi_file !== 'undefined'){
            $newpath = $upload_path.'/home_eval/user_'.$applicant->id;
            $filename = $request->nbi_file->getClientOriginalName();
            $uploaded_file = $request->nbi_file->move($newpath, $filename);
            unset($uploaded_file);
          }

          if($request->id_image !== 'undefined'){
            $newpath_id = $upload_path.'/employee_profile_pic';
            $filename_id = 'id_'.$applicant->id.'_'.$request->id_image->getClientOriginalName();
            $uploaded_file_id = $request->id_image->move($newpath_id, $filename_id);
            unset($uploaded_file_id);
          }

          $media = new Media;

          if($filename != ''){
            $nbi_image = $media->updateOrCreate(['id' => $applicant->nbi_image_id], ['name' => '/uploaded/home_eval/user_'.$applicant->id.'/'.$filename]);
            $applicant->nbi_image_id = $nbi_image->id;
            $applicant->save();
          }

          if($filename_id != ''){
            $profile_image = $media->updateOrCreate(['id' => $applicant->profile_image_id], ['name' => $filename_id]);
            $applicant->profile_image_id = $profile_image->id;
            $applicant->save();
          }

          unset($media);

          $employee_emergency_contact = [
            'contact_person_name' => $request->contact_person,
            'contact_person_relation' => $request->contact_person_relation,
            'contact_person_number' => $request->contact_person_number
          ];

        $pplicantEmergencyRecord = ApplicantEmergencyContact::updateOrCreate(['applicant_id' => $applicant->id], $employee_emergency_contact);

        if (Auth::check()){
          $logged_user_id = Auth::id();
          $userlog = new UserLogController;
          $action = 'Updated Training Record for user: '.$request->applicant_id;
          $userlog->save($logged_user_id, $action);
          unset($userlog);
        }
      }

        unset($request);
        unset($applicant);
        unset($pplicantEmergencyRecord);

        return '1';
     }

     public function delete(Request $request){
       $id = TrainingRecord::whereId($request->id)->delete();

       if (Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Deleted Training Record #'.$request->id;
         $userlog->save($logged_user_id, $action);
       }
        unset($request);
        return $id;
     }

     public function sendNotification($applicant, $date, $type_training){
      DB::connection()->disableQueryLog();
       if (Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Sent training schedule to '.$applicant->name;
         $userlog->save($logged_user_id, $action);
         unset($userlog);
       }

       $from = 'requirements@myvirtudesk.com';
       $to = $applicant->email;
       $subject = 'Pre-training Requirements | Virtudesk Requirements Team';
       $dtype = 'pretraining';
       $status = 'sent';

       $save_email = new EmailRecordController();
       $save_email->save($to, $from,	$logged_user_id,	$subject,	$body,	$status, $dtype, $type_training);

       SendEmailPretrainingQueues::dispatch($applicant, $date, $type_training);

       unset($save_email);
       unset($applicant);
     }

     public function sendNotificationBatch(Request $request){

       $batch = PreTrainingBatch::where('batch_number', '=', $request->id)->first();
       $trainees = [];
       $d1 = explode(' ', $batch->start_date);
       $d2 = explode(' ', $batch->end_date);

       $start_date = $d1[0].' '.$d1[1].' '.$d1[2];
       $end_date = $d2[0].' '.$d2[1].' '.$d2[2];

       $start_time = $d1[3].' '.$d1[4];
       $end_time = $d1[6].' '.$d1[7];

       //return $end_time;

       foreach($request->recipients as $trainee){
         $t = ApplicantRecord::where('id', '=', $trainee)->first();
         $trainees[] = $t->email;
       }

       Mail::send('emails.pre-training-bulk', ['start_date' => $start_date, 'end_date' => $end_date, 'start_time' => $start_time, 'end_time' => $end_time], function ($m) use ($trainees) {
         $m->from('requirements@myvirtudesk.com', 'MyVirtudesk');
         $m->cc('requirements@myvirtudesk.com', 'MyVirtudesk');
         $m->replyTo('requirements@myvirtudesk.com');
         $m->to($trainees)
         ->subject('IMPORTANT: PLEASE READ THE WHOLE EMAIL - Virtudesk - Training Reminder');
       });

       if(count(Mail::failures()) > 0 ) {
         return 'An error occured while sending bulk email. Please contact Newton';
       }else{
         $batch->email_sent = 'yes';
         $batch->save();

         if(Auth::check()){
           $userlog = new UserLogController;
           $logged_user_id = Auth::id();
           $action = 'Sent batch email #'.$batch->batch_number;
           $userlog->save($logged_user_id, $action);
         }
         //gc_collect_cycles();
         return '1';
       }
     }

     public function create_training_record(Request $request){

       $applicant = ApplicantRecord::whereId($request->applicant_id)->with('trainings')->first();
       $applicant->workflow_status = 'training_record';

       if($applicant->trainings->count() <= 0){
         return 0;
         exit;
       }

       $applicant->save();

       if (Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created Training Record for user: '.$request->applicant_id;
         $userlog->save($logged_user_id, $action);
         unset($userlog);
         unset($action);
       }

       if($applicant){
            unset($request);
            return '1';
       }

        unset($request);
        return '0';
     }

     public function revoke($id){

       $contract = EmployeeContract::whereId($id)->first();

       $revoke_contract = LaravelEsignatureWrapper::revokeContract(['id' => $contract->contract_id]);
       return redirect()->back()->with('success', 'Contract withdrawn');
     }

     public function get_trainers(Request $request){
       $html = '';
       $alltrainer = User::role('trainer')->get();
       foreach($alltrainer as $trainer){
         $html .= '<option value="'.$trainer->id.'">'.$trainer->first_name.' '.$trainer->last_name.'</option>';
       }

       unset($alltrainer);
       return $html;
     }

     public function batches(){
       $batches = PreTrainingBatch::orderBy('batch_number', 'desc')->get();
       return datatables()->of($batches)

       ->addColumn('number_trainees', function($batches){
         $num_training = TrainingRecord::where('batch', '=', $batches->batch_number)->get();
         return $num_training->count() ?? '0';
        })

       ->make(true);
     }

     public function batch(Request $request){

       //return $request;

       if($request->batch_num == '' || !isset($request->batch_num)){
         return 'Batch number is required';
       }

       $start_date = $request->start_date.' '.$request->start_time.' - '.$request->end_time;
       $end_date = $request->end_date.' '.$request->start_time.' - '.$request->end_time;

       $batch = PreTrainingBatch::updateOrCreate(['batch_number' => $request->batch_num], ['start_date' => $start_date, 'end_date' => $end_date]);

       if (Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created/updated Training Batch: #'.$request->batch_num;
         $userlog->save($logged_user_id, $action);
       }

       return '1';
     }

     public function destroy_batch(Request $request){
       $batch = PreTrainingBatch::whereId($request->id)->delete();
       if (Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Deleted Training Batch: #'.$request->id;
         $userlog->save($logged_user_id, $action);
       }
       return $batch;
     }

     public function batch_trainers(Request $request){
       $trainings = TrainingRecord::where('batch', '=', $request->batch_num)->with('trainee')->get();
       $html = '';

       foreach($trainings as $training){

         if(isset($training->trainee->id)){
           $html .= '<tr><td><div class="form-check"><input checked class="form-check-input" type="checkbox" value="'.$training->trainee->id.'" id="defaultCheck1"><label class="form-check-label" for="defaultCheck1"></label></div></td><td>'.$training->trainee->name.'</td><td>'.$training->trainee->email.'</td><td>'.$training->program.'</td></tr>';
         }

       }

       unset($trainings);
       return $html;
     }

     public function getNewBatch(Request $request){
       $batches = PreTrainingBatch::where('email_sent', '=', 'no')->orderBy('id', 'desc')->get();
       $html = '';

       foreach($batches as $batch){
         $html .= '<option value="'.$batch->batch_number.'">'.$batch->batch_number.'</option>';
       }

       unset($batches);
       return $html;
     }


     public function getNewBatchDetails(Request $request){

       $batch = PreTrainingBatch::where('batch_number', '=', $request->id)->first();

       $data = [
         'start' => $batch->start_date,
         'end' => $batch->end_date
       ];

        unset($request);

       if($data){
         return $data;
       }
     }

     public function getNewBatcheshDetails(Request $request){

       //return $request;

       $mybatch = preg_replace('/[^a-zA-Z]/', '', $request->program);
       $batch = [
         '1' => 'ISA',
         '2' => 'GVA',
         '3' => 'EVA',
         '4' => 'TC'
       ];

       $batch = PreTrainingBatch::where('batch_number', 'REGEXP', $batch[$request->program])->get();

        unset($request);

       if($batch){
         return $batch;
       }

     }

}
