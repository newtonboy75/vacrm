<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Lava;
use App\ApplicantRecord;
use App\Models\Auth\User;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Http\Request;
use App\ApplicantFirstInterview;
use App\BlockSchedule;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\EmployeeContract;
use Illuminate\Support\Facades\Mail;
use App\MemoRecord;
use App\TrainingRecord;
use App\Http\Controllers\Backend\EmailRecordController;
use App\Charts\BarChartStat;
//SendEmailAllQueues
use App\Jobs\SendEmailAllQueues;
use App\Jobs\SendEmailContractsQueues;
use App\Jobs\CreateContractFile;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Image;


/**
* Class DashboardController.
*/
class DashboardController extends Controller
{
  /**
  * @return \Illuminate\View\View
  */

  use HasRoles;

  public function index()
  {

    $sourcing_record = ApplicantRecord::where('workflow_status', '=', 'sourcing_record')->where('archived', '=', '0')->orderBy('id', 'desc')->get();
    $talent_acquisition = ApplicantRecord::where('workflow_status', '=', 'talent_acquisition')->where('archived', '=', '0')->orderBy('id', 'desc')->get();
    $reference_check = ApplicantRecord::where('workflow_status', '=', 'reference_check')->where('archived', '=', '0')->orderBy('id', 'desc')->get();
    $pre_training = ApplicantRecord::where('workflow_status', '=', 'pre_training')->where('archived', '=', '0')->orderBy('id', 'desc')->get();
    $training = ApplicantRecord::where('workflow_status', '=', 'training_record')->where('archived', '=', '0')->orderBy('id', 'desc')->get();

    $chart = new BarChartStat;
    $chart->labels(['Sourcing Record', 'Talent Acquition', 'Reference Check', 'PreTraining', 'Training']);
    $chart->dataset('Current Applicants in Each Department', 'horizontalBar', [$sourcing_record->count(), $talent_acquisition->count(), $reference_check->count(), $pre_training->count(), $training->count()])
    ->backgroundColor('#ff4810')
    ->lineTension(8.2)
    ->fill(false);
    return view('backend.dashboard', compact('chart', 'sourcing_record', 'talent_acquisition', 'reference_check', 'pre_training', 'training'));
  }

  public function recruiters_hub(){

    $title = 'Recruiter\'s Hub';
    $recruiters = User::role('recruiter manager')->get();
    $users = ApplicantRecord::whereIn('recruiter_id', $recruiters)->get();
    unset($recruiters);
    return view('backend.recruiters-hub', compact('title', 'users'));
  }

  public function load_interview_schedules(Request $request){

    $html = '';

    if($request->id == '0'){

      $initial_interviews = ApplicantFirstInterview::with('interview_record')->get();

      if($initial_interviews){
        foreach($initial_interviews as $d=>$fi){
          if(isset($fi->interview_record[0]->date)){
            $dbdate = date('Y-m-d', strtotime($fi->interview_record[0]->date));
            if($dbdate == $request->date){
              $fi->interview_record[0]->applicant = $fi->applicant_id;
              $interviews[strtotime($fi->interview_record[0]->date)][] = $fi->interview_record;
            }
          }
        }
      }

      if(!isset($interviews)){
        return '<p>&nbsp;</p><p>No schdule for today yet</p><p>&nbsp;</p>';
        exit;
      }

      ksort($interviews);

      if($request->external == '1'){
        foreach($interviews as $l=>$f){
          $info[$l]['date'] = date('g:i a', strtotime($f[0][0]->date));
          $info[$l]['count'] = count($f);

          foreach($f as $d=>$f1){
            $apid = $f1[0]->applicant ?? 'Recruiter Not Available';
            $iid =  $f1[0]->id;

            if(isset($f1[0]->applicant)){
              $applicant = ApplicantRecord::whereId($apid)->first();
              $recruiter = User::whereId($applicant->recruiter_id)->first();
              $info[$l]['recruiter_id'] = $applicant->recruiter_id;
            }else{
              $recruiter = BlockSchedule::where('interview_id', '=', $iid)->with('recruiter')->first();
              $recid = $recruiter->recruiter_id ?? '';
              $info[$l]['recruiter_id'] = $recid;
              unset($recruiter);
            }
          }
        }

        return $info;
        exit;
      }

      if(count($interviews) > 0){
        foreach($interviews as $f){
          $html .= '<li id="current">';
          $html .= '<a id="tl" disabled href="javascript:;" rel="s">'.date('g:i a', strtotime($f[0][0]->date)).'</a><span class="float-right text-danger font-weight-bold" id="dcount">'.count($f).'</span><p>';

          foreach($f as $d=>$f1){
            $apid = $f1[0]->applicant ?? 'Recruiter Not Available';
            $iid =  $f1[0]->id;

            if(isset($f1[0]->applicant)){
              $applicant = ApplicantRecord::whereId($apid)->first();

              $recuiter_id = isset($applicant->recruiter_id) ?? '';

              if(isset($recuiter_id)){
                $recruiter = User::whereId($recuiter_id)->first();
                $badge_color = isset($recruiter->badge_color) ?? 'gray';
                $applicant_name = isset($applicant->name) ?? '';
              }

              $html .= '<span title="delete schedule" rel="'.$recuiter_id.'" id="badge" class="badge-'.$badge_color.'"><i class="fas fa-calendar-check"></i> '.$applicant_name.'<span rel="'.$iid.'" id="del-sked">x</span></span>&nbsp;';
            }else{
              $recruiter = BlockSchedule::where('interview_id', '=', $iid)->with('recruiter')->first();
              $color = $recruiter->recruiter[0]->badge_color ?? 'gray';
              $recid = $recruiter->recruiter_id ?? '';
              $html .= '<span title="delete schedule" rel="'.$recid.'" id="badge" class="badge-'.$color.'"><i class="fas fa-calendar-times"></i> recruiter not available<span rel="'.$iid.'" id="del-sked">x</span></span>&nbsp;';
              unset($recruiter);
            }
          }
          $html .= '</p></li>';
        }
      }
    }

    unset($request);
    unset($interviews);
    unset($initial_interviews);
    gc_collect_cycles();
    return $html;

  }

  public function load_recruiters(Request $request){
    $html = '';
    $recruiters = User::role('recruiter')->get();
    foreach($recruiters as $recruiter){
      $recs[$recruiter->id] = array($recruiter->id, $recruiter->first_name.' '.$recruiter->last_name, $recruiter->badge_color);
    }

    unset($recruiters );
    unset($request);
    return $recs;
  }

  public function get_upload_image($request){
    return 'https://vetiverscent.com/wp-content/uploads/2019/01/oud-base-fragrance-1180x664.jpg';
  }

  public function esignature($type = null, $to = null, $memoid = null, $id = null){

    $applicant = ApplicantRecord::whereId($to)->first();
    $contract = '';

    $type_of_contract = '';

    $contract_type = [
      'caf' =>  'Corrective Action Form',
      'gva_quarterly' => 'GVA Quarterly',
      'final' => 'Final',
      'hold-suspension' => 'Hold Suspension',
      'isa-quarterly' => 'ISA Quarterly',
      'disclosure-form' => 'Disclosure',
      'memorandum-form' => 'Memorandum',
      'assurance-form' => 'Assurance Form',
      'codeofconduct-form' => 'Code of Conduct',
      'ica-form' => 'ICA Form',
      'nca-form' => 'NCA Form',
      'fte-form' => 'FTE Form',
      'nte-form' => 'NTE Form',
      'termination-letter' => 'Termination Letter',
      'ica-eva' => 'ICA EVA',
      'ica-isa' => 'ICA ISA',
      'ica-gva' => 'ICA GVA'
    ];


    $type_of_contract = $contract_type[$type];

    if(isset($id)){
      $contract = EmployeeContract::whereId($id)->with('actions')->first();
      $doc = '';


      if($contract){
        $doc = $this->create_file($contract->uid, $type_of_contract);

        if($contract->signed != 'no'){
          return view('backend.esignature_pending')->withContract($contract)->withApplicant($applicant)->withContractType($type_of_contract)->withDoc($doc);
        }

      }else{
        $doc = $this->create_file(null, $type);
      }

      return view('backend.esignature')->withContract($contract)->withApplicant($applicant)->withContractType($type_of_contract)->withDoc($doc);

    }else{
      $doc = $this->create_file(null, $type);
      return view('backend.esignature')->withContract($contract)->withApplicant($applicant)->withContractType($type_of_contract)->withDoc($doc);
    }
  }

  public function esignature_save(Request $request){

    //dd($request);

    $contract_uid = $request->contract_id;
    $employee_id = $request->employee_id;
    $contract_type = $request->contract_type;

    $contract = EmployeeContract::where('uid', '=', $contract_uid)->first();

    if(!$contract){
      $uid = uniqid();
      $data = [
        'employee_id' => $employee_id,
        'contract_type' =>$contract_type,
        'uid' => $uid,
        'contract_id' => $uid,
      ];
      $contract = EmployeeContract::create($data);
    }

    if(null !== $request->doc_body){
      //$this->process_file($request->doc_body, $contract->uid);
      //dispatch(new CreateContractFile($request->doc_body, $contract->uid));
      //CreateContractFile::dispatch($request->doc_body, $contract->uid)->delay(now()->addSeconds(30));

      //uue this
      //CreateContractFile::dispatchNow($request->doc_body, $contract->uid);

        $url = 'https://services.myvirtudesk.com/api/contract/create_contract_file';

        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post($url, [
        'form_params' => [
        'email' => 'newtonboy@gmail.com',
        'password' => 'renton75',
        'doc_body' => $request->doc_body,
        'uid' => $contract->uid
          ]
        ]);
        $r = json_encode($result->getBody()->getContents());

    }

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Created/saved contract: #'.$contract_type.' - '. $contract->uid;
      $userlog->save($logged_user_id, $action);
      unset($userlog);
      unset($r);
    }

    return $contract;
  }

  public function process_file($doc_body, $uid){

    $newfile = public_path().'/form_templates/'.$uid;
    $html = $doc_body;

    if(!file_exists($newfile)){
      fopen($newfile, "a");
    }

    preg_match_all('/<img[^>]+>/i', $html, $imgTags);

    for ($i = 0; $i < count($imgTags[0]); $i++) {
      preg_match('/src="([^"]+)/i',$imgTags[0][$i], $imgage);
      $origImageSrc[] = str_ireplace( 'src="', '',  $imgage[0]);
      $url =  str_ireplace( 'src="', '',  $imgage[0]);

      if(!strstr($url, 'data:image')){
        $path =  public_path().$url;
        $imgData = base64_encode(file_get_contents($path));

        //convert img to base64
        $src = 'data:'.mime_content_type($path).';base64,'.$imgData;
        $html = str_replace($url, $src, $doc_body);
      }
    }

    unset($imgTags);
    file_put_contents($newfile, $html);

    gc_collect_cycles();
  }

  public function esignature_send(Request $request){

    $token = uniqid();
    $applicant = ApplicantRecord::whereId($request->employee_id)->first();
    $contract = EmployeeContract::where('uid', '=', $request->contract_id)->first();


    if(!$contract){
      $contract = $this->esignature_save($request);
    }

    $contract->contract_id = $token;
    $contract->signed = 'sent';
    $contract->save();

    $applicant_name = ucfirst($applicant->name);
    //$landing_url = route('sign', ['id'=>$token.'_'.$contract->uid]);
    $landing_url = 'https://virtudeskpro.com/sign/'.$token.'_'.$contract->uid;

    $hr_email = 'hr@myvirtudesk.com';
    $requirements_email = 'requirements@myvirtudesk.com';
    $hr_subject = 'Human Resource | Virtudesk';
    $requirements_subject = 'Pre-training Requirements | Virtudesk Requirements Team';

    $contract_type = [
      'caf' =>  array('Corrective Action Form', $hr_email, $hr_subject),
      'gva_quarterly' => array('GVA Quarterly', $hr_email, $hr_subject),
      'final' => array('Final', $hr_email, $hr_subject),
      'hold-suspension' => array('Hold Suspension', $hr_email, $hr_subject),
      'isa-quarterly' => array('ISA Quarterly', $hr_email, $hr_subject),
      'disclosure-form' => array('Disclosure', $hr_email, $hr_subject),
      'fte-form' => array('FTE Form', $hr_email, $hr_subject),
      'nte-form' => array('NTE Form', $hr_email, $hr_subject),
      'termination-letter' => array('Termination Letter', $hr_email, $hr_subject),
      'memorandum-form' => array('Memorandum', $requirements_email, $requirements_subject),
      'assurance-form' => array('Assurance Form', $requirements_email, $requirements_subject),
      'codeofconduct-form' => array('Code of Conduct', $requirements_email, $requirements_subject),
      'ica-form' => array('ICA Form', $requirements_email, $requirements_subject),
      'nca-form' => array('NCA Form', $requirements_email, $requirements_subject),
      'ica-eva' => array('ICA EVA', $requirements_email, $requirements_subject),
      'ica-isa' => array('ICA ISA', $requirements_email, $requirements_subject),
      'ica-gva' => array('ICA GVA', $requirements_email, $requirements_subject),
    ];

    $email_use = '';

    if($request->contract_type != 'memorandum-form' || $request->contract_type != 'assurance-form' || $request->contract_type != 'codeofconduct-form' || $request->contract_type != 'ica-form' || $request->contract_type != 'disclosure' || $request->contract_type != 'ica-eva' || $request->contract_type != 'ica-isa'){

      $memo = MemoRecord::whereId($request->memoid)->first();

      if($memo){
        $memo->memo = $contract->contract_id;
        $memo->save();
      }
    }

    $micar_contracts = ['ICA Form', 'Memorandum', 'Disclosure', 'Assurance Form', 'Code of Conduct', 'ICA EVA', 'ICA ISA', 'ISA GVA'];
    $email_use = $applicant->email;


    $mycontract = $contract_type[$request->contract_type][0];
    $contract1 = $contract_type[$request->contract_type][1];
    $contract2 = $contract_type[$request->contract_type][2];

    $new_request = $request->all();

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Sent contract: # '.$contract_type[$contract->contract_type][0].' - '.$contract->contract_id;
      $userlog->save($logged_user_id, $action);
      unset($userlog);
    }

    //SendEmailContractsQueues::dispatchNow($applicant, $applicant_name, $new_request, $email_use, $landing_url, $mycontract, $contract1, $contract2);

    $url = 'http://52.10.202.252/api/contract/create_contract';

    $client = new Client(); //GuzzleHttp\Client
    $result = $client->post($url, [
      'form_params' => [
        'email' => 'newtonboy@gmail.com',
        'password' => 'renton75',
        'applicant' => $applicant,
        'applicant_name' => $applicant_name,
        'new_request' => $new_request,
        'email_use' => $email_use,
        'landing_url' => $landing_url,
        'mycontract' => $mycontract,
        'contract1' => $contract1,
        'contract2' => $contract2
      ]
    ]);

    //$r = json_encode($result->getBody()->getContents());
    unset($result);

    return  redirect()->back()->with('success','Contract sent');
  }

  public function create_file($fname, $template_type){

    $doc = '';
    $file = null;

    if(null !== $fname){
      $file = public_path().'/form_templates/'.$fname;
    }else{
      $file = public_path().'/form_templates/templates/'.$template_type;
    }

    if(file_exists($file)){
        if ($fh = fopen($file, 'r')) {
            while (!feof($fh)) {
            $doc .= fgets($fh);
            }
            fclose($fh);
            return $doc;
        }
    }

    gc_collect_cycles();

  }

  public function avatar(Request $request){
    if($request->hasFile('avatar')){
      $avatar = $request->file('avatar');

      $filename = preg_replace("/(\W)+/", "", strtolower($request->first_name)).'_'.preg_replace("/(\W)+/", "", strtolower($request->last_name)).'-'.$request->user_id . '.' . $avatar->getClientOriginalExtension();
      \Image::make($avatar)->resize(300,300)->save( public_path('/img/backend/employee_photo/avatar/' . $filename) );
    }
  }

  public function mailer(Request $request){
    $new_request = null;
    $to_app = $request->to;
    $type = $request->type;
    $html = $request->body;
    $subject = $request->subject;
    $from = explode('|', $request->from);
    $status = 'sent';
    $dtype = 'talent';

    $new_request = $request->all();
    //dispatch(new SendEmailAllQueues($new_request));

    if($request->email_type == 'reapplied' || $request->email_type == 'nonfilipino'){
      $applicant = ApplicantRecord::whereId($request->user_id)->first();
      $dtype = 'sourcing';
      if($applicant){
        $applicant->archived = '1';
        $applicant->save();
        unset($applicant);
      }

    }elseif($request->email_type == 'pretrain'){
      $training = TrainingRecord::whereId($request->training_id)->first();
      $dtype = 'pretraining';
      if($training){
        $training->email_sent = 'yes';
        $training->save();
        $dtype = 'pretraining';
      }
      unset($training);
    }

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Sent email: '.$request->email_type;
      $userlog->save($logged_user_id, $action);
      unset($userlog);
    }

    if($request->email_type == 'resend-cv'){
      $dtype = 'sourcing';
    }elseif ($request->email_type == 'pretrain2') {
      $dtype = 'pretraining';
    }elseif($request->email_type == 'training-resched'){
      $dtype = 'training';
    }

    $save_email = new EmailRecordController();
    $save_email->save($to_app,	$from[0],	$logged_user_id,	$subject,	$html,	$status, $dtype, $request->email_type);

    //SendEmailAllQueues::dispatchNow($new_request);
    $this->sendAllEmail($new_request);

    unset($save_email);
    unset($request);
    unset($new_request);
    return '1';
  }

  public function sendAllEmail($request){

    $url = 'https://services.myvirtudesk.com/api/send_all_email';

    $client = new Client(); //GuzzleHttp\Client
    $result = $client->post($url, [
      'form_params' => [
        'email' => 'newtonboy@gmail.com',
        'password' => 'renton75',
        'allrec' => $request,
      ]
    ]);

    //$r = json_encode($result->getBody()->getContents());
    //unset($r);
  }

  //userview and edit sendConfirmationEmail
  public function profile($action){
    $logged_user_id = Auth::id();
    $user = User::whereId($logged_user_id)->first();

    if($action=='view'){
      return view('backend.profile-show', compact('user'));
    }elseif($action=='edit'){
      return view('backend.profile-edit', compact('user'));
    }
  }

  public function profile_update(Request $request){

    $user = User::whereId($request->userid)->first();
    $filename = $user->avatar_location ?? 'temp.jpg';

    if($request->file('avatar') != null){
      $file = $request->file('avatar');
      $filename = $file->getClientOriginalName();
      $dir = public_path('img/backend/employee_photo/avatar');
      $resize_image = Image::make($file->getRealPath());
      $resize_image->resize(720, null, function($constraint){
        $constraint->aspectRatio();
      })->save($dir . '/' . $filename);
    }


      $user->first_name = $request->first_name;
      $user->last_name = $request->last_name;
      $user->email = $request->email;
      $user->password = \Hash::make($request->password);
      $user->skype_id = $request->skype_id;
      $user->timesched = serialize($request->timesched);
      $user->day_of_week = serialize($request->day_of_week);
      $user->avatar_location = $filename;
      $user->save();

      Auth::login($user);
    return back()->withFlashSuccess('Profile updated.');

  }

//end class
}
