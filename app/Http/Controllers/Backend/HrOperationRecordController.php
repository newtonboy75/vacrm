<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ApplicantRecord;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\FosGroup;
use App\JobRecord;
use App\EmployeeJob;
use App\Models\Auth\User;
use App\CoachingRecord;
use App\ApplicantCoaching;
use App\MemoRecord;
use App\ApplicantMemo;
use App\EmployeeRecord;
use App\CheckInRecord;
use App\ApplicantCheckin;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Backend\EsignatureController;
use App\ClientRecord;
use App\EmployeeContract;
use Illuminate\Support\Facades\Mail;
use App\EmployeeCheckInRecord;
use PDF;
use LaravelEsignatureWrapper;
use App\Media;
use App\Http\Controllers\Backend\AdministrativeController;
/**
 * Class SourcingRecordController.
 */
class HrOperationRecordController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
     public function index(){
       $chart = new AdministrativeController;
       $lava = $chart->getChartAll();
       return view('backend.hr-record', compact('lava'));
     }

     public function load(){

       $tab = request()->tab;
       $stats = request()->archived;

       if($stats != ''){
         $type = request()->hr_type;

         if($type == 'all'){
           $stats = ['0', '1', '2', '3', null];
           $tab =  ['hr_record'];
         }elseif($type == 'active'){
           $stats = [0];
           $tab =  ['hr_record'];
         }elseif($type == 'inactive'){
           $stats = ['1'];
           $tab =  ['hr_record'];
         }
       }


       //$all_records = ApplicantRecord::whereIn('workflow_status', $tab)->whereIn('archived', $stats)->groupBy('name')->orderBy('id', 'desc')->get();
       $all_records = ApplicantRecord::where('workflow_status', '=', 'hr_record')->whereIn('archived', $stats)->orderBy('id', 'desc');
       //$all_records = ApplicantRecord::where('name', '!=', null)->groupBy('name')->where('workflow_status', '=', 'hr_record')->whereIn('archived', $stats)->orderBy('id', 'desc');

       //Log::info($all_records);

       return datatables()->of($all_records)

       ->editColumn('name', function($all_records){
         $html = $all_records->name ?? '';
         return $html;
       })

       ->editColumn('email', function($all_records){
         $html = $all_records->vd_email ?? '';
         return $html;
       })

       ->editColumn('status', function($all_records){
         $html = $all_records->status_context() ?? '';
         return $html;
       })

       ->addColumn('date', function($all_records){
         $html = date('F d, Y h:i', strtotime($all_records->created_at)) ?? '';
         return $html;
       })

       ->editColumn('name', function($all_records){
         return '<a style="color: #1976d2; margin-top: 10px !important; margin-bottom: 10px !important;" href="/admin/hr-operation-record/'.$all_records->id.'/edit"><strong>'.$all_records->name.'</strong></a>';
       })
       ->rawColumns(['name'])

       ->make(true);

     }

     public function edit(Request $request){
       $edit = new ApplicantRecordController;
       return $edit->edit($request);
     }

     public function update(Request $request){

       if($request->from_tab == 'empl_info'){
         return $this->save_employment_info($request);
       }elseif($request->from_tab == 'coaching'){
         return $this->save_coaching($request);
       }elseif($request->from_tab == 'memo'){
         return $this->save_memo($request);
       }elseif($request->from_tab == 'emp_info'){
         return $this->save_emp_info($request);
       }elseif($request->from_tab == 'va_checkin'){
         return $this->save_va_checkin($request);
       }elseif($request->from_tab == 'credential'){
         return $this->save_employee_credentials($request);
       }
     }

     public function save_employee_credentials(Request $request){

       $applicant = ApplicantRecord::whereId($request->applicant_id)->first();

       if($applicant){
         $applicant->vd_email = $request->vd_email;
         $applicant->vd_email_pass = $request->vd_email_pass;
         $applicant->vd_skype = $request->vd_skype;
         $applicant->vd_skype_pass = $request->vd_skype_pass;
         $applicant->save();
       }

        unset($request);

       return '1';
     }

     public function save_va_checkin($request){

       foreach($request->issued_by as $k=>$req){
         $data = [
           'served_date' => date('Y-m-d h:i:s', strtotime($request['served_date'][$k])),
           'signed_date' => date('Y-m-d h:i:s', strtotime($request['signed_date'][$k])),
           'issued_by' => $request['issued_by'][$k],
           'file' => $request['file'][$k]
         ];

         $new_checkin = CheckInRecord::updateOrCreate(['id' => $request['va_id'][$k]], $data);

         $data2 = [
           'employee_id' => $request->applicant_id,
           'checkin_id' => $new_checkin->id
         ];

         $applicant_checkin = ApplicantCheckin::updateOrCreate(['checkin_id' => $new_checkin->id], $data2);

       }

       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Saved VA Checkin for employee ID: '.$request->applicant_id;
         $userlog->save($logged_user_id, $action);
       }

        unset($request);

       return '1';

     }

     public function save_emp_info($request){

       $applicant = ApplicantRecord::whereId($request->applicant_id)->first();

       if($request->file('emp_photo')){
         $file = $request->file('emp_photo');
         $img_name = $request->applicant_id.'_'.$file->getClientOriginalName();
         $file->move(public_path('/uploaded/employee_profile_pic'), $img_name);

         $profile_pic = $applicant->profile_image_id;

         if($profile_pic != ''){

           $img = Media::whereId($profile_pic)->first();
           $img->name = $img_name;
           $img->save();
         }else{
           $img = Media::create(['name' => $img_name]);
           $applicant->profile_image_id = $img->id;
         }

       }

       if($request->user_status == '3'){
         $applicant->archived = '1';
       }else{
         $applicant->archived = '0';
       }

       $applicant->status = $request->user_status;
       $applicant->name = $request->applicant_name;
       $applicant->address = $request->address;
       $applicant->mobile_number = $request->mobile_number;
       $applicant->landing_number = $request->landing_number;
       $applicant->va_level = $request->va_level;
       $applicant->save();

       if(isset($request->lob)){
         $employee_record = EmployeeRecord::where('applicant_id', '=', $request->applicant_id)->first();
         $employee_record->lob = $request->lob;
         $employee_record->save();
       }

       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Saved Employment Information for employee ID: '.$request->applicant_id;
         $userlog->save($logged_user_id, $action);
       }

        unset($request);

       return '1';

     }

     public function save_employment_info($request){

       $reload = 'yes';

       if(!isset($request->client_id)){
         return 'Client is requied';
       }

       foreach($request->empid as $k=>$req){

         //if($request->empid[$k] == ''){
           //$reload = 'yes';
         //}

         $data = [
           'description' => $request->job_description[$k],
           'hourly_rate' => $request->hourly_rate[$k],
           'schedule' => $request->schedule[$k],
           'manager' => $request->manager[$k],
           'start_date' => date('Y-m-d h:i:s', strtotime($request->start_date[$k])),
           'end_date' => date('Y-m-d h:i:s', strtotime($request->end_date[$k])),
           'status' => $request->empstatus[$k],
           'notes' => $request->note[$k],
           'tools' => $request->system_tools[$k],
           'team' => $request->team[$k],
           'level' => $request->level[$k],
           'employment_status' => $request->employment_status[$k],
           'client_id' => $request->client_id[$k]
         ];

         $new_emp_info = JobRecord::updateOrCreate(['id' => $request->empid[$k]], $data);

         $data2 = [
           'job_id' => $new_emp_info->id,
           'employee_id' => $request->applicant_id
         ];
         $new_emp_job_info = EmployeeJob::updateOrCreate(['job_id' => $new_emp_info->id], $data2);
       }

       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Saved Employment Job Information for employee ID: '.$request->applicant_id;
         $userlog->save($logged_user_id, $action);
       }

       unset($request);
       unset($data);

       if($reload == 'yes'){
         return '1';
       }



       return '0';
     }

     public function save_coaching($request){


        foreach($request->issued_by as $k=>$req){

          $data = [
            'date_served' => date('Y-m-d h:i:s', strtotime($request->date_served[$k])),
            'date_signed' => date('Y-m-d h:i:s', strtotime($request->date_signed[$k])),
            'issued_by' => $request->issued_by[$k],
            'status' => $request->status[$k],
            'violation' => $request->violation[$k],
          ];

          $new_coaching = CoachingRecord::updateOrCreate(['id' => $request->coaching_id[$k]], $data);

          $fle = '';
          $fname = '';
          if(isset($request->file('file')[$k])){
            $fle = $request->file('file')[$k]->getClientOriginalExtension();
            $i = date("M_D_Y_h_i_s");
            $user_dir = public_path('/uploaded/coaching').'/coaching_'.$request->applicant_id.$i.'-'.$new_coaching->id.'.'.$fle;
            $uploaded_file = $request->file('file')[$k]->move(public_path().'/uploaded/coaching/', $user_dir);
            $fname = 'coaching_'.$request->applicant_id.$i.'-'.$new_coaching->id.'.'.$fle;
            $new_coaching->memo = $fname;
            $new_coaching->save();
          }

          $data2 = [
            'employee_id' => $request->applicant_id,
            'coaching_log_id' => $new_coaching->id
          ];
          $new_emp_coaching = ApplicantCoaching::updateOrCreate(['coaching_log_id' => $new_coaching->id], $data2);

        }

        if(Auth::check()){
          $userlog = new UserLogController;
          $logged_user_id = Auth::id();
          $action = 'Saved Coaching Information for employee #'.$request->applicant_id;
          $userlog->save($logged_user_id, $action);
        }

        unset($request);
         unset($data);

        return '1';

     }

     public function save_memo($request){

        foreach($request->issued_by as $k=>$req){

          $data = [
            'date_served' => date('Y-m-d h:i:s', strtotime($request->date_served[$k])),
            'date_signed' => date('Y-m-d h:i:s', strtotime($request->date_signed[$k])),
            'issued_by' => $request->issued_by[$k],
            'status' => $request->status[$k],
            'violation' => $request->violation[$k],
            'memo' => $request->termination_file[$k]
          ];

          $new_memo = MemoRecord::updateOrCreate(['id' => $request->coaching_id[$k]], $data);

          $fle = '';
          $fname = '';
          if(isset($request->file('file')[$k])){
            $fle = $request->file('file')[$k]->getClientOriginalExtension();
            $i = date("M_D_Y_h_i_s");
            $user_dir = public_path('/uploaded/memo').'/memo_'.$request->applicant_id.$i.'-'.$new_memo->id.'.'.$fle;
            $uploaded_file = $request->file('file')[$k]->move(public_path().'/uploaded/memo/', $user_dir);
            $fname = 'memo_'.$request->applicant_id.$i.'-'.$new_memo->id.'.'.$fle;
            $new_memo->memo = trim($fname);
            $new_memo->save();
          }

          $data2 = [
            'employee_id' => $request->applicant_id,
            'memo_id' => $new_memo->id
          ];
          $new_emp_memo = ApplicantMemo::updateOrCreate(['memo_id' => $new_memo->id], $data2);

        }

        if(Auth::check()){
          $userlog = new UserLogController;
          $logged_user_id = Auth::id();
          $action = 'Saved Memo for employee # '.$request->applicant_id;
          $userlog->save($logged_user_id, $action);
        }

        unset($request);
        unset($data);

        return '1';

     }

     public function getAllCoaches(){
       $html = '';
       $allcoaches = User::role('coaches')->orderBy('first_name', 'asc')->get();
       foreach($allcoaches as $coach){
         $html .= '<option value="'.$coach->id.'">'.$coach->first_name.' '.$coach->last_name.'</option>';
       }
       return $html;
     }

     public function get_coaches(Request $request){
       $html = '';
       $allcoaches = User::role('coaches')->orderBy('first_name', 'asc')->get();
       foreach($allcoaches as $coach){
         $html .= '<option value="'.$coach->id.'">'.$coach->first_name.' '.$coach->last_name.'</option>';
       }
       return $html;
     }

     public function get_groups(Request $request){
       $groups = FosGroup::all();
       $html = '';
       foreach($groups as $group){
         $html .= '<option value="'.$group->id.'">'.$group->name.'</option>';
       }

       return $html;
     }

     public function get_clients(Request $request){
       $clients = ClientRecord::where('status', '=', 'active')->get();
       $html = '';
       foreach($clients as $client){
         $html .= '<option value="'.$client->id.'">'.$client->name.'</option>';
       }

       unset($request);

       return $html;
     }

     public function delete(Request $request){

       //return $request;
       $memorecord = '';

       $memorec = MemoRecord::whereId($request->id)->first();

       if($memorec){
         $memorecord = $memorec->memo;
       }


       if($request->from == 'memo-info'){
         $d1 = ApplicantMemo::where('memo_id', '=', $request->id)->delete();
         $d2 = MemoRecord::whereId($request->id)->delete();
         $d3 = EmployeeContract::where('contract_id', '=', $memorecord)->delete();

         if(Auth::check()){
           $userlog = new UserLogController;
           $logged_user_id = Auth::id();
           $action = 'Deleted Memo record ID: '.$request->id;
           $userlog->save($logged_user_id, $action);
         }

         return $d2;
       }elseif($request->from == 'checkin-info'){
         $d1 = ApplicantCheckin::where('checkin_id', '=', $request->id)->delete();
         $d2 = CheckinRecord::whereId($request->id)->delete();

         if(Auth::check()){
           $userlog = new UserLogController;
           $logged_user_id = Auth::id();
           $action = 'Deleted Checkin record ID: '.$request->id;
           $userlog->save($logged_user_id, $action);
         }

         return $d2;
       }elseif($request->from == 'coaching-info'){
         $d1 = ApplicantCoaching::where('coaching_log_id', '=', $request->id)->delete();
         $d2 = CoachingRecord::whereId($request->id)->delete();

         if(Auth::check()){
           $userlog = new UserLogController;
           $logged_user_id = Auth::id();
           $action = 'Deleted Coaching record ID: '.$request->id;
           $userlog->save($logged_user_id, $action);
         }

         unset($request);

         return $d2;
       }elseif($request->from == 'ref-emp-info'){


         $d1 = EmployeeJob::where('job_id', '=', $request->id)->delete();
         $d2 = JobRecord::whereId($request->id)->delete();

         if(Auth::check()){
           $userlog = new UserLogController;
           $logged_user_id = Auth::id();
           $action = 'Deleted Job record ID: '.$request->id;
           $userlog->save($logged_user_id, $action);
         }

         unset($request);

         return $d2;
       }

     }

     public function get_contract($user_id, $filename, $client){
       $contract = new EsignatureController;
       $template = $contract->loadContractTemplate($user_id, $filename, $client);
       unset($contract);
       return $template;
     }

     public function employee(Request $request){
       $applicant = EmployeeRecord::where('applicant_id', '=', $request->id)->first();
       $name = $applicant->info->name;
       $group = $applicant->group[0]['fos_group'][0]['name'];

       $info = array($name, $group);
       $appid = $applicant->id;

       unset($request);
       unset($applicant);

       return $appid;
     }

     public function load_contract(Request $request){

       $filename = '';

       if($request->file != ''){
         $file = $request->file;
         $i = date("m_d_y_h_i_a");
         $filename = 'corrective_sc_'.$i.'_'.$request->user_id.'.'.$file->getClientOriginalExtension();
         $uploaded =  $file->move(public_path('/uploaded/hr_files'), $filename);
       }

       $client = ClientRecord::whereId($request->client)->first();

       $contract_url = $this->get_contract($request->user_id, $filename, $client->name);
       $userid = $request->user_id;

       unset($request);
       unset($uploaded);

       return view('backend.includes.tabs.contracts.contract')->withUrl($contract_url)->withUserId($userid);
     }

     public function success($id, $uid){

      $emcontract = EmployeeContract::where('uid', '=', $uid)->first();
      $emcontract->signed = 'yes';
      $emcontract->save();

       return view('backend.includes.tabs.contracts.back')->withId($id);
     }

     public function contract_lists($id){

       $employee_contracts = EmployeeContract::where('employee_id', '=', $id)->get();

       return datatables()->of($employee_contracts)

       ->editColumn('contract_type', function($employee_contracts){

         $contract_type = [
           'caf' =>  'Corrective Action Form',
           'gva_quarterly' => 'GVA Quarterly',
           'final' => 'Final',
           'hold-suspension' => 'Hold Suspension',
           'isa-quarterly' => 'ISA Quarterly',
           'disclosure-form' => 'Disclosure',
           'fte-form' => 'FTE Form',
           'nte-form' => 'NTE Form',
           'termination-letter' => 'Termination Letter',
           'memorandum-form' =>'Memorandum',
           'assurance-form' => 'Assurance Form',
           'codeofconduct-form' =>'Code of Conduct',
           'ica-form' => 'ICA Form',
           'nca-form' =>'NCA Form'
         ];


         return $employee_contracts->contract_type;
       })

       ->editColumn('status', function($employee_contracts){
         //return array($employee_contracts->recipient_action, $employee_contracts->contract_type);

            if($employee_contracts->recipient_action == 'signed'){
             $html = '<i class="fas fa-file-download"></i><a style="text-transform: uppercase" target="_blank" rel="" href="/sign/'.$employee_contracts->contract_id.'_'.$employee_contracts->uid.'" title="View" class="">'.$employee_contracts->recipient_action.'</a>';
            }elseif($employee_contracts->recipient_action == 'pending'){
              $html = '<i class="far fa-clock"></i><a style="text-transform: uppercase" target="_blank" rel="" href="/admin/esignature/'.$employee_contracts->contract_type.'/'.$employee_contracts->employee_id.'" title="Edit" class="">'.$employee_contracts->recipient_action.'</a>';
            }else{
               $html = '<i class="fas fa-exclamation"></i><a style="text-transform: uppercase" target="_blank" rel="" href="/admin/esignature/'.$employee_contracts->contract_type.'/'.$employee_contracts->employee_id.'" title="Edit" class="">'.$employee_contracts->recipient_action.'</a>';
            }

        return $html;
       })

      ->rawColumns(['status'])

       ->make(true);
     }

     public function esig_final_notice($id, $type){
       $applicant = ApplicantRecord::whereId($id)->first();

       if($type == 'termination'){
         return view('backend.includes.tabs.contracts.termination')->withApplicant($applicant);
       }

       return view('backend.includes.tabs.contracts.final_notice')->withApplicant($applicant);
     }

     public function esig_final_notice2(Request $request){
       //$contract = new EsignatureController;
       //$template = $contract->loadContractTemplateFinalNotice($request);
       //return $template;

       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: Final Notice';
         $userlog->save($logged_user_id, $action);
       }

       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($request->id);
     }

     public function esig_hold(Request $request){
       //$contract = new EsignatureController;
       //$template = $contract->loadContractTemplateHoldSuspension($request);

       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: Hold/Suspension';
         $userlog->save($logged_user_id, $action);
       }

       $recid = $request->id;
       unset($request);

       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($recid);
     }

     public function scorecard($id, $type){
       //$contract = new EsignatureController;
       //$template = $contract->loadContractTemplateGvaScorecard($id, $type);
       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: Scorecard ('.$type.')';
         $userlog->save($logged_user_id, $action);
       }
       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($id);
     }

     public function disclosure_form($id){
       $contract = EmployeeContract::where('employee_id', '=', $id)->where('contract_type', '=', 'disclosure_form')->first();

       if($contract){
          return redirect()->back()->with(['error' => 'Contract has already been sent to the user']);
          exit;
       }

       //$contract = new EsignatureController;
       //$template = $contract->loadContractDisclosure($id);
       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: Disclosure Form';
         $userlog->save($logged_user_id, $action);
       }
       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($id);
     }

     public function memorandum_form($id){
       $contract = EmployeeContract::where('employee_id', '=', $id)->where('contract_type', '=', 'memorandum_form')->first();

       if($contract){
          return redirect()->back()->with(['error' => 'Contract has already been sent to the user']);
          exit;
       }

       //$contract = new EsignatureController;
       //$template = $contract->loadContractMemorandum($id);
       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: Memorandum Form';
         $userlog->save($logged_user_id, $action);
       }
       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($id);
     }

     public function assurance_form($id){
       $contract = EmployeeContract::where('employee_id', '=', $id)->where('contract_type', '=', 'assurance_form')->first();

       if($contract){
          return redirect()->back()->with(['error' => 'Contract has already been sent to the user']);
          exit;
       }

       //$contract = new EsignatureController;
       //$template = $contract->loadAssuranceForm($id);
       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: Assurance Form';
         $userlog->save($logged_user_id, $action);
       }
       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($id);
     }

     public function coc_form($id){
       $contract = EmployeeContract::where('employee_id', '=', $id)->where('contract_type', '=', 'codeofconduct_form')->first();

       if($contract){
          return redirect()->back()->with(['error' => 'Contract has already been sent to the user']);
          exit;
       }

       //$contract = new EsignatureController;
       //$template = $contract->loadCocForm($id);
       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: Code of Conduct Form';
         $userlog->save($logged_user_id, $action);
       }
       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($id);
     }

     public function ica_form($id){
       $contract = EmployeeContract::where('employee_id', '=', $id)->where('contract_type', '=', 'ica_form')->first();

       if($contract){
         return redirect()->back()->with(['error' => 'Contract has already been sent to the user']);
       }

       //$contract = new EsignatureController;
       //$template = $contract->loadIcaForm($id);
       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: Independent Contractor Agreement Form';
         $userlog->save($logged_user_id, $action);
       }
       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($id);
     }

     public function nca_form($id){
       $contract = EmployeeContract::where('employee_id', '=', $id)->where('contract_type', '=', 'nca_form')->first();

       if($contract){
          return redirect()->back()->with(['error' => 'Contract has already been sent to the user']);
          exit;
       }

       //$contract = new EsignatureController;
       //$template = $contract->loadNcaForm($id);
       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: Non-compete Agreement Form';
         $userlog->save($logged_user_id, $action);
       }
       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($id);
     }

     public function fte_form_get(Request $request){
       $applicant = ApplicantRecord::whereId($request->id)->first();
       return view('backend.includes.tabs.contracts.popup_final')->withApplicant($applicant);
     }

     public function fte_form(Request $request){
       //$contract = new EsignatureController;
       //$template = $contract->loadFteForm($request);
       if(Auth::check()){
         $userlog = new UserLogController;
         $logged_user_id = Auth::id();
         $action = 'Created esig contract: FTE Form';
         $userlog->save($logged_user_id, $action);
       }
       return view('backend.includes.tabs.contracts.contract')->withUrl($template)->withUserId($request->id);
     }

     public function termination(Request $request){
       $html = '<p id="prev-date">'.$request->current_date.'</p><p id="prev-to">'.$request->recipient.'</p><p id="prev-body">'.$request->body.'</p>';
       return $html;
     }

     public function terminationSend(Request $request){

       $html = '<p>'.$request->current_date.'</p>';
       $html .= '<p>'.$request->recipient.',</p>';
       $html .= $request->body;
       $to_ = $request->recipient_email;
       $subject = $request->subject;

       Mail::send([], [], function ($message) use($to_, $html, $subject){
         $message->to($to_)
           ->subject('Termination Letter')
           ->from('hr@myvirtudesk.com', 'Human Resource - MyVirtudesk')
           ->replyTo('hr@myvirtudesk.com')
           ->setBody($html, 'text/html');
       });

       if (count(Mail::failures()) > 0 ) {
         return redirect()->back()->with(['error' => 'An error occurred while sending termination email. Please contact Newton']);
         exit;
       }

       $uid = uniqid();

       $db_filename = 'termination_'.$uid.'.pdf';
       PDF::loadHTML($html)->setWarnings(false)->save(public_path('/uploaded/memo/').$db_filename);


       $data = [
         'contract_id' => $uid,
         'employee_id' => $request->id,
         'uid' => $uid,
         'contract_type' => 'termination_letter'
       ];

       $contract = EmployeeContract::create($data);

       $fname = MemoRecord::whereId($request->memoid)->first();
       $fname->memo = $contract->uid;
       $fname->save();

       return redirect()->route('admin.hr-operation-record.edit', [$request->id])->with(['success' => 'Termination letter sent successfully']);
       exit;

     }

     public function download($id){
       $contract = EmployeeContract::where('uid', '=', $id)->first();
       $contract_type = $contract->contract_type;
       $nocontract = array('termination_letter', 'caf');

       if(!in_array($contract_type, $nocontract)){
         $url = LaravelEsignatureWrapper::getContract($contract->contract_id);
         $status = $url['data']['status'];

         if($status == 'signed'){
           $attachment_location = $url['data']['contract_pdf_url'];
           return redirect()->to($attachment_location);
         }else{
           return back()->with('error', 'Document has not been signed yet');
         }

       }else{
         if($contract->contract_type == 'termination_letter'){
           $attachment_location = public_path().'/uploaded/memo/termination_'.$id.'.pdf';
           $fname = "termination_".$id.".pdf";
         }else{
           $attachment_location = public_path().'/form_templates/caf_'.$id.'.pdf';
           $fname = "caf_".$id.".pdf";
         }

         header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
         header("Cache-Control: public"); // needed for internet explorer
         header("Content-Type: application/zip");
         header("Content-Transfer-Encoding: Binary");
         header("Content-Length:".filesize($attachment_location));
         header("Content-Disposition: attachment; filename=".$fname);
         readfile($attachment_location);
         die();

       }
     }

     public function caf($id){
       $applicant = ApplicantRecord::whereId($id)->first();
       $logged_user_id = Auth::user();
       //dd($logged_user_id);
       return view('backend.includes.tabs.contracts.caf')->withApplicant($applicant)->withUser($logged_user_id);
     }

     public function cafSend(Request $request){
       $src = 'src="/';
//dd($request);
      $html = '<style>body{font-family: Arial;}</style>';
      $html .= '<strong>VA Name:</strong> '.$request->recipient.'<br>';
      $html .= '<strong>Client Name:</strong> '.$request->client_name.'<br>';
      $html .= '<strong>Team:</strong> '.$request->team.'<br>';
      $html .= '<strong>Assigned Team Lead:</strong> '.$request->team_lead.'<br>';
      $html .= '<strong>Date Field:</strong> '.$request->date.'<br>';
      $html .= '<strong>Prepared By:</strong> '.$request->prepared_by.'<br><br>';
      $html .= '<hr><i>For Operations Use</i><p></p>';
      $html .= '<h5>I. INCIDENT REPORT DETAILS</h5>';
      $html .= '<strong>Date of Incident:</strong> '.$request->date_incident.'<br>';
      $html .= '<strong>Description of the Offense:</strong> '.$request->offense_desc.'<br>';
      $html .= '<hr><h5>II. EVIDENCES</h5>';
      $html .= '<strong>Evidences: </strong>'.str_replace($src, 'src="'.public_path().'/', $request->evidendes).'<br>';
      $html .= '<hr><h5>III. CODE OF CONDUCT POLICY VIOLATED</h5>';
      $html .= $request->coc;
      $html .= '<hr><i>For Human Resource Use</i><p></p>';
      $html .= '<h5>I. DECISION</h5>';
      $html .= $request->decision;

      $date = date('ymdhis');
      $uid = uniqid();

      $pdf = \PDF::loadHTML($html)->save(public_path().'/form_templates/caf_'.$uid.'.pdf')->stream('download.pdf');

      $data = [
        'contract_id' => $uid,
        'employee_id' => $request->id,
        'uid' => $uid,
        'contract_type' => 'caf'
      ];

      $contract = EmployeeContract::create($data);

      $fname = MemoRecord::whereId($request->memoid)->first();
      $fname->memo = $contract->uid;
      $fname->save();

      return redirect()->route('admin.hr-operation-record.edit', [$request->id])->with(['success' => 'Termination letter sent successfully']);
     }

     public function delChecking(Request $request){
       $chekin = CheckInRecord::whereId($request->id)->delete();
       $vacheckin = EmployeeCheckInRecord::where('checkin_id', '=', $request->id)->delete();
       return $vacheckin;
     }
}
