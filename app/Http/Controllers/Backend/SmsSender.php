<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;

class SmsSender extends Controller
{
  //Send SMS verification code using Twilio
  public function sendSMS(Request $request){
    $sid = config('app.twilio')['account_sid'];
    $token = config('app.twilio')['app_token'];
    $from = config('app.twilio')['from_num'];

    $to_num = preg_replace('/\D/', '', $request->to);

    $to = '+63'.ltrim(preg_replace('/\D/', '', $request->to), '0');

    $client = new Client($sid, $token);

    try {
      $message = $client->messages
      ->create($to, // to
      array(
        "body" => $request->sms_body,
        "from" => $from
      )
    );

    Log::info('SMS s'.$message);

    if (Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Sent sms: '.$request->to;
      $userlog->save($logged_user_id, $action);
    }

  } catch (\Exception $e){
    $message = $e->getMessage();
    Log::info('SMS error'.$message);
    if($e->getCode() == 21211)
    {
      $message = $e->getMessage();
      Log::info('SMS error'.$message);
    }
  }
  }
}
