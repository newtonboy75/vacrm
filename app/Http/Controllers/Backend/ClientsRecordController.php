<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ClientRecord;
use App\ClientPlacementRecord;
use App\ClientSuspensionRecord;
use App\ClientCancellationRecord;
use App\ApplicantRecord;
use App\EmployeeRecord;
use App\ClientReplacementRecord;
use DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\ClientNotes;
use App\EmployeeJob;

class ClientsRecordController extends Controller
{
  public function index(){
    $title = 'Clients';
    $year = date('Y');

    $clients = ClientPlacementRecord::selectRaw('count(status) as count, status')->orderBy('count', 'desc')->groupBy('status')->get();
    $clients_active = ClientPlacementRecord::where('status', '=', '1')->whereYear('current_va_start_date', '=', date('Y'))->select('client_id')->get();

    $pie = [];

    $status = [
      '0' => 'Null',
      '1' => 'Active',
      '2' => 'Inactive',
      '3' => 'Cancelled',
      '4' => 'Placement',
      '5' => 'Suspended',
      '6' => 'Completed',
      '7' => 'Incomplete',
      '8' => 'Probono',
      '9' => 'On Hold'
    ];

    $dyear = [];

    foreach($clients as $client){
      if($client->status){
        $pie[$status[$client->status]] = $client->count;
      }
    }

    $allcl = [];

    foreach($clients_active as $ca){
      $allcl[] = $ca->client_id;
    }

    $cls = ClientRecord::selectRaw('count(id) as count, timezone')->whereIn('id', $allcl)->orderBy('id', 'desc')->groupBy('timezone')->get();

    foreach($cls as $cl){
      $dyear[$cl->timezone] = $cl->count;
    }
    
    unset($cls);
    unset($clients);
    unset($clients_active);
    
    return view('backend.clients', compact('title', 'pie', 'dyear'));
  }

  public function load(){
    $client_placement_records = ClientPlacementRecord::where(
      function($q){
      $q->select('name')
        ->from('clients')
        ->whereColumn('id', '=', 'client_placement_info.client_id')
        ->latest()
        ->limit(1);
        })
        ->where('marketing_specialist', '!=', NULL)
        ->orWhere('current_va_start_date', '!=', NULL)
        ->get();

    return datatables()->of($client_placement_records)

    ->editColumn('client_name', function($client_placement_records){
      $html = $client_placement_records->client_info[0]['name'] ?? '';
      return $html;
    })

    ->editColumn('marketing_specialist', function($client_placement_records){
      $html = $client_placement_records->marketing_specialist;
      return $html;
    })

    ->editColumn('status', function($client_placement_records){
      $html = $client_placement_records->status;
      return $html;
    })


    ->editColumn('start_date_with_va', function($client_placement_records){
      return isset($client_placement_records->start_date_with_va) ?  date('M d, Y', strtotime($client_placement_records->start_date_with_va)) : '';
    })

    ->editColumn('hiring_objective', function($client_placement_records){
      return ucfirst($client_placement_records->hiring_objective);
    })

    ->editColumn('contract_signed', function($client_placement_records){
      return ucfirst($client_placement_records->contract_signed);
    })

    ->editColumn('current_va', function($client_placement_records){
      return $client_placement_records->current_va_old;
    })

    ->editColumn('employment_status', function($client_placement_records){

      $emp_stat = '';

      switch($client_placement_records->employment_status){
        case '1':
        $emp_stat = 'Full Time';
        break;

        case '2':
        $emp_stat = 'Part Time';
        break;

        case '3':
        $emp_stat = 'Timeblock';
        break;

        case '4':
        $emp_stat = '1 Month';
        break;

        case '5':
        $emp_stat = '3 Months';
        break;

        case '1':
        $emp_stat = '6 Months';
        break;
      }

      return $emp_stat;
    })

    ->editColumn('status', function($client_placement_records){

      $status = '';

      switch($client_placement_records->status){
        case '1':
        $status = 'Active';
        break;
        case '2':
        $status = 'Inactive';
        break;
        case '3':
        $status = 'Cancelled';
        break;
        case '4':
        $status = 'Placement';
        break;
        case '5':
        $status = 'Suspended';
        break;
        case '6':
        $status = 'Completed';
        break;
        case '7':
        $status = 'Incomplete';
        break;
        case '8':
        $status = 'Probono';
        break;
        case '9':
        $status = 'On Hold';
        break;
        default:
        $staus = '';
      }
      return $status;
    })

    ->editColumn('contract_tb1', function($client_placement_records){
      return $client_placement_records->contract_tb1 ;
    })

    ->editColumn('current_va_start_date', function($client_placement_records){
      $current_va_start = '';

      if($client_placement_records->current_va_start_date != ''){
        $year = date('Y', strtotime($client_placement_records->current_va_start_date));
        if($year > '1970'){
          $current_va_start = date('M d, Y h:i', strtotime($client_placement_records->current_va_start_date));
        }

      }
      return $current_va_start;
    })

    ->make(true);
  }

  public function list(){
    $clients = ClientRecord::all();
    return datatables()->of($clients)

    ->editColumn('status', function($clients){
      $client_placement_records = ClientPlacementRecord::where('client_id', '=', $clients->id)->orderBy('id', 'desc')->first();
      $status = '';

      if($client_placement_records){
        switch($client_placement_records->status){
          case '1':
          $status = 'Active';
          break;
          case '2':
          $status = 'Inactive';
          break;
          case '3':
          $status = 'Cancelled';
          break;
          case '4':
          $status = 'Placement';
          break;
          case '5':
          $status = 'Suspended';
          break;
          case '6':
          $status = 'Completed';
          break;
          case '7':
          $status = 'Incomplete';
          break;
          case '8':
          $status = 'Probono';
          break;
          default:
          $staus = '';
        }
      }
      
      unset($client_placement_records);
      return $status;
    })

    ->editColumn('register_at', function($clients){
      $date = $clients->register_at == '0000-00-00' ? '' : date('M d, Y',  strtotime($clients->register_at));
      return $date;
    })
    ->make(true);
  }

  public function save_new_client(Request $request){

    $data = [
      'name' => $request->name,
      'timezone' => $request->timezone,
      'state' => $request->state,
      'email' => $request->email,
      'phone_number' => $request->phone,
      'register_at' => date('Y-m-d h:i:s')
    ];

    $client = ClientRecord::updateOrCreate(['id' => $request->id], $data);

    if($client){

      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Created new client #: '.$client->id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
      
      unset($data);
      unset($client);

      return '1';
    }
    //return $client;
  }


  public function destroy_client(Request $request){
    $client = ClientPlacementRecord::destroy($request->id);

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Deleted client #: '.$request->id;
      $userlog->save($logged_user_id, $action);
      unset($userlog);
    }

    return $client;
  }

  public function get_client_lists(){
    $clients = ClientRecord::where('status', '!=', '3')->get();
    return $clients;
  }

  public function get_va_lists(){
    $clients = ApplicantRecord::all();
    return $clients;
  }

  public function client_placement(Request $request){

    $val_id = null;
    $val_name = '';
    $val_email = '';
    $val_phone = '';
    $val_timezone = '';
    $val_state = '';

    if($request->save_action == "edit_client_info"){
      if($request->client_name == ''){
        return 'Client name is required';
      }elseif($request->email == ''){
        return 'Email is required';
      }

      $client = ClientRecord::where('id', '=', $request->client_id)->first();

      $val_id = $request->current_va;
      $val_name = $client->name;
      $val_email = $client->email;
      $val_phone = $client->phone_number;
      $val_state = $client->state;
      $val_timezone = $client->timezone;
    }elseif($request->save_action == "add_new_client_info"){

      if($request->client_id == ''){
        return 'Please choose client';
      }

      $client = ClientRecord::where('id', '=', $request->client_id)->first();
      $val_name = $client->name;
      $val_email = $client->email;
      $val_phone = $client->phone_number;
      $val_id = $request->current_va;
      $val_state = $request->state;
      $val_timezone = $request->timezone;
    }elseif($request->save_action == "add_new_client"){
      $val_name = $request->client_name;
      $val_email = $request->email;
      $val_phone = $request->phone_number;
      $val_state = $request->state;
      $val_timezone = $request->timezone;

      if($val_name == ''){
        return  'Name is required';
      }elseif($val_email == '') {
        return  'Email is required';
      }elseif(!filter_var($val_email, FILTER_VALIDATE_EMAIL)){
        return  'Email is invalid';
      }

    }

    $info = [
      'name' => ucfirst($val_name),
      'email' => $val_email,
      'phone_number' => $val_phone,
      'timezone' => $val_timezone,
      'state' => $val_state,
      'register_at' => date('Y-m-d h:i:s')
    ];

    $client_record = ClientRecord::updateOrCreate(['id' => $request->client_id], $info);

    if($client_record){
      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Created/updated client #: '.$client_record->id;
        $userlog->save($logged_user_id, $action);
      }
    }

    $date_va_start = isset($request->start_date_va) ? date('Y-m-d h:i:s', strtotime($request->start_date_va)) : null;
    $three_mos_contract = isset($request->three_mos_contract) ? date('Y-m-d h:i:s', strtotime($request->three_mos_contract)) : null;
    $current_va_date = isset($request->current_va_date) ? date('Y-m-d h:i:s', strtotime($request->current_va_date)) : null;

    $data = [
      'client_id' => $client_record->id,
      'marketing_specialist' => $request->marketing_specialist,
      'start_date_with_va' => $date_va_start,
      'three_month_contract' => $three_mos_contract,
      'employment_status' => $request->job_term,
      'hiring_objective' => $request->designation,
      'contract_signed' => $request->contract_signed,
      'current_va' => $val_id,
      'status' => $request->status,
      'contract_tb1' => $request->type_contract,
      'current_va_start_date' => $current_va_date
    ];

    if($request->save_action == 'add_client_info' || $request->save_action == 'edit_client_info'){
      if( $request->marketing_specialist == ''){
        return 'Marketing Specialist is required';
      }elseif($request->status == ''){
        return 'Status is required';
      }elseif($request->designation == ''){
        return 'What they hired for is required';
      }elseif($request->job_term == ''){
        return 'Employment Status is required';
      }

      if($request->status == '3' && $request->cancellation_date == ''){
        return 'Cancellation date/reason is required';
      }elseif($request->status == '4' && $request->suspension_date == ''){
        return 'Suspension date/reason is required';
      }
    }

    if($request->save_action == 'add_client_info'){
      $client_placement_record = ClientPlacementRecord::updateOrCreate($data);
      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Added client placement info #: '.$client_placement_record->id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
    }else{
      $client_placement_record = ClientPlacementRecord::updateOrCreate(['id' => $request->id], $data);
      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Updated client placement info #: '.$client_placement_record->id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
    }

    if($request->replacement_va != ''){

      if($request->replacement_va == $client_placement_record->current_va){
        return 'Replacement VA should not be the same as the current va';
        exit;
      }elseif($request->replacement_reason == ''){
        return 'Please add reason for replacement';
        exit;
      }

      $data = [
        'client_info_id' => $client_placement_record->id,
        'replacement_id' => $client_placement_record->current_va,
        'replaced_by' => $request->replacement_va,
        'replacement_reason' => $request->replacement_reason,
        'date' => date('Y-m-d h:i:s')
      ];

      $replacement = ClientReplacementRecord::create($data);
      $client_placement_record->current_va = $request->replacement_va;
      $client_placement_record->save();

      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Replaced VA #: '.$client_placement_record->current_va.' with'.$request->replacement_va;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
    }

    if($request->cancellation_date !=''){
      $cancellation_data = [
        'client_info_id' => $client_placement_record->id,
        'cancellation_date' => date('Y-m-d h:i:s', strtotime($request->cancellation_date)),
        'reason' => $request->cancellation_reason,
        'sub_reason' => $request->sub_reason,
        'form_sent' => $request->sent_form
      ];
      $cancellation = ClientCancellationRecord::updateOrCreate(['client_info_id' => $client_placement_record->id], $cancellation_data);
      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Cancelled client placement info #: '.$client_placement_record->id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
    }

    if($request->suspension_date !=''){
      $suspension_data = [
        'suspension_date' => date('Y-m-d h:i:s', strtotime($request->suspension_date)),
        'suspension_reason' => $request->suspension_reason,
        'form_sent' => $request->sent_form_suspend
      ];
      $cancellation = ClientSuspensionRecord::updateOrCreate(['client_info_id' => $client_placement_record->id], $suspension_data);
      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Suspended client placement info #: '.$client_placement_record->id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
    }
    
    unset($cancellation);
    unset($data);
    unset( $replacement);

    if($client_placement_record){
      return '1';
    }
    return $request;
  }

  public function load_cancellation(){
    $cancellations = ClientCancellationRecord::where(function($q){
      $q->selectRaw('id')
      ->from(with(new ClientPlacementRecord)->getTable())
      ->join('client', 'client.id', '=', 'client_id');})->orderBy('id', 'desc');
    return datatables()->of($cancellations)

    ->addColumn('id', function($cancellations){
      $cancel = $cancellations->info;
      return $cancellations->id;
    })

    ->editColumn('cancellation_date', function($cancellations){
      $date = date('M d, Y', strtotime($cancellations->cancellation_date));
      return $date;
    })

    ->addColumn('client_name', function($cancellations){
      $client_name = $cancellations->info;
      return $client_name['client_info'][0]['name'];
    })

    ->addColumn('va_name', function($cancellations){
      $va_id = $cancellations['info']->current_va ?? '';
      $current_va = EmployeeRecord::where('applicant_id', '=', $va_id)->first();
      $va = $current_va['info']['name'] ?? '';
      return $va;
    })

    ->addColumn('team', function($cancellations){
      $team = '';
      $va_id = $cancellations['info']->current_va ?? '';
      $info = EmployeeJob::where('employee_id', '=', $va_id)->first();

      if($info){
        $team = $info->job_info;

        if($team){
          return $team[0]->myteam->name;
        }
      }
      return $team;
    })

    ->make(true);
  }

  public function load_replacements(){
      
    $replacements = ClientReplacementRecord::where(function($q){
      $q->selectRaw('id')
      ->from(with(new ClientPlacementRecord)->getTable())
      ->join('client', 'client.id', '=', 'client_id');});
      
    return datatables()->of($replacements)

    ->editColumn('client_info_id', function($replacements){
      return $replacements->client_info_id;
    })

    ->editColumn('date', function($replacements){
      return date('M d, Y', strtotime($replacements->date));
    })

    ->addColumn('client_name', function($replacements){
      $replaced = $replacements->placement_info->client_info[0]['name'] ?? '';
      return $replaced;
    })

    ->addColumn('replaced_by', function($replacements){
      return $replacements->replacedby->name;
    })

    ->addColumn('replaced_va', function($replacements){
      //$replaced = $replacements['placement']['curr_va']['name'] ?? '';
      $replaced = '';

      if($replacements){
        if(isset($replacements->replaced->name)){
          $replaced = $replacements->replaced->name;
        }
      }

      return $replaced;
    })

    ->make(true);
  }

  public function load_suspension(){
    $suspensions = ClientSuspensionRecord::where(function($q){
      $q->selectRaw('id')
      ->from(with(new ClientPlacementRecord)->getTable())
      ->join('client', 'client.id', '=', 'client_id');});

    return datatables()->of($suspensions)
    ->editColumn('suspension_date', function($suspensions){
      return date('M d, Y', strtotime($suspensions->suspension_date)) ?? '';
    })

    ->addColumn('client_name', function($suspensions){
      return $suspensions['info']['client_info'][0]['name'];
    })

    ->addColumn('reason', function($suspensions){
      return $suspensions->suspension_reason;
    })

    ->addColumn('team', function($suspensions){
      $team = '';
      $va_id = $suspensions['info']->current_va ?? '';
      $info = EmployeeJob::where('employee_id', '=', $va_id)->first();

      if($info){
        $team = $info->job_info;

        if($team){
          return $team[0]->myteam->name;
        }
      }
      return $team;
    })

    ->addColumn('va_name', function($suspensions){
      $va_id = $suspensions['info']['current_va'];
      $employee = EmployeeRecord::where('applicant_id', '=', $va_id)->first();
      return $employee['info']['name'];
    })

    ->make(true);
  }

  public function load_stats(Request $request){

    $years = DB::select('SELECT YEAR(register_at) as year FROM client GROUP BY year ORDER BY year DESC');

    //Log::info($years);

    return datatables()->of($years)

    ->editColumn('year', function($years){
      return $years->year ?? '< 2017';
    })

    ->addColumn('pst', function($years){

      $info = ClientRecord::where('timezone', '=', 'PST')->whereYear('register_at', '=', $years->year)->get();

      if($years->year == ''){
        $info = ClientRecord::where('timezone', '=', 'PST')->whereNull('register_at')->get();
      }


      return $info->count();
    })


    ->addColumn('est', function($years){
      $info = ClientRecord::where('timezone', '=', 'EST')->whereYear('register_at', '=', $years->year)->get();
      if($years->year == ''){
        $info = ClientRecord::where('timezone', '=', 'EST')->whereNull('register_at')->get();
      }
      return $info->count();
    })

    ->addColumn('cst', function($years){
        $info = ClientRecord::where('timezone', '=', 'CST')->whereYear('register_at', '=', $years->year)->get();
        if($years->year == ''){
          $info = ClientRecord::where('timezone', '=', 'CST')->whereNull('register_at')->get();
        }
      return $info->count();
    })

    ->addColumn('mst', function($years){
        $info = ClientRecord::where('timezone', '=', 'MST')->whereYear('register_at', '=', $years->year)->get();
        if($years->year == ''){
          $info = ClientRecord::where('timezone', '=', 'MST')->whereNull('register_at')->get();
        }
      return count($info);
    })

    ->addColumn('hst', function($years){
        $info = ClientRecord::where('timezone', '=', 'HST')->whereYear('register_at', '=', $years->year)->get();
        if($years->year == ''){
          $info = ClientRecord::where('timezone', '=', 'HST')->whereNull('register_at')->get();
        }
      return $info->count();
    })

    ->addColumn('total', function($years){

      $info = ClientRecord::whereYear('register_at', '=', $years->year)->get();

      if($years->year == ''){
        $info = ClientRecord::whereNull('register_at')->get();
      }

      return $info->count();
    })


    ->make(true);

  }

  public function load_stats_years(){
    $years = DB::select('SELECT year FROM client_placement_info WHERE year <> "NULL" GROUP BY year ORDER BY year DESC');

    return datatables()->of($years)

    ->addColumn('year', function($years){
      if($years->year != '' || $years->year > 0){
        return $years->year;
      }
    })

    ->addColumn('active', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE status = \'1\' AND year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);
    })

    ->addColumn('inactive', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE status = \'2\' AND year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);
    })

    ->addColumn('cancelled', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE status = \'3\' AND year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);

    })

    ->addColumn('suspended', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE status = \'5\' AND year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);

    })

    ->addColumn('completed', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE status = \'6\' AND year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);


    })

    ->addColumn('incomplete', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE status = \'7\' AND year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);


    })

    ->addColumn('placement', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE status = \'4\' AND year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);

    })

    ->addColumn('probono', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE status = \'8\' AND year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);

    })

    ->addColumn('timeblock', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE employment_status = \'3\' AND status = \'1\' AND year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);
    })

    ->addColumn('total', function($years){

      $active = DB::select('SELECT count(client_id) FROM client_placement_info WHERE year = '.$years->year.' GROUP BY client_id ORDER BY year DESC');

      return count($active);
    })

    ->make(true);

  }

  public function remove_client(Request $request){
    $client = ClientRecord::destroy($request->id);

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Removed client #: '.$request->id;
      $userlog->save($logged_user_id, $action);
    }

    return $client;
  }

  public function client_info($id){

    $title = 'Clients';
    $client = ClientRecord::whereId($id)->first();
    $client_info = ClientPlacementRecord::where('client_id', '=', $client->id)->orderBy('id', 'desc')->first();

    $va_lists = $this->get_va_lists();

    return view('backend.includes.clients.client', compact('title', 'client', 'client_info', 'va_lists'));
  }

  public function search(){

    $queries = ApplicantRecord::select('id', 'name')->where('name', 'REGEXP', request()->term)->get();
    //Log::info($va);
    $result = '';

    if($queries){
      foreach ($queries as $query)
      {
        $results[] = ['id' => $query->id, 'value' => ucfirst($query->name)];
      }

      if(isset($results)){
        return response()->json($results);
      }

    }

    return $result;
  }

  public function save_info(Request $request){

    $client_history = [];

    $placement = ClientPlacementRecord::where('id', '=', $request->id)->first();

    $old_va = $placement->current_va;

    $applicant = ApplicantRecord::whereName($request->current_va)->first();

    $placement->marketing_specialist = $request->marketing_specialist;
    $placement->status = $request->status;
    $placement->hiring_objective = $request->hiring_objective;
    $placement->contract_signed = $request->contract_signed;
    $placement->contract_tb1 = $request->contract_tb1;
    $placement->current_va = $applicant->id ?? NULL;
    $placement->current_va_old = $applicant->name ?? '';
    $placement->current_va_start_date = $request->current_va_date ? date('Y-m-d h:i:s', strtotime($request->current_va_date)) : '';
    $placement->employment_status = $request->employment_status;

    if($request->cancellation_reason != ''){
      $placement->cancellation_date = date('Y-m-d h:i:s');
      $placement->cancellation_reason = $request->cancellation_reason;
    }

    if($request->suspension_reason != ''){
      $placement->suspension_date = date('Y-m-d h:i:s');
      $placement->suspension_reason = $request->suspension_reason;
    }

    if (Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Updated Client Placement Record: #'.$request->id;
      $userlog->save($logged_user_id, $action);
      unset($userlog);
    }

    $placement->save();

    if(isset($applicant->id)){
      if($old_va != $applicant->id){
        $data = [
          'client_info_id' => $placement->id,
          'replacement_id' => $old_va,
          'replaced_by' => $applicant->id,
          'replacement_reason' => $request->replacement_reason,
          'date' => date('Y-m-d h:i:s')
        ];

        $replacement = ClientReplacementRecord::create($data);
        unset($data);

        if (Auth::check()){
          $userlog = new UserLogController;
          $logged_user_id = Auth::id();
          $action = 'Replace VA '.$old_va.' with '.$applicant->id;
          $userlog->save($logged_user_id, $action);
          unset($userlog);
        }
      }
    }


    if($request->status == '3'){
      $cancellation_data = [
        'cancellation_date' => date('Y-m-d h:i:s'),
        'reason' => $request->cancellation_reason,
        'sub_reason' => $request->sub_reason,
        'form_sent' => $request->sent_form
      ];
      
      $cancellation = ClientCancellationRecord::updateOrCreate(['client_info_id' => $placement->id], $cancellation_data);
      unset($cancellation_data);
      
      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Cancelled client placement info #: '.$placement->id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
    }

    if($request->status == '5'){
      $suspension_data = [
        'suspension_date' => date('Y-m-d h:i:s', strtotime($request->suspension_date)),
        'suspension_reason' => $request->suspension_reason,
        'form_sent' => $request->sent_form_suspend
      ];
      
      $cancellation = ClientSuspensionRecord::updateOrCreate(['client_info_id' => $placement->id], $suspension_data);
      unset($suspension_data);
      
      if(Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Suspended client placement info #: '.$placement->id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
    }

    if($placement){
      unset($placement);
      return back()->with('success', 'Client info saved successfully');
      exit;
    }
    
    unset($placement);
    return back()->with('error_success', 'An error occured while saving client info. Contract Newton.');
  }

  public function update_client_info(Request $request){

    $client = ClientRecord::whereId($request->client_id)->first();
    $client->name = $request->name;
    $client->email = $request->email;
    $client->phone_number = $request->phone_number;
    $client->state = $request->state;
    $client->timezone = $request->timezone;
    $client->status = $request->status;
    $client->register_at = $request->date_registered ? date('Y-m-d h:i:s', strtotime($request->date_registered)) : date('Y-m-d h:i:s');

    if($request->status == 'inactive'){
      $client->cancel_at = date('Y-m-d h:i:s');
    }

    $client->save();

    if($client){

      if (Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Updated Information for Client: #'.$request->client_id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
      
      unset($client);
      return back()->with('saved', 'Client info updated');
      exit;
    }
    
    unset($client);
    return back()->with('error_save', 'An error occured while saving client info. Contract Newton.');

  }

  public function new_client_info(Request $request){

    $applicant = ApplicantRecord::whereId($request->current_va)->orWhere('name', '=', trim($request->current_va))->first();

    $data = [
      'client_id' => $request->client_id,
      'marketing_specialist' => $request->marketing_specialist,
      'status' => $request->status,
      'hiring_objective' => $request->hiring_objective,
      'contract_signed' => $request->contract_signed,
      'contract_tb1' => $request->contract_tb1,
      'current_va' => $applicant->id ?? '0',
      'current_va_old' => $applicant->name ?? '',
      'current_va_start_date' => date('Y-m-d h:i:s', strtotime($request->current_va_date)),
      'year' => date('Y', strtotime($request->current_va_date)),
      'employment_status' => $request->employment_status
    ];

    $placement = ClientPlacementRecord::create($data);
    unset($data);
    
    if($placement){

      if (Auth::check()){
        $userlog = new UserLogController;
        $logged_user_id = Auth::id();
        $action = 'Created new Placement info for Client: #'.$request->client_id;
        $userlog->save($logged_user_id, $action);
        unset($userlog);
      }
      
      unset($placement);
      return '1';
      exit;
    }

    return 'An error occured while saving client info. Contract Newton.';

  }

  public function single($client_id){
    $client_placement_records = ClientPlacementRecord::where('client_id', '=', $client_id)->where('current_va', '!=', "")->get();

    return datatables()->of($client_placement_records)

    ->editColumn('client_name', function($client_placement_records){
      $html = $client_placement_records->client_info[0]['name'];
      return $html;
    })

    ->editColumn('client_id', function($client_placement_records){
      return $client_placement_records->client_info[0]['id'];
    })

    ->editColumn('marketing_specialist', function($client_placement_records){
      $html = $client_placement_records->marketing_specialist;
      return $html;
    })

    ->editColumn('status', function($client_placement_records){
      $html = $client_placement_records->status;
      return $html;
    })

    ->addColumn('register_at', function($client_placement_records){
      $date = $client_placement_records->client_info[0]['register_at'] == '0000-00-00' ? '' : date('M d, Y', strtotime($client_placement_records->client_info[0]['register_at']));
      return $date;
    })

    ->editColumn('month', function($client_placement_records){
      return $client_placement_records->month;
    })

    ->editColumn('year', function($client_placement_records){
      return $client_placement_records->year;
    })

    ->editColumn('start_date_with_va', function($client_placement_records){
      return isset($client_placement_records->start_date_with_va) ?  date('M d, Y', strtotime($client_placement_records->start_date_with_va)) : '';
    })

    ->editColumn('three_month_contract', function($client_placement_records){
      $three_mos_contract = $client_placement_records->three_month_contract != '' ? date('M d, Y', strtotime($client_placement_records->three_month_contract)) : '';
      return $three_mos_contract;
    })

    ->addColumn('state', function($client_placement_records){
      return $client_placement_records->client_info[0]['state'];
    })

    ->addColumn('timezone', function($client_placement_records){
      return $client_placement_records->client_info[0]['timezone'];
    })

    ->addColumn('phone_number', function($client_placement_records){
      return $client_placement_records->client_info[0]['phone_number'];
    })

    ->addColumn('email', function($client_placement_records){
      return $client_placement_records->client_info[0]['email'];
    })

    ->editColumn('hiring_objective', function($client_placement_records){
      return ucfirst($client_placement_records->hiring_objective);
    })

    ->editColumn('contract_signed', function($client_placement_records){
      return ucfirst($client_placement_records->contract_signed);
    })

    ->editColumn('past_vas', function($client_placement_records){
      return $client_placement_records->past_vas;
    })

    ->editColumn('current_va', function($client_placement_records){
      return $client_placement_records->current_va_old;
    })

    ->editColumn('employment_status', function($client_placement_records){

      $emp_stat = '';

      switch($client_placement_records->employment_status){
        case '1':
        $emp_stat = 'Full Time';
        break;

        case '2':
        $emp_stat = 'Part Time';
        break;

        case '3':
        $emp_stat = 'Timeblock';
        break;

        case '4':
        $emp_stat = '1 Month';
        break;

        case '5':
        $emp_stat = '3 Months';
        break;

        case '1':
        $emp_stat = '6 Months';
        break;
      }

      return $emp_stat;
    })

    ->editColumn('status', function($client_placement_records){

      $status = '';

      switch($client_placement_records->status){
        case '1':
        $status = 'Active';
        break;
        case '2':
        $status = 'Inactive';
        break;
        case '3':
        $status = 'Cancelled';
        break;
        case '4':
        $status = 'Placement';
        break;
        case '5':
        $status = 'Suspended';
        break;
        case '6':
        $status = 'Completed';
        break;
        case '7':
        $status = 'Incomplete';
        break;
        case '8':
        $status = 'Probono';
        break;
        default:
        $staus = '';
      }
      return $status;
    })

    ->editColumn('contract_tb1', function($client_placement_records){
      return $client_placement_records->contract_tb1 ;
    })

    ->editColumn('current_va_start_date', function($client_placement_records){
      $current_va_start = '';
      if($client_placement_records->current_va_start_date != ''){
        $current_va_start = date('M d, Y', strtotime($client_placement_records->current_va_start_date));
      }
      return $current_va_start;
    })

    ->addColumn('cancellation_date', function($client_placement_records){
      $date_cancellation = ClientCancellationRecord::where('client_info_id', '=', $client_placement_records->id)->orderBy('id', 'desc')->first();
      return isset($date_cancellation) ? date('M d, Y', strtotime($date_cancellation->cancellation_date)) : '';
    })

    ->addColumn('reason', function($client_placement_records){
      $cancellations = ClientCancellationRecord::where('client_info_id', '=', $client_placement_records->id)->orderBy('id', 'desc')->first();
      return $cancellations->cancellation_reason ?? '';
    })

    ->addColumn('suspension_date', function($client_placement_records){
      //$date_suspension = $client_placement_records->suspensions['suspension_date'] != '' ? date('M d, Y', strtotime($client_placement_records->suspensions['suspension_date'])) : '';
      $date_suspension = ClientSuspensionRecord::where('client_info_id', '=', $client_placement_records->id)->orderBy('id', 'desc')->first();
      return isset($date_suspension) ? date('M d, Y', strtotime($date_suspension->suspension_date)) : '';
    })

    ->addColumn('suspension_reason', function($client_placement_records){
      $suspensions = ClientSuspensionRecord::where('client_info_id', '=', $client_placement_records->id)->orderBy('id', 'desc')->first();
      return $suspensions->suspension_reason ?? '';
    })


    ->make(true);
  }

  public function notes(Request $request){
    $action = request()->action;

    if($action == 'save'){
      return $this->noteSave($request);
    }
  }

  public function noteSave($request){

    if(Auth::check()){
      $userlog = new UserLogController;
      $logged_user_id = Auth::id();
      $action = 'Created note '.$request->note_type;
      $userlog->save($logged_user_id, $action);
    }

    //return $request;
    $note_data = [
      'client_id' => $request->client_id,
      'note_type' => $request->note_type,
      'note' => $request->note_body,
      'interaction_type' => $request->interaction_type,
      'status' => $request->note_status,
      'noted_by' => $logged_user_id,
      'others_status' => $request->others_status,
      'others_type' => $request->others_type,
      'others_interaction' => $request->others_interaction
    ];

    $notes = ClientNotes::updateOrCreate(['id' => $request->note_id], $note_data);
    unset($note_data);

    if($notes){
      return $notes->id;
    }

    return '0';
  }

  public function uploader(Request $request){

      move_uploaded_file($_FILES["upload"]["tmp_name"],
      public_path().'/temp_uploads/'.$_FILES["upload"]["name"]);
      return response()->json([ 'fileName' => $_FILES["upload"]["name"], 'uploaded' => true, 'url' => '/temp_uploads/'.$_FILES["upload"]["name"], ]);

  }

  public function note($id){
    if($id == 'all'){
      $notes = ClientNotes::all();
    }else{
      $notes = ClientNotes::where('client_id', '=', $id)->get();
    }

    return datatables()->of($notes)

    ->editColumn('date', function($notes){
      $date = date('F d, Y h:i A', strtotime($notes->created_at));
      return $date;
    })

    ->editColumn('note', function($notes){
      $content = preg_replace("/<img[^>]+\>/i", "", $notes->note);
      $content = strip_tags($content);
      return [$notes->id, strlen($content) > 50 ? substr($content,0,50)."..." : $content];
    })

    ->editColumn('note_type', function($notes){
      $html = $notes->note_type;
      if($notes->note_type == 'others'){
        $html = $notes->others_type;
      }

      return strtolower($html);
    })

    ->editColumn('interaction_type', function($notes){
      $html = $notes->interaction_type;
      if($notes->interaction_type == 'others'){
        $html = $notes->others_interaction;
      }

      return strtolower($html);
    })

    ->editColumn('status', function($notes){
      $html = $notes->status;
      if($notes->status == 'others'){
        $html = $notes->others_status;
      }

      return strtolower($html);
    })

    ->editColumn('client', function($notes){
      $client = ClientRecord::whereId($notes->client_id)->first();
      if($client){
        return [$client->id, $client->name];
      }
      return null;
    })

    ->editColumn('noted_by', function($notes){
      $noted_by = Auth::user()::whereId($notes->noted_by)->first();

      $fname = '';
      $lname = '';

      if($noted_by){
        $fname = $noted_by->first_name ?? '';
        $lname = $noted_by->last_name ?? '';
      }

      return $fname.' '.$lname;

    })

    ->make(true);
  }

  public function getNote(Request $request){
    $note = ClientNotes::where('id', '=', $request->id)->first();
    return $note;
  }

  public function destroyNote(Request $request){
    $note = ClientNotes::find($request->id)->delete();
    return 1;
  }

  //end controller
}
