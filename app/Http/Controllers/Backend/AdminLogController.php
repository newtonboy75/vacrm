<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserLog;
use Illuminate\Support\Facades\Log;

class AdminLogController extends Controller
{
    public function index(){
      $title = 'Admin log';
      return view('backend.admin-log', compact('title'));
    }

    public function load(){

    //date_default_timezone_set('Asia/Manila');
      //Log::info(request()->search['value']);
      $date_selected = date('Y-m-d');

      if(request()->search['value'] != ''){

        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", request()->search['value'])){
          $date_selected = request()->search['value'];
          $admin_logs = UserLog::whereDate('date_created', '=', $date_selected)->orderBy('date_created', 'desc');
        }else{
          $admin_logs = UserLog::orderBy('id', 'desc');
        }

      }else{
        $admin_logs = UserLog::whereDate('date_created', '>=', date('Y-m-d'))->orderBy('date_created', 'desc');
      }


      //Log::info($admin_logs);

      return datatables()->of($admin_logs)

      ->editColumn('date_created', function($admin_logs){
        $ltime = $admin_logs->date_created.' +8hours ';
        $date = $admin_logs->date_created != '' ? date('M d, Y h:i:s A', strtotime($ltime)) : '';
        return  $date;
      })

      ->addColumn('admin', function($admin_logs){
        $fname = $admin_logs->user[0]['first_name'] ?? '';
        $lname = $admin_logs->user[0]['last_name'] ?? '';
        return $fname.' '.$lname;
      })

      ->make(true);
    }
}
