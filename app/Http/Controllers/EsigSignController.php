<?php

namespace App\Http\Controllers;
use App\ApplicantRecord;
use App\Models\Auth\User;
use App\ApplicantFirstInterview;
use App\Http\Controllers\Backend\UserLogController;
use Illuminate\Support\Facades\Auth;
use App\EmployeeContract;
use App\EmployeeContractActions;
use Illuminate\Support\Facades\Mail;
use PDF;
use App\Jobs\SendEmailEmployerContractsQueues;
use App\Jobs\SendEmailEmployeeContractsQueues;
use App\Jobs\CreatePdfQueues;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
/**
 * Class LanguageController.
 */
class EsigSignController extends Controller
{
    /**
     * @param $locale
     *
     * @return \Illuminate\Http\RedirectResponse
     */
     //new esign
     public function new_sign($id, $action){
         $pattern = "/[-_]/";
         $id_exp = preg_split($pattern, $id);
         $s3path  = Storage::disk('s3')->exists('/pdf/'.$id_exp[1].'.pdf');
         
         
         if($s3path){
               $path = 'pdf';
              $filename = $id_exp[1].'.pdf';
              
              return redirect(Storage::disk('s3')->temporaryUrl(
                $path.'/'.$filename,
                now()->addHour(),
                ['ResponseContentDisposition' => 'inline']
             ));
         }

        //perform guzzle
       $url = 'https://services.myvirtudesk.com/api/signing';
    
        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post($url, [
          'form_params' => [
            'email' => 'newtonboy@gmail.com',
            'password' => 'renton75',
            'id' => $id,
            'myaction' => $action
          ]
        ]);
        
        $r = json_decode($result->getBody()->getContents());

       if($r->page == 'contract_template'){

           $contract = $r->contract;
           $doc = $r->doc;
           unset($r);
           return view('backend.includes.pages.contract_template', compact('contract', 'doc'));

       }else{
           //return 'here';
           $recipient_action = $r->recipient_action;
           $contract = $r->contract;
           $contractType = $r->contractType;
           unset($r);
           return view('backend.includes.pages.contract_template_action', compact('recipient_action', 'contract', 'contractType'));
           
       }
        
    }
    public function sign($id, $action = null)
    {
      $user_ip = $_SERVER['REMOTE_ADDR'];
       
      if(isset($action)){

        $recipient_action = '';

        $contract_id = explode('-', $id);
        $contract = EmployeeContract::where('id','=', $contract_id[0])->where('uid', '=', $contract_id[1])->first();

        $date_signed = $action == 'signed' ? date('Y-m-d h:i:s') : null;

        if($contract){

            if($action == 'signed'){
              $contract->signed = 'yes';
              $contract->recipient_action = 'signed';
              $contract->recipient_signed_date = $date_signed;
              $contract->save();
              $recipient_action = 'signed';
            }elseif($action == 'declined'){
              $contract->signed = 'no';
              $contract->recipient_action = 'declined';
              $contract->save();
              $recipient_action = 'declined';
            }

            if($recipient_action != 'none'){
                $action = EmployeeContractActions::create(['action' => $recipient_action, 'contract_id' => $contract->contract_id, 'ip_address' => $user_ip]);
            }

            $contract_type = [
                'caf' =>  'Corrective Action Form',
                'gva_quarterly' => 'GVA Quarterly',
                'final' => 'Final',
                'hold-suspension' => 'Hold Suspension',
                'isa-quarterly' => 'ISA Quarterly',
                'disclosure-form' => 'Disclosure',
                'fte-form' => 'FTE Form',
                'nte-form' => 'NTE Form',
                'termination-letter' => 'Termination Letter',
                'memorandum-form' =>'Memorandum',
                'assurance-form' => 'Assurance Form',
                'codeofconduct-form' =>'Code of Conduct',
                'ica-form' => 'ICA Form',
                'nca-form' =>'NCA Form',
                'ica-eva' =>'ICA EVA',
                'ica-isa' => 'ICA ISA',
                'ica-gva' => 'ICA GVA'
            ];

            $contractType = $contract_type[$contract->contract_type];
          
            //$this->send_employee_contract_confirm($contract);
            
            if($contract->recipient_action == 'signed'){
                CreatePdfQueues::dispatchNow($contract);
                $this->send_employee_contract_confirm($contract);
            }
            
            return view('backend.includes.pages.contract_template_action', compact('recipient_action', 'contract', 'contractType'));
        }
        
            return view('backend.includes.pages.contract_template_action', compact('recipient_action', 'contract', 'contractType'));

      }else{

        $contract_id = explode('_', $id);
        $contract = EmployeeContract::where('contract_id', '=', $contract_id[0])->where('uid', '=', $contract_id[1])->with('actions')->first();

        if($contract->recipient_action == 'signed' || $contract->recipient_action == 'declined'){
          $recipient_action = 'none';
          $doc = '';
          if($contract){
     
            $file = 'https://virtudeskcrm.s3-us-west-2.amazonaws.com/pdf/'.$contract->uid.'.pdf';
            if(file_exists($file)){
              $doc = 'https://virtudeskcrm.s3-us-west-2.amazonaws.com/pdf/'.$contract->uid.'.pdf';
            }else{
                $file = public_path().'/form_templates/'.$contract->uid.'.pdf';
            }
          }
          
          $path = 'pdf';
          $filename = $contract->uid.'.pdf';
          
          return redirect(Storage::disk('s3')->temporaryUrl(
            $path.'/'.$filename,
            now()->addHour(),
            ['ResponseContentDisposition' => 'inline']
         ));
         
         unset($file);
      
        }

        if($contract){

            $action = EmployeeContractActions::create(['action' => 'viewed', 'contract_id' => $contract_id[0], 'ip_address' => $user_ip]);
            $doc = \File::get(public_path('form_templates/'.$contract->uid));
        }

        return view('backend.includes.pages.contract_template', compact('contract', 'doc'));

      }
    }

    public function send_admin_contract_confirm(EmployeeContract $contract){
        
      $employee = null;
      $contract_type = [];

      $employee = ApplicantRecord::whereId($contract->employee_id)->first();

      $hr_email = 'hr@myvirtudesk.com';
      $requirements_email = 'requirements@myvirtudesk.com';
      $hr_subject = 'Human Resource | Virtudesk';
      $requirements_subject = 'Pre-training Requirements | Virtudesk Requirements Team';

      $contract_type = [
        'caf' =>  array('Corrective Action Form', $hr_email, $hr_subject, 'HR Team'),
        'gva_quarterly' => array('GVA Quarterly', $hr_email, $hr_subject, 'HR Team'),
        'final' => array('Final', $hr_email, $hr_subject, 'HR Team'),
        'hold-suspension' => array('Hold Suspension', $hr_email, $hr_subject, 'HR Team'),
        'isa-quarterly' => array('ISA Quarterly', $hr_email, $hr_subject, 'HR Team'),
        'disclosure-form' => array('Disclosure', $hr_email, $hr_subject, 'HR Team'),
        'fte-form' => array('FTE Form', $hr_email, $hr_subject, 'HR Team'),
        'nte-form' => array('NTE Form', $hr_email, $hr_subject, 'HR Team'),
        'termination-letter' => array('Termination Letter', $hr_email, $hr_subject, 'HR Team'),
        'memorandum-form' => array('Memorandum', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'assurance-form' => array('Assurance Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'codeofconduct-form' => array('Code of Conduct', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-form' => array('ICA Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'nca-form' => array('NCA Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-eva' => array('ICA EVA', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-isa' => array('ICA ISA', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-gva' => array('ICA GVA', $requirements_email, $requirements_subject, 'Recruitment Team'),
      ];
      
      $employee_name = ucwords($employee->name);
      $contract_id = $contract->contract_id;
      $contract_types = $contract_type[$contract->contract_type][0];
      $who = $contract_type[$contract->contract_type][3];
      $to = $contract_type[$contract->contract_type][1];
      
      SendEmailEmployeeContractsQueues::dispatchNow($employee_name, $contract_id, $contract_types, $who, $to);
      unset($contract);
      unset($employee);
      unset($contract_type);
      exit;
    }
    
    //old implementation
    public function createPdfContract(EmployeeContract $contract){
      $employee = null;
      $contract_type = [];

      $employee = ApplicantRecord::whereId($contract->employee_id)->first();
      $audit = $contract->actions;

      $hr_email = 'hr@myvirtudesk.com';
      $requirements_email = 'requirements@myvirtudesk.com';
      $hr_subject = 'Human Resource | Virtudesk';
      $requirements_subject = 'Pre-training Requirements | Virtudesk Requirements Team';

      $contract_type = [
        'caf' =>  array('Corrected Action Form', $hr_email, $hr_subject, 'HR Team'),
        'gva_quarterly' => array('GVA Quarterly', $hr_email, $hr_subject, 'HR Team'),
        'final' => array('Final', $hr_email, $hr_subject, 'HR Team'),
        'hold-suspension' => array('Hold Suspension', $hr_email, $hr_subject, 'HR Team'),
        'isa-quarterly' => array('ISA Quarterly', $hr_email, $hr_subject, 'HR Team'),
        'disclosure-form' => array('Disclosure', $hr_email, $hr_subject, 'HR Team'),
        'fte-form' => array('FTE Form', $hr_email, $hr_subject, 'HR Team'),
        'nte-form' => array('NTE Form', $hr_email, $hr_subject, 'HR Team'),
        'termination-letter' => array('Termination Letter', $hr_email, $hr_subject, 'HR Team'),
        'memorandum-form' => array('Memorandum', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'assurance-form' => array('Assurance Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'codeofconduct-form' => array('Code of Conduct', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-form' => array('ICA Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'nca-form' => array('NCA Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-eva' => array('ICA EVA', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-isa' => array('ICA ISA', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-gva' => array('ICA GVA', $requirements_email, $requirements_subject, 'Recruitment Team'),
      ];

      $doc = '<html><head><title>Contract</title>';
      $doc .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
      $doc .= '<link href="https://fonts.googleapis.com/css?family=Satisfy&display=swap" rel="stylesheet"><style>.page-break { page-break-after: always; } body{font-family: sans-serif; arial; font-size: 13px; color: #333;} #sig{ font-family: \'Satisfy\', cursive !important; font-size: 22px;}</style></head><body>';
      $doc .= '<div style="float: right;"><small>'.date('F d, Y').'</small></div><br>';
      $doc .= '<div style="width: 100%; text-align:center;"><img style="width: 18%;" src="https://virtudeskpro.com/img/backend/brand/virtudesk_logo_pdf.png"></div><p>&nbsp;</p>';

      if($contract){
        $file = public_path().'/form_templates/'.$contract->uid;
        if(file_exists($file)){
          if ($fh = fopen($file, 'r')) {
            while (!feof($fh)) {
              $doc .= fgets($fh);
            }
            fclose($fh);
          }
        }
      }

      $doc .= '<div class="page-break"></div>';
      $doc .= '<div style="width: 100%; text-align:right;"><img style="width: 6%;" src="https://virtudeskpro.com/img/backend/brand/virtudesk_logo_pdf.png"></div><p><hr style="background: gray;"></p>';
      $doc .= '<div style="font-size: 12px;"><br><strong>Electronic Signatures</strong><br><br>';
      $doc .= '<div id="sig">'.$contract_type[$contract->contract_type][3].'<br><span style="font-size: 10px; color: gray; font-family: sans-serif !important;">(digital representation of the signature)</span></div>';
      $doc .= '<br><div><strong>'.$contract_type[$contract->contract_type][3].'</strong></div>';
      $doc .= '<div style="font-size: 10px; color: gray">'.date('F d, Y h:i', strtotime($contract->sent_date)).'</div>';
      $doc .= '<div style="font-size: 10px; color: gray"><span style="font-weight: bold;">Email: </span>'.$contract_type[$contract->contract_type][1].'</div>';
      $doc .= '<br><br><div id="sig">'.ucwords($employee->name).'<br><span style="font-size: 10px; color: gray; font-family: sans-serif !important;">(digital representation of the signature)</span></div>';
      $doc .= '<br><div><strong>'.ucwords($employee->name).'</strong></div>';
      $doc .= '<div style="font-size: 10px; color: gray">'.date('F d, Y h:i', strtotime($contract->recipient_signed_date)).'</div>';
      $doc .= '<div style="font-size: 10px; color: gray"><span style="font-weight: bold;">Email: </span>'.$employee->email.'</div></div>';

      $doc .='<p>&nbsp;</p>';
      $doc .= '<div style="font-size: 12px;"><br><strong>Audit Trail</strong><br><br>';

      foreach($contract->actions as $action){

        $doc .= '<div style="font-size: 10px; font-weight: bold">'.date('F d, Y h:i a', strtotime($action->date_action)).'</div>';
        if($action->action == 'signed'){
          $doc .= '<div style="font-size: 10px;">'.ucfirst($action->action).' by '.ucwords($employee->name).'<br>Contract ID: '.$contract->contract_id.'</div><br>';
        }else{
          $doc .= '<div style="font-size: 10px;">'.ucfirst($action->action).' by '.ucwords($employee->name).'</div><br>';
        }

      }

      $doc .= '<div style="font-size: 10px; font-weight: bold">'.date('F d, Y h:i a', strtotime($contract->sent_date)).'</div>';
      $doc .= '<div style="font-size: 10px;">Contract sent by '.$contract_type[$contract->contract_type][3].'</div>';
      $doc .= '</body></html>';

      \PDF::loadHTML($doc)->save(public_path().'/form_templates/'.$contract->uid.'.pdf')->stream('download.pdf');
      
      unset($doc);
      unset($file);
      unset($contract);

    }

    public function send_employee_contract_confirm(EmployeeContract $contract){
      $employee = ApplicantRecord::whereId($contract->employee_id)->first();

      $hr_email = 'hr@myvirtudesk.com';
      $requirements_email = 'requirements@myvirtudesk.com';
      $hr_subject = 'Human Resource | Virtudesk';
      $requirements_subject = 'Pre-training Requirements | Virtudesk Requirements Team';

      $contract_type = [
        'caf' =>  array('GVA', $hr_email, $hr_subject, 'HR Team'),
        'gva_quarterly' => array('GVA Quarterly', $hr_email, $hr_subject, 'HR Team'),
        'final' => array('Final', $hr_email, $hr_subject, 'HR Team'),
        'hold-suspension' => array('Hold Suspension', $hr_email, $hr_subject, 'HR Team'),
        'isa-quarterly' => array('ISA Quarterly', $hr_email, $hr_subject, 'HR Team'),
        'disclosure-form' => array('Disclosure', $hr_email, $hr_subject, 'HR Team'),
        'fte-form' => array('FTE Form', $hr_email, $hr_subject, 'HR Team'),
        'nte-form' => array('NTE Form', $hr_email, $hr_subject, 'HR Team'),
        'termination-letter' => array('Termination Letter', $hr_email, $hr_subject, 'HR Team'),
        'memorandum-form' => array('Memorandum', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'assurance-form' => array('Assurance Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'codeofconduct-form' => array('Code of Conduct', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-form' => array('ICA Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'nca-form' => array('NCA Form', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-eva' => array('ICA EVA', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-isa' => array('ICA ISA', $requirements_email, $requirements_subject, 'Recruitment Team'),
        'ica-gva' => array('ICA GVA', $requirements_email, $requirements_subject, 'Recruitment Team'),
      ];
      
      $pathToFile = 'https://virtudeskcrm.s3-us-west-2.amazonaws.com/pdf/'.$contract->uid.'.pdf';
      if(file_exists(public_path().'/form_templates/'.$contract->uid.'.pdf')){
          $pathToFile = public_path().'/form_templates/'.$contract->uid.'.pdf';
      }
      
      $landing_url = route('sign', ['id'=>$contract->contract_id.'_'.$contract->uid]);

      $display = $contract_type[$contract->contract_type][0].' File';

     // $micar_contracts = ['ICA Form', 'Memorandum', 'Disclosure', 'Assurance Form', 'Code of Conduct', 'ICA EVA', 'ICA ISA'];


      //if(in_array($contract_type[$contract->contract_type][0], $micar_contracts)){
      $email_use = $employee->email;
      //}else{
        //$email_use = $employee->vd_email;
      //}
      
      $employee_name = ucwords($employee->name);
      $contract_id = $contract->contract_id;
      $contract_types = $contract_type[$contract->contract_type][0];
      $from = $contract_type[$contract->contract_type][1];
      
      //dispatch(new SendEmailEmployerContractsQueues($landing_url, $employee_name, $contract_types, $email_use, $contract_id, $pathToFile, $display, $from));
      
      SendEmailEmployerContractsQueues::dispatchNow($landing_url, $employee_name, $contract_types, $email_use, $contract_id, $pathToFile, $display, $from);
      //SendEmailEmployeeContractsQueues::dispatchNow($employee_name, $contract_id, $contract_types, $employee_name, $to, $pathToFile);
      
      unset($contract);
      unset($pathToFile);
      unset($employee);
      //gc_collect_cycles();
    }

    //end here
}
