<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingRecord extends Model
{
    protected $table = 'training';
    protected $fillable = ['trainer_id',	'program',	'schedule',	'start_date',	'end_date',	'training_status',	'batch',	'strengths',	'improvement',	'applicant_id', 'notes', 'va_experience', 'last_work_experience', 'work_experience_notes'];

    public $timestamps = false;

    public function trainer(){
      return  $this->hasOne('App\Models\Auth\User', 'id', 'trainer_id');
    }

    public function trainee(){
      return  $this->hasOne('App\ApplicantRecord', 'id', 'applicant_id');
    }

    public function training_status(){

      $status = $this->training_status;
      $context = '';
      switch ($status){
        case '1':
        $context = 'Present';
        break;
        case '2':
        $context = 'Absent w/ Notif';
        break;
        case '3':
        $context = 'NCNS';
        break;
        case '4':
        $context = 'Transferred to next batch - Family Emergency';
        break;
        case '5':
        $context = 'Transferred to next batch - System Requirements';
        break;
        case '6':
        $context = 'Transferred to next batch - Force Majeure';
        break;
        case '7':
        $context = 'Transferred to next batch - Medical';
        break;
        case '8':
        $context = 'Transferred to next batch - Did not see email';
        break;
        case '9':
        $context = 'Transferred to next batch - Personal';
        break;
        case '10':
        $context = 'Transferred to next batch - Employed (Rendering Resignation)';
        break;
        case '11':
        $context = 'Terminated - Attitude/Behavior';
        break;
        case '12':
        $context = 'Terminated - Attendance';
        break;
        case '13':
        $context = 'Terminated - Employment Status';
        break;
        case '14':
        $context = 'Terminated - Performance';
        break;
        case '15':
        $context = 'Resigned - Personal';
        break;
        case '16':
        $context = 'Resigned - Business Structure';
        break;
        case '17':
        $context = 'Resigned - Health Issues';
        break;
        case '18':
        $context = 'Resigned - Family Emergency';
        break;
        case '19':
        $context = 'Resigned - system Requirements';
        break;
        case '20':
        $context = 'Late';
        break;
        case '21':
        $context = 'Transferred to ISA';
        break;
        case '22':
        $context = 'Transferred to GVA';
        break;
        case '23':
        $context = 'Re-Application - System Requirements';
        break;
        case '24':
        $context = 'Re-Application - Health Issues';
        break;
        case '25':
        $context = 'Re-Application - Currently Employed';
        break;
        case '26':
        $context = 'Re-Application - Family Matters';
        break;
        case '27':
        $context = 'Passed ISA Training';
        break;
        case '28':
        $context = 'Passed GVA Training';
        break;
        case '29':
        $context = 'Failed ISA Training';
        break;
        case '30':
        $context = 'Failed GVA Training';
        break;
        case '31':
        $context = 'Blacklisted';
        break;
      }

      return $context;
    }
}
