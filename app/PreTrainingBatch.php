<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PreTrainingBatch extends Model
{
    protected $table = 'pre_training_batch_emails';
    protected $fillable = ['batch_number', 'email_sent', 'start_date', 'end_date'];
    public $timestamps = false;

}
