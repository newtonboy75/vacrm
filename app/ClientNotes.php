<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClientNotes extends Model
{
    protected $table = 'client_notes';
    protected $fillable = ['client_id', 'note_type', 'note', 'interaction_type', 'status', 'noted_by', 'others_type', 'others_status', 'others_interaction'];
    public $timestamps = false;

}
