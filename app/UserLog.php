<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserLog extends Model
{
    protected $table = 'user_log';
    protected $fillable = ['user_id', 'activity', 'date_created'];
    public $timestamps = false;

    public function user(){
      return  $this->hasMany('App\Models\Auth\User', 'id', 'user_id');
    }

}
