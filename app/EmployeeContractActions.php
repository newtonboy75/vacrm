<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmployeeContractActions extends Model
{
    protected $table = 'contract_action';
    protected $fillable = ['contract_id', 'action', 'ip_address'];
    public $timestamps = false;

}
