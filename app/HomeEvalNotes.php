<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class HomeEvalNotes extends Model
{
    protected $table = 'home_eval_note';
    protected $fillable = ['home_eval_id', 'note'];
    public $timestamps = false;



}
