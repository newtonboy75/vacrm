<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantExperience extends Model
{
    protected $table = 'applicants_experiences';
    protected $fillable = ['applicant_id', 'experience_id'];
    public $timestamps = false;


    public function experience_record(){
      return  $this->hasMany('App\ApplicantExperienceRecord', 'id', 'experience_id');
    }

}
