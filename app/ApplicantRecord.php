<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicantRecord extends Model
{
  use SoftDeletes;

  protected $table = 'applicant';
  protected $fillable = ['sourcer_id', 'recruiter_id', 'name', 'email', 'mobile_number', 'address', 'skype_id', 'position', 'come_from', 'red_flag', 'notes', 'red_flag_notes', 'status', 'non_compete_agreement_id',	'vd_academy',	'coe',	'vd_skype',	'vd_skype_pass',	'vd_email',	'vd_email_pass',	'vd_academy_pass',	'va_level',	'landing_number',	'archived',	'applicants_hash',	'workflow_status',	'created_at',	'updated_at',	'position_applying_for',	'pre_training_requirements_status',	'placements_status', 'temp_pass', 'has_real_estate_exp', 'pou'];

  public function total(){
    return $this->get()->count();
  }

  public function position_context(){
    $position_applied = $this->position_applying_for;

    $position = '';

    switch ($position_applied){
      case 'eva':
      $position = 'Executive Virtual Assistant (EVA)';
      break;
      case 'isa':
      $position = 'Inside Sales Associate (ISA)';
      break;
      case 'gva':
      $position = 'General Virtual Assistant (GVA)';
      break;
    }

    return $position;
  }

  public function status_context(){
    $status = $this->workflow_status;

    $context = '';
    switch ($status){
      case 'sourcing_record':
      $context = 'Sourcing Record';
      break;
      case 'pre_training':
      $context = 'Pre Training Requirements';
      break;
      case 'reference_check':
      $context = 'Reference Check';
      break;
      case 'home_eval':
      $context = 'Home Evaluation Record';
      break;
      case 'talent_acquisition':
      $context = 'Talent Acquistion Record';
      break;
      case 'training_record':
      $context = 'Training Record';
      break;
      case 'hr_record':
      $context = 'HR-Operation Record';
      break;
    }
    return $context;
  }

  public function application_status(){

    $status = $this->status;

    switch ($status){
      case '1':
      $context = 'Submitted Application Form';
      break;
      case '2':
      $context = 'Passed Paper Screening';
      break;
      case '3':
      $context = 'Failed Paper Screening';
      break;
      case '4':
      $context = 'Scheduled';
      break;
      case '5':
      $context = 'Waiting for Client\'s Decision';
      break;
      case '6':
      $context = 'Hired';
      break;
      case '7':
      $context = 'Available';
      break;
      case '8':
      $context = 'On Hold';
      break;
      case '9':
      $context = 'Declined';
      break;
      default:
      $context = 'Submitted Application Form';
    }

    return $context;
  }

  public function va_level(){
    $va_level = $this->va_level;
    $context = '';
    switch ($va_level){
      case '1':
      $context = 'ISA';
      break;
      case '2':
      $context = 'GVA';
      break;
      case '3':
      $context = 'EVA';
      break;
    }
    return $context;
  }

  public function placements_status(){
    $placements_status = $this->placements_status;
    $context = '';
    switch ($placements_status){
      case '5':
      $context = 'Scheduled';
      break;
      case '6':
      $context = 'Waiting for Client\'s Decision';
      break;
      case '7':
      $context = 'Hired';
      break;
      case '8':
      $context = 'Available';
      break;
      case '9':
      $context = 'On Hold';
      break;
      case '10':
      $context = 'Declined';
      break;
    }
    return $context;
  }

  public function sourcer_name(){
    return  $this->hasMany('App\Models\Auth\User', 'id', 'sourcer_id');
  }

  public function recruiter_name(){
    return  $this->hasOne('App\Models\Auth\User', 'id', 'recruiter_id')->orderBy('id', 'desc');
  }

  public function initial_interview(){
    return  $this->hasMany('App\ApplicantFirstInterview', 'applicant_id', 'id')->orderBy('id', 'desc')->with('interview_record');
  }

  public function final_interview(){
    return  $this->hasMany('App\ApplicantFinalInterview', 'applicant_id', 'id')->orderBy('id', 'desc')->with('interview_record');
  }

  public function home_evaluation_record(){
    return  $this->hasMany('App\HomeEvaluationRecord', 'id', 'home_evaluation')->with('system');
  }

  public function system_check_record(){
    return  $this->hasManyThrough('App\HomeEvaluationRecord', 'App\SystemCheckRecord', 'id', 'system_check');
  }

  public function photo_addendum(){
    return  $this->hasMany('App\PhotoAddendum', 'owner', 'id')->with('images');
  }

  public function trainings(){
    return  $this->hasMany('App\TrainingRecord', 'applicant_id', 'id')->orderBy('id', 'desc');
  }

  public function last_training(){
    return  $this->hasOne('App\TrainingRecord', 'applicant_id', 'id')->orderBy('id', 'desc');
  }

  public function cv(){
    return  $this->hasMany('App\Cvs', 'applicant_id', 'id');
  }

  public function jobs(){
    return $this->hasMany('App\EmployeeJob', 'employee_id', 'id');
  }

  public function nbi_image(){
    return $this->hasOne('App\Media', 'id', 'nbi_image_id');
  }

  public function pou_file(){
    return $this->hasOne('App\Media', 'id', 'pou');
  }

  public function esigs(){
    return $this->hasMany('App\EmployeeContract', 'employee_id', 'id');
  }

  public function employee_emergency_contact(){
    return $this->hasOne('App\ApplicantEmergencyContact', 'applicant_id', 'id');
  }

  public function position_applied(){

    $position_applied = strtolower($this->position_applying_for) ?? '';

    $position = array('gva' => 'General Virtual Assistant', 'tc' => 'Transaction Coordinator', 'isa' => 'Inside Sales Associate', 'eva' => 'Executive Virtual Assistant');

    return $position[$position_applied];
  }

  public function pre_training_notes(){
    return $this->hasOne('App\PreTrainingNotes', 'applicant_id', 'id');
  }

  public function ratings(){
    return $this->hasOne('App\ApplicantRatings', 'app_id', 'id');
  }

  public function vats(){
    return $this->hasOne('App\VatsSystem', 'user_id', 'id');
  }

  public function kid(){
    return $this->id;
  }

  public function getVatsCountAttribute(){
    $vc = \App\VatsSystem::where('status', '0')->select('employment_history');

    return '321';
  }

}
