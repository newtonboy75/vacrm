<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmployeeContract extends Model
{
    protected $table = 'employee_contracts';
    protected $fillable = ['contract_id', 'employee_id', 'uid', 'contract_type'];
    public $timestamps = false;

    public function actions(){
      return $this->hasMany('App\EmployeeContractActions', 'contract_id', 'contract_id')->orderBy('id', 'desc');
    }

    public function employee(){
      return $this->hasOne('App\ApplicantRecord', 'id', 'employee_id');
    }

}
