<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantFirstInterview extends Model
{
    protected $table = 'applicants_initial_interviews';
    protected $fillable = ['applicant_id', 'interview_id'];
    public $timestamps = false;

    public function interview_record(){
      return  $this->hasMany('App\InterviewRecord', 'id', 'interview_id')->orderBy('id', 'desc');
    }

    public function application_status(){
      $status = $this->status;

      if($status == '0'){
        $context = 'Failed Paper Screening';
      }else{
        $context = 'Passed Paper Screening';
      }

      return $context;
    }

}
