<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClientSuspensionRecord extends Model
{
    protected $table = 'client_suspension';
    protected $fillable = ['client_info_id', 'suspension_reason'];
    public $timestamps = false;

    public function info(){
      return $this->hasOne('App\ClientPlacementRecord', 'id', 'client_info_id');
    }

}
