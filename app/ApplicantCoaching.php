<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApplicantCoaching extends Model
{
    protected $table = 'employee_coaching_logs';
    protected $fillable = ['employee_id', 'coaching_log_id'];
    public $timestamps = false;

    public function coachings(){
      return $this->hasMany('App\CoachingRecord', 'id', 'coaching_log_id');
    }
}
