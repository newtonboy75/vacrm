<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MemoRecord extends Model
{
    protected $table = 'memo';
    protected $fillable = ['date_served', 'issued_by', 'date_signed', 'status', 'memo', 'violation', 'memo'];
    public $timestamps = false;

}
