<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmployeeJob extends Model
{
    protected $table = 'employees_jobs';
    protected $fillable = ['employee_id', 'job_id'];
    public $timestamps = false;

    public function job_info(){
      return $this->hasMany('App\JobRecord', 'id', 'job_id')->orderBy('id', 'desc');
    }

}
