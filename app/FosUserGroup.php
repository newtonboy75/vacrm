<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FosUserGroup extends Model
{
    protected $table = 'fos_user_user_group';
    protected $fillable = ['user_id', 'group_id'];
    public $timestamps = false;

    public function fos_group(){
      return $this->hasMany('App\FosGroup', 'id', 'group_id');
    }

}
