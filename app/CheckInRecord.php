<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CheckInRecord extends Model
{
    protected $table = 'checkin';
    protected $fillable = ['served_date', 'file', 'signed_date', 'issued_by'];
    public $timestamps = false;

}
