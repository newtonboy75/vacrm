<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClientCancellationRecord extends Model
{
    protected $table = 'client_cancellation';
    protected $fillable = ['client_info_id', 'cancellation_date', 'reason', 'sub_reason', 'form_sent'];
    public $timestamps = false;

    public function info(){
      return $this->hasOne('App\ClientPlacementRecord', 'id', 'client_info_id');
    }

}
