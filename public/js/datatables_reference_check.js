
//ref
$('#data-ref').DataTable({
  pageLength:25,
  "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
  processing: true,
  serverSide: true,

    dom: 'ltipr',
  language: {
        "processing": "Loading. Please wait..."
    },
  ajax: {
       url: '/admin/reference-check/load',
       dataType: 'json',
       cache:false,
       type: 'GET',
       data: function ( d ) {

         if($('#all-rec'). is(":checked")){
           d.tab = ['sourcing_record', 'talent_acquisition', 'hr_record', 'reference_check', 'pre_training', 'training_record', 'placement_record'];
         }else{
           d.tab ='reference_check';
           //d.tab = ['hr_record'];
         }

          if($('#all-emp-stat'). is(":checked")){
            d.archived = ['0', '1', '2', '3'];
          }else{
            d.archived = ['0'];
          }


        }
    },
  "aaSorting": [],
  "columnDefs": [
       { "targets": [2,3,4,5], "searchable": false }
   ],
  "columnDefs": [ {
    "targets": 6,
    "data": "id",
    "render": function ( data, type, row, meta ) {
      return '<a href="/admin/reference-check/'+data+'/edit" title="Edit" class="text-primary"><i class="fas fa-edit"></i></a>&nbsp;<a href="/admin/sourcing-record/'+data+'/delete" id="del-app-rec" rel="'+data+'" title="Delete record" class="text-red"><i class="fas fa-times"></i></a>';
    }
  } ],
  "columns": [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email'},
            { data: 'created_at', name: 'created_at' },
            { data: 'transition_date', name: 'transition_date' },
            { data: 'workflow_status', name: 'workflow_status' },
            { data: 'id', name: 'id' },
        ]
});

var oTable = $('#data-ref').DataTable();
//$('#custom-search-input').on('keyup', function(){
  //alert('test');
  //oTable.search($(this).val()).draw();
//});
$('#custom-search-input').on( "keydown", function(event) {
      if(event.which == 13){
        oTable.search($(this).val()).draw();
      }
});

$('#cl-search a').click(function(){
  $('#custom-search-input').val('');
  $('#all-rec').prop("checked", false);
  $('#all-emp-stat').prop("checked", false);
  oTable.search('').draw();
});

$('#popupStatsModal').on('shown.bs.modal', function () {
  //alert('test');
  $('#temps_div').show();

});

$('#popupStatsModal').on('hide.bs.modal', function () {
  //alert('test');
  $('#temps_div').hide();

});
