var alldata = '';
$(function(){ //

$("[id='interviewInviteModalManual']").on("show.bs.modal", function (e) {

let rec_data = '';
let rec_skype = '';
let rec_email = '';
let  textbody = '';
let new_text_body = '';
let embody = '';
let allbody = '';

  if($("#sourcing_source_id").val() == '0'){
    alert('Please choose a recruiter');
  }else{
    textbody = $('#ebody').val();
    alldata = textbody;
    var dte = $('[name="sourcing_initial_interview_date"]').val();
    rec_data = $("#sourcing_source_id option:selected").attr('rel').split('|');
    rec_skype = rec_data[1];
    rec_email = rec_data[0];
    $('#ebody').val('');
    new_text_body = textbody.replace(/<span id="date_interview_src">[\s\S]*?<\/span>/, '<span id="date_interview_src">' + dte + '<\/span>');
    //var embody = new_text_body.replace('norecemail', rec_email);
    embody = new_text_body.replace(/<span id="pop_rec_email">[\s\S]*?<\/span>/, '<span id="pop_rec_email">' + rec_email + '<\/span>');
    //var allbody = embody.replace('norecskype', rec_skype); <span id="pop_rec_skype">{{$applicant->recruiter_name->skype_id ?? 'please choose a recruiter' }}</span>
    allbody = embody.replace(/<span id="pop_rec_skype">[\s\S]*?<\/span>/, '<span id="pop_rec_skype">' + rec_skype + '<\/span>');
    //alert(allbody);
    $('#ebody').val(allbody);
    CKEDITOR.replace('ebody');
    CKEDITOR.instances.ebody.setData(allbody);
  }

//CKEDITOR.replace( 'ebody');
});

$("[id='interviewInviteModalManual']").on("hide.bs.modal", function (e) {
  CKEDITOR.instances.ebody.destroy();
  $('#ebody').val(alldata);
});

$('#turndown_send_email').click(function(){
  if($('[name="sourcing_application_status"]').children("option:selected").val() != '3'){
    alert('Can only send Failed Initial Interview email to applicant with Failed Paper Screening status');
  }else{
    $('#interviewTurndownModal').modal('show');
  }
});

$('#reapplied').click(function(){
  $('#sendReappliedModal').modal('show');
});

$('#nonpinoy').click(function(){
  $('#sendNonPinoyModal').modal('show');
});

$('#duplicate_application').click(function(){
  if (confirm('Delete this record?')) {
  var id = $('[name="sourcing_applicant_id"]').val();

    $.ajax({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/sourcing-record/delete-application',
      type: 'post',
      data: { id: id },
      success: function(data) {
        if(data == '1'){
          alert('Application record deleted successfully');
          window.location.href = "/admin/sourcing-record";
        }else{
          alert('An error occured while processing request. Please contact Newton');
        }
      }
    });
  }
});

$('#preset_send_email').click(function(){
  if($('[name="sourcing_application_status"]').children("option:selected").val() != '2'){
    alert('Can only send Initial Interview email to applicant with Passed Paper Screening status');
  }else if($("#sourcing_source_id").val() == '0'){
    alert('Please choose a recruiter first and Update');
  }else{
    $('#interviewInviteModal').modal('show');
  }
});

$('#manual_send_email').click(function(){
  if($('[name="sourcing_application_status"]').children("option:selected").val() != '2'){
    alert('Can only send Initial Interview email to applicant with Passed Paper Screening status');
  }else if($("#sourcing_source_id").val() == '0'){
    alert('Please choose a recruiter first and Update');
  }else{
    var source_len = $("#sourcing_source_id option:selected").attr('rel').split('|');
    if(source_len[1] == ''){
      alert('Recruiter doesn\'t have a valid skype id');
    }else{
      $('#interviewInviteModalManual').modal('show');
    }
  }
});

$("[id='interviewInviteModal']").on("show.bs.modal", function (e) {

let rec_data = '';
let rec_skype = '';
let rec_email = '';
let  textbody = '';
let new_text_body = '';
let embody = '';
let allbody = '';

  if($("#sourcing_source_id").val() == '0'){
    alert('Please choose a recruiter');
  }else{
    textbody = $('#email-body').val();
    alldata = textbody;
    var dte = $('[name="sourcing_initial_interview_date"]').val();
    rec_data = $("#sourcing_source_id option:selected").attr('rel').split('|');
    rec_skype = rec_data[1];
    rec_email = rec_data[0];
    $('#ebody').val('');
    embody = textbody.replace(/<span id="pop_rec_email">[\s\S]*?<\/span>/, '<span id="pop_rec_email">' + rec_email + '<\/span>');
    allbody = embody.replace(/<span id="pop_rec_skype">[\s\S]*?<\/span>/, '<span id="pop_rec_skype">' + rec_skype + '<\/span>');

    $('#email-body').val(allbody);
    CKEDITOR.replace('email-body');
    CKEDITOR.instances.ebody.setData(allbody);
  }

//CKEDITOR.replace( 'ebody');
});

});

$('#exp-add-new').click(function(){

  if($('#source-experience').length > 1){
    $("#tbl-exp").append('<tr class="partial-experience"><td><input class="form-control" type="text" name="exp_company_name[]"></td><td><input class="form-control" type="text" name="exp_job_pos[]"></td><td><div class="input-group input-group"><input type="text" class="form-control" id="dateyear" name="exp_date_start[]"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div></div></td><td><div class="input-group input-group"><input type="text" class="form-control" id="dateyear" name="exp_date_end[]"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div></div></td><td><textarea class="form-control" name="exp_job_resp[]"></textarea></td><td scope="row" class="text-center"><div id="del-exp" rel=""><i class="fas fa-trash-alt"></i></div></td></tr>');

  }else{
    $('#source-experience').show('fast');

    $("#tbl-exp").append('<tr class="partial-experience"><td><input class="form-control" type="text" name="exp_company_name[]"></td><td><input class="form-control" type="text" name="exp_job_pos[]"></td><td><div class="input-group input-group"><input type="text" class="form-control" id="dateyear" name="exp_date_start[]"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div></div></td><td><div class="input-group input-group"><input type="text" class="form-control" id="dateyear" name="exp_date_end[]"><div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-calendar-alt"></i></span></div></div></td><td><textarea class="form-control" name="exp_job_resp[]"></textarea></td><td scope="row" class="text-center"><div id="del-exp" rel=""><i class="fas fa-trash-alt"></i></div></td></tr>');
  }
});

//talent tab
$('#interview1-add-new').click(function(){
  if($('#i_interview').css('display') == 'none'){
    $('#i_interview').show();
  }
  $("#tbl-interview").append('<tr><input type="hidden" name="talent_application_type_first[]" value="initial" required><td><select class="form-control" name="talent_application_status_first[]"><option value=""></option><option value="1">Passed Initial Interview</option><option value="2">Failed Initial Interview</option><option value="3">No Show</option><option value="5">Pending Interview</option><option value="4">Blacklisted</option><option value="6">Re-scheduled | Health Issues</option><option value="7">Re-scheduled | System Requirements</option><option value="8">Re-scheduled | Personal Matters</option><option value="9">Re-scheduled | Employment</option><option value="10">Re-scheduled | Family Emergency</option><option value="11">On Hold | System Requirements</option><option value="12">On Hold | Undecided</option><option value="13">Re-Application | System Requirements</option><option value="14">Re-Application | Undecided</option><option value="15">Re-Application | Health Issues</option><option value="16">Re-Application | Re-schedule issues</option><option value="17">Re-Application | Employed</option><option value="18">Re-Application | Personal Matters</option><option value="19">Re-Application | Emergency</option><option value="20">Re-Application | Business Structure</option><option value="21">Withdrawn | System Requirements</option><option value="22">Withdrawn | Undecided</option><option value="23">Withdrawn| Health Issues</option><option value="24">Withdrawn | Re-schedule issues</option><option value="25">Withdrawn | Employed</option><option value="26">Withdrawn| Personal Matters</option><option value="27">Withdrawn | Emergency</option><option value="28">Withdrawn | Business Structure</option><option value="29">Pending Exam</option><option value="30">Pending Systems Check</option><option value="31">Failed Systems Check</option><option value="32">Failed Exam</option></select></td><td><textarea name="talent_note_first[]" class="form-control"></textarea></td><td><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="talent_exp_date_end_first[]" value=""></td><td><textarea class="form-control" name="talent_rescheduled_first[]"></textarea></td><td><select class="form-control" id="tl_recruiter2" name="tl_recruiter[]"></select></td><td scope="row" class="text-center" rel="initial"><div id="del-int" rel=""><i class="fas fa-trash-alt"></i></div></td></tr>');

  $('[id="tl_recruiter2"]').html('');
  var $options = $("#nodisp > option").clone();
  $('[id="tl_recruiter2"]').html($options);

  $('[id="dpk2"]').Zebra_DatePicker({
    format: 'Y-m-d H:i A',
    direction: 0,
    show_icon: false,
    default_position: 'below',
    first_day_of_week : 0,
    enabled_minutes: [0, 15, 30, 45, 60]
  });
});

$('#interview2-add-new').click(function(){

  if($('#int-final').length > 1){
    $("#tbl-finterview").append('<tr><td><<input type="hidden" name="talent_application_type_final[]" value="final" required><select class="form-control" name="talent_application_status_final[]"><option value=""></option><option value="1">Passed Final Interview</option><option value="2">Failed Final Interview</option><option value="3">No Show</option><option value="5">Pending Interview</option><option value="4">Blacklisted</option><option value="6">Re-scheduled | Health Issues</option><option value="7">Re-scheduled | System Requirements</option><option value="8">Re-scheduled | Personal Matters</option><option value="9">Re-scheduled | Employment</option><option value="10">Re-scheduled | Family Emergency</option><option value="11">On Hold | System Requirements</option><option value="12">On Hold | Undecided</option><option value="13">Re-Application | System Requirements</option><option value="14">Re-Application | Undecided</option><option value="15">Re-Application | Health Issues</option><option value="16">Re-Application | Re-schedule issues</option><option value="17">Re-Application | Employed</option><option value="18">Re-Application | Personal Matters</option><option value="19">Re-Application | Emergency</option><option value="20">Re-Application | Business Structure</option><option value="21">Withdrawn | System Requirements</option><option value="22">Withdrawn | Undecided</option><option value="23">Withdrawn| Health Issues</option><option value="24">Withdrawn | Re-schedule issues</option><option value="25">Withdrawn | Employed</option><option value="26">Withdrawn| Personal Matters</option><option value="27">Withdrawn | Emergency</option><option value="28">Withdrawn | Business Structure</option><option value="29">Pending Exam</option><option value="30">Pending Systems Check</option><option value="31">Failed Systems Check</option><option value="32">Failed Exam</option></select></td><td><textarea name="talent_note_final[]" class="form-control" value="">tesss</textarea></td><td><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="talent_exp_date_end_final[]" value=""></td><td><textarea class="form-control" name="talent_rescheduled_final[]"></textarea></td><td><select class="form-control" id="tl_recruiter2" name="tl_recruiter_final[]"></select></td><td scope="row" class="text-center" rel="final"><div id="del-int" rel=""><i class="fas fa-trash-alt"></i></div></td></tr>');

    $('[id="tl_recruiter2"]').html('');
    var $options = $("#nodisp > option").clone();
    $('[id="tl_recruiter2"]').html($options);
  }else{
    $('#int-final').show('fast');
    $("#tbl-finterview").append('<tr><input type="hidden" name="talent_application_type_final[]" value="final" required><td><select class="form-control" name="talent_application_status_final[]"><option value=""></option><option value="1">Passed Final Interview</option><option value="2">Failed Final Interview</option><option value="3">No Show</option><option value="5">Pending Interview</option><option value="4">Blacklisted</option><option value="6">Re-scheduled | Health Issues</option><option value="7">Re-scheduled | System Requirements</option><option value="8">Re-scheduled | Personal Matters</option><option value="9">Re-scheduled | Employment</option><option value="10">Re-scheduled | Family Emergency</option><option value="11">On Hold | System Requirements</option><option value="12">On Hold | Undecided</option><option value="13">Re-Application | System Requirements</option><option value="14">Re-Application | Undecided</option><option value="15">Re-Application | Health Issues</option><option value="16">Re-Application | Re-schedule issues</option><option value="17">Re-Application | Employed</option><option value="18">Re-Application | Personal Matters</option><option value="19">Re-Application | Emergency</option><option value="20">Re-Application | Business Structure</option><option value="21">Withdrawn | System Requirements</option><option value="22">Withdrawn | Undecided</option><option value="23">Withdrawn| Health Issues</option><option value="24">Withdrawn | Re-schedule issues</option><option value="25">Withdrawn | Employed</option><option value="26">Withdrawn| Personal Matters</option><option value="27">Withdrawn | Emergency</option><option value="28">Withdrawn | Business Structure</option><option value="29">Pending Exam</option><option value="30">Pending Systems Check</option><option value="31">Failed Systems Check</option><option value="32">Failed Exam</option></select></td><td><textarea name="talent_note_final[]" class="form-control"></textarea></td><td><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="talent_exp_date_end_final[]" value=""></td><td><textarea class="form-control" name="talent_rescheduled_final[]"></textarea></td><td><select class="form-control" id="tl_recruiter2" name="tl_recruiter_final[]"></select></td><td scope="row" class="text-center" rel="final"><div id="del-int" rel=""><i class="fas fa-trash-alt"></i></div></td></tr>');

    $('[id="tl_recruiter2"]').html('');
    var $options = $("#nodisp > option").clone();
    $('[id="tl_recruiter2"]').html($options);
  }

  $('[id="dpk2"]').Zebra_DatePicker({
    format: 'Y-m-d H:i A',
    direction: 0,
    show_icon: false,
    default_position: 'below',
    first_day_of_week : 0,
    enabled_minutes: [0, 15, 30, 45, 60]
  });
});

//reference tab
$('#ref-add-new').click(function(){
  $('#tbl-ref').append('<tr><input type="hidden" name="ref_id[]" value=""><td><input type="text" name="referencer[]" class="form-control" value=""></td><td><input type="text" name="company[]" class="form-control" value=""></td><td><input type="text" name="position[]" class="form-control" value=""></td><td><input type="text" name="mobile_number[]" class="form-control" value=""></td><td><input type="text" name="landing_number[]" class="form-control" value=""></td><td><input type="text" name="email[]" class="form-control" value=""></td><td><input type="text" name="best_time_to_contact[]" class="form-control" value=""></td><td class="text-center"><div id="ref-int" rel=""><i class="fas fa-trash-alt"></i></div></td></tr><tr><td colspan="8"><div class="mr-5" style="width: 300px;"><label class="label-info">Date Contacted</label><input type="text" name="date_contacted[]" class="form-control" id="dpk-ref"></div></div><br><div style="width: 300px;"><label class="label-info"><i class="far fa-sticky-note"></i> Recruiter\'s Note &nbsp;&nbsp;</label><textarea class="form-control" name="note[]"></textarea></div></td></tr>');

  $('[id="dpk-ref"]').Zebra_DatePicker({
    format: 'M d, Y h:mm A',
    direction: 0,
    show_icon: false,
    first_day_of_week : 0,
    enabled_minutes: [0, 15, 30, 45, 60]
  });
});

  //trainings
$(document).on('click', '#training-add-new', function(e) {

e.preventDefault();
$('#tbl-training').append('<tr><input type="hidden" name="training_id[]" value=""><td><select id="batchid-new" name="batch[]" class="form-control"></select></td><td><input type="text" class="form-control" name="program[]" value=""></td><td><input type="text" class="form-control" name="schedule[]" value=""></td><td><select name="trainer_id[]" class="form-control" id="trainer_new"></select></td><td><input type="text" class="form-control dd1" id="dpk2_" name="start_date[]" value=""></td><td><input type="text" class="form-control dd2" id="dpk2_" name="end_date[]" value=""></td><td></td><td class="text-center"><div id="del-pre" rel=""><i class="fas fa-trash-alt"></i></div></td></tr>');

$.ajax({
  headers: {
  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
  url: '/admin/pre-training-requirements/get-new-batches',
  type: 'post',
  data: { id: 0 },
  success: function(data) {
    $('#batchid-new').html('<option disabled selected>Choose</option>' + data);
  }
});

$('[id="trainer_new"]').html('');
$('[id="batchid"]').css('width', '50px');

$.ajax({
  headers: {
  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
  url: '/admin/pre-training-requirements/trainers',
  type: 'post',
  data: { id: 0 },
  success: function(data) {
    $('[id="trainer_new"]').html(data);
  }
});

});

$(document).on('click', '#client-add-new', function() {

var options = $('#client-interview-tbl tr:last-child').find("#client_old > option").clone();

$('#client-interview-tbl').append('<tr><td><input type="hidden" name="client_interview_id[]" value=""><label class="top-label mt-1">Client Interviews</label><select class="form-control client_id" id="client_new" name="client_id[]"></select><button class="btn btn-sm btn-success mt-1">Add New</button><br><label class="top-label mt-3">Job Description</label><textarea class="form-control mb-3" name="interview_description[]"></textarea></td><td><select class="form-control mt-2" style="width:80px !important;" name="client_va_pool[]" id="client_va_pool"><option value="1">ISA</option><option value="2">GVA - TC</option><option value="3">GVA - General</option><option value="4">GVA - Marketing</option><option value="5">EVA</option></select></td><td><select class="form-control mt-2" style="width:80px !important;" name="client_va_status[]"><option value="1">FT</option><option value="2">PT</option><option value="3">Timeblock</option><option value="4">Trial - 20 hours</option><option value="5">Trial - 40 hours</option><option value="6">Trial -1 month</option><option value="7">Trial - 2 months</option></select></td><td><input data-zdp_readonly_element="false" style="background: #fff !important;" class="form-control mt-2 dpk" id="dpk" type="text" value="" name="client_interview_sched[]"></td><td><select id="client_interview_status" name="client_interview_status[]" class="form-control mt-2" style="width:120px !important"><option value="5">Scheduled</option><option value="6">Waiting For Client\'s Decision</option><option value="7">Hired</option><option value="8">Available</option><option value="9">On Hold</option><option value="10">Declined</option></select></td><td><textarea name="client_interview_note[]" class="form-control mt-2"></textarea></td><td class="text-center"><button class="btn btn-sm btn-success mt-2" id="client_send_email" rel="">Send email</button><div class="position-relative" style="margin-top: 140px;" title="delete record" id="del-placement" rel=""><i class="fas fa-trash-alt"></i> Delete</div></td></tr>');

  $('[id="dpk"]').Zebra_DatePicker({
  format: 'Y-m-d H:i A',
  direction: 0,
  show_icon: false,
  icon_position: 'left',
  default_position: 'below',
  first_day_of_week : 0,
  enabled_minutes: [0, 15, 30, 45, 60]
  });

  if($('[id="client_new"]').size === undefined){
    $.ajax({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/placement-record/get_clients',
      type: 'post',
      data: { id: 0 },
      success: function(data) {
        $('[id="client_new"]').append('<option selected disabled>Choose a client</option>' + data);
      }
    });
  }else{
    $('[id="client_new"]').append(options);
  }
});

$(document).on('click', '#del-placement', function() {
var el = $(this);
var id = el.attr('rel');

  if(id === ''){
  $(this).parent().parent().hide('slow').remove();
  }else{
    $.ajax({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/placement-record/delete',
      type: 'post',
      data: { id: id },
      success: function(data) {
      //console.log(data);
        if(data == '1'){
          el.parent().parent().hide('slow').remove();
        }
      }
    });
  }
});

$(document).on('click', '#client_send_email', function() {
var id = $(this).attr('rel');
var client_interview_status = $(this).parent().parent().find('#client_interview_status').val();
var position = $(this).parent().parent().find('#client_va_pool option:selected').text();
var client_interview_sched  = $(this).parent().parent().find('#client_interview_sched').val();
var client = $(this).parent().parent().find('.client_id :selected').val();

$('#message').html('Sending email... please wait...');

  if(client_interview_sched === ''){
    alert('Date is required');
  }else if(id === ''){
    alert('Please save the changes first');
  }else{

    $.ajax({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/placement-record/hired',
    type: 'post',
    data: { id: id, position: position,  date: client_interview_sched, client: client},
    success: function(data) {
    console.log(data);
      if(data == '1'){
      alert('Email sent to applicant');
        $('#message').html('');
      }else{
        alert(data);
        $('#message').html('');
      }
        $('#message').html('');
      }
    });

  }
});

$('#save-add-client').click(function(){
var client_name = $('#client_name').val();
var state = $('#client_state').val();
var tz = $('#client_tz').val();

    if(client_name == ''){
      alert('Client name is required');
    }else{
      $.ajax({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/placement-record/add_new_client',
      type: 'post',
      data: { client_name: client_name , state: state, tz: tz},
      success: function(data) {
      //console.log(data);
        if(data[0] == '1'){
        alert('New client added successfully');
          $('.client_id').append($('<option>', {
            value: data[1]['id'],
            text : data[1]['name']
          }));
        }else{
        alert('An error occured while processing data. Please contact Newton');
        }
      }
    });
  }
});

$('#applicant-record-save').click(function(){
var active_pane = $('.tab-content .active');
var from_tab = active_pane.find('#from_tab').val();
var els = '';

$('#message').html('processing... please wait...');

  if(from_tab === undefined){
    els = $('.card-pad').find('input, select, textarea');
    from_tab = $('.card-pad').find('#from_tab').val();
    }else{
    els = active_pane.find('input, select, textarea');
    }

    if(from_tab == 'pre-training-requirements'){
    var preform = $('#pretraining-form')[0];
    var formData = new FormData(preform);
    formData.append('nbi_file', $('#fileInput2')[0].files[0]);
    formData.append('id_image', $('#fileInput')[0].files[0]);
      $.ajax({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/admin/'+from_tab+'/update',
        type: 'post',
        async: false,
        cache: false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        data:formData,
        success: function(data) {
        $('#message').html('');
        console.log(data);
          if(data == '1'){
            if(from_tab == 'placement-record'){
            location.reload();
            }
            //location.reload();
            $('#message').append('<span class="text-success">Update successful</span>');
            location.reload();
            }else{
              alert(data);
            }
        }
      });
  }else{
    $.ajax({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/'+from_tab+'/update',
      type: 'post',
      data: els.serialize(),
      success: function(data) {
      $('#message').html('');
      console.log(data);
      if(data == '1'){
        if(from_tab == 'placement-record'){
          location.reload();
        }

        $('#message').append('<span class="text-success">Update successful</span>');
        location.reload();
      }else{
        alert(data);
      }
      }
    });
  }
});

$(document).on('click', '#del-exp', function() {
var el = $(this);
var exp_id = $(this).attr('rel');
  if(exp_id === ''){
    $(this).parent().parent().hide('slow').remove();
  }else{
    $.ajax({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/sourcing-record/delete',
      type: 'post',
      data: { id: exp_id },
      success: function(data) {
        if(data == '1'){
          $(el).parent().parent().hide('slow').remove();
        }
      }
    });
  }
});

$(document).on('click', '#del-int', function() {

  if (confirm('Delete this item?')) {
    var el = $(this);
    var int_id = $(this).attr('rel');
    var int_type = $(this).parent().attr('rel');
    if(int_id === ''){
      $(this).parent().parent().hide('slow').remove();
    }else{
      $.ajax({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/admin/talent-acquisition-record/delete',
        type: 'post',
        data: { id: int_id, type:  int_type },
        success: function(data) {
        //console.log(data);
          if(data == '1'){
          location.reload()
          $(el).parent().parent().hide('slow').remove();
          }
        }
      });
    }
  }
});

$(document).on('click', '#ref-int', function() {

  if (confirm('Delete this item?')) {
  var el = $(this);
  var int_id = $(this).attr('rel');

    if(int_id === ''){
      $(this).parent().parent().hide('slow').remove();
    }else{
      $.ajax({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/admin/reference-check/delete',
        type: 'post',
        data: { id: int_id },
        success: function(data) {
          if(data == '1'){
            $(el).parent().parent().hide('slow').remove();
          }
        }
      });
    }
  }
});

var flag = 0;
//uploader
$('[id="uploader"]').click(function() {
  var fd = new FormData();
  var files = $(this).prev('input')[0].files[0];
  fd.append('file', files);
  var id = $(this).parent().find('#imageid').val();
  fd.append('id', id);
  var from_id = $(this).parent().find('#from').val();
  fd.append('from', from_id);
  var file_id = $(this).parent().find('#fileid').val();
  fd.append('file_id', file_id);
  var user_id = $(this).parent().find('#user_id').val();
  fd.append('user_id', user_id);
  var up = false;


  $.ajax({
  url: '/admin/home-evaluation-record/upload',
  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
  type: 'post',
  data: fd,
  contentType: false,
  processData: false,
  success: function(response){
  //console.log(response);

    if(response != 0){
      if(flag == 0) {
      $('img#'+from_id).attr('src', response);
      flag = 1;
      }
      else if(flag == 1) {
      $('img#'+from_id).attr('src', response);
      flag = 0;
      }
    }
    else{
      alert('file not uploaded');
    }
  },
  });
});

$(document).on('click', '#del-pre', function() {

  if (confirm('Delete this item?')) {
    var el = $(this);
    var int_id = $(this).attr('rel');
    var int_type = $(this).parent().attr('rel');

    if(int_id === ''){
      $(this).parent().parent().hide('slow').remove();
    }else{
      $.ajax({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/admin/pre-training-requirements/delete',
        type: 'post',
        data: { id: int_id, type:  int_type },
        success: function(data) {
        //console.log(data);
          if(data == '1'){
          //location.reload()
          $(el).parent().parent().hide('slow').remove();
          }
        }
      });
    }
  }

});

$('[id="file"]').change( function(event) {
  var target = $(this).attr('rel');

  var reader = new FileReader();
  reader.onload = function(){
    var output = $('.thumb-wrapper').find(target).find('img'); //document.getElementById(target);
    output.src = reader.result;
  };
  reader.readAsDataURL(event.target.files[0]);
  $(this).next('input').click();

});

$(document).ready( function () {

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$('[id="sender"]').click(function(){
var parent_div = '';
var err = 0;

  if($(this).attr('rel') === 'sourcing-record1'){
    parent_div = $('#interviewInviteModalManual');
    var to = parent_div.find('#to').val();
    //var email_body = parent_div.find('textarea').val();
    var email_body = CKEDITOR.instances['ebody'].getData();
    var email_type = parent_div.find('#email_type').val();
    var user_id = parent_div.find('#user_id').val();
  }else if($(this).attr('rel') === 'sourcing-record2'){
    parent_div = $('#interviewTurndownModal');
    var to = parent_div.find('#to').val();
    //var email_body = parent_div.find('textarea').val();
    var email_body = CKEDITOR.instances['email-body2'].getData();
    var email_type = parent_div.find('#email_type').val();
    var user_id = parent_div.find('#user_id').val();
  }else{
    parent_div = $('#interviewInviteModal');
  }

  if(err == 1){
    alert('Please choose a client');
  }else{
    $('.wait-status').html('sending email... please wait');
    $.ajax({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/sourcing-record/send_email',
      type: 'post',
      data: { to: to, type:  email_type, email_body: email_body, user_id: user_id },
      success: function(data) {
      console.log(data);
      if(data == '1'){
        //parent_div.modal('hide');
        $('#message-alert').html('Email has been sent').show().delay(3000).hide(0);
        $('.wait-status').html('');
        parent_div.find('#email_dismiss').click();
        $('#interviewInviteModal').modal('hide');
        $('.modal').modal('hide');
      }
      }
    });
  }
});

$('[id="sender2"]').click(function(){

var parent_div = '';

if($(this).attr('rel') === 'interview_passed'){
  parent_div = $('#sendReferenceEmailModal');
  var email_body = CKEDITOR.instances['email-body-t'].getData();
}else{
  parent_div = $('#sendFailedEmailModal');
  var email_body = CKEDITOR.instances['email-body4'].getData();
}

var to = parent_div.find('#to').val();
//var email_body = parent_div.find('textarea').val();

var email_type = parent_div.find('#email_type').val();
var user_id = parent_div.find('#user_id').val();
$('.wait-status').html('sending email... please wait');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/talent-acquisition-record/send_email',
    type: 'post',
    data: { to: to, type:  email_type, email_body: email_body, user_id: user_id },
    success: function(data) {
      console.log(data);
      if(data == '1'){
        //parent_div.modal('hide');
        $('#message-alert').html('Email has been sent').show().delay(2000).hide(0);
        $('.wait-status').html('');

        email_body = '';
        parent_div.find('#email_dismiss').click();
        $('.modal').modal('hide');

      }
    }
  });
});

$('[id="sender3"]').click(function(){

var parent_div = '';

if($(this).attr('rel') === 'home_eval'){
  parent_div = $('#sendHomeEvalModal');
  var email_body = CKEDITOR.instances['email-body-4'].getData();
}else{
  parent_div = $('#sendRegretModal');
  var email_body = CKEDITOR.instances['email-body-5'].getData();
}

var to = parent_div.find('#to').val();
var email_type = parent_div.find('#email_type').val();
var user_id = parent_div.find('#user_id').val();

$('.wait-status').html('sending email... please wait');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/reference-check/send_email',
    type: 'post',
    data: { to: to, type:  email_type, email_body: email_body, user_id: user_id },
    success: function(data) {
      //  //console.log(data);
      if(data == '1'){
        //parent_div.modal('hide');
        $('#message-alert').html('Email has been sent').show().delay(3000).hide(0);
        $('.wait-status').html('');
        parent_div.find('#email_dismiss').click();

      }
    }
  });
});

$('#create_placement_record').click(function(){
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/training-record/create_placement_record',
      type: 'post',
      data: { applicant_id : $('#applicant_id').val()},
      success: function(data) {
        //console.log(data);
        if(data == '1'){
          $('#message-alert').html('Placement Record created successfully').show().delay(3000).hide(0);
          $('.wait-status').html('');
        }
      }
    });
  });

$('#create_training_record').click(function(){
$.ajax({
headers: {
  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
  url: '/admin/pre-training-requirements/create_training_record',
  type: 'post',
  data: { applicant_id : $('#applicant_id').val()},
  success: function(data) {
    console.log(data);
    if(data == '1'){
      $('#message-alert').html('Placement Record created successfully').show().delay(3000).hide(0);
      $('.wait-status').html('');
    }else{
      alert('Cannot create training record. Please check if all information is complete or training date is already set');
    }
  }
});
});

$('#create_hr_record').click(function(){
$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/placement-record/create_hr_record',
    type: 'post',
    data: { id : $('#applicant_id').val()},
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#message-alert').html('Placement Record created successfully').show().delay(3000).hide(0);
        $('.wait-status').html('');
      }
    }
  });
});

$('#add_coaching_row').click(function(){
$('#coaching_tbl').append('<tr><td><input type="hidden" name="coaching_id[]" value=""><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="date_served[]" value=""></td><td><select id="issued_by" class="form-control issued_by" name="issued_by[]"></select></td><td><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="date_signed[]" value=""></td><td><input type="text" class="form-control" id="violation" name="violation[]" value=""></td><td><select class="form-control" name="status[]"><option value="1">Active</option><option value="2">Inactive</option></select></td><td><input class="form-control" type="file" id="file" name="file[]" multiple></td><td><div title="Delete item" class="mt-1 top-label del-item checkin-info-delete"id="coaching-info" rel=""><i class="fas fa-trash-alt"></i></div></td></tr>');
$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/hr-operation-record/get_coaches',
    type: 'post',
    data: { id: 0 },
    success: function(data) {
      //console.log(data);
      $('.issued_by').html(data);
    }
  });

  $('[id="dpk2"]').Zebra_DatePicker({
    format: 'Y-m-d H:i A',
    direction: 0,
    show_icon: false,
    first_day_of_week : 0,
    enabled_minutes: [0, 15, 30, 45, 60]
  });
});

$(document).on('click', '#memo-info', function(){
var from = $(this).attr('id');
var id = $(this).attr('rel');

var el = $(this);

if(id == ''){
  el.parent().parent().remove();
}else{
  if (confirm('Delete this item?')) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/admin/hr-operation-record/delete',
        type: 'post',
        data: { id: id, from: from },
        success: function(data) {
          //console.log(data);
          location.reload();
          if(data == '1'){
            el.parent().parent().hide();
          }

        }
      });
    }
  }
});

$(document).on('click', '#coaching-info', function(){
var from = $(this).attr('id');
var id = $(this).attr('rel');

var el = $(this);

if(id == ''){
  el.parent().parent().remove();
}else{
  if (confirm('Delete this item?')) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/admin/hr-operation-record/delete',
        type: 'post',
        data: { id: id, from: from },
        success: function(data) {
          //console.log(data);
          location.reload();
          if(data == '1'){
            el.parent().parent().hide();
          }

        }
      });
    }
  }
});

$('#add_memo_row').click(function(){
$('#memo_tbl').append('<tr><td><input type="hidden" name="coaching_id[]" value=""><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="date_served[]" value=""></td><td><select id="issued_by" class="form-control issued_by" name="issued_by[]"></select></td><td><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="date_signed[]" value=""></td><td><input type="text" class="form-control" id="violation" name="violation[]" value=""></td><td><select class="form-control" name="status[]"><option value="1">Active</option><option value="2">Inactive</option></select></td><td><input type="hidden" name="termination_file[]" value=""><select title="Please save changes to activate" disabled class="form-control" id="esig_contracts"><option>Select</option><option value="fte">FTE</option><option value="nte">NTE</option><option value="va_placement_hold">Placement - Hold/Suspension</option><option value="caf">Corrective Action Form</option><option value="termination">Termination Letter</option></select></td><td><div title="Delete memo" class="mt-1 top-label del-item checkin-info-delete" id="memo-info" rel=""><i class="fas fa-trash-alt"></i></div></td></tr>');
$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/hr-operation-record/get_coaches',
    type: 'post',
    data: { id: 0 },
    success: function(data) {
      //console.log(data);
      $('.issued_by').html(data);
    }
  });

  $('[id="dpk2"]').Zebra_DatePicker({
    format: 'Y-m-d H:i A',
    direction: 0,
    show_icon: false,
    first_day_of_week : 0,
    enabled_minutes: [0, 15, 30, 45, 60]
  });
});

$('#sms_text_body').keypress(function(e) {
var el = $(this);
if(el.val().length >= 160 ){
  e.preventDefault();
}else{
  $('#text_count').text(el.val().length+1);
}
});

$(document).on('click', '#send_sms', function(){
var parent_div = $('#interviewInviteModalSMS');
var to = parent_div.find('#to').val();
var sms_body = parent_div.find('textarea').val();
var user_id = parent_div.find('#user_id').val();
$('.wait-status').html('sending sms... please wait');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/send_sms',
    type: 'post',
    data: { to: to, sms_body: sms_body },
    success: function(data) {
      //console.log(data);
      $('.wait-status').html('sms sent');
    }
  });

});

$(document).on('click', '#add_checkin_row', function(){

$('#checkin_tbl').append('<tr><td><input type="hidden" name="va_id[]" value=""><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="served_date[]" value=""></td><td><select name="issued_by[]" class="form-control issued_by" ></select></td><td><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="signed_date[]" value=""></td><td style="text-align:center"><input type="hidden" name="file[]" id="file"><div class="btn-group btn-group-sm mx-auto" role="group" aria-label="File"><a class="btn btn-primary" href="javascript:;" id="va_checkin_file" title="Create" data-toggle="modal" data-target="#checkinModal"><i class="far fa-edit"></i></a></div></td><td class="text-center"><div title="Delete item" class="mt-1 top-label del-item checkin-info-delete" id="checkin-info" rel=""><i class="fas fa-times"></i></div></td></tr>');

$.ajax({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
url: '/admin/hr-operation-record/get_coaches',
type: 'post',
data: { id: 0 },
success: function(data) {
  //console.log(data);
  $('.issued_by').html(data);
}
});

$('[id="dpk2"]').Zebra_DatePicker({
format: 'Y-m-d H:i A',
direction: 0,
show_icon: false,
first_day_of_week : 0,
enabled_minutes: [0, 15, 30, 45, 60]
});
});

$(document).on('click', '#add_new_emp_info', function(){
var text = '<div class="row" style="border: 1px solid #f0f0f0; padding: 20px; background: #fafafa; margin-top:16px;"><div class="col-sm-12"><h5>Create new job</h5><input type="hidden" name="empid[]" value=""><br></div><div class="col-sm-4"><label class="top-label mt-2">Client</label><select class="form-control client_id" id="client_new" name="client_id[]"></select><label class="top-label mt-2">Level</label><select class="form-control" id="level" name="level[]"><option value="1">1st VA</option><option value="2">Replacement VA</option><option value="3">Multiple VA</option></select><label class="top-label mt-2">Hourly Rate</label><div class="input-group"><div class="input-group-prepend"><span class="input-group-text" id="basic-addon1">$</span></div><input type="number" class="form-control" id="hourly_rate" name="hourly_rate[]"></div><label class="top-label mt-2">Job Description</label><textarea class="form-control" name="job_description[]" id="job_description"></textarea><label class="top-label mt-2">Schedule</label><input class="form-control" type="text" name="schedule[]" id="schedule" value=""></div><div class="col-sm-4"><label class="top-label mt-2">Manager</label><input class="form-control" type="text" name="manager[]" id="manager" value=""><label class="top-label mt-2">Team</label><select class="form-control" name="team[]" id="newteam"></select><label class="top-label mt-2">Start Date</label><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="start_date[]" value=""><label class="top-label mt-2">End Date</label><input data-zdp_readonly_element="false" type="text" class="form-control" id="dpk2" name="end_date[]" value=""><label class="top-label mt-2">Status</label><select class="form-control" id="status" name="empstatus[]"><option value="1">Active</option><option value="2">Canceled</option><option value="3">Replaced</option><option value="4">For Cancellation</option><option value="5">For Replacement</option></select></div><div class="col-sm-4"><label class="top-label mt-2">Employment Status</label><select class="form-control mt-2" name="employment_status[]" id="employment_status"><option value="1">FT</option><option value="2">PT</option><option value="3">Timeblock</option><option value="4">Trial - 20 hours</option><option value="5">Trial - 40 hours</option><option value="6">Trial -1 month</option><option value="7">Trial - 2 months</option></select><label class="top-label mt-2">Notes</label><textarea class="form-control" id="note" name="note[]"></textarea><label class="top-label mt-2">System Tools</label><textarea class="form-control" id="system_tools" name="system_tools[]"></textarea><div title="Delete item" class="mt-3 float-right top-label del-item-hr" id="ref-emp-info" rel=""><i class="fas fa-trash-alt"></i> Delete Record</div></div></div><br>';

$('#add_new_emp_info').before(text);
var assigned_client = $('#assigned_client').val();

$.ajax({
headers: {
  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
  url: '/admin/hr-operation-record/get_groups',
  type: 'post',
  data: { id: 0 },
  success: function(data) {
    console.log(data);
    $('#newteam:last').html('<option disabled selected>Choose Team</option>' + data);
    //$('#team option[value='+assigned_client+']').attr('selected','selected');
  }
});


if($('#client_id').lenght > 0){
  var options = $("#client_id > option").clone();
  $('.client_id:last').html('<option disabled selected>Choose Client</option>' + options);
}else{
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/hr-operation-record/get_clients',
      type: 'post',
      data: { id: 0 },
      success: function(data) {
        console.log(data);
        $('.client_id:last').html('<option disabled selected>Choose Client</option>' + data);
        //$('.client_id option[value='+assigned_client+']').attr('selected','selected');
      }
    });
  }

  $('[id="dpk2"]').Zebra_DatePicker({
    format: 'Y-m-d H:i A',
    direction: 0,
    show_icon: false,
    first_day_of_week : 0,
    enabled_minutes: [0, 15, 30, 45, 60]
  });

});

$('#hr-record-save').click(function(){

var active_tab = $('#myTabContent').find('div.active').attr('id');
var els = $('#myTabContent').find('div.active').find('input, select, textarea');
var send_data = '';

if(active_tab === 'coaching_log' || active_tab === 'memo' ||  active_tab == 'employee_info'){
var formData = new FormData($('#'+active_tab).find("#formEmployee")[0]);
formData.append('file', $('#formIinput'));
send_data = formData;
//console.log(send_data);

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/hr-operation-record/update',
    type: 'post',
    data: send_data,
    mimeTypes:"multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    success: function(data) {
      console.log(data);
      if(data == '1'){
        alert('Update successful');
        location.reload();
      }else{
        alert(data);
      }
    }
  });
}else{

  send_data = els.serialize();
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/hr-operation-record/update',
      type: 'post',
      data: send_data,
      success: function(data) {
        //console.log(data);
        if(data == '1'){
          alert('Update successful');
          location.reload();
        }
      }
    });
  }
  //alert(active_tab); formData.append('file', $('#file')[0].files[0]);

});

//send_resched

$('#send_resched').click(function(){
var to = $('#app_to').val();
var body = CKEDITOR.instances['email-body3'].getData();

$('#resmsg').html('Sending email... please wait');
$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/talent-acquisition-record/resched',
    type: 'post',
    data: { to: to, body: body },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#sendRescheduleEmailModal').modal('hide');
        alert('Email sent successfully');
        $('#resmsg').html('');
        //location.reload();

      }
    }
  });
});

//delete item
$(document).on('click', '.del-item-hr', function(){
var from = $(this).attr('id');
var id = $(this).attr('rel');

var el = $(this);

if(id == ''){
  el.parent().parent().remove();
}else{
  if (confirm('Delete this item?')) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/admin/hr-operation-record/delete',
        type: 'post',
        data: { id: id, from: from },
        success: function(data) {
          //console.log(data);
          location.reload();
          if(data == '1'){
            el.parent().parent().hide();
          }

        }
      });
    }
  }

});

//resend

$('#resend_email').click(function(){
  $('#sendResendModal').modal('show');
});


//emails for reapplication
$('#reapply_email').click(function(){
var email_body = CKEDITOR.instances['email-body-reapply'].getData();
var to = $('#sendReappliedModal').find('#to').val();
var type = $('#sendReappliedModal').find('#email_type').val();
var from = $('#sendReappliedModal').find('#from_admin').val();
var subject = $('#sendReappliedModal').find('#subject').val();
var user_id = $('#sendReappliedModal').find('#user_id').val();
var email_type = $('#sendReappliedModal').find('#email_type').val();
$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/dashboard/mailer',
    type: 'post',
    data: { user_id: user_id, to: to, body: email_body, type: type, from: from, subject: subject, email_type: email_type },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#sendReappliedModal').modal('hide');
        alert('Email sent successfully');
        $('.wait-status').html('');
        //location.reload();

      }
    }
  });

});

//emails for non pinoy
$('#nonpinoy_email').click(function(){
var email_body = CKEDITOR.instances['email-body-nonpinoy'].getData();
var to = $('#sendNonPinoyModal').find('#to').val();
var type = $('#sendNonPinoyModal').find('#email_type').val();
var from = $('#sendNonPinoyModal').find('#from_admin').val();
var subject = $('#sendNonPinoyModal').find('#subject').val();
var user_id = $('#sendNonPinoyModal').find('#user_id').val();
var email_type = $('#sendNonPinoyModal').find('#email_type').val();
$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/dashboard/mailer',
    type: 'post',
    data: { user_id: user_id, to: to, body: email_body, type: type, from: from, subject: subject, email_type: email_type },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#sendNonPinoyModal').modal('hide');
        alert('Email sent successfully');
        $('.wait-status').html('');
        //location.reload();

      }
    }
  });

});

$('#newsked_email_final').click(function(){
var email_body = CKEDITOR.instances['email-body-invite-initial'].getData();
var to = $('#sendNewFinal').find('#to').val();
var type = $('#sendNewFinal').find('#email_type').val();
var from = $('#sendNewFinal').find('#from_admin').val();
var subject = $('#sendNewFinal').find('#subject').val();
var user_id = $('#sendNewFinal').find('#user_id').val();
var email_type = $('#sendNewFinal').find('#email_type').val();
$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/dashboard/mailer',
    type: 'post',
    data: { user_id: user_id, to: to, body: email_body, type: type, from: from, subject: subject, email_type: email_type },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#sendNewFinal').modal('hide');
        alert('Email sent successfully');
        $('.wait-status').html('');
        //location.reload();

      }
    }
  });
});


$('#newsked_email_initial').click(function(){
var email_body = CKEDITOR.instances['email-body-invite-final'].getData();
var to = $('#sendNewInitial').find('#to').val();
var type = $('#sendNewInitial').find('#email_type').val();
var from = $('#sendNewInitial').find('#from_admin').val();
var subject = $('#sendNewInitial').find('#subject').val();
var user_id = $('#sendNewInitial').find('#user_id').val();
var email_type = $('#sendNewInitial').find('#email_type').val();
$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/dashboard/mailer',
    type: 'post',
    data: { user_id: user_id, to: to, body: email_body, type: type, from: from, subject: subject, email_type: email_type },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#sendNewInitial').modal('hide');
        alert('Email sent successfully');
        $('.wait-status').html('');
        //location.reload();

      }
    }
  });
});

//
$('#presched').click(function(){
var email_body = CKEDITOR.instances['email-body-presched'].getData();
var to = $('#sendTrainingSched').find('#to').val();
var type = $('#sendTrainingSched').find('#email_type').val();
var from = $('#sendTrainingSched').find('#from_admin').val();
var subject = $('#sendTrainingSched').find('#subject').val();
var user_id = $('#sendTrainingSched').find('#user_id').val();
var email_type = $('#sendTrainingSched').find('#email_type').val();
var training_id = $('#sendTrainingSched').find('#training_id').val();

$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/dashboard/mailer',
    type: 'post',
    data: { training_id: training_id, user_id: user_id, to: to, body: email_body, type: type, from: from, subject: subject, email_type: email_type },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#sendTrainingSched').modal('hide');
        //alert('Email sent successfully');
        $('#message-alert').html('Email has been sent').show().delay(3000).hide(0);
        $('.wait-status').html('');
        //location.reload();

      }
    }
  });
});

//resched training
$('#training_resched').click(function(){
var email_body = CKEDITOR.instances['email_body_resend_resched'].getData();
var to = $('#popupTrainingReschedModal').find('#to').val();
var type = $('#popupTrainingReschedModal').find('#email_type').val();
var from = $('#popupTrainingReschedModal').find('#from_admin').val();
var subject = $('#popupTrainingReschedModal').find('#subject').val();
var user_id = $('#popupTrainingReschedModal').find('#user_id').val();
var email_type = $('#popupTrainingReschedModal').find('#email_type').val();
var training_id = $('#popupTrainingReschedModal').find('#training_id').val();

$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/dashboard/mailer',
    type: 'post',
    data: { training_id: training_id, user_id: user_id, to: to, body: email_body, type: type, from: from, subject: subject, email_type: email_type },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#popupTrainingReschedModal').modal('hide');
        //alert('Email sent successfully');
        $('#message-alert').html('Email has been sent').show().delay(3000).hide(0);
        $('.wait-status').html('');
        //location.reload();

      }
    }
  });
});

$('#presched-bs').click(function(){
var email_body = CKEDITOR.instances['email-body-resched-bs'].getData();
var to = $('#sendReschedBs').find('#to').val();
var type = $('#sendReschedBs').find('#email_type').val();
var from = $('#sendReschedBs').find('#from_admin').val();
var subject = $('#sendReschedBs').find('#subject').val();
var user_id = $('#sendReschedBs').find('#user_id').val();
var email_type = $('#sendReschedBs').find('#email_type').val();
var training_id = $('#sendReschedBs').find('#training_id').val();

$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/dashboard/mailer',
    type: 'post',
    data: { training_id: training_id, user_id: user_id, to: to, body: email_body, type: type, from: from, subject: subject, email_type: email_type },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#sendReschedBs').modal('hide');
        alert('Email sent successfully');
        $('.wait-status').html('');
        //location.reload();

      }
    }
  });
});

$('#presched-em').click(function(){
var email_body = CKEDITOR.instances['email-body-presched-em'].getData();
var to = $('#sendTrainingEmail').find('#to').val();
var type = $('#sendTrainingEmail').find('#email_type').val();
var from = $('#sendTrainingEmail').find('#from_admin').val();
var subject = $('#sendTrainingEmail').find('#subject').val();
var user_id = $('#sendTrainingEmail').find('#user_id').val();
var email_type = $('#sendTrainingEmail').find('#email_type').val();

$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/dashboard/mailer',
    type: 'post',
    data: { user_id: user_id, to: to, body: email_body, type: type, from: from, subject: subject, email_type: email_type },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#sendTrainingEmail').modal('hide');
        //alert('Email sent successfully');
        $('#message-alert').html('Email has been sent').show().delay(3000).hide(0);
        $('.wait-status').html('');
        //location.reload();

      }
    }
  });
});

$('#resend_cv_email').click(function(){
var email_body = CKEDITOR.instances['email-body-resend-cv'].getData();
var to = $('#sendResendModal').find('#to').val();
var type = $('#sendResendModal').find('#email_type').val();
var from = $('#sendResendModal').find('#from_admin').val();
var subject = $('#sendResendModal').find('#subject').val();
var user_id = $('#sendResendModal').find('#user_id').val();
var email_type = $('#sendResendModal').find('#email_type').val();

$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/dashboard/mailer',
    type: 'post',
    data: { user_id: user_id, to: to, body: email_body, type: type, from: from, subject: subject, email_type: email_type },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        $('#sendResendModal').modal('hide');
        alert('Email sent successfully');
        $('.wait-status').html('');
        //location.reload();

      }
    }
  });
});


$('[id="sender4"]').click(function(){

var parent_div = '';

if($(this).attr('rel') === 'congratulatory'){
  parent_div = $('#sendCongratulatoryEmailModal');
  var email_body = CKEDITOR.instances['email-body-6'].getData();

}else  if($(this).attr('rel') === 'ta-incom'){
  parent_div = $('#sendTaIncompleteSchedModal');
  var email_body = CKEDITOR.instances['email-body-taincom'].getData();

}else  if($(this).attr('rel') === 'ta-comsched'){
  parent_div = $('#sendTaCompleteSchedModal');
  var email_body = CKEDITOR.instances['email-body-tacom'].getData();

}else  if($(this).attr('rel') === 'ta-inc'){
  parent_div = $('#sendTaIncompleteEmailModal');
  var email_body = CKEDITOR.instances['email-body-tain'].getData();

}else{
  parent_div = $('#sendRedolModal');
  var email_body = CKEDITOR.instances['email-body-7'].getData();
}

var to = parent_div.find('#to').val();

var email_type = parent_div.find('#email_type').val();
var user_id = parent_div.find('#user_id').val();

$('.wait-status').html('sending email... please wait... ');

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/home-evaluation-record/send_email',
    type: 'post',
    data: { to: to, type:  email_type, email_body: email_body, user_id: user_id },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        //$(parent_div).modal('hide');
        $('#message-alert').html('Email has been sent').show().delay(3000).hide(0);
        $('.wait-status').html('');
        parent_div.find('#email_dismiss').click();
        //location.reload()
        //$(el).parent().parent().hide('slow').remove();
      }else{
        $('.wait-status').html('');
      }

    }
  });
});

$('#sendReferenceEmailModal').on('shown.bs.modal', function () {
  CKEDITOR.replace('email-body-t');
});

$('#checkinModal').on('shown.bs.modal', function () {
if($('#va-popup-client option').length <= 0){
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/placement-record/get_clients',
      type: 'post',
      data: { id: 0 },
      success: function(data) {
        //console.log(data);
        $('[id="va-popup-client"]').append('<option disabled selected>Choose Client</option>' + data);
      }
    });
  }
});

var checkin_row_index = '';

$(document).on('click', '#va_checkin_file', function(){
  checkin_row_index = $(this).parent('div').parent('td').parent('tr').index();
  //console.log(checkin_row_index);
});

$('#create_checkin_file').click(function(){
$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/hr-operation-record/va-checkin',
    type: 'post',
    data: { id: $('#client_id').val(), vaname: $('#vaname').val(), client: $('#va-popup-client option:selected').text(), status: $('#va_status').val(), strength: $('#strength').val(), opportunity: $('#opportunity').val(), action: $('#action').val() },
    success: function(data) {
      //console.log(data);
      if(data != '1'){
        var file_link = '<div class="btn-group btn-group-sm mx-auto" role="group" aria-label="File"><input type="hidden" name="file[]" id="file" value="'+data+'"><a title="view" class="btn btn-success" href="/form_templates/'+data+'" target="_blank"><i class="far fa-file-pdf"></i></a><a class="btn btn-red" href="javascript:;" id="va_checkin_file-delete" rel="" title="remove"><i class="fas fa-times-circle"></i></a></div>';
        $('#checkin_tbl').find('tr:eq('+checkin_row_index+')').find('td:eq(3)').html(file_link);

        $('#checkinModal').modal('hide');
      }
    }
  });
});

$(document).on('click', '#va_checkin_file-delete', function(){
  if (confirm("Remove this file?")) {
    var curr_row = $(this).parent('div').parent('td');
    curr_row.find('div').remove();
    curr_row.html('<div class="btn-group btn-group-sm mx-auto" role="group" aria-label="File"><input type="hidden" name="file[]" id="file" value=""><a class="btn btn-primary" title="create" href="javascript:;" id="va_checkin_file" data-toggle="modal" data-target="#checkinModal"><i class="far fa-edit"></i></div>');
  }
});

$('body').on('click', '#collapse-cursor', function(e) {
  $('.td-collapse').find('#custom_collapse').slideUp();

  var current = $(this).next('#custom_collapse');
  if (current.is(':visible')) {
    current.slideUp();
  } else {
    current.slideDown();
  }

});

//create new batch record
$('#batchModal').on('shown.bs.modal', function (e) {
  $('.dpk-batch').Zebra_DatePicker({
    format: 'F d, Y',
    direction: 0,
    show_icon: false,
    default_position: 'below',
    first_day_of_week : 0,
    enabled_minutes: [0, 15, 30, 45, 60]
  });

  $('.dpk-batch-time').Zebra_DatePicker({
    format: 'h:i a',
    direction: 0,
    show_icon: false,
    default_position: 'below',
    enabled_minutes: [0, 15, 30, 45, 60]
  });
});

$(document).on('click', '#edit-batch', function(){
  var el = $(this);
  var tr_id = el.parent('td').parent('tr').index() + 1;
  var batch_num = $('#data-batches').find('tr:eq('+tr_id+')').find('td:eq(1)').text();
  var start_date = $('#data-batches').find('tr:eq('+tr_id+')').find('td:eq(2)').text();
  var end_date = $('#data-batches').find('tr:eq('+tr_id+')').find('td:eq(3)').text();
  var time_str = start_date.split(" ");
  var time_str_end = end_date.split(" ");

  //alert(time_str[3]);

  $('#batchModal').modal('show');

  $('#batch_num').val(batch_num);
  $('#start_date').val(time_str[0] + ' ' + time_str[1] + ' ' + time_str[2]);
  $('#end_date').val(time_str_end[0] + ' ' + time_str_end[1] + ' ' + time_str_end[2]);
  $('#start_time').val(time_str[3] + ' ' + time_str[4]);
  $('#end_time').val(time_str[6] + ' ' + time_str[7]);

});

$('#va_level').change(function(){
var program = $(this).val();

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/pre-training-requirements/batch-get',
    type: 'post',
    data: { program: program },
    success: function(data) {
      console.log(data);
      $('#batch_id').html('');
      var options = '<option value="0" selected disabled>Please change Batch ID</option>';
      $.each( data, function( key, value ) {
        //console.log( key + ": " + value.batch_number );
        options += '<option rel="'+value.start_date+'|'+value.end_date+'" value="'+value.id+'">'+value.batch_number+'</option>'
      });

      $('#batch_id').html(options);
      $('#tr-start-date').val('');
      $('#tr-end-date').val('');
      $('#trstart-date').val('');
      $('#trend-date').val('');
    }
  });

});

$('#batch_id').change(function(){
  var date = $(this).children("option:selected").attr('rel').split('|');
  var prog = $(this).children("option:selected").text();

  $('#tr-start-date').val(date[0]);
  $('#tr-end-date').val(date[1]);

  $('#trstart-date').val(date[0]);
  $('#trend-date').val(date[1]);

  $('[id="tr-start"]').text(date[0]);
  $('[id="tr-end"]').text(date[1]);
  $('[id="tr-program"]').text(prog);
});

$('#popupTrainingResched').click(function(){

  var date = $('#batch_id').children("option:selected").attr('rel').split('|');
  var prog = $('#batch_id').children("option:selected").text();
  $('[id="tr-start"]').text(date[0]);
  $('[id="tr-end"]').text(date[1]);
  $('[id="tr-program"]').text(prog);

  CKEDITOR.instances.email_body_resend_resched.setData($('#email_body_resend_resched').val());

  $('#popupTrainingReschedModal').modal('show');
});

$('#save_new_batch').click(function(){
var batch_num = $('#batch_num').val();
var start_date = $('#start_date').val();
var end_date = $('#end_date').val();
var start_time = $('#start_time').val();
var end_time = $('#end_time').val();
  $.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/pre-training-requirements/batch',
    type: 'post',
    data: { batch_num: batch_num, start_date:  start_date, end_date: end_date, start_time: start_time, end_time: end_time },
    success: function(data) {
      //console.log(data);
      if(data == '1'){
        alert('New batch date added successfully');
        location.reload();
      }else{
        alert(data);
      }
    }
  });
});


$('#rating_toggler').click(function(){
  if($('#toggle_ratings').css('display') == 'none')
  {
    $('#toggle_ratings').css('display', 'block');
  }else{
    $('#toggle_ratings').css('display', 'none');
  }
});

$('#send_bulk_mail').click(function(){
$('#info-bulk').html('please wait...');

var sendto = [];
var id = $('#batch_num_p2').val();

$.each($("input[id='defaultCheck1']:checked"), function(){
  sendto.push($(this).val());
});

$.ajax({
headers: {
  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
  url: '/admin/pre-training-requirements/batch-email',
  type: 'post',
  data: { id: id, recipients: sendto },
  success: function(data) {
    //console.log(data);
    if(data == '1'){
      alert('Batch email sent successfully');
      $('#info-bulk').html('');
      location.reload();
    }else{
      alert(data);
    }
  }
});

});

$(document).on('click', '#destroy-batch', function(){
var id = $(this).attr('rel');

if (confirm('Delete this item?')) {
  $.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/pre-training-requirements/destroy-batch',
    type: 'post',
    data: { id: id },
    success: function(data) {
      alert('Batch removed successfully');
      location.reload();
    }
  });
}
});

$('#create-new-batch-record').click(function(){
  $('#batch_num').val('');
  $('#start_date').val('');
  $('#end_date').val('');
});

$(document).on('click', '#view-batch', function(){
var tr_id = $(this).parent('td').parent('tr').index() + 1;
var id = $(this).attr('rel');
var batch_num = $('#data-batches').find('tr:eq('+tr_id+')').find('td:eq(1)').text();
var start_date = $('#data-batches').find('tr:eq('+tr_id+')').find('td:eq(2)').text();
var end_date = $('#data-batches').find('tr:eq('+tr_id+')').find('td:eq(3)').text();
$('#batch_num_p2').val(batch_num);
$('#batch_id').val(id);
$('#start_date_p2').val(start_date);
$('#end_date_p2').val(end_date);

$('#all_trainees').html('loading trainees... please wait....');

  $.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/pre-training-requirements/batch-trainers',
    type: 'post',
    data: { batch_num: batch_num },
    success: function(data) {
      //console.log(data);
      $('#all_trainees').html(data);
      //alert('Batch removed successfully');
      //location.reload();
    }
  });
  $('#viewBatchModalCenter').modal('show');
});

$('#batches-tab').on('shown.bs.tab', function (e) {
  $('#data-batches').DataTable({
    heightStyle: "content",
    processing: true,
    serverSide: true,

    "bDestroy": true,
    scrollY: "300px",
    scrollX: true,
    scrollCollapse: true,
    paging: true,
    fixedColumns:   {
      leftColumns: 2,
    },
    ajax: {
      url: '/admin/pre-training-requirements/batches',
      dataType: 'json',
      cache:false,
      type: 'GET',
    },
    "aaSorting": [],
    "order": [[ 0, "desc" ]],
    "columnDefs": [
      {
        "targets": 6,
        "data": "id",
        "render": function ( data, type, row, meta ) {
          return '<a class="txt-green" rel="'+data+'" id="view-batch" title="View / send email"><i class="far fa-file-alt"></i></a><a rel="'+data+'" id="edit-batch" href="#" title="Edit" class=""><i class="fas fa-edit"></i></a>&nbsp;<a href="#" title="Delete" class="text-red" id="destroy-batch" rel="'+data+'"><i class="fas fa-times"></i></a>';
        },
      },
    ],
    "columns": [
      { data: 'id', name: 'id' },
      { data: 'batch_number', name: 'batch_number' },
      { data: 'start_date', name: 'start_date' },
      { data: 'end_date', name: 'end_date' },
      { data: 'number_trainees', name: 'number_trainees' },
      { data: 'email_sent', name: 'email_sent' },

    ]
  });
});

$('#btn-new-application').click(function(){
var els = $('#applicationModal').find('input, select, textarea');

  if($('#full_name').val() == ''){
  alert('Applicant name is required.');
  }else if($('#email').val() == ''){
  alert('Email is required');
  }else if(!isValidEmail($('#email').val())){
  alert('Email is not valid');
  }else if($('#mobile_number').val() == ''){
  alert('Mobile is required');
  }else{
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/api/create',
      type: 'post',
      data: els.serialize(),
      success: function(data) {
        //console.log(data);

        if(data == '1'){
          $('#applicationModal').modal('hide');
          alert('New applicant saved successfully');
          location.reload();
        }else{
          alert('An error occured while processing request. Please contact Newton');
        }
      }
    });
  }
});

$(document).on('change', '#batchid-new', function(){
var id = $(this).val();

$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/pre-training-requirements/get-batch-details',
    type: 'post',
    data: { id: id },
    success: function(data) {
      $('.dd1').val(data['start']);
      $('.dd2').val(data['end']);
    }
  });

});

$(document).on('click', '.checkin-info-delete', function(){
  var el = $(this);
  $(el).parent().parent().hide('slow').remove();
});

$(document).on('click', '#checkin-info-delete', function(){
var id = $(this).attr('rel');
var el = $(this);

if (confirm('Delete this item?')) {
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/hr-operation-record/delete-checkin',
      type: 'post',
      data: { id: id },
      success: function(data) {
        //console.log(data);
        if(data == '1'){
          $(el).parent().parent().hide('slow').remove();
        }else{
          alert('An error occured while processing request. Please contact Newton');
        }
      }
    });
  }

  $('ul.dropdown-menu').on("click", function(e){
    e.stopPropagation();
    e.preventDefault();
  });

});

$('#date-log-picker').datetimepicker({
  inline: true,
  sideBySide: false,
});

$('#date-log-picker').on("dp.change",function (e) {
  var cdate = $('#date-log-picker').data('DateTimePicker').date();
  var dLogTable = $('#data-admin-log').DataTable();
  var ddate = cdate.format('YYYY-MM-DD');
  dLogTable.search(ddate).draw();
  //alert(ddate);
});

$(document).on('click', '#del-app-rec', function(e){
e.preventDefault();
var curloc = $(location).attr("href");
var id = $(this).attr('rel');
var url = $(this).attr('href');
if (confirm('Delete this item?')) {
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: url,
      type: 'get',
      data: { id: id },
      success: function(data) {
        //console.log(data);
        if(data == '1'){
          alert('Applicant record deleted');
          if(curloc.indexOf('/hr-operation-record')){
            location.reload();
          }else{
            location.href = '/admin/sourcing-record';
          }

        }
      }
    });
  }
});

$(document).on('change', '#change-cv', function(){
  var formdata = new FormData();
  if($(this).prop('files').length > 0)
  {
    file = $(this).prop('files')[0];
    formdata.append("cv_file", file);
    formdata.append("applicant_id", $('[name="sourcing_applicant_id"]').val());
  }

  $.ajax({
    url: '/admin/sourcing-record/cv_upload',
    type: "POST",
    data: formdata,
    processData: false,
    contentType: false,
    success: function (result) {
      console.log(result);
    }
  });

});

$('#enable-upload').click(function(){
  if (confirm('Warning! \r\nEnabling file upload and clicking Choose File button will delete current cv file on the server and will be replaced by the new file. Continue?')) {
    $('#change-cv').prop("disabled", false);
  }
});

$('#data-admin-log').DataTable({
  heightStyle: "content",
  processing: true,
  serverSide: true,

  paging: true,
  ajax: {
    url: '/admin/admin-log-load',
    dataType: 'json',
    cache:false,
    type: 'GET',
  },
  "aaSorting": [],
  "order": [[ 0, "desc" ]],
  "columns": [
    { data: 'date_created', name: 'date_created' },
    { data: 'admin', name: 'admin' },
    { data: 'activity', name: 'activity' },
  ]
});

$('.custom-vert-menu .nav-item a').click(function(){
var dtype = $(this).attr('rel');
$('.custom-vert-menu .nav-item a').removeClass('active');
$(this).addClass('active');
$('.eml-body').html('&nbsp;loading...');
$.ajax({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    url: '/admin/outbox-type',
    type: 'post',
    data: { type: dtype },
    success: function(data) {
      console.log(data);
      $('.outbox-ul').html('');
      var html = '';
      $.each(data.data, function(index, value){
        var timestamp = value.date_sent;
        var recipient = value.sent_to.name ? value.sent_to.name.toUpperCase() : '';
        var email = value.sent_to.email;
        var formattedDate = moment(timestamp).local().add(8, 'hours').format("MMM D, YYYY hh:mm a");
        html += '<li class="summary-li" rel="'+value.id+'"><small class="small-gray">'+formattedDate+'</small><br><span style="color: #546e7a; display: inline-block;">'+recipient+'</span> ['+email+']'+'<br>'+value.subject+'</li>';
      });

      $('.outbox-ul').html(html);

      if(data == ''){
        html = '&nbsp;&nbsp;no emails found';
        $('.eml-body').html('&nbsp;&nbsp;no emails found');
      }

      $('.outbox-ul').html(html);
      $('.outbox-ul li').first().trigger('click');

    }
  });
});

$('#btn-add-he-note').click(function(){
  $('#dbr').append('<div style="cursor: pointer; border-radius: 3px; background: #fafafa; padding: 6px; margin-bottom: 10px;"><span class="text-muted"></span> <span class="text-muted float-right pr-2" id="hr-del"><i class="fas fa-times"></i></span><div><textarea class="form-control" name="notes[]" id="hrnote"></textarea></div></div><p id="dbr"></p>');
});

$(document).on('click', '#hr-del', function(){
  $(this).parent().remove();
});

$(document).on('click', '.summary-li', function(){
  var id = $(this).attr('rel');
  $('.summary-li').removeClass('li-active');
  $(this).addClass('li-active');
  $('.eml-body').html('&nbsp;loading...');

  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/outbox-body',
      type: 'post',
      data: { id: id },
      success: function(data) {
        //console.log(data);
        $('.eml-body').html('');
        var timestamp = data.date_sent;
        var formattedDate = moment(timestamp).local().add(8, 'hours').format("MMM D, YYYY hh:mm a");
        var header = '<p>Date: '+formattedDate+'<br>';
        header += 'Sender: '+data.sender+'<br>';
        header += 'Sent by: '+data.sender_name.first_name+' '+data.sender_name.last_name+'</p><hr>';
        $('.eml-body').html(header + data.body);
      }
    });
  });
  //end admin log
});


function isValidEmail(email)
{
  return /^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test(email)
  && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(email);
}

function setCookie(key, value, expiry) {
  var expires = new Date();
  expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
  document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
  var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
  return keyValue ? keyValue[2] : null;
}

function eraseCookie(key) {
  var keyValue = getCookie(key);
  setCookie(key, keyValue, '-1');
}
