$(function () {
  var curdate = moment().format("YYYY-MM-DD");
  load_calendar(curdate);
  load_recruiters();

  $(document).on('click', '#del-sked', function(){
    var el = $(this);
    var id = el.attr('rel');

    if (confirm('Deleting this schedule will also delete the interview data. Continue to delete this schedule?')) {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: '/api/delete_schedule',
          type: 'post',
          data: {id: id},
          success: function(data) {
            //console.log(data);
            if(data == '1'){
              el.parent().remove();
            }
            //$(this).parent().remove();
          }
        });
      }
    });

    $('#today').click(function(){
      $('#datetimepicker12').data("DateTimePicker").date(new Date());
    });

    $('#tomorrow').click(function(){
      var tomorrow = moment().add(1, 'day').endOf('day');
      $('#datetimepicker12').data("DateTimePicker").date(tomorrow);
    });

    $('#prev').click(function(){
      var date_new = $("#datetimepicker12").data("DateTimePicker").date();
      var prev = moment(date_new).subtract(1, 'days');
      $('#datetimepicker12').data("DateTimePicker").date(prev);
    });

    $('#next').click(function(){
      var date_new = $("#datetimepicker12").data("DateTimePicker").date();
      var prev = moment(date_new).add(1, 'days');
      $('#datetimepicker12').data("DateTimePicker").date(prev);
    });


    $('#set_sked').click(function(){
      var from = $('#from-hour').val() + ':' + $('#from-min').val() + ' ' +$('#from-tod').val();
      var to = $('#to-hour').val() + ':' + $('#to-min').val() + ' ' +$('#to-tod').val();
      var dte = $('#date-to').text();
      var recruiter = $('#recruiters_opt').val();

      var format = 'hh:mm:ss a';
      beforeTime = moment(from, format).valueOf();
      afterTime = moment(to, format).valueOf();
      var error =  0;

      $("li#current #badge").each(function(index, v) {
        //console.log($(this).attr('rel'));
        //console.log($(this).parent('p').parent().find('#tl').text());
        var format = 'hh:mm:ss a';
        var time = moment($(this).parent('p').parent().find('#tl').text(), format).valueOf();
        var recid = $(this).attr('rel');

        if(time >= beforeTime && time <= afterTime){
          //console.log(recid);
          if(recid == recruiter){
            $(this).parent('p').parent().addClass('current');
            error += 1;
          }
        }

      });

      //console.log(error);

      if(error > 0){
        alert('timeslot has been occupied. please choose another');
      }else{
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '/admin/set_sched_by_recruiter',
            type: 'post',
            data: {from: from, to: to, date_to: dte, recruiter_id: recruiter},
            success: function(data) {
              if(data == '1'){
                var date_new = $("#datetimepicker12").data("DateTimePicker").date();
                //$("#today").click();
                $('#datetimepicker12').data("DateTimePicker").date(date_new);
              }
            }
          });
        }


      });

      $('#datetimepicker12').datetimepicker({
        inline: true,
        sideBySide: false,
        format: 'DT',
      });

      $('#close-option').click(function(){
        $('#date-to').text('');
        $('.date-options').hide();
      });

      $('#cancel_sked').click(function(){
        $('#close-option').click();
      });

      var i = 0, timeOut = 0;

      $(".datepicker999 .datepicker-days").on('mousedown touchstart', 'td.day', function () {
        var dd = $(this).data('day');
        timeOut = setInterval(function(){
          clearInterval(timeOut);

          $('.date-options').show('slow');

          $('#date-to').text(dd);
        }, 300);
      }).bind('mouseup mouseleave touchend', function() {
        //$(this).removeClass('active');
        clearInterval(timeOut);
      });

      $("#datetimepicker12").on("dp.change", function (e) {
        var date = e.date;//e.date is a moment object
        var target = $(e.target).attr('name');

        var format = 'YYYY-MM-DD';
        current = moment(date, format).format(format);

        //alert(current);
        load_calendar(current);
      });

      function load_calendar(current){
        var id = '0';
        //var current_date = moment().format("YYYY-MM-DD");
        //alert(current_date);
        var timeline_loaded = false;
        $('ul.timeline').html('');
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '/admin/load_interview_schedules',
            type: 'post',
            data: { id: id, date: current},
            success: function(data) {
              //console.log(data);
              timeline_loaded = false;
              $('ul.timeline').append(data);
              timeline_loaded = true;
              var ttl = 0;

              setTimeout(function(){
                if(timeline_loaded == true){
                  //var time = moment().format('hh:mm a');
                  $("#current a").each(function(n) {
                    var me = $(this);
                    //alert($(this).text());
                    var format = 'hh:mm:ss a';
                    var time = moment(),
                    beforeTime = moment($(this).text().trim(), format),
                    afterTime = moment($(this).text().trim(), format).add(15, 'minutes');
                    //var testt = moment('10:35 am', format);

                    if (time >= beforeTime && time < afterTime) {
                      //alert('yes');
                      me.parent('li').addClass('current');
                      me.focus();
                    }

                    ttl += parseInt(me.parent('li').find('#dcount').text());
                  });

                  $('.total').text(ttl);
                }
              },1000);

            }
          });

        }

        function load_recruiters(){

          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              url: '/admin/load_recruiters',
              type: 'post',
              data: {id: '1'},
              success: function(data) {
                var $li = '';
                var $options = '';
                var $buttons = '';

                //$('.recruiters-list').append(data);

                $.each(data, function(i, item) {
                  //console.log(item);
                  $li += '<li><i class="fas fa-circle text-'+item[2]+'"></i>&nbsp;&nbsp;'+item[1]+'</li>';
                  $options += '<option value="'+item[0]+'">'+item[1]+'</option>';
                  $buttons += '<a href="javascript:;" id="recruiter_modal" rel="'+item[0]+'" class="recruiter_modal btn btn-small btn-'+item[2]+'"><i class="fas fa-calendar-alt"></i>&nbsp;&nbsp;'+item[1]+'</a>';

                });
                $('#btns-recruiters').append($buttons);
                $('#recruiters_opt').append($options);
                $('.recruiters-list').append($li);
              }
            });
          }



          //end here
        });
