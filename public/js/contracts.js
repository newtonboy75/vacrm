$(document).on('change', '#esig_contract', function(){
  var empid = $('#contract_empid').val();
  var contract_type = $(this).val();

  if(contract_type == 'caf'){
    var empid = $('#contract_empid').val();
    var memoid = $(this).parent('td').attr('rel');
    window.location.href = '/admin/hr-operation-record/caf/'+empid+'/'+memoid;
  }else if(contract_type == 'va_final_notice'){
    window.location.href = '/admin/hr-operation-record/esig_final_notice/'+empid;

    /*$('#boxFinalModal').modal('show');

    CKEDITOR.replace('ebody', {
    toolbar: 'Custom', //makes all editors use this toolbar
    toolbarStartupExpanded : false,
    toolbarCanCollapse  : true,
    //toolbar_Custom: [] //define an empty array or whatever buttons you want.
  });

  CKEDITOR.replace('ebody2', {
  toolbar: 'Custom', //makes all editors use this toolbar
  toolbarStartupExpanded : false,
  toolbarCanCollapse  : false,
  toolbar_Custom: [] //define an empty array or whatever buttons you want.
}); */
}else if(contract_type == 'va_placement_hold'){
  //window.location.href = '/admin/hr-operation-record/esig-hold/'+empid;
  $('#boxHoldSuspensionModal').modal('show');
  $('[id="client-lists"]').html('');
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/placement-record/get_clients',
      type: 'post',
      data: { id: 0 },
      success: function(data) {
        //console.log(data);
        $('[id="client-lists"]').append('<option disabled selected>Choose client</option>');
        $('[id="client-lists"]').append(data);
      }
    });
  }else if(contract_type == 'isa_quarterly'){
    window.location.href = '/admin/hr-operation-record/esig-isa-scorecard/'+empid+'/isa';
  }else if(contract_type == 'gva_quarterly'){
    window.location.href = '/admin/hr-operation-record/esig-isa-scorecard/'+empid+'/gva';
  }else if(contract_type == 'discoluser_form'){
    window.location.href = '/admin/hr-operation-record/disclosure-form/'+empid;
  }else if(contract_type == 'memorandum_form'){
    window.location.href = '/admin/hr-operation-record/memorandum-form/'+empid;
  }else if(contract_type == 'assurance_form'){
    window.location.href = '/admin/hr-operation-record/assurance-form/'+empid;
  }else if(contract_type == 'codeofconduct_form'){
    window.location.href = '/admin/hr-operation-record/codeofconduct-form/'+empid;
  }else if(contract_type == 'ica_form'){
    window.location.href = '/admin/hr-operation-record/ica-form/'+empid;
  }else if(contract_type == 'nca_form'){
    window.location.href = '/admin/hr-operation-record/nca-form/'+empid;
  }else if(contract_type == 'fte'){
    window.location.href = '/admin/hr-operation-record/esig_final_notice/'+empid+'/fte';
  }else if(contract_type == 'nte'){
    window.location.href = '/admin/hr-operation-record/esig_final_notice/'+empid+'/nte';
  }else if(contract_type == 'termination'){
    window.location.href = '/admin/hr-operation-record/esig_final_notice/'+empid+'/termination';
  }
  //else here http://127.0.0.1:8000/admin/hr-operation-record/13291/edit
});

$(document).on('change', '#esig_contracts', function(){
  var empid = $('#uid').val();
  var contract_type = $(this).val();
  var memoid = $(this).parent('td').attr('rel');

  //admin/esignature/ica-form/13293

  //if(contract_type == 'fte'){
    window.location.href = '/admin/esignature/'+contract_type+'/'+empid+'/'+memoid;
  //}else if(contract_type == 'nte'){
    //window.location.href = '/admin/esignature/nte-form/'+empid+'/'+memoid;
  //}else if(contract_type == 'termination'){
    //window.location.href = '/admin/esignature/termination-letter/'+empid+'/'+memoid;
  //}else if(contract_type == 'caf'){
    //window.location.href = '/admin/esignature/caf/'+empid+'/'+memoid;
  //}else if(){

  //}
});

$('[id="current_va_date"]').Zebra_DatePicker({
  format: 'M d, Y',
  direction: false,
  show_icon: false,
});

$("#contracts-tab").on('shown.bs.tab', function(e) {

  var pathArray = window.location.pathname.split('/');

  $('#data-contracts-signed').DataTable({
    heightStyle: "content",
    processing: true,
    serverSide: true,
    "bDestroy": true,
    "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
    dom: 'Blfrtip',
    scrollY: "400px",
    scrollX: true,
    scrollCollapse: true,
    paging: true,
    fixedColumns:   {
      leftColumns: 1,
    },
    ajax: {
      url: '/admin/hr-operation-record/contract_lists/'+pathArray[3],
      dataType: 'json',
      cache:false,
      type: 'GET',
    },

    "aaSorting": [],
    "order": [[ 0, "desc" ]],
    "columns": [
      { data: 'id', name: 'id' },
      { data: 'contract_id', name: 'contract_id' },
      { data: 'contract_type', name: 'contract_type' },
      { data: 'date_created', name: 'date_created' },
      { data: 'status', name: 'status' },
    ]
  });
});

//
var CSRFToken = $('meta[name="csrf-token"]').attr('content');
var server_url = window.location.host;
CKEDITOR.replace('ebody', {
  toolbar: 'Custom', //makes all editors use this toolbar
  toolbarStartupExpanded : true,
  toolbarCanCollapse  : true,
  filebrowserUploadUrl: "/admin/hr-operation-record/uploader?_token="+CSRFToken,
  //toolbar_Custom: [] //define an empty array or whatever buttons you want.
  toolbarGroups: [{
    "name": "basicstyles",
    "groups": ["basicstyles"]
  },
  {
    "name": "links",
    "groups": ["links"]
  },
  {
    "name": "paragraph",
    "groups": ["list", "blocks"]
  },

  {
    "name": "insert",
    "groups": ["insert"]
  },
  {
    "name": "styles",
    "groups": ["styles"]
  },

],
});
var server_url = window.location.host;
CKEDITOR.replace('ebody2', {

  toolbar: 'Custom', //makes all editors use this toolbar
  toolbarStartupExpanded : true,
  toolbarCanCollapse  : true,
  filebrowserUploadUrl: "/admin/hr-operation-record/uploader?_token="+CSRFToken,
  //toolbar_Custom: [] //define an empty array or whatever buttons you want.
  toolbarGroups: [{
    "name": "basicstyles",
    "groups": ["basicstyles"]
  },
  {
    "name": "links",
    "groups": ["links"]
  },
  {
    "name": "paragraph",
    "groups": ["list", "blocks"]
  },

  {
    "name": "insert",
    "groups": ["insert"]
  },
  {
    "name": "styles",
    "groups": ["styles"]
  },

],
});


$(document).on('click', '#preview', function(e){
  //e.preventDefault;
  var els = $('#tf').find('input, select, textarea');
  var name = $('#recipient').val();
  var date = $('#current_date').val();
  var body = CKEDITOR.instances['ebody'].getData();

  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '/admin/hr-operation-record/termination-form',
      type: 'post',
      data: { recipient: name, current_date: date, body: body},
      success: function(data) {
        //console.log(data);
        $('#table-holder').html('');
        $('#boxTerminationModal').modal('show');
        $('#table-holder').html(data);

      }
    });

    return false;
  });

  



  $('#preview_send_final').click(function(){
    alert('test');
    var els = $('#final_body').find('input, select, textarea');
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/admin/hr-operation-record/send_final_notice',
        type: 'post',
        data: els.serialize(),
        success: function(data, request, textStatus) {
          var link = document.createElement("a");
          link.download = data[0];
          link.href = data[1];
          link.click();

        }
      });
    });
