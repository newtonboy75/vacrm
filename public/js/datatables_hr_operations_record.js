
$('#data-hr').DataTable({
  pageLength:25,
  heightStyle: "content",
  select: 'single',
  processing: true,
  serverSide: true,

  "bDestroy": true,
  "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
  deferRender: true,
  scroller: false,
  scrollCollapse: false,
  paging: true,
  dom: 'Blrtip',
  language: {
        "processing": "Loading. Please wait..."
    },
  ajax: {
       url: '/admin/hr-operation-record/load',
       dataType: 'json',
       cache:false,
       type: 'GET',
       data: function ( d ) {

         if($('#all-rec'). is(":checked")){
           d.tab = ['sourcing_record', 'talent_acquisition', 'hr_record', 'reference_check', 'pre_training', 'training_record', 'placement_record'];
         }else{
           //d.tab =['placement_record'];
           d.tab = ['hr_record'];
         }

          if($('#all-emp-stat'). is(":checked")){
            d.archived = ['0', '1', '2', '3'];
          }else{
            d.archived = ['0'];
          }

          d.hr_type = $("#hr-type option").filter(":selected").val();

        }
    },
  "aaSorting": [],
  "order": [[ 0, "desc" ]],
  "columnDefs": [ {
    "targets": 5,
    "data": "id",
    "render": function ( data, type, row, meta ) {
      if(isadmin == 'yes'){
        return '<a href="/admin/hr-operation-record/'+data+'/edit" title="Edit" class="text-primary"><i class="fas fa-edit"></i></a>&nbsp;<a href="/admin/sourcing-record/'+data+'/delete" id="del-app-rec" rel="'+data+'" title="Delete record" class="text-danger"><i class="fas fa-times"></i></a>';
      }else{
        return '<a href="/admin/hr-operation-record/'+data+'/edit" title="Edit" class="btn text-blue btn-sm"><i class="fas fa-edit"></i> Edit</a>&nbsp;';
      }



    }
  } ],
  "columns": [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'date', name: 'date'},
            {data: 'status', name: 'status'},

        ]
});

var oTable = $('#data-hr').DataTable();
//$('#custom-search-input').on('keyup', function(){
  //alert('test');
  //oTable.search($(this).val()).draw();
//});
$('#custom-search-input').on( "keydown", function(event) {
      if(event.which == 13){
        oTable.search($(this).val()).draw();
      }
});

var oTable = $('#data-hr').DataTable();

$('#hr-type').change(function(){
  oTable.search('').draw();
});

$('#popupStatsModal').on('shown.bs.modal', function () {
  //alert('test');
  $('#temps_div').show();

});

$('#popupStatsModal').on('hide.bs.modal', function () {
  //alert('test');
  $('#temps_div').hide();

});
