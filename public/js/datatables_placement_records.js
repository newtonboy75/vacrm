$('#data-placement').DataTable({
  pageLength:25,
  "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
  processing: true,
  serverSide: true,

    dom: 'ltipr',
  language: {
        "processing": "Loading. Please wait..."
    },
  ajax: {
       url: '/admin/placement-record/load',
       dataType: 'json',
       cache:false,
       type: 'GET',
       data: function ( d ) {

         if($('#all-rec'). is(":checked")){
           d.tab = ['hr_record', 'placement_record', 'training_record'];
         }else{
           d.tab =['placement_record', 'hr_record'];
           //d.tab = ['hr_record'];
         }

          if($('#all-emp-stat').is(":checked")){
            d.archived = ['0', '1', '2', '3'];
          }else{
            d.archived = ['0'];
          }

          d.placement_type = $("#placement-type option").filter(":selected").val();

          //alert(d.placement_type);

        }
    },
  "aaSorting": [],
  "columnDefs": [ {
    "targets": 5,
    "data": "id",
    "render": function ( data, type, row, meta ) {
      return '<a href="/admin/placement-record/'+data+'/edit" title="Edit" class="text-primary"><i class="fas fa-edit"></i></a>&nbsp;<a href="/admin/sourcing-record/'+data+'/delete" id="del-app-rec" rel="'+data+'" title="Delete record" class="text-danger"><i class="fas fa-times"></i></a>';
    }
  },
    { "searchable": false, "targets": [1,3] } ],
  "columns": [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'program', name: 'program'},
            { data: 'status', name: 'status' },
            { data: 'client_interview_status', name: 'client_interview_status' },

        ]
});

var oTable = $('#data-placement').DataTable();
//$('#custom-search-input').on('keyup', function(){
  //alert('test');
  //oTable.search($(this).val()).draw();
//});
$('#custom-search-input').on( "keydown", function(event) {
      if(event.which == 13){
        oTable.search($(this).val()).draw();
      }
});

$('#placement-type').change(function(){
  oTable.draw();
});


$('#cl-search a').click(function(){
  $('#custom-search-input').val('');
  $('#all-rec').prop("checked", false);
  $('#all-emp-stat').prop("checked", false);
  oTable.search('').draw();
});

$.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
        if (settings.oPreviousSearch.sSearch === "") return true; // Always return true if search is blank (save processing)

        var search = $.fn.DataTable.util.escapeRegex(settings.oPreviousSearch.sSearch);
        var newFilter = data.slice();

        for (var i = 0; i < settings.aoColumns.length; i++) {
            if (!settings.aoColumns[i] == 2) {
                newFilter.splice(i, 1);
            }
        }

        var regex = new RegExp("^(?=.*?" + search + ").*$", "i");
        return regex.test(newFilter.join(" "));
    }
);

$('#popupStatsModal').on('shown.bs.modal', function () {
  //alert('test');
  $('#temps_div').show();

});

$('#popupStatsModal').on('hide.bs.modal', function () {
  //alert('test');
  $('#temps_div').hide();

});
