$(function() {

var lastTab = getCookie('last_tab');
gotoTab(lastTab);
console.log(lastTab);
        
  load_stats();
  load_stats_year();

  var current_url = window.location.pathname;

  if(current_url == '/admin/clients'){
    //$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        
    //alert($('#myTab li a').attr("href"));
    
    var curr_tab = $('#myTab li a').attr("href");
    gotoTab(curr_tab);
    
    $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
      var target = $(e.target).attr("href"); // activated tab
       gotoTab(target);
    });
  }
  
  function gotoTab(target){
       if(target == '#placement' || target == 'placement-tab'){
        process_placement();
      }else if(target == '#list'){
        load_client_lists();
      }else if(target == '#cancellation' || target == 'cancellation-tab'){
        load_cancellation_lists();
      }else if(target == '#replacement' || target =='replacement-tab'){
        load_replacement_lists();
      }else if(target == '#suspension' || target == 'suspension-tab'){
        load_suspension_lists();
      }else if(target == '#dashboard' || target == 'dashboard-tab'){
        load_stats();
        load_stats_year();
      }else if(target == '#lists' || target == 'lists-tab'){
        load_lists();
      }else if(target == 'lists-notes'){
          var dt = $('#data-client-note').dataTable();
          dt.fnDraw();
      }
 }


  function load_lists(){
    $('#data-client-lists-all').DataTable({
      heightStyle: "content",

      pageLength:50,
      processing: true,
      serverSide: true,
      "bDestroy": true,
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      scrollX: true,
      scrollCollapse: true,
      paging: true,
      filter: true,
      //"bInfo" : true,
      dom: 'Bflrtp',
      ajax: {
        url: '/admin/clients/list',
        dataType: 'json',
        cache:false,
        type: 'GET',
      },

      "order": [[ 0, "desc" ]],
      "columnDefs": [
        {
          "targets": 8,
          "data": "id",
          "render": function ( data, type, row, meta ) {
            return '<a id="edit" href="/admin/clients/client/'+data+'" title="Edit" class=""><i class="fas fa-edit"></i></a>&nbsp;<a href="#" title="Delete" class="text-red" id="destroy-client-record" rel="'+data+'"><i class="fas fa-times"></i></a> &nbsp;&nbsp;';
          },
        },
        {
          "targets": 1,
          "data": "name",
          "render": function ( data, type, row, meta ) {
            return '<a style="color: #1976d2;" id="edit" href="/admin/clients/client/'+row.id+'" title="Edit" class=""><strong>'+data+'</strong></a> &nbsp;&nbsp;';
          },
        },
      ],
      "columns": [
        { data: 'id', name: 'id' },
        { data: 'name', name: 'name' },
        { data: 'timezone', name: 'timezone' },
        { data: 'state', name: 'state' },
        { data: 'phone_number', name: 'phone_number' },
        { data: 'email', name: 'email' },
        { data: 'status', name: 'status' },
        { data: 'register_at', name: 'register_at' },
      ]
    });
  }

  function load_stats(){
    $('#data-client-stats').DataTable({
      heightStyle: "content",

      processing: true,
      serverSide: true,
      "bDestroy": true,
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      scrollY: false,
      scrollX: false,
      scrollCollapse: false,
      paging: false,
      bFilter: false,
      "bInfo" : false,
      ajax: {
        url: '/admin/clients/stats',
        dataType: 'json',
        cache:false,
        type: 'GET',
      },
      "targets": 'no-sort',

      "order": [[ 0, "desc" ]],
      "columns": [
        { data: 'year', name: 'year' },
        { data: 'pst', name: 'pst' },
        { data: 'est', name: 'est' },
        { data: 'cst', name: 'cst' },
        { data: 'mst', name: 'mst' },
        { data: 'hst', name: 'hst' },
        { data: 'total', name: 'total' },
      ],
      "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
          return typeof i === 'string' ?
          i.replace(/[\$,]/g, '')*1 :
          typeof i === 'number' ?
          i : 0;
        };

        // Total over all pages
        total = api
        .column( 6 )
        .data()
        .reduce( function (a, b) {
          return intVal(a) + intVal(b);
        }, 0 );

        // Total over this page
        pageTotal = api
        .column( 6, { page: 'current'} )
        .data()
        .reduce( function (a, b) {
          return intVal(a) + intVal(b);
        }, 0 );

        // Update footer
        $('#stat_total').html('Total:&nbsp;&nbsp;' + total + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
      }

    });
  }

  function load_stats_year(){
    var stats = $('#data-client-stats-year').DataTable({
      heightStyle: "content",
      processing: true,
      serverSide: true,
      "bDestroy": true,

      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      scrollY: false,
      scrollX: false,
      scrollCollapse: false,
      paging: false,
      bFilter: false,
      "bInfo" : false,
      ajax: {
        url: '/admin/clients/years',
        dataType: 'json',
        cache:false,
        type: 'GET',
      },
      "targets": 'no-sort',
      "bSort": false,
      "order": [[ 0, "desc" ]],
      "columns": [
        { data: 'year', name: 'year' },
        { data: 'active', name: 'active' },
        { data: 'inactive', name: 'inactive' },
        { data: 'cancelled', name: 'cancelled' },
        { data: 'suspended', name: 'suspended' },
        { data: 'completed', name: 'completed' },
        { data: 'incomplete', name: 'incomplete' },
        { data: 'placement', name: 'placement' },
        { data: 'probono', name: 'probono' },
        { data: 'timeblock', name: 'timeblock' },
        { data: 'total', name: 'total' },
      ],
      "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
          return typeof i === 'string' ?
          i.replace(/[\$,]/g, '')*1 :
          typeof i === 'number' ?
          i : 0;
        };

        // Total over all pages
        total = api
        .column( 10 )
        .data()
        .reduce( function (a, b) {
          return intVal(a) + intVal(b);
        }, 0 );

        // Total over this page
        pageTotal = api
        .column( 10, { page: 'current'} )
        .data()
        .reduce( function (a, b) {
          return intVal(a) + intVal(b);
        }, 0 );

        // Update footer
        $('#stat_total_year').html('Total:&nbsp;&nbsp;' + total + '&nbsp;&nbsp;&nbsp;');
      }

    });


  }

  function load_suspension_lists(){
    $('#data-client-suspension').DataTable({
      heightStyle: "content",
      processing: true,
      pageLength:50,
      serverSide: true,
      "bDestroy": true,
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      dom: 'Blfrtip',
      scrollY: "400px",
      scrollX: true,
      scrollCollapse: true,
      paging: true,
      fixedColumns:   {
        leftColumns: 1,
      },
      ajax: {
        url: '/admin/clients/suspension',
        dataType: 'json',
        cache:false,
        type: 'GET',
      },
      "aaSorting": [],
      "order": [[ 0, "desc" ]],
      "columns": [
        { data: 'id', name: 'id' },
        { data: 'suspension_date', name: 'suspension_date' },
        { data: 'client_info_id', name: 'client_info_id' },
        { data: 'client_name', name: 'client_name' },
        { data: 'va_name', name: 'va_name' },
        { data: 'team', name: 'team' },
        { data: 'reason', name: 'reason' },
        { data: 'form_sent', name: 'form_sent' },
      ]
    });
  }


  function load_replacement_lists(){
    $('#data-client-replacement').DataTable({
      heightStyle: "content",
      pageLength:50,
      processing: true,
      serverSide: true,
      "bDestroy": true,
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      dom: 'Blfrtip',
      scrollY: "400px",
      scrollX: true,
      scrollCollapse: true,
      paging: true,
      fixedColumns:   {
        leftColumns: 1,
      },
      ajax: {
        url: '/admin/clients/replacement',
        dataType: 'json',
        cache:false,
        type: 'GET',
      },
      "aaSorting": [],
      "order": [[ 0, "desc" ]],
      "columns": [
        { data: 'id', name: 'id' },
        { data: 'client_info_id', name: 'client_info_id' },
        { data: 'client_name', name: 'client_name' },
        { data: 'date', name: 'date' },
        { data: 'replaced_va', name: 'replaced_va' },
        { data: 'replaced_by', name: 'replaced_by' },
        { data: 'replacement_reason', name: 'replacement_reason' },

      ]
    });
  }

  function load_cancellation_lists(){
    $('#data-client-cancellation').DataTable({
      heightStyle: "content",
      pageLength:50,
      processing: true,
      serverSide: true,
      "bDestroy": true,
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      dom: 'Blfrtip',
      scrollY: "300px",
      scrollX: true,
      scrollCollapse: true,
      paging: true,
      ajax: {
        url: '/admin/clients/cancellation',
        dataType: 'json',
        cache:false,
        type: 'GET',
      },
      "aaSorting": [],
      "order": [[ 0, "desc" ]],
      "columns": [
        { data: 'id', name: 'id' },
        { data: 'cancellation_date', name: 'cancellation_date' },
        { data: 'client_name', name: 'client_name' },
        { data: 'va_name', name: 'va_name' },
        { data: 'team', name: 'team' },
        { data: 'reason', name: 'reason' },
        { data: 'sub_reason', name: 'sub_reason' },
        { data: 'form_sent', name: 'form_sent' },
      ]
    });
  }

  function process_placement(){
    $('#data-client-placements').DataTable({
      heightStyle: "content",
      select: 'single',
      processing: true,
      serverSide: true,
      pageLength:50,
      "bDestroy": true,
      "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
      scrollY: "400px",
      scrollX: true,
      deferRender: true,
      scroller: false,
      scrollCollapse: true,
      paging: true,
      dom: 'Blfrtip',
      ajax: {
        url: '/admin/clients/load',
        dataType: 'json',
        cache:false,
        type: 'GET',
      },
      "aaSorting": [],
      "order": [[ 0, "desc" ]],
      "columnDefs": [

        {
          "targets": 10,
          "data": "client_id",
          "orderable": false,
          "visible": false,
          "render": function ( data, type, row, meta ) {
            return '<span id="cid" rel="'+data+'"></span>';
          },
        },

        {
          "targets": 1,
          "data": "client_name",
          "orderable": true,
          "render": function ( data, type, row, meta ) {
            return '<a href="#" class="font-weight-bold" id="pkey">' + data + '</a>';
          },
        },


      ],
      "columns": [
        { data: 'id', name: 'id' },
        { data: 'client_name', name: 'client_name' },
        { data: 'marketing_specialist', name: 'marketing_specialist' },
        { data: 'status', name: 'status' },
        { data: 'hiring_objective', name: 'hiring_objective' },
        { data: 'contract_signed', name: 'contract_signed' },
        { data: 'current_va', name: 'current_va' },
        { data: 'employment_status', name: 'employment_status' },
        { data: 'contract_tb1', name: 'contract_tb1' },
        { data: 'current_va_start_date', name: 'current_va_start_date' },
      ]
    });
  }

  function load_client_lists(){
    $('#data-client-lists').DataTable({
      heightStyle: "content",
      pageLength:50,
      processing: true,
      serverSide: true,
      "bDestroy": true,
      "bFilter": true,
      scrollY: "300px",
      scrollX: true,
      scrollCollapse: true,
      paging: true,
      ajax: {
        url: '/admin/clients/list',
        dataType: 'json',
        cache:false,
        type: 'GET',
      },
      "aaSorting": [],
      "order": [[ 0, "desc" ]],
      "columnDefs": [
        {
          "targets": 7,
          "data": "id",
          "render": function ( data, type, row, meta ) {
            return '</a>&nbsp;<a rel="'+data+'" id="edit-client" href="#" title="Edit" class=""><i class="fas fa-edit"></i></a>&nbsp;<a href="#" title="Delete" class="text-red" id="destroy-client" rel="'+data+'"><i class="fas fa-times"></i></a>';
          },
        },
      ],
      "columns": [
        { data: 'id', name: 'id' },
        { data: 'name', name: 'name' },
        { data: 'timezone', name: 'timezone' },
        { data: 'state', name: 'state' },
        { data: 'phone_number', name: 'phone_number' },
        { data: 'email', name: 'email' },
        { data: 'register_at', name: 'register_at' },
      ]
    });
  }

  //savings
  $('#btn-add-client').click(function(){
    $('#clientPlacementModal').modal('toggle');
  });

  $('#btn-placement-client').click(function(){
    $("#placement-info-section").toggle();
  });

  $('#btn-add-cancellation').click(function(){
    $("#cancellation_section").toggle();
  });

  $('#btn-add-suspension').click(function(){
    $("#suspension_section").toggle();
  });

  $(document).on('click', '#pkey', function(){
    var tr_parent = $(this).parent('td').parent('tr').attr('data-dt-row');
    var tr_id = parseInt(tr_parent) + 1;
    var id =$('#data-client-placements').DataTable().row( $(this).parents('tr') ).data()["client_id"];

    window.location.href = "/admin/clients/client/" + id;
    //console.log(id);
  });

  $(document).on('click', '#edit-client-placement-info', function(){
    $('#clientPlacementModalLabel').text('Edit current client record');
    $('#save-action').val('edit_client_info');
    var tr_parent = $(this).parent('td').parent('tr');

    var id = tr_parent.find("td:eq(0)").text();
    var client_name = tr_parent.find("td:eq(1)").text().split(' ');
    var marketing_specialist = tr_parent.find("td:eq(2)").text();
    var client_id = tr_parent.find("td:eq(23)").text();
    var status = tr_parent.find("td:eq(3)").text();
    var start_date_va = tr_parent.find("td:eq(4)").text();
    var three_mos_contract = tr_parent.find("td:eq(5)").text();
    var designation = tr_parent.find("td:eq(6)").text();
    var contract_signed = tr_parent.find("td:eq(7)").text();
    var type_contract = tr_parent.find("td:eq(11)").text();
    var job_term = tr_parent.find("td:eq(10)").text();
    var current_va_date = tr_parent.find("td:eq(17)").text();
    var cancellation_date = tr_parent.find("td:eq(13)").text();
    var cancellation_reason = tr_parent.find("td:eq(14)").text();
    var suspension_date = tr_parent.find("td:eq(15)").text();
    var suspension_reason = tr_parent.find("td:eq(16)").text();
    var current_va = tr_parent.find("td:eq(9)").text();
    $('#va_name').val(current_va);
    $('#replacement_va').val('');
    $('#replacement_reason').val('');

    //$('#client-section').hide();
    $('.hr2').hide();
    //alert(current_va);
    $('[id="clientinfo"]').hide();
    $('#client-section').hide();
    $('#modal-title').text('Edit Client placecement info');
    $('#cancellation_section').hide();
    $('#suspension-row').hide();

    if(marketing_specialist != ''){
      $('#placement-info-section').show();
    }else{
      $('#placement-info-section').hide();
    }

    var cid = $('#data-client-placements').dataTable().fnGetData(tr_parent);

    $('#placement_id').val(id);
    $('#pl_client_id').val(cid['client_id']);
    $('#fname').val(client_name[0]).prop("disabled", false);
    $('#lname').val(client_name[1]).prop("disabled", false);
    $('#email').val(email).prop("disabled", false);
    $('#phone').val(phone).prop("disabled", false);
    $('#replacement-section').hide();

    $("#state option").filter(function() {
      return $(this).val() == state;
    }).prop("selected", true);
    $("#state").prop("disabled", false);

    $("#timezone option").filter(function() {
      return $(this).val() == timezone;
    }).prop("selected", true);
    $("#timezone").prop("disabled", false);

    $('#marketing_specialist').val(marketing_specialist);

    $("#pl_status option").filter(function() {
      return $(this).text() == status;
    }).prop("selected", true);

    $('#start_date_va').val(start_date_va);
    $('#three_mos_contract').val(three_mos_contract);

    $("#designation option").filter(function() {
      return $(this).text() == designation;
    }).prop("selected", true);

    $("#contract_signed option").filter(function() {
      return $(this).text() == contract_signed;
    }).prop("selected", true);

    $("#type_contract option").filter(function() {
      return $(this).text() == type_contract;
    }).prop("selected", true);

    $("#job_term option").filter(function() {
      return $(this).text() == job_term;
    }).prop("selected", true);

    $('#current_va_date').val(current_va_date);


    if(cancellation_date != ''){
      $('#cancellation_date').val(cancellation_date);
      $('#cancellation_reason').val(cancellation_reason);
      $('#cancellation-row').show();
      $('#cancellation_section').show();
      $('#btn-add-cancellation').hide();
    }else{
      $('#cancellation-row').hide();
      $('#cancellation_section').hide();
      $('#btn-add-cancellation').hide();
    }

    if(suspension_date != ''){
      $('#suspension_date').val(suspension_date);
      $('#suspension_reason').val(suspension_reason);
      $('#suspension_section').show();
      $('#suspension-row').show();
      $('#btn-add-suspension').hide();
    }else{
      $('#suspension_section').css('display', 'none');
      $('#suspension-row').css('display', 'none');
      $('#btn-add-suspension').css('display', 'none');
    }

    $('#placement-info-row').show();
    $('#cancellation-row').show();
    $('#suspension-row').show();

    $('#clientPlacementModal').modal('toggle');
    $("#va_list option").html('');
    $("#va_list option").html('<option disabled value="" selected>Please choose</option>');
  });

  $('#btn-add-placement-info').click(function(){
    $('#clientPlacementModal').modal('toggle');
    $("#va_list option").html('');
    $('[id="clientinfo"]').hide();
    $('#clientPlacementModalLabel').text('Add new client info');
    $('#save-action').val('add_new_client_info');

    $('#client-section').show();
    $('#placement-info-row').show();
    $('#cancellation-row').hide();
    $('#suspension-row').hide();
    $('#btn-replace-va').hide();
    $('#placement-info-section').show();
    $('#btn-placement-client').prop('disabled', true);
  });

  $('#client_list').on('change', function() {
    $('#pl_client_id').val($(this).val());
  });

  $(document).on('click', '#add-new-client-info', function(){
    $('#replacement-section').hide();
    $('#clientPlacementModalLabel').text('Add new client placement info');
    $('#save-action').val('add_client_info');
    $('#marketing_specialist').val('');
    $('#pl_status').val('');
    $('#start_date_va').val('');
    $('#three_mos_contract').val('');
    $('#designation').val('');
    $('#contract_signed').val('');
    $('#job_term').val('');
    $('#type_contract').val('');
    $('#current_va_date').val('');
    $('#cancellation_date').val('');
    $('#cancellation_reason').val('');
    $('#suspension_date').val('');
    $('#suspension_reason').val('');
    $('#placement-info-section').hide();
    $('#cancellation_section').hide();
    $('#suspension_section').hide();
    $('#placement-info-row').show();
    $('#cancellation-row').hide();
    $('#suspension-row').hide();
    $('#va_name').val('');
    $('#replacement_va').val('');
    $('#replacement_reason').val('');

    var tr_parent = $(this).parent('td').parent('tr');
    var id = tr_parent.find("td:eq(0)").text();
    var client_name = tr_parent.find("td:eq(1)").text().split(' ');
    var marketing_specialist = tr_parent.find("td:eq(7)").text();
    var email = tr_parent.find("td:eq(2)").text();
    var phone = tr_parent.find("td:eq(3)").text();
    var state = tr_parent.find("td:eq(4)").text();
    var timezone = tr_parent.find("td:eq(5)").text();
    var cid = $('#data-client-placements').dataTable().fnGetData(tr_parent);
    $('#placement_id').val(id);
    $('#pl_client_id').val(cid['client_id']);
    $('#fname').val(client_name[0]).prop("disabled", true);
    $('#lname').val(client_name[1]).prop("disabled", true);
    $('#email').val(email).prop("disabled", true);
    $('#phone').val(phone).prop("disabled", true);

    $("#state option").filter(function() {
      return $(this).val() == state;
    }).prop("selected", true);
    $('#state').prop("disabled", true);

    $("#timezone option").filter(function() {
      return $(this).val() == timezone;
    }).prop("selected", true);
    $('#timezone').prop("disabled", true);

    $('#client-section').hide();

    $('#clientPlacementModal').modal('toggle');
  });

  $('#clientPlacementModal').on('hidden.bs.modal', function (e) {
    $('#placement_id').val('');
    $('#pl_client_id').val('');
    $('#fname').val('');
    $('#lname').val('');
    $('#email').val('');
    $('#phone').val('');
    $('#state').val('AL');
    $('#timezone').val('PST');
    $('#marketing_specialist').val('');
    $('#pl_status').val('');
    $('#start_date_va').val('');
    $('#three_mos_contract').val('');
    $('#designation').val('');
    $('#contract_signed').val('');
    $('#job_term').val('');
    $('#type_contract').val('');
    $('#current_va_date').val('');
    $('#cancellation_date').val('');
    $('#cancellation_reason').val('');
    $('#suspension_date').val('');
    $('#suspension_reason').val('');
    $('#placement-info-section').hide();
    $('#cancellation_section').hide();
    $('#suspension_section').hide();
    $('#replacement_va').val('');
    $('#replacement_reason').val('');
    $('[id="clientinfo"]').show();
  });

  $('#clientPlacementModal').on('show.bs.modal', function () {
    $('#va_list').html('');
    $('#client_list').html('');
    $('#client_list').html('<option value="" selected disabled>Choose Client</option>');
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/admin/clients/get_client_lists',
        type: 'post',
        data: { id: 0 },
        success: function(data) {
          var html = ''
          //console.log(data);
          $.each(data, function(key, value) {
            $('#client_list')
            .append($("<option></option>")
            .attr("value", value.id)
            .text(value.name));
          });
        }
      });

      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: '/admin/clients/get_va_lists',
          type: 'post',
          data: { id: 0 },
          success: function(data) {
            //console.log(data);
            var html = '<option value="" disabled selected>Please Select</option>';
            $.each(data, function(key, value) {
              html += '<option value="'+value.id+'">'+value.name+'</option>';
            });

            $('#va_list').html(html);

            var va_name = $('#va_name').val();
            $("#va_list option").filter(function() {
              return $(this).text() == va_name;
            }).prop("selected", true);

            $("#va_list").prop("disabled", false);

            if($('#va_name').val() == ''){
              $('#btn-replace-va').hide();
            }else{
              $('#btn-replace-va').show();
            }

          }
        });
      });

      $('#btn-replace-va').click(function(){
        $('#replacement-section').toggle('slow');
        var options = $("#va_list").html();
        $('#replacement_va').html(options);
        $('#replacement_reason').val('');
      });

      $('#save-client-placement').click(function(){

        var id = $('#placement_id').val();
        var client_id = $('#pl_client_id').val();
        var client_name = $('#fname').val() + ' ' + $('#lname').val();
        var email = $('#email').val();
        var phone_number = $('#phone').val();
        var state = $('#state option').filter(":selected").val();
        var timezone = $('#timezone').val();
        var signup_date = $('#signup-date').val();
        var save_action = $('#save-action').val();
        var start_date_va = $('#start_date_va').val();
        var marketing_specialist = $('#marketing_specialist').val();
        var three_mos_contract = $('#three_mos_contract').val();
        var designation = $('#designation option').filter(":selected").val();
        var contract_signed = $('#contract_signed option').filter(":selected").val();
        var job_term = $('#job_term option').filter(":selected").val();
        var type_contract = $('#type_contract option').filter(":selected").val();
        var current_va_date = $('#current_va_date').val();
        var status = $('#pl_status option').filter(":selected").val();
        var current_va = $('#va_list option').filter(":selected").val();
        var replacement_va = $('#replacement_va option').filter(":selected").val();
        var replacement_reason = $('#replacement_reason').val();

        var cancellation_date = $('#cancellation_date').val();
        var cancellation_reason = $('#cancellation_reason').val();
        var sent_form = $('#sent_form').val();
        var sub_reason = $('#sub_reason').val();

        var suspension_date = $('#suspension_date').val();
        var suspension_reason = $('#suspension_reason').val();
        var sent_form_suspend = $('#sent_form_suspend option').filter(":selected").val();


        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '/admin/clients/client_placement',
            type: 'post',
            data: { id: id, client_id: client_id, client_name: client_name, email: email, phone_number: phone_number, state: state, timezone: timezone, signup_date: signup_date, start_date_va: start_date_va, marketing_specialist: marketing_specialist, three_mos_contract: three_mos_contract, job_term: job_term , designation: designation, current_va_date: current_va_date, status: status, type_contract: type_contract, cancellation_date: cancellation_date, cancellation_reason: cancellation_reason, suspension_date: suspension_date, suspension_reason: suspension_reason, contract_signed: contract_signed, save_action: save_action, current_va: current_va, sub_reason: sub_reason, sent_form: sent_form, replacement_va: replacement_va, replacement_reason: replacement_reason, sent_form_suspend: sent_form_suspend},
            success: function(data) {
              //console.log(data);
              if(data == '1'){
                alert('Info added successfully');
                var dt = $('#data-client-placements').dataTable();
                dt.fnDraw();
                var dt2 = $('#data-client-lists-all').dataTable();
                dt2.fnDraw();
                $('#btn-dismiss').click();
              }else{
                alert(data);
              }
            }
          });
        });

        $('[id="current_va_date"], [id="cancellation_date"], [id="suspension_date"], [id="three_mos_contract"], [id="signup-date"], [id="start_date_va"]').Zebra_DatePicker({
          format: 'M d, Y',
          show_icon: false,
        });


        $('#clientTab4').on('hidden.bs.modal', function (e) {
          $('#client_id').val('');
          $('#name').val('');
          $('#timezone').val('');
          $('#state').val('');
          $('#phone').val('');
          $('#email').val('');
        });


        $(document).on('click', '#del-client-info', function(){
          if(confirm('Delete record for client #' + $(this).attr('rel'))) {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/clients/destroy',
                type: 'post',
                data: { id: $(this).attr('rel') },
                success: function(data) {
                  //console.log(data);
                  if(data == '1'){
                    alert('Client record has been removed');
                    var dt = $('#data-client-placements').dataTable();
                    dt.fnDraw();
                  }
                }
              });
            }
          });

          $(document).on('click', '#destroy-client', function(){
            if (confirm('Delete client #' + $(this).attr('rel'))) {
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  url: '/admin/clients/destroy',
                  type: 'post',
                  data: { id: $(this).attr('rel') },
                  success: function(data) {
                    //console.log(data);
                    if(data == '1'){
                      alert('Client has been removed');
                      var dt = $('#data-client-lists').dataTable();
                      dt.fnDraw();
                    }
                  }
                });
              }
            });

            $(document).on('click', '#edit-client', function(){
              //alert($(this).attr('rel'));
              $('#clientPlacementModal').modal('toggle');
              $('#client-section').hide();
              $('#cancellation-row').hide();
              $('#suspension-row').hide();

              var tr_parent = $(this).parent('td').parent('tr');

              var id = tr_parent.find("td:eq(0)").text();
              var name = tr_parent.find("td:eq(1)").text();
              var tz = tr_parent.find("td:eq(2)").text();
              var state = tr_parent.find("td:eq(3)").text();
              var phone = tr_parent.find("td:eq(4)").text();
              var email = tr_parent.find("td:eq(5)").text();
              var name = name.split(' ');

              $('#pl_client_id').val(id);
              $('#save-action').val('add_new_client');

              $('#client_id').val(id);
              $('#fname').val(name[0]);
              $('#lname').val(name[1]);
              $('#timezone').val(tz);
              $('#state').val(state);
              $('#phone_number').val(phone);
              $('#email').val(email);
            });

            $('#save-new-client').click(function(){
              var id = $('#client_id').val();
              var name = $('#name').val();
              var tz = $('#timezone').val();
              var state = $('#state').val();
              var phone = $('#phone_number').val();
              var email = $('#email').val();
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  url: '/admin/clients/save',
                  type: 'post',
                  data: { id: id, name: name, timezone: tz, state: state, phone: phone, email: email },
                  success: function(data) {
                    //console.log(data);
                    if(data == '1'){
                      //location.reload()

                      alert('New client has been saved');
                      $('#client_id').val('');
                      $('#name').val('');
                      $('#timezone').val('');
                      $('#state').val('');
                      $('#phone').val('');
                      $('#email').val('');
                      $('#clientPlacementModal').modal('toggle');
                      var dt = $('#data-client-lists').dataTable();
                      dt.fnDraw();

                      var dt2 = $('#data-client-lists-all').dataTable();
                      dt2.fnDraw();
                    }
                  }
                });
                //end function
              });

              $(document).on('click', '#destroy-client-record', function(){
                if (confirm('Delete client #' + $(this).attr('rel'))) {
                  $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                      url: '/admin/clients/remove',
                      type: 'post',
                      data: { id: $(this).attr('rel') },
                      success: function(data) {
                        //console.log(data);
                        if(data == '1'){
                          alert('Client has been removed');
                          var dt = $('#data-client-lists-all').dataTable();
                          dt.fnDraw();
                        }
                      }
                    });
                  }
                });

                $('#pl_status').change(function(){
                  var opt_val = $(this).val();

                  if(opt_val == '3'){
                    $('#cancellation-row').show();
                    $('#cancellation_section').show();

                    $('#suspension-row').hide();
                    $('#suspension_section').hide();
                  }else if(opt_val == '4'){

                    $('#suspension-row').show();
                    $('#suspension_section').show();

                    $('#cancellation-row').hide();
                    $('#cancellation_section').hide();

                  }
                });

                $( function() {
                  $( '[id="current_va"]' ).autocomplete({
                    source: "/admin/clients/search",
                    minLength: 3,
                    select: function( event, ui ) {
                      //console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );
                    }
                  });

                  $( '[id="current_va_pp"]' ).autocomplete({
                    source: "/admin/clients/search",
                    minLength: 3,
                    select: function( event, ui ) {
                      //console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );
                    },
                    open: function(){
                        $(this).autocomplete('widget').css({'z-index': '999999', 'max-height': '120px', 'overflow-y': 'auto'});
                        return false;
                    },
                  });
                });

                $( function() {
                  $( "#current_va2" ).autocomplete({
                    source: "/admin/clients/search",
                    minLength: 3,
                    select: function( event, ui ) {
                      //console.log( "Selected: " + ui.item.value + " aka " + ui.item.id );
                    }
                  });
                });

                $('#save_new_info_client').click(function(){
                  var els = $('#new_record').find('input, select, textarea');
                  $('#new_alert').text('');
                  $('#new_alert').hide();
                  $('#new_error').text('');
                  $('#new_error').hide();

                  $.ajax({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                      url: '/admin/clients/new_client_info',
                      type: 'post',
                      data: els.serialize(),
                      success: function(data) {

                      if(data == '1'){
                        $('#new_alert').text('New Client info saved successfully');
                        $('#new_alert').show();
                        $('#new_error').text('');
                        $('#new_error').hide();
                        $('#newCInfo').modal('hide');
                        location.reload();
                      }else{
                        $('#new_alert').text('');
                        $('#new_alert').hide();
                        $('#new_error').text(data);
                        $('#new_error').show();
                      }

                      //console.log(data);
                      }
                    });

                });

          $(document).on('change', '#pl_status', function(){
            $('.cancelled_reason').hide();
            $('.suspended_reason').hide();

            if($(this).val() == '3'){
              $('.cancelled_reason').show();
            }else if($(this).val() == '5'){
              $('.suspended_reason').show();
            }
          });

          $(document).on('change', '#current_va', function(){
            if($(this).val().trim() !== $('#oldva').val().trim()){
              //alert('changed' + $('#oldva').val());
              $('.replacement_reason').show();
            }else{
              $('.replacement_reason').hide();
            }
            //alert('changed');
          });

          $(document).on('click', '#ui-id-1 li', function(){
            if($('#current_va').val().trim() !== $('#oldva').val().trim()){
              //alert('changed' + $('#oldva').val());
              $('.replacement_reason').show();
              $('.replacement_reason').focus();
            }else{
              $('.replacement_reason').hide();
            }
          });

          $('.notes-title').click(function(){
              $( ".notes-data" ).toggle(function(){
                if($('#c-up').is(":hidden")){
                  $('#c-up').show();
                  $('#c-down').hide();
                }else{
                  $('#c-up').hide();
                  $('#c-down').show();
                }
              });
          });


          var CSRFToken = $('meta[name="csrf-token"]').attr('content');

          CKEDITOR.replace( 'client-notes-pp', {
            height: "180px",
            filebrowserUploadUrl: "/admin/clients/uploader?_token="+CSRFToken,
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: [{
                "name": "basicstyles",
                "groups": ["basicstyles", "insert"]
              },
            ],

            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,Flash,Smiley,Iframe,InsertPre,Table'
          } );


          $('#save-note-floating').click(function(e){
            e.preventDefault();
            var note_body = CKEDITOR.instances['client-notes-pp'].getData();


            //var note_body = CKEDITOR.instances['client_notes_pp_single'].getData();

            var note_type = $('#note-type option:selected').val();
            var interaction_type = $('#interaction_type option:selected').val();
            var note_status = $('#note_status option:selected').val();
            var note_id = $('#note_id').val();
            var pp_client_id = $('#pp_client_id').val();
            var others_status = $('#note-status-others').val();
            var others_type = $('#note-type-others').val();
            var others_interaction = $('#note-interaction-others').val();
            $('#msg-note').html('');

            if($('#note-type option:selected').val() == ''){
              $('#msg-note').html('<span class="text-red">*** Note Type is required</span>');
            }else if(note_body == ''){
              $('#msg-note').html('<span class="text-red">*** Note is required</span>');
            }else if($('#interaction_type option:selected').val() == ''){
              $('#msg-note').html('<span class="text-red">*** Interaction Type is required</span>');
            }else if($('#note_status option:selected').val() == ''){
              $('#msg-note').html('<span class="text-red">*** Note Status is required</span>');
            }else{
              $('#msg-note').html('<span class="text-green">Saving... please wait...</span>');

              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  url: '/admin/clients/notes',
                  type: 'post',
                  data: { action: "save", note_id: note_id, note_type: note_type, note_body: note_body, interaction_type: interaction_type, note_status: note_status, client_id:pp_client_id, others_status: others_status, others_type: others_type, others_interaction: others_interaction },
                  success: function(data) {
                  //console.log(data);

                    if(data != '0'){
                      $('#note_id').val(data);
                      $('#msg-note').html('<span class="text-green">Note saved successfully!</span>');
                    }
                  }
                });

            }
            //return false;
            //alert('hello');
          });
          var global_note_id = '';
          $(document).on('click', '#view-note', function(){
            //alert($(this).attr('rel'));
            //var row_index = $(this).closest('tr').children('td:eq(7)').find('a').attr('rel');

            var id = $(this).attr('rel');
            global_note_id = id;
            $('#singleNoteModal').modal('show');

          });

          $('#singleNoteModal').on('hidden.bs.modal', function (e) {
            $('#singleNoteModal').find('#pp_client_id').val('');
            $('#singleNoteModal').find('#note_id').val('');
            $('#singleNoteModal').find('#note-type option:first').prop('selected',true);
            $('#singleNoteModal').find('#client-notes-pp-single').val('');
            CKEDITOR.instances.client_notes_pp_single.setData('');
            $('#singleNoteModal').find('#interaction_type option:first').prop('selected',true);
            $('#singleNoteModal').find('#note_status option:first').prop('selected',true);
          });

          $('#singleNoteModal').on('show.bs.modal', function () {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '/admin/clients/get-note',
                type: 'post',
                data: {id: global_note_id},
                success: function(data) {
                //console.log(data);
                //process here
                $('#singleNoteModal').find('#pp_client_id').val(data.client_id);
                $('#singleNoteModal').find('#note_id').val(data.id);

                $('#singleNoteModal').find('#note-type').val(data.note_type).change();

                if(data.note_type == 'others'){
                  $('#singleNoteModal').find('#note-type-others').show();
                  $('#singleNoteModal').find('#note-type-others').val(data.others_type);
                }else{
                  $('#singleNoteModal').find('#note-type-others').hide();
                }
                $('#singleNoteModal').find('#client-notes-pp-single').val(data.note);
                $('#singleNoteModal').find('#interaction_type').val(data.interaction_type).change();
                if(data.interaction_type == 'others'){
                  $('#singleNoteModal').find('#note-interaction-others').show();
                  $('#singleNoteModal').find('#note-interaction-others').val(data.others_interaction);
                }else{
                  $('#singleNoteModal').find('#note-interaction-others').hide();
                }
                $('#singleNoteModal').find('#note_status').val(data.status).change();
                if(data.status == 'others'){
                  $('#note-status-others').show();
                  $('#note-status-others').val(data.status);
                }else{
                  $('#note-status-others').hide();
                }
                CKEDITOR.instances.client_notes_pp_single.setData(data.note);

                }
            });

          });

          $(document).on('change', '#note-type', function(){
            if($(this).val() == 'others'){
              $('#note-type-others').show();
              $('#note-type-others').focus();
            }else{
              $('#note-type-others').hide();
            }
          });

          $(document).on('change', '#note_status', function(){
            if($(this).val() == 'others'){
              $('#note-status-others').show();
              $('#note-status-others').focus();
            }else{
              $('#note-status-others').hide();
            }
          });

          $(document).on('change', '#interaction_type', function(){
            if($(this).val() == 'others'){
              $('#note-interaction-others').show();
              $('#note-interaction-others').focus();
            }else{
              $('#note-interaction-others').hide();
            }
          });

          var CSRFToken = $('meta[name="csrf-token"]').attr('content');
          CKEDITOR.replace( 'client_notes_pp_single', {
            height: "180px",
            filebrowserUploadUrl: "/admin/clients/uploader?_token="+CSRFToken,
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: [{
                "name": "basicstyles",
                "groups": ["basicstyles", "insert"]
              },
            ],

            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar,Flash,Smiley,Iframe,InsertPre,Table'
          } );

          $('#edit-note-floating').click(function(e){
            e.preventDefault();
            //alert('here');
            var note_body = CKEDITOR.instances['client_notes_pp_single'].getData();
            var msg_txt = $('#singleNoteModal').find('#msg-note');
            msg_txt.html('');
            var note_type = $('#singleNoteModal').find('#note-type option:selected').val();
            var interaction_type = $('#singleNoteModal').find('#interaction_type option:selected').val();
            var note_status = $('#singleNoteModal').find('#note_status option:selected').val();
            var note_id = $('#singleNoteModal').find('#note_id').val();
            var pp_client_id = $('#singleNoteModal').find('#pp_client_id').val();
            var others_status = $('#singleNoteModal').find('#note-status-others').val();
            var others_type = $('#singleNoteModal').find('#note-type-others').val();
            var others_interaction = $('#singleNoteModal').find('#note-interaction-others').val();

            if(note_type == ''){
              msg_txt.html('<span class="text-red">*** Note Type is required</span>');
            }else if(note_body == ''){
              msg_txt.html('<span class="text-red">*** Note is required</span>');
            }else if(interaction_type == ''){
              msg_txt.html('<span class="text-red">*** Interaction Type is required</span>');
            }else if(note_status == ''){
              msg_txt.html('<span class="text-red">*** Note Status is required</span>');
            }else{
              msg_txt.html('<span class="text-green">Saving... please wait...</span>');

              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  url: '/admin/clients/notes',
                  type: 'post',
                  data: { action: "save", note_id: note_id, note_type: note_type, note_body: note_body, interaction_type: interaction_type, note_status: note_status, client_id:pp_client_id, others_status: others_status, others_type: others_type, others_interaction: others_interaction},
                  success: function(data) {
                  //console.log(data);

                    if(data != '0'){
                      msg_txt.html('<span class="text-green">Note saved successfully!</span>');
                      var dt = $('#data-client-note').dataTable();
                      dt.fnDraw();

                    }else{
                      msg_txt.html(data);
                    }
                  }
                });

            }
            //return false;
            //alert('hello');
          });

          $('#clientTabs').on('shown.bs.tab', function (e) {
            var activeTab = $(".tab-content").find(".active");
            var currId = activeTab.attr('id');
            if(currId == 'placement-note'){
              var dt = $('#data-client-note').dataTable();
              dt.fnDraw();
            }
          });

          $('#myTab').on('shown.bs.tab', function (e) {
            var activeTab = $(".tab-content").find(".active");
            var currId = activeTab.attr('id');

            //alert(currId);
            if(currId == 'notes'){
              var dt = $('#data-client-note').dataTable();

              $('#data-client-note').DataTable({
                processing: true,
                serverSide: true,
                "bDestroy": true,
                "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                deferRender: true,
                scroller: false,
                scrollCollapse: true,
                paging: true,
                dom: 'Blfrtip',
                ajax: {
                  url: '/admin/clients/note/all',
                  dataType: 'json',
                  cache:false,
                  type: 'GET',
                },
                "aaSorting": [],
                "order": [[ 0, "desc" ]],
                "columnDefs": [
                  {
                    "targets": 3,
                    "data": "note",
                    "orderable": false,
                    "visible": true,
                    "render": function ( data, type, row, meta ) {
                      return '<a href="javascript:;" id="view-note" rel="'+data[0]+'">'+data[1]+'</a>';
                    },
                  },
                  {
                    "targets": 8,
                    "data": "id",
                    "orderable": false,
                    "visible": true,
                    "render": function ( data, type, row, meta ) {
                      return '<a href="javascript:;" id="del-client-note-s" rel="'+data+'" title="delete"><i class="fas fa-times"></i></a>';
                    },
                  },
                  {
                    "targets": 7,
                    "data": "client",
                    "orderable": false,
                    "visible": true,
                    "render": function ( data, type, row, meta ) {
                      return '<a href="#" id="clientinfo" rel="'+data[0]+'" title="view">'+data[1]+'</a>';
                    },
                  },
                ],
                "columns": [
                  { data: 'id', name: 'id' },
                  { data: 'date', name: 'date' },
                  { data: 'note_type', name: 'note_type' },
                  { data: 'note', name: 'note' },
                  { data: 'interaction_type', name: 'interaction_type' },
                  { data: 'status', name: 'status' },
                  { data: 'noted_by', name: 'noted_by' },
                  { data: 'client', name: 'client' },
                ]
              });

              dt.fnDraw();
            }
          });

          $(document).on('click', '#del-client-note-s', function(){
            var id = $(this).attr('rel');
            if(confirm('Delete item #' + $(this).attr('rel'))) {
              $.ajax({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  url: '/admin/clients/destroy-note',
                  type: 'post',
                  data: { id: id },
                  success: function(data) {
                    //console.log(data);
                    if(data == '1'){
                      var dt = $('#data-client-note').dataTable();
                      dt.fnDraw();
                      alert('Note record has been removed');
                    }
                  }
                });
              }
            });

          //note myTabContent
          

});
