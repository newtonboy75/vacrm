
$('#data-eval').DataTable({
  pageLength:25,
  "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
  processing: true,
  serverSide: true,
  dom: 'ltipr',
  language: {
        "processing": "Loading. Please wait..."
    },
  ajax: {
       url: '/admin/home-evaluation-record/load',
       dataType: 'json',
       cache:false,
       type: 'GET',
       data: function ( d ) {

         if($('#all-rec'). is(":checked")){
           d.tab = ['sourcing_record', 'talent_acquisition', 'hr_record', 'reference_check', 'home_eval', 'pre_training', 'training_record', 'placement_record'];
         }else{
           d.tab =['home_eval'];
           //d.tab = ['hr_record'];
         }

          if($('#all-emp-stat'). is(":checked")){
            d.archived = ['0', '1', '2', '3'];
          }else{
            d.archived = ['0'];
          }


        }
    },
  "aaSorting": [],
  "columnDefs": [ {
    "targets": 4,
    "data": "id",
    "render": function ( data, type, row, meta ) {
      return '<a href="/admin/home-evaluation-record/'+data+'/edit" title="Edit" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</a>';
    }
  } ],
  "columns": [
            { data: 'name', name: 'name' },
            { data: 'created_at', name: 'created_at' },
            { data: 'transition_date', name: 'transition_date'},
            { data: 'home_eval_result', name: 'home_eval_result' },
            { data: 'id', name: 'id' },
        ]
});

var oTable = $('#data-eval').DataTable();
//$('#custom-search-input').on('keyup', function(){
  //alert('test');
  //oTable.search($(this).val()).draw();
//});
$('#custom-search-input').on( "keydown", function(event) {
      if(event.which == 13){
        oTable.search($(this).val()).draw();
      }
});
