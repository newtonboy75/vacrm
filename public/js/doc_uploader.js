(function() {

var CSRFToken = $('meta[name="csrf-token"]').attr('content');
//CKEDITOR.instances.ebody.setData(allbody);

CKEDITOR.replace('document_renderer', {
toolbar: 'Custom', //makes all editors use this toolbar
toolbarStartupExpanded : true,
filebrowserUploadUrl: "/admin/hr-operation-record/uploader?_token="+CSRFToken,
height: '600px',
resize_maxHeight: '600px',
resize_enabled: true
});

$(document).on('change', '.pre-training-dp', function(){
  location.href = '/admin/esignature/'+$(this).val()+'/'+$(this).attr('rel');
});

//
$('#contract_send').submit(function(e) {
    //e.preventDefault();

    var doc_rendered = CKEDITOR.instances.document_renderer.getData();
    $('#doc_body2').val(doc_rendered);
//alert(doc_rendered);

    //$('.alert-dismissible').show();
    //$(this).submit();
});


//
document.getElementById("doc-uploader")
        .addEventListener("change", handleFileSelect, false);

    function handleFileSelect(event) {
        readFileInputEventAsArrayBuffer(event, function(arrayBuffer) {
          mammoth.convertToHtml({arrayBuffer: arrayBuffer})
              .then(displayResult)
              .done();
        });
    }

    function displayResult(result) {
        //document.getElementById("output").innerHTML = result.value;
        CKEDITOR.instances.document_renderer.setData(result.value);

        //var messageHtml = result.messages.map(function(message) {
            //return '<li class="' + message.type + '">' + escapeHtml(message.message) + "</li>";
        //}).join("");

        //document.getElementById("messages").innerHTML = "<ul>" + messageHtml + "</ul>";
    }

    function readFileInputEventAsArrayBuffer(event, callback) {
        var file = event.target.files[0];

        var reader = new FileReader();

        reader.onload = function(loadEvent) {
            var arrayBuffer = loadEvent.target.result;
            callback(arrayBuffer);
        };

        reader.readAsArrayBuffer(file);
    }

    function escapeHtml(value) {
        return value
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }

//end
})();
