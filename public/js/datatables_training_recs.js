$('#data-training').DataTable({
  pageLength:25,
  "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
  processing: true,
  serverSide: true,

    dom: 'ltipr',
  language: {
        "processing": "Loading. Please wait..."
    },
  ajax: {
       url: '/admin/training-record/load',
       dataType: 'json',
       cache:false,
       type: 'GET',
       data: function ( d ) {

         if($('#all-rec'). is(":checked")){
           d.tab = ['sourcing_record', 'talent_acquisition', 'hr_record', 'reference_check', 'pre_training', 'training_record', 'placement_record'];
         }else{
           d.tab =['training_record'];
           //d.tab = ['hr_record'];
         }

          if($('#all-emp-stat'). is(":checked")){
            d.archived = ['0', '1', '2', '3'];
          }else{
            d.archived = ['0'];
          }

        }
    },
  "aaSorting": [],
  "columnDefs": [
       { "targets": [1,2,3,4,5,6,7,8], "searchable": false }
   ],
  "columnDefs": [ {
    "targets": 9,
    "data": "id",
    "render": function ( data, type, row, meta ) {
      return '<a href="/admin/training-record/'+data+'/edit" title="Edit" class="text-primary"><i class="fas fa-edit"></i></a>&nbsp;<a href="/admin/sourcing-record/'+data+'/delete" id="del-app-rec" rel="'+data+'" title="Delete record" class="text-red"><i class="fas fa-times"></i></a>';
    }
  }, {
    "targets": 3,
    "data": "training_status",
    "render": function ( data, type, row, meta ) {
      return '<div id="training-truncate" title="'+data+'">'+data+'</div>';
    }
  } ],
  "columns": [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'program', name: 'program'},
            { data: 'batch', name: 'batch' },
            { data: 'training_status', name: 'training_status' },
            { data: 'schedule', name: 'schedule' },
            { data: 'start_date', name: 'start_date' },
            { data: 'end_date', name: 'end_date' },
            { data: 'workflow_status', name: 'workflow_status' },
        ]
});

var oTable = $('#data-training').DataTable();
//$('#custom-search-input').on('keyup', function(){
  //alert('test');
  //oTable.search($(this).val()).draw();
//});
$('#custom-search-input').on( "keydown", function(event) {
      if(event.which == 13){
        oTable.search($(this).val()).draw();
      }
});

$('#cl-search a').click(function(){
  $('#custom-search-input').val('');
  $('#all-rec').prop("checked", false);
  $('#all-emp-stat').prop("checked", false);
  oTable.search('').draw();
});

$('#popupStatsModal').on('shown.bs.modal', function () {
  //alert('test');
  $('#temps_div').show();

});

$('#popupStatsModal').on('hide.bs.modal', function () {
  //alert('test');
  $('#temps_div').hide();

});
