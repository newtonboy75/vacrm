$('#data-talent').DataTable({
  language: {
        "processing": "Loading. Please wait..."
  },
  pageLength:25,
  "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
  processing: true,
  serverSide: true,

    dom: 'ltipr',
  ajax: {
       url: '/admin/talent-acquisition-record/load',
       dataType: 'json',
       cache:false,
       type: 'GET',
       data: function ( d ) {

         if($('#all-rec'). is(":checked")){
           d.tab = ['sourcing_record', 'talent_acquisition', 'hr_record', 'reference_check', 'pre_training', 'training_record', 'placement_record'];
         }else{
           d.tab ='talent_acquisition';
           //d.tab = ['hr_record'];
         }

          if($('#all-emp-stat'). is(":checked")){
            d.archived = ['0', '1', '2', '3'];
          }else{
            d.archived = ['0'];
          }

        }
    },
  "aaSorting": [],
  "aoColumnDefs": [{ 'bSortable': true, 'aTargets': [3] }],
  "columnDefs": [
       { "targets": [1,2,3,4,5], "searchable": false }
   ],
  "columnDefs": [ {
    "targets": 6,
    "data": "id",
    "render": function ( data, type, row, meta ) {
      return '<div class="width: 100px !important; margin-left: auto; margin-right: auto; text-align:center !important;"><a href="/admin/talent-acquisition-record/'+data+'/edit" title="Edit" class="text-primary"><i class="fas fa-edit"></i></a>&nbsp;<a href="/admin/sourcing-record/'+data+'/delete" id="del-app-rec" rel="'+data+'" title="Delete record" class="text-red"><i class="fas fa-times"></i></a></div>';
    }
  } ],
  "columns": [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'created_at', name: 'created_at'},
            { data: 'recruiter_id', name: 'recruiter_id' },
            { data: 'status', name: 'workflow_status'},
            { data: 'workflow_status', name: 'workflow_status' },
            { data: 'id', name: null },
        ]
});


//$('#custom-search-input').on('keyup', function(){
  //alert('test');
  //oTable.search($(this).val()).draw();
//});
var oTable = $('#data-talent').DataTable();
$('#custom-search-input').on( "keydown", function(event) {
      if(event.which == 13){
        oTable.search($(this).val()).draw();
      }
});

$('#cl-search a').click(function(){
  $('#custom-search-input').val('');
  $('#all-rec').prop("checked", false);
  $('#all-emp-stat').prop("checked", false);
  oTable.search('').draw();
});

$('#popupStatsModal').on('shown.bs.modal', function () {
  //alert('test');
  $('#temps_div').show();

});

$('#popupStatsModal').on('hide.bs.modal', function () {
  //alert('test');
  $('#temps_div').hide();

});
