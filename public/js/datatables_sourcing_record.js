var sortdata = [];

$('#data-sourcing').DataTable({
  "dom": 'ltipr',
  "bSortable" : true,
  "oLanguage": {
   "sSearch": "Quick Search ",
  },
  "language": {
   "processing": "loading. please wait..."
  },
  "pageLength":"25",
  "lengthMenu": [ [10, 30, 25, 100, -1], [10, 30, 50, 100, "All"] ],
  "processing": true,
  "serverSide": true,
  "cache": false,
    "ajax": {
       "url": '/api/load',
       "dataType": 'json',
       "cache":false,
       "type": 'GET',
       "data": function ( d ) {

         if($('#all-rec'). is(":checked")){
           d.tab = ['sourcing_record', 'talent_acquisition', 'hr_record', 'reference_check', 'pre_training', 'training_record', 'placement_record'];
         }else{
           d.tab ='sourcing_record';
         }

          if($('#all-emp-stat'). is(":checked")){
            d.archived = ['0', '1', '2', '3'];
          }else{
            d.archived = ['0'];
          }

          d.sortby = sortdata;
        }
    },



  "columnDefs": [
    {
    "targets": 9,
    "data": "id",
    "render": function ( data, type, row, meta ) {
      if(isadmin == 'yes'){
        return '<a href="/admin/sourcing-record/'+data+'/edit" title="Edit" class="text-primary"><i class="fas fa-edit"></i></a>&nbsp;<a href="/admin/sourcing-record/'+data+'/delete" id="del-app-rec" rel="'+data+'" title="Delete record" class="text-red"><i class="fas fa-times"></i></a>';
      }else{
        return '<a href="/admin/sourcing-record/'+data+'/edit" title="Edit" class="btn btn-sm text-blue"><i class="fas fa-edit"></i> Edit</a>&nbsp;';
      }

    }
  },
  { "searchable": false, "targets": [5,7] },
 ],
  "columns": [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'created_at', name: 'created_at'},
            { data: 'come_from', name: 'come_from' },
            { data: 'position', name: 'position' },
            { data: 'status', name: 'status'},
            { data: 'workflow_status', name: 'workflow_status' },
            { data: 'has_real_estate_exp', name: 'has_real_estate_exp' },
            { data: 'kw_count', name: 'kw_count' },
            { data: 'id', name: null },
        ]
});

var oTable = $('#data-sourcing').DataTable();
var order = oTable.order();
//sortdata = order;

$('#custom-search-input').on( "keydown", function(event) {
      sortdata = [];
      if(event.which == 13){
        oTable.search($(this).val()).draw();
      }
});

$('#cl-search a').click(function(){
  sortdata = [];
  $('#custom-search-input').val('');
  $('#all-rec').prop("checked", false);
  $('#all-emp-stat').prop("checked", false);
  oTable.search('').draw();
});


$('#btn-add-reload').click(function(){
  sortdata = [];
  $('#custom-search-input').val('');
  $('#all-rec').prop("checked", false);
  $('#all-emp-stat').prop("checked", false);
  oTable.search('').draw();
});

$('th').bind('click', function(){
  //console.log($(this).index());
  //console.log(order);
  sortdata = order;
});


$('#popupStatsModal').on('shown.bs.modal', function () {
  //alert('test');
  $('#temps_div').show();

});

$('#popupStatsModal').on('hide.bs.modal', function () {
  //alert('test');
  $('#temps_div').hide();

});

$(document).on('change', '.dte', function(){

  var date_from = $('#filter-date-from').val();
  var date_to = $('#filter-date-to').val();
  console.log(date_from);
  console.log(date_to);

  var url_segment = window.location.pathname.split("/").pop();
  $.getJSON('/admin/get-chart/'+date_from+'/'+date_to+'/'+url_segment, function (dataTableJson) {
    lava.loadData('BcharStat', dataTableJson, function (chart) {
      console.log(chart);
      lava.redrawCharts();
    });
  });

});
