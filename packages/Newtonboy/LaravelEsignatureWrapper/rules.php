<?php

// Available placeholders: Newtonboy, LaravelEsignatureWrapper, newtonboy, laravelesignaturewrapper
return [
    'src/MyPackage.php' => 'src/LaravelEsignatureWrapper.php',
    'config/mypackage.php' => 'config/laravelesignaturewrapper.php',
    'src/Facades/MyPackage.php' => 'src/Facades/LaravelEsignatureWrapper.php',
    'src/MyPackageServiceProvider.php' => 'src/LaravelEsignatureWrapperServiceProvider.php',
];