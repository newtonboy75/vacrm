<?php

namespace Newtonboy\LaravelEsignatureWrapper\Facades;

use Illuminate\Support\Facades\Facade;

class LaravelEsignatureWrapper extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravelesignaturewrapper';
    }
}
