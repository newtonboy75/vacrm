<?php

namespace Newtonboy\LaravelEsignatureWrapper;

use GuzzleHttp\Client;

class LaravelEsignatureWrapper
{
  //private $config;
  private $client;
  private $baseUrl;

  function __construct()
  {
    $this->api = config('app.esig')['secret_id'];
    $this->baseUrl = 'https://'.$this->api.':@esignatures.io/api/';
    $this->client = new Client(['base_uri' => $this->baseUrl, 'headers' => $this->getHeaders()]);
  }

  public function getTemplates(){
    $request = $this->client->get('templates');
    return $view = $this->rawJson($request);
  }

  public function getHeaders($accept = 'application/json', $contentType = 'application/json'){
    return array(
      'secret-token'=>$this->api,
      'Accept' => $accept,
      'Content-Type' => $contentType
    );
  }

  public function createContract($data) {
    $request = $this->client->post('contracts/', ['json' => $data]);
    return $envelope = $this->rawJson($request);
  }

  public function editContract($id, $data) {
    $request = $this->client->post('contracts/'.$id, ['json' => $data]);
    return $envelope = $this->rawJson($request);
  }

  public function getContract($id) {
    $request = $this->client->get('contracts/'.$id);
    return $envelope = $this->rawJson($request);
  }

  ///api/contracts/<contract_id>/signers/<signer_id>/send_contract
  public function sendContractRecipient($contract_id, $signer_id){
    $request = $this->client->post('contracts/'.$contract_id.'/signers/'.$signer_id.'/send_contract');
    return $envelope = $this->rawJson($request);
  }

  public function addContractRecipient($contract_id, $signer_id, $data){
    $request = $this->client->post('contracts/'.$contract_id.'/signers/'.$signer_id, ['json' => $data]);
    return $envelope = $this->rawJson($request);
  }

  ///contracts/<contract_id>/withdraw
  public function revokeContract($data) {
    $request = $this->client->post('contracts/'.$data['id'].'/withdraw', ['json' => $data]);
    return $envelope = $this->rawJson($request);
  }

  public function rawJson($response)
  {
    return json_decode($response->getBody()->getContents(), true);
  }

}
