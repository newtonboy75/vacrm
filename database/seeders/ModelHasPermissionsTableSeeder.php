<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ModelHasPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('model_has_permissions')->delete();
        
        \DB::table('model_has_permissions')->insert(array (
            0 => 
            array (
                'model_id' => 1,
                'model_type' => 'App\\Models\\Auth\\User',
                'permission_id' => 1,
            ),
            1 => 
            array (
                'model_id' => 3,
                'model_type' => 'App\\Models\\Auth\\User',
                'permission_id' => 1,
            ),
            2 => 
            array (
                'model_id' => 9,
                'model_type' => 'App\\Models\\Auth\\User',
                'permission_id' => 1,
            ),
            3 => 
            array (
                'model_id' => 10,
                'model_type' => 'App\\Models\\Auth\\User',
                'permission_id' => 1,
            ),
            4 => 
            array (
                'model_id' => 11,
                'model_type' => 'App\\Models\\Auth\\User',
                'permission_id' => 1,
            ),
            5 => 
            array (
                'model_id' => 16,
                'model_type' => 'App\\Models\\Auth\\User',
                'permission_id' => 1,
            ),
            6 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'permission_id' => 1,
            ),
            7 => 
            array (
                'model_id' => 18,
                'model_type' => 'App\\Models\\Auth\\User',
                'permission_id' => 1,
            ),
            8 => 
            array (
                'model_id' => 34,
                'model_type' => 'App\\Models\\Auth\\User',
                'permission_id' => 1,
            ),
        ));
        
        
    }
}