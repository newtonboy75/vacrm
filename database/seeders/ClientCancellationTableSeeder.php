<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClientCancellationTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('client_cancellation')->delete();
        
        \DB::table('client_cancellation')->insert(array (
            0 => 
            array (
                'cancellation_date' => '2018-05-16',
                'client_info_id' => 1,
                'form_sent' => NULL,
                'id' => 1,
                'reason' => 'Restrategizing',
                'sub_reason' => NULL,
            ),
            1 => 
            array (
                'cancellation_date' => '2017-06-01',
                'client_info_id' => 2,
                'form_sent' => NULL,
                'id' => 2,
                'reason' => 'unhappy with the results',
                'sub_reason' => NULL,
            ),
            2 => 
            array (
                'cancellation_date' => '2017-01-20',
                'client_info_id' => 3,
                'form_sent' => NULL,
                'id' => 3,
                'reason' => 'Unsatisfied with VA\'s performance',
                'sub_reason' => NULL,
            ),
            3 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 4,
                'form_sent' => NULL,
                'id' => 4,
                'reason' => 'Financial difficulty',
                'sub_reason' => NULL,
            ),
            4 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 5,
                'form_sent' => NULL,
                'id' => 5,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            5 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 6,
                'form_sent' => NULL,
                'id' => 6,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            6 => 
            array (
                'cancellation_date' => '2017-04-04',
                'client_info_id' => 7,
                'form_sent' => NULL,
                'id' => 7,
                'reason' => 'Company policy that restricts them from using an appointment setter',
                'sub_reason' => NULL,
            ),
            7 => 
            array (
                'cancellation_date' => '2017-06-17',
                'client_info_id' => 8,
                'form_sent' => NULL,
                'id' => 8,
                'reason' => 'not getting ROI',
                'sub_reason' => NULL,
            ),
            8 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 9,
                'form_sent' => NULL,
                'id' => 9,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            9 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 10,
                'form_sent' => NULL,
                'id' => 10,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            10 => 
            array (
                'cancellation_date' => '2017-01-21',
                'client_info_id' => 11,
                'form_sent' => NULL,
                'id' => 11,
                'reason' => 'Finances',
                'sub_reason' => NULL,
            ),
            11 => 
            array (
                'cancellation_date' => '2017-06-06',
                'client_info_id' => 12,
                'form_sent' => NULL,
                'id' => 12,
                'reason' => 'unhappy with the results',
                'sub_reason' => NULL,
            ),
            12 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 13,
                'form_sent' => NULL,
                'id' => 13,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            13 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 14,
                'form_sent' => NULL,
                'id' => 14,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            14 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 15,
                'form_sent' => NULL,
                'id' => 15,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            15 => 
            array (
                'cancellation_date' => '2017-03-04',
                'client_info_id' => 16,
                'form_sent' => NULL,
                'id' => 16,
                'reason' => 'Concentrated on circle prospecting',
                'sub_reason' => NULL,
            ),
            16 => 
            array (
                'cancellation_date' => '2017-04-14',
                'client_info_id' => 17,
                'form_sent' => NULL,
                'id' => 17,
                'reason' => 'Went for cheaper services',
                'sub_reason' => NULL,
            ),
            17 => 
            array (
                'cancellation_date' => '2017-03-17',
                'client_info_id' => 18,
                'form_sent' => NULL,
                'id' => 18,
                'reason' => 'Went for cheaper services',
                'sub_reason' => NULL,
            ),
            18 => 
            array (
                'cancellation_date' => '2018-02-01',
                'client_info_id' => 19,
                'form_sent' => NULL,
                'id' => 19,
                'reason' => 'Business Restructuring',
                'sub_reason' => NULL,
            ),
            19 => 
            array (
                'cancellation_date' => '2017-10-24',
                'client_info_id' => 20,
                'form_sent' => NULL,
                'id' => 20,
                'reason' => 'Unhappy with VAs performance',
                'sub_reason' => NULL,
            ),
            20 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 21,
                'form_sent' => NULL,
                'id' => 21,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            21 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 22,
                'form_sent' => NULL,
                'id' => 22,
                'reason' => 'VAs performance',
                'sub_reason' => NULL,
            ),
            22 => 
            array (
                'cancellation_date' => '2017-05-16',
                'client_info_id' => 23,
                'form_sent' => NULL,
                'id' => 23,
                'reason' => 'unhappy with VA\'s attendance',
                'sub_reason' => NULL,
            ),
            23 => 
            array (
                'cancellation_date' => '2017-09-01',
                'client_info_id' => 24,
                'form_sent' => NULL,
                'id' => 24,
                'reason' => 'VAs performance',
                'sub_reason' => NULL,
            ),
            24 => 
            array (
                'cancellation_date' => '2017-07-20',
                'client_info_id' => 25,
                'form_sent' => NULL,
                'id' => 25,
                'reason' => 'unahppy with VA\'s performance, had concerns on new CRM Sierra',
                'sub_reason' => NULL,
            ),
            25 => 
            array (
                'cancellation_date' => '2017-05-12',
                'client_info_id' => 26,
                'form_sent' => NULL,
                'id' => 26,
                'reason' => 'unhappy with VA\'s performance',
                'sub_reason' => NULL,
            ),
            26 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 27,
                'form_sent' => NULL,
                'id' => 27,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            27 => 
            array (
                'cancellation_date' => '2018-03-05',
                'client_info_id' => 28,
                'form_sent' => NULL,
                'id' => 28,
                'reason' => 'unhappy with VA\'s performance ',
                'sub_reason' => NULL,
            ),
            28 => 
            array (
                'cancellation_date' => '2017-07-07',
                'client_info_id' => 29,
                'form_sent' => NULL,
                'id' => 29,
                'reason' => 'not much work to give the VA',
                'sub_reason' => NULL,
            ),
            29 => 
            array (
                'cancellation_date' => '2017-07-13',
                'client_info_id' => 30,
                'form_sent' => NULL,
                'id' => 30,
                'reason' => 'unahppy with VA\'s performance',
                'sub_reason' => NULL,
            ),
            30 => 
            array (
                'cancellation_date' => '2017-06-23',
                'client_info_id' => 31,
                'form_sent' => NULL,
                'id' => 31,
                'reason' => 'unhappy with VA',
                'sub_reason' => NULL,
            ),
            31 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 32,
                'form_sent' => NULL,
                'id' => 32,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            32 => 
            array (
                'cancellation_date' => '2017-08-11',
                'client_info_id' => 33,
                'form_sent' => NULL,
                'id' => 33,
                'reason' => 'Business Reorganization',
                'sub_reason' => NULL,
            ),
            33 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 34,
                'form_sent' => NULL,
                'id' => 34,
                'reason' => 'financial difficulty',
                'sub_reason' => NULL,
            ),
            34 => 
            array (
                'cancellation_date' => '2017-06-08',
                'client_info_id' => 35,
                'form_sent' => NULL,
                'id' => 35,
                'reason' => 'cancelled due to attendance',
                'sub_reason' => NULL,
            ),
            35 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 36,
                'form_sent' => NULL,
                'id' => 36,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            36 => 
            array (
                'cancellation_date' => '2017-10-17',
                'client_info_id' => 37,
                'form_sent' => NULL,
                'id' => 37,
                'reason' => 'unhappy with VAs performance',
                'sub_reason' => NULL,
            ),
            37 => 
            array (
                'cancellation_date' => '2017-08-26',
                'client_info_id' => 38,
                'form_sent' => NULL,
                'id' => 38,
                'reason' => 'VAs performance',
                'sub_reason' => NULL,
            ),
            38 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 39,
                'form_sent' => NULL,
                'id' => 39,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            39 => 
            array (
                'cancellation_date' => '2017-08-17',
                'client_info_id' => 40,
                'form_sent' => NULL,
                'id' => 40,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            40 => 
            array (
                'cancellation_date' => '2017-09-06',
                'client_info_id' => 41,
                'form_sent' => NULL,
                'id' => 41,
                'reason' => 'VA performance/leads quality',
                'sub_reason' => NULL,
            ),
            41 => 
            array (
                'cancellation_date' => '2017-09-18',
                'client_info_id' => 42,
                'form_sent' => NULL,
                'id' => 42,
                'reason' => 'Previous VA performance',
                'sub_reason' => NULL,
            ),
            42 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 43,
                'form_sent' => NULL,
                'id' => 43,
                'reason' => 'VAs performance',
                'sub_reason' => NULL,
            ),
            43 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 44,
                'form_sent' => NULL,
                'id' => 44,
                'reason' => 'MIA',
                'sub_reason' => NULL,
            ),
            44 => 
            array (
                'cancellation_date' => '2017-07-28',
                'client_info_id' => 45,
                'form_sent' => NULL,
                'id' => 45,
                'reason' => 'Financial difficulty',
                'sub_reason' => NULL,
            ),
            45 => 
            array (
                'cancellation_date' => '2017-10-06',
                'client_info_id' => 46,
                'form_sent' => NULL,
                'id' => 46,
                'reason' => 'not enough leads to work on',
                'sub_reason' => NULL,
            ),
            46 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 47,
                'form_sent' => NULL,
                'id' => 47,
                'reason' => 'havent heard from her anymore',
                'sub_reason' => NULL,
            ),
            47 => 
            array (
                'cancellation_date' => '2017-09-21',
                'client_info_id' => 48,
                'form_sent' => NULL,
                'id' => 48,
                'reason' => 'Offered replacement, refused, said they\'ll just cancel the service, unhappy with VA\'s performance',
                'sub_reason' => NULL,
            ),
            48 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 49,
                'form_sent' => NULL,
                'id' => 49,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            49 => 
            array (
                'cancellation_date' => '2017-10-07',
                'client_info_id' => 50,
                'form_sent' => NULL,
                'id' => 50,
                'reason' => 'unhappy with VAs performance',
                'sub_reason' => NULL,
            ),
            50 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 51,
                'form_sent' => NULL,
                'id' => 51,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            51 => 
            array (
                'cancellation_date' => '2017-09-05',
                'client_info_id' => 52,
                'form_sent' => NULL,
                'id' => 52,
                'reason' => 'Due to Hurricane in Houston',
                'sub_reason' => NULL,
            ),
            52 => 
            array (
                'cancellation_date' => '2017-10-12',
                'client_info_id' => 53,
                'form_sent' => NULL,
                'id' => 53,
                'reason' => 'Unhappy with VA\'s Performance',
                'sub_reason' => NULL,
            ),
            53 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 54,
                'form_sent' => NULL,
                'id' => 54,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            54 => 
            array (
                'cancellation_date' => '2017-09-15',
                'client_info_id' => 55,
                'form_sent' => NULL,
                'id' => 55,
                'reason' => 'unhappy with VA\'s performance',
                'sub_reason' => NULL,
            ),
            55 => 
            array (
                'cancellation_date' => '2017-12-29',
                'client_info_id' => 56,
                'form_sent' => NULL,
                'id' => 56,
                'reason' => 'financials, company structuring',
                'sub_reason' => NULL,
            ),
            56 => 
            array (
                'cancellation_date' => '2017-12-31',
                'client_info_id' => 57,
                'form_sent' => NULL,
                'id' => 57,
                'reason' => 'business, restructuring',
                'sub_reason' => NULL,
            ),
            57 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 58,
                'form_sent' => NULL,
                'id' => 58,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            58 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 59,
                'form_sent' => NULL,
                'id' => 59,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            59 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 60,
                'form_sent' => NULL,
                'id' => 60,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            60 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 61,
                'form_sent' => NULL,
                'id' => 61,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            61 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 62,
                'form_sent' => NULL,
                'id' => 62,
                'reason' => 'Did not specify the reason',
                'sub_reason' => NULL,
            ),
            62 => 
            array (
                'cancellation_date' => '2017-12-22',
                'client_info_id' => 63,
                'form_sent' => NULL,
                'id' => 63,
                'reason' => 'Unreachable',
                'sub_reason' => NULL,
            ),
            63 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 64,
                'form_sent' => NULL,
                'id' => 64,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            64 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 65,
                'form_sent' => NULL,
                'id' => 65,
                'reason' => 'Did not continue with services',
                'sub_reason' => NULL,
            ),
            65 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 66,
                'form_sent' => NULL,
                'id' => 66,
                'reason' => 'wanted to get someone local',
                'sub_reason' => NULL,
            ),
            66 => 
            array (
                'cancellation_date' => '2018-05-15',
                'client_info_id' => 67,
                'form_sent' => NULL,
                'id' => 67,
                'reason' => 'Company Reorganization',
                'sub_reason' => NULL,
            ),
            67 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 68,
                'form_sent' => NULL,
                'id' => 68,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            68 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 69,
                'form_sent' => NULL,
                'id' => 69,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            69 => 
            array (
                'cancellation_date' => '2018-05-17',
                'client_info_id' => 70,
                'form_sent' => NULL,
                'id' => 70,
                'reason' => 'No more leads to work on',
                'sub_reason' => NULL,
            ),
            70 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 71,
                'form_sent' => NULL,
                'id' => 71,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            71 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 72,
                'form_sent' => NULL,
                'id' => 72,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            72 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 73,
                'form_sent' => NULL,
                'id' => 73,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            73 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 74,
                'form_sent' => NULL,
                'id' => 74,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            74 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 75,
                'form_sent' => NULL,
                'id' => 75,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            75 => 
            array (
                'cancellation_date' => '2019-02-06',
                'client_info_id' => 76,
                'form_sent' => NULL,
                'id' => 76,
                'reason' => 'Business restructuring',
                'sub_reason' => NULL,
            ),
            76 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 77,
                'form_sent' => NULL,
                'id' => 77,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            77 => 
            array (
                'cancellation_date' => '2018-05-03',
                'client_info_id' => 78,
                'form_sent' => NULL,
                'id' => 78,
                'reason' => 'WIll rethink their strategy',
                'sub_reason' => NULL,
            ),
            78 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 79,
                'form_sent' => NULL,
                'id' => 79,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            79 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 80,
                'form_sent' => NULL,
                'id' => 80,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            80 => 
            array (
                'cancellation_date' => '2018-03-24',
                'client_info_id' => 81,
                'form_sent' => NULL,
                'id' => 81,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            81 => 
            array (
                'cancellation_date' => '2017-12-04',
                'client_info_id' => 82,
                'form_sent' => NULL,
                'id' => 82,
                'reason' => 'Multiple issues - calendar not updated, Vulcan 7 missing logs, absence due to YEP',
                'sub_reason' => NULL,
            ),
            82 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 83,
                'form_sent' => NULL,
                'id' => 83,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            83 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 84,
                'form_sent' => NULL,
                'id' => 84,
                'reason' => 'Financial Difficulty',
                'sub_reason' => NULL,
            ),
            84 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 85,
                'form_sent' => NULL,
                'id' => 85,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            85 => 
            array (
                'cancellation_date' => '2018-01-24',
                'client_info_id' => 86,
                'form_sent' => NULL,
                'id' => 86,
                'reason' => 'Personal reason, has to attend to daughter\'s needs',
                'sub_reason' => NULL,
            ),
            86 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 87,
                'form_sent' => NULL,
                'id' => 87,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            87 => 
            array (
                'cancellation_date' => '2018-11-23',
                'client_info_id' => 88,
                'form_sent' => NULL,
                'id' => 88,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            88 => 
            array (
                'cancellation_date' => '2017-12-19',
                'client_info_id' => 89,
                'form_sent' => NULL,
                'id' => 89,
                'reason' => 'Switching to another industry',
                'sub_reason' => NULL,
            ),
            89 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 90,
                'form_sent' => NULL,
                'id' => 90,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            90 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 91,
                'form_sent' => NULL,
                'id' => 91,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            91 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 92,
                'form_sent' => NULL,
                'id' => 92,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            92 => 
            array (
                'cancellation_date' => '2018-02-09',
                'client_info_id' => 93,
                'form_sent' => NULL,
                'id' => 93,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            93 => 
            array (
                'cancellation_date' => '2018-02-12',
                'client_info_id' => 94,
                'form_sent' => NULL,
                'id' => 94,
                'reason' => 'VA Performance',
                'sub_reason' => NULL,
            ),
            94 => 
            array (
                'cancellation_date' => '2018-09-14',
                'client_info_id' => 95,
                'form_sent' => NULL,
                'id' => 95,
                'reason' => 'Hired someone locally',
                'sub_reason' => NULL,
            ),
            95 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 96,
                'form_sent' => NULL,
                'id' => 96,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            96 => 
            array (
                'cancellation_date' => '2017-11-21',
                'client_info_id' => 97,
                'form_sent' => NULL,
                'id' => 97,
                'reason' => 'unsatisfied with VA\'s performance',
                'sub_reason' => NULL,
            ),
            97 => 
            array (
                'cancellation_date' => '2018-03-10',
                'client_info_id' => 98,
                'form_sent' => NULL,
                'id' => 98,
                'reason' => 'Hiring someone locally',
                'sub_reason' => NULL,
            ),
            98 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 99,
                'form_sent' => NULL,
                'id' => 99,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            99 => 
            array (
                'cancellation_date' => '2018-02-06',
                'client_info_id' => 100,
                'form_sent' => NULL,
                'id' => 100,
                'reason' => 'Hiring someone else',
                'sub_reason' => NULL,
            ),
            100 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 101,
                'form_sent' => NULL,
                'id' => 101,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            101 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 102,
                'form_sent' => NULL,
                'id' => 102,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            102 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 103,
                'form_sent' => NULL,
                'id' => 103,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            103 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 104,
                'form_sent' => NULL,
                'id' => 104,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            104 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 105,
                'form_sent' => NULL,
                'id' => 105,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            105 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 106,
                'form_sent' => NULL,
                'id' => 106,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            106 => 
            array (
                'cancellation_date' => '2018-11-19',
                'client_info_id' => 107,
                'form_sent' => NULL,
                'id' => 107,
                'reason' => 'Financials',
                'sub_reason' => NULL,
            ),
            107 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 108,
                'form_sent' => NULL,
                'id' => 108,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            108 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 109,
                'form_sent' => NULL,
                'id' => 109,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            109 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 110,
                'form_sent' => NULL,
                'id' => 110,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            110 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 111,
                'form_sent' => NULL,
                'id' => 111,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            111 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 112,
                'form_sent' => NULL,
                'id' => 112,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            112 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 113,
                'form_sent' => NULL,
                'id' => 113,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            113 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 114,
                'form_sent' => NULL,
                'id' => 114,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            114 => 
            array (
                'cancellation_date' => '2018-04-26',
                'client_info_id' => 115,
                'form_sent' => NULL,
                'id' => 115,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            115 => 
            array (
                'cancellation_date' => '2018-02-03',
                'client_info_id' => 116,
                'form_sent' => NULL,
                'id' => 116,
                'reason' => 'Unhappy ith VA\'s Performance',
                'sub_reason' => NULL,
            ),
            116 => 
            array (
                'cancellation_date' => '2018-03-31',
                'client_info_id' => 117,
                'form_sent' => NULL,
                'id' => 117,
                'reason' => 'Business Strategy',
                'sub_reason' => NULL,
            ),
            117 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 118,
                'form_sent' => NULL,
                'id' => 118,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            118 => 
            array (
                'cancellation_date' => '2018-08-20',
                'client_info_id' => 119,
                'form_sent' => NULL,
                'id' => 119,
                'reason' => 'Business reasons- hiring a VA is not in line with her financial needs right now',
                'sub_reason' => NULL,
            ),
            119 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 120,
                'form_sent' => NULL,
                'id' => 120,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            120 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 121,
                'form_sent' => NULL,
                'id' => 121,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            121 => 
            array (
                'cancellation_date' => '2019-03-06',
                'client_info_id' => 122,
                'form_sent' => NULL,
                'id' => 122,
                'reason' => 'Business - Trasnferring RE to business parters',
                'sub_reason' => NULL,
            ),
            122 => 
            array (
                'cancellation_date' => '2018-03-09',
                'client_info_id' => 123,
                'form_sent' => NULL,
                'id' => 123,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            123 => 
            array (
                'cancellation_date' => '2018-10-05',
                'client_info_id' => 124,
                'form_sent' => NULL,
                'id' => 124,
                'reason' => 'Financials, cant afford VA for now',
                'sub_reason' => NULL,
            ),
            124 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 125,
                'form_sent' => NULL,
                'id' => 125,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            125 => 
            array (
                'cancellation_date' => '2018-05-01',
                'client_info_id' => 126,
                'form_sent' => NULL,
                'id' => 126,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            126 => 
            array (
                'cancellation_date' => '2018-07-26',
                'client_info_id' => 127,
                'form_sent' => NULL,
                'id' => 127,
                'reason' => 'Wanted to hire someone locally',
                'sub_reason' => NULL,
            ),
            127 => 
            array (
                'cancellation_date' => '2018-05-18',
                'client_info_id' => 128,
                'form_sent' => NULL,
                'id' => 128,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            128 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 129,
                'form_sent' => NULL,
                'id' => 129,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            129 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 130,
                'form_sent' => NULL,
                'id' => 130,
                'reason' => 'VA Performance',
                'sub_reason' => NULL,
            ),
            130 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 131,
                'form_sent' => NULL,
                'id' => 131,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            131 => 
            array (
                'cancellation_date' => '2020-03-03 04:42:23',
                'client_info_id' => 132,
                'form_sent' => NULL,
                'id' => 132,
                'reason' => 'Financial Difficulty',
                'sub_reason' => NULL,
            ),
            132 => 
            array (
                'cancellation_date' => '2018-04-10',
                'client_info_id' => 133,
                'form_sent' => NULL,
                'id' => 133,
                'reason' => 'VA Unsatisfactory Performance',
                'sub_reason' => NULL,
            ),
            133 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 134,
                'form_sent' => NULL,
                'id' => 134,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            134 => 
            array (
                'cancellation_date' => '2018-04-27',
                'client_info_id' => 135,
                'form_sent' => NULL,
                'id' => 135,
                'reason' => 'Transferred company',
                'sub_reason' => NULL,
            ),
            135 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 136,
                'form_sent' => NULL,
                'id' => 136,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            136 => 
            array (
                'cancellation_date' => '2018-08-13',
                'client_info_id' => 137,
                'form_sent' => NULL,
                'id' => 137,
                'reason' => 'unhappy with results',
                'sub_reason' => NULL,
            ),
            137 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 138,
                'form_sent' => NULL,
                'id' => 138,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            138 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 139,
                'form_sent' => NULL,
                'id' => 139,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            139 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 140,
                'form_sent' => NULL,
                'id' => 140,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            140 => 
            array (
                'cancellation_date' => '2018-10-12',
                'client_info_id' => 141,
                'form_sent' => NULL,
                'id' => 141,
                'reason' => ' son met an accident and will be working for her',
                'sub_reason' => NULL,
            ),
            141 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 142,
                'form_sent' => NULL,
                'id' => 142,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            142 => 
            array (
                'cancellation_date' => '2018-06-28',
                'client_info_id' => 143,
                'form_sent' => NULL,
                'id' => 143,
                'reason' => 'wanted to hire someone locally',
                'sub_reason' => NULL,
            ),
            143 => 
            array (
                'cancellation_date' => '2018-06-12',
                'client_info_id' => 144,
                'form_sent' => NULL,
                'id' => 144,
                'reason' => 'Personal, family emergency that he has to attend to',
                'sub_reason' => NULL,
            ),
            144 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 145,
                'form_sent' => NULL,
                'id' => 145,
                'reason' => 'VA Performance',
                'sub_reason' => NULL,
            ),
            145 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 146,
                'form_sent' => NULL,
                'id' => 146,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            146 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 147,
                'form_sent' => NULL,
                'id' => 147,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            147 => 
            array (
                'cancellation_date' => '2018-03-23',
                'client_info_id' => 148,
                'form_sent' => NULL,
                'id' => 148,
                'reason' => 'Unhappy that VA resigned before they started',
                'sub_reason' => NULL,
            ),
            148 => 
            array (
                'cancellation_date' => '2018-06-01',
                'client_info_id' => 149,
                'form_sent' => NULL,
                'id' => 149,
                'reason' => 'Business, not enough leads to work on',
                'sub_reason' => NULL,
            ),
            149 => 
            array (
                'cancellation_date' => '2018-05-23',
                'client_info_id' => 150,
                'form_sent' => NULL,
                'id' => 150,
                'reason' => 'Unhappy with VA\'s performance',
                'sub_reason' => NULL,
            ),
            150 => 
            array (
                'cancellation_date' => '2018-10-26',
                'client_info_id' => 151,
                'form_sent' => NULL,
                'id' => 151,
                'reason' => 'Hired a local VA',
                'sub_reason' => NULL,
            ),
            151 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 152,
                'form_sent' => NULL,
                'id' => 152,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            152 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 153,
                'form_sent' => NULL,
                'id' => 153,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            153 => 
            array (
                'cancellation_date' => '2018-03-14',
                'client_info_id' => 154,
                'form_sent' => NULL,
                'id' => 154,
                'reason' => 'Client is not ready system and structure wise for a VA',
                'sub_reason' => NULL,
            ),
            154 => 
            array (
                'cancellation_date' => '2018-07-03',
                'client_info_id' => 155,
                'form_sent' => NULL,
                'id' => 155,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            155 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 156,
                'form_sent' => NULL,
                'id' => 156,
                'reason' => 'Pavel decided not to work with her',
                'sub_reason' => NULL,
            ),
            156 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 157,
                'form_sent' => NULL,
                'id' => 157,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            157 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 158,
                'form_sent' => NULL,
                'id' => 158,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            158 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 159,
                'form_sent' => NULL,
                'id' => 159,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            159 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 160,
                'form_sent' => NULL,
                'id' => 160,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            160 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 161,
                'form_sent' => NULL,
                'id' => 161,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            161 => 
            array (
                'cancellation_date' => '2019-02-09',
                'client_info_id' => 162,
                'form_sent' => NULL,
                'id' => 162,
                'reason' => 'Financial - cut expenses',
                'sub_reason' => NULL,
            ),
            162 => 
            array (
                'cancellation_date' => '2018-03-20',
                'client_info_id' => 163,
                'form_sent' => NULL,
                'id' => 163,
                'reason' => 'Going for someone local',
                'sub_reason' => NULL,
            ),
            163 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 164,
                'form_sent' => NULL,
                'id' => 164,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            164 => 
            array (
                'cancellation_date' => '2018-06-28',
                'client_info_id' => 165,
                'form_sent' => NULL,
                'id' => 165,
                'reason' => 'Low number of leads from RedX, asked Imee to do manual lead gen from Zillow for FSBOs. Effectivity June 28th. Sharon wil stil try to reach out and turn things around',
                'sub_reason' => NULL,
            ),
            165 => 
            array (
                'cancellation_date' => '2018-11-21',
                'client_info_id' => 166,
                'form_sent' => NULL,
                'id' => 166,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            166 => 
            array (
                'cancellation_date' => '2018-06-15',
                'client_info_id' => 167,
                'form_sent' => NULL,
                'id' => 167,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            167 => 
            array (
                'cancellation_date' => '2020-06-29',
                'client_info_id' => 168,
                'form_sent' => NULL,
                'id' => 168,
                'reason' => 'Venturing out on another business',
                'sub_reason' => NULL,
            ),
            168 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 169,
                'form_sent' => NULL,
                'id' => 169,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            169 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 170,
                'form_sent' => NULL,
                'id' => 170,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            170 => 
            array (
                'cancellation_date' => '2018-05-23',
                'client_info_id' => 171,
                'form_sent' => NULL,
                'id' => 171,
                'reason' => 'VA\'s Attendance',
                'sub_reason' => NULL,
            ),
            171 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 172,
                'form_sent' => NULL,
                'id' => 172,
                'reason' => 'VA\'s performance, unable to provide appointments, VA was doing circle prospecting',
                'sub_reason' => NULL,
            ),
            172 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 173,
                'form_sent' => NULL,
                'id' => 173,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            173 => 
            array (
                'cancellation_date' => '2018-10-31',
                'client_info_id' => 174,
                'form_sent' => NULL,
                'id' => 174,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            174 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 175,
                'form_sent' => NULL,
                'id' => 175,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            175 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 176,
                'form_sent' => NULL,
                'id' => 176,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            176 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 177,
                'form_sent' => NULL,
                'id' => 177,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            177 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 178,
                'form_sent' => NULL,
                'id' => 178,
                'reason' => 'Was not happy with the VAs she interviewed. ',
                'sub_reason' => NULL,
            ),
            178 => 
            array (
                'cancellation_date' => '2018-09-03',
                'client_info_id' => 179,
                'form_sent' => NULL,
                'id' => 179,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            179 => 
            array (
                'cancellation_date' => '2018-07-17',
                'client_info_id' => 180,
                'form_sent' => NULL,
                'id' => 180,
                'reason' => 'Closing down his real estate business, traveling with wife',
                'sub_reason' => NULL,
            ),
            180 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 181,
                'form_sent' => NULL,
                'id' => 181,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            181 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 182,
                'form_sent' => NULL,
                'id' => 182,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            182 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 183,
                'form_sent' => NULL,
                'id' => 183,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            183 => 
            array (
                'cancellation_date' => '2018-07-19',
                'client_info_id' => 184,
                'form_sent' => NULL,
                'id' => 184,
                'reason' => 'Low number of leads, VA is doing circle prospecting',
                'sub_reason' => NULL,
            ),
            184 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 185,
                'form_sent' => NULL,
                'id' => 185,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            185 => 
            array (
                'cancellation_date' => '2018-08-31',
                'client_info_id' => 186,
                'form_sent' => NULL,
                'id' => 186,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            186 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 187,
                'form_sent' => NULL,
                'id' => 187,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            187 => 
            array (
                'cancellation_date' => '2018-07-25',
                'client_info_id' => 188,
                'form_sent' => NULL,
                'id' => 188,
                'reason' => 'No leads to work on',
                'sub_reason' => NULL,
            ),
            188 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 189,
                'form_sent' => NULL,
                'id' => 189,
                'reason' => 'Unsatisfied with VA\'s Perfroamnce',
                'sub_reason' => NULL,
            ),
            189 => 
            array (
                'cancellation_date' => '2018-10-31',
                'client_info_id' => 190,
                'form_sent' => NULL,
                'id' => 190,
                'reason' => '3 due to non payment',
                'sub_reason' => NULL,
            ),
            190 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 191,
                'form_sent' => NULL,
                'id' => 191,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            191 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 192,
                'form_sent' => NULL,
                'id' => 192,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            192 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 193,
                'form_sent' => NULL,
                'id' => 193,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            193 => 
            array (
                'cancellation_date' => '2018-05-11',
                'client_info_id' => 194,
                'form_sent' => NULL,
                'id' => 194,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            194 => 
            array (
                'cancellation_date' => '2018-09-26',
                'client_info_id' => 195,
                'form_sent' => NULL,
                'id' => 195,
                'reason' => '1. Lack of substantial appointments made.2. Trying a different calling service.',
                'sub_reason' => NULL,
            ),
            195 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 196,
                'form_sent' => NULL,
                'id' => 196,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            196 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 197,
                'form_sent' => NULL,
                'id' => 197,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            197 => 
            array (
                'cancellation_date' => '2018-11-08',
                'client_info_id' => 198,
                'form_sent' => NULL,
                'id' => 198,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            198 => 
            array (
                'cancellation_date' => '2018-06-07',
                'client_info_id' => 199,
                'form_sent' => NULL,
                'id' => 199,
                'reason' => 'Lack of tools, unwilling to get a phone numebr for the VA. VA was using google hangouts, leadsource are from Mojo, manual dialing',
                'sub_reason' => NULL,
            ),
            199 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 200,
                'form_sent' => NULL,
                'id' => 200,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            200 => 
            array (
                'cancellation_date' => '2018-08-27',
                'client_info_id' => 201,
                'form_sent' => NULL,
                'id' => 201,
                'reason' => 'Unhappy with ROI, very minimal appointments from Edward, Cherryll\'s appontments were not new leads but old leads that they were able to contact before. Disappointed that expectations were not set prior to signing the contract re tools they need to prepare and purchase. Tried reaching out to James after but did get a response. ',
                'sub_reason' => NULL,
            ),
            201 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 202,
                'form_sent' => NULL,
                'id' => 202,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            202 => 
            array (
                'cancellation_date' => '2018-08-31',
                'client_info_id' => 203,
                'form_sent' => NULL,
                'id' => 203,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            203 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 204,
                'form_sent' => NULL,
                'id' => 204,
                'reason' => 'Financials',
                'sub_reason' => NULL,
            ),
            204 => 
            array (
                'cancellation_date' => '2018-08-22',
                'client_info_id' => 205,
                'form_sent' => NULL,
                'id' => 205,
                'reason' => 'Will go for someone onshore',
                'sub_reason' => NULL,
            ),
            205 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 206,
                'form_sent' => NULL,
                'id' => 206,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            206 => 
            array (
                'cancellation_date' => '2018-11-09',
                'client_info_id' => 207,
                'form_sent' => NULL,
                'id' => 207,
                'reason' => 'Financial reasons',
                'sub_reason' => NULL,
            ),
            207 => 
            array (
                'cancellation_date' => '2018-10-06',
                'client_info_id' => 208,
                'form_sent' => NULL,
                'id' => 208,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            208 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 209,
                'form_sent' => NULL,
                'id' => 209,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            209 => 
            array (
                'cancellation_date' => '2018-08-29',
                'client_info_id' => 210,
                'form_sent' => NULL,
                'id' => 210,
                'reason' => 'Unhappy with Landvoice not being setup',
                'sub_reason' => NULL,
            ),
            210 => 
            array (
                'cancellation_date' => '2018-09-06',
                'client_info_id' => 211,
                'form_sent' => NULL,
                'id' => 211,
                'reason' => 'Low ROI',
                'sub_reason' => NULL,
            ),
            211 => 
            array (
                'cancellation_date' => '2018-12-29',
                'client_info_id' => 212,
                'form_sent' => NULL,
                'id' => 212,
                'reason' => 'Venturing on another business',
                'sub_reason' => NULL,
            ),
            212 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 213,
                'form_sent' => NULL,
                'id' => 213,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            213 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 214,
                'form_sent' => NULL,
                'id' => 214,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            214 => 
            array (
                'cancellation_date' => '096/25/2018',
                'client_info_id' => 215,
                'form_sent' => NULL,
                'id' => 215,
                'reason' => 'Low ROI',
                'sub_reason' => NULL,
            ),
            215 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 216,
                'form_sent' => NULL,
                'id' => 216,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            216 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 217,
                'form_sent' => NULL,
                'id' => 217,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            217 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 218,
                'form_sent' => NULL,
                'id' => 218,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            218 => 
            array (
                'cancellation_date' => '2018-10-19',
                'client_info_id' => 219,
                'form_sent' => NULL,
                'id' => 219,
                'reason' => 'Financials',
                'sub_reason' => NULL,
            ),
            219 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 220,
                'form_sent' => NULL,
                'id' => 220,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            220 => 
            array (
                'cancellation_date' => '2018-09-21',
                'client_info_id' => 221,
                'form_sent' => NULL,
                'id' => 221,
                'reason' => 'Unhappy with results',
                'sub_reason' => NULL,
            ),
            221 => 
            array (
                'cancellation_date' => '2018-11-27',
                'client_info_id' => 222,
                'form_sent' => NULL,
                'id' => 222,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            222 => 
            array (
                'cancellation_date' => '2018-10-12',
                'client_info_id' => 223,
                'form_sent' => NULL,
                'id' => 223,
                'reason' => 'Financials',
                'sub_reason' => NULL,
            ),
            223 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 224,
                'form_sent' => NULL,
                'id' => 224,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            224 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 225,
                'form_sent' => NULL,
                'id' => 225,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            225 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 226,
                'form_sent' => NULL,
                'id' => 226,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            226 => 
            array (
                'cancellation_date' => '2018-10-16',
                'client_info_id' => 227,
                'form_sent' => NULL,
                'id' => 227,
                'reason' => 'Leting go of CRM, doesnt want to get more leads. VA will mine lead til end of contract',
                'sub_reason' => NULL,
            ),
            227 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 228,
                'form_sent' => NULL,
                'id' => 228,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            228 => 
            array (
                'cancellation_date' => '2018-07-18',
                'client_info_id' => 229,
                'form_sent' => NULL,
                'id' => 229,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            229 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 230,
                'form_sent' => NULL,
                'id' => 230,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            230 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 231,
                'form_sent' => NULL,
                'id' => 231,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            231 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 232,
                'form_sent' => NULL,
                'id' => 232,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            232 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 233,
                'form_sent' => NULL,
                'id' => 233,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            233 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 234,
                'form_sent' => NULL,
                'id' => 234,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            234 => 
            array (
                'cancellation_date' => '2018-07-26',
                'client_info_id' => 235,
                'form_sent' => NULL,
                'id' => 235,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            235 => 
            array (
                'cancellation_date' => '2018-11-12',
                'client_info_id' => 236,
                'form_sent' => NULL,
                'id' => 236,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            236 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 237,
                'form_sent' => NULL,
                'id' => 237,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            237 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 238,
                'form_sent' => NULL,
                'id' => 238,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            238 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 239,
                'form_sent' => NULL,
                'id' => 239,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            239 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 240,
                'form_sent' => NULL,
                'id' => 240,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            240 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 241,
                'form_sent' => NULL,
                'id' => 241,
                'reason' => 'Unresponsive, havent paid the invoice',
                'sub_reason' => NULL,
            ),
            241 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 242,
                'form_sent' => NULL,
                'id' => 242,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            242 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 243,
                'form_sent' => NULL,
                'id' => 243,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            243 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 244,
                'form_sent' => NULL,
                'id' => 244,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            244 => 
            array (
                'cancellation_date' => '2018-12-05',
                'client_info_id' => 245,
                'form_sent' => NULL,
                'id' => 245,
                'reason' => 'Financials',
                'sub_reason' => NULL,
            ),
            245 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 246,
                'form_sent' => NULL,
                'id' => 246,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            246 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 247,
                'form_sent' => NULL,
                'id' => 247,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            247 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 248,
                'form_sent' => NULL,
                'id' => 248,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            248 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 249,
                'form_sent' => NULL,
                'id' => 249,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            249 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 250,
                'form_sent' => NULL,
                'id' => 250,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            250 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 251,
                'form_sent' => NULL,
                'id' => 251,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            251 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 252,
                'form_sent' => NULL,
                'id' => 252,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            252 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 253,
                'form_sent' => NULL,
                'id' => 253,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            253 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 254,
                'form_sent' => NULL,
                'id' => 254,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            254 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 255,
                'form_sent' => NULL,
                'id' => 255,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            255 => 
            array (
                'cancellation_date' => '2018-12-14',
                'client_info_id' => 256,
                'form_sent' => NULL,
                'id' => 256,
                'reason' => 'Opted to hire an onshore va',
                'sub_reason' => NULL,
            ),
            256 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 257,
                'form_sent' => NULL,
                'id' => 257,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            257 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 258,
                'form_sent' => NULL,
                'id' => 258,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            258 => 
            array (
                'cancellation_date' => '2018-12-15',
                'client_info_id' => 259,
                'form_sent' => NULL,
                'id' => 259,
                'reason' => 'non responsive leads',
                'sub_reason' => NULL,
            ),
            259 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 260,
                'form_sent' => NULL,
                'id' => 260,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            260 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 261,
                'form_sent' => NULL,
                'id' => 261,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            261 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 262,
                'form_sent' => NULL,
                'id' => 262,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            262 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 263,
                'form_sent' => NULL,
                'id' => 263,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            263 => 
            array (
                'cancellation_date' => '2019-03-25',
                'client_info_id' => 264,
                'form_sent' => NULL,
                'id' => 264,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            264 => 
            array (
                'cancellation_date' => '2018-11-23',
                'client_info_id' => 265,
                'form_sent' => NULL,
                'id' => 265,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            265 => 
            array (
                'cancellation_date' => '2018-10-25',
                'client_info_id' => 266,
                'form_sent' => NULL,
                'id' => 266,
                'reason' => 'Not yet ready in getting leads converted / Business reasons',
                'sub_reason' => NULL,
            ),
            266 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 267,
                'form_sent' => NULL,
                'id' => 267,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            267 => 
            array (
                'cancellation_date' => '2018-12-04',
                'client_info_id' => 268,
                'form_sent' => NULL,
                'id' => 268,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            268 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 269,
                'form_sent' => NULL,
                'id' => 269,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            269 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 270,
                'form_sent' => NULL,
                'id' => 270,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            270 => 
            array (
                'cancellation_date' => '2018-10-04',
                'client_info_id' => 271,
                'form_sent' => NULL,
                'id' => 271,
                'reason' => 'Hiring someone local instead',
                'sub_reason' => NULL,
            ),
            271 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 272,
                'form_sent' => NULL,
                'id' => 272,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            272 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 273,
                'form_sent' => NULL,
                'id' => 273,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            273 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 274,
                'form_sent' => NULL,
                'id' => 274,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            274 => 
            array (
                'cancellation_date' => '2019-04-12',
                'client_info_id' => 275,
                'form_sent' => NULL,
                'id' => 275,
                'reason' => 'wanted to hire someone locally',
                'sub_reason' => NULL,
            ),
            275 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 276,
                'form_sent' => NULL,
                'id' => 276,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            276 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 277,
                'form_sent' => NULL,
                'id' => 277,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            277 => 
            array (
                'cancellation_date' => '2018-11-26',
                'client_info_id' => 278,
                'form_sent' => NULL,
                'id' => 278,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            278 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 279,
                'form_sent' => NULL,
                'id' => 279,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            279 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 280,
                'form_sent' => NULL,
                'id' => 280,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            280 => 
            array (
                'cancellation_date' => '2019-01-29',
                'client_info_id' => 281,
                'form_sent' => NULL,
                'id' => 281,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            281 => 
            array (
                'cancellation_date' => '2018-11-30',
                'client_info_id' => 282,
                'form_sent' => NULL,
                'id' => 282,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            282 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 283,
                'form_sent' => NULL,
                'id' => 283,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            283 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 284,
                'form_sent' => NULL,
                'id' => 284,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            284 => 
            array (
                'cancellation_date' => '2018-12-06',
                'client_info_id' => 285,
                'form_sent' => NULL,
                'id' => 285,
                'reason' => 'End of contract',
                'sub_reason' => NULL,
            ),
            285 => 
            array (
                'cancellation_date' => '2019-03-09',
                'client_info_id' => 286,
                'form_sent' => NULL,
                'id' => 286,
                'reason' => 'Not enough work for the VA',
                'sub_reason' => NULL,
            ),
            286 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 287,
                'form_sent' => NULL,
                'id' => 287,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            287 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 288,
                'form_sent' => NULL,
                'id' => 288,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            288 => 
            array (
                'cancellation_date' => '2019-01-18',
                'client_info_id' => 289,
                'form_sent' => NULL,
                'id' => 289,
                'reason' => 'End of contract',
                'sub_reason' => NULL,
            ),
            289 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 290,
                'form_sent' => NULL,
                'id' => 290,
                'reason' => 'Get back 1st week of January',
                'sub_reason' => NULL,
            ),
            290 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 291,
                'form_sent' => NULL,
                'id' => 291,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            291 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 292,
                'form_sent' => NULL,
                'id' => 292,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            292 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 293,
                'form_sent' => NULL,
                'id' => 293,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            293 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 294,
                'form_sent' => NULL,
                'id' => 294,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            294 => 
            array (
                'cancellation_date' => '2019-04-10',
                'client_info_id' => 295,
                'form_sent' => NULL,
                'id' => 295,
                'reason' => 'cannot convert appointments to listings',
                'sub_reason' => NULL,
            ),
            295 => 
            array (
                'cancellation_date' => '2019-04-25',
                'client_info_id' => 296,
                'form_sent' => NULL,
                'id' => 296,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            296 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 297,
                'form_sent' => NULL,
                'id' => 297,
                'reason' => 'no more task for the GVA',
                'sub_reason' => NULL,
            ),
            297 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 298,
                'form_sent' => NULL,
                'id' => 298,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            298 => 
            array (
                'cancellation_date' => '2018-12-14',
                'client_info_id' => 299,
                'form_sent' => NULL,
                'id' => 299,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            299 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 300,
                'form_sent' => NULL,
                'id' => 300,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            300 => 
            array (
                'cancellation_date' => '2019-01-31',
                'client_info_id' => 301,
                'form_sent' => NULL,
                'id' => 301,
                'reason' => 'Done with timeblock',
                'sub_reason' => NULL,
            ),
            301 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 302,
                'form_sent' => NULL,
                'id' => 302,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            302 => 
            array (
                'cancellation_date' => '2019-02-22',
                'client_info_id' => 303,
                'form_sent' => NULL,
                'id' => 303,
                'reason' => 'Done with 1 month trial',
                'sub_reason' => NULL,
            ),
            303 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 304,
                'form_sent' => NULL,
                'id' => 304,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            304 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 305,
                'form_sent' => NULL,
                'id' => 305,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            305 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 306,
                'form_sent' => NULL,
                'id' => 306,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            306 => 
            array (
                'cancellation_date' => '2018-12-20',
                'client_info_id' => 307,
                'form_sent' => NULL,
                'id' => 307,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            307 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 308,
                'form_sent' => NULL,
                'id' => 308,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            308 => 
            array (
                'cancellation_date' => '2019-04-20',
                'client_info_id' => 309,
                'form_sent' => NULL,
                'id' => 309,
                'reason' => 'end of contract and client is unable to convert VAs listing appts',
                'sub_reason' => NULL,
            ),
            309 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 310,
                'form_sent' => NULL,
                'id' => 310,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            310 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 311,
                'form_sent' => NULL,
                'id' => 311,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            311 => 
            array (
                'cancellation_date' => '2019-03-04',
                'client_info_id' => 312,
                'form_sent' => NULL,
                'id' => 312,
                'reason' => 'unsatisfied performance, Leads are fast moving',
                'sub_reason' => NULL,
            ),
            312 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 313,
                'form_sent' => NULL,
                'id' => 313,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            313 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 314,
                'form_sent' => NULL,
                'id' => 314,
                'reason' => 'done with timeblock',
                'sub_reason' => NULL,
            ),
            314 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 315,
                'form_sent' => NULL,
                'id' => 315,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            315 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 316,
                'form_sent' => NULL,
                'id' => 316,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            316 => 
            array (
                'cancellation_date' => '2019-04-19',
                'client_info_id' => 317,
                'form_sent' => NULL,
                'id' => 317,
                'reason' => 'Financials/ Market Issues',
                'sub_reason' => NULL,
            ),
            317 => 
            array (
                'cancellation_date' => '2019-03-08',
                'client_info_id' => 318,
                'form_sent' => NULL,
                'id' => 318,
                'reason' => 'Done with trial period',
                'sub_reason' => NULL,
            ),
            318 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 319,
                'form_sent' => NULL,
                'id' => 319,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            319 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 320,
                'form_sent' => NULL,
                'id' => 320,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            320 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 321,
                'form_sent' => NULL,
                'id' => 321,
                'reason' => 'Pending SUF payment',
                'sub_reason' => NULL,
            ),
            321 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 322,
                'form_sent' => NULL,
                'id' => 322,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            322 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 323,
                'form_sent' => NULL,
                'id' => 323,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            323 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 324,
                'form_sent' => NULL,
                'id' => 324,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            324 => 
            array (
                'cancellation_date' => '2019-03-12',
                'client_info_id' => 325,
                'form_sent' => NULL,
                'id' => 325,
                'reason' => 'Doesnt need VA services now',
                'sub_reason' => NULL,
            ),
            325 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 326,
                'form_sent' => NULL,
                'id' => 326,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            326 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 327,
                'form_sent' => NULL,
                'id' => 327,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            327 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 328,
                'form_sent' => NULL,
                'id' => 328,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            328 => 
            array (
                'cancellation_date' => '2019-04-19',
                'client_info_id' => 329,
                'form_sent' => NULL,
                'id' => 329,
                'reason' => 'no enough funds',
                'sub_reason' => NULL,
            ),
            329 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 330,
                'form_sent' => NULL,
                'id' => 330,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            330 => 
            array (
                'cancellation_date' => '2019-03-21',
                'client_info_id' => 331,
                'form_sent' => NULL,
                'id' => 331,
                'reason' => 'Financials',
                'sub_reason' => NULL,
            ),
            331 => 
            array (
                'cancellation_date' => '2019-04-17',
                'client_info_id' => 332,
                'form_sent' => NULL,
                'id' => 332,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            332 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 333,
                'form_sent' => NULL,
                'id' => 333,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            333 => 
            array (
                'cancellation_date' => '2019-03-22',
                'client_info_id' => 334,
                'form_sent' => NULL,
                'id' => 334,
                'reason' => 'low ROI',
                'sub_reason' => NULL,
            ),
            334 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 335,
                'form_sent' => NULL,
                'id' => 335,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            335 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 336,
                'form_sent' => NULL,
                'id' => 336,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            336 => 
            array (
                'cancellation_date' => '2020-05-15 10:55:54',
                'client_info_id' => 337,
                'form_sent' => NULL,
                'id' => 337,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            337 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 338,
                'form_sent' => NULL,
                'id' => 338,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            338 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 339,
                'form_sent' => NULL,
                'id' => 339,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            339 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 340,
                'form_sent' => NULL,
                'id' => 340,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            340 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 341,
                'form_sent' => NULL,
                'id' => 341,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            341 => 
            array (
                'cancellation_date' => '2019-05-10',
                'client_info_id' => 342,
                'form_sent' => NULL,
                'id' => 342,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            342 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 343,
                'form_sent' => NULL,
                'id' => 343,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            343 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 344,
                'form_sent' => NULL,
                'id' => 344,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            344 => 
            array (
                'cancellation_date' => '2019-03-18',
                'client_info_id' => 345,
                'form_sent' => NULL,
                'id' => 345,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            345 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 346,
                'form_sent' => NULL,
                'id' => 346,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            346 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 347,
                'form_sent' => NULL,
                'id' => 347,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            347 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 348,
                'form_sent' => NULL,
                'id' => 348,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            348 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 349,
                'form_sent' => NULL,
                'id' => 349,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            349 => 
            array (
                'cancellation_date' => '2019-02-19',
                'client_info_id' => 350,
                'form_sent' => NULL,
                'id' => 350,
                'reason' => 'Unhappy with VA, refused replacement',
                'sub_reason' => NULL,
            ),
            350 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 351,
                'form_sent' => NULL,
                'id' => 351,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            351 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 352,
                'form_sent' => NULL,
                'id' => 352,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            352 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 353,
                'form_sent' => NULL,
                'id' => 353,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            353 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 354,
                'form_sent' => NULL,
                'id' => 354,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            354 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 355,
                'form_sent' => NULL,
                'id' => 355,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            355 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 356,
                'form_sent' => NULL,
                'id' => 356,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            356 => 
            array (
                'cancellation_date' => '2019-03-25',
                'client_info_id' => 357,
                'form_sent' => NULL,
                'id' => 357,
                'reason' => 'Personal, sick',
                'sub_reason' => NULL,
            ),
            357 => 
            array (
                'cancellation_date' => '2019-05-15',
                'client_info_id' => 358,
                'form_sent' => NULL,
                'id' => 358,
                'reason' => 'Financial',
                'sub_reason' => NULL,
            ),
            358 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 359,
                'form_sent' => NULL,
                'id' => 359,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            359 => 
            array (
                'cancellation_date' => 'Ddd not push through',
                'client_info_id' => 360,
                'form_sent' => NULL,
                'id' => 360,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            360 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 361,
                'form_sent' => NULL,
                'id' => 361,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            361 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 362,
                'form_sent' => NULL,
                'id' => 362,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            362 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 363,
                'form_sent' => NULL,
                'id' => 363,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            363 => 
            array (
                'cancellation_date' => '2019-03-15',
                'client_info_id' => 364,
                'form_sent' => NULL,
                'id' => 364,
                'reason' => 'Wanted to hire someone locally, offered to hire 2 VAs, 1 PT ISA and 1 PT GVA',
                'sub_reason' => NULL,
            ),
            364 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 365,
                'form_sent' => NULL,
                'id' => 365,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            365 => 
            array (
                'cancellation_date' => '2019-04-17',
                'client_info_id' => 366,
                'form_sent' => NULL,
                'id' => 366,
                'reason' => 'no more tasks for Lara',
                'sub_reason' => NULL,
            ),
            366 => 
            array (
                'cancellation_date' => '2019-03-06',
                'client_info_id' => 367,
                'form_sent' => NULL,
                'id' => 367,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            367 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 368,
                'form_sent' => NULL,
                'id' => 368,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            368 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 369,
                'form_sent' => NULL,
                'id' => 369,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            369 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 370,
                'form_sent' => NULL,
                'id' => 370,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            370 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 371,
                'form_sent' => NULL,
                'id' => 371,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            371 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 372,
                'form_sent' => NULL,
                'id' => 372,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            372 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 373,
                'form_sent' => NULL,
                'id' => 373,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            373 => 
            array (
                'cancellation_date' => '2019-04-15',
                'client_info_id' => 374,
                'form_sent' => NULL,
                'id' => 374,
                'reason' => 'no more tasks for the GVA',
                'sub_reason' => NULL,
            ),
            374 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 375,
                'form_sent' => NULL,
                'id' => 375,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            375 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 376,
                'form_sent' => NULL,
                'id' => 376,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            376 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 377,
                'form_sent' => NULL,
                'id' => 377,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            377 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 378,
                'form_sent' => NULL,
                'id' => 378,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            378 => 
            array (
                'cancellation_date' => '2019-03-21',
                'client_info_id' => 379,
                'form_sent' => NULL,
                'id' => 379,
                'reason' => 'Realized that he doesnt need a VA',
                'sub_reason' => NULL,
            ),
            379 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 380,
                'form_sent' => NULL,
                'id' => 380,
                'reason' => 'done with timevblock',
                'sub_reason' => NULL,
            ),
            380 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 381,
                'form_sent' => NULL,
                'id' => 381,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            381 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 382,
                'form_sent' => NULL,
                'id' => 382,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            382 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 383,
                'form_sent' => NULL,
                'id' => 383,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            383 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 384,
                'form_sent' => NULL,
                'id' => 384,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            384 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 385,
                'form_sent' => NULL,
                'id' => 385,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            385 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 386,
                'form_sent' => NULL,
                'id' => 386,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            386 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 387,
                'form_sent' => NULL,
                'id' => 387,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            387 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 388,
                'form_sent' => NULL,
                'id' => 388,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            388 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 389,
                'form_sent' => NULL,
                'id' => 389,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            389 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 390,
                'form_sent' => NULL,
                'id' => 390,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            390 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 391,
                'form_sent' => NULL,
                'id' => 391,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            391 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 392,
                'form_sent' => NULL,
                'id' => 392,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            392 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 393,
                'form_sent' => NULL,
                'id' => 393,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            393 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 394,
                'form_sent' => NULL,
                'id' => 394,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            394 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 395,
                'form_sent' => NULL,
                'id' => 395,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            395 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 396,
                'form_sent' => NULL,
                'id' => 396,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            396 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 397,
                'form_sent' => NULL,
                'id' => 397,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            397 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 398,
                'form_sent' => NULL,
                'id' => 398,
                'reason' => 'His sale on Friday didnt push through budget is tight right now, wondering how he can get the SUF back',
                'sub_reason' => NULL,
            ),
            398 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 399,
                'form_sent' => NULL,
                'id' => 399,
                'reason' => 'Opted to hire someone onshore',
                'sub_reason' => NULL,
            ),
            399 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 400,
                'form_sent' => NULL,
                'id' => 400,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            400 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 401,
                'form_sent' => NULL,
                'id' => 401,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            401 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 402,
                'form_sent' => NULL,
                'id' => 402,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            402 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 403,
                'form_sent' => NULL,
                'id' => 403,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            403 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 404,
                'form_sent' => NULL,
                'id' => 404,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            404 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 405,
                'form_sent' => NULL,
                'id' => 405,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            405 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 406,
                'form_sent' => NULL,
                'id' => 406,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            406 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 407,
                'form_sent' => NULL,
                'id' => 407,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            407 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 408,
                'form_sent' => NULL,
                'id' => 408,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            408 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 409,
                'form_sent' => NULL,
                'id' => 409,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            409 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 410,
                'form_sent' => NULL,
                'id' => 410,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            410 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 411,
                'form_sent' => NULL,
                'id' => 411,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            411 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 412,
                'form_sent' => NULL,
                'id' => 412,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            412 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 413,
                'form_sent' => NULL,
                'id' => 413,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            413 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 414,
                'form_sent' => NULL,
                'id' => 414,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            414 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 415,
                'form_sent' => NULL,
                'id' => 415,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            415 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 416,
                'form_sent' => NULL,
                'id' => 416,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            416 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 417,
                'form_sent' => NULL,
                'id' => 417,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            417 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 418,
                'form_sent' => NULL,
                'id' => 418,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            418 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 419,
                'form_sent' => NULL,
                'id' => 419,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            419 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 420,
                'form_sent' => NULL,
                'id' => 420,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            420 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 421,
                'form_sent' => NULL,
                'id' => 421,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            421 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 422,
                'form_sent' => NULL,
                'id' => 422,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            422 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 423,
                'form_sent' => NULL,
                'id' => 423,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            423 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 424,
                'form_sent' => NULL,
                'id' => 424,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            424 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 425,
                'form_sent' => NULL,
                'id' => 425,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            425 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 426,
                'form_sent' => NULL,
                'id' => 426,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            426 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 427,
                'form_sent' => NULL,
                'id' => 427,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            427 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 428,
                'form_sent' => NULL,
                'id' => 428,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            428 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 429,
                'form_sent' => NULL,
                'id' => 429,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            429 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 430,
                'form_sent' => NULL,
                'id' => 430,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            430 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 431,
                'form_sent' => NULL,
                'id' => 431,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            431 => 
            array (
                'cancellation_date' => '2020-03-08 03:16:00',
                'client_info_id' => 432,
                'form_sent' => NULL,
                'id' => 432,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            432 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 433,
                'form_sent' => NULL,
                'id' => 433,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            433 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 434,
                'form_sent' => NULL,
                'id' => 434,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            434 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 435,
                'form_sent' => NULL,
                'id' => 435,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            435 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 436,
                'form_sent' => NULL,
                'id' => 436,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            436 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 437,
                'form_sent' => NULL,
                'id' => 437,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            437 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 438,
                'form_sent' => NULL,
                'id' => 438,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            438 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 439,
                'form_sent' => NULL,
                'id' => 439,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            439 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 440,
                'form_sent' => NULL,
                'id' => 440,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            440 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 441,
                'form_sent' => NULL,
                'id' => 441,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            441 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 442,
                'form_sent' => NULL,
                'id' => 442,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            442 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 443,
                'form_sent' => NULL,
                'id' => 443,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            443 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 444,
                'form_sent' => NULL,
                'id' => 444,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            444 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 445,
                'form_sent' => NULL,
                'id' => 445,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            445 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 446,
                'form_sent' => NULL,
                'id' => 446,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            446 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 447,
                'form_sent' => NULL,
                'id' => 447,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            447 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 448,
                'form_sent' => NULL,
                'id' => 448,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            448 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 449,
                'form_sent' => NULL,
                'id' => 449,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            449 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 450,
                'form_sent' => NULL,
                'id' => 450,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            450 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 451,
                'form_sent' => NULL,
                'id' => 451,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            451 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 452,
                'form_sent' => NULL,
                'id' => 452,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            452 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 453,
                'form_sent' => NULL,
                'id' => 453,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            453 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 454,
                'form_sent' => NULL,
                'id' => 454,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            454 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 455,
                'form_sent' => NULL,
                'id' => 455,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            455 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 456,
                'form_sent' => NULL,
                'id' => 456,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            456 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 457,
                'form_sent' => NULL,
                'id' => 457,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            457 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 458,
                'form_sent' => NULL,
                'id' => 458,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            458 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 459,
                'form_sent' => NULL,
                'id' => 459,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            459 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 460,
                'form_sent' => NULL,
                'id' => 460,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            460 => 
            array (
                'cancellation_date' => '2020-04-25 01:41:24',
                'client_info_id' => 461,
                'form_sent' => NULL,
                'id' => 461,
                'reason' => 'VA Performance/End of contract
11-Apr-2020',
                'sub_reason' => NULL,
            ),
            461 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 462,
                'form_sent' => NULL,
                'id' => 462,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            462 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 463,
                'form_sent' => NULL,
                'id' => 463,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            463 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 464,
                'form_sent' => NULL,
                'id' => 464,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            464 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 465,
                'form_sent' => NULL,
                'id' => 465,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            465 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 466,
                'form_sent' => NULL,
                'id' => 466,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            466 => 
            array (
                'cancellation_date' => '2020-04-10 12:49:21',
                'client_info_id' => 467,
                'form_sent' => NULL,
                'id' => 467,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            467 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 468,
                'form_sent' => NULL,
                'id' => 468,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            468 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 469,
                'form_sent' => NULL,
                'id' => 469,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            469 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 470,
                'form_sent' => NULL,
                'id' => 470,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            470 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 471,
                'form_sent' => NULL,
                'id' => 471,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            471 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 472,
                'form_sent' => NULL,
                'id' => 472,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            472 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 473,
                'form_sent' => NULL,
                'id' => 473,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            473 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 474,
                'form_sent' => NULL,
                'id' => 474,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            474 => 
            array (
                'cancellation_date' => '2021-05-25 05:13:22',
                'client_info_id' => 475,
                'form_sent' => NULL,
                'id' => 475,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            475 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 476,
                'form_sent' => NULL,
                'id' => 476,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            476 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 477,
                'form_sent' => NULL,
                'id' => 477,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            477 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 478,
                'form_sent' => NULL,
                'id' => 478,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            478 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 479,
                'form_sent' => NULL,
                'id' => 479,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            479 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 480,
                'form_sent' => NULL,
                'id' => 480,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            480 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 481,
                'form_sent' => NULL,
                'id' => 481,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            481 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 482,
                'form_sent' => NULL,
                'id' => 482,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            482 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 483,
                'form_sent' => NULL,
                'id' => 483,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            483 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 484,
                'form_sent' => NULL,
                'id' => 484,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            484 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 485,
                'form_sent' => NULL,
                'id' => 485,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            485 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 486,
                'form_sent' => NULL,
                'id' => 486,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            486 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 487,
                'form_sent' => NULL,
                'id' => 487,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            487 => 
            array (
                'cancellation_date' => '2020-03-14 12:26:00',
                'client_info_id' => 488,
                'form_sent' => NULL,
                'id' => 488,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            488 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 489,
                'form_sent' => NULL,
                'id' => 489,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            489 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 490,
                'form_sent' => NULL,
                'id' => 490,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            490 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 491,
                'form_sent' => NULL,
                'id' => 491,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            491 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 492,
                'form_sent' => NULL,
                'id' => 492,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            492 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 493,
                'form_sent' => NULL,
                'id' => 493,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            493 => 
            array (
                'cancellation_date' => '2020-04-09 12:05:02',
                'client_info_id' => 494,
                'form_sent' => NULL,
                'id' => 494,
                'reason' => 'Please see the cancellation details below.

VA Name: Laradeth Renacido
Reason|Sub-reason for Cancellation:  No tasks/ honoured contract | End of Contract
Effectivity date:  October 4, 2019
Client: Jessica Powers',
                'sub_reason' => NULL,
            ),
            494 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 495,
                'form_sent' => NULL,
                'id' => 495,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            495 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 496,
                'form_sent' => NULL,
                'id' => 496,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            496 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 497,
                'form_sent' => NULL,
                'id' => 497,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            497 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 498,
                'form_sent' => NULL,
                'id' => 498,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            498 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 499,
                'form_sent' => NULL,
                'id' => 499,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            499 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 500,
                'form_sent' => NULL,
                'id' => 500,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
        ));
        \DB::table('client_cancellation')->insert(array (
            0 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 501,
                'form_sent' => NULL,
                'id' => 501,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            1 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 502,
                'form_sent' => NULL,
                'id' => 502,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            2 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 503,
                'form_sent' => NULL,
                'id' => 503,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            3 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 504,
                'form_sent' => NULL,
                'id' => 504,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            4 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 505,
                'form_sent' => NULL,
                'id' => 505,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            5 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 506,
                'form_sent' => NULL,
                'id' => 506,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            6 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 507,
                'form_sent' => NULL,
                'id' => 507,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            7 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 508,
                'form_sent' => NULL,
                'id' => 508,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            8 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 509,
                'form_sent' => NULL,
                'id' => 509,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            9 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 510,
                'form_sent' => NULL,
                'id' => 510,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            10 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 511,
                'form_sent' => NULL,
                'id' => 511,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            11 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 512,
                'form_sent' => NULL,
                'id' => 512,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            12 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 513,
                'form_sent' => NULL,
                'id' => 513,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            13 => 
            array (
                'cancellation_date' => '2020-03-08 03:28:59',
                'client_info_id' => 514,
                'form_sent' => NULL,
                'id' => 514,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            14 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 515,
                'form_sent' => NULL,
                'id' => 515,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            15 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 516,
                'form_sent' => NULL,
                'id' => 516,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            16 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 517,
                'form_sent' => NULL,
                'id' => 517,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            17 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 518,
                'form_sent' => NULL,
                'id' => 518,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            18 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 519,
                'form_sent' => NULL,
                'id' => 519,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            19 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 520,
                'form_sent' => NULL,
                'id' => 520,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            20 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 521,
                'form_sent' => NULL,
                'id' => 521,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            21 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 522,
                'form_sent' => NULL,
                'id' => 522,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            22 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 523,
                'form_sent' => NULL,
                'id' => 523,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            23 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 524,
                'form_sent' => NULL,
                'id' => 524,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            24 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 525,
                'form_sent' => NULL,
                'id' => 525,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            25 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 526,
                'form_sent' => NULL,
                'id' => 526,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            26 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 527,
                'form_sent' => NULL,
                'id' => 527,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            27 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 528,
                'form_sent' => NULL,
                'id' => 528,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            28 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 529,
                'form_sent' => NULL,
                'id' => 529,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            29 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 530,
                'form_sent' => NULL,
                'id' => 530,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            30 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 531,
                'form_sent' => NULL,
                'id' => 531,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            31 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 532,
                'form_sent' => NULL,
                'id' => 532,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            32 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 533,
                'form_sent' => NULL,
                'id' => 533,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            33 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 534,
                'form_sent' => NULL,
                'id' => 534,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            34 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 535,
                'form_sent' => NULL,
                'id' => 535,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            35 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 536,
                'form_sent' => NULL,
                'id' => 536,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            36 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 537,
                'form_sent' => NULL,
                'id' => 537,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            37 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 538,
                'form_sent' => NULL,
                'id' => 538,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            38 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 539,
                'form_sent' => NULL,
                'id' => 539,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            39 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 540,
                'form_sent' => NULL,
                'id' => 540,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            40 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 541,
                'form_sent' => NULL,
                'id' => 541,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            41 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 542,
                'form_sent' => NULL,
                'id' => 542,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            42 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 543,
                'form_sent' => NULL,
                'id' => 543,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            43 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 544,
                'form_sent' => NULL,
                'id' => 544,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            44 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 545,
                'form_sent' => NULL,
                'id' => 545,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            45 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 546,
                'form_sent' => NULL,
                'id' => 546,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            46 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 547,
                'form_sent' => NULL,
                'id' => 547,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            47 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 548,
                'form_sent' => NULL,
                'id' => 548,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            48 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 549,
                'form_sent' => NULL,
                'id' => 549,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            49 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 550,
                'form_sent' => NULL,
                'id' => 550,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            50 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 551,
                'form_sent' => NULL,
                'id' => 551,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            51 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 552,
                'form_sent' => NULL,
                'id' => 552,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            52 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 553,
                'form_sent' => NULL,
                'id' => 553,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            53 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 554,
                'form_sent' => NULL,
                'id' => 554,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            54 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 555,
                'form_sent' => NULL,
                'id' => 555,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            55 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 556,
                'form_sent' => NULL,
                'id' => 556,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            56 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 557,
                'form_sent' => NULL,
                'id' => 557,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            57 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 558,
                'form_sent' => NULL,
                'id' => 558,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            58 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 559,
                'form_sent' => NULL,
                'id' => 559,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            59 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 560,
                'form_sent' => NULL,
                'id' => 560,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            60 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 561,
                'form_sent' => NULL,
                'id' => 561,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            61 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 562,
                'form_sent' => NULL,
                'id' => 562,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            62 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 563,
                'form_sent' => NULL,
                'id' => 563,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            63 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 564,
                'form_sent' => NULL,
                'id' => 564,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            64 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 565,
                'form_sent' => NULL,
                'id' => 565,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            65 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 566,
                'form_sent' => NULL,
                'id' => 566,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            66 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 567,
                'form_sent' => NULL,
                'id' => 567,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            67 => 
            array (
                'cancellation_date' => '2020-06-25 12:09:17',
                'client_info_id' => 568,
                'form_sent' => NULL,
                'id' => 568,
                'reason' => 'Unresponsive',
                'sub_reason' => NULL,
            ),
            68 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 569,
                'form_sent' => NULL,
                'id' => 569,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            69 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 570,
                'form_sent' => NULL,
                'id' => 570,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            70 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 571,
                'form_sent' => NULL,
                'id' => 571,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            71 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 572,
                'form_sent' => NULL,
                'id' => 572,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            72 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 573,
                'form_sent' => NULL,
                'id' => 573,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            73 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 574,
                'form_sent' => NULL,
                'id' => 574,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            74 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 575,
                'form_sent' => NULL,
                'id' => 575,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            75 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 576,
                'form_sent' => NULL,
                'id' => 576,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            76 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 577,
                'form_sent' => NULL,
                'id' => 577,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            77 => 
            array (
                'cancellation_date' => '2020-06-25 12:11:00',
                'client_info_id' => 578,
                'form_sent' => NULL,
                'id' => 578,
                'reason' => 'unresponsive',
                'sub_reason' => NULL,
            ),
            78 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 579,
                'form_sent' => NULL,
                'id' => 579,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            79 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 580,
                'form_sent' => NULL,
                'id' => 580,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            80 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 581,
                'form_sent' => NULL,
                'id' => 581,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            81 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 582,
                'form_sent' => NULL,
                'id' => 582,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            82 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 583,
                'form_sent' => NULL,
                'id' => 583,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            83 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 584,
                'form_sent' => NULL,
                'id' => 584,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            84 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 585,
                'form_sent' => NULL,
                'id' => 585,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            85 => 
            array (
                'cancellation_date' => '2020-10-22 03:02:26',
                'client_info_id' => 586,
                'form_sent' => NULL,
                'id' => 586,
                'reason' => 'Personal	
Need a licensed ISA	
9-Jan-2020',
                'sub_reason' => NULL,
            ),
            86 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 587,
                'form_sent' => NULL,
                'id' => 587,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            87 => 
            array (
                'cancellation_date' => '2020-05-15 10:52:46',
                'client_info_id' => 588,
                'form_sent' => NULL,
                'id' => 588,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            88 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 589,
                'form_sent' => NULL,
                'id' => 589,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            89 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 590,
                'form_sent' => NULL,
                'id' => 590,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            90 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 591,
                'form_sent' => NULL,
                'id' => 591,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            91 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 592,
                'form_sent' => NULL,
                'id' => 592,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            92 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 593,
                'form_sent' => NULL,
                'id' => 593,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            93 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 594,
                'form_sent' => NULL,
                'id' => 594,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            94 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 595,
                'form_sent' => NULL,
                'id' => 595,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            95 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 596,
                'form_sent' => NULL,
                'id' => 596,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            96 => 
            array (
                'cancellation_date' => '2020-05-15 10:51:18',
                'client_info_id' => 597,
                'form_sent' => NULL,
                'id' => 597,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            97 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 598,
                'form_sent' => NULL,
                'id' => 598,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            98 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 599,
                'form_sent' => NULL,
                'id' => 599,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            99 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 600,
                'form_sent' => NULL,
                'id' => 600,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            100 => 
            array (
                'cancellation_date' => '2020-03-22 06:27:21',
                'client_info_id' => 601,
                'form_sent' => NULL,
                'id' => 601,
                'reason' => 'end of contract',
                'sub_reason' => NULL,
            ),
            101 => 
            array (
                'cancellation_date' => '2020-03-22 06:16:24',
                'client_info_id' => 602,
                'form_sent' => NULL,
                'id' => 602,
                'reason' => 'Reason|Sub-reason for Cancellation: VA and client differences / Client not happy with the VA\'s performance
Effectivity date:  February 13, 2020',
                'sub_reason' => NULL,
            ),
            102 => 
            array (
                'cancellation_date' => '2020-03-27 06:10:22',
                'client_info_id' => 603,
                'form_sent' => NULL,
                'id' => 603,
                'reason' => 'Financial Difficulties',
                'sub_reason' => NULL,
            ),
            103 => 
            array (
                'cancellation_date' => 'NULL',
                'client_info_id' => 604,
                'form_sent' => NULL,
                'id' => 604,
                'reason' => 'NULL',
                'sub_reason' => NULL,
            ),
            104 => 
            array (
                'cancellation_date' => '2020-02-08 12:39:21',
                'client_info_id' => 607,
                'form_sent' => NULL,
                'id' => 605,
                'reason' => 'sdsdsdsd',
                'sub_reason' => NULL,
            ),
            105 => 
            array (
                'cancellation_date' => '2020-03-19 03:13:56',
                'client_info_id' => 626,
                'form_sent' => NULL,
                'id' => 606,
                'reason' => NULL,
                'sub_reason' => NULL,
            ),
            106 => 
            array (
                'cancellation_date' => '2020-03-22 05:24:26',
                'client_info_id' => 663,
                'form_sent' => NULL,
                'id' => 607,
                'reason' => NULL,
                'sub_reason' => NULL,
            ),
            107 => 
            array (
                'cancellation_date' => '2020-03-22 05:36:42',
                'client_info_id' => 611,
                'form_sent' => NULL,
                'id' => 608,
                'reason' => NULL,
                'sub_reason' => NULL,
            ),
            108 => 
            array (
                'cancellation_date' => '2020-03-22 05:58:27',
                'client_info_id' => 672,
                'form_sent' => NULL,
                'id' => 609,
                'reason' => 'Reason|Sub-reason for Cancellation:  No more tasks | End of Contract
Effectivity date:  January 31, 2020',
                'sub_reason' => NULL,
            ),
            109 => 
            array (
                'cancellation_date' => '2020-03-28 11:31:12',
                'client_info_id' => 670,
                'form_sent' => NULL,
                'id' => 610,
                'reason' => 'End of contract / Financials',
                'sub_reason' => NULL,
            ),
            110 => 
            array (
                'cancellation_date' => '2020-04-07 01:08:52',
                'client_info_id' => 654,
                'form_sent' => NULL,
                'id' => 611,
                'reason' => 'Reason|Sub-reason for Cancellation: Financials / COVID issue 
Effectivity date:  March 31, 2020',
                'sub_reason' => NULL,
            ),
            111 => 
            array (
                'cancellation_date' => '2020-04-08 12:28:50',
                'client_info_id' => 627,
                'form_sent' => NULL,
                'id' => 612,
                'reason' => 'Financials / End of Contract',
                'sub_reason' => NULL,
            ),
            112 => 
            array (
                'cancellation_date' => '2020-04-25 01:36:51',
                'client_info_id' => 609,
                'form_sent' => NULL,
                'id' => 613,
                'reason' => 'Jessa Mae Jagualing and Elvin Britanico
Reason: Financials / COVID Pandemic 
Effectivity date:  April 9, 2020',
                'sub_reason' => NULL,
            ),
            113 => 
            array (
                'cancellation_date' => '2020-05-15 10:57:14',
                'client_info_id' => 666,
                'form_sent' => NULL,
                'id' => 614,
                'reason' => 'Financial Issues',
                'sub_reason' => NULL,
            ),
            114 => 
            array (
                'cancellation_date' => '2020-05-28 04:58:08',
                'client_info_id' => 616,
                'form_sent' => NULL,
                'id' => 615,
                'reason' => 'Financial Issues',
                'sub_reason' => NULL,
            ),
            115 => 
            array (
                'cancellation_date' => '2020-05-28 04:59:27',
                'client_info_id' => 628,
                'form_sent' => NULL,
                'id' => 616,
                'reason' => 'Client unresponsive',
                'sub_reason' => NULL,
            ),
            116 => 
            array (
                'cancellation_date' => '2020-06-12 09:30:01',
                'client_info_id' => 682,
                'form_sent' => NULL,
                'id' => 617,
                'reason' => 'VA attendance issues and performance',
                'sub_reason' => NULL,
            ),
            117 => 
            array (
                'cancellation_date' => '2020-06-17 12:31:08',
                'client_info_id' => 724,
                'form_sent' => NULL,
                'id' => 618,
                'reason' => 'Financial Difficulties',
                'sub_reason' => NULL,
            ),
            118 => 
            array (
                'cancellation_date' => '2020-06-20 07:22:25',
                'client_info_id' => 723,
                'form_sent' => NULL,
                'id' => 619,
                'reason' => 'Getting an "in person" assistant that can help deliver flyers, hang key boxes',
                'sub_reason' => NULL,
            ),
            119 => 
            array (
                'cancellation_date' => '2020-07-22 04:41:49',
                'client_info_id' => 784,
                'form_sent' => NULL,
                'id' => 620,
                'reason' => NULL,
                'sub_reason' => NULL,
            ),
            120 => 
            array (
                'cancellation_date' => '2020-07-22 06:54:18',
                'client_info_id' => 809,
                'form_sent' => NULL,
                'id' => 621,
                'reason' => 'No other tasks for VA | Feels that it is too early for his business to get a VA - Financial',
                'sub_reason' => NULL,
            ),
            121 => 
            array (
                'cancellation_date' => '2020-07-28 06:59:56',
                'client_info_id' => 816,
                'form_sent' => NULL,
                'id' => 622,
                'reason' => 'We\'ve set up 3 interviews details are as follows:

7/15 - Client no show
7/17 - Rescheduled
7/21 - Showed but disconnected before the 1st candidate was able to join in',
                'sub_reason' => NULL,
            ),
            122 => 
            array (
                'cancellation_date' => '2020-08-15 05:38:39',
                'client_info_id' => 842,
                'form_sent' => NULL,
                'id' => 623,
                'reason' => 'August 10 // Sent Irene dela Cruz\' profile. Client expressed he will not be moving forward. Sent a follow up  question, if we find a stronger candidate, will he be open to an interview. He replied, "Not for the time being"
Sent email and glip to James',
                'sub_reason' => NULL,
            ),
            123 => 
            array (
                'cancellation_date' => '2020-08-20 12:22:59',
                'client_info_id' => 837,
                'form_sent' => NULL,
                'id' => 624,
                'reason' => '8/17/2020 Profiles do not fit criteria client has set',
                'sub_reason' => NULL,
            ),
            124 => 
            array (
                'cancellation_date' => '2020-08-28 12:00:38',
                'client_info_id' => 872,
                'form_sent' => NULL,
                'id' => 625,
                'reason' => 'Lynda Session <lsession@kw.com>
Tue, Aug 25, 5:58 AM 
I called Customer Service today and talked to Nikko.  I wish to cancel my contract.
I am within my 3 (business day period to request cancellation of a contract.
I am not financially able at this time to move forward.',
                    'sub_reason' => NULL,
                ),
                125 => 
                array (
                    'cancellation_date' => '2020-09-10 03:10:42',
                    'client_info_id' => 868,
                    'form_sent' => NULL,
                    'id' => 626,
                    'reason' => 'Sept 3
Unresponsive in Placements',
                    'sub_reason' => NULL,
                ),
                126 => 
                array (
                    'cancellation_date' => '2020-09-24 01:44:44',
                    'client_info_id' => 913,
                    'form_sent' => NULL,
                    'id' => 627,
                    'reason' => 'Sept 22
Notified intention to cancel; hired a local intern; profiles sent were not a fit to her preference',
                    'sub_reason' => NULL,
                ),
                127 => 
                array (
                    'cancellation_date' => '2020-09-24 01:53:07',
                    'client_info_id' => 911,
                    'form_sent' => NULL,
                    'id' => 628,
                    'reason' => NULL,
                    'sub_reason' => NULL,
                ),
                128 => 
                array (
                    'cancellation_date' => '2020-10-08 05:25:09',
                    'client_info_id' => 912,
                    'form_sent' => NULL,
                    'id' => 629,
                    'reason' => 'October 2, 2020 Hired someone in the states but referred VD to another agent in their office who said he already had our info',
                    'sub_reason' => NULL,
                ),
                129 => 
                array (
                    'cancellation_date' => '2020-10-13 03:04:51',
                    'client_info_id' => 939,
                    'form_sent' => NULL,
                    'id' => 630,
                    'reason' => 'Oct 9, 2020
Financial; refinance home; will revisit after the first of the year',
                    'sub_reason' => NULL,
                ),
                130 => 
                array (
                    'cancellation_date' => '2020-12-08 03:04:26',
                    'client_info_id' => 676,
                    'form_sent' => NULL,
                    'id' => 631,
                    'reason' => '7/1/2020 Financial issues | Company was sold',
                    'sub_reason' => NULL,
                ),
                131 => 
                array (
                    'cancellation_date' => '2020-12-17 07:00:07',
                    'client_info_id' => 992,
                    'form_sent' => NULL,
                    'id' => 632,
                    'reason' => NULL,
                    'sub_reason' => NULL,
                ),
                132 => 
                array (
                    'cancellation_date' => '2020-12-22 07:54:44',
                    'client_info_id' => 774,
                    'form_sent' => NULL,
                    'id' => 633,
                    'reason' => 'Client doesn\'t have any tasks for our VA, tried to give TC tasks for the VA but still won\'t work since he already has an inhouse TC.
Effectivity date:  August 6, 2020',
                    'sub_reason' => NULL,
                ),
                133 => 
                array (
                    'cancellation_date' => '2021-02-08 11:15:24',
                    'client_info_id' => 832,
                    'form_sent' => NULL,
                    'id' => 634,
                    'reason' => 'Business/State Law Limitations',
                    'sub_reason' => NULL,
                ),
                134 => 
                array (
                    'cancellation_date' => '2021-05-25 06:27:32',
                    'client_info_id' => 1031,
                    'form_sent' => NULL,
                    'id' => 635,
                    'reason' => NULL,
                    'sub_reason' => NULL,
                ),
                135 => 
                array (
                    'cancellation_date' => '2021-05-25 05:53:53',
                    'client_info_id' => 741,
                    'form_sent' => NULL,
                    'id' => 636,
                    'reason' => NULL,
                    'sub_reason' => NULL,
                ),
                136 => 
                array (
                    'cancellation_date' => '2021-05-25 05:54:26',
                    'client_info_id' => 822,
                    'form_sent' => NULL,
                    'id' => 637,
                    'reason' => NULL,
                    'sub_reason' => NULL,
                ),
                137 => 
                array (
                    'cancellation_date' => '2021-05-25 05:54:57',
                    'client_info_id' => 923,
                    'form_sent' => NULL,
                    'id' => 638,
                    'reason' => NULL,
                    'sub_reason' => NULL,
                ),
                138 => 
                array (
                    'cancellation_date' => '2021-05-25 05:55:26',
                    'client_info_id' => 819,
                    'form_sent' => NULL,
                    'id' => 639,
                    'reason' => NULL,
                    'sub_reason' => NULL,
                ),
                139 => 
                array (
                    'cancellation_date' => '2021-05-27 03:42:53',
                    'client_info_id' => 1080,
                    'form_sent' => NULL,
                    'id' => 640,
                    'reason' => NULL,
                    'sub_reason' => NULL,
                ),
            ));
        
        
    }
}