<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'created_at' => '2019-09-25 13:37:32',
                'guard_name' => 'web',
                'id' => 1,
                'name' => 'view backend',
                'updated_at' => '2019-09-25 13:37:32',
            ),
        ));
        
        
    }
}