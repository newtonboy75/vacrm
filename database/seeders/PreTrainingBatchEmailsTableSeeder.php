<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PreTrainingBatchEmailsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pre_training_batch_emails')->delete();
        
        \DB::table('pre_training_batch_emails')->insert(array (
            0 => 
            array (
                'batch_number' => 'GVA 53',
                'date_created' => '2020-02-27 19:47:44',
                'email_sent' => 'no',
                'end_date' => 'March 13, 2020 08:00 pm - 05:00 am',
                'id' => 2,
                'start_date' => 'March 09, 2020 08:00 pm - 05:00 am',
            ),
            1 => 
            array (
                'batch_number' => 'ISA 105',
                'date_created' => '2020-02-27 19:48:53',
                'email_sent' => 'no',
                'end_date' => 'March 13, 2020 08:00 pm - 05:00 am',
                'id' => 3,
                'start_date' => 'March 09, 2020 08:00 pm - 05:00 am',
            ),
            2 => 
            array (
                'batch_number' => 'EVA 17',
                'date_created' => '2020-02-27 19:49:50',
                'email_sent' => 'no',
                'end_date' => 'March 27, 2020 08:00 pm - 05:00 am',
                'id' => 4,
                'start_date' => 'March 16, 2020 08:00 pm - 05:00 am',
            ),
            3 => 
            array (
                'batch_number' => 'ISA 106',
                'date_created' => '2020-02-27 19:52:43',
                'email_sent' => 'no',
                'end_date' => 'March 20, 2020 08:00 pm - 05:00 am',
                'id' => 5,
                'start_date' => 'March 16, 2020 08:00 pm - 05:00 am',
            ),
            4 => 
            array (
                'batch_number' => 'ISA 107',
                'date_created' => '2020-02-27 22:10:58',
                'email_sent' => 'yes',
                'end_date' => 'May 01, 2020 08:00 pm - 05:00 am',
                'id' => 7,
                'start_date' => 'April 27, 2020 08:00 pm - 05:00 am',
            ),
            5 => 
            array (
                'batch_number' => 'EVA 18',
                'date_created' => '2020-02-27 22:11:44',
                'email_sent' => 'no',
                'end_date' => 'April 17, 2020 08:00 pm - 05:00 am',
                'id' => 8,
                'start_date' => 'April 06, 2020 08:00 pm - 05:00 am',
            ),
            6 => 
            array (
                'batch_number' => 'ISA 108',
                'date_created' => '2020-02-27 22:12:25',
                'email_sent' => 'no',
                'end_date' => 'June 01, 2020 08:00 pm - 05:00 am',
                'id' => 9,
                'start_date' => 'May 26, 2020 08:00 pm - 05:00 am',
            ),
            7 => 
            array (
                'batch_number' => 'EVA 19',
                'date_created' => '2020-02-27 22:14:53',
                'email_sent' => 'no',
                'end_date' => 'May 22, 2020 08:00 pm - 05:00 am',
                'id' => 12,
                'start_date' => 'May 11, 2020 08:00 pm - 05:00 am',
            ),
            8 => 
            array (
                'batch_number' => 'EVA 20',
                'date_created' => '2020-05-11 12:59:17',
                'email_sent' => 'yes',
                'end_date' => 'June 15, 2020 08:00PM - - 05:00 am',
                'id' => 16,
                'start_date' => 'June 02, 2020 08:00PM - - 05:00 am',
            ),
            9 => 
            array (
                'batch_number' => 'ISA 109',
                'date_created' => '2020-05-23 00:12:38',
                'email_sent' => 'no',
                'end_date' => 'June 08, 2020 08:00PM - 05:00 am',
                'id' => 17,
                'start_date' => 'June 02, 2020 08:00PM - 05:00 am',
            ),
            10 => 
            array (
                'batch_number' => 'EVA 21',
                'date_created' => '2020-05-23 00:23:18',
                'email_sent' => 'no',
                'end_date' => 'June 29, 2020 08:00PM - 05:00 am',
                'id' => 18,
                'start_date' => 'June 16, 2020 08:00PM - 05:00 am',
            ),
            11 => 
            array (
                'batch_number' => 'ISA 110',
                'date_created' => '2020-05-23 00:24:36',
                'email_sent' => 'no',
                'end_date' => 'June 22, 2020 08:00PM - 05:00 am',
                'id' => 19,
                'start_date' => 'June 16, 2020 08:00PM - 05:00 am',
            ),
            12 => 
            array (
                'batch_number' => 'EVA 22',
                'date_created' => '2020-06-02 10:56:46',
                'email_sent' => 'no',
                'end_date' => 'July 13, 2020 08:00 pm - 05:00 am',
                'id' => 20,
                'start_date' => 'June 30, 2020 08:00 pm - 05:00 am',
            ),
            13 => 
            array (
                'batch_number' => 'ISA 111',
                'date_created' => '2020-06-02 10:57:21',
                'email_sent' => 'no',
                'end_date' => 'July 06, 2020 08:00 pm - 05:00 am',
                'id' => 21,
                'start_date' => 'June 30, 2020 08:00 pm - 05:00 am',
            ),
            14 => 
            array (
                'batch_number' => 'GVA 54',
                'date_created' => '2020-06-12 16:48:28',
                'email_sent' => 'no',
                'end_date' => 'June 29, 2020 08:00PM - 05:00 am',
                'id' => 22,
                'start_date' => 'June 23, 2020 08:00PM - 05:00 am',
            ),
            15 => 
            array (
                'batch_number' => 'EVA 23',
                'date_created' => '2020-06-17 19:10:36',
                'email_sent' => 'no',
                'end_date' => 'July 27, 2020 08:00PM - 05:00 am',
                'id' => 23,
                'start_date' => 'July 14, 2020 08:00PM - 05:00 am',
            ),
            16 => 
            array (
                'batch_number' => 'ISA 112',
                'date_created' => '2020-06-17 19:11:54',
                'email_sent' => 'no',
                'end_date' => 'July 20, 2020 08:00PM - 05:00 am',
                'id' => 24,
                'start_date' => 'July 14, 2020 08:00PM - 05:00 am',
            ),
            17 => 
            array (
                'batch_number' => 'GVA 56',
                'date_created' => '2020-06-17 19:12:56',
                'email_sent' => 'no',
                'end_date' => 'July 27, 2020 08:00PM - 05:00 am',
                'id' => 25,
                'start_date' => 'July 21, 2020 08:00PM - 05:00 am',
            ),
            18 => 
            array (
                'batch_number' => 'GVA 55',
                'date_created' => '2020-06-23 17:06:03',
                'email_sent' => 'no',
                'end_date' => 'July 13, 2020 08:00PM - 05:00 am',
                'id' => 26,
                'start_date' => 'July 07, 2020 08:00PM - 05:00 am',
            ),
            19 => 
            array (
                'batch_number' => 'EVA 24',
                'date_created' => '2020-07-08 15:14:51',
                'email_sent' => 'no',
                'end_date' => 'August 10, 2020 08:00PM - 05:00 am',
                'id' => 27,
                'start_date' => 'July 28, 2020 08:00PM - 05:00 am',
            ),
            20 => 
            array (
                'batch_number' => 'ISA 113',
                'date_created' => '2020-07-08 15:15:29',
                'email_sent' => 'no',
                'end_date' => 'August 03, 2020 08:00PM - - am undefined',
                'id' => 28,
                'start_date' => 'July 28, 2020 08:00PM - - am undefined',
            ),
            21 => 
            array (
                'batch_number' => 'GVA 57',
                'date_created' => '2020-07-08 15:15:58',
                'email_sent' => 'no',
                'end_date' => 'August 10, 2020 08:00PM - 05:00 am',
                'id' => 29,
                'start_date' => 'August 04, 2020 08:00PM - 05:00 am',
            ),
            22 => 
            array (
                'batch_number' => 'EVA 25',
                'date_created' => '2020-07-10 12:51:49',
                'email_sent' => 'no',
                'end_date' => 'August 24, 2020 08:00PM - 05:00 am',
                'id' => 30,
                'start_date' => 'August 11, 2020 08:00PM - 05:00 am',
            ),
            23 => 
            array (
                'batch_number' => 'ISA 114',
                'date_created' => '2020-07-10 12:52:40',
                'email_sent' => 'no',
                'end_date' => 'August 17, 2020 08:00PM - 05:00 am',
                'id' => 31,
                'start_date' => 'August 11, 2020 08:00PM - 05:00 am',
            ),
            24 => 
            array (
                'batch_number' => 'GVA 58',
                'date_created' => '2020-07-10 12:53:10',
                'email_sent' => 'no',
                'end_date' => 'August 24, 2020 08:00PM - 05:00 am',
                'id' => 32,
                'start_date' => 'August 18, 2020 08:00PM - 05:00 am',
            ),
            25 => 
            array (
                'batch_number' => 'EVA 26',
                'date_created' => '2020-07-10 12:54:15',
                'email_sent' => 'no',
                'end_date' => 'September 08, 2020 08:00 pm - 05:00 am',
                'id' => 33,
                'start_date' => 'August 25, 2020 08:00 pm - 05:00 am',
            ),
            26 => 
            array (
                'batch_number' => 'ISA 115',
                'date_created' => '2020-07-10 12:54:42',
                'email_sent' => 'no',
                'end_date' => 'August 31, 2020 08:00PM - 05:00 am',
                'id' => 34,
                'start_date' => 'August 25, 2020 08:00PM - 05:00 am',
            ),
            27 => 
            array (
                'batch_number' => 'GVA 59',
                'date_created' => '2020-07-10 12:55:20',
                'email_sent' => 'no',
                'end_date' => 'September 08, 2020 08:00PM - 05:00 pm',
                'id' => 35,
                'start_date' => 'September 01, 2020 08:00PM - 05:00 pm',
            ),
            28 => 
            array (
                'batch_number' => 'EVA 27',
                'date_created' => '2020-08-11 20:36:58',
                'email_sent' => 'no',
                'end_date' => 'September 22, 2020 08:00PM - 05:00 am',
                'id' => 36,
                'start_date' => 'September 09, 2020 08:00PM - 05:00 am',
            ),
            29 => 
            array (
                'batch_number' => 'ISA 116',
                'date_created' => '2020-08-11 20:37:51',
                'email_sent' => 'no',
                'end_date' => 'September 15, 2020 08:00 PM - - 5 AM',
                'id' => 37,
                'start_date' => 'September 09, 2020 08:00 PM - - 5 AM',
            ),
            30 => 
            array (
                'batch_number' => 'GVA 60',
                'date_created' => '2020-08-11 20:38:39',
                'email_sent' => 'no',
                'end_date' => 'September 22, 2020 08:00PM - 05:00 am',
                'id' => 38,
                'start_date' => 'September 16, 2020 08:00PM - 05:00 am',
            ),
            31 => 
            array (
                'batch_number' => 'EVA 28',
                'date_created' => '2020-08-30 09:45:49',
                'email_sent' => 'no',
                'end_date' => 'October 06, 2020 08:00PM - 05:00 am',
                'id' => 39,
                'start_date' => 'September 23, 2020 08:00PM - 05:00 am',
            ),
            32 => 
            array (
                'batch_number' => 'ISA 117',
                'date_created' => '2020-08-30 09:46:19',
                'email_sent' => 'no',
                'end_date' => 'September 29, 2020 08:00PM - 05:00 am',
                'id' => 40,
                'start_date' => 'September 23, 2020 08:00PM - 05:00 am',
            ),
            33 => 
            array (
                'batch_number' => 'GVA 61',
                'date_created' => '2020-08-30 09:46:46',
                'email_sent' => 'no',
                'end_date' => 'October 06, 2020 08:00PM - 05:00 am',
                'id' => 41,
                'start_date' => 'September 30, 2020 08:00PM - 05:00 am',
            ),
            34 => 
            array (
                'batch_number' => 'EVA 29',
                'date_created' => '2020-08-30 09:47:26',
                'email_sent' => 'no',
                'end_date' => 'October 20, 2020 08:00PM - 05:00 am',
                'id' => 42,
                'start_date' => 'October 07, 2020 08:00PM - 05:00 am',
            ),
            35 => 
            array (
                'batch_number' => 'GVA 62',
                'date_created' => '2020-08-30 09:48:24',
                'email_sent' => 'no',
                'end_date' => 'October 20, 2020 08:00PM - 05:00 am',
                'id' => 43,
                'start_date' => 'October 14, 2020 08:00PM - 05:00 am',
            ),
            36 => 
            array (
                'batch_number' => 'ISA 118',
                'date_created' => '2020-08-30 09:52:15',
                'email_sent' => 'no',
                'end_date' => 'October 13, 2020 08:00PM - 05:00 am',
                'id' => 44,
                'start_date' => 'October 07, 2020 08:00PM - 05:00 am',
            ),
            37 => 
            array (
                'batch_number' => 'EVA 30',
                'date_created' => '2020-09-22 12:32:35',
                'email_sent' => 'no',
                'end_date' => 'November 03, 2020 08:00PM - 05:00 am',
                'id' => 45,
                'start_date' => 'October 21, 2020 08:00PM - 05:00 am',
            ),
            38 => 
            array (
                'batch_number' => 'ISA 119',
                'date_created' => '2020-09-22 12:33:06',
                'email_sent' => 'no',
                'end_date' => 'October 27, 2020 08:00PM - 05:00 am',
                'id' => 46,
                'start_date' => 'October 21, 2020 08:00PM - 05:00 am',
            ),
            39 => 
            array (
                'batch_number' => 'GVA 63',
                'date_created' => '2020-09-22 12:33:37',
                'email_sent' => 'no',
                'end_date' => 'November 03, 2020 08:00PM - 05:00 am',
                'id' => 47,
                'start_date' => 'October 28, 2020 08:00PM - 05:00 am',
            ),
            40 => 
            array (
                'batch_number' => 'EVA 31',
                'date_created' => '2020-09-23 17:46:48',
                'email_sent' => 'no',
                'end_date' => 'November 17, 2020 09:00 pm - 06:00 am',
                'id' => 48,
                'start_date' => 'November 04, 2020 09:00 pm - 06:00 am',
            ),
            41 => 
            array (
                'batch_number' => 'ISA 120',
                'date_created' => '2020-09-23 17:48:38',
                'email_sent' => 'no',
                'end_date' => 'November 10, 2020 09:00 pm - 06:00 am',
                'id' => 49,
                'start_date' => 'November 04, 2020 09:00 pm - 06:00 am',
            ),
            42 => 
            array (
                'batch_number' => 'GVA 64',
                'date_created' => '2020-09-23 17:49:31',
                'email_sent' => 'no',
                'end_date' => 'November 17, 2020 09:00 pm - 06:00 am',
                'id' => 50,
                'start_date' => 'November 11, 2020 09:00 pm - 06:00 am',
            ),
            43 => 
            array (
                'batch_number' => 'EVA 32',
                'date_created' => '2020-10-22 14:43:10',
                'email_sent' => 'no',
                'end_date' => 'December 02, 2020 09:00 pm - 06:00 am',
                'id' => 51,
                'start_date' => 'November 18, 2020 09:00 pm - 06:00 am',
            ),
            44 => 
            array (
                'batch_number' => 'ISA 121',
                'date_created' => '2020-10-22 14:44:09',
                'email_sent' => 'no',
                'end_date' => 'November 24, 2020 09:00 pm - 06:00 am',
                'id' => 52,
                'start_date' => 'November 18, 2020 09:00 pm - 06:00 am',
            ),
            45 => 
            array (
                'batch_number' => 'GVA 65',
                'date_created' => '2020-10-22 14:45:00',
                'email_sent' => 'no',
                'end_date' => 'December 02, 2020 09:00 pm - 06:00 am',
                'id' => 53,
                'start_date' => 'November 25, 2020 09:00 pm - 06:00 am',
            ),
            46 => 
            array (
                'batch_number' => 'EVA 33',
                'date_created' => '2020-10-22 14:49:55',
                'email_sent' => 'no',
                'end_date' => 'December 16, 2020 09:00 pm - 06:00 am',
                'id' => 54,
                'start_date' => 'December 03, 2020 09:00 pm - 06:00 am',
            ),
            47 => 
            array (
                'batch_number' => 'ISA 122',
                'date_created' => '2020-10-22 14:52:35',
                'email_sent' => 'no',
                'end_date' => 'December 09, 2020 09:00 pm - 06:00 am',
                'id' => 55,
                'start_date' => 'December 03, 2020 09:00 pm - 06:00 am',
            ),
            48 => 
            array (
                'batch_number' => 'GVA 66',
                'date_created' => '2020-10-22 14:53:13',
                'email_sent' => 'no',
                'end_date' => 'December 16, 2020 09:00 pm - 06:00 am',
                'id' => 56,
                'start_date' => 'December 10, 2020 09:00 pm - 06:00 am',
            ),
            49 => 
            array (
                'batch_number' => 'EVA 34',
                'date_created' => '2020-10-22 14:54:46',
                'email_sent' => 'no',
                'end_date' => 'January 05, 2021 09:00 pm - 06:00 am',
                'id' => 57,
                'start_date' => 'December 17, 2020 09:00 pm - 06:00 am',
            ),
            50 => 
            array (
                'batch_number' => 'ISA 123',
                'date_created' => '2020-10-22 14:55:29',
                'email_sent' => 'no',
                'end_date' => 'December 23, 2020 09:00 pm - 06:00 am',
                'id' => 58,
                'start_date' => 'December 17, 2020 09:00 pm - 06:00 am',
            ),
            51 => 
            array (
                'batch_number' => 'GVA 67',
                'date_created' => '2020-10-22 14:56:41',
                'email_sent' => 'no',
                'end_date' => 'January 05, 2021 09:00 pm - 06:00 am',
                'id' => 59,
                'start_date' => 'December 28, 2020 09:00 pm - 06:00 am',
            ),
            52 => 
            array (
                'batch_number' => 'EVA 35',
                'date_created' => '2020-12-16 05:25:54',
                'email_sent' => 'no',
                'end_date' => 'January 19, 2021 09:00 pm - 06:00 am',
                'id' => 60,
                'start_date' => 'January 06, 2021 09:00 pm - 06:00 am',
            ),
            53 => 
            array (
                'batch_number' => 'ISA 124',
                'date_created' => '2020-12-16 05:26:31',
                'email_sent' => 'no',
                'end_date' => 'January 12, 2021 09:00 pm - 06:00 am',
                'id' => 61,
                'start_date' => 'January 06, 2021 09:00 pm - 06:00 am',
            ),
            54 => 
            array (
                'batch_number' => 'GVA 68',
                'date_created' => '2020-12-16 05:26:59',
                'email_sent' => 'no',
                'end_date' => 'January 19, 2021 09:00 pm - 06:00 am',
                'id' => 62,
                'start_date' => 'January 13, 2021 09:00 pm - 06:00 am',
            ),
            55 => 
            array (
                'batch_number' => 'EVA 36',
                'date_created' => '2020-12-16 05:31:05',
                'email_sent' => 'no',
                'end_date' => 'February 02, 2021 09:00 pm - 06:00 am',
                'id' => 63,
                'start_date' => 'January 20, 2021 09:00 pm - 06:00 am',
            ),
            56 => 
            array (
                'batch_number' => 'ISA 125',
                'date_created' => '2020-12-16 05:31:47',
                'email_sent' => 'no',
                'end_date' => 'January 26, 2021 09:00 pm - 06:00 am',
                'id' => 64,
                'start_date' => 'January 20, 2021 09:00 pm - 06:00 am',
            ),
            57 => 
            array (
                'batch_number' => 'GVA 69',
                'date_created' => '2020-12-16 05:32:22',
                'email_sent' => 'no',
                'end_date' => 'February 02, 2021 09:00 pm - 06:00 am',
                'id' => 65,
                'start_date' => 'January 27, 2021 09:00 pm - 06:00 am',
            ),
            58 => 
            array (
                'batch_number' => 'EVA 37',
                'date_created' => '2021-01-12 14:41:08',
                'email_sent' => 'no',
                'end_date' => 'February 16, 2021 09:00 pm - 06:00 am',
                'id' => 66,
                'start_date' => 'February 03, 2021 09:00 pm - 06:00 am',
            ),
            59 => 
            array (
                'batch_number' => 'ISA 126',
                'date_created' => '2021-01-12 14:42:29',
                'email_sent' => 'no',
                'end_date' => 'February 09, 2021 09:00 pm - 06:00 pm',
                'id' => 67,
                'start_date' => 'February 03, 2021 09:00 pm - 06:00 pm',
            ),
            60 => 
            array (
                'batch_number' => 'GVA 70',
                'date_created' => '2021-01-12 14:43:24',
                'email_sent' => 'no',
                'end_date' => 'February 16, 2021 09:00 pm - 06:00 am',
                'id' => 68,
                'start_date' => 'February 10, 2021 09:00 pm - 06:00 am',
            ),
            61 => 
            array (
                'batch_number' => 'EVA 38',
                'date_created' => '2021-01-12 14:45:23',
                'email_sent' => 'no',
                'end_date' => 'March 02, 2021 09:00 pm - 06:00 am',
                'id' => 69,
                'start_date' => 'February 17, 2021 09:00 pm - 06:00 am',
            ),
            62 => 
            array (
                'batch_number' => 'ISA 127',
                'date_created' => '2021-01-12 14:46:08',
                'email_sent' => 'no',
                'end_date' => 'February 23, 2021 09:00 pm - 06:00 am',
                'id' => 70,
                'start_date' => 'February 17, 2021 09:00 pm - 06:00 am',
            ),
            63 => 
            array (
                'batch_number' => 'GVA 71',
                'date_created' => '2021-01-12 14:47:16',
                'email_sent' => 'no',
                'end_date' => 'March 02, 2021 09:00 pm - 06:00 am',
                'id' => 71,
                'start_date' => 'February 24, 2021 09:00 pm - 06:00 am',
            ),
            64 => 
            array (
                'batch_number' => 'EVA 39',
                'date_created' => '2021-02-02 14:47:36',
                'email_sent' => 'no',
                'end_date' => 'March 16, 2021 09:00 pm - 06:00 am',
                'id' => 72,
                'start_date' => 'March 03, 2021 09:00 pm - 06:00 am',
            ),
            65 => 
            array (
                'batch_number' => 'ISA 128',
                'date_created' => '2021-02-02 14:48:19',
                'email_sent' => 'no',
                'end_date' => 'March 09, 2021 09:00 pm - 06:00 am',
                'id' => 73,
                'start_date' => 'March 03, 2021 09:00 pm - 06:00 am',
            ),
            66 => 
            array (
                'batch_number' => 'GVA 72',
                'date_created' => '2021-02-02 14:49:01',
                'email_sent' => 'no',
                'end_date' => 'March 16, 2021 09:00 pm - 06:00 am',
                'id' => 74,
                'start_date' => 'March 10, 2021 09:00 pm - 06:00 am',
            ),
            67 => 
            array (
                'batch_number' => 'EVA 40',
                'date_created' => '2021-02-02 14:49:36',
                'email_sent' => 'no',
                'end_date' => 'March 30, 2021 09:00 pm - 06:00 am',
                'id' => 75,
                'start_date' => 'March 17, 2021 09:00 pm - 06:00 am',
            ),
            68 => 
            array (
                'batch_number' => 'ISA 129',
                'date_created' => '2021-02-02 14:50:09',
                'email_sent' => 'no',
                'end_date' => 'March 23, 2021 09:00 pm - 06:00 am',
                'id' => 76,
                'start_date' => 'March 17, 2021 09:00 pm - 06:00 am',
            ),
            69 => 
            array (
                'batch_number' => 'GVA 73',
                'date_created' => '2021-02-02 14:50:48',
                'email_sent' => 'no',
                'end_date' => 'March 30, 2021 08:00 PM - 05:00 AM',
                'id' => 77,
                'start_date' => 'March 24, 2021 08:00 PM - 05:00 AM',
            ),
            70 => 
            array (
                'batch_number' => 'EVA 41',
                'date_created' => '2021-03-03 23:55:17',
                'email_sent' => 'no',
                'end_date' => 'April 13, 2021 08:00PM - - 05:00 am',
                'id' => 78,
                'start_date' => 'March 31, 2021 08:00PM - - 05:00 am',
            ),
            71 => 
            array (
                'batch_number' => 'ISA 130',
                'date_created' => '2021-03-03 23:56:07',
                'email_sent' => 'no',
                'end_date' => 'April 06, 2021 08:00 PM - 05:00 AM',
                'id' => 79,
                'start_date' => 'March 31, 2021 08:00 PM - 05:00 AM',
            ),
            72 => 
            array (
                'batch_number' => 'GVA 74',
                'date_created' => '2021-03-03 23:57:28',
                'email_sent' => 'no',
                'end_date' => 'March 30, 2021 08:00PM - - 05:00 am',
                'id' => 80,
                'start_date' => 'March 24, 2021 08:00PM - - 05:00 am',
            ),
            73 => 
            array (
                'batch_number' => 'EVA 42',
                'date_created' => '2021-03-03 23:59:05',
                'email_sent' => 'no',
                'end_date' => 'April 27, 2021 08:00PM - - 05:00 am',
                'id' => 81,
                'start_date' => 'April 14, 2021 08:00PM - - 05:00 am',
            ),
            74 => 
            array (
                'batch_number' => 'ISA 131',
                'date_created' => '2021-03-03 23:59:51',
                'email_sent' => 'no',
                'end_date' => 'April 13, 2021 08:00PM - - 05:00 am',
                'id' => 82,
                'start_date' => 'April 07, 2021 08:00PM - - 05:00 am',
            ),
            75 => 
            array (
                'batch_number' => 'GVA 75',
                'date_created' => '2021-03-04 00:00:25',
                'email_sent' => 'no',
                'end_date' => 'April 06, 2021 08:00PM - - 05:00 am',
                'id' => 83,
                'start_date' => 'March 31, 2021 08:00PM - - 05:00 am',
            ),
            76 => 
            array (
                'batch_number' => 'ISA 132',
                'date_created' => '2021-03-18 21:54:06',
                'email_sent' => 'no',
                'end_date' => 'April 20, 2021 08:00 pm - 05:00 am',
                'id' => 84,
                'start_date' => 'April 14, 2021 08:00 pm - 05:00 am',
            ),
            77 => 
            array (
                'batch_number' => 'EVA 43',
                'date_created' => '2021-03-18 21:56:07',
                'email_sent' => 'no',
                'end_date' => 'May 04, 2021 08:00 pm - 05:00 am',
                'id' => 85,
                'start_date' => 'April 21, 2021 08:00 pm - 05:00 am',
            ),
            78 => 
            array (
                'batch_number' => 'ISA 133',
                'date_created' => '2021-03-18 21:57:01',
                'email_sent' => 'no',
                'end_date' => 'April 27, 2021 08:00 pm - 05:00 am',
                'id' => 86,
                'start_date' => 'April 21, 2021 08:00 pm - 05:00 am',
            ),
            79 => 
            array (
                'batch_number' => 'GVA 76',
                'date_created' => '2021-03-18 21:58:16',
                'email_sent' => 'no',
                'end_date' => 'April 13, 2021 08:00 pm - 05:00 am',
                'id' => 87,
                'start_date' => 'April 07, 2021 08:00 pm - 05:00 am',
            ),
            80 => 
            array (
                'batch_number' => 'ISA 134',
                'date_created' => '2021-03-18 22:15:08',
                'email_sent' => 'yes',
                'end_date' => 'April 27, 2021 08:00 pm - 05:00 am',
                'id' => 88,
                'start_date' => 'April 23, 2021 08:00 pm - 05:00 am',
            ),
            81 => 
            array (
                'batch_number' => 'ISA 135',
                'date_created' => '2021-03-18 22:16:03',
                'email_sent' => 'no',
                'end_date' => 'May 04, 2021 08:00 pm - 05:00 am',
                'id' => 89,
                'start_date' => 'April 28, 2021 08:00 pm - 05:00 am',
            ),
            82 => 
            array (
                'batch_number' => 'GVA 77',
                'date_created' => '2021-03-19 20:31:43',
                'email_sent' => 'no',
                'end_date' => 'April 20, 2021 08:00 pm - 05:00 am',
                'id' => 90,
                'start_date' => 'April 14, 2021 08:00 pm - 05:00 am',
            ),
            83 => 
            array (
                'batch_number' => 'EVA 44',
                'date_created' => '2021-03-19 20:33:42',
                'email_sent' => 'no',
                'end_date' => 'May 09, 2021 08:00 pm - 05:00 am',
                'id' => 91,
                'start_date' => 'April 28, 2021 08:00 pm - 05:00 am',
            ),
            84 => 
            array (
                'batch_number' => 'GVA 78',
                'date_created' => '2021-03-19 20:35:14',
                'email_sent' => 'no',
                'end_date' => 'April 27, 2021 08:00 pm - 05:00 am',
                'id' => 92,
                'start_date' => 'April 21, 2021 08:00 pm - 05:00 am',
            ),
            85 => 
            array (
                'batch_number' => 'GVA 79',
                'date_created' => '2021-03-22 12:26:31',
                'email_sent' => 'no',
                'end_date' => 'May 04, 2021 08:00 pm - 05:00 am',
                'id' => 93,
                'start_date' => 'April 28, 2021 08:00 pm - 05:00 am',
            ),
            86 => 
            array (
                'batch_number' => 'EVA 45',
                'date_created' => '2021-04-05 12:44:26',
                'email_sent' => 'no',
                'end_date' => 'May 18, 2021 08:00 pm - 05:00 am',
                'id' => 94,
                'start_date' => 'May 05, 2021 08:00 pm - 05:00 am',
            ),
            87 => 
            array (
                'batch_number' => 'EVA 46',
                'date_created' => '2021-04-05 12:47:04',
                'email_sent' => 'no',
                'end_date' => 'May 23, 2021 08:00 pm - 05:00 am',
                'id' => 95,
                'start_date' => 'May 12, 2021 08:00 pm - 05:00 am',
            ),
            88 => 
            array (
                'batch_number' => 'EVA 47',
                'date_created' => '2021-04-05 12:48:25',
                'email_sent' => 'no',
                'end_date' => 'June 02, 2021 08:00 pm - 05:00 am',
                'id' => 96,
                'start_date' => 'May 19, 2021 08:00 pm - 05:00 am',
            ),
            89 => 
            array (
                'batch_number' => 'EVA 48',
                'date_created' => '2021-04-05 12:49:10',
                'email_sent' => 'no',
                'end_date' => 'June 06, 2021 08:00 pm - 05:00 am',
                'id' => 97,
                'start_date' => 'May 26, 2021 08:00 pm - 05:00 am',
            ),
            90 => 
            array (
                'batch_number' => 'EVA 49',
                'date_created' => '2021-04-05 12:49:44',
                'email_sent' => 'no',
                'end_date' => 'June 16, 2021 08:00 pm - 05:00 am',
                'id' => 98,
                'start_date' => 'June 03, 2021 08:00 pm - 05:00 am',
            ),
            91 => 
            array (
                'batch_number' => 'EVA 50',
                'date_created' => '2021-04-05 12:50:17',
                'email_sent' => 'no',
                'end_date' => 'June 20, 2021 08:00 pm - 05:00 am',
                'id' => 99,
                'start_date' => 'June 09, 2021 08:00 pm - 05:00 am',
            ),
            92 => 
            array (
                'batch_number' => 'ISA 136',
                'date_created' => '2021-04-05 12:53:07',
                'email_sent' => 'no',
                'end_date' => 'May 04, 2021 08:00 pm - 05:00 am',
                'id' => 100,
                'start_date' => 'April 30, 2021 08:00 pm - 05:00 am',
            ),
            93 => 
            array (
                'batch_number' => 'ISA 137',
                'date_created' => '2021-04-05 12:53:44',
                'email_sent' => 'no',
                'end_date' => 'May 11, 2021 08:00 pm - 05:00 am',
                'id' => 101,
                'start_date' => 'May 05, 2021 08:00 pm - 05:00 am',
            ),
            94 => 
            array (
                'batch_number' => 'ISA 138',
                'date_created' => '2021-04-05 12:54:21',
                'email_sent' => 'no',
                'end_date' => 'May 11, 2021 08:00 pm - 05:00 am',
                'id' => 102,
                'start_date' => 'May 07, 2021 08:00 pm - 05:00 am',
            ),
            95 => 
            array (
                'batch_number' => 'ISA 139',
                'date_created' => '2021-04-05 12:54:53',
                'email_sent' => 'no',
                'end_date' => 'May 18, 2021 08:00 pm - 05:00 am',
                'id' => 103,
                'start_date' => 'May 12, 2021 08:00 pm - 05:00 am',
            ),
            96 => 
            array (
                'batch_number' => 'ISA 140',
                'date_created' => '2021-04-05 12:55:23',
                'email_sent' => 'no',
                'end_date' => 'May 18, 2021 08:00 pm - 05:00 am',
                'id' => 104,
                'start_date' => 'May 14, 2021 08:00 pm - 05:00 am',
            ),
            97 => 
            array (
                'batch_number' => 'GVA 80',
                'date_created' => '2021-04-05 12:56:02',
                'email_sent' => 'no',
                'end_date' => 'May 11, 2021 08:00 pm - 05:00 am',
                'id' => 105,
                'start_date' => 'May 05, 2021 08:00 pm - 05:00 am',
            ),
            98 => 
            array (
                'batch_number' => 'GVA 81',
                'date_created' => '2021-04-05 12:56:34',
                'email_sent' => 'no',
                'end_date' => 'May 18, 2021 08:00 pm - 05:00 am',
                'id' => 106,
                'start_date' => 'May 12, 2021 08:00 pm - 05:00 am',
            ),
            99 => 
            array (
                'batch_number' => 'GVA 82',
                'date_created' => '2021-04-05 12:57:10',
                'email_sent' => 'no',
                'end_date' => 'May 25, 2021 08:00 pm - 05:00 am',
                'id' => 107,
                'start_date' => 'May 19, 2021 08:00 pm - 05:00 am',
            ),
            100 => 
            array (
                'batch_number' => 'GVA 83',
                'date_created' => '2021-04-05 12:57:47',
                'email_sent' => 'no',
                'end_date' => 'June 02, 2021 08:00 pm - 05:00 am',
                'id' => 108,
                'start_date' => 'May 26, 2021 08:00 pm - 05:00 am',
            ),
            101 => 
            array (
                'batch_number' => 'GVA 84',
                'date_created' => '2021-04-05 12:58:17',
                'email_sent' => 'no',
                'end_date' => 'June 09, 2021 08:00 pm - 05:00 am',
                'id' => 109,
                'start_date' => 'June 03, 2021 08:00 pm - 05:00 am',
            ),
            102 => 
            array (
                'batch_number' => 'GVA 85',
                'date_created' => '2021-04-05 12:58:47',
                'email_sent' => 'no',
                'end_date' => 'June 16, 2021 08:00 pm - 05:00 am',
                'id' => 110,
                'start_date' => 'June 10, 2021 08:00 pm - 05:00 am',
            ),
            103 => 
            array (
                'batch_number' => 'EVA 51',
                'date_created' => '2021-05-13 13:24:28',
                'email_sent' => 'no',
                'end_date' => 'June 30, 2021 08:00 pm - 05:00 am',
                'id' => 111,
                'start_date' => 'June 17, 2021 08:00 pm - 05:00 am',
            ),
            104 => 
            array (
                'batch_number' => 'EVA 52',
                'date_created' => '2021-05-13 13:25:53',
                'email_sent' => 'no',
                'end_date' => 'July 04, 2021 08:00 pm - 05:00 am',
                'id' => 112,
                'start_date' => 'June 23, 2021 08:00 pm - 05:00 am',
            ),
            105 => 
            array (
                'batch_number' => 'EVA 53',
                'date_created' => '2021-05-13 13:26:38',
                'email_sent' => 'no',
                'end_date' => 'July 14, 2021 08:00 pm - 05:00 am',
                'id' => 113,
                'start_date' => 'July 01, 2021 08:00 pm - 05:00 am',
            ),
            106 => 
            array (
                'batch_number' => 'GVA 86',
                'date_created' => '2021-05-13 13:28:12',
                'email_sent' => 'no',
                'end_date' => 'June 23, 2021 08:00 pm - 05:00 am',
                'id' => 114,
                'start_date' => 'June 17, 2021 08:00 pm - 05:00 am',
            ),
            107 => 
            array (
                'batch_number' => 'GVA 87',
                'date_created' => '2021-05-13 13:28:48',
                'email_sent' => 'no',
                'end_date' => 'June 30, 2021 08:00 pm - 05:00 am',
                'id' => 115,
                'start_date' => 'June 24, 2021 08:00 pm - 05:00 am',
            ),
            108 => 
            array (
                'batch_number' => 'GVA 88',
                'date_created' => '2021-05-13 13:29:30',
                'email_sent' => 'no',
                'end_date' => 'July 07, 2021 08:00 pm - 05:00 am',
                'id' => 116,
                'start_date' => 'July 01, 2021 08:00 pm - 05:00 am',
            ),
            109 => 
            array (
                'batch_number' => 'ISA 141',
                'date_created' => '2021-05-13 13:30:24',
                'email_sent' => 'no',
                'end_date' => 'May 25, 2021 08:00 pm - 05:00 am',
                'id' => 117,
                'start_date' => 'May 19, 2021 08:00 pm - 05:00 am',
            ),
            110 => 
            array (
                'batch_number' => 'ISA 142',
                'date_created' => '2021-05-13 13:31:21',
                'email_sent' => 'no',
                'end_date' => 'May 25, 2021 08:00 pm - 05:00 am',
                'id' => 118,
                'start_date' => 'May 21, 2021 08:00 pm - 05:00 am',
            ),
            111 => 
            array (
                'batch_number' => 'ISA 143',
                'date_created' => '2021-05-13 13:32:02',
                'email_sent' => 'no',
                'end_date' => 'June 02, 2021 08:00 pm - 05:00 am',
                'id' => 119,
                'start_date' => 'May 26, 2021 08:00 pm - 05:00 am',
            ),
            112 => 
            array (
                'batch_number' => 'ISA 144',
                'date_created' => '2021-05-13 13:32:41',
                'email_sent' => 'no',
                'end_date' => 'June 04, 2021 08:00 pm - 05:00 am',
                'id' => 120,
                'start_date' => 'May 28, 2021 08:00 pm - 05:00 am',
            ),
            113 => 
            array (
                'batch_number' => 'ISA 145',
                'date_created' => '2021-05-13 13:33:15',
                'email_sent' => 'no',
                'end_date' => 'June 09, 2021 08:00 pm - 05:00 am',
                'id' => 121,
                'start_date' => 'June 03, 2021 08:00 pm - 05:00 am',
            ),
            114 => 
            array (
                'batch_number' => 'ISA 146',
                'date_created' => '2021-05-13 13:33:52',
                'email_sent' => 'no',
                'end_date' => 'June 09, 2021 08:00 pm - 05:00 am',
                'id' => 122,
                'start_date' => 'June 05, 2021 08:00 pm - 05:00 am',
            ),
            115 => 
            array (
                'batch_number' => 'ISA 147',
                'date_created' => '2021-05-13 13:34:23',
                'email_sent' => 'no',
                'end_date' => 'June 16, 2021 08:00 pm - 05:00 am',
                'id' => 123,
                'start_date' => 'June 10, 2021 08:00 pm - 05:00 am',
            ),
            116 => 
            array (
                'batch_number' => 'ISA 148',
                'date_created' => '2021-05-13 13:34:56',
                'email_sent' => 'no',
                'end_date' => 'June 16, 2021 08:00 pm - 05:00 am',
                'id' => 124,
                'start_date' => 'June 12, 2021 08:00 pm - 05:00 am',
            ),
            117 => 
            array (
                'batch_number' => 'ISA 149',
                'date_created' => '2021-05-13 13:35:25',
                'email_sent' => 'no',
                'end_date' => 'June 23, 2021 08:00 pm - 05:00 am',
                'id' => 125,
                'start_date' => 'June 17, 2021 08:00 pm - 05:00 am',
            ),
            118 => 
            array (
                'batch_number' => 'ISA 150',
                'date_created' => '2021-05-13 13:36:00',
                'email_sent' => 'no',
                'end_date' => 'June 23, 2021 08:00 pm - 05:00 am',
                'id' => 126,
                'start_date' => 'June 19, 2021 08:00 pm - 05:00 am',
            ),
            119 => 
            array (
                'batch_number' => 'ISA 151',
                'date_created' => '2021-05-13 13:36:37',
                'email_sent' => 'no',
                'end_date' => 'June 30, 2021 08:00 pm - 05:00 am',
                'id' => 127,
                'start_date' => 'June 24, 2021 08:00 pm - 05:00 am',
            ),
            120 => 
            array (
                'batch_number' => 'ISA 152',
                'date_created' => '2021-05-13 13:37:20',
                'email_sent' => 'no',
                'end_date' => 'June 30, 2021 08:00 pm - 05:00 am',
                'id' => 128,
                'start_date' => 'June 26, 2021 08:00 pm - 05:00 am',
            ),
            121 => 
            array (
                'batch_number' => 'ISA 153',
                'date_created' => '2021-05-28 17:56:30',
                'email_sent' => 'no',
                'end_date' => 'July 07, 2021 08:00 pm - 05:00 am',
                'id' => 129,
                'start_date' => 'July 01, 2021 08:00 pm - 05:00 am',
            ),
            122 => 
            array (
                'batch_number' => 'ISA 154',
                'date_created' => '2021-05-28 18:31:25',
                'email_sent' => 'no',
                'end_date' => 'July 07, 2021 08:00 pm - 05:00 am',
                'id' => 130,
                'start_date' => 'July 03, 2021 08:00 pm - 05:00 am',
            ),
            123 => 
            array (
                'batch_number' => 'ISA 155',
                'date_created' => '2021-05-28 18:32:36',
                'email_sent' => 'no',
                'end_date' => 'July 14, 2021 08:00 pm - 05:00 am',
                'id' => 131,
                'start_date' => 'July 08, 2021 08:00 pm - 05:00 am',
            ),
            124 => 
            array (
                'batch_number' => 'ISA 156',
                'date_created' => '2021-05-28 18:33:12',
                'email_sent' => 'no',
                'end_date' => 'July 14, 2021 08:00 pm - 05:00 am',
                'id' => 132,
                'start_date' => 'July 10, 2021 08:00 pm - 05:00 am',
            ),
            125 => 
            array (
                'batch_number' => 'ISA 157',
                'date_created' => '2021-05-28 18:34:02',
                'email_sent' => 'no',
                'end_date' => 'July 21, 2021 08:00 pm - 05:00 am',
                'id' => 133,
                'start_date' => 'July 15, 2021 08:00 pm - 05:00 am',
            ),
            126 => 
            array (
                'batch_number' => 'ISA 158',
                'date_created' => '2021-05-28 18:34:43',
                'email_sent' => 'no',
                'end_date' => 'July 21, 2021 08:00 pm - 05:00 am',
                'id' => 134,
                'start_date' => 'July 17, 2021 08:00 pm - 05:00 am',
            ),
            127 => 
            array (
                'batch_number' => 'ISA 159',
                'date_created' => '2021-05-28 18:35:30',
                'email_sent' => 'no',
                'end_date' => 'July 28, 2021 08:00 pm - 05:00 am',
                'id' => 135,
                'start_date' => 'July 22, 2021 08:00 pm - 05:00 am',
            ),
            128 => 
            array (
                'batch_number' => 'ISA 160',
                'date_created' => '2021-05-28 18:36:09',
                'email_sent' => 'no',
                'end_date' => 'July 28, 2021 08:00 pm - 05:00 am',
                'id' => 136,
                'start_date' => 'July 24, 2021 08:00 pm - 05:00 am',
            ),
            129 => 
            array (
                'batch_number' => 'ISA 161',
                'date_created' => '2021-05-28 18:36:53',
                'email_sent' => 'no',
                'end_date' => 'August 04, 2021 08:00 pm - 05:00 am',
                'id' => 137,
                'start_date' => 'July 29, 2021 08:00 pm - 05:00 am',
            ),
            130 => 
            array (
                'batch_number' => 'ISA 162',
                'date_created' => '2021-05-28 18:37:41',
                'email_sent' => 'no',
                'end_date' => 'August 04, 2021 08:00 pm - 05:00 am',
                'id' => 138,
                'start_date' => 'July 31, 2021 08:00 pm - 05:00 am',
            ),
            131 => 
            array (
                'batch_number' => 'ISA 163',
                'date_created' => '2021-05-28 18:38:43',
                'email_sent' => 'no',
                'end_date' => 'August 11, 2021 08:00 pm - 05:00 am',
                'id' => 139,
                'start_date' => 'August 05, 2021 08:00 pm - 05:00 am',
            ),
            132 => 
            array (
                'batch_number' => 'ISA 164',
                'date_created' => '2021-05-28 18:39:21',
                'email_sent' => 'no',
                'end_date' => 'August 11, 2021 08:00 pm - 05:00 am',
                'id' => 140,
                'start_date' => 'August 07, 2021 08:00 pm - 05:00 am',
            ),
            133 => 
            array (
                'batch_number' => 'ISA 165',
                'date_created' => '2021-05-28 18:40:05',
                'email_sent' => 'no',
                'end_date' => 'August 18, 2021 08:00 pm - 05:00 am',
                'id' => 141,
                'start_date' => 'August 12, 2021 08:00 pm - 05:00 am',
            ),
            134 => 
            array (
                'batch_number' => 'ISA 166',
                'date_created' => '2021-05-28 18:40:49',
                'email_sent' => 'no',
                'end_date' => 'August 18, 2021 08:00 pm - 05:00 am',
                'id' => 142,
                'start_date' => 'August 14, 2021 08:00 pm - 05:00 am',
            ),
            135 => 
            array (
                'batch_number' => 'ISA 167',
                'date_created' => '2021-05-28 18:41:49',
                'email_sent' => 'no',
                'end_date' => 'August 25, 2021 08:00 pm - 05:00 am',
                'id' => 143,
                'start_date' => 'August 19, 2021 08:00 pm - 05:00 am',
            ),
            136 => 
            array (
                'batch_number' => 'ISA 168',
                'date_created' => '2021-05-28 18:42:29',
                'email_sent' => 'no',
                'end_date' => 'August 25, 2021 08:00 pm - 05:00 am',
                'id' => 144,
                'start_date' => 'August 21, 2021 08:00 pm - 05:00 am',
            ),
            137 => 
            array (
                'batch_number' => 'ISA 169',
                'date_created' => '2021-05-28 18:43:06',
                'email_sent' => 'no',
                'end_date' => 'September 01, 2021 08:00 pm - 05:00 am',
                'id' => 145,
                'start_date' => 'August 26, 2021 08:00 pm - 05:00 am',
            ),
            138 => 
            array (
                'batch_number' => 'EVA 54',
                'date_created' => '2021-05-28 18:49:33',
                'email_sent' => 'no',
                'end_date' => 'July 18, 2021 08:00 pm - 05:00 am',
                'id' => 146,
                'start_date' => 'July 07, 2021 08:00 pm - 05:00 am',
            ),
            139 => 
            array (
                'batch_number' => 'EVA 55',
                'date_created' => '2021-05-28 18:50:20',
                'email_sent' => 'no',
                'end_date' => 'July 28, 2021 08:00 pm - 05:00 am',
                'id' => 147,
                'start_date' => 'July 15, 2021 08:00 pm - 05:00 am',
            ),
            140 => 
            array (
                'batch_number' => 'EVA 56',
                'date_created' => '2021-05-28 18:51:22',
                'email_sent' => 'no',
                'end_date' => 'August 01, 2021 08:00 pm - 05:00 am',
                'id' => 148,
                'start_date' => 'July 21, 2021 08:00 pm - 05:00 am',
            ),
            141 => 
            array (
                'batch_number' => 'EVA 57',
                'date_created' => '2021-05-28 18:52:28',
                'email_sent' => 'no',
                'end_date' => 'August 11, 2021 08:00 pm - 05:00 am',
                'id' => 149,
                'start_date' => 'July 29, 2021 08:00 pm - 05:00 am',
            ),
            142 => 
            array (
                'batch_number' => 'EVA 58',
                'date_created' => '2021-05-28 18:53:46',
                'email_sent' => 'no',
                'end_date' => 'August 15, 2021 08:00 pm - 05:00 am',
                'id' => 150,
                'start_date' => 'August 04, 2021 08:00 pm - 05:00 am',
            ),
            143 => 
            array (
                'batch_number' => 'EVA 59',
                'date_created' => '2021-05-28 18:54:25',
                'email_sent' => 'no',
                'end_date' => 'August 25, 2021 08:00 pm - 05:00 am',
                'id' => 151,
                'start_date' => 'August 12, 2021 08:00 pm - 05:00 am',
            ),
            144 => 
            array (
                'batch_number' => 'EVA 60',
                'date_created' => '2021-05-28 18:55:35',
                'email_sent' => 'no',
                'end_date' => 'August 29, 2021 08:00 pm - 05:00 am',
                'id' => 152,
                'start_date' => 'August 18, 2021 08:00 pm - 05:00 am',
            ),
            145 => 
            array (
                'batch_number' => 'EVA 61',
                'date_created' => '2021-05-28 18:57:45',
                'email_sent' => 'no',
                'end_date' => 'September 09, 2021 08:00 pm - 05:00 am',
                'id' => 153,
                'start_date' => 'August 26, 2021 08:00 pm - 05:00 am',
            ),
            146 => 
            array (
                'batch_number' => 'EVA 62',
                'date_created' => '2021-05-28 18:58:59',
                'email_sent' => 'no',
                'end_date' => 'September 12, 2021 08:00 pm - 05:00 am',
                'id' => 154,
                'start_date' => 'September 01, 2021 08:00 pm - 05:00 am',
            ),
            147 => 
            array (
                'batch_number' => 'GVA 89',
                'date_created' => '2021-05-28 19:11:17',
                'email_sent' => 'no',
                'end_date' => 'July 14, 2021 08:00 pm - 05:00 am',
                'id' => 155,
                'start_date' => 'July 08, 2021 08:00 pm - 05:00 am',
            ),
            148 => 
            array (
                'batch_number' => 'GVA 90',
                'date_created' => '2021-05-28 19:12:52',
                'email_sent' => 'no',
                'end_date' => 'July 21, 2021 08:00 pm - 05:00 am',
                'id' => 156,
                'start_date' => 'July 15, 2021 08:00 pm - 05:00 am',
            ),
            149 => 
            array (
                'batch_number' => 'GVA 91',
                'date_created' => '2021-05-28 19:14:03',
                'email_sent' => 'no',
                'end_date' => 'July 28, 2021 08:00 pm - 05:00 am',
                'id' => 157,
                'start_date' => 'July 22, 2021 08:00 pm - 05:00 am',
            ),
            150 => 
            array (
                'batch_number' => 'GVA 92',
                'date_created' => '2021-05-28 19:14:45',
                'email_sent' => 'no',
                'end_date' => 'August 04, 2021 08:00 pm - 05:00 am',
                'id' => 158,
                'start_date' => 'July 29, 2021 08:00 pm - 05:00 am',
            ),
            151 => 
            array (
                'batch_number' => 'GVA 93',
                'date_created' => '2021-05-28 19:17:00',
                'email_sent' => 'no',
                'end_date' => 'August 11, 2021 08:00 pm - 05:00 am',
                'id' => 159,
                'start_date' => 'August 05, 2021 08:00 pm - 05:00 am',
            ),
            152 => 
            array (
                'batch_number' => 'GVA 94',
                'date_created' => '2021-05-28 19:19:32',
                'email_sent' => 'no',
                'end_date' => 'August 18, 2021 08:00 pm - 05:00 am',
                'id' => 160,
                'start_date' => 'August 12, 2021 08:00 pm - 05:00 am',
            ),
            153 => 
            array (
                'batch_number' => 'GVA 95',
                'date_created' => '2021-05-28 19:20:07',
                'email_sent' => 'no',
                'end_date' => 'August 25, 2021 08:00 pm - 05:00 am',
                'id' => 161,
                'start_date' => 'August 19, 2021 08:00 pm - 05:00 am',
            ),
            154 => 
            array (
                'batch_number' => 'GVA 96',
                'date_created' => '2021-05-28 19:20:49',
                'email_sent' => 'no',
                'end_date' => 'September 01, 2021 08:00 pm - 05:00 am',
                'id' => 162,
                'start_date' => 'August 26, 2021 08:00 pm - 05:00 am',
            ),
            155 => 
            array (
                'batch_number' => 'GVA 97',
                'date_created' => '2021-05-28 19:21:26',
                'email_sent' => 'no',
                'end_date' => 'September 09, 2021 08:00 pm - 05:00 am',
                'id' => 163,
                'start_date' => 'September 02, 2021 08:00 pm - 05:00 am',
            ),
            156 => 
            array (
                'batch_number' => 'GVA 98',
                'date_created' => '2021-05-28 19:22:10',
                'email_sent' => 'no',
                'end_date' => 'September 16, 2021 08:00 pm - 05:00 am',
                'id' => 164,
                'start_date' => 'September 10, 2021 08:00 pm - 05:00 am',
            ),
            157 => 
            array (
                'batch_number' => 'GVA 99',
                'date_created' => '2021-05-28 19:23:14',
                'email_sent' => 'no',
                'end_date' => 'September 23, 2021 08:00 pm - 05:00 am',
                'id' => 165,
                'start_date' => 'September 17, 2021 08:00 pm - 05:00 am',
            ),
            158 => 
            array (
                'batch_number' => 'GVA 100',
                'date_created' => '2021-05-28 19:23:52',
                'email_sent' => 'no',
                'end_date' => 'September 30, 2021 08:00 pm - 05:00 am',
                'id' => 166,
                'start_date' => 'September 24, 2021 08:00 pm - 05:00 am',
            ),
            159 => 
            array (
                'batch_number' => 'EVA 63',
                'date_created' => '2021-06-16 14:57:52',
                'email_sent' => 'no',
                'end_date' => 'September 23, 2021 08:00 pm - 05:00 am',
                'id' => 167,
                'start_date' => 'September 10, 2021 08:00 pm - 05:00 am',
            ),
            160 => 
            array (
                'batch_number' => 'EVA 64',
                'date_created' => '2021-06-16 14:58:52',
                'email_sent' => 'no',
                'end_date' => 'September 26, 2021 08:00 pm - 05:00 am',
                'id' => 168,
                'start_date' => 'September 15, 2021 08:00 pm - 05:00 am',
            ),
            161 => 
            array (
                'batch_number' => 'EVA 65',
                'date_created' => '2021-06-16 14:59:34',
                'email_sent' => 'no',
                'end_date' => 'October 07, 2021 08:00 pm - 05:00 am',
                'id' => 169,
                'start_date' => 'September 24, 2021 08:00 pm - 05:00 am',
            ),
            162 => 
            array (
                'batch_number' => 'EVA 66',
                'date_created' => '2021-06-16 15:00:21',
                'email_sent' => 'no',
                'end_date' => 'October 10, 2021 08:00 pm - 05:00 am',
                'id' => 170,
                'start_date' => 'September 29, 2021 08:00 pm - 05:00 am',
            ),
            163 => 
            array (
                'batch_number' => 'ISA 170',
                'date_created' => '2021-07-29 15:28:41',
                'email_sent' => 'no',
                'end_date' => 'September 01, 2021 08:00PM - 05:00 am',
                'id' => 171,
                'start_date' => 'August 28, 2021 08:00PM - 05:00 am',
            ),
            164 => 
            array (
                'batch_number' => 'ISA 171',
                'date_created' => '2021-07-29 17:28:29',
                'email_sent' => 'no',
                'end_date' => 'September 09, 2021 08:00 pm - 05:00 am',
                'id' => 172,
                'start_date' => 'September 02, 2021 08:00 pm - 05:00 am',
            ),
            165 => 
            array (
                'batch_number' => 'ISA 172',
                'date_created' => '2021-07-29 17:29:15',
                'email_sent' => 'no',
                'end_date' => 'September 11, 2021 08:00 pm - 05:00 am',
                'id' => 173,
                'start_date' => 'September 04, 2021 08:00 pm - 05:00 am',
            ),
            166 => 
            array (
                'batch_number' => 'ISA 173',
                'date_created' => '2021-07-29 17:30:24',
                'email_sent' => 'no',
                'end_date' => 'September 16, 2021 08:00 pm - 05:00 am',
                'id' => 174,
                'start_date' => 'September 10, 2021 08:00 pm - 05:00 am',
            ),
            167 => 
            array (
                'batch_number' => 'ISA 174',
                'date_created' => '2021-07-29 17:31:26',
                'email_sent' => 'no',
                'end_date' => 'September 18, 2021 08:00 pm - 05:00 am',
                'id' => 175,
                'start_date' => 'September 12, 2021 08:00 pm - 05:00 am',
            ),
            168 => 
            array (
                'batch_number' => 'ISA 175',
                'date_created' => '2021-07-29 17:32:11',
                'email_sent' => 'no',
                'end_date' => 'September 23, 2021 08:00 pm - 05:00 am',
                'id' => 176,
                'start_date' => 'September 17, 2021 08:00 pm - 05:00 am',
            ),
            169 => 
            array (
                'batch_number' => 'ISA 176',
                'date_created' => '2021-07-29 17:32:53',
                'email_sent' => 'no',
                'end_date' => 'September 25, 2021 08:00 pm - 05:00 am',
                'id' => 177,
                'start_date' => 'September 19, 2021 08:00 pm - 05:00 am',
            ),
            170 => 
            array (
                'batch_number' => 'ISA 177',
                'date_created' => '2021-07-29 17:33:34',
                'email_sent' => 'no',
                'end_date' => 'September 30, 2021 08:00 pm - 05:00 am',
                'id' => 178,
                'start_date' => 'September 24, 2021 08:00 pm - 05:00 am',
            ),
            171 => 
            array (
                'batch_number' => 'ISA 178',
                'date_created' => '2021-07-29 17:34:26',
                'email_sent' => 'no',
                'end_date' => 'October 02, 2021 08:00 pm - 05:00 am',
                'id' => 179,
                'start_date' => 'September 26, 2021 08:00 pm - 05:00 am',
            ),
        ));
        
        
    }
}