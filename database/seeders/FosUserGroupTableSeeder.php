<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FosUserGroupTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fos_user_group')->delete();
        
        \DB::table('fos_user_group')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Raiders',
                'roles' => '',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Seahawk',
                'roles' => '',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Sounders',
                'roles' => '',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Ravens',
                'roles' => '',
            ),
        ));
        
        
    }
}