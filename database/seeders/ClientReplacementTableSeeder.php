<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClientReplacementTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('client_replacement')->delete();
        
        \DB::table('client_replacement')->insert(array (
            0 => 
            array (
                'client_info_id' => 607,
                'date' => '2020-01-30 07:13:03',
                'id' => 1,
                'replaced_by' => 1603,
                'replacement_id' => 6,
                'replacement_reason' => 'wala lang',
            ),
            1 => 
            array (
                'client_info_id' => 584,
                'date' => '2020-01-30 07:44:27',
                'id' => 2,
                'replaced_by' => 10665,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            2 => 
            array (
                'client_info_id' => 601,
                'date' => '2020-02-06 07:34:42',
                'id' => 3,
                'replaced_by' => 13269,
                'replacement_id' => 0,
                'replacement_reason' => 'endo',
            ),
            3 => 
            array (
                'client_info_id' => 602,
                'date' => '2020-02-07 05:51:35',
                'id' => 4,
                'replaced_by' => 13321,
                'replacement_id' => 0,
                'replacement_reason' => 'Client Request',
            ),
            4 => 
            array (
                'client_info_id' => 607,
                'date' => '2020-02-08 12:39:21',
                'id' => 5,
                'replaced_by' => 13306,
                'replacement_id' => 1603,
                'replacement_reason' => 'test',
            ),
            5 => 
            array (
                'client_info_id' => 570,
                'date' => '2020-02-27 12:05:27',
                'id' => 6,
                'replaced_by' => 13668,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            6 => 
            array (
                'client_info_id' => 209,
                'date' => '2020-03-08 01:49:25',
                'id' => 7,
                'replaced_by' => 13728,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            7 => 
            array (
                'client_info_id' => 182,
                'date' => '2020-03-08 01:51:49',
                'id' => 8,
                'replaced_by' => 13729,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            8 => 
            array (
                'client_info_id' => 432,
                'date' => '2020-03-08 03:16:00',
                'id' => 9,
                'replaced_by' => 13733,
                'replacement_id' => 0,
                'replacement_reason' => 'Cancelled',
            ),
            9 => 
            array (
                'client_info_id' => 490,
                'date' => '2020-03-08 03:25:39',
                'id' => 10,
                'replaced_by' => 13740,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            10 => 
            array (
                'client_info_id' => 514,
                'date' => '2020-03-08 03:28:17',
                'id' => 11,
                'replaced_by' => 13700,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            11 => 
            array (
                'client_info_id' => 514,
                'date' => '2020-03-08 03:28:48',
                'id' => 12,
                'replaced_by' => 4686,
                'replacement_id' => 13700,
                'replacement_reason' => NULL,
            ),
            12 => 
            array (
                'client_info_id' => 614,
                'date' => '2020-03-08 03:57:42',
                'id' => 13,
                'replaced_by' => 13742,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            13 => 
            array (
                'client_info_id' => 577,
                'date' => '2020-03-08 04:08:37',
                'id' => 14,
                'replaced_by' => 13721,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            14 => 
            array (
                'client_info_id' => 575,
                'date' => '2020-03-08 04:11:26',
                'id' => 15,
                'replaced_by' => 13728,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            15 => 
            array (
                'client_info_id' => 571,
                'date' => '2020-03-08 04:15:35',
                'id' => 16,
                'replaced_by' => 13733,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            16 => 
            array (
                'client_info_id' => 564,
                'date' => '2020-03-08 04:19:17',
                'id' => 17,
                'replaced_by' => 13734,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            17 => 
            array (
                'client_info_id' => 587,
                'date' => '2020-03-08 04:41:16',
                'id' => 18,
                'replaced_by' => 4686,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            18 => 
            array (
                'client_info_id' => 504,
                'date' => '2020-03-08 04:45:47',
                'id' => 19,
                'replaced_by' => 13721,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            19 => 
            array (
                'client_info_id' => 508,
                'date' => '2020-03-08 05:25:44',
                'id' => 20,
                'replaced_by' => 13739,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            20 => 
            array (
                'client_info_id' => 462,
                'date' => '2020-03-08 07:10:03',
                'id' => 21,
                'replaced_by' => 13736,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            21 => 
            array (
                'client_info_id' => 449,
                'date' => '2020-03-08 07:13:14',
                'id' => 22,
                'replaced_by' => 13735,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            22 => 
            array (
                'client_info_id' => 425,
                'date' => '2020-03-08 07:17:22',
                'id' => 23,
                'replaced_by' => 13734,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            23 => 
            array (
                'client_info_id' => 333,
                'date' => '2020-03-08 07:20:47',
                'id' => 24,
                'replaced_by' => 13690,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            24 => 
            array (
                'client_info_id' => 612,
                'date' => '2020-03-08 10:47:53',
                'id' => 25,
                'replaced_by' => 13306,
                'replacement_id' => NULL,
                'replacement_reason' => 'test',
            ),
            25 => 
            array (
                'client_info_id' => 488,
                'date' => '2020-03-14 12:25:49',
                'id' => 26,
                'replaced_by' => 13692,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            26 => 
            array (
                'client_info_id' => 612,
                'date' => '2020-03-14 07:35:51',
                'id' => 27,
                'replaced_by' => 14229,
                'replacement_id' => 13306,
                'replacement_reason' => 'Testing',
            ),
            27 => 
            array (
                'client_info_id' => 567,
                'date' => '2020-03-17 06:21:11',
                'id' => 28,
                'replaced_by' => 13656,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            28 => 
            array (
                'client_info_id' => 624,
                'date' => '2020-03-19 02:49:36',
                'id' => 29,
                'replaced_by' => 15371,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            29 => 
            array (
                'client_info_id' => 615,
                'date' => '2020-03-20 05:14:53',
                'id' => 30,
                'replaced_by' => 15460,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            30 => 
            array (
                'client_info_id' => 638,
                'date' => '2020-03-20 06:54:12',
                'id' => 31,
                'replaced_by' => 633,
                'replacement_id' => 12768,
                'replacement_reason' => 'Attendance',
            ),
            31 => 
            array (
                'client_info_id' => 651,
                'date' => '2020-03-21 12:39:58',
                'id' => 32,
                'replaced_by' => 13312,
                'replacement_id' => 13706,
                'replacement_reason' => 'VA Rowena requested to be replaced',
            ),
            32 => 
            array (
                'client_info_id' => 423,
                'date' => '2020-03-22 04:49:12',
                'id' => 33,
                'replaced_by' => 13727,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            33 => 
            array (
                'client_info_id' => 423,
                'date' => '2020-03-22 04:50:59',
                'id' => 34,
                'replaced_by' => 13710,
                'replacement_id' => 13727,
                'replacement_reason' => NULL,
            ),
            34 => 
            array (
                'client_info_id' => 670,
                'date' => '2020-03-22 05:10:02',
                'id' => 35,
                'replaced_by' => 13692,
                'replacement_id' => 12499,
                'replacement_reason' => 'pregnancy non-disclosure',
            ),
            35 => 
            array (
                'client_info_id' => 672,
                'date' => '2020-03-22 05:55:06',
                'id' => 36,
                'replaced_by' => 13687,
                'replacement_id' => 10665,
                'replacement_reason' => NULL,
            ),
            36 => 
            array (
                'client_info_id' => 672,
                'date' => '2020-03-22 05:56:51',
                'id' => 37,
                'replaced_by' => 13670,
                'replacement_id' => 13687,
                'replacement_reason' => NULL,
            ),
            37 => 
            array (
                'client_info_id' => 603,
                'date' => '2020-03-22 06:11:02',
                'id' => 38,
                'replaced_by' => 2375,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            38 => 
            array (
                'client_info_id' => 601,
                'date' => '2020-03-22 06:24:59',
                'id' => 39,
                'replaced_by' => 5867,
                'replacement_id' => 13269,
                'replacement_reason' => NULL,
            ),
            39 => 
            array (
                'client_info_id' => 601,
                'date' => '2020-03-22 06:26:54',
                'id' => 40,
                'replaced_by' => 13433,
                'replacement_id' => 5867,
                'replacement_reason' => 'replace EVA with tenured ISA to set appointments',
            ),
            40 => 
            array (
                'client_info_id' => 593,
                'date' => '2020-03-24 05:15:18',
                'id' => 41,
                'replaced_by' => 13687,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            41 => 
            array (
                'client_info_id' => 598,
                'date' => '2020-03-24 06:58:39',
                'id' => 42,
                'replaced_by' => 13682,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            42 => 
            array (
                'client_info_id' => 598,
                'date' => '2020-03-24 06:59:30',
                'id' => 43,
                'replaced_by' => 13656,
                'replacement_id' => 13682,
                'replacement_reason' => 'Initial VA',
            ),
            43 => 
            array (
                'client_info_id' => 598,
                'date' => '2020-03-24 07:00:54',
                'id' => 44,
                'replaced_by' => 3695,
                'replacement_id' => 13656,
                'replacement_reason' => 'flyer project',
            ),
            44 => 
            array (
                'client_info_id' => 597,
                'date' => '2020-03-24 07:59:22',
                'id' => 45,
                'replaced_by' => 13677,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            45 => 
            array (
                'client_info_id' => 636,
                'date' => '2020-03-29 04:42:18',
                'id' => 46,
                'replaced_by' => 15677,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            46 => 
            array (
                'client_info_id' => 454,
                'date' => '2020-03-31 12:45:49',
                'id' => 47,
                'replaced_by' => 13323,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            47 => 
            array (
                'client_info_id' => 339,
                'date' => '2020-03-31 02:41:33',
                'id' => 48,
                'replaced_by' => 13711,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            48 => 
            array (
                'client_info_id' => 562,
                'date' => '2020-04-03 10:04:01',
                'id' => 49,
                'replaced_by' => 13742,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            49 => 
            array (
                'client_info_id' => 492,
                'date' => '2020-04-04 01:40:05',
                'id' => 50,
                'replaced_by' => 13650,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            50 => 
            array (
                'client_info_id' => 646,
                'date' => '2020-04-07 01:27:17',
                'id' => 51,
                'replaced_by' => 13642,
                'replacement_id' => 10969,
                'replacement_reason' => 'Initial VA Jester Muldong replaced by JC Reyes and not Juan Carlo Bartolome',
            ),
            51 => 
            array (
                'client_info_id' => 652,
                'date' => '2020-04-07 06:30:59',
                'id' => 52,
                'replaced_by' => 13711,
                'replacement_id' => 11377,
                'replacement_reason' => 'Feb 24, 2020 Hold request; to put up training and system in place

April 2, 2020 Resumption of Service and GVA Replacement details for Adilah Curry.

VA Name: Jesamari Brosas
Schedule: Mon-Fri 12NN-4PM CST
Start Date: April 2, 2020',
            ),
            52 => 
            array (
                'client_info_id' => 633,
                'date' => '2020-04-07 06:56:41',
                'id' => 53,
                'replaced_by' => 15826,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            53 => 
            array (
                'client_info_id' => 634,
                'date' => '2020-04-07 07:04:16',
                'id' => 54,
                'replaced_by' => 15630,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            54 => 
            array (
                'client_info_id' => 615,
                'date' => '2020-04-07 09:02:49',
                'id' => 55,
                'replaced_by' => 13433,
                'replacement_id' => 15460,
                'replacement_reason' => 'Perfromance / No appointments for 2 weeks',
            ),
            55 => 
            array (
                'client_info_id' => 494,
                'date' => '2020-04-08 11:50:24',
                'id' => 56,
                'replaced_by' => 3247,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            56 => 
            array (
                'client_info_id' => 467,
                'date' => '2020-04-10 12:49:21',
                'id' => 57,
                'replaced_by' => 13714,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            57 => 
            array (
                'client_info_id' => 529,
                'date' => '2020-04-10 04:02:44',
                'id' => 58,
                'replaced_by' => 5867,
                'replacement_id' => 0,
                'replacement_reason' => 'incorrect VA placed in VDPro',
            ),
            58 => 
            array (
                'client_info_id' => 536,
                'date' => '2020-04-10 04:47:10',
                'id' => 59,
                'replaced_by' => 13645,
                'replacement_id' => 0,
                'replacement_reason' => 'booked VA',
            ),
            59 => 
            array (
                'client_info_id' => 543,
                'date' => '2020-04-10 04:57:07',
                'id' => 60,
                'replaced_by' => 13722,
                'replacement_id' => 0,
                'replacement_reason' => 'booked VA - never replied',
            ),
            60 => 
            array (
                'client_info_id' => 494,
                'date' => '2020-04-15 04:23:54',
                'id' => 61,
                'replaced_by' => 13717,
                'replacement_id' => 13728,
                'replacement_reason' => 'New set up service',
            ),
            61 => 
            array (
                'client_info_id' => 495,
                'date' => '2020-04-24 08:04:10',
                'id' => 62,
                'replaced_by' => 13674,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            62 => 
            array (
                'client_info_id' => 195,
                'date' => '2020-04-24 11:43:07',
                'id' => 63,
                'replaced_by' => 12538,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            63 => 
            array (
                'client_info_id' => 679,
                'date' => '2020-04-25 03:36:41',
                'id' => 64,
                'replaced_by' => 13727,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            64 => 
            array (
                'client_info_id' => 674,
                'date' => '2020-04-30 08:13:24',
                'id' => 65,
                'replaced_by' => 15837,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            65 => 
            array (
                'client_info_id' => 684,
                'date' => '2020-05-02 03:11:26',
                'id' => 66,
                'replaced_by' => 13727,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            66 => 
            array (
                'client_info_id' => 691,
                'date' => '2020-05-06 12:13:45',
                'id' => 67,
                'replaced_by' => 13713,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            67 => 
            array (
                'client_info_id' => 695,
                'date' => '2020-05-09 01:32:44',
                'id' => 68,
                'replaced_by' => 13821,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            68 => 
            array (
                'client_info_id' => 688,
                'date' => '2020-05-09 01:42:23',
                'id' => 69,
                'replaced_by' => 13685,
                'replacement_id' => 0,
                'replacement_reason' => 'New Hire',
            ),
            69 => 
            array (
                'client_info_id' => 544,
                'date' => '2020-05-13 02:10:45',
                'id' => 70,
                'replaced_by' => 6929,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            70 => 
            array (
                'client_info_id' => 699,
                'date' => '2020-05-13 10:16:35',
                'id' => 71,
                'replaced_by' => 13733,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            71 => 
            array (
                'client_info_id' => 706,
                'date' => '2020-05-14 10:48:29',
                'id' => 72,
                'replaced_by' => 13685,
                'replacement_id' => 0,
                'replacement_reason' => 'Client: Gusty Gulas
VA: Kenah Lequigan
Shift sched:  anytime after 2:30 CST
M&G date: May 15, 10 AM CST
Start date: TBD
Coach: Cheryll Cos',
            ),
            72 => 
            array (
                'client_info_id' => 337,
                'date' => '2020-05-15 10:55:54',
                'id' => 73,
                'replaced_by' => 13732,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            73 => 
            array (
                'client_info_id' => 715,
                'date' => '2020-05-15 11:06:10',
                'id' => 74,
                'replaced_by' => 13699,
                'replacement_id' => 0,
                'replacement_reason' => 'Client: Kevin  Piechota
VA: Louis Magno
Shift sched: TBD
M&G date: May 16, 8 AM PST
Start date: May 18, Monday
Coach: Sharon Fabian',
            ),
            74 => 
            array (
                'client_info_id' => 622,
                'date' => '2020-05-15 11:32:53',
                'id' => 75,
                'replaced_by' => 13683,
                'replacement_id' => 4074,
                'replacement_reason' => NULL,
            ),
            75 => 
            array (
                'client_info_id' => 250,
                'date' => '2020-05-15 11:33:45',
                'id' => 76,
                'replaced_by' => 13673,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            76 => 
            array (
                'client_info_id' => 694,
                'date' => '2020-05-16 05:24:09',
                'id' => 77,
                'replaced_by' => 15369,
                'replacement_id' => NULL,
                'replacement_reason' => 'VA: Cielo Macalalad
Shift sched: 2-6 PM EST
M&G date: May 18, 10 AM EST
Start date: May 18, Monday
Coach: Cheryll Cos',
            ),
            77 => 
            array (
                'client_info_id' => 713,
                'date' => '2020-05-19 08:40:37',
                'id' => 78,
                'replaced_by' => 13931,
                'replacement_id' => 0,
                'replacement_reason' => 'Booked: May 14
VA: Jaime De Guzman
Shift sched: Tue-Fri 5 hrs/day
M&G date: May 15, 1 PM EST
Start date: May 19
Coach: Sharon',
            ),
            78 => 
            array (
                'client_info_id' => 707,
                'date' => '2020-05-21 04:17:00',
                'id' => 79,
                'replaced_by' => 13683,
                'replacement_id' => 0,
                'replacement_reason' => 'VA: Christian Angara
Shift sched: M-TH and Sat 9AM EST - 1PM EST, FRI SUN off
M&G date: May 21, 11 AM EST
Start date: May 26, Tues
Coach: Nica Castillo
Booked: May 20, 2020',
            ),
            79 => 
            array (
                'client_info_id' => 724,
                'date' => '2020-05-22 05:24:02',
                'id' => 80,
                'replaced_by' => 13838,
                'replacement_id' => 0,
                'replacement_reason' => 'VA: Charmaen Luisa Espina
Shift sched: 9AM -1PM EST
M&G date: May 19, 10 AM EST
Start date: May 21, Thursday
Booked: May 18
Coach: Aiko Saito',
            ),
            80 => 
            array (
                'client_info_id' => 492,
                'date' => '2020-06-02 02:20:27',
                'id' => 81,
                'replaced_by' => 11377,
                'replacement_id' => 13650,
                'replacement_reason' => 'Harvey transitioned to an EVA client',
            ),
            81 => 
            array (
                'client_info_id' => 726,
                'date' => '2020-06-02 08:58:26',
                'id' => 82,
                'replaced_by' => 10784,
                'replacement_id' => 0,
                'replacement_reason' => 'Booked
Preferred shift: 2-6 PM EST
Start date: June 1, Monday
Meet & Greet: May 29, 11 AM EST | May 29, 11 PM MNL
Coach: Aiko',
            ),
            82 => 
            array (
                'client_info_id' => 741,
                'date' => '2020-06-10 09:21:00',
                'id' => 83,
                'replaced_by' => 15584,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            83 => 
            array (
                'client_info_id' => 725,
                'date' => '2020-06-10 09:31:43',
                'id' => 84,
                'replaced_by' => 13713,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            84 => 
            array (
                'client_info_id' => 731,
                'date' => '2020-06-10 09:32:18',
                'id' => 85,
                'replaced_by' => 4200,
                'replacement_id' => 17837,
                'replacement_reason' => NULL,
            ),
            85 => 
            array (
                'client_info_id' => 703,
                'date' => '2020-06-10 09:33:22',
                'id' => 86,
                'replaced_by' => 13690,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            86 => 
            array (
                'client_info_id' => 728,
                'date' => '2020-06-10 09:35:49',
                'id' => 87,
                'replaced_by' => 14785,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            87 => 
            array (
                'client_info_id' => 752,
                'date' => '2020-06-11 12:49:00',
                'id' => 88,
                'replaced_by' => 16739,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            88 => 
            array (
                'client_info_id' => 760,
                'date' => '2020-06-12 01:24:11',
                'id' => 89,
                'replaced_by' => 16948,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            89 => 
            array (
                'client_info_id' => 706,
                'date' => '2020-06-16 11:07:53',
                'id' => 90,
                'replaced_by' => 16997,
                'replacement_id' => 13685,
                'replacement_reason' => 'VA needed to transfer homes where internet connection is not available. VA Resigned.',
            ),
            90 => 
            array (
                'client_info_id' => 763,
                'date' => '2020-06-17 12:50:00',
                'id' => 91,
                'replaced_by' => 13933,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            91 => 
            array (
                'client_info_id' => 764,
                'date' => '2020-06-20 07:19:18',
                'id' => 92,
                'replaced_by' => 13741,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            92 => 
            array (
                'client_info_id' => 759,
                'date' => '2020-06-20 07:24:09',
                'id' => 93,
                'replaced_by' => 13654,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            93 => 
            array (
                'client_info_id' => 765,
                'date' => '2020-06-20 07:26:58',
                'id' => 94,
                'replaced_by' => 13700,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            94 => 
            array (
                'client_info_id' => 777,
                'date' => '2020-06-24 11:46:15',
                'id' => 95,
                'replaced_by' => 16375,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            95 => 
            array (
                'client_info_id' => 776,
                'date' => '2020-06-24 11:48:56',
                'id' => 96,
                'replaced_by' => 16997,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            96 => 
            array (
                'client_info_id' => 566,
                'date' => '2020-06-25 12:07:41',
                'id' => 97,
                'replaced_by' => 16584,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            97 => 
            array (
                'client_info_id' => 568,
                'date' => '2020-06-25 12:09:17',
                'id' => 98,
                'replaced_by' => 8320,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            98 => 
            array (
                'client_info_id' => 578,
                'date' => '2020-06-25 12:11:00',
                'id' => 99,
                'replaced_by' => 13654,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            99 => 
            array (
                'client_info_id' => 780,
                'date' => '2020-06-27 05:42:01',
                'id' => 100,
                'replaced_by' => 16744,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            100 => 
            array (
                'client_info_id' => 762,
                'date' => '2020-07-07 02:20:08',
                'id' => 101,
                'replaced_by' => 17756,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            101 => 
            array (
                'client_info_id' => 775,
                'date' => '2020-07-07 02:23:39',
                'id' => 102,
                'replaced_by' => 2040,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            102 => 
            array (
                'client_info_id' => 782,
                'date' => '2020-07-10 03:14:19',
                'id' => 103,
                'replaced_by' => 13737,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            103 => 
            array (
                'client_info_id' => 767,
                'date' => '2020-07-13 09:04:25',
                'id' => 104,
                'replaced_by' => 16826,
                'replacement_id' => 9573,
            'replacement_reason' => 'VA on indefinite Leave (Medical Leave)',
            ),
            104 => 
            array (
                'client_info_id' => 590,
                'date' => '2020-07-17 05:22:54',
                'id' => 105,
                'replaced_by' => 13678,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            105 => 
            array (
                'client_info_id' => 492,
                'date' => '2020-07-17 08:12:50',
                'id' => 106,
                'replaced_by' => 13696,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            106 => 
            array (
                'client_info_id' => 818,
                'date' => '2020-07-29 08:27:55',
                'id' => 107,
                'replaced_by' => 18984,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            107 => 
            array (
                'client_info_id' => 853,
                'date' => '2020-08-19 11:26:05',
                'id' => 108,
                'replaced_by' => 19698,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            108 => 
            array (
                'client_info_id' => 850,
                'date' => '2020-08-21 09:42:27',
                'id' => 109,
                'replaced_by' => 4789,
                'replacement_id' => NULL,
                'replacement_reason' => 'Position: PT ISA Start Date: Aug 31 Schedule: 10am to 2pm EST, M-F Meet & Greet: 10am EST Aug 24 VA: Raron Mediana',
            ),
            109 => 
            array (
                'client_info_id' => 874,
                'date' => '2020-09-01 03:18:16',
                'id' => 110,
                'replaced_by' => 22700,
                'replacement_id' => 0,
                'replacement_reason' => 'Position: PT ISA
Start date: TBD
Shift: 8-12pm EST
Meet & Greet: Sept 1, 3:00 pm EST
VA: Yvette Marie Mediana
Coach: Greziel',
            ),
            110 => 
            array (
                'client_info_id' => 856,
                'date' => '2020-09-01 06:00:47',
                'id' => 111,
                'replaced_by' => 6733,
                'replacement_id' => 0,
                'replacement_reason' => 'VA Name: Nina Alexandra Palmares
Client Name: Sasha Smith
Schedule: Mon-Fri 8am-12pm PST
Start Date: September 1, 2020
Coach: Sharon',
            ),
            111 => 
            array (
                'client_info_id' => 867,
                'date' => '2020-09-01 06:03:03',
                'id' => 112,
                'replaced_by' => 17876,
                'replacement_id' => 0,
                'replacement_reason' => 'VA Name: Vaughn Vincent Torralba
Client Name: Jessica Hays
Schedule: Monday - Friday 10am-2pm EST
Coach: Greziel',
            ),
            112 => 
            array (
                'client_info_id' => 593,
                'date' => '2020-09-09 02:17:08',
                'id' => 113,
                'replaced_by' => 13644,
                'replacement_id' => 13687,
                'replacement_reason' => NULL,
            ),
            113 => 
            array (
                'client_info_id' => 677,
                'date' => '2020-09-09 02:18:05',
                'id' => 114,
                'replaced_by' => 13673,
                'replacement_id' => 13768,
                'replacement_reason' => NULL,
            ),
            114 => 
            array (
                'client_info_id' => 878,
                'date' => '2020-09-10 12:34:04',
                'id' => 115,
                'replaced_by' => 23469,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            115 => 
            array (
                'client_info_id' => 768,
                'date' => '2020-09-10 02:40:57',
                'id' => 116,
                'replaced_by' => 18664,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            116 => 
            array (
                'client_info_id' => 869,
                'date' => '2020-09-10 03:14:55',
                'id' => 117,
                'replaced_by' => 22668,
                'replacement_id' => 0,
                'replacement_reason' => 'Aug 28 | Abby
Position: FT EVA
Start: Sept 8
Schedule: 830am to 530pm EST, Mondays to Fridays
Meet & Greet: Sept 3, 12pm EST
VA: Conny Lamadora',
            ),
            117 => 
            array (
                'client_info_id' => 870,
                'date' => '2020-09-10 03:17:35',
                'id' => 118,
                'replaced_by' => 22700,
                'replacement_id' => 0,
                'replacement_reason' => 'Aug 26 | Lalla
Position: PT GVA-TC
Start date: Sept 9, Wednesday
Shift: 9am-1pm EST
Meet & Greet: Sept 8, 9:00 am EST
VA: Yvette Marie Mediana
Coach: Greziel',
            ),
            118 => 
            array (
                'client_info_id' => 866,
                'date' => '2020-09-11 11:59:59',
                'id' => 119,
                'replaced_by' => 21058,
                'replacement_id' => 0,
                'replacement_reason' => 'Position: FT GVA
Start date: Aug 31
Schedule: 10am to 7pm EST, Mondays to Fridays
Meet & Greet: Aug 27 9am EST',
            ),
            119 => 
            array (
                'client_info_id' => 871,
                'date' => '2020-09-12 12:01:39',
                'id' => 120,
                'replaced_by' => 16894,
                'replacement_id' => 0,
                'replacement_reason' => 'Position: PT EVA
Start date: Aug 27, Thursday
Shift: 9am-1pm EST
Meet & Greet: Aug 26, 10 am EST',
            ),
            120 => 
            array (
                'client_info_id' => 875,
                'date' => '2020-09-12 12:07:18',
                'id' => 121,
                'replaced_by' => 22967,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            121 => 
            array (
                'client_info_id' => 877,
                'date' => '2020-09-12 05:48:21',
                'id' => 122,
                'replaced_by' => 15826,
                'replacement_id' => 0,
                'replacement_reason' => 'Position: PT GVA and PT ISA/Marketing
Start date: TBD
Shift: 12-4 pm M-F
Meet & Greet: Sept 10, 9 am PST
VA: July Parrocha(GVA) and Louis Magno(ISA)
Coach: Alecs',
            ),
            122 => 
            array (
                'client_info_id' => 665,
                'date' => '2020-09-18 10:27:51',
                'id' => 123,
                'replaced_by' => 13663,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            123 => 
            array (
                'client_info_id' => 905,
                'date' => '2020-09-24 01:51:27',
                'id' => 124,
                'replaced_by' => 23105,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            124 => 
            array (
                'client_info_id' => 915,
                'date' => '2020-09-24 05:00:35',
                'id' => 125,
                'replaced_by' => 22941,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            125 => 
            array (
                'client_info_id' => 908,
                'date' => '2020-09-25 02:46:20',
                'id' => 126,
                'replaced_by' => 13740,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            126 => 
            array (
                'client_info_id' => 916,
                'date' => '2020-09-29 12:12:48',
                'id' => 127,
                'replaced_by' => 19012,
                'replacement_id' => 0,
                'replacement_reason' => 'Position: 2 PT ISA
Start date: Sept 29, Tuesday
Shift: TBD
Meet & Greet: Sept 25, 2:00 pm PST 
VA: Niño Ridhen Anies and Kavin Suan
Coach: Alecs',
            ),
            127 => 
            array (
                'client_info_id' => 918,
                'date' => '2020-09-29 12:17:14',
                'id' => 128,
                'replaced_by' => 23431,
                'replacement_id' => 0,
                'replacement_reason' => 'Position: FT ISA
Start date: Sept 28, Monday
Shift: M-F 9am-6pm PST
Meet & Greet: Sept 25, 10:00 am PST 
VA: Kathrine Cariasco
Coach: Cheryll Cos',
            ),
            128 => 
            array (
                'client_info_id' => 898,
                'date' => '2020-10-05 02:37:59',
                'id' => 129,
                'replaced_by' => 13727,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            129 => 
            array (
                'client_info_id' => 904,
                'date' => '2020-10-05 02:40:27',
                'id' => 130,
                'replaced_by' => 22921,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            130 => 
            array (
                'client_info_id' => 421,
                'date' => '2020-10-06 05:43:56',
                'id' => 131,
                'replaced_by' => 23743,
                'replacement_id' => 0,
                'replacement_reason' => 'n/a',
            ),
            131 => 
            array (
                'client_info_id' => 920,
                'date' => '2020-10-07 11:57:37',
                'id' => 132,
                'replaced_by' => 15466,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            132 => 
            array (
                'client_info_id' => 917,
                'date' => '2020-10-07 11:59:38',
                'id' => 133,
                'replaced_by' => 24243,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            133 => 
            array (
                'client_info_id' => 944,
                'date' => '2020-10-15 08:58:46',
                'id' => 134,
                'replaced_by' => 15626,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            134 => 
            array (
                'client_info_id' => 945,
                'date' => '2020-10-16 11:39:39',
                'id' => 135,
                'replaced_by' => 22700,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            135 => 
            array (
                'client_info_id' => 946,
                'date' => '2020-10-16 11:41:04',
                'id' => 136,
                'replaced_by' => 25740,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            136 => 
            array (
                'client_info_id' => 947,
                'date' => '2020-10-16 11:41:48',
                'id' => 137,
                'replaced_by' => 15210,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            137 => 
            array (
                'client_info_id' => 948,
                'date' => '2020-10-16 11:46:19',
                'id' => 138,
                'replaced_by' => 24021,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            138 => 
            array (
                'client_info_id' => 953,
                'date' => '2020-10-22 01:08:21',
                'id' => 139,
                'replaced_by' => 15541,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            139 => 
            array (
                'client_info_id' => 955,
                'date' => '2020-10-27 11:55:22',
                'id' => 140,
                'replaced_by' => 23469,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            140 => 
            array (
                'client_info_id' => 817,
                'date' => '2020-10-28 08:23:09',
                'id' => 141,
                'replaced_by' => 21515,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            141 => 
            array (
                'client_info_id' => 911,
                'date' => '2020-10-30 12:56:09',
                'id' => 142,
                'replaced_by' => 21736,
                'replacement_id' => NULL,
                'replacement_reason' => '10/28 Client decided to return and continue services',
            ),
            142 => 
            array (
                'client_info_id' => 962,
                'date' => '2020-10-30 12:57:47',
                'id' => 143,
                'replaced_by' => 24961,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            143 => 
            array (
                'client_info_id' => 960,
                'date' => '2020-11-06 04:06:27',
                'id' => 144,
                'replaced_by' => 23743,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            144 => 
            array (
                'client_info_id' => 963,
                'date' => '2020-11-06 06:23:52',
                'id' => 145,
                'replaced_by' => 22700,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            145 => 
            array (
                'client_info_id' => 961,
                'date' => '2020-11-06 06:25:28',
                'id' => 146,
                'replaced_by' => 16375,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            146 => 
            array (
                'client_info_id' => 965,
                'date' => '2020-11-06 06:43:32',
                'id' => 147,
                'replaced_by' => 21449,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            147 => 
            array (
                'client_info_id' => 966,
                'date' => '2020-11-06 07:03:21',
                'id' => 148,
                'replaced_by' => 13663,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            148 => 
            array (
                'client_info_id' => 964,
                'date' => '2020-11-07 06:53:35',
                'id' => 149,
                'replaced_by' => 15541,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            149 => 
            array (
                'client_info_id' => 940,
                'date' => '2020-11-10 10:38:05',
                'id' => 150,
                'replaced_by' => 17674,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            150 => 
            array (
                'client_info_id' => 978,
                'date' => '2020-11-19 04:30:11',
                'id' => 151,
                'replaced_by' => 28837,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            151 => 
            array (
                'client_info_id' => 973,
                'date' => '2020-11-20 02:43:44',
                'id' => 152,
                'replaced_by' => 13711,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            152 => 
            array (
                'client_info_id' => 996,
                'date' => '2020-12-29 02:09:32',
                'id' => 153,
                'replaced_by' => 18135,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            153 => 
            array (
                'client_info_id' => 475,
                'date' => '2021-05-25 05:13:22',
                'id' => 154,
                'replaced_by' => 13739,
                'replacement_id' => 0,
                'replacement_reason' => NULL,
            ),
            154 => 
            array (
                'client_info_id' => 1034,
                'date' => '2021-05-25 06:25:00',
                'id' => 155,
                'replaced_by' => 42087,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            155 => 
            array (
                'client_info_id' => 1046,
                'date' => '2021-05-26 11:29:04',
                'id' => 156,
                'replaced_by' => 42886,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
            156 => 
            array (
                'client_info_id' => 1310,
                'date' => '2021-06-30 02:49:44',
                'id' => 157,
                'replaced_by' => 42605,
                'replacement_id' => NULL,
                'replacement_reason' => NULL,
            ),
        ));
        
        
    }
}