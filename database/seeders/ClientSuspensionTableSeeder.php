<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClientSuspensionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('client_suspension')->delete();
        
        \DB::table('client_suspension')->insert(array (
            0 => 
            array (
                'client_info_id' => 27,
                'form_sent' => 'no',
                'id' => 1,
                'suspension_date' => '2019-12-20 04:00:00',
                'suspension_reason' => 'testtt',
            ),
            1 => 
            array (
                'client_info_id' => 22,
                'form_sent' => 'no',
                'id' => 2,
                'suspension_date' => '2019-12-31 04:00:00',
                'suspension_reason' => 'End of year',
            ),
            2 => 
            array (
                'client_info_id' => 41,
                'form_sent' => 'no',
                'id' => 3,
                'suspension_date' => '2019-12-23 04:00:00',
                'suspension_reason' => 'ok',
            ),
            3 => 
            array (
                'client_info_id' => 40,
                'form_sent' => 'no',
                'id' => 4,
                'suspension_date' => '2019-12-27 04:00:00',
                'suspension_reason' => 'sfssasdafsdf',
            ),
            4 => 
            array (
                'client_info_id' => 48,
                'form_sent' => 'no',
                'id' => 5,
                'suspension_date' => '2019-12-31 04:00:00',
                'suspension_reason' => 'sdsdsds',
            ),
            5 => 
            array (
                'client_info_id' => 628,
                'form_sent' => 'no',
                'id' => 6,
                'suspension_date' => NULL,
                'suspension_reason' => 'Temporary - due to COVID-19',
            ),
            6 => 
            array (
                'client_info_id' => 593,
                'form_sent' => 'no',
                'id' => 7,
                'suspension_date' => NULL,
                'suspension_reason' => 'Temporary - due to COVID-19',
            ),
            7 => 
            array (
                'client_info_id' => 617,
                'form_sent' => 'no',
                'id' => 8,
                'suspension_date' => NULL,
                'suspension_reason' => 'Temporary - due to COVID-19',
            ),
            8 => 
            array (
                'client_info_id' => 622,
                'form_sent' => 'no',
                'id' => 9,
                'suspension_date' => NULL,
                'suspension_reason' => 'Temporary - due to COVID-19',
            ),
            9 => 
            array (
                'client_info_id' => 250,
                'form_sent' => 'no',
                'id' => 10,
                'suspension_date' => NULL,
                'suspension_reason' => 'Temporary - due to COVID19',
            ),
            10 => 
            array (
                'client_info_id' => 698,
                'form_sent' => 'no',
                'id' => 11,
                'suspension_date' => NULL,
                'suspension_reason' => NULL,
            ),
        ));
        
        
    }
}