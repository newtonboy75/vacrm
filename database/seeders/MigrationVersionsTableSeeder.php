<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MigrationVersionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('migration_versions')->delete();
        
        \DB::table('migration_versions')->insert(array (
            0 => 
            array (
                'executed_at' => '2019-08-17 20:11:39',
                'version' => '20190514125118',
            ),
            1 => 
            array (
                'executed_at' => '2019-08-17 20:11:39',
                'version' => '20190514151707',
            ),
            2 => 
            array (
                'executed_at' => '2019-08-17 20:11:39',
                'version' => '20190517150036',
            ),
            3 => 
            array (
                'executed_at' => '2019-08-17 20:11:39',
                'version' => '20190521130613',
            ),
            4 => 
            array (
                'executed_at' => '2019-08-17 20:11:39',
                'version' => '20190527160057',
            ),
            5 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190528115736',
            ),
            6 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190529094946',
            ),
            7 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190529153510',
            ),
            8 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190530130157',
            ),
            9 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190530141728',
            ),
            10 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190530143017',
            ),
            11 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190603134654',
            ),
            12 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190603135500',
            ),
            13 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190605134513',
            ),
            14 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190606094450',
            ),
            15 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190606143155',
            ),
            16 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190607121046',
            ),
            17 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190609154001',
            ),
            18 => 
            array (
                'executed_at' => '2019-08-17 20:11:40',
                'version' => '20190612083016',
            ),
            19 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190613122055',
            ),
            20 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190614115711',
            ),
            21 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190614142023',
            ),
            22 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190618153617',
            ),
            23 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190618161409',
            ),
            24 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190619075135',
            ),
            25 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190619104651',
            ),
            26 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190619141317',
            ),
            27 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190708151800',
            ),
            28 => 
            array (
                'executed_at' => '2019-08-17 20:11:41',
                'version' => '20190710124007',
            ),
        ));
        
        
    }
}