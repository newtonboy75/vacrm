<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HomeEvalNoteTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('home_eval_note')->delete();
        
        \DB::table('home_eval_note')->insert(array (
            0 => 
            array (
                'date_created' => '2020-03-26 00:20:22',
                'home_eval_id' => 34,
                'id' => 19,
                'note' => 'test',
            ),
            1 => 
            array (
                'date_created' => '2020-03-26 00:20:22',
                'home_eval_id' => 34,
                'id' => 20,
                'note' => 'another test',
            ),
            2 => 
            array (
                'date_created' => '2020-03-26 00:20:22',
                'home_eval_id' => 34,
                'id' => 21,
                'note' => 'test',
            ),
            3 => 
            array (
                'date_created' => '2020-03-30 14:47:59',
                'home_eval_id' => 124,
                'id' => 22,
                'note' => 'Re-do HEF. Backup system is Windows 7',
            ),
            4 => 
            array (
                'date_created' => '2020-03-30 14:50:21',
                'home_eval_id' => 122,
                'id' => 23,
                'note' => '03/25/2020 

Re-do HEF. No backup power source',
            ),
            5 => 
            array (
                'date_created' => '2020-03-30 14:51:24',
                'home_eval_id' => 123,
                'id' => 24,
                'note' => 'Re-do. backup system is Mac Catalina',
            ),
            6 => 
            array (
                'date_created' => '2020-03-30 14:52:36',
                'home_eval_id' => 125,
                'id' => 25,
                'note' => 'Re-do HEF. Backup ISP photo is the same as Main ISP. Main & backup system specs are blurry.',
            ),
            7 => 
            array (
                'date_created' => '2020-03-30 15:13:30',
                'home_eval_id' => 131,
                'id' => 29,
                'note' => 'all declared backup systems are on her cousin\'s house and she can\'t go there due to the lockdown.',
            ),
            8 => 
            array (
                'date_created' => '2020-04-03 14:34:03',
                'home_eval_id' => 134,
                'id' => 35,
                'note' => 'no backup power source',
            ),
            9 => 
            array (
                'date_created' => '2020-04-03 14:36:56',
                'home_eval_id' => 129,
                'id' => 36,
                'note' => 'specifications screenshot of his main and backup are the same',
            ),
            10 => 
            array (
                'date_created' => '2020-04-03 14:36:56',
                'home_eval_id' => 129,
                'id' => 37,
                'note' => 'Sent updated HEF. backup is windows 8',
            ),
            11 => 
            array (
                'date_created' => '2020-04-03 15:55:15',
                'home_eval_id' => 128,
                'id' => 39,
                'note' => 'backup computer is windows 8',
            ),
            12 => 
            array (
                'date_created' => '2020-04-03 15:56:34',
                'home_eval_id' => 33,
                'id' => 40,
                'note' => 'Forfeited. Did not submit updated HEF. 
Transition date: 02-27-2020',
            ),
            13 => 
            array (
                'date_created' => '2020-04-10 17:13:02',
                'home_eval_id' => 130,
                'id' => 41,
                'note' => 'backup power source is an iphone and backup system is windows 8',
            ),
            14 => 
            array (
                'date_created' => '2020-04-13 17:53:58',
                'home_eval_id' => 143,
                'id' => 43,
                'note' => 'Headset declared is not a USB headset.',
            ),
            15 => 
            array (
                'date_created' => '2020-04-13 17:57:09',
                'home_eval_id' => 72,
                'id' => 44,
                'note' => 'On hold since March 12. Did not submit updated HEF. Forfeited.',
            ),
            16 => 
            array (
                'date_created' => '2020-04-16 13:09:01',
                'home_eval_id' => 140,
                'id' => 45,
                'note' => 'Backup system specs does not include operating system',
            ),
            17 => 
            array (
                'date_created' => '2020-05-12 15:34:22',
                'home_eval_id' => 151,
                'id' => 47,
                'note' => 'Headset declared is "borrowed". Need to clarify this with the applicant.',
            ),
            18 => 
            array (
                'date_created' => '2020-05-12 15:34:22',
                'home_eval_id' => 151,
                'id' => 48,
                'note' => 'replied and mentioned that this will not be a problem. he can use the headset during work hours at night and is planning to purchase one as soon as he receives his first pay check.',
            ),
            19 => 
            array (
                'date_created' => '2020-05-18 17:42:37',
                'home_eval_id' => 154,
                'id' => 50,
                'note' => 'backup system specs is the same as the main system',
            ),
            20 => 
            array (
                'date_created' => '2020-05-20 18:43:38',
                'home_eval_id' => 157,
                'id' => 54,
                'note' => 'backup computer is running on Windows 8.',
            ),
            21 => 
            array (
                'date_created' => '2020-05-22 23:42:47',
                'home_eval_id' => 160,
                'id' => 57,
                'note' => 'backup system runs on a Windows 7 OS.',
            ),
            22 => 
            array (
                'date_created' => '2020-05-29 12:16:21',
                'home_eval_id' => 178,
                'id' => 59,
                'note' => 'Main system is the same as backup system',
            ),
            23 => 
            array (
                'date_created' => '2020-05-29 12:18:57',
                'home_eval_id' => 184,
                'id' => 60,
                'note' => 'Backup Internet is below speed requirements',
            ),
            24 => 
            array (
                'date_created' => '2020-05-29 12:27:37',
                'home_eval_id' => 174,
                'id' => 62,
                'note' => 'Backup system is Catalina',
            ),
            25 => 
            array (
                'date_created' => '2020-06-05 18:46:33',
                'home_eval_id' => 197,
                'id' => 67,
                'note' => 'Headset is not USB type',
            ),
            26 => 
            array (
                'date_created' => '2020-06-05 19:03:58',
                'home_eval_id' => 207,
                'id' => 69,
                'note' => 'Backup is Mojave',
            ),
            27 => 
            array (
                'date_created' => '2020-06-09 17:31:38',
                'home_eval_id' => 218,
                'id' => 80,
                'note' => '- backup system is not windows 10',
            ),
            28 => 
            array (
                'date_created' => '2020-06-09 17:31:38',
                'home_eval_id' => 218,
                'id' => 81,
                'note' => 'passed home evaluation',
            ),
            29 => 
            array (
                'date_created' => '2020-06-09 17:34:10',
                'home_eval_id' => 206,
                'id' => 82,
                'note' => 'backup & main system specs are the same',
            ),
            30 => 
            array (
                'date_created' => '2020-06-09 17:34:10',
                'home_eval_id' => 206,
                'id' => 83,
                'note' => 'passed home evaluation',
            ),
            31 => 
            array (
                'date_created' => '2020-06-09 17:35:49',
                'home_eval_id' => 211,
                'id' => 84,
                'note' => 'no backup computer specs screenshot',
            ),
            32 => 
            array (
                'date_created' => '2020-06-09 17:35:49',
                'home_eval_id' => 211,
                'id' => 85,
                'note' => 'passed home evaluation',
            ),
            33 => 
            array (
                'date_created' => '2020-06-09 17:37:03',
                'home_eval_id' => 204,
                'id' => 86,
                'note' => 'Backup system is Windows 7',
            ),
            34 => 
            array (
                'date_created' => '2020-06-09 17:37:03',
                'home_eval_id' => 204,
                'id' => 87,
                'note' => 'passed home evaluation',
            ),
            35 => 
            array (
                'date_created' => '2020-06-09 18:44:54',
                'home_eval_id' => 209,
                'id' => 88,
                'note' => 'Same ISP for main & backup.',
            ),
            36 => 
            array (
                'date_created' => '2020-06-09 18:44:54',
                'home_eval_id' => 209,
                'id' => 89,
                'note' => 'Passed home evaluation. sent a separate email with additional backup internet.',
            ),
            37 => 
            array (
                'date_created' => '2020-06-09 18:58:35',
                'home_eval_id' => 219,
                'id' => 90,
                'note' => 'backup power source',
            ),
            38 => 
            array (
                'date_created' => '2020-06-09 18:58:35',
                'home_eval_id' => 219,
                'id' => 91,
                'note' => 'passed home evaluation. sent a separate email with additional contingency plans for backup power source.',
            ),
            39 => 
            array (
                'date_created' => '2020-06-10 10:33:02',
                'home_eval_id' => 216,
                'id' => 92,
                'note' => '- backup Internet & backup power source photo is the same as the main internet photo
- owner of backup ISP and power source is a tenant living with them.',
            ),
            40 => 
            array (
                'date_created' => '2020-06-10 10:33:02',
                'home_eval_id' => 216,
                'id' => 93,
                'note' => '- sent an email for additional backup power source.',
            ),
            41 => 
            array (
                'date_created' => '2020-06-10 10:34:02',
                'home_eval_id' => 222,
                'id' => 94,
                'note' => '- backup power source is a power bank',
            ),
            42 => 
            array (
                'date_created' => '2020-06-10 10:34:02',
                'home_eval_id' => 222,
                'id' => 95,
                'note' => '- sent an email for additional backup power source.',
            ),
        ));
        
        
    }
}