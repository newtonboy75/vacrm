<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class VatsKeywordsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('vats_keywords')->delete();
        
        \DB::table('vats_keywords')->insert(array (
            0 => 
            array (
                'created_at' => '2021-08-16 17:51:45',
                'deleted_at' => NULL,
                'id' => 1,
                'keywords' => 'real estate, bpo, 6 months, data entry, admin task, email management, social media management, blog writing, creation of marketing collaterals, database management, data management, virtual assistant, e-commerce, lead generation, vlogging, email management, email support, social media, marketing',
                'position' => 'gva',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'created_at' => '2021-08-16 17:51:45',
                'deleted_at' => NULL,
                'id' => 2,
                'keywords' => 'real estate, bpo, 6 months, manager, managerial, outbound calling, cold calling, outbound, outbound cold calling, appointment setting, admin task, email management, social media management, social media, blog, blog writing, marketing, collateral, va, creation of marketing collaterals, database management, virtual assistant, e-commerce, lead generation, email support, chat support, vlogging, email management, csr, customer service representative, tsr, technical service representative, customer service, technical service',
                'position' => 'eva',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'created_at' => '2021-08-16 17:51:45',
                'deleted_at' => NULL,
                'id' => 3,
                'keywords' => 'real estate, bpo, 6 months, outbound cold calling, outbound, outbound calling, cold calling, appointment setting, lead nurturing, lead generation, lead, crm, lead generation and nurturing, crm management, concierge, concierge services, database management, virtual assistant, csr, customer service representative, tsr, technical service representative, chat support, customer support, technical support',
                'position' => 'isa',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}