<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PasswordResetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('password_resets')->delete();
        
        \DB::table('password_resets')->insert(array (
            0 => 
            array (
                'created_at' => '2021-04-09 12:03:09',
                'email' => 'hr@myvirtudesk.com',
                'token' => '$2y$10$73QZeoVnCqadZElAcennx.nePGp27XQS6r5mnefxFAjsE3oRbO0sS',
            ),
            1 => 
            array (
                'created_at' => '2021-07-03 18:46:39',
                'email' => 'newtonboy@gmail.com',
                'token' => '$2y$10$h6M38t4Mpo.4KnCXtmlU1.Qz/395TMDRY.OzFVwnNAvO9DUISvjWa',
            ),
            2 => 
            array (
                'created_at' => '2021-07-31 02:46:46',
                'email' => 'alecs@myvirtudesk.com',
                'token' => '$2y$10$xuSOfb0.o7wFJ1b4WoDAn.ysCNnx6CghdMOqWZxND7GL8x2SUMeNS',
            ),
        ));
        
        
    }
}