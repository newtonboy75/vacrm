<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClientNotesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('client_notes')->delete();
        
        \DB::table('client_notes')->insert(array (
            0 => 
            array (
                'client_id' => 579,
                'created_at' => '2020-03-05 23:04:03',
                'deleted_at' => NULL,
                'id' => 2,
                'interaction_type' => 'email',
                'note' => '<p>testing</p>

<p><img alt="" src="/temp_uploads/Natl-ID.jpg" style="height:67px; width:100px" /></p>

<p>This is a test&nbsp;</p>',
                'note_type' => 'consultation',
                'noted_by' => 3,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            1 => 
            array (
                'client_id' => 579,
                'created_at' => '2020-03-06 15:55:52',
                'deleted_at' => NULL,
                'id' => 4,
                'interaction_type' => 'others',
                'note' => '<p>Preference call:</p>

<p>Client:<br />
Company:<br />
Timezone:<br />
Contact Number:<br />
email:<br />
Service:<br />
Contract signed:<br />
Contract notes:<br />
Marketing Specialist:<br />
Marketing notes:<br />
Others:</p>',
                'note_type' => 'others',
                'noted_by' => 3,
                'others_interaction' => 'Test',
                'others_status' => 'Please specify status type',
                'others_type' => 'Test',
                'status' => 'open',
            ),
            2 => 
            array (
                'client_id' => 612,
                'created_at' => '2020-03-13 17:46:41',
                'deleted_at' => NULL,
                'id' => 5,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>CI Schedule: 14 March, 3 AM MNL Company: Equity PA Real Estate Timezone: PA - EST</p>

<p>PT ISA</p>

<p>Preferences:</p>

<p>- Looking for someone with solid cold calling background</p>

<p>- Flexible with a weekend schedule</p>

<p>- Neighborhood cold calling and some old database leads</p>

<p>CRM and Tools: Mojo, Ring Central</p>

<p>Shift: 4PM-8PM EDT(with a flexible weekend schedule)</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            3 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-03-16 19:46:44',
                'deleted_at' => NULL,
                'id' => 6,
                'interaction_type' => 'email',
                'note' => '<p><strong>Topic:&nbsp;</strong>Services temporary on hold</p>

<p><strong>Timeline</strong>: Until end of the month</p>

<p><strong>Resumption of services:&nbsp;</strong>1st week of April</p>

<p><strong>Client Name:&nbsp;</strong>Maria Castro</p>

<p>&nbsp;</p>

<p>Today, Maria officially requested to put the services on hold starting tomorrow, March 17, 2020. Services will resume 1st week of April.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold request',
                'status' => 'acknowleged',
            ),
            4 => 
            array (
                'client_id' => 435,
                'created_at' => '2020-03-16 22:27:07',
                'deleted_at' => NULL,
                'id' => 7,
                'interaction_type' => 'call',
                'note' => '<p>Date: March 16, 2020</p>

<p>Issue: Been receiving complaints about quality of calls from VA Charmain. Will give her a chance to continue once all is good with her connection.</p>

<p>Possible Solution: Will reach out for possible replacement. Will send multiple profiles.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            5 => 
            array (
                'client_id' => 539,
                'created_at' => '2020-03-16 22:32:23',
                'deleted_at' => NULL,
                'id' => 8,
                'interaction_type' => 'others',
                'note' => '<p>Date: March 16, 2020</p>

<p>Concern: Already converted and signed 30-day contract.&nbsp;</p>

<p>Next Step: Will send VA profiles for CI</p>',
                'note_type' => 'others',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => 'Transition from TB to PT',
                'status' => 'ongoing',
            ),
            6 => 
            array (
                'client_id' => 614,
                'created_at' => '2020-03-16 23:33:33',
                'deleted_at' => NULL,
                'id' => 9,
                'interaction_type' => 'text',
            'note' => '<p>Requested to put the services on hold starting today, March 16, 2020 until end of the month (2weeks)</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            7 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-03-16 23:40:54',
                'deleted_at' => NULL,
                'id' => 10,
                'interaction_type' => 'call',
                'note' => '<p>Temporary Reduce Hours for:</p>

<p>Helen Grace Galay &amp; Leslie Canda-Arita</p>

<p>New schedule updated.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            8 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-03-17 13:50:11',
                'deleted_at' => NULL,
                'id' => 11,
                'interaction_type' => 'email',
                'note' => '<p>Requested to put the services on hold for 1 week.&nbsp;</p>

<p>Kavin will be back to work on Monday, March 23, 2020. He also wanted to give him a $100 compensation for this week since he will not be reporting for work - to be credited on the nearest pay out.&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            9 => 
            array (
                'client_id' => 572,
                'created_at' => '2020-03-17 21:17:35',
                'deleted_at' => NULL,
                'id' => 12,
                'interaction_type' => 'call',
                'note' => '<p>Requested to put the services/contract on hold for 30days due to COVID19 effect. They will be having a complete shutdown.&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            10 => 
            array (
                'client_id' => 616,
                'created_at' => '2020-03-19 14:32:30',
                'deleted_at' => NULL,
                'id' => 13,
                'interaction_type' => 'call',
                'note' => '<p>He requested to put the services/contract on hold until end of the month because business is slowing down due to COVID19.</p>

<p>Requested for touchbase on the 1st week of April.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Request',
                'status' => 'acknowleged',
            ),
            11 => 
            array (
                'client_id' => 567,
                'created_at' => '2020-03-19 20:44:43',
                'deleted_at' => NULL,
                'id' => 14,
                'interaction_type' => 'others',
                'note' => '<p>-Valerie is doing great in setting up appointments for him</p>

<p>-Moving forward, all appointments she will be setting will be assigned to him instead of Ben.</p>

<p>-He will start to interview GVA candidates on the 1st week of April when everything and everyone has calmed down because of COVID19.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Hangouts',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            12 => 
            array (
                'client_id' => 548,
                'created_at' => '2020-03-19 20:54:28',
                'deleted_at' => NULL,
                'id' => 15,
                'interaction_type' => 'others',
                'note' => '<p>Quick touchbase how he is doing in this COVID19 outbreak</p>

<p>-He said that they were advised by the city government to stay at home as much as possible but they are not locked down.</p>

<p>-Business is functioning as usual there in their area, only restaurants and bars are closed.</p>

<p>-Sheryl is doing a great job. She&#39;s not just setting up appointments for them, she&#39;s also doing some admin tasks as well.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            13 => 
            array (
                'client_id' => 641,
                'created_at' => '2020-03-19 21:35:18',
                'deleted_at' => NULL,
                'id' => 16,
                'interaction_type' => 'call',
                'note' => '<p>Quick Touchbase:</p>

<p>-Talked about COVID19 in their area. He said they are not on complete lockdown yet. Restaurants are still open but they can still buy food to go.</p>

<p>-He approved Chiello&#39;s rate increase of $0.50/hr for her 2nd anniversary with them</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            14 => 
            array (
                'client_id' => 224,
                'created_at' => '2020-03-20 14:02:35',
                'deleted_at' => NULL,
                'id' => 17,
                'interaction_type' => 'call',
                'note' => '<p>-He said that their Governer just implementred the lockdown of the whole state of PA last night. It&#39;s a but crazy there right now.</p>

<p>-He will not have anything for Mitchu to do as of the moment. He requested to put the services on hold until Thursday next week, March 26, 2020</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Request',
                'status' => 'acknowleged',
            ),
            15 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-03-20 20:59:00',
                'deleted_at' => NULL,
                'id' => 18,
                'interaction_type' => 'email',
                'note' => '<p>-Sent an inquiry via email if Virtudesk has a program for RE agents situation at this time with the Corona Virus since their businesses are starting to dry up. She said even inspection companies doesn&#39;t want to do inspections any more.&nbsp;</p>

<p>-Also she said that they have 2 listings that were pulled out from the Market because of the virus.</p>

<p>-Asked her if I can call her and discuss everything over the phone. She requested to have a call on Monday at 11AM CST because she&#39;s not available today.</p>',
                'note_type' => 'consultation',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            16 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-03-20 21:39:56',
                'deleted_at' => NULL,
                'id' => 19,
                'interaction_type' => 'text',
                'note' => '<p>-Followed up on Gracee and Jonie&#39;s training for Follow Up Boss today. Also informed him that they will also have training for Ylopo.</p>

<p>-He asked if both of the VAs are proficient in using Ring Central account.</p>

<p>-He also said that he&#39;s going to invite them to a few Facebook groups that they will have a role</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            17 => 
            array (
                'client_id' => 274,
                'created_at' => '2020-03-21 22:59:38',
                'deleted_at' => NULL,
                'id' => 20,
                'interaction_type' => 'email',
                'note' => '<p>VA Cancellation l Aldrin Maghirang l Debra Burke l Ravens</p>

<p><strong>Cherryll Lamata</strong></p>

<p>Mar 21, 2020, 1:33 AM</p>

<p>Hi Team,</p>

<p>Please see cancellation details below:<br />
<strong>GVA Name:</strong>&nbsp;Aldrin Maghirang<br />
<strong>Reason for Cancellation:&nbsp;</strong>&nbsp;Already assigned tasks to VA Wena and the newly hired onshore assistant<br />
<strong>Effectivity date:</strong>&nbsp;&nbsp;March 20, 2020<br />
<br />
Please note that his last log-in was March 13 but client requested for his last week to be paid (March 16-20).</p>',
                'note_type' => 'notification',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            18 => 
            array (
                'client_id' => 565,
                'created_at' => '2020-03-23 21:18:49',
                'deleted_at' => NULL,
                'id' => 21,
                'interaction_type' => 'call',
                'note' => '<p>&gt;Communucated last Saturday that he wanted to cancel the services due to business taking hits because of COVID19.</p>

<p>&gt;Client changed his mind in cancelling and just changed Lyka&#39;s schedule because they won&#39;t be needing her during Sundays anymore as they won&#39;t be having open houses for the mean time.</p>

<p>&gt;Lyka will be working moving forward from Monday to Friday starting at 9:30AM - 1:30PM PST.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
            'others_type' => 'Cancellation request (Saved)',
                'status' => 'done',
            ),
            19 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-03-23 21:24:58',
                'deleted_at' => NULL,
                'id' => 22,
                'interaction_type' => 'email',
                'note' => '<p>&gt;Harrison sent another email informing us that he would extend on having the services/contract on hold for another week.</p>

<p>&gt;He said he would have Kavin resume by next week.</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            20 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-03-23 21:28:18',
                'deleted_at' => NULL,
                'id' => 23,
                'interaction_type' => 'call',
                'note' => '<p>&gt;Gave intention to put the services/contract on hold because of COVID19.</p>

<p>&gt;Offered 30%-50% discount</p>

<p>&gt;Client accepted the 50% discount until March 31, 2020 only.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
            'others_type' => 'On-Hold Request (Saved)',
                'status' => 'done',
            ),
            21 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-03-23 21:34:50',
                'deleted_at' => NULL,
                'id' => 24,
                'interaction_type' => 'call',
            'note' => '<p>&gt;Spoke to one of Vito&#39;s associate (Edward Young).</p>

<p>&gt;Discussed KPIs for AJ moving forward.</p>

<p>&gt;A new close.io account will be set up for the VA so we can better monitor her phone number and dial outs.</p>

<p>&gt;Instructed VA to create a separate sheet to track her for nurture leads</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            22 => 
            array (
                'client_id' => 46,
                'created_at' => '2020-03-24 02:46:03',
                'deleted_at' => NULL,
                'id' => 25,
                'interaction_type' => 'call',
                'note' => '<hr />
<p>Requested holding services for a month because of Covid effective today</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            23 => 
            array (
                'client_id' => 504,
                'created_at' => '2020-03-24 02:49:47',
                'deleted_at' => NULL,
                'id' => 26,
                'interaction_type' => 'call',
                'note' => '<p>Client Cancellation</p>

<p>Om advised notification from Kevin, Om tried save up offer, Kevin agreed 50% discounted rate. VA Chie declined reduced hourly rate. Om acknowledged</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            24 => 
            array (
                'client_id' => 603,
                'created_at' => '2020-03-24 02:52:33',
                'deleted_at' => NULL,
                'id' => 27,
                'interaction_type' => 'text',
                'note' => '<p>VA feedback&nbsp;</p>

<p>&quot;I like Aldrin, he is great!&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            25 => 
            array (
                'client_id' => 339,
                'created_at' => '2020-03-24 02:53:40',
                'deleted_at' => NULL,
                'id' => 28,
                'interaction_type' => 'text',
                'note' => '<p>Followed up Skype subscription renewal, awaiting response</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            26 => 
            array (
                'client_id' => 594,
                'created_at' => '2020-03-24 02:58:16',
                'deleted_at' => NULL,
                'id' => 29,
                'interaction_type' => 'call',
                'note' => '<p>Client cancellation notification</p>

<p>Called Kim and offered 50% discount, Kim agreed. .&nbsp;</p>

<p>Kimberly Falker - &nbsp;from 9.60 down to $4.80 / VA Cielo will get $2 as per Om</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            27 => 
            array (
                'client_id' => 504,
                'created_at' => '2020-03-24 17:52:45',
                'deleted_at' => NULL,
                'id' => 30,
                'interaction_type' => 'others',
                'note' => '<p>Kevin will just pay the regular rate as per Om and keep VA Chie. Advised Chie , she will also keep the same GVA rate , $3/hr</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Update',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            28 => 
            array (
                'client_id' => 536,
                'created_at' => '2020-03-24 18:33:34',
                'deleted_at' => NULL,
                'id' => 31,
                'interaction_type' => 'call',
                'note' => '<p>Date: March 24, 2020</p>

<p>Notification on cancellation because of COVID 19. Recommended to utilize services but mentioned that she already has an admin. Agreed on the offered 50% discount on rate. -OM approved-</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            29 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-03-25 16:19:04',
                'deleted_at' => NULL,
                'id' => 32,
                'interaction_type' => 'email',
                'note' => '<p>Disputed his invoice. He signed up for an FT ISA contract but he was charged $10.75/hr instead of $9.60/hr.</p>

<p>-Explained to billing that since we were having challenges in profiling FT ISAs at the time, we opted to place 2 PT ISAs instead.&nbsp;</p>

<p>-OM approved that it should be $9.60/hr since it&#39;s considered FT contract.&nbsp;</p>

<p>-Updated Denis that it will be reversed.&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Invoice Dispute',
                'status' => 'acknowleged',
            ),
            30 => 
            array (
                'client_id' => 580,
                'created_at' => '2020-03-26 02:04:22',
                'deleted_at' => NULL,
                'id' => 33,
                'interaction_type' => 'call',
                'note' => '<p>Manny requested to cancel the services because business is slowing down due to COVID19. He said there&#39;s no income lately because most houses are being put off the market.</p>

<p>&gt;discussed that he&#39;s signed up on the 90 day contract. If he cancels before April 27th, he needs to pay the $500 ETF.</p>

<p>&gt;Save offer- reduced hours and availed 50% discount on his hourly rate charge (approved by OM).</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Reduce Hours and Rate Discount Request',
                'status' => 'done',
            ),
            31 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-03-26 02:08:32',
                'deleted_at' => NULL,
                'id' => 34,
                'interaction_type' => 'others',
                'note' => '<p>Discussed pending invoice for $178.54 covering March 11th - 15th hours of VA Charlyn Hernando before she was placed on hold.</p>

<p>&gt;She invoice will be settled tomorrow.</p>',
                'note_type' => 'collection',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            32 => 
            array (
                'client_id' => 591,
                'created_at' => '2020-03-26 17:55:50',
                'deleted_at' => NULL,
                'id' => 35,
                'interaction_type' => 'call',
                'note' => '<p>Followed up today&#39;s task for Hanna. reached vm and sent text .</p>

<p>Ben emailed the tasks. All good</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            33 => 
            array (
                'client_id' => 136,
                'created_at' => '2020-03-26 18:00:17',
                'deleted_at' => NULL,
                'id' => 36,
                'interaction_type' => 'call',
                'note' => '<p>Followed up start date and tasks for VA Stephanie and Clarisse with POC Dan Murray</p>

<p>Left VM and sent text</p>

<p>Awaiting response</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            34 => 
            array (
                'client_id' => 158,
                'created_at' => '2020-03-26 18:03:40',
                'deleted_at' => NULL,
                'id' => 37,
                'interaction_type' => 'email',
                'note' => '<p>Coordinated with Mary, Zach&#39;s accountant</p>

<p>Infomed payment payment option</p>

<p>As per Om , Pavel can drive and pick it up. they can leave it in an envelope by the front door also</p>

<p>Awaiting response</p>

<p>&nbsp;</p>',
                'note_type' => 'collection',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            35 => 
            array (
                'client_id' => 224,
                'created_at' => '2020-03-26 21:43:20',
                'deleted_at' => NULL,
                'id' => 38,
                'interaction_type' => 'call',
                'note' => '<p>&gt;Spoke to him to confirm resumption of services. He wants to extend it until April 3.&nbsp;</p>

<p>&gt;He also said that he will talk to his team first on Monday and get back to me to give an update if they can bring back Michelle sooner.</p>

<p>&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            36 => 
            array (
                'client_id' => 575,
                'created_at' => '2020-03-26 22:09:29',
                'deleted_at' => NULL,
                'id' => 39,
                'interaction_type' => 'others',
                'note' => '<p>Called last week, yesterday and today to follow up on their hold status but no response. Sent email and message via skype as well.</p>

<p>&gt;Final email notification sent today.</p>

<p>&gt;Email cancellation sent.</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => 'Call, Email and Skype',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            37 => 
            array (
                'client_id' => 466,
                'created_at' => '2020-03-28 15:19:38',
                'deleted_at' => NULL,
                'id' => 40,
                'interaction_type' => 'call',
                'note' => '<p>26- March 2020</p>

<p>Touch base on clients concerns on VA&#39;s power outage issues. Client got Robbed. Invoice will be settled after 3-5 business days</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            38 => 
            array (
                'client_id' => 633,
                'created_at' => '2020-03-28 15:21:11',
                'deleted_at' => NULL,
                'id' => 41,
                'interaction_type' => 'email',
                'note' => '<p>27-March 2020</p>

<p>I&#39;m very happy with July&#39;s performance so far. He&#39;s very smart and efficient. Also, he figures out things out very quickly.</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            39 => 
            array (
                'client_id' => 636,
                'created_at' => '2020-03-28 15:23:20',
                'deleted_at' => NULL,
                'id' => 42,
                'interaction_type' => 'text',
                'note' => '<p>28- March 2020</p>

<p>Mark sent a text reply for Toni&#39;s performance.&nbsp;</p>

<p>She&#39;s been very good</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            40 => 
            array (
                'client_id' => 645,
                'created_at' => '2020-03-28 15:28:57',
                'deleted_at' => NULL,
                'id' => 43,
                'interaction_type' => 'email',
                'note' => '<p>Client cancelled service due to finacial issues and end of contract</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Confirmed cancellation',
                'status' => 'done',
            ),
            41 => 
            array (
                'client_id' => 612,
                'created_at' => '2020-03-28 15:34:44',
                'deleted_at' => NULL,
                'id' => 44,
                'interaction_type' => 'email',
                'note' => '<p>28 March - Sent touch base request thru email. awaiting client&#39;s response</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            42 => 
            array (
                'client_id' => 313,
                'created_at' => '2020-03-30 18:42:21',
                'deleted_at' => NULL,
                'id' => 45,
                'interaction_type' => 'email',
                'note' => '<p>&ldquo;All is well.&nbsp; We are trying to justify keeping her now that things have changed for our industry, but that has nothing to do with her performance.&nbsp; Thanks. We will let you know if there is any need to change anything.&rdquo;</p>

<p>Acknowledged and asked to update me..</p>

<p>Chris&#39; reply:</p>

<p>&quot;Most certainly. Nicole and I are doing everything we can to make sure that she stays with us&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            43 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-03-30 18:46:26',
                'deleted_at' => NULL,
                'id' => 46,
                'interaction_type' => 'email',
                'note' => '<p>Will have the CI for Replacement GVA on March 30, 4pm CST - that&#39;s March 31 at 5am MNL</p>

<p>Candidates:</p>

<p>Aldrin Maghirang</p>

<p>Jesamari Brosas</p>

<p>Daisy de Castro</p>

<p>Jazz Flores</p>

<p>Aware of the updated schedule and will be attending the CI</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            44 => 
            array (
                'client_id' => 497,
                'created_at' => '2020-03-30 19:24:10',
                'deleted_at' => NULL,
                'id' => 47,
                'interaction_type' => 'text',
                'note' => '<p>ff up if he has decided on who to get as replacement ISA called - left VM and SMS sent</p>

<p>April 13</p>

<p>ff up on resumption of svc with replacement ISA</p>

<ul>
<li style="list-style-type:disc">Things are crazy for the past weeks - will be working on the replacement ISA</li>
</ul>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            45 => 
            array (
                'client_id' => 634,
                'created_at' => '2020-03-30 19:26:57',
                'deleted_at' => NULL,
                'id' => 48,
                'interaction_type' => 'email',
                'note' => '<p>Shereece has advised that she is currently crating the task list with log ins.</p>

<p>She will be sending over later today</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            46 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-03-30 21:59:22',
                'deleted_at' => NULL,
                'id' => 49,
                'interaction_type' => 'email',
                'note' => '<p>&gt;Client informed that he&#39;s extending the hold on Kavin&#39;s services until this week</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Extension',
                'status' => 'acknowleged',
            ),
            47 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-03-30 22:02:49',
                'deleted_at' => NULL,
                'id' => 50,
                'interaction_type' => 'others',
                'note' => '<p>&gt;Dawn wanted to put the services on-hold until April 30th.</p>

<p>&gt;She still wants Iryn to work until tomorrow (March 31-end of month).</p>

<p>&gt;Services will resume May 1st.</p>

<p>&gt;Gave her a heads up that when she comes back, Iryn might not be available anymore.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'Call & Email',
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Request',
                'status' => 'acknowleged',
            ),
            48 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-03-30 22:10:32',
                'deleted_at' => NULL,
                'id' => 51,
                'interaction_type' => 'others',
                'note' => '<p>&gt;Followed up on her pending invoice for $178.54 for Charlyn&#39;s hours from March 11-13.</p>

<p>&gt;She said that her old credit card was lost and she&#39;s still on the process of getting a new one.</p>',
                'note_type' => 'collection',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            49 => 
            array (
                'client_id' => 562,
                'created_at' => '2020-03-30 22:25:41',
                'deleted_at' => NULL,
                'id' => 52,
                'interaction_type' => 'text',
                'note' => '<p>&gt;Informed Rossy that Mark is currently having intermittent connection and it&#39;s being fixed.</p>

<p>&gt;Mark will notify her once he&#39;s back online.</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            50 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-03-30 22:52:55',
                'deleted_at' => NULL,
                'id' => 53,
                'interaction_type' => 'others',
                'note' => '<p>&gt;Gracee&#39;s Saturday schedule will be changed from 10AM - 2PM EST to 9AM - 1PM EST.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Schedule Change',
                'status' => 'done',
            ),
            51 => 
            array (
                'client_id' => 630,
                'created_at' => '2020-03-31 14:39:32',
                'deleted_at' => NULL,
                'id' => 54,
                'interaction_type' => 'text',
                'note' => '<p>He is great. He has been flexible with changing task due to how COVID-19 is impacting our business</p>

<p><br />
&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            52 => 
            array (
                'client_id' => 580,
                'created_at' => '2020-03-31 14:49:48',
                'deleted_at' => NULL,
                'id' => 55,
                'interaction_type' => 'others',
                'note' => '<p>&gt;Client requested to have Kenah Lequigan&#39;s schedule from 8:30AM - 10:30AM changed to 5PM - 7PM EST.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'Hang Outs',
                'others_status' => 'Please specify status type',
                'others_type' => 'Schedule Change Request',
                'status' => 'done',
            ),
            53 => 
            array (
                'client_id' => 314,
                'created_at' => '2020-03-31 15:31:10',
                'deleted_at' => NULL,
                'id' => 56,
                'interaction_type' => 'call',
                'note' => '<p>Andrey: Request for weekly and bi-weekly reports</p>

<p>Laura: Confirmed training/meeting tomorrow on processes, products and standards of Flyhomes</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            54 => 
            array (
                'client_id' => 435,
                'created_at' => '2020-03-31 15:38:01',
                'deleted_at' => NULL,
                'id' => 57,
                'interaction_type' => 'email',
                'note' => '<p>John decided to cease the service until April 10th. Checked if wil be considering to get another VA instead-declined. Advised of the cancellation process. Coordinated with OM Tima.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            55 => 
            array (
                'client_id' => 539,
                'created_at' => '2020-03-31 15:43:15',
                'deleted_at' => NULL,
                'id' => 58,
                'interaction_type' => 'call',
                'note' => '<p>Followed-up on the decision on the latest CI. Requested to get another set of VA profiles to review.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            56 => 
            array (
                'client_id' => 279,
                'created_at' => '2020-03-31 15:47:47',
                'deleted_at' => NULL,
                'id' => 59,
                'interaction_type' => 'email',
                'note' => '<p>Reached out to inform about making sure that Rhony follows instructions/directives. Informed about mislabeling on Dotloop tasks. Informed of thorough coaching with Rhony.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            57 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-03-31 15:50:33',
                'deleted_at' => NULL,
                'id' => 60,
                'interaction_type' => 'call',
                'note' => '<p><br />
Request for cancellation due to current crisis and that they need to cut losses. Assured to resume services once the situation improves.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            58 => 
            array (
                'client_id' => 618,
                'created_at' => '2020-03-31 18:08:37',
                'deleted_at' => NULL,
                'id' => 61,
                'interaction_type' => 'others',
                'note' => '<p>Scheduled call today at 11am Pacific , reached vm, sent text, awaiting response</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            59 => 
            array (
                'client_id' => 648,
                'created_at' => '2020-03-31 18:10:56',
                'deleted_at' => NULL,
                'id' => 62,
                'interaction_type' => 'call',
                'note' => '<p>Requested account suspension for a Month.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            60 => 
            array (
                'client_id' => 612,
                'created_at' => '2020-03-31 20:25:38',
                'deleted_at' => NULL,
                'id' => 63,
                'interaction_type' => 'call',
                'note' => '<p>Showed concerns regarding VA&#39;s performance. Low ROI. May opt for replacement if no results by Friday</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            61 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-04-01 02:27:30',
                'deleted_at' => NULL,
                'id' => 64,
                'interaction_type' => 'call',
                'note' => '<p>&gt;She&#39;s complaining about 50% discounted rate discontinued because there&#39;s no assurance from her that she will resume the services after April 30th.</p>

<p>&gt;Also requesting for Iryn to work again tomorrow to turn finish the tasks she assigned (she also wants this at a discounted rate). If she can&#39;t be on a discounted rate, today will be Iryn&#39;s last day with her.</p>

<p>&gt;Discussed why we can&#39;t give her the discount any more.</p>

<p>&gt;Will call her back tomorrow for the cancellation.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Discount Rate Dispute',
                'status' => 'acknowleged',
            ),
            62 => 
            array (
                'client_id' => 387,
                'created_at' => '2020-04-01 02:32:06',
                'deleted_at' => NULL,
                'id' => 65,
                'interaction_type' => 'call',
                'note' => '<p>&gt;Sent a text message requesting to cancel Dawn Hao&#39;s services because it is not working anymore.</p>

<p>&gt;Discussed about our cancellation policy. We require 14 days notice for the cancellation.</p>

<p>&gt;Advised her and her Business Manager Hamid that April 14, 2020 will be Dawn&#39;s last day with them.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation',
                'status' => 'acknowleged',
            ),
            63 => 
            array (
                'client_id' => 311,
                'created_at' => '2020-04-01 02:35:29',
                'deleted_at' => NULL,
                'id' => 66,
                'interaction_type' => 'email',
                'note' => '<p>&gt;Confirmed that she requested for her VA Celliza to take today off with Pay.</p>

<p>&gt;Notification sent via email by VA</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            64 => 
            array (
                'client_id' => 637,
                'created_at' => '2020-04-01 16:19:44',
                'deleted_at' => NULL,
                'id' => 67,
                'interaction_type' => 'email',
                'note' => '<p>1st touch base</p>

<p>&nbsp;Kris is doing a great job. We love that he has been very proactive and ready to tackle any problems. Great attitude and we are happy that we chose him.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            65 => 
            array (
                'client_id' => 585,
                'created_at' => '2020-04-02 01:19:45',
                'deleted_at' => NULL,
                'id' => 68,
                'interaction_type' => 'others',
                'note' => '<p>Requested his services to be put on hold for another 2weeks, from April 1-15, 2020. He will resume services on April 16th.</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            66 => 
            array (
                'client_id' => 631,
                'created_at' => '2020-04-02 01:26:43',
                'deleted_at' => NULL,
                'id' => 69,
                'interaction_type' => 'email',
                'note' => '<p>Received a new copy of TB contract. Coordinated with CSM , she also confirms the status&nbsp;with marketing.&nbsp;</p>

<p>Informed Ria that the initial contract with the 14hrs remaining is still active.</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            67 => 
            array (
                'client_id' => 296,
                'created_at' => '2020-04-02 01:29:00',
                'deleted_at' => NULL,
                'id' => 70,
                'interaction_type' => 'email',
                'note' => '<p>Informed closing bonus for Giselle has been forwarded to Billing. Sent the closing bonus tracker.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            68 => 
            array (
                'client_id' => 614,
                'created_at' => '2020-04-02 03:23:07',
                'deleted_at' => NULL,
                'id' => 71,
                'interaction_type' => 'email',
                'note' => '<p>Hi Aiko,</p>

<p>&nbsp;</p>

<p>We are in a shelter in place until April 30 so I won&rsquo;t be able to go on appointments or really work until after that. I am due for having my baby on May 4. Realistically, I don&rsquo;t see being able to push business practices until the middle to end of May. I don&rsquo;t think we will be able to work freely for a while in the United States. They have quarantined us and put us into a recession. Crazy times. &nbsp;</p>

<p>&nbsp;</p>

<p>I hope this helps the understanding! &nbsp;</p>

<p>&nbsp;</p>

<p>Nikki&nbsp;</p>

<p>-----------------------------</p>

<p>&gt;Sent her the Disaster Loan Assistance link&nbsp;</p>

<p>&gt;OM approved the hold extension for her.</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            69 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-04-02 03:24:42',
                'deleted_at' => NULL,
                'id' => 72,
                'interaction_type' => 'others',
                'note' => '<p>&gt;Requested to extend the hold on the services for another 2 weeks.</p>

<p>&gt;OM approved.</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            70 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-04-02 03:27:14',
                'deleted_at' => NULL,
                'id' => 73,
                'interaction_type' => 'call',
                'note' => '<p>&gt;Client requested that she be granted the 50% discount for the month of April because her husband doesn&#39;t have a job anymore and she and her son was quarantined because they came back from Amsterdam and Germany.&nbsp;</p>

<p>&gt;Also requested that for the month of April, VA will only log in if she&#39;s needed.&nbsp;</p>

<p>&gt;OM approved</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            71 => 
            array (
                'client_id' => 361,
                'created_at' => '2020-04-02 20:04:42',
                'deleted_at' => NULL,
                'id' => 74,
                'interaction_type' => 'email',
                'note' => '<p>Email sent with information and link to POC Kierra.</p>

<p>Reply received:</p>

<p>That is very kind of you to share, thank you! I hope you are staying safe and healthy!</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'SBA - Disaster Loan Assistance due to Covid 19',
                'status' => 'acknowleged',
            ),
            72 => 
            array (
                'client_id' => 136,
                'created_at' => '2020-04-02 21:12:29',
                'deleted_at' => NULL,
                'id' => 75,
                'interaction_type' => 'others',
                'note' => '<p>VA recommendation, he is looking for a video editor.</p>

<p>He is still thinking about about the replacement VA. Will check with their head editor to see what he needs exactly.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            73 => 
            array (
                'client_id' => 603,
                'created_at' => '2020-04-02 21:14:31',
                'deleted_at' => NULL,
                'id' => 76,
                'interaction_type' => 'others',
                'note' => '<p>Endorsed JF to continue remaining tasks. Brianna will add TB hrs.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Email , texr',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            74 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-04-03 01:58:57',
                'deleted_at' => NULL,
                'id' => 77,
                'interaction_type' => 'email',
                'note' => '<p>Harrison Thornhill</p>

<p>1:07 AM (20 hours ago)</p>

<p>to&nbsp;me,&nbsp;Adam,&nbsp;Fatima,&nbsp;Jose,&nbsp;Kavin,&nbsp;kavinjay</p>

<p><img alt="" src="https://mail.google.com/mail/u/0/images/cleardot.gif" /></p>

<p>Hello everyone,</p>

<p>&nbsp;</p>

<p>I am currently waiting for our office to confirm the implementation of our new rental software (this should have been completed weeks ago). I should have more information on the completion date after a call tomorrow or Friday&nbsp;at the latest.</p>

<p>&nbsp;</p>

<p>We are awaiting that and a shelter in place order from the governor to go into effect on Friday, announced this evening. Details on this order other than the start day have not been announced. Currently, We don&rsquo;t have details on this or if real estate will still be able to function and to what extent when it does go into effect.&nbsp;</p>

<p>&nbsp;</p>

<p>For now, just need Kavin to standby until next week when we know more.&nbsp;</p>

<p>&nbsp;</p>

<p>My apologies for continuing to push this off - but these are very strange times.&nbsp; Thank you to everyone for being flexible.&nbsp;</p>

<p>&nbsp;</p>

<p>Sincerely,</p>

<p>Harrison&nbsp;</p>

<p>-------------------------------------</p>

<p>-Sent him the link for the Disaster Loan Assistance.</p>

<p>&gt;He said that they already applied for it and they&#39;re just awaiting response.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Extension',
                'status' => 'acknowleged',
            ),
            75 => 
            array (
                'client_id' => 224,
                'created_at' => '2020-04-03 02:01:15',
                'deleted_at' => NULL,
                'id' => 78,
                'interaction_type' => 'email',
                'note' => '<p>John Rainville</p>

<p>11:33 AM (10 hours ago)</p>

<p>to&nbsp;<strong>Robin</strong>,&nbsp;<strong>Betty</strong>,&nbsp;me</p>

<p><img alt="" src="https://mail.google.com/mail/u/0/images/cleardot.gif" /></p>

<p>Aiko,</p>

<p>&nbsp;</p>

<p>We are still shut for business thru the end if April at this point&nbsp;in time.&nbsp;</p>

<p>&nbsp;</p>

<p>I do not want to incur Bills I cannot pay.</p>

<p>&nbsp;</p>

<p>I&#39;d like to maintain the pause on Michelle thru May 1st.&nbsp; I understand if she needs to go to work for someone else.</p>

<p>----------------------------------</p>

<p>&gt;Sent him the link for the Disaster Loan Assistance.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Extension',
                'status' => 'acknowleged',
            ),
            76 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-04-03 02:02:49',
                'deleted_at' => NULL,
                'id' => 79,
                'interaction_type' => 'call',
                'note' => '<p>&gt;Discussed her revised invoice.</p>

<p>&gt;Informed that she still wants Iryn to work tomorrow before we transition to TB.</p>

<p>&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            77 => 
            array (
                'client_id' => 534,
                'created_at' => '2020-04-03 02:06:54',
                'deleted_at' => NULL,
                'id' => 80,
                'interaction_type' => 'call',
                'note' => '<p>&gt;Provided very good feedback for Roselyn for taking on all their admin tasks</p>

<p>&gt;Said that they are actually thinking of increasing the VA&#39;s hours (PT to FT)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            78 => 
            array (
                'client_id' => 466,
                'created_at' => '2020-04-03 17:41:20',
                'deleted_at' => NULL,
                'id' => 81,
                'interaction_type' => 'call',
                'note' => '<p>Rate change inquiry.&nbsp;</p>',
                'note_type' => 'consultation',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            79 => 
            array (
                'client_id' => 591,
                'created_at' => '2020-04-03 17:43:50',
                'deleted_at' => NULL,
                'id' => 82,
                'interaction_type' => 'email',
                'note' => '<p>Additional VA endorsement , coordinated with CSM La</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            80 => 
            array (
                'client_id' => 635,
                'created_at' => '2020-04-03 20:43:42',
                'deleted_at' => NULL,
                'id' => 83,
                'interaction_type' => 'email',
                'note' => '<p>April 1, 2020</p>

<p>Sent The 18 Advantges of Today&#39;s Market from Mike Ferry Office to help motivate Rich in this trying time.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            81 => 
            array (
                'client_id' => 632,
                'created_at' => '2020-04-03 20:56:37',
                'deleted_at' => NULL,
                'id' => 84,
                'interaction_type' => 'text',
                'note' => '<p>April 2, 2020</p>

<p>Jeff has requested to have a conference call this morning with his Broker.&nbsp;</p>

<p>Jeff has decided to move the conference call to next week (prob Monday) as his broker didn&#39;t make it today.</p>

<p>This has been acknowledged</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
            'others_type' => 'For Conference Call with Broker (add\'l time for VA)',
                'status' => 'open',
            ),
            82 => 
            array (
                'client_id' => 343,
                'created_at' => '2020-04-03 21:25:40',
                'deleted_at' => NULL,
                'id' => 85,
                'interaction_type' => 'others',
                'note' => '<p>Account on Temporary Hold on March 23, 2020 for 30 days.&nbsp;</p>

<p>Resumption date: April 22, 2020</p>

<p>April 3, 2020</p>

<p>Payment reminder sent - text/ FB msgr and VM</p>

<p>REplied via FB messenger - she will provide new cc dtls</p>',
                'note_type' => 'collection',
                'noted_by' => 22,
                'others_interaction' => 'FB Msgr',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            83 => 
            array (
                'client_id' => 440,
                'created_at' => '2020-04-03 21:39:21',
                'deleted_at' => NULL,
                'id' => 86,
                'interaction_type' => 'text',
                'note' => '<p>Invoice settled. Holde service request for a month due to COVID 19</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            84 => 
            array (
                'client_id' => 603,
                'created_at' => '2020-04-04 04:51:19',
                'deleted_at' => NULL,
                'id' => 87,
                'interaction_type' => 'text',
                'note' => '<p>Asked availability on when she can confirm the verification code required to log in on social media platforms for VA JF , follow up call on Monday</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            85 => 
            array (
                'client_id' => 594,
                'created_at' => '2020-04-04 04:53:15',
                'deleted_at' => NULL,
                'id' => 88,
                'interaction_type' => 'email',
                'note' => '<p>Sent additional admin/marketing tasks that she can assign with Cielo, acknowledged schedule update</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            86 => 
            array (
                'client_id' => 640,
                'created_at' => '2020-04-04 04:55:34',
                'deleted_at' => NULL,
                'id' => 89,
                'interaction_type' => 'others',
                'note' => '<p>Touchbase follow up regarding VA services, contract ends on Monday April 6th</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            87 => 
            array (
                'client_id' => 423,
                'created_at' => '2020-04-04 22:33:32',
                'deleted_at' => NULL,
                'id' => 90,
                'interaction_type' => 'others',
                'note' => '<p>Coordinated on the request to add more hours for VA Pam.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            88 => 
            array (
                'client_id' => 307,
                'created_at' => '2020-04-04 22:35:09',
                'deleted_at' => NULL,
                'id' => 91,
                'interaction_type' => 'email',
                'note' => '<p>Followed-up on the requested meeting to discuss adding more hours for VA Bianca.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            89 => 
            array (
                'client_id' => 539,
                'created_at' => '2020-04-04 22:38:19',
                'deleted_at' => NULL,
                'id' => 92,
                'interaction_type' => 'others',
                'note' => '<p>Requested to finish TBS hours until yesterday.&nbsp; Will follow up for the ISA services after 30 days.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            90 => 
            array (
                'client_id' => 598,
                'created_at' => '2020-04-04 22:40:29',
                'deleted_at' => NULL,
                'id' => 93,
                'interaction_type' => 'email',
                'note' => '<p>Coordinated on SBA DLA information sent to her. She will consider applying for the program.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            91 => 
            array (
                'client_id' => 638,
                'created_at' => '2020-04-04 22:54:04',
                'deleted_at' => NULL,
                'id' => 94,
                'interaction_type' => 'others',
                'note' => '<p>Manager Laura is also continuously training both VAs on processes and tools.</p>

<p>Had a second round of interview with shortlisted candidates.</p>

<p>April 4- followed up on the status; advised to coordinate with Andrey</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            92 => 
            array (
                'client_id' => 274,
                'created_at' => '2020-04-04 22:56:31',
                'deleted_at' => NULL,
                'id' => 95,
                'interaction_type' => 'others',
                'note' => '<p>April 1***<br />
Requested to temporarily reduce hours because of COVID situation.<br />
Request to receive annual invoice as requirement for the disaster relief loan.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            93 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-04-06 15:35:25',
                'deleted_at' => NULL,
                'id' => 96,
                'interaction_type' => 'email',
                'note' => '<p>Received schedule update and login info. Lara&#39;s start date today.</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            94 => 
            array (
                'client_id' => 603,
                'created_at' => '2020-04-06 15:40:02',
                'deleted_at' => NULL,
                'id' => 97,
                'interaction_type' => 'text',
                'note' => '<p>TB tasks follow up</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            95 => 
            array (
                'client_id' => 591,
                'created_at' => '2020-04-06 15:45:27',
                'deleted_at' => NULL,
                'id' => 98,
                'interaction_type' => 'call',
                'note' => '<p>Followed up CI schedule for the additional VA.</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            96 => 
            array (
                'client_id' => 534,
                'created_at' => '2020-04-07 00:07:37',
                'deleted_at' => NULL,
                'id' => 99,
                'interaction_type' => 'text',
                'note' => '<p>Asked if Roselyne is willing to attend training and learn Adobe Photoshop - informed her that I already discussed it with VA and she&#39;s willing</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            97 => 
            array (
                'client_id' => 616,
                'created_at' => '2020-04-07 00:08:46',
                'deleted_at' => NULL,
                'id' => 100,
                'interaction_type' => 'text',
                'note' => '<p>Requested to extend the hold on the services for another 2weeks and see if everything is back to normal by then - expected resumption of services is on April 20, 2020. Sent him the new link for the DIsaster assistance loan.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Extension',
                'status' => 'acknowleged',
            ),
            98 => 
            array (
                'client_id' => 548,
                'created_at' => '2020-04-07 00:09:53',
                'deleted_at' => NULL,
                'id' => 101,
                'interaction_type' => 'others',
                'note' => '<p>Quick Touchbase - said that so far, business is still running. They are still within target when it comes to closings and they can now schedule home showings as long as they observe proper social distancing. They don&#39;t have open houses yet though.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'FB messenger',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            99 => 
            array (
                'client_id' => 160,
                'created_at' => '2020-04-07 00:11:03',
                'deleted_at' => NULL,
                'id' => 102,
                'interaction_type' => 'others',
                'note' => '<p>Touchbased with him via FB messenger - said that business is a bit slow at this time but he thinks that they are still doing okay and will be able to pull through. John is a big help to them.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'FB messenger',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            100 => 
            array (
                'client_id' => 387,
                'created_at' => '2020-04-07 00:12:07',
                'deleted_at' => NULL,
                'id' => 103,
                'interaction_type' => 'call',
                'note' => '<p>Confirmed that they are retracting their cancellation request - Dawn will continue to work for them</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Retraction of Cancellation',
                'status' => 'done',
            ),
            101 => 
            array (
                'client_id' => 586,
                'created_at' => '2020-04-07 13:01:16',
                'deleted_at' => NULL,
                'id' => 104,
                'interaction_type' => 'call',
                'note' => '<p>Client initially requested to suspend services due to fiancial issues brought by COVID 19. Offered the 50% rate discount. Agreed to continue services</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Request for account suspension',
                'status' => 'acknowleged',
            ),
            102 => 
            array (
                'client_id' => 612,
                'created_at' => '2020-04-07 13:10:45',
                'deleted_at' => NULL,
                'id' => 105,
                'interaction_type' => 'call',
                'note' => '<p>As per agreement John was not able to set up appointments. Replacement identified.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            103 => 
            array (
                'client_id' => 591,
                'created_at' => '2020-04-07 16:33:13',
                'deleted_at' => NULL,
                'id' => 106,
                'interaction_type' => 'text',
                'note' => '<p>Confirmed CI schedule today at 11am PST.</p>

<p>Endorsed to CSM La</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            104 => 
            array (
                'client_id' => 603,
                'created_at' => '2020-04-07 16:36:30',
                'deleted_at' => NULL,
                'id' => 107,
                'interaction_type' => 'others',
                'note' => '<p>Call today at 11am</p>

<p>She emailed and reschedule at 12:30 CST</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call , email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            105 => 
            array (
                'client_id' => 594,
                'created_at' => '2020-04-07 16:38:44',
                'deleted_at' => NULL,
                'id' => 108,
                'interaction_type' => 'call',
                'note' => '<p>Scheduled touchbase at 11am EST</p>

<p>Reached VM</p>

<p>Email additional marketing tasks list recommendation</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            106 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-04-07 16:46:46',
                'deleted_at' => NULL,
                'id' => 109,
                'interaction_type' => 'email',
                'note' => '<p>Received scripts for Lara, prefers personal approach.&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            107 => 
            array (
                'client_id' => 146,
                'created_at' => '2020-04-07 16:48:05',
                'deleted_at' => NULL,
                'id' => 110,
                'interaction_type' => 'email',
                'note' => '<p>Sent pay raise notification for VA Rowena, 2yr milestone</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            108 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-04-08 00:21:30',
                'deleted_at' => NULL,
                'id' => 111,
                'interaction_type' => 'others',
                'note' => '<p>Reached out to her to follow-up on her pending invoice of $178.54. She said she hasn&#39;t gotten her new credit card yet and will inform us before the week ends. Expected resumption of services will be on April 14, 2020</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Collection & Follow Up',
                'status' => 'acknowleged',
            ),
            109 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-04-08 00:22:26',
                'deleted_at' => NULL,
                'id' => 112,
                'interaction_type' => 'others',
                'note' => '<p>Discussed Jefte&#39;s improvement in sending reports and in tracking warm leads for them. This is a very slow season but he was able to set up 1 appt/demo calls for them last week.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Zoom Conference',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            110 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-04-08 00:23:34',
                'deleted_at' => NULL,
                'id' => 113,
                'interaction_type' => 'text',
                'note' => '<p>Touchbase - he said that Gracee is a superstar but Edjonie isn&#39;t doing a good job unfortunately. He will be sending me some of Jonie&#39;s recordings tonight and we will discuss tomorrow if we are going to have Gracee on FT or replace Jonie.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            111 => 
            array (
                'client_id' => 509,
                'created_at' => '2020-04-08 17:53:52',
                'deleted_at' => NULL,
                'id' => 114,
                'interaction_type' => 'text',
                'note' => '<p>She is happy and everything works well with her VA.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            112 => 
            array (
                'client_id' => 427,
                'created_at' => '2020-04-09 02:12:09',
                'deleted_at' => NULL,
                'id' => 115,
                'interaction_type' => 'others',
                'note' => '<p>He said they cutting back on Customer Service Hours for Allen but they are not reducing his hours - they endorsed him to Gerry the VP of Sales for the company and Allen will be doing Admin work for the mean time.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Skype',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            113 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-04-09 02:14:00',
                'deleted_at' => NULL,
                'id' => 116,
                'interaction_type' => 'call',
                'note' => '<p>Calibration call on Jonie&#39;s performance - listened to some of her calls and discussed call handling and other inconsistencies. He said he can&#39;t have her represent him so he decided to cancel on Edjonie and replace her with a more experienced ISA that is confident on the phone. He will interview Charlyn Hernando and JC Reyes tomorrow at 3PM EST.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            114 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-04-09 02:15:00',
                'deleted_at' => NULL,
                'id' => 117,
                'interaction_type' => 'others',
                'note' => '<p>Gave new credit card information to pay for her pending invoice of $178.54. She requested to use the new credit card moving forward. She&#39;s looking forward to having Charlyn back on April 14th.</p>',
                'note_type' => 'collection',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            115 => 
            array (
                'client_id' => 587,
                'created_at' => '2020-04-09 14:50:22',
                'deleted_at' => NULL,
                'id' => 118,
                'interaction_type' => 'text',
                'note' => '<p>April 8, 2020</p>

<p>Mikki has advised that she will not extend - to cancel svcs/ End of Contract</p>

<p>- wants to put acct on 6 month hold - explained to her that we can&#39;t put it on hold for that long.</p>

<p>- no money coming in and clients are cancelling due to the Covid pandemic</p>

<p>VAs are good and have been following through - SPECIAL KUDOS to Sam Jagualing - amazing job</p>

<p>Cancellation effectivity date: April 9, 2020</p>

<p>cc on file is already frozen.</p>

<p>Asked for Final bill so she can make payment&nbsp;- aware that payment due is April 10, 2020</p>

<p>Final Invoice has been sent to her with cc form</p>

<p>==============</p>

<p>April 13, 2020</p>

<p>ff up on Final invoice payment - text and call - left VM - no reply</p>

<p>update - Mikki has signed cancellation form but no update on cc dtls</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            116 => 
            array (
                'client_id' => 632,
                'created_at' => '2020-04-09 14:58:12',
                'deleted_at' => NULL,
                'id' => 119,
                'interaction_type' => 'call',
                'note' => '<p>April 6, 2020</p>

<p>Conference Call with Jeff and Keith (broker)</p>

<ul>
<li style="list-style-type:disc">Keith - leaning to going to FT for Jen</li>
<li style="list-style-type:disc">Jenyly is talented -&nbsp;</li>
<li style="list-style-type:disc">Wants to keep her</li>
<li style="list-style-type:disc">Impressed with her</li>
<li style="list-style-type:disc">Extremely happy with work that she has been doing</li>
<li style="list-style-type:disc">Understands the risk</li>
<li style="list-style-type:disc">Add more hours in 2 or 3 weeks</li>
<li style="list-style-type:disc">might add 2 hrs initially then gradually go to FT - depending on market (due to Covid)</li>
<li style="list-style-type:disc">Whatever is done today - money in 60-90 days</li>
<li style="list-style-type:disc">Pause in the market - positioned well</li>
<li style="list-style-type:disc">New card to use for payment</li>
</ul>

<p>CC form has been sent by OM for new card dtls&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            117 => 
            array (
                'client_id' => 617,
                'created_at' => '2020-04-09 16:36:17',
                'deleted_at' => NULL,
                'id' => 120,
                'interaction_type' => 'call',
                'note' => '<p>April 6, 2020</p>

<p>hold for 30 days effective tom, Apr 7- May 7</p>

<p>Reason: No appts to be set due to Covid Pandemic</p>

<p>no back office or soc med tasks to assign to VA</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            118 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-04-09 16:41:51',
                'deleted_at' => NULL,
                'id' => 121,
                'interaction_type' => 'text',
                'note' => '<p>April 6, 2020</p>

<p>requested for Jesa not to log in today and to just log in on Sat.</p>

<ul>
<li style="list-style-type:disc">Acknowledged</li>
</ul>

<p>April 8, 2020</p>

<p>Feedback on Jesa</p>

<p>&quot;Jesa&#39;s doing great! She&#39;s an excellent VA. She has initiative, follows directions very well and is a quick learner. I really appreciate her. Thanks!&quot;&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            119 => 
            array (
                'client_id' => 441,
                'created_at' => '2020-04-09 16:58:40',
                'deleted_at' => NULL,
                'id' => 122,
                'interaction_type' => 'email',
                'note' => '<p>*** changed/ updated client infor - all info inputted were for a former client:&nbsp;Jim Schaecher</p>

<p>*** Jose still with original VA - Aileen Pormento</p>

<p>******************</p>

<p>April 6, 2020</p>

<p>applied SBA loan yday - asking if he should re-apply</p>

<ul>
<li style="list-style-type:disc">Advd that SBA site is overwhelmed that&rsquo;s why it crashed - he can re apply or call help/ assistance line for clarification.</li>
<li style="list-style-type:disc">provided help line #</li>
</ul>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            120 => 
            array (
                'client_id' => 347,
                'created_at' => '2020-04-09 17:44:02',
                'deleted_at' => NULL,
                'id' => 123,
                'interaction_type' => 'email',
                'note' => '<p>Touch base sent awaiting response</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            121 => 
            array (
                'client_id' => 397,
                'created_at' => '2020-04-09 18:07:27',
                'deleted_at' => NULL,
                'id' => 124,
                'interaction_type' => 'email',
                'note' => '<p>April 7, 2020</p>

<p>Janki wanted to know if Rich has been trained for soc med marketing, etc</p>

<p>Advd that Rich has been trained to do soc med marketing and we also have SMEs and training videos available if VA needs a refresher</p>

<p>=====</p>

<p>April 9, 2020</p>

<p>ff up call made re Janki&#39;s initial query - no answer, left VM</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            122 => 
            array (
                'client_id' => 501,
                'created_at' => '2020-04-09 20:12:42',
                'deleted_at' => NULL,
                'id' => 125,
                'interaction_type' => 'email',
                'note' => '<p>April 7, 2020</p>

<p>Feedback: &ldquo;Paola is doing fine.&rdquo;</p>

<p>Query if Paola can go on FT</p>

<p>Advd that Paola is unable to go on FT as she also have another PT client.</p>

<p>Offered antoher VA - advd that Paola can train new VA as well (watiing for reply)</p>

<p>=========</p>

<p>April 10, 2020</p>

<p>David decided to add another VA for PT&nbsp;</p>

<p>- for chat support</p>

<ul>
<li style="list-style-type:disc">With excellent written english skills</li>
<li style="list-style-type:disc">Online school - courses are self paced/ kids watch online and when they have questions or cxs write in sales questions - can be helped for the next morning.</li>
<li style="list-style-type:disc">Intercom - chat svc that they use - allow to see tickets that come in that are technical questions that&nbsp;</li>
<li style="list-style-type:disc">Set responses that would send back to cxs</li>
<li style="list-style-type:disc">Set up in Callandly</li>
<li style="list-style-type:disc">Sales related questions</li>
<li style="list-style-type:disc">Support data base with all answers -&nbsp;</li>
<li style="list-style-type:disc">If slow - another assignment that can be put on like a back burner type thing</li>
<li style="list-style-type:disc">Watching computer tutorials and creates a guide.</li>
<li style="list-style-type:disc">Hours - 7pm-11pm AZ Mon-Fri</li>
<li style="list-style-type:disc">Test - set up a chat session with them - take through a number of scenarios</li>
<li style="list-style-type:disc">Word processing skills - ability to watch videos and create guides&nbsp;</li>
<li style="list-style-type:disc">Attention to detail - detail oriented.</li>
<li style="list-style-type:disc">Excellent interpersonal skills over chat</li>
<li style="list-style-type:disc">As soon as possible&nbsp;</li>
<li style="list-style-type:disc">If applicants have computer programming before - is a plus</li>
</ul>

<p>via phone call</p>

<p>April 13, 2020</p>

<p>CVs of candidates has been sent. - waiting for reply on time of test</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            123 => 
            array (
                'client_id' => 628,
                'created_at' => '2020-04-09 20:39:53',
                'deleted_at' => NULL,
                'id' => 126,
                'interaction_type' => 'call',
                'note' => '<p>April 7, 2020</p>

<p>wants to buy a 2nd monitor as a gift - to make her work flow easier -&nbsp;</p>

<ul>
<li style="list-style-type:disc">Cebu Shop - 27 inch monitor - 12500/ $250</li>
<li style="list-style-type:disc">She&rsquo;s doing a great job - She&rsquo;s very fast and very efficient.</li>
<li style="list-style-type:disc">Easier on her eyes.</li>
</ul>

<p>&nbsp;Advd that she will be invoiced on the converted amt of the monitor and Shannon will rcv the money on the upcoming payday. - Annaliese agreed</p>

<p>Interaction type - phone call and email</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            124 => 
            array (
                'client_id' => 515,
                'created_at' => '2020-04-09 21:00:21',
                'deleted_at' => NULL,
                'id' => 127,
                'interaction_type' => 'call',
                'note' => '<p>touch base ff up- text via mobile and fb msgr</p>

<p>Still no response received.</p>

<p>Client sometimes takes more than a month to reply to email or text msg&nbsp;</p>

<p>last response/ reply was Feb 21 from Cory</p>

<p>Interaction type - call, email, text and FB msgr</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            125 => 
            array (
                'client_id' => 619,
                'created_at' => '2020-04-09 21:09:40',
                'deleted_at' => NULL,
                'id' => 128,
                'interaction_type' => 'text',
                'note' => '<p>April 8, 2020</p>

<p>touch base if ready to resume svcs</p>

<p>- no reply rcvd</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            126 => 
            array (
                'client_id' => 361,
                'created_at' => '2020-04-09 21:18:46',
                'deleted_at' => NULL,
                'id' => 129,
                'interaction_type' => 'email',
                'note' => '<p>Feedback from POC Kierra:&nbsp;</p>

<p>&ldquo;Aldrin is doing great, he is helping us create email templates so our operations can run more efficiently once we get busy again.&rdquo;</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            127 => 
            array (
                'client_id' => 46,
                'created_at' => '2020-04-10 00:47:33',
                'deleted_at' => NULL,
                'id' => 130,
                'interaction_type' => 'others',
                'note' => '<p>Resumption of VA services tomorrow, April 10th</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => 'Text , Email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            128 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-04-10 03:07:55',
                'deleted_at' => NULL,
                'id' => 131,
                'interaction_type' => 'others',
                'note' => '<p>Advised on behalf of Vito that Jefte will be calling the EZ YCC leads starting tomorrow because he won&#39;t be dialing for Michele Harris&#39; leads for the mean time</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            129 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-04-10 03:09:25',
                'deleted_at' => NULL,
                'id' => 132,
                'interaction_type' => 'others',
                'note' => '<p>Interviewed both Charlyn Hernando and JC Reyes - he said both VAs are good and he wants to sleep on it tonight before making a decision. Scheduled another call tomorrow for his decision.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'Interview',
                'status' => 'pending',
            ),
            130 => 
            array (
                'client_id' => 534,
                'created_at' => '2020-04-10 03:10:34',
                'deleted_at' => NULL,
                'id' => 133,
                'interaction_type' => 'call',
                'note' => '<p>Discussed FT schedule for VA Roselyne starting on Tuesday, April 14th. Explained the arrangement to her about the VA having another PT client</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'PT to FT Transition',
                'status' => 'done',
            ),
            131 => 
            array (
                'client_id' => 640,
                'created_at' => '2020-04-10 15:51:28',
                'deleted_at' => NULL,
                'id' => 134,
                'interaction_type' => 'others',
                'note' => '<p>Invoice payment follow up, nsufficient funds error</p>

<p>left vm and sent text</p>',
                'note_type' => 'collection',
                'noted_by' => 23,
                'others_interaction' => 'Call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            132 => 
            array (
                'client_id' => 633,
                'created_at' => '2020-04-10 17:03:23',
                'deleted_at' => NULL,
                'id' => 135,
                'interaction_type' => 'text',
                'note' => '<p>Invoice reminder: will be settling her bill on Monday&nbsp;</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            133 => 
            array (
                'client_id' => 637,
                'created_at' => '2020-04-10 17:05:51',
                'deleted_at' => NULL,
                'id' => 136,
                'interaction_type' => 'text',
                'note' => '<p>Invoice collection. Transferred funds to her card. Payment process</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            134 => 
            array (
                'client_id' => 582,
                'created_at' => '2020-04-10 17:09:48',
                'deleted_at' => NULL,
                'id' => 137,
                'interaction_type' => 'text',
                'note' => '<p>Invoice collection. waiting for response.</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            135 => 
            array (
                'client_id' => 610,
                'created_at' => '2020-04-10 18:22:55',
                'deleted_at' => NULL,
                'id' => 138,
                'interaction_type' => 'text',
                'note' => '<p>check on service resumption and requested to extend suspension of service up until the end of month</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            136 => 
            array (
                'client_id' => 301,
                'created_at' => '2020-04-11 02:05:15',
                'deleted_at' => NULL,
                'id' => 139,
                'interaction_type' => 'call',
                'note' => '<p>He is Kyle McLaughlin&#39;s accountant - followed up on their pending invoice. He said that Kyle&#39;s credit card has already been cancelled so they will be using his credit card moving forward. New credit card details sent to OM and billing - invoice settled</p>',
                'note_type' => 'collection',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            137 => 
            array (
                'client_id' => 534,
                'created_at' => '2020-04-11 02:06:42',
                'deleted_at' => NULL,
                'id' => 140,
                'interaction_type' => 'others',
            'note' => '<p>Confirmed new schedule for Roselyne starting on Tuesday, April 14th (2PM - 10PM CST) - transition from PT to FT</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'HangOuts',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            138 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-04-11 02:07:42',
                'deleted_at' => NULL,
                'id' => 141,
                'interaction_type' => 'text',
                'note' => '<p>He said they are still waiting to hear back from the Government for the stimulus money - as soon as they receive this, Kavin will come back.</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            139 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-04-11 02:08:52',
                'deleted_at' => NULL,
                'id' => 142,
                'interaction_type' => 'others',
            'note' => '<p>He hasn&#39;t decided yet between Charlyn Hernando and JC Reyes (advised him to just get both) - he said he will think about this over the weekend since this is a business move in the middle of a global crisis. Another call scheduled on Monday, 3PM EST</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            140 => 
            array (
                'client_id' => 244,
                'created_at' => '2020-04-13 14:22:53',
                'deleted_at' => NULL,
                'id' => 143,
                'interaction_type' => 'email',
                'note' => '<p>Received an email confirmation on CI schedule.</p>

<p>We can book appointment for 11 am on Tuesday, March 14th.</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            141 => 
            array (
                'client_id' => 603,
                'created_at' => '2020-04-13 14:29:26',
                'deleted_at' => NULL,
                'id' => 144,
                'interaction_type' => 'text',
                'note' => '<p>Followed up next tasks for VA JF.&nbsp;</p>

<p>Social media as per Brianna</p>

<p>&quot;Out of the box ideas and thoughts to reach people specifically with the pandemic and going through divorce, probate, senior care transitions&quot;</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            142 => 
            array (
                'client_id' => 284,
                'created_at' => '2020-04-13 15:31:31',
                'deleted_at' => NULL,
                'id' => 145,
                'interaction_type' => 'email',
                'note' => '<p>Would like to change the credit card being used for Virtudesk, sent credit card update form.</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            143 => 
            array (
                'client_id' => 468,
                'created_at' => '2020-04-13 20:46:10',
                'deleted_at' => NULL,
                'id' => 146,
                'interaction_type' => 'call',
                'note' => '<p>Card successfully updated thru updated.</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            144 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-04-14 02:12:11',
                'deleted_at' => NULL,
                'id' => 147,
                'interaction_type' => 'text',
                'note' => '<p>Requested to extend the hold on the services because they are still waiting for the stimulus funds from the Government</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Hold Extension',
                'status' => 'acknowleged',
            ),
            145 => 
            array (
                'client_id' => 534,
                'created_at' => '2020-04-14 02:13:12',
                'deleted_at' => NULL,
                'id' => 148,
                'interaction_type' => 'others',
            'note' => '<p>Confirmed VA transition from PT to FT starting tomorrow, April 14th - new schedule will be from 2PM to 10PM CST. Also discussed the rate adjustment for GVA from PT rate ($9.60/hr) to FT rate ($8.60/hr).</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'Call & Email',
                'others_status' => 'Please specify status type',
                'others_type' => 'PT to FT Transition',
                'status' => 'done',
            ),
            146 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-04-14 02:14:40',
                'deleted_at' => NULL,
                'id' => 149,
                'interaction_type' => 'text',
                'note' => '<p>He confirmed that he will go for VA Charlyn Hernando - he may have her start though on Monday next week, April 20th. Schedule still undecided</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            147 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-04-14 02:15:57',
                'deleted_at' => NULL,
                'id' => 150,
                'interaction_type' => 'others',
            'note' => '<p>Edward Young, Vito&#39;s Assistant that handles KPIs for their ISAs - gave directive about the changes on Jefte&#39;s lead assignments (Monday is for ZOHO leads and Tuesday will be for ZCC leads)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            148 => 
            array (
                'client_id' => 427,
                'created_at' => '2020-04-14 02:17:01',
                'deleted_at' => NULL,
                'id' => 151,
                'interaction_type' => 'call',
                'note' => '<p>Requested to reduce VA Allen&#39;s hours from 8hrs to 4hrs/day - he said they will put their focus on sales efforts with Allen. He also said that this will only be for 2 - 3 weeks until everything gets back to normal. New VA schedule is 1PM - 5PM EST starting on April 15, 2020</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Reduce Hours',
                'status' => 'done',
            ),
            149 => 
            array (
                'client_id' => 244,
                'created_at' => '2020-04-14 15:26:10',
                'deleted_at' => NULL,
                'id' => 152,
                'interaction_type' => 'others',
                'note' => '<p>Facilitated CI with VA Al Exconde</p>

<p>&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => 'RC meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'Client Interview',
                'status' => 'done',
            ),
            150 => 
            array (
                'client_id' => 591,
                'created_at' => '2020-04-14 15:40:55',
                'deleted_at' => NULL,
                'id' => 153,
                'interaction_type' => 'text',
                'note' => '<p>Touchbase schedule reminder , awaiting response</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            151 => 
            array (
                'client_id' => 603,
                'created_at' => '2020-04-14 15:41:51',
                'deleted_at' => NULL,
                'id' => 154,
                'interaction_type' => 'text',
                'note' => '<p>Follow up if the design JF created is approved for social media postings.&nbsp;</p>

<p>She disapproved the design and pause TB for now</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            152 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-04-14 16:24:37',
                'deleted_at' => NULL,
                'id' => 155,
                'interaction_type' => 'text',
                'note' => '<p>follow up on payment for $339.16.</p>

<p>Advd of directive to put VA svc on hold today due to non payment</p>

<p>=====</p>

<p>Adilah called with new cc dtls (call routed to Nica)</p>

<p>New cc dtls provided. Payment processed.</p>

<p>Jesa was able to continue with VA svcs today</p>',
                'note_type' => 'collection',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            153 => 
            array (
                'client_id' => 487,
                'created_at' => '2020-04-14 16:30:04',
                'deleted_at' => NULL,
                'id' => 156,
                'interaction_type' => 'others',
                'note' => '<p>Request to put services on hold until May 3. To resume on May 4th.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and sms',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            154 => 
            array (
                'client_id' => 157,
                'created_at' => '2020-04-14 16:33:20',
                'deleted_at' => NULL,
                'id' => 157,
                'interaction_type' => 'others',
                'note' => '<p>April 10- Request to reduce hours. Provided other options-will check with Sean.</p>

<p>April 13- Accepted offer of a dollar less on the current rate.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and SMS',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            155 => 
            array (
                'client_id' => 537,
                'created_at' => '2020-04-14 16:37:45',
                'deleted_at' => NULL,
                'id' => 158,
                'interaction_type' => 'text',
                'note' => '<p>April 13, 2020</p>

<p>ff up on resumption of svc and replacement GVA -VM left/ SMS sent</p>

<ul>
<li style="list-style-type:disc">Still in lockdown - may be operational by next month - end of next month</li>
</ul>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            156 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-04-14 16:50:56',
                'deleted_at' => NULL,
                'id' => 159,
                'interaction_type' => 'others',
                'note' => '<p>April 7- Confirmed hiring of 2 additional VAs</p>

<p>April 10- Follow-up on additional VAs start date-no decision just yet</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'email, call and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            157 => 
            array (
                'client_id' => 279,
                'created_at' => '2020-04-14 16:55:39',
                'deleted_at' => NULL,
                'id' => 160,
                'interaction_type' => 'email',
                'note' => '<p>April 10-&nbsp;Conference call with Rhony today. Provided feedback on Rhony&#39;s performance. Decided to reduce Rhony&#39;s hours back to 40 and will just hire another assistant to help her on tasks.</p>

<p>April 14- Inform of Rhony&#39;s absence</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            158 => 
            array (
                'client_id' => 435,
                'created_at' => '2020-04-14 16:56:53',
                'deleted_at' => NULL,
                'id' => 161,
                'interaction_type' => 'email',
                'note' => '<p>April 10-Coordinated on VA&#39;s last day today. Sent a thank you email and acknowledged VA&#39;s help and assistance.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            159 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-04-14 17:01:29',
                'deleted_at' => NULL,
                'id' => 162,
                'interaction_type' => 'others',
                'note' => '<p>Followed-up on access on tools and systems of VA Katrina.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            160 => 
            array (
                'client_id' => 536,
                'created_at' => '2020-04-14 17:10:51',
                'deleted_at' => NULL,
                'id' => 163,
                'interaction_type' => 'others',
            'note' => '<p>April 10-&nbsp;Follow up on invoice- will transfer funds on CC today (to follow up on Monday).</p>

<p>April 13- payment follow-up successful</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'email, text and call',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            161 => 
            array (
                'client_id' => 549,
                'created_at' => '2020-04-14 17:13:56',
                'deleted_at' => NULL,
                'id' => 164,
                'interaction_type' => 'others',
                'note' => '<p>April 10- Follow up on invoice- awaiting response.</p>

<p>April 13- Payment follow-up-successful</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            162 => 
            array (
                'client_id' => 650,
                'created_at' => '2020-04-14 21:48:02',
                'deleted_at' => NULL,
                'id' => 165,
                'interaction_type' => 'email',
                'note' => '<p>Rescheduled m&amp;g tomorrow</p>

<p>&quot;I am having a reaction to my asthma medication and it hurts to talk right now&quot;</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and greet',
                'status' => 'done',
            ),
            163 => 
            array (
                'client_id' => 591,
                'created_at' => '2020-04-14 21:50:28',
                'deleted_at' => NULL,
                'id' => 166,
                'interaction_type' => 'call',
                'note' => '<p>Discussed about his decision to replace Hanna with VA Aldrin</p>

<p>Hi, we just can&#39;t commit to 20 hours with a new VA. I feel bad about this but is it possible to replace Hanna with Aldrin? She&#39;s been great but we can&#39;t have both and his marketing experience is much more important right now. I could maybe bring her back on down the road if she&#39;s available.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            164 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-04-15 02:13:20',
                'deleted_at' => NULL,
                'id' => 167,
                'interaction_type' => 'email',
                'note' => '<p>He commended Jefte&#39;s improvement for the past 3weeks - VA was able to consistently set appointments/demo calls for them compared from the last 12months</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            165 => 
            array (
                'client_id' => 565,
                'created_at' => '2020-04-15 02:14:52',
                'deleted_at' => NULL,
                'id' => 168,
                'interaction_type' => 'email',
            'note' => '<p>Requested to put the services on hold for Lyka for 1month - he said that the business is greatly affected by the crisis and California is on lockdown until further notice (awaiting response and confirmation on the date of the hold)</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Hold Request',
                'status' => 'acknowleged',
            ),
            166 => 
            array (
                'client_id' => 567,
                'created_at' => '2020-04-15 02:16:30',
                'deleted_at' => NULL,
                'id' => 169,
                'interaction_type' => 'others',
                'note' => '<p>He commended Valerie for being able to set 5 appointments today when she actually just got back from her 3 days leave. He is very happy and thankful</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Zoom Conference',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            167 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-04-15 02:17:17',
                'deleted_at' => NULL,
                'id' => 170,
                'interaction_type' => 'text',
            'note' => '<p>Followed-up on Charlyn Hernando&#39;s start date and schedule (awaiting response)</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            168 => 
            array (
                'client_id' => 650,
                'created_at' => '2020-04-15 17:17:33',
                'deleted_at' => NULL,
                'id' => 171,
                'interaction_type' => 'others',
                'note' => '<p>Reschedule meet and greet today with Janah</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call, text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            169 => 
            array (
                'client_id' => 565,
                'created_at' => '2020-04-16 02:01:09',
                'deleted_at' => NULL,
                'id' => 172,
                'interaction_type' => 'email',
                'note' => '<p>Requested to put the services on hold for a month starting today - he said that California is on lockdown until further notice and all of their transactions are on hold, too but they are sure to come back because they like Lyka. He said that Lyka is very easy to work with.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Hold Request',
                'status' => 'acknowleged',
            ),
            170 => 
            array (
                'client_id' => 387,
                'created_at' => '2020-04-16 02:02:27',
                'deleted_at' => NULL,
                'id' => 173,
                'interaction_type' => 'others',
                'note' => '<p>Disappointed that Dawn couldn&#39;t make it to work today due to power outage - as per Dawn before he shift she already spoke to Hamid via video call and informed him of her situation. She couldn&#39;t work over at another VAs&#39; house that&#39;s also in Pampanga because of curfew. I gave Hamid Dawn&#39;s and her husband&#39;s mobile number because they said they wanted to make an overseas call to her. He also told me to tell Dawn to contact them right away via WhatsApp as soon as she&#39;s in tomorrow. Offered my assistance to any urgent tasks that they have for Dawn for today - I told him I can do it for today in replacement for Dawn.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Call & Text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            171 => 
            array (
                'client_id' => 572,
                'created_at' => '2020-04-16 02:03:27',
                'deleted_at' => NULL,
                'id' => 174,
                'interaction_type' => 'others',
            'note' => '<p>Followed-up on the resumption of services for his VA Christian Angara on April 17th (awaiting confirmation)</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => 'HangOuts',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            172 => 
            array (
                'client_id' => 580,
                'created_at' => '2020-04-16 02:04:47',
                'deleted_at' => NULL,
                'id' => 175,
                'interaction_type' => 'others',
                'note' => '<p>Confirmed that he&#39;s approving Kenah&#39;s 1hr make up shift for today because she was late</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'Hang Outs',
                'others_status' => 'Please specify status type',
                'others_type' => 'MUS Approval',
                'status' => 'done',
            ),
            173 => 
            array (
                'client_id' => 387,
                'created_at' => '2020-04-17 02:18:12',
                'deleted_at' => NULL,
                'id' => 176,
                'interaction_type' => 'call',
                'note' => '<p>Had a conference with Hamid and Dawn - he already accepted Dawn&#39;s resignation. They agreed that the VA will only be working until Wednesday next week, April 22nd to transition everything</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Resignation',
                'status' => 'done',
            ),
            174 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-04-17 02:35:28',
                'deleted_at' => NULL,
                'id' => 177,
                'interaction_type' => 'others',
            'note' => '<p>(New Client) Meet &amp; Greet with VA Kenneth Gomez<br />
-Schedule: 2PM - 6PM EST | Monday - Friday<br />
-Start Date: April 20, 2020<br />
-CRM/Tools: KV Core<br />
-Tasks: Prospecting/Appointment Setting<br />
-Leads to call: expireds/fsbos/buyers<br />
-Mode of communication: WhatsApp/Email<br />
-Goal: 10 appts/week</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet & Greet',
                'status' => 'done',
            ),
            175 => 
            array (
                'client_id' => 572,
                'created_at' => '2020-04-17 02:36:31',
                'deleted_at' => NULL,
                'id' => 178,
                'interaction_type' => 'email',
                'note' => '<p>Requested to extend the hold on the services - he said his business is still on shutdown and he has to save up as much as he can while waiting for the stimulus funds from the Government</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Hold Extension',
                'status' => 'acknowleged',
            ),
            176 => 
            array (
                'client_id' => 468,
                'created_at' => '2020-04-17 20:30:12',
                'deleted_at' => NULL,
                'id' => 179,
                'interaction_type' => 'email',
                'note' => '<p>we are working on overcoming objection and gain&nbsp; more confidence in her skills to overcome these and book appointments. Perhaps more assistance with that. She is dialing great ratios, but we need to help increase skills. I went back to 4 people that told her no and booked 3 out of the 4. Yesterday I saw improvement in appointments set for the day.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            177 => 
            array (
                'client_id' => 546,
                'created_at' => '2020-04-18 02:55:20',
                'deleted_at' => NULL,
                'id' => 180,
                'interaction_type' => 'others',
                'note' => '<p>Touchbase via WhatsApp - said that Kirk is very hard working and dedicted. He&#39;s good with data, design and website platform. He wants him to have training though for outlook and excel.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            178 => 
            array (
                'client_id' => 567,
                'created_at' => '2020-04-18 02:56:44',
                'deleted_at' => NULL,
                'id' => 181,
                'interaction_type' => 'others',
                'note' => '<p>He commended the Valerie for farming leads for him to list. She has been very consistent in setting up appointments for him despite of the pandemic.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Zoom Conference',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            179 => 
            array (
                'client_id' => 580,
                'created_at' => '2020-04-18 02:57:56',
                'deleted_at' => NULL,
                'id' => 182,
                'interaction_type' => 'others',
                'note' => '<p>Approved that Kenah taking the day off today because RexX/Vortex is not working - she will just make up for the hours on Monday</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'HangOuts',
                'others_status' => 'Please specify status type',
                'others_type' => 'MUS approval',
                'status' => 'done',
            ),
            180 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-04-18 02:59:17',
                'deleted_at' => NULL,
                'id' => 183,
                'interaction_type' => 'text',
                'note' => '<p>Already made a decision to have Charlyn start on Wednesday, April 23, 2020 - shift will be 2:30PM - 6:30PM EST</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            181 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-04-21 12:30:39',
                'deleted_at' => NULL,
                'id' => 184,
                'interaction_type' => 'email',
                'note' => '<p>Requested to extend the hold on the services for another week as he has not received the stimulus funds from the Government yet</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Extension',
                'status' => 'done',
            ),
            182 => 
            array (
                'client_id' => 616,
                'created_at' => '2020-04-21 12:32:59',
                'deleted_at' => NULL,
                'id' => 185,
                'interaction_type' => 'others',
            'note' => '<p>Followed up on the resumption of services for Chrislane Adao (no answer yet - email and text sent, awaiting response)</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => 'Call, Email & Text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            183 => 
            array (
                'client_id' => 652,
                'created_at' => '2020-04-21 12:37:24',
                'deleted_at' => NULL,
                'id' => 186,
                'interaction_type' => 'call',
                'note' => '<p>Reached out to her to welcome her to the team and to set proper expectations with her new VA Elvin Britanico</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            184 => 
            array (
                'client_id' => 387,
                'created_at' => '2020-04-21 12:38:50',
                'deleted_at' => NULL,
                'id' => 187,
                'interaction_type' => 'call',
                'note' => '<p>Called today to inform me that she will let me know tomorrow if they will still be needing Dawn&#39;s help for the transition until Wednesday</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            185 => 
            array (
                'client_id' => 616,
                'created_at' => '2020-04-22 01:33:01',
                'deleted_at' => NULL,
                'id' => 188,
                'interaction_type' => 'email',
                'note' => '<p>Requested to extend the hold on the services for another 2 weeks because he is still working on his finances</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Extension',
                'status' => 'acknowleged',
            ),
            186 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-04-22 01:34:23',
                'deleted_at' => NULL,
                'id' => 189,
                'interaction_type' => 'others',
                'note' => '<p>Approved change of schedule for his VA Kenneth Gomez - new schedule is 2:15PM - 6:15PM EST</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Schedule Update',
                'status' => 'done',
            ),
            187 => 
            array (
                'client_id' => 311,
                'created_at' => '2020-04-22 01:35:18',
                'deleted_at' => NULL,
                'id' => 190,
                'interaction_type' => 'text',
            'note' => '<p>Confirmed that Celliza&#39;s last day will be on Friday, April 24th - VA will be paid in full (4hrs)the 24th eventhough she&#39;s only working for 2hrs/day</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            188 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-04-22 01:36:12',
                'deleted_at' => NULL,
                'id' => 191,
                'interaction_type' => 'others',
                'note' => '<p>Ed Young -&nbsp;Informed that he&#39;s having some issues sending out his new campaign this morning to Jefte - VA will be using any smartview that has ZCC in the title for the mean time</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            189 => 
            array (
                'client_id' => 567,
                'created_at' => '2020-04-22 01:37:21',
                'deleted_at' => NULL,
                'id' => 192,
                'interaction_type' => 'others',
                'note' => '<p>Client commended his VA Valerie Regalario for consistently setting up appointments for him - they have converted 2 of her appointments into listing this week</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'HangOuts',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            190 => 
            array (
                'client_id' => 508,
                'created_at' => '2020-04-22 14:17:35',
                'deleted_at' => NULL,
                'id' => 193,
                'interaction_type' => 'email',
                'note' => '<p>Touch base request sent. Client feedback . Norleen is wonderful, as always.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            191 => 
            array (
                'client_id' => 576,
                'created_at' => '2020-04-22 14:18:39',
                'deleted_at' => NULL,
                'id' => 194,
                'interaction_type' => 'text',
                'note' => '<p>check on plans of service resumption. awaiting client response.</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            192 => 
            array (
                'client_id' => 428,
                'created_at' => '2020-04-22 15:05:36',
                'deleted_at' => NULL,
                'id' => 195,
                'interaction_type' => 'others',
                'note' => '<p>Sarah is doing a great job. We have been lucky so far our business is still going fairly strong. Thank you for checking Hope you &amp; yours are safe &amp; well.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => 'whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            193 => 
            array (
                'client_id' => 653,
                'created_at' => '2020-04-22 18:25:04',
                'deleted_at' => NULL,
                'id' => 196,
                'interaction_type' => 'call',
            'note' => '<p>Yvette requested for an additional VA to handle their marketing. Hired Clarisse and start date identified (April 22, 2020)</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'additinal VA',
                'status' => 'done',
            ),
            194 => 
            array (
                'client_id' => 654,
                'created_at' => '2020-04-23 04:13:43',
                'deleted_at' => NULL,
                'id' => 197,
                'interaction_type' => 'call',
                'note' => '<p>Meet and Greet with VA Shekah</p>

<p>- Schedule: Mon-Fri 8:30 - 12:30PM PST&nbsp;</p>

<p>- Start date: Fri April 24th&nbsp;</p>

<p>- CRM Tools - Brivity, Mojo , Landvoice&nbsp;</p>

<p>- System Logins - will be emailed</p>

<p>- Expectations -&nbsp;</p>

<p>&nbsp; Schedule zoom virtual meeting with the lead, phone appt if the lead is unable to do a zoom meeting</p>

<p>24hr notice of appts</p>

<p>she always sent a confirmation email,&nbsp;make sure to confirm the email</p>

<p>Shekah can schedule the appt Mon-Sat between&nbsp;1pm and 5pm PST&nbsp;</p>

<p>&nbsp;</p>

<p>- Leads to call - Expireds, Just solds, and just listed&nbsp;</p>

<p>- Selling points -will include in goal sheet</p>

<p>- Scripts - she fined tuned the script , will be emailed&nbsp;</p>

<p>- Time in between appointments- 30mins</p>

<p>- VM strategy- skip VM</p>

<p>- Calendar sharing- outlook calendar , she will give access to ms 365</p>

<p>- Mode of Communication - text&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and Greet',
                'status' => 'done',
            ),
            195 => 
            array (
                'client_id' => 656,
                'created_at' => '2020-04-23 04:14:59',
                'deleted_at' => NULL,
                'id' => 198,
                'interaction_type' => 'others',
                'note' => '<p>Rescheduled meet and greet tomorrow, 3:30pm MST</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => 'Call and Text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            196 => 
            array (
                'client_id' => 158,
                'created_at' => '2020-04-23 04:17:08',
                'deleted_at' => NULL,
                'id' => 199,
                'interaction_type' => 'others',
                'note' => '<p>VA Services Cancellation</p>

<p>We are cancelling our contract with Virtudesk with the required two weeks&rsquo; notice. Although they are very good options, there are just too many uncertainties that will continue to impact our business once the COVID Shelter in Place is lifted.&quot; Advised Om</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => 'Call and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            197 => 
            array (
                'client_id' => 603,
                'created_at' => '2020-04-23 04:17:56',
                'deleted_at' => NULL,
                'id' => 200,
                'interaction_type' => 'email',
                'note' => '<p>Received TB tasks , forwarded to VA JF</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            198 => 
            array (
                'client_id' => 415,
                'created_at' => '2020-04-23 04:19:32',
                'deleted_at' => NULL,
                'id' => 201,
                'interaction_type' => 'email',
                'note' => '<p>Confirms annual pay raise for VA Lance</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            199 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-04-23 15:50:13',
                'deleted_at' => NULL,
                'id' => 202,
                'interaction_type' => 'others',
                'note' => '<p>4/19 -&nbsp;Confirmation on VA training date on Friday; to start on Monday.<br />
Request for VAs&#39; information for tools and system&#39;s access.<br />
Also requested to receive a new contract agreement with a different signatory.</p>

<p>4/22-&nbsp;Acknowledged training schedule and full time schedules of VA starting next week. Will confirm VA schedules for next week by tomorrow.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            200 => 
            array (
                'client_id' => 343,
                'created_at' => '2020-04-23 15:51:59',
                'deleted_at' => NULL,
                'id' => 203,
                'interaction_type' => 'others',
                'note' => '<p>4/19-Requested on hold extension for another month as they are still waiting for an update regarding filed assistance.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            201 => 
            array (
                'client_id' => 625,
                'created_at' => '2020-04-23 15:58:17',
                'deleted_at' => NULL,
                'id' => 204,
                'interaction_type' => 'email',
                'note' => '<p>4/19-Sent introductory email- requesting for touch base.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            202 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-04-23 16:00:21',
                'deleted_at' => NULL,
                'id' => 205,
                'interaction_type' => 'others',
                'note' => '<p>4/19-&nbsp;First day assistance and monitoring with the additional VAs.<br />
Informed about&nbsp; client referral; client will reach out soon.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'text, email and call',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            203 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-04-23 16:04:38',
                'deleted_at' => NULL,
                'id' => 206,
                'interaction_type' => 'email',
                'note' => '<p>4/22-&nbsp;Acknowledged request to have VA Jesa&#39;s off for today.<br />
Sent a touch base request to check for updates and feedback.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            204 => 
            array (
                'client_id' => 136,
                'created_at' => '2020-04-23 18:48:41',
                'deleted_at' => NULL,
                'id' => 207,
                'interaction_type' => 'text',
                'note' => '<p>Received an update from Dan, LCA&#39;s Marketing Director , that he will send&nbsp;Gab a test created by their current video editor to see what he&#39;s capable of.</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            205 => 
            array (
                'client_id' => 656,
                'created_at' => '2020-04-24 00:04:30',
                'deleted_at' => NULL,
                'id' => 208,
                'interaction_type' => 'call',
                'note' => '<p>Meet and greet with VA Janah</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and greet',
                'status' => 'done',
            ),
            206 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-04-24 02:19:29',
                'deleted_at' => NULL,
                'id' => 209,
                'interaction_type' => 'text',
                'note' => '<p>He wants to send $50 to Kavin as a monetary assistance while he&#39;s on hold. He said that as soon as he receives the funds, he will resume services. I also told him about my conversation with Kavin - VA wants to be endorsed to other clients for PT because he needs to sustain his family&#39;s needs. But I assured him that he will only get a PT post so VA can still accommodate him when he comes back</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            207 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-04-24 02:20:14',
                'deleted_at' => NULL,
                'id' => 210,
                'interaction_type' => 'text',
                'note' => '<p>He said Charlyn had a very strong start - but would want to see her putting more notes on leads account in Follow-Up Boss</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            208 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-04-24 02:21:10',
                'deleted_at' => NULL,
                'id' => 211,
                'interaction_type' => 'others',
                'note' => '<p>Edward Young -&nbsp;Wants Jefte to focus on Base B, Zurich and Quaenet account as the other accounts have been paused and they need to focus on those three.</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            209 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-04-24 02:21:54',
                'deleted_at' => NULL,
                'id' => 212,
                'interaction_type' => 'others',
                'note' => '<p>Provided feedback for VA Kenneth Gomez - he said that Kenneth is very eager to learn and he believes he would be a great asset to his team</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            210 => 
            array (
                'client_id' => 546,
                'created_at' => '2020-04-24 02:22:52',
                'deleted_at' => NULL,
                'id' => 213,
                'interaction_type' => 'others',
                'note' => '<p>Requesting for a full statement of overall billing sent to his email address by tomorrow morning</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Billing Request',
                'status' => 'pending',
            ),
            211 => 
            array (
                'client_id' => 528,
                'created_at' => '2020-04-24 14:45:09',
                'deleted_at' => NULL,
                'id' => 214,
                'interaction_type' => 'email',
                'note' => '<p>She is doing great! she is a hard worker and on top of everything.</p>

<p>&nbsp;</p>

<p>Client needed help in video creation. The biggest thing I need is any support for her in creating videos.&nbsp; She&#39;s doing an excellent job with them.&nbsp; Just saying any training or programs to help her be able to make it look more and more professional is great.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            212 => 
            array (
                'client_id' => 634,
                'created_at' => '2020-04-24 15:01:24',
                'deleted_at' => NULL,
                'id' => 215,
                'interaction_type' => 'call',
                'note' => '<p>Requested for discount / Coordinated reqyest / will renew 20hrs next week. Forecast to place VA on PT aroung First week of July.</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Fees on Renewal of hours',
                'status' => 'done',
            ),
            213 => 
            array (
                'client_id' => 619,
                'created_at' => '2020-04-24 19:43:12',
                'deleted_at' => NULL,
                'id' => 216,
                'interaction_type' => 'call',
                'note' => '<p>April 23 - Service resumption call but client expressed intention to cancel; shared active client&#39;s best practices for VA tasks and possible government subsidies during shelter in place; agreed to 30 days hold extension but will understand if VA is placed with a different client</p>',
                'note_type' => 'followup',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            214 => 
            array (
                'client_id' => 244,
                'created_at' => '2020-04-25 02:35:18',
                'deleted_at' => NULL,
                'id' => 217,
                'interaction_type' => 'others',
                'note' => '<p>Sent Shekah&#39;s profile and followed up CI schedule on Monday</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            215 => 
            array (
                'client_id' => 533,
                'created_at' => '2020-04-25 02:36:30',
                'deleted_at' => NULL,
                'id' => 218,
                'interaction_type' => 'email',
                'note' => '<p>VA Incentive bonus for VA Stephanie, bonus for every 10 appointments booked:</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Incentive',
                'status' => 'done',
            ),
            216 => 
            array (
                'client_id' => 656,
                'created_at' => '2020-04-25 02:37:15',
                'deleted_at' => NULL,
                'id' => 219,
                'interaction_type' => 'email',
                'note' => '<p>Followed up system logins for Janah, starrt date on Monday</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            217 => 
            array (
                'client_id' => 647,
                'created_at' => '2020-04-25 02:39:13',
                'deleted_at' => NULL,
                'id' => 220,
                'interaction_type' => 'others',
                'note' => '<p>Invoice payment follow up , touchbase rescheduled on Monday at 1pm EST</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => 'Call and Text',
                'others_status' => 'Please specify status type',
                'others_type' => 'Invoice Collection',
                'status' => 'done',
            ),
            218 => 
            array (
                'client_id' => 504,
                'created_at' => '2020-04-25 02:40:14',
                'deleted_at' => NULL,
                'id' => 221,
                'interaction_type' => 'others',
                'note' => '<p>Invoice payment follow up , awaiting response</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => 'Call and text',
                'others_status' => 'Please specify status type',
                'others_type' => 'Invoice Collection',
                'status' => 'pending',
            ),
            219 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-04-28 03:16:51',
                'deleted_at' => NULL,
                'id' => 222,
                'interaction_type' => 'text',
                'note' => '<p>Informed that he needs to extend the hold on the services for another week because their finances are not yet stable</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Extension',
                'status' => 'acknowleged',
            ),
            220 => 
            array (
                'client_id' => 580,
                'created_at' => '2020-04-28 03:17:48',
                'deleted_at' => NULL,
                'id' => 223,
                'interaction_type' => 'others',
            'note' => '<p>Tried to touchbase with him about Kenah&#39;s performance - and to talk about his contract since the 90th day is today. Client not answering (awaiting response)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'HangOuts',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            221 => 
            array (
                'client_id' => 534,
                'created_at' => '2020-04-28 03:18:49',
                'deleted_at' => NULL,
                'id' => 224,
                'interaction_type' => 'email',
                'note' => '<p>She said that they were able to get an in-person assistant sooner than they expected so they want to put VA Roselyne Libago back to PT on her original schedule</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
            'others_type' => 'Schedule Change (Reduce Hours)',
                'status' => 'done',
            ),
            222 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-04-28 03:19:48',
                'deleted_at' => NULL,
                'id' => 225,
                'interaction_type' => 'others',
                'note' => '<p>Talked to her today via WhatsApp - she wants to return already by Wednesday. She wants to know though if Charlyn is still available and she wants to know her available schedule. If she doesn&#39;t like the schedule, she will interview a new VA to proceed</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Services',
                'status' => 'pending',
            ),
            223 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-04-28 03:20:48',
                'deleted_at' => NULL,
                'id' => 226,
                'interaction_type' => 'call',
                'note' => '<p>Discussed VA Kenneth Gomez&#39;s screenshot on watching youtube videos and for using an RC account that belonged to the VA&#39;s previous company - apologized to him and assured him that we value VA&#39;s integrity a lot and offered replacement. He will talk to VA Kavin Suan tomorrow at 9AM EST for the replacement</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Replacement Offer',
                'status' => 'acknowleged',
            ),
            224 => 
            array (
                'client_id' => 359,
                'created_at' => '2020-04-28 03:21:42',
                'deleted_at' => NULL,
                'id' => 227,
                'interaction_type' => 'call',
                'note' => '<p>Collection - followed up on his pending invoice of $467.10 - he provided new credit card details but gave strict instructions to run it tomorrow late morning</p>',
                'note_type' => 'collection',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            225 => 
            array (
                'client_id' => 162,
                'created_at' => '2020-04-28 15:48:52',
                'deleted_at' => NULL,
                'id' => 228,
                'interaction_type' => 'text',
                'note' => '<p>Update on invoice, Will settle invoice today with OM Tima</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            226 => 
            array (
                'client_id' => 582,
                'created_at' => '2020-04-28 15:56:24',
                'deleted_at' => NULL,
                'id' => 229,
                'interaction_type' => 'text',
                'note' => '<p>Reminded on her invoice and asked plans on the&nbsp; services that she has. Still waiting for response</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            227 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-04-28 19:24:41',
                'deleted_at' => NULL,
                'id' => 230,
                'interaction_type' => 'others',
                'note' => '<p>April 27-&nbsp;First day of regular schedule with the additional VAs. Checked tasks, accesses and log-ins.<br />
VAs are ready for the calls tomorrow.<br />
Follow-up on payment-waiting for the response from Jared.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            228 => 
            array (
                'client_id' => 536,
                'created_at' => '2020-04-28 19:25:34',
                'deleted_at' => NULL,
                'id' => 231,
                'interaction_type' => 'email',
                'note' => '<p>April 27-<br />
Follow up on payment successful.</p>',
                'note_type' => 'collection',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            229 => 
            array (
                'client_id' => 559,
                'created_at' => '2020-04-28 19:27:06',
                'deleted_at' => NULL,
                'id' => 232,
                'interaction_type' => 'others',
                'note' => '<p>*Touchbase and collection*</p>

<p>April 27-&nbsp;Sent an updated CC information. Payment successful.<br />
Provided positive feedback on Lady&#39;s tasks.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            230 => 
            array (
                'client_id' => 244,
                'created_at' => '2020-04-29 00:22:12',
                'deleted_at' => NULL,
                'id' => 233,
                'interaction_type' => 'call',
                'note' => '<p>CI with VA Shekah Marquez.</p>

<p>Meet and greet Schedule: May 4th&nbsp;</p>

<p>Preferred Schedule: M-F 4pm-6pm or 5-7pm EST</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Client Interview',
                'status' => 'done',
            ),
            231 => 
            array (
                'client_id' => 296,
                'created_at' => '2020-04-29 00:23:20',
                'deleted_at' => NULL,
                'id' => 234,
                'interaction_type' => 'others',
                'note' => '<p>Touchbase request ,reached vm, sent text</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            232 => 
            array (
                'client_id' => 648,
                'created_at' => '2020-04-29 00:25:39',
                'deleted_at' => NULL,
                'id' => 235,
                'interaction_type' => 'others',
                'note' => '<p>He is unavailable to touchbase yet, will get back to me soon with a time</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'call, text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            233 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-04-29 00:27:21',
                'deleted_at' => NULL,
                'id' => 236,
                'interaction_type' => 'call',
                'note' => '<p>Received a call from Adilah, she gave her updated card details. Forwarded to Om and called Jesa to resume today&#39;s shift with Adilah</p>

<p>~Coach Sha</p>',
                'note_type' => 'collection',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            234 => 
            array (
                'client_id' => 642,
                'created_at' => '2020-04-29 00:29:03',
                'deleted_at' => NULL,
                'id' => 237,
                'interaction_type' => 'others',
                'note' => '<p>Touchbase request, reached vm. Received text confirmation , touchbase tomorrow at 1pm EST</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            235 => 
            array (
                'client_id' => 136,
                'created_at' => '2020-04-29 00:30:51',
                'deleted_at' => NULL,
                'id' => 238,
                'interaction_type' => 'text',
                'note' => '<p>Followed up with Dan&nbsp;about Gab&#39;s status</p>

<p>He sent the test yesterday and awaiting progress with Gab.&nbsp;</p>

<p>Advised Gab to update Dan.</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            236 => 
            array (
                'client_id' => 580,
                'created_at' => '2020-04-29 01:20:32',
                'deleted_at' => NULL,
                'id' => 239,
                'interaction_type' => 'others',
                'note' => '<p>Cancellation Request - he said that he&#39;s really in trouble when it comes to his finances since he left KW before the COVID happened. He is now barely surviving and he needs to cut expenses and losses. He promised to come back by October or November though as soon as his finances are already stable.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'HangOuts',
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation',
                'status' => 'done',
            ),
            237 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-04-29 01:21:38',
                'deleted_at' => NULL,
                'id' => 240,
                'interaction_type' => 'others',
            'note' => '<p>Meet &amp; Greet with new VA Kavin Suan - discussed schedule and the tools that they will be using like KVCore and Zillow. VA will also be doing some social media management on Facebook. Also discussed lead generation and the leads he has (expireds, fsbos and buyer leads). Start date is tomorrow, April 29 from 2PM - 6PM EST.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
            'others_type' => 'VA Replacement (M&G)',
                'status' => 'done',
            ),
            238 => 
            array (
                'client_id' => 658,
                'created_at' => '2020-04-29 01:23:07',
                'deleted_at' => NULL,
                'id' => 241,
                'interaction_type' => 'email',
                'note' => '<p>Emailed to request for her meet &amp; greet schedule with VA Charlyn Hernando be moved tomorrow same time, 2PM CST.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G Reschedule',
                'status' => 'acknowleged',
            ),
            239 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-04-29 01:23:57',
                'deleted_at' => NULL,
                'id' => 242,
                'interaction_type' => 'others',
                'note' => '<p>Discussed resumption of services tomorrow - pending VAs&#39; new schedule. She will decide tomorrow whether she likes to have the VA on straight shift or split shift</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Services',
                'status' => 'pending',
            ),
            240 => 
            array (
                'client_id' => 658,
                'created_at' => '2020-04-30 03:00:27',
                'deleted_at' => NULL,
                'id' => 243,
                'interaction_type' => 'email',
                'note' => '<p>Requested to have her meet &amp; greet schedule with Charlyn Hernando be moved on Friday same time, 2PM CST - she is currently caring for her very ill mother that is recovering from COVID19</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G Rescheduled',
                'status' => 'acknowleged',
            ),
            241 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-04-30 03:01:23',
                'deleted_at' => NULL,
                'id' => 244,
                'interaction_type' => 'others',
                'note' => '<p>Resumption of services - she will just wait for Charlyn&#39;s availability on Friday to work on her schedule. Resuming on Monday, May 4th</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Services',
                'status' => 'pending',
            ),
            242 => 
            array (
                'client_id' => 652,
                'created_at' => '2020-04-30 03:02:28',
                'deleted_at' => NULL,
                'id' => 245,
                'interaction_type' => 'text',
            'note' => '<p>Reached out to her today for a quick touchbase - she said that she is on a Zoom Training so she wants me to call her at 5PM CST but no answer. Sent an email to her requesting for Elvin Britanico&#39;s schedule during Thurs and Fridays be moved to 1PM - 5PM CST to accommodate his new client (pending approval)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            243 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-04-30 03:03:37',
                'deleted_at' => NULL,
                'id' => 246,
                'interaction_type' => 'text',
            'note' => '<p>Quick touchbase (about VAs Gracee &amp; Charlyn) - he said everything is doing well. Gracee is a superstar and Charlyn is very persistent. He will have Charlyn start calling his expired leads tomorrow</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            244 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-04-30 03:04:30',
                'deleted_at' => NULL,
                'id' => 247,
                'interaction_type' => 'others',
                'note' => '<p>He said that they need to switch directions again - Jefte will be calling the following accounts moving forward: BaseB 33%, Jedox 33%, Quaenet 33%. He will already stop calling the ZCC leads.</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            245 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-04-30 03:05:24',
                'deleted_at' => NULL,
                'id' => 248,
                'interaction_type' => 'email',
                'note' => '<p>Agreed to have a call tomorrow at 9AM CST to discuss resumption of services by next week</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Services',
                'status' => 'pending',
            ),
            246 => 
            array (
                'client_id' => 440,
                'created_at' => '2020-04-30 16:59:18',
                'deleted_at' => NULL,
                'id' => 249,
                'interaction_type' => 'text',
                'note' => '<p>Checked for service resumption. Requested for another month to hold services.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            247 => 
            array (
                'client_id' => 659,
                'created_at' => '2020-04-30 17:06:35',
                'deleted_at' => NULL,
                'id' => 250,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Meet and greet with elvin / discussed the tools she have and how we could help her eliminate some of the systems she paid and not using /discussed start date and schedule</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and greet',
                'status' => 'done',
            ),
            248 => 
            array (
                'client_id' => 466,
                'created_at' => '2020-04-30 17:07:57',
                'deleted_at' => NULL,
                'id' => 251,
                'interaction_type' => 'text',
                'note' => '<p>Heads up on her VA&#39;s absence due to power outage caused by thunderstorms in theri location.</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            249 => 
            array (
                'client_id' => 501,
                'created_at' => '2020-04-30 22:52:41',
                'deleted_at' => NULL,
                'id' => 252,
                'interaction_type' => 'call',
                'note' => '<p>PT VA that specializes in video editing of our instructional videos. We use Adobe Premiere, but anyone with strong editing experience with other software suites can likely learn Premiere quickly.</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA',
                'status' => 'done',
            ),
            250 => 
            array (
                'client_id' => 158,
                'created_at' => '2020-04-30 22:54:48',
                'deleted_at' => NULL,
                'id' => 253,
                'interaction_type' => 'email',
                'note' => '<p>Followed up if they will push through the cancellation</p>

<p>Zach will speak to Louis next week about the VA services and he is considering the discounted rate offer.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            251 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-05-01 03:47:52',
                'deleted_at' => NULL,
                'id' => 254,
                'interaction_type' => 'call',
            'note' => '<p>Resumption of Services - she said she is ready to come back on Monday, May 4th and have VA Iryn Ocampo back. VA&#39;s original schedule will be retained (8AM - 12PM CST).</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Services',
                'status' => 'done',
            ),
            252 => 
            array (
                'client_id' => 652,
                'created_at' => '2020-05-01 03:48:47',
                'deleted_at' => NULL,
                'id' => 255,
                'interaction_type' => 'call',
            'note' => '<p>Touchbase - she said everything good with Elvin but she wants him to spread his time on other tasks as well and not just focus on her database. She said she will send me the list of tasks that she assigned to Elvin so I can work closely with him on those. She also requested for us to uptrain VAs on some topics to bring him up to speed. We also discussed changing the Elvin&#39;s schedule with her during Thursdays and Fridays (1:15PM - 5:15PM CST starting next week)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            253 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-05-01 03:49:46',
                'deleted_at' => NULL,
                'id' => 256,
                'interaction_type' => 'others',
                'note' => '<p>Resumption of services - confirmed that she will come back on Monday, May 4th and will take whatever schedule Charlyn is available. Final schedule will be given tomorrow</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Services',
                'status' => 'done',
            ),
            254 => 
            array (
                'client_id' => 224,
                'created_at' => '2020-05-01 03:50:43',
                'deleted_at' => NULL,
                'id' => 257,
                'interaction_type' => 'email',
                'note' => '<p>Requested to have the hold on the services be extended for another 2 weeks because they are still on lockdown in PA</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Extension',
                'status' => 'acknowleged',
            ),
            255 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-05-01 03:51:40',
                'deleted_at' => NULL,
                'id' => 258,
                'interaction_type' => 'others',
                'note' => '<p>Discussed options for Kavin&#39;s VOIP - advised that the most cost efficient is skype subscription which only costs him about $10/month</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            256 => 
            array (
                'client_id' => 658,
                'created_at' => '2020-05-02 04:16:12',
                'deleted_at' => NULL,
                'id' => 259,
                'interaction_type' => 'others',
                'note' => '<p>Scheduled to have the Meet &amp; Greet today with VA Charlyn Hernando but client was a no show - sent her an email, called her and sent her a text message but no response</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'Call, Email & Text',
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G No Show',
                'status' => 'pending',
            ),
            257 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-05-02 04:17:21',
                'deleted_at' => NULL,
                'id' => 260,
                'interaction_type' => 'others',
            'note' => '<p>Resuming Services and discussed schedule for VA Charlyn Hernando starting Monday, May 4th - she decided to have the VA on split shift (8AM - 10AM &amp; 12PM - 2PM EST)</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Services',
                'status' => 'done',
            ),
            258 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-05-02 04:21:44',
                'deleted_at' => NULL,
                'id' => 261,
                'interaction_type' => 'others',
            'note' => '<p>Meet &amp; Greet with VAs Villa and Zoe (2 PT ISAs)<br />
- Schedule: 9AM - 1PM EST Villa (Tues- Sat)<br />
2PM - 6PM EST Zoe (sun - Thur)-<br />
Start date: Monday, May 4th<br />
- CRM Tools - MOJO<br />
- System Logins - will be given on Monday<br />
- Expectations - Appointment goal not set yet<br />
- Task/ Job Description - Circle Prospecting<br />
- Scripts - sent via email<br />
- Mode of Communication - Email/HangOuts<br />
- Touch base session - every week<br />
&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet & Greet',
                'status' => 'done',
            ),
            259 => 
            array (
                'client_id' => 427,
                'created_at' => '2020-05-02 04:22:45',
                'deleted_at' => NULL,
                'id' => 262,
                'interaction_type' => 'others',
                'note' => '<p>Tammy Rutledge -She said that as per Kris Cone, they will confirm on Monday if they will be transitioning VA Allen Salindong to Full Time already</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Skype',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            260 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-05-02 04:24:14',
                'deleted_at' => NULL,
                'id' => 263,
                'interaction_type' => 'others',
            'note' => '<p>Requested assistance in setting up a skype calling subscription for his VA Kavin Suan - also commended his VA for being able to set up 3 appointments for him today (2 phone appointments and 1 face-to-face appointment)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            261 => 
            array (
                'client_id' => 666,
                'created_at' => '2020-05-02 05:57:14',
                'deleted_at' => NULL,
                'id' => 264,
                'interaction_type' => 'call',
                'note' => '<p>Meet and Greet with VA Shekah</p>

<p>- Schedule: Mon-Fri 5pm-7pm EST ( will increase the hours on the 2nd week)</p>

<p>- Start date: May 5th</p>

<p>- CRM Tools - FUB , RC</p>

<p>- System Logins - will be emailed</p>

<p>- Expectations -&nbsp;&nbsp;book appointment for his pre constructions business</p>

<p>Schedule a zoom conference call or phone appt if they cant do a conference call</p>

<p>- Leads to call - buyer leads</p>

<p>- Selling points -&nbsp;will send via email</p>

<p>- Scripts -will send all scripts</p>

<p>- Time in between appointments- once Shekah sent the leads he will just take care of the schedule</p>

<p>- VM strategy - leave a vm, he will send the script for the VM</p>

<p>- Calendar sharing - google calendar</p>

<p>- Mode of Communication - Whatsapp</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and greet',
                'status' => 'done',
            ),
            262 => 
            array (
                'client_id' => 501,
                'created_at' => '2020-05-02 05:58:14',
                'deleted_at' => NULL,
                'id' => 265,
                'interaction_type' => 'call',
                'note' => '<p>Facilitated CI and&nbsp;Meet&nbsp;and&nbsp;Greet with GVA Aaron</p>

<p>- Schedule:&nbsp; M-F 12pm-4pm PST&nbsp;</p>

<p>- Start date: may 4, 2020</p>

<p>- CRM Tools - adobe illustrator&nbsp; and adobe premiere, googld docs and dropbox</p>

<p>Tasks-</p>

<p>create brochures and process guides</p>

<p>video editing of instructional videos- primary tasks</p>

<p>David has course guides, VA will take courses</p>

<p>marketing tasks</p>

<p>*he will arrange a Zoom meeting / training with him on Monday, start of shift</p>

<p>- System Logins - will be emailed</p>

<p>- Expectations -</p>

<p>- Tasklist / Daily Agenda&nbsp;</p>

<p>- Mode of Communication - they will set up a slack account&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'CI and meet and greet',
                'status' => 'done',
            ),
            263 => 
            array (
                'client_id' => 648,
                'created_at' => '2020-05-02 05:59:00',
                'deleted_at' => NULL,
                'id' => 266,
                'interaction_type' => 'call',
                'note' => '<p>VA feedback</p>

<p>so far so good , his energy and enthusiasm has been appreciated. He has a learning curve with the platform but it seems like he&#39;s diving in to the information and making progress, looking for him executing at a higher level as each week passes.</p>

<p>Cant think of anything to improve on to impove on right now, he had a good phone productive coversation yesterday with Kenneth, he&#39;s enthusiastic to be helpful and Scott appreciates that. Overall they are pleased with his work.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            264 => 
            array (
                'client_id' => 533,
                'created_at' => '2020-05-02 05:59:40',
                'deleted_at' => NULL,
                'id' => 267,
                'interaction_type' => 'call',
                'note' => '<p>Touchbase , he will join army training for 7 weeks, he is confindent that Stephanie can do the tasks assigned while he is away, discussed endorsements and tasks. VA will follow up and update his leads.Sent google voice and Skype set up instructions.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            265 => 
            array (
                'client_id' => 468,
                'created_at' => '2020-05-04 12:36:01',
                'deleted_at' => NULL,
                'id' => 268,
                'interaction_type' => 'call',
            'note' => '<p>Client expressed sevice continuation however still expecting a much better output from the VA. Requests for best practices from Pamera (Bryan Rowland&#39;s VA)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            266 => 
            array (
                'client_id' => 563,
                'created_at' => '2020-05-04 15:15:07',
                'deleted_at' => NULL,
                'id' => 269,
                'interaction_type' => 'email',
                'note' => '<p>Requested a copy of invoice, she wiill also hire a FT ISA.&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA',
                'status' => 'done',
            ),
            267 => 
            array (
                'client_id' => 665,
                'created_at' => '2020-05-04 18:46:47',
                'deleted_at' => NULL,
                'id' => 270,
                'interaction_type' => 'others',
                'note' => '<p>Followed up meet and greet schedule. Reached VM , sent text, awaiting response</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'call and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            268 => 
            array (
                'client_id' => 653,
                'created_at' => '2020-05-04 19:40:53',
                'deleted_at' => NULL,
                'id' => 271,
                'interaction_type' => 'email',
                'note' => '<p>Thank you for checking in. Nog is wonderful and a joy to work with. She is learning our systems.&nbsp;</p>

<p>&nbsp;</p>

<p>I&#39;m not sure Clarisse is going to work out for us. She is very nice, but I think maybe a little distracted?&nbsp;Projects seem to take some time and not much on the&nbsp;project was changed from what we originally gave her. Since most of what we are giving her is templated and/or she can use her creativity there is not a lot of training, but we do meet with her daily on zoom. I&#39;m just not sure that she is understanding what we are saying. For example: I asked her to create a Mother&#39;s Day post for social media using all of the women in our brokerage. She sent me the attached. I&#39;m not sure if she thought all of the agents wishing people Happy Mother&#39;s Day? I feel like I went over it thoroughly&nbsp;but somewhere we had miscommunication. I can see the confusion but still not sure it&#39;s going to work out.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>I know&nbsp;Linda spoke to her last week because I was out of the office. They were going over something&nbsp;and at the end she asked Linda things she already went over. When Linda asked her did you not hear what I was saying this whole time she told Linda she was distracted.&nbsp;</p>

<p>&nbsp;</p>

<p>I&#39;m not sure if you know but do you have a very seasoned transaction coordinator? We want to see someone&nbsp;who has been a TC for a minimum of 2 to 3 years.&nbsp;</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            269 => 
            array (
                'client_id' => 563,
                'created_at' => '2020-05-05 14:52:12',
                'deleted_at' => NULL,
                'id' => 272,
                'interaction_type' => 'others',
                'note' => '<p>ISA Preferences</p>

<p>Preferences and tasks:<br />
Calling data base, fsbo, expireds must be experienced and speak english well</p>

<p>CRM/Tools<br />
Follow up boss, Outlook, Vulcan 7, Slack</p>

<p>&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call, text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            270 => 
            array (
                'client_id' => 676,
                'created_at' => '2020-05-05 17:23:43',
                'deleted_at' => NULL,
                'id' => 273,
                'interaction_type' => 'call',
                'note' => '<p>Preference: female<br />
Tools: FUB<br />
Leads | Tasks: expired, probate,; database mgt<br />
VoIP: FUB dialer<br />
Shift:8-12 M-F sat&nbsp; sun 10-2pm<br />
CI Schedule - 2 PM Wed</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Preference Call',
                'status' => 'done',
            ),
            271 => 
            array (
                'client_id' => 674,
                'created_at' => '2020-05-05 17:39:35',
                'deleted_at' => NULL,
                'id' => 274,
                'interaction_type' => 'call',
                'note' => '<p>Preference: VA is fun-loving; easy going; engaging; personable; professional<br />
Tools: Spreadsheet<br />
Leads | Tasks: List new properties; narrow down call list from source; sccripts will be provided; calendar blocked schedules for appt; follow up; will leave voicemail message; basic RE<br />
VoIP: Line2<br />
Shift: between 11am -4pm<br />
CI Schedule - May 7, afternoon EST<br />
Others: --leads are business owners and investors</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Preference',
                'status' => 'done',
            ),
            272 => 
            array (
                'client_id' => 660,
                'created_at' => '2020-05-05 18:23:01',
                'deleted_at' => NULL,
                'id' => 275,
                'interaction_type' => 'email',
                'note' => '<p>Hazel is doing great! We are looking for a 2nd VA for property management admin tasks for a 650 unit apartment building. They would be <strong>calling residents who are late on rent</strong>, <strong>handling maintenance requests</strong>, <strong>processing applications</strong>, <strong>processing leases/move-ins/move-outs,&nbsp;</strong>and doing other administrative back-end work.</p>

<p>&nbsp;</p>

<p>Can you send us some resumes with property management experience and a small recording where they do a fake call with a resident who is behind on rent:</p>

<ol style="list-style-type:decimal">
<li>Introduce yourself from the leasing office</li>
<li>Ask how they are doing and be empathetic to their situation</li>
<li>Bring up that they are late on rent payments</li>
<li>Ask when they will be able to pay</li>
<li>Set a follow up date for another call to make sure they are on track to pay</li>
<li>And just be generally friendly and empathetic to the resident who is experiencing hard times, but also be firm and schedule a time for them to pay and for follow up</li>
</ol>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => '2nd VA request',
                'status' => 'ongoing',
            ),
            273 => 
            array (
                'client_id' => 652,
                'created_at' => '2020-05-06 02:43:13',
                'deleted_at' => NULL,
                'id' => 276,
                'interaction_type' => 'call',
                'note' => '<p>Touch base - discussed that Elvin needs help with Content Creation for their Social Media posts every week. She also said that she wants him to manage her calendar moving forward and for him to function more like an EA</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            274 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-05-06 02:44:06',
                'deleted_at' => NULL,
                'id' => 277,
                'interaction_type' => 'call',
                'note' => '<p>Touch base - said that he&#39;s very happy with Gracee and Charlyn. Both of his VAs are superstars and they are creating more work for him than expected. He might even need to hire a Buyer&#39;s agent because of the appointments they are setting.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            275 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-05-06 02:44:55',
                'deleted_at' => NULL,
                'id' => 278,
                'interaction_type' => 'others',
                'note' => '<p>Wants Kavin to try skip tracing using truepeoplesearch.com because Propstream has a few of $0.25/lead search</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            276 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-05-06 02:46:00',
                'deleted_at' => NULL,
                'id' => 279,
                'interaction_type' => 'text',
                'note' => '<p>Before he brings Kavin back, he said he wanted to make sure that he will still have his discounted rate of $9/hr like before</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            277 => 
            array (
                'client_id' => 427,
                'created_at' => '2020-05-06 02:47:04',
                'deleted_at' => NULL,
                'id' => 280,
                'interaction_type' => 'others',
                'note' => '<p>They will be bringing on Allen on Full Time schedule starting tomorrow - new schedule is 8AM - 5PM PST</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'Skype',
                'others_status' => 'Please specify status type',
                'others_type' => 'Transition from PT to FT',
                'status' => 'done',
            ),
            278 => 
            array (
                'client_id' => 662,
                'created_at' => '2020-05-06 05:32:13',
                'deleted_at' => NULL,
                'id' => 281,
                'interaction_type' => 'text',
                'note' => '<p>VA: Jemelyn Padawan<br />
Shift sched: 10AM-2PM PDT<br />
M&amp;G date: May 6, 2 PM PDT<br />
Start date: TBD<br />
Coach: Cheryll Cos</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G endorsement',
                'status' => 'done',
            ),
            279 => 
            array (
                'client_id' => 675,
                'created_at' => '2020-05-06 07:52:23',
                'deleted_at' => NULL,
                'id' => 282,
                'interaction_type' => 'call',
                'note' => '<p>Preferences: - Good communication Skills - Organized, guaranteed appointments - Detail Oriented Tasks: - Setting Up Appointments - Expired, FSBO, Cancelled, Facebook Inquiries. - Follow up calls - Tracking - Seller leads CRM: RedX Vortez, Zillow, realtor.com Preferred Schedule: 8 AM - 10 AM EST and 2 hours in the afternoon</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Client Preference',
                'status' => 'done',
            ),
            280 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-05-07 03:10:34',
                'deleted_at' => NULL,
                'id' => 283,
                'interaction_type' => 'others',
                'note' => '<p>Called in to inform us that she wants her VA Iryn Ocampo to start today at 10AM CST</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Temporary Schedule Change',
                'status' => 'done',
            ),
            281 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-05-07 03:11:42',
                'deleted_at' => NULL,
                'id' => 284,
                'interaction_type' => 'call',
            'note' => '<p>Spoke to him and said that the VAs can start calling out on Monday - system log ins (company email, MOJO &amp; RC access) are given today, scripts and some instructions will be provided tomorrow</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            282 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-05-07 03:13:28',
                'deleted_at' => NULL,
                'id' => 285,
                'interaction_type' => 'text',
                'note' => '<p>Confirmed that he will resume services on the 18th - schedule not finalized yet. He also wants us to schedule Kavin for an SME session for Follow-Up Boss and Ylopo</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Services',
                'status' => 'pending',
            ),
            283 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-05-07 03:14:44',
                'deleted_at' => NULL,
                'id' => 286,
                'interaction_type' => 'email',
                'note' => '<p>Informed that he wants his VA Gracee Galay to be on Full Time starting Monday - schedule not finalized yet. We are still waiting for VAs&#39; other client to approve the change of schedule</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Transition from PT to FT',
                'status' => 'pending',
            ),
            284 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-05-07 03:16:23',
                'deleted_at' => NULL,
                'id' => 287,
                'interaction_type' => 'others',
                'note' => '<p>Informed that her skype calling subscription already expired - she said she will renew that today so Charlyn can start using it again tomorrow</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            285 => 
            array (
                'client_id' => 570,
                'created_at' => '2020-05-07 03:17:18',
                'deleted_at' => NULL,
                'id' => 288,
                'interaction_type' => 'call',
                'note' => '<p>Sent in a new Flyer Project - looking for a VA replacement for Paul tomorrow and have the project turned in on Friday</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Flyer Project',
                'status' => 'pending',
            ),
            286 => 
            array (
                'client_id' => 534,
                'created_at' => '2020-05-07 03:18:30',
                'deleted_at' => NULL,
                'id' => 289,
                'interaction_type' => 'call',
            'note' => '<p>Expressed some concerns on Roselyn&#39;s schedule. She said they need to align her schedule with the in-house admin&#39;s schedule - tentatively, we talked about moving it to 1:30 - 5:30 PM CST (pending John Carey&#39;s schedule change approval). Also sent her credentials in Timedly Portal - she said she can&#39;t log in</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            287 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-05-07 03:19:48',
                'deleted_at' => NULL,
                'id' => 290,
                'interaction_type' => 'others',
                'note' => '<p>He said he will be purchasing a subscription in Propstream for Kavin for 1000 leads to increase productivity</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            288 => 
            array (
                'client_id' => 362,
                'created_at' => '2020-05-08 13:39:48',
                'deleted_at' => NULL,
                'id' => 291,
                'interaction_type' => 'email',
                'note' => '<p>So far all is well and working out perfectly</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            289 => 
            array (
                'client_id' => 307,
                'created_at' => '2020-05-08 18:16:05',
                'deleted_at' => NULL,
                'id' => 292,
                'interaction_type' => 'others',
                'note' => '<p>May 4-<br />
Provided positive feedback on VA Bianca&#39;s performance. &quot;&quot;Bianca is a pro. Very communicative and will take the initiative.&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'call and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            290 => 
            array (
                'client_id' => 559,
                'created_at' => '2020-05-08 18:23:02',
                'deleted_at' => NULL,
                'id' => 293,
                'interaction_type' => 'others',
                'note' => '<p>May 4-&nbsp;Followed-up to check pending tasks on invoicing-all good now.</p>

<p>May 5-Head&#39;s up on new POC. Transitioned all her tasks to TJ Smith.</p>

<p>May 6-&nbsp;Provided positive feedback on VA Lady&#39;s performance. Will continue to directly work with Lady until Hilary comes back from hiatus.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            291 => 
            array (
                'client_id' => 586,
                'created_at' => '2020-05-11 20:16:55',
                'deleted_at' => NULL,
                'id' => 294,
                'interaction_type' => 'call',
                'note' => '<p>Cancellation due to change of business. 14 days notice accepted. may still change mind within the 14 days.</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation',
                'status' => 'acknowleged',
            ),
            292 => 
            array (
                'client_id' => 612,
                'created_at' => '2020-05-11 20:28:21',
                'deleted_at' => NULL,
                'id' => 295,
                'interaction_type' => 'call',
                'note' => '<p>Client is having issues with her financial and low returns. Acknowledge the 14 days for transition</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation',
                'status' => 'acknowleged',
            ),
            293 => 
            array (
                'client_id' => 666,
                'created_at' => '2020-05-12 04:03:57',
                'deleted_at' => NULL,
                'id' => 296,
                'interaction_type' => 'others',
                'note' => '<p>Will give new card. Sent C update form</p>',
                'note_type' => 'collection',
                'noted_by' => 23,
                'others_interaction' => 'Call and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            294 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-05-12 04:04:45',
                'deleted_at' => NULL,
                'id' => 297,
                'interaction_type' => 'others',
                'note' => '<p>Follow up call with Kim about invoice concerns, said that she will call me back, recheduled the call tomorrow.</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call , text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            295 => 
            array (
                'client_id' => 136,
                'created_at' => '2020-05-12 04:05:46',
                'deleted_at' => NULL,
                'id' => 298,
                'interaction_type' => 'others',
                'note' => '<p>Feedback on Gab , he has concerns in his computer system. Scheduled meeting tomorrow with his lead VAs, Sandra and Billy,</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => 'Text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            296 => 
            array (
                'client_id' => 650,
                'created_at' => '2020-05-12 04:06:28',
                'deleted_at' => NULL,
                'id' => 299,
                'interaction_type' => 'text',
                'note' => '<p>She will use a different card for her invoice payment because she&#39;s unable to load tha card on file just yet due to pandemic. Sent CC update card form.</p>',
                'note_type' => 'collection',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            297 => 
            array (
                'client_id' => 656,
                'created_at' => '2020-05-12 04:07:03',
                'deleted_at' => NULL,
                'id' => 300,
                'interaction_type' => 'text',
                'note' => '<p>Invoice collection, she said please try this afternoon. All good</p>',
                'note_type' => 'collection',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            298 => 
            array (
                'client_id' => 647,
                'created_at' => '2020-05-12 04:08:02',
                'deleted_at' => NULL,
                'id' => 301,
                'interaction_type' => 'others',
                'note' => '<p>Reuested account suspension for a month due to pandemic, PA is still heavily affected and would like re evaluate his business. Awaiting response on invoice payment</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => 'Call , email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            299 => 
            array (
                'client_id' => 652,
                'created_at' => '2020-05-12 13:36:05',
                'deleted_at' => NULL,
                'id' => 302,
                'interaction_type' => 'email',
                'note' => '<p>May 11, 2020</p>

<p>Informed her of Elvin Britanico&#39;s absence for today - she just wants to make sure that Elvin will be able to finish his deliverables tomorrow and catch up with their back logs</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            300 => 
            array (
                'client_id' => 651,
                'created_at' => '2020-05-12 13:37:12',
                'deleted_at' => NULL,
                'id' => 303,
                'interaction_type' => 'others',
                'note' => '<p>May 11, 2020</p>

<p>Informed him of Kavin Suan&#39;s absence for today due to sickness - told him that VA notified that he&#39;s sick (90.3 fever with vomitting) - he acknowledged Kavin&#39;s notification and said it is fine as long as he makes up for the hours</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            301 => 
            array (
                'client_id' => 387,
                'created_at' => '2020-05-12 13:38:14',
                'deleted_at' => NULL,
                'id' => 304,
                'interaction_type' => 'text',
                'note' => '<p>May 11, 2020</p>

<p>Informed her that I am still profiling candidates for her based on her qualifications and preferred schedule - profiled 3 candidates for her today: Claire Cruz, Bianca Santos and Lykalyn Espera (Sheila wants Female only). Interview will be tomorrow</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            302 => 
            array (
                'client_id' => 675,
                'created_at' => '2020-05-12 13:39:48',
                'deleted_at' => NULL,
                'id' => 305,
                'interaction_type' => 'others',
                'note' => '<p>May 11, 2020</p>

<p>Facilitated Meet &amp; Greet session today with her new VA Kenah Lequigan:<br />
Start Date: May 13, 2020 (Wednesday)<br />
Schedule: Tuesday - Saturday 8AM - 10AM &amp; 3PM - 5PM EST<br />
CRM/Tools: Zillow,&nbsp;Realtor.com, MLS, Fivestreet, RedX, Ring Central<br />
Scripts: will be provided to the VA (Tom Ferry scripts)<br />
Calendar: Google calendar to be shared to the VA<br />
Leads: Fsbos, Expireds and buyers. Will also be doing Circle Prospecting<br />
Goals: 5 appts/week</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'Ring Central Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet & Greet',
                'status' => 'done',
            ),
            303 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-05-12 13:41:07',
                'deleted_at' => NULL,
                'id' => 306,
                'interaction_type' => 'others',
                'note' => '<p>May 11, 2020</p>

<p>David Rosenthal</p>

<p>He&#39;s Vitaly Vishnepolsky&#39;s new Business Development Manager and he will be our new POC - discussed our processes and accounts handled (Joopy and Queanet).</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Google Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            304 => 
            array (
                'client_id' => 157,
                'created_at' => '2020-05-12 16:23:42',
                'deleted_at' => NULL,
                'id' => 307,
                'interaction_type' => 'others',
                'note' => '<p>Payment follow up successful</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            305 => 
            array (
                'client_id' => 487,
                'created_at' => '2020-05-12 16:25:55',
                'deleted_at' => NULL,
                'id' => 308,
                'interaction_type' => 'others',
                'note' => '<p>Confirmed last week of lessened hours. To start with original hours next Monday.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            306 => 
            array (
                'client_id' => 536,
                'created_at' => '2020-05-12 16:27:36',
                'deleted_at' => NULL,
                'id' => 309,
                'interaction_type' => 'others',
                'note' => '<p>Follow-up on payment. Agreed on terms for this cut-off. Will settle at the end of day.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            307 => 
            array (
                'client_id' => 279,
                'created_at' => '2020-05-12 16:28:52',
                'deleted_at' => NULL,
                'id' => 310,
                'interaction_type' => 'email',
                'note' => '<p>Request for touch base to discuss temporary replacement-no response just yet.</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            308 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-05-12 16:30:01',
                'deleted_at' => NULL,
                'id' => 311,
                'interaction_type' => 'others',
            'note' => '<p>VA Kumi (4th VA)- First day tasks</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            309 => 
            array (
                'client_id' => 666,
                'created_at' => '2020-05-12 16:53:04',
                'deleted_at' => NULL,
                'id' => 312,
                'interaction_type' => 'call',
                'note' => '<p>He will update his credit card details today. Reminded him that Shekah will be on hold today.</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            310 => 
            array (
                'client_id' => 516,
                'created_at' => '2020-05-12 18:12:52',
                'deleted_at' => NULL,
                'id' => 313,
                'interaction_type' => 'text',
                'note' => '<p>Very well. Archie is very reliable and on top of everything. He also takes initiative to recommend or begins things to my attention. Thank you</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            311 => 
            array (
                'client_id' => 586,
                'created_at' => '2020-05-13 13:14:27',
                'deleted_at' => NULL,
                'id' => 314,
                'interaction_type' => 'call',
                'note' => '<p>Jamie express that she may need to cancel as she is changing business perspective. Offered Admin work but to no avail. Honored 14days for VA transition</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Service cancellation',
                'status' => 'acknowleged',
            ),
            312 => 
            array (
                'client_id' => 387,
                'created_at' => '2020-05-13 13:15:47',
                'deleted_at' => NULL,
                'id' => 315,
                'interaction_type' => 'others',
                'note' => '<p>May 12, 2020</p>

<p>Sent her 3 GVA Profiles (Bianca Santos, Lyka Espera &amp; Claire Cruz) - she shortlisted Bianca and Lyka. Interviewed both VAs today and chose Bianca Santos. VA will be starting tomorrow with them and the schedule chosen is, Monday to Friday 8AM - 12PM EST.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'CI',
                'status' => 'done',
            ),
            313 => 
            array (
                'client_id' => 660,
                'created_at' => '2020-05-13 13:15:52',
                'deleted_at' => NULL,
                'id' => 316,
                'interaction_type' => 'call',
                'note' => '<p>Was able to identify his additional VA. Jayson Canlas schedule and tools and work flow discussed.</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA',
                'status' => 'done',
            ),
            314 => 
            array (
                'client_id' => 616,
                'created_at' => '2020-05-13 13:17:25',
                'deleted_at' => NULL,
                'id' => 317,
                'interaction_type' => 'text',
                'note' => '<p>May 12, 2020</p>

<p>Followed up on resumption of services for VA Chrislane Adao - client unresponsive, sent email and text message as well</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            315 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-05-13 13:18:51',
                'deleted_at' => NULL,
                'id' => 318,
                'interaction_type' => 'others',
                'note' => '<p>May 12, 2020</p>

<p>Had a meeting with both of his VAs Zoe &amp; Villa. Discussed their calling list in MOJO and requested to have another meeting tomorrow at 9:30AM EST with me so we can go over the scripts</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => 'Google Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            316 => 
            array (
                'client_id' => 675,
                'created_at' => '2020-05-13 13:20:48',
                'deleted_at' => NULL,
                'id' => 319,
                'interaction_type' => 'email',
                'note' => '<p>May 12, 2020</p>

<p>Followed up on the system log ins for her new VA Kenah Lequigan - sent her Kenah&#39;s email address and whatsapp number as well so they can communicate as soon as she starts tomorrow</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            317 => 
            array (
                'client_id' => 170,
                'created_at' => '2020-05-13 13:22:50',
                'deleted_at' => NULL,
                'id' => 320,
                'interaction_type' => 'call',
                'note' => '<p>Called and offered the Virtudial with a trial discount</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Provided update on the launch of the dialer. Training with her VA would be provided within the week</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            318 => 
            array (
                'client_id' => 680,
                'created_at' => '2020-05-13 13:30:07',
                'deleted_at' => NULL,
                'id' => 321,
                'interaction_type' => 'others',
                'note' => '<p>Meet &amp; Greet today with her new VAs Christopher Dumao and Al Norman Exconde:<br />
Start Date: Al, May 12 &amp; Chris May 13<br />
Schedule: Al - 9AM to 8PM EST (Sunday - Friday)<br />
Chris - 8AM - 7PM EST (Monday - Saturday)<br />
Systems:&nbsp;Outeast.com&nbsp;and Google Voice<br />
Leads will be provided to them on a spreadsheet<br />
<br />
We also attended the training today for her website for Property Management called Outeast.com<br />
**Al was requested to work today up until 10PM EST**</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G',
                'status' => 'done',
            ),
            319 => 
            array (
                'client_id' => 501,
                'created_at' => '2020-05-13 13:31:52',
                'deleted_at' => NULL,
                'id' => 322,
                'interaction_type' => 'others',
                'note' => '<p>May 11 - Client Interview for 2 additional PT VAs - done</p>

<p>- TASK to create teaching guides (PDF) from teaching videos</p>

<p>- chose 1 VA - Jayson</p>

<p>-2nd VA to be decided (subj for review of blogs/ other sample work)</p>

<p>May 12 - Start date and Training for Jayson Javier&nbsp;</p>

<p>- Jayson to be on full time effective on Monday, May 18, 2020</p>

<p>- 2nd VA to be decided on and will start next week as well</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => 'Call/ Skype/ Zoom',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            320 => 
            array (
                'client_id' => 80,
                'created_at' => '2020-05-13 13:36:20',
                'deleted_at' => NULL,
                'id' => 323,
                'interaction_type' => 'call',
                'note' => '<p>May 11, 2020</p>

<p>Inquired on current business status</p>

<p>- pretty good/ slowly trying to get there</p>

<p>will be in touch and provide updates</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            321 => 
            array (
                'client_id' => 582,
                'created_at' => '2020-05-13 13:39:20',
                'deleted_at' => NULL,
                'id' => 324,
                'interaction_type' => 'text',
                'note' => '<p>Collection on unpaid invoice for 2 payperiod. Client unresponsive</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            322 => 
            array (
                'client_id' => 659,
                'created_at' => '2020-05-13 13:41:44',
                'deleted_at' => NULL,
                'id' => 325,
                'interaction_type' => 'email',
                'note' => '<p>Heads up on VA&#39;s absence due to fever and flu. Expected to be back within the week and advised that VA will be doing MUS</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            323 => 
            array (
                'client_id' => 635,
                'created_at' => '2020-05-13 13:45:44',
                'deleted_at' => NULL,
                'id' => 326,
                'interaction_type' => 'text',
                'note' => '<p>May 8, 2020</p>

<p>advd that he has referred 2 REAs from his office who needs VAs but is too busy to provide other info at the monent</p>

<p>May 11, 2020</p>

<p>Ff up on touch base and referral - David said that he&#39;d call me back but wasn&#39;t able to do so as he is too busy</p>

<p>May 12 - ff up on touch base</p>

<p>Calls done - left VM</p>

<p>Replies via email only</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            324 => 
            array (
                'client_id' => 487,
                'created_at' => '2020-05-13 14:18:23',
                'deleted_at' => NULL,
                'id' => 327,
                'interaction_type' => 'others',
                'note' => '<p><br />
Checked current update; will still continue to have decreased hours until Friday. Informed of Katrina&#39;s absence with proposed MUS approved.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            325 => 
            array (
                'client_id' => 536,
                'created_at' => '2020-05-13 14:21:25',
                'deleted_at' => NULL,
                'id' => 328,
                'interaction_type' => 'others',
                'note' => '<p>Settled invoice-VA resumed services.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            326 => 
            array (
                'client_id' => 647,
                'created_at' => '2020-05-13 14:22:19',
                'deleted_at' => NULL,
                'id' => 329,
                'interaction_type' => 'call',
                'note' => '<p>Followed unsettled invoice payment, reached vm, sent text and email</p>',
                'note_type' => 'collection',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            327 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-05-13 14:27:16',
                'deleted_at' => NULL,
                'id' => 330,
                'interaction_type' => 'email',
            'note' => '<p>Updated on current tasks; created a dashboard for all 4 VAs. (Laura Wey)</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            328 => 
            array (
                'client_id' => 543,
                'created_at' => '2020-05-13 14:33:38',
                'deleted_at' => NULL,
                'id' => 331,
                'interaction_type' => 'others',
                'note' => '<p>-end of a month notification-<br />
Last day of services due to financial difficulties brought about by COVID situation. Will be reaching out last quarter to continue with VA services.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'text, call and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            329 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-05-13 14:34:55',
                'deleted_at' => NULL,
                'id' => 332,
                'interaction_type' => 'email',
                'note' => '<p>Informed of client referral bonus for referring Adam Mahfouda.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            330 => 
            array (
                'client_id' => 593,
                'created_at' => '2020-05-13 14:36:17',
                'deleted_at' => NULL,
                'id' => 333,
                'interaction_type' => 'others',
                'note' => '<p>Followed up resumption of VA services. Client is still unreachable</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            331 => 
            array (
                'client_id' => 631,
                'created_at' => '2020-05-13 14:39:11',
                'deleted_at' => NULL,
                'id' => 334,
                'interaction_type' => 'email',
                'note' => '<p>Adivsed that TB tasks is completed, asked if the real estate animated ad is approved. Awaiting response</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            332 => 
            array (
                'client_id' => 632,
                'created_at' => '2020-05-13 14:45:49',
                'deleted_at' => NULL,
                'id' => 335,
                'interaction_type' => 'call',
                'note' => '<p>May 11, 2020</p>

<p>Jeff inquired on current charges</p>

<p>- Explained that total amount includes taxes</p>

<p>- Client understood</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Bill Query',
                'status' => 'done',
            ),
            333 => 
            array (
                'client_id' => 669,
                'created_at' => '2020-05-13 14:51:15',
                'deleted_at' => NULL,
                'id' => 336,
                'interaction_type' => 'text',
                'note' => '<p>VA Start Date on May 7, 2020</p>

<p>May 11, 2020</p>

<p>Feedback: &quot;She&rsquo;s been great&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            334 => 
            array (
                'client_id' => 679,
                'created_at' => '2020-05-14 02:50:07',
                'deleted_at' => NULL,
                'id' => 337,
                'interaction_type' => 'email',
                'note' => '<p>Client: Gusty Gulas<br />
VA: Kenah Lequigan<br />
Shift sched: &nbsp;anytime after 2:30 CST<br />
M&amp;G date: May 15, 10 AM CST<br />
Start date: TBD<br />
Coach: Cheryll Cos</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G endorsement',
                'status' => 'done',
            ),
            335 => 
            array (
                'client_id' => 679,
                'created_at' => '2020-05-14 02:50:07',
                'deleted_at' => NULL,
                'id' => 338,
                'interaction_type' => 'email',
                'note' => '<p>Client: Gusty Gulas<br />
VA: Kenah Lequigan<br />
Shift sched: &nbsp;anytime after 2:30 CST<br />
M&amp;G date: May 15, 10 AM CST<br />
Start date: TBD<br />
Coach: Cheryll Cos</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G endorsement',
                'status' => 'done',
            ),
            336 => 
            array (
                'client_id' => 688,
                'created_at' => '2020-05-14 05:18:42',
                'deleted_at' => NULL,
                'id' => 339,
                'interaction_type' => 'others',
                'note' => '<p>CI Schedule: May 15, 9 PM MNL</p>

<p>Client: Kevin &nbsp;Piechota<br />
Timezone: CA - PST<br />
PT GVA</p>

<p>Preferences and Tasks:<br />
- Marketing and admin support for 2 different types of business<br />
A. Real Estate Marketing admin<br />
- Email marketing management<br />
- CRM management for Salesforce<br />
- website management<br />
- Graphic design<br />
B. Whissel Realty admin<br />
- Support on admin task for Boomtown, Follow Up Boss, Ylopo<br />
- Data management</p>

<p>CRM and Tools: homes.com, Salesforce, BoomTown, Google slides; Follow Up Boss, Ylopo; https://www.whisselrealty.com/agents/201206-Kevin-Piechota/</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => 'CI Alert',
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Preference',
                'status' => 'open',
            ),
            337 => 
            array (
                'client_id' => 683,
                'created_at' => '2020-05-14 07:35:54',
                'deleted_at' => NULL,
                'id' => 341,
                'interaction_type' => 'email',
                'note' => '<p>Client: Stanley Bresca<br />
VA: Irish Abao<br />
Shift sched: &nbsp;TBD<br />
M&amp;G date: May 14, 4PM EST<br />
Start date: TBD<br />
Coach: Cheryll Cos</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G Endorsement',
                'status' => 'done',
            ),
            338 => 
            array (
                'client_id' => 675,
                'created_at' => '2020-05-14 14:18:24',
                'deleted_at' => NULL,
                'id' => 342,
                'interaction_type' => 'others',
                'note' => '<p>May 13, 2020</p>

<p>Followed up on Kenah&#39;s system log ins. Informed her that the Zillow and Vortex log ins she provided were not working. We reminded her of the RC access for the VA as well</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            339 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-05-14 14:19:23',
                'deleted_at' => NULL,
                'id' => 343,
                'interaction_type' => 'others',
                'note' => '<p>Conference/Meeting via Google Meetings today - he addressed questions of the VAs about their calling list in MOJO and went over the scripts that they will be using. Another meeting scheduled tomorrow at 9:30AM EST</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => 'Google Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            340 => 
            array (
                'client_id' => 680,
                'created_at' => '2020-05-14 14:20:57',
                'deleted_at' => NULL,
                'id' => 344,
                'interaction_type' => 'others',
                'note' => '<p>May 13, 2020</p>

<p>Conference/Meeting via RC Meetings today - gave directives about some property rentals that needs to be prioritized today by Chris and Al</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            341 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-05-14 14:21:59',
                'deleted_at' => NULL,
                'id' => 345,
                'interaction_type' => 'text',
                'note' => '<p>May 13, 2020</p>

<p>Requested to adjust Gracee&#39;s schedule a bit because it&#39;s overlapping Charlyn&#39;s schedule and they can&#39;t use Follow-Up Boss at the same time</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Schedule Change',
                'status' => 'done',
            ),
            342 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-05-14 14:22:55',
                'deleted_at' => NULL,
                'id' => 346,
                'interaction_type' => 'text',
                'note' => '<p>May 13, 2020</p>

<p>Followed up on the resumption of services - confirmed that Kavin will be back working for him on Monday, May 18th. We will work on Kavin&#39;s schedule tomorrow since he&#39;s already hired by another PT client from 2PM - 6PM EST</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            343 => 
            array (
                'client_id' => 543,
                'created_at' => '2020-05-14 14:48:55',
                'deleted_at' => NULL,
                'id' => 347,
                'interaction_type' => 'email',
                'note' => '<p><br />
Confirmation of VA Chris&#39; last day yesterday. Sent an acknowledgement letter.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            344 => 
            array (
                'client_id' => 487,
                'created_at' => '2020-05-14 14:51:47',
                'deleted_at' => NULL,
                'id' => 348,
                'interaction_type' => 'email',
                'note' => '<p>Sent new leads to call for VA Katrina.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            345 => 
            array (
                'client_id' => 549,
                'created_at' => '2020-05-14 14:55:20',
                'deleted_at' => NULL,
                'id' => 349,
                'interaction_type' => 'others',
                'note' => '<p>Provided positive feedback on Sam&#39;s tasks. Assured that Sam will continue working for him and that business is sustaining. Currently concentrating on doing marketing tasks.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            346 => 
            array (
                'client_id' => 307,
                'created_at' => '2020-05-14 14:56:32',
                'deleted_at' => NULL,
                'id' => 350,
                'interaction_type' => 'others',
                'note' => '<p>Notification on putting services on hold because of financial difficulties due to Covid19.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            347 => 
            array (
                'client_id' => 279,
                'created_at' => '2020-05-14 14:58:34',
                'deleted_at' => NULL,
                'id' => 351,
                'interaction_type' => 'others',
                'note' => '<p>Follow-up on rescheduled triad call with VA Rhony.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            348 => 
            array (
                'client_id' => 688,
                'created_at' => '2020-05-14 15:52:24',
                'deleted_at' => NULL,
                'id' => 352,
                'interaction_type' => 'call',
                'note' => '<p>Called to check details needed for his future VA . endorsed to Lalla for candidate&nbsp;profiles&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => 'CI Alert',
                'others_status' => 'Please specify status type',
                'others_type' => 'Preference Call',
                'status' => 'ongoing',
            ),
            349 => 
            array (
                'client_id' => 636,
                'created_at' => '2020-05-14 16:17:07',
                'deleted_at' => NULL,
                'id' => 353,
                'interaction_type' => 'email',
                'note' => '<p>sent touchbase request.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            350 => 
            array (
                'client_id' => 633,
                'created_at' => '2020-05-14 16:37:25',
                'deleted_at' => NULL,
                'id' => 354,
                'interaction_type' => 'text',
                'note' => '<p>Touch base iwth July&#39;s performance and everything is good so far.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            351 => 
            array (
                'client_id' => 684,
                'created_at' => '2020-05-14 18:28:08',
                'deleted_at' => NULL,
                'id' => 355,
                'interaction_type' => 'call',
                'note' => '<p>Meet&nbsp;and&nbsp;Greet with VA Richelle</p>

<p>- Schedule: Mon - Fri 1pm-5pm PST</p>

<p>- Start date: May 18th</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and greet',
                'status' => 'done',
            ),
            352 => 
            array (
                'client_id' => 589,
                'created_at' => '2020-05-14 19:05:53',
                'deleted_at' => NULL,
                'id' => 356,
                'interaction_type' => 'call',
                'note' => '<p>May 13, 2020</p>

<p>Feedback for &nbsp;VA</p>

<p>&quot;doing fine/ happy with her.&quot;</p>

<ul>
<li style="list-style-type:disc">Evolve her to next stage</li>
<li style="list-style-type:disc">Will assign her to new tasks/ other accounts (Commercial/ etc)</li>
</ul>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            353 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-05-15 14:44:07',
                'deleted_at' => NULL,
                'id' => 357,
                'interaction_type' => 'others',
                'note' => '<p>May 14, 2020</p>

<p>Pushed back Villa &amp; Zoe&#39;s start date tomorrow due to technical issues and calling list - offered help to call Mojo and asked for assistance but he refused. Offered as well to have the VAs do manual dialing since their Google Voice is already set up but he also refused and said he needs one more day to set everything up. Advised him that this is already the 2nd week that he pushed the start date back - if VAs will not start this week, we will endorse them to other clients by next week and he might need to interview new VAs</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => 'Google Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            354 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-05-15 14:45:22',
                'deleted_at' => NULL,
                'id' => 358,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>May 14, 2020</p>

<p>She agreed to have VA Charlyn Hernando do an MUS tomorrow for 2hrs to make up for the lost hours today - we also discussed that she&#39;s struggling with her leads right now because it has dried up. I offered to train Charlyn on how to farm expired leads in MLS and FSBO leads in Zillow. She provided feedback on Charlyn&#39;s EOD and said that she liked the kind of leads that we mined today and she wants to nurture them.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            355 => 
            array (
                'client_id' => 675,
                'created_at' => '2020-05-15 14:46:44',
                'deleted_at' => NULL,
                'id' => 359,
                'interaction_type' => 'others',
                'note' => '<p>May 14, 2020</p>

<p>She agreed to having Kenah do manual dialing for the mean time since she already has her calling leads and RC account while she&#39;s updating her subscription for RedX/Vortex because it was not equipped with Storm Dialer. VA was able to set 1 appointment for her today on her first day of dialing. She&#39;s very happy.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            356 => 
            array (
                'client_id' => 387,
                'created_at' => '2020-05-15 14:47:54',
                'deleted_at' => NULL,
                'id' => 360,
                'interaction_type' => 'call',
                'note' => '<p>May 14, 2020</p>

<p>&nbsp;</p>

<p>Spoke to Hamid&nbsp;about their new VA Bianca Santos - informed him that VA is okay with doing Zoom screenshare the whole shift but not with having her video cam on the whole time while he&#39;s watching her. He got mad and said that he requires it to prove to their agents that she&#39;s a real person. Informed him that we have timedly so they can track the VAs productivity but he still refused. I informed him I will talk to Sheila tomorrow.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            357 => 
            array (
                'client_id' => 224,
                'created_at' => '2020-05-15 14:49:15',
                'deleted_at' => NULL,
                'id' => 361,
                'interaction_type' => 'email',
                'note' => '<p>May 14, 2020</p>

<p>Sent an email to inform us that he wants to extend the hold on the services until the 1st week of June because they are still not allowed to work in Pennsylvania</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            358 => 
            array (
                'client_id' => 688,
                'created_at' => '2020-05-15 15:09:47',
                'deleted_at' => NULL,
                'id' => 362,
                'interaction_type' => 'email',
                'note' => '<p>Client: Kevin &nbsp;Piechota<br />
VA: Louis Magno<br />
Shift sched: TBD<br />
M&amp;G date: May 16, 8 AM PST<br />
Start date: May 18, Monday<br />
Coach: Sharon Fabian</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G endorsement',
                'status' => 'done',
            ),
            359 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-05-15 16:41:59',
                'deleted_at' => NULL,
                'id' => 363,
                'interaction_type' => 'others',
                'note' => '<p>Laura Wey-&nbsp;Notification on VA Jester&#39;s last day today as they need to downsize. Informed that Jester will still be paid until the 31st.<br />
New process discussed.</p>

<p>Jared Berman-Reiterated on the need to make sure that only valid appointments are set to be assigned to the agents. Assured that will continue to coordinate and monitor the team.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            360 => 
            array (
                'client_id' => 487,
                'created_at' => '2020-05-15 16:43:44',
                'deleted_at' => NULL,
                'id' => 364,
                'interaction_type' => 'email',
                'note' => '<p>Memorial Day VA attendance responses for Lady and Katrina</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            361 => 
            array (
                'client_id' => 184,
                'created_at' => '2020-05-15 16:47:43',
                'deleted_at' => NULL,
                'id' => 365,
                'interaction_type' => 'email',
                'note' => '<p><br />
Provided consistent positive feedback on Lady&#39;s tasks</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            362 => 
            array (
                'client_id' => 677,
                'created_at' => '2020-05-16 09:26:09',
                'deleted_at' => NULL,
                'id' => 366,
                'interaction_type' => 'email',
                'note' => '<p>VA: Cielo Macalalad<br />
Shift sched: 2-6 PM EST<br />
M&amp;G date: May 18, 10 AM EST<br />
Start date: May 18, Monday<br />
Coach: Cheryll Cos</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G endorsement',
                'status' => 'done',
            ),
            363 => 
            array (
                'client_id' => 677,
                'created_at' => '2020-05-16 09:26:09',
                'deleted_at' => NULL,
                'id' => 367,
                'interaction_type' => 'email',
                'note' => '<p>VA: Cielo Macalalad<br />
Shift sched: 2-6 PM EST<br />
M&amp;G date: May 18, 10 AM EST<br />
Start date: May 18, Monday<br />
Coach: Cheryll Cos</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G endorsement',
                'status' => 'done',
            ),
            364 => 
            array (
                'client_id' => 660,
                'created_at' => '2020-05-18 17:34:11',
                'deleted_at' => NULL,
                'id' => 368,
                'interaction_type' => 'email',
                'note' => '<p>Touch base on Hazel&#39;s performance&nbsp;</p>

<p>She&rsquo;s doing great thanks!</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            365 => 
            array (
                'client_id' => 666,
                'created_at' => '2020-05-18 23:34:44',
                'deleted_at' => NULL,
                'id' => 369,
                'interaction_type' => 'others',
                'note' => '<p>Sunil will add hours for Shekah, from 10-20hrs effective tomorrow.&nbsp;</p>

<p>Updated schedule:&nbsp;Tues - Fri 4pm-8pm&nbsp; and Sat 12pm-4pm EST</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => 'Call , text',
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional Hours',
                'status' => 'done',
            ),
            366 => 
            array (
                'client_id' => 114,
                'created_at' => '2020-05-18 23:44:26',
                'deleted_at' => NULL,
                'id' => 370,
                'interaction_type' => 'others',
                'note' => '<p>Touchbase request, left VM, sent sms awaiting reponse</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            367 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-05-19 15:07:22',
                'deleted_at' => NULL,
                'id' => 371,
                'interaction_type' => 'others',
                'note' => '<p>DJ Paris-&nbsp;<br />
Informed of Kumi&#39;s issue on not being able to dial. Provided SIP settings to provide support-issue resolved.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            368 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-05-19 15:08:28',
                'deleted_at' => NULL,
                'id' => 372,
                'interaction_type' => 'others',
                'note' => '<p>Laura Wey-Coordinated on new leads to contact per VA. Reminded of the directives discussed during their meeting.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            369 => 
            array (
                'client_id' => 487,
                'created_at' => '2020-05-19 15:12:03',
                'deleted_at' => NULL,
                'id' => 373,
                'interaction_type' => 'email',
                'note' => '<p>Confirmed 4 hours of tasks for both VAs.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            370 => 
            array (
                'client_id' => 559,
                'created_at' => '2020-05-19 15:17:18',
                'deleted_at' => NULL,
                'id' => 374,
                'interaction_type' => 'others',
                'note' => '<p>TJ Smith-Provided positive feedback on Lady&#39;s performance on tasks. Says that she is very independent and proactive.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            371 => 
            array (
                'client_id' => 274,
                'created_at' => '2020-05-19 15:28:17',
                'deleted_at' => NULL,
                'id' => 375,
                'interaction_type' => 'others',
                'note' => '<p>Followed-up on start date for TBS.-awaiting response</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            372 => 
            array (
                'client_id' => 659,
                'created_at' => '2020-05-19 20:44:46',
                'deleted_at' => NULL,
                'id' => 376,
                'interaction_type' => 'call',
                'note' => '<p>Called Tatiana and clarified&nbsp; Elvin&#39;s rate&nbsp; and discussed the tools to&nbsp;organize and provided feedback with how great Elvin is.</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Rate Clarification',
                'status' => 'acknowleged',
            ),
            373 => 
            array (
                'client_id' => 387,
                'created_at' => '2020-05-19 21:41:10',
                'deleted_at' => NULL,
                'id' => 377,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>May 18, 2020</p>

<p>Discussed that we cannot require Bianca to have her video cam turned on the whole shift. Explained to her that it might affect her internet speed and in a way, we are violating the VA&#39;s privacy. Sheila disagreed and said that today can be the VA&#39;s last day. She asked Bianca to log out early but promised she will pay her in full today</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation',
                'status' => 'done',
            ),
            374 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-05-19 21:42:45',
                'deleted_at' => NULL,
                'id' => 378,
                'interaction_type' => 'email',
                'note' => '<p>May 18, 2020</p>

<p>Requesting for additional ISA for FT - he wants someone experienced and with good communication skills (sent Jester Muldong and Jazel Lolos&#39; profile). I promised that I will send another one tomorrow, I&#39;ll just need to confirm with the VA (Laurice)</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Add On ISA',
                'status' => 'pending',
            ),
            375 => 
            array (
                'client_id' => 614,
                'created_at' => '2020-05-19 21:43:53',
                'deleted_at' => NULL,
                'id' => 379,
                'interaction_type' => 'others',
                'note' => '<p>May 18, 2020</p>

<p>&nbsp;</p>

<p>Discussed resumption of services. She said that she safely delivered her baby boy last May 4th and she started working a bit again - she wants to know how she can start with a VA again soon. Informed her that VA Gracee already has a client now for FT so she might need to interview VAs for her again. Profiling VAs already</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Services',
                'status' => 'pending',
            ),
            376 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-05-19 21:45:02',
                'deleted_at' => NULL,
                'id' => 380,
                'interaction_type' => 'others',
                'note' => '<p>May 18, 2020</p>

<p>Approved Charlyn&#39;s absence for today due to sickness contingent upon the VA would render an MUS. She is also requesting to confirm when is her contract ending</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            377 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-05-19 21:46:24',
                'deleted_at' => NULL,
                'id' => 381,
                'interaction_type' => 'text',
                'note' => '<p>May 18, 2020</p>

<p>Approved Charlyn&#39;s absence for today due to sickness. He also said that he wants Charlyn to take the day off on Memorial day and it will be paid.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            378 => 
            array (
                'client_id' => 690,
                'created_at' => '2020-05-20 16:12:48',
                'deleted_at' => NULL,
                'id' => 382,
                'interaction_type' => 'others',
                'note' => '<p>May 19, 2020</p>

<p>Meet &amp; Greet today with his new VA Charmaen Espina:<br />
Schedule:&nbsp;3PM - 7PM EST (Tuesday - Saturday)<br />
Start Date:&nbsp;Thursday<br />
CRM/Tools:&nbsp;RedX/Vortex, Follow Up Boss, KV Core<br />
System Log ins:&nbsp;already sent via WhatsApp<br />
Goals:&nbsp;3-5 appointments per week<br />
Leads/Tasks:&nbsp;FSBOs, Circle Prospecting<br />
Mode of Comms:&nbsp;WhatsApp</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet & Greet',
                'status' => 'done',
            ),
            379 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-05-20 16:13:46',
                'deleted_at' => NULL,
                'id' => 383,
                'interaction_type' => 'others',
                'note' => '<p>May 19, 2020</p>

<p>&nbsp;</p>

<p>Conference call with the VAs via Google Meetings - he gave brief instructions to both Zoe and Villa to memorize the things their company offers. He also said that this week &amp; next week they will have very limited leads so they should enjoy a loght workload for the mean time because once they are done scrubbing 30,000 leads, they will be very busy</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Google Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            380 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-05-20 16:14:45',
                'deleted_at' => NULL,
                'id' => 384,
                'interaction_type' => 'email',
                'note' => '<p>May 19, 2020</p>

<p>Additional FT ISA - Sent him profiles and updated CVs for Jester Muldong, Jazel Lolos and JC Reyes - tentative CI schedule will be tomorrow, no definite time yet from Robert. He is also open to have 2 PT ISAs instead of 1 FT</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            381 => 
            array (
                'client_id' => 613,
                'created_at' => '2020-05-20 16:16:14',
                'deleted_at' => NULL,
                'id' => 385,
                'interaction_type' => 'email',
                'note' => '<p>May 19, 2020</p>

<p>&nbsp;</p>

<p>She wants to know how many days left there is for her contract - explained to her that she signed 90 days contract. She was only able to use 43 days from it when she requested to put the services on hold last March 14th. She resumed May 4th with Charlyn so the contract should be until June 19th since we did not include the days that she&#39;s on hold</p>',
                'note_type' => 'consultation',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            382 => 
            array (
                'client_id' => 675,
                'created_at' => '2020-05-20 16:17:18',
                'deleted_at' => NULL,
                'id' => 386,
                'interaction_type' => 'email',
                'note' => '<p>May 19, 2020</p>

<p>She just wants to make sure that her VA Kenah Lequigan is only working for 20hrs per week - I assured her that Kenah is working on the agreed hours (20hrs/week, 4hrs/day). She might have been confused because her VA is working on a split shift schedule (2hrs in the morning and 2hrs in the afternoon).</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            383 => 
            array (
                'client_id' => 565,
                'created_at' => '2020-05-20 16:20:00',
                'deleted_at' => NULL,
                'id' => 387,
                'interaction_type' => 'email',
                'note' => '<p>May 19, 2020</p>

<p>Requested to extend the hold on the services until the 1st week of June - he said he wants to see how things are going to play for his business in California</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'On-Hold Extension',
                'status' => 'done',
            ),
            384 => 
            array (
                'client_id' => 563,
                'created_at' => '2020-05-20 17:32:25',
                'deleted_at' => NULL,
                'id' => 388,
                'interaction_type' => 'others',
                'note' => '<p>Sent Jester&#39;s introduction video, Awaiting CI schedule confirmation.&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Text and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            385 => 
            array (
                'client_id' => 653,
                'created_at' => '2020-05-20 20:39:47',
                'deleted_at' => NULL,
                'id' => 389,
                'interaction_type' => 'email',
                'note' => '<p>Heads up on Clarisse&#39;s absence and plans of cancelling Clarisse to add hours for Norleen</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            386 => 
            array (
                'client_id' => 681,
                'created_at' => '2020-05-21 08:26:41',
                'deleted_at' => NULL,
                'id' => 390,
                'interaction_type' => 'email',
                'note' => '<p>VA: Adrian Concepcion<br />
Shift sched: 3-7 PM EST<br />
M&amp;G date: May 22, 11 AM EST<br />
Start date: TBD<br />
Coach:Cheryll Cos</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G endorsement',
                'status' => 'done',
            ),
            387 => 
            array (
                'client_id' => 653,
                'created_at' => '2020-05-21 14:07:30',
                'deleted_at' => NULL,
                'id' => 391,
                'interaction_type' => 'text',
                'note' => '<p>Confirmed cancellation for Clarisse and may add hours instead to Norleen.</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            388 => 
            array (
                'client_id' => 688,
                'created_at' => '2020-05-21 14:15:58',
                'deleted_at' => NULL,
                'id' => 392,
                'interaction_type' => 'call',
                'note' => '<p>Talked to Kevin about the delay on start date due to lack of San Diego VPN. VA already subscribed to Frost and waiting for ticket to be resolved</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            389 => 
            array (
                'client_id' => 497,
                'created_at' => '2020-05-21 22:53:42',
                'deleted_at' => NULL,
                'id' => 393,
                'interaction_type' => 'call',
                'note' => '<ul>
<li style="list-style-type:disc">Wants to get a VA who can do soc media marketing</li>
<li style="list-style-type:disc">FB/ IG - YT channel as well. - content and stuff that can be run with.</li>
<li style="list-style-type:disc">Tues/ Wed for - CI</li>
<li style="list-style-type:disc">Tom to send at least 5 CV of candidates with sample work.</li>
<li style="list-style-type:disc">For resumption of svc on June 1 from ISA to GVA</li>
<li style="list-style-type:disc">still thinking if he would still get an ISA to make calls and set appts for him (for future discussion)</li>
</ul>

<p style="list-style-type:disc">===============</p>

<p style="list-style-type:disc">May 25/ 26 - CVs and sample work sent to Shane</p>

<p style="list-style-type:disc">May 27 10PM CST - CI interview</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            390 => 
            array (
                'client_id' => 686,
                'created_at' => '2020-05-21 22:56:25',
                'deleted_at' => NULL,
                'id' => 394,
                'interaction_type' => 'text',
            'note' => '<p>Advd Toni of Jim&#39;s absence (2nd day)</p>

<p>Jim&#39;s still having internet issues at the moment. Both his main and back up svc providers aren&#39;t working. He is not able to report today as well. He&#39;s been trying his best and he even attempted to go to another location, however - he wasn&#39;t allowed to enter the subdivision he was going to at the moment due to the lockdown/Quarantine guidelines of their town/barangay. Apologies about this. He will make up for the lost time. -&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            391 => 
            array (
                'client_id' => 501,
                'created_at' => '2020-05-21 23:01:00',
                'deleted_at' => NULL,
                'id' => 395,
                'interaction_type' => 'text',
                'note' => '<p>confirmed add&rsquo;l 8 hrs for Claire - 4 hrs for SAt/ 4 hrs for Sunday</p>

<ul>
<li style="list-style-type:disc">Affirming if he give authorization for Lauren to be his POC&nbsp;</li>
<li style="list-style-type:disc">Validating Malaya&rsquo;s start date</li>
<li style="list-style-type:disc">(Malaya is okay to have the training on May 25 at 3pm MST, AZ, since it&#39;ll just be for 30-45 mins. Then she can start on May 26 with the schedule of 12am-4am MST, AZ)</li>
</ul>

<p style="list-style-type:disc">Comms - email with Lauren (David&#39;s wife) and Skype chat with David Dodge</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            392 => 
            array (
                'client_id' => 687,
                'created_at' => '2020-05-22 09:17:38',
                'deleted_at' => NULL,
                'id' => 396,
                'interaction_type' => 'others',
                'note' => '<p>VA: Jaime De Guzman<br />
Shift sched: Tue-Fri 5 hrs/day<br />
M&amp;G date: May 15, 1 PM EST<br />
Start date: TBD<br />
Booked: May 14<br />
Coach: Cheryll Cos</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G endorsement',
                'status' => 'done',
            ),
            393 => 
            array (
                'client_id' => 687,
                'created_at' => '2020-05-22 09:17:38',
                'deleted_at' => NULL,
                'id' => 397,
                'interaction_type' => 'others',
                'note' => '<p>VA: Jaime De Guzman<br />
Shift sched: Tue-Fri 5 hrs/day<br />
M&amp;G date: May 15, 1 PM EST<br />
Start date: TBD<br />
Booked: May 14<br />
Coach: Cheryll Cos</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'M&G endorsement',
                'status' => 'done',
            ),
            394 => 
            array (
                'client_id' => 691,
                'created_at' => '2020-05-22 09:30:09',
                'deleted_at' => NULL,
                'id' => 398,
                'interaction_type' => 'call',
                'note' => '<p>CI Schedule: May 26,&nbsp; 2020 - 2 AM MNL<br />
Client&#39;s Name: Aaron Bagsby<br />
Timezone: IN - EST<br />
PT GVA<br />
<br />
Preferences:<br />
- Very organized and can keep client on track with current systems and process<br />
- Self starter and accountable<br />
<br />
Tasks:<br />
- Email clean up and management<br />
- Calendar management<br />
- Database management<br />
- Process management<br />
<br />
CRM: KW Command, MarketLeader, Google suite<br />
<br />
Schedule: 2-6 PM EST; to start 1st week of June</p>',
                'note_type' => 'others',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Preference',
                'status' => 'done',
            ),
            395 => 
            array (
                'client_id' => 664,
                'created_at' => '2020-05-22 09:41:58',
                'deleted_at' => NULL,
                'id' => 399,
                'interaction_type' => 'others',
                'note' => '<p>Sent VA profiles, he will review them and schedule a CI</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            396 => 
            array (
                'client_id' => 676,
                'created_at' => '2020-05-22 09:43:01',
                'deleted_at' => NULL,
                'id' => 400,
                'interaction_type' => 'email',
                'note' => '<p>System logins follow up<br />
Sent Timedly logins and instructional video</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            397 => 
            array (
                'client_id' => 626,
                'created_at' => '2020-05-22 09:44:05',
                'deleted_at' => NULL,
                'id' => 401,
                'interaction_type' => 'text',
                'note' => '<p>Touchbase call next week after Memorial day</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            398 => 
            array (
                'client_id' => 684,
                'created_at' => '2020-05-22 09:45:05',
                'deleted_at' => NULL,
                'id' => 402,
                'interaction_type' => 'call',
                'note' => '<p>VA Richellle has been doing terrifc and she&#39;s been helpful. She just needs some help to get organized</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            399 => 
            array (
                'client_id' => 563,
                'created_at' => '2020-05-22 09:45:49',
                'deleted_at' => NULL,
                'id' => 403,
                'interaction_type' => 'others',
                'note' => '<p>Zoom call and meet and greet tomorrow with Jester and her team<br />
at 3:30pm EST</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Email ,text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            400 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-05-22 18:59:16',
                'deleted_at' => NULL,
                'id' => 404,
                'interaction_type' => 'email',
                'note' => '<p>May 21, 2020</p>

<p>Informed him that unfortunately Jester Muldong was already hired yesterday - he agreed to replace him with another candidate and push back the CI tomorrow at 3:30 PM EST. Sent him Louize Castro&#39;s profile and update CV with DISC</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Add On',
                'status' => 'done',
            ),
            401 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-05-22 19:01:21',
                'deleted_at' => NULL,
                'id' => 405,
                'interaction_type' => 'call',
                'note' => '<p>May 21, 2020</p>

<p>He said he will have Zoe take the day off on Monday, Memorial Day. He said it doesn&#39;t make sense for her to report to work when there&#39;s nothing to do</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            402 => 
            array (
                'client_id' => 680,
                'created_at' => '2020-05-22 19:04:18',
                'deleted_at' => NULL,
                'id' => 406,
                'interaction_type' => 'others',
                'note' => '<p>May 21, 2020</p>

<p>She said that both Al and Chris can take the day off on Monday, Memorial Day but she might need them to schedule a make up shift for it in the coming week</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            403 => 
            array (
                'client_id' => 690,
                'created_at' => '2020-05-22 19:05:35',
                'deleted_at' => NULL,
                'id' => 407,
                'interaction_type' => 'others',
                'note' => '<p>May 21, 2020</p>

<p>Coordinated with him on Charmaen&#39;s log ins - gave VA directive&#39;s on what to do when the calls are routed to voicemails and what message to leave</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            404 => 
            array (
                'client_id' => 664,
                'created_at' => '2020-05-26 20:56:49',
                'deleted_at' => NULL,
                'id' => 408,
                'interaction_type' => 'call',
                'note' => '<p>Facilitated CI with VA John Prancis</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Client Interview',
                'status' => 'done',
            ),
            405 => 
            array (
                'client_id' => 688,
                'created_at' => '2020-05-27 14:05:19',
                'deleted_at' => NULL,
                'id' => 409,
                'interaction_type' => 'others',
                'note' => '<p>Follow up on Louis start date as IP addressed already established ( arranged call and moved start date to May 27, 2020</p>',
                    'note_type' => 'followup',
                    'noted_by' => 21,
                    'others_interaction' => 'whatsapp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                406 => 
                array (
                    'client_id' => 428,
                    'created_at' => '2020-05-27 14:09:44',
                    'deleted_at' => NULL,
                    'id' => 410,
                    'interaction_type' => 'email',
                    'note' => '<p>Sarah is doing a great job as always. Linda is very happy with her VA. Have not discussed any issues with her VA</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                407 => 
                array (
                    'client_id' => 347,
                    'created_at' => '2020-05-27 14:29:42',
                    'deleted_at' => NULL,
                    'id' => 411,
                    'interaction_type' => 'email',
                    'note' => '<p>notification on her VA&#39;s absence</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                408 => 
                array (
                    'client_id' => 536,
                    'created_at' => '2020-05-27 15:35:20',
                    'deleted_at' => NULL,
                    'id' => 412,
                    'interaction_type' => 'others',
                    'note' => '<p>Confirmed continuing of service. Acknowledged going back to regular rate.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'email, text and call',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                409 => 
                array (
                    'client_id' => 482,
                    'created_at' => '2020-05-27 15:37:28',
                    'deleted_at' => NULL,
                    'id' => 413,
                    'interaction_type' => 'email',
                    'note' => '<p>Informed of Jayson&#39;s absence due to medical reason-approved MUS request.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                410 => 
                array (
                    'client_id' => 649,
                    'created_at' => '2020-05-27 15:43:01',
                    'deleted_at' => NULL,
                    'id' => 414,
                    'interaction_type' => 'email',
                    'note' => '<p>Laura Wey-&nbsp;Provided new calling lists for the week for each VAs. Scheduled 1 on 1 meeting with Paula tomorrow at 3am.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                411 => 
                array (
                    'client_id' => 608,
                    'created_at' => '2020-05-27 15:47:33',
                    'deleted_at' => NULL,
                    'id' => 415,
                    'interaction_type' => 'others',
                    'note' => '<p>Provided positive feedback on VA Ro&#39;s performance.<br />
&quot;There have been times during this Covid stuff where I kinda lost a lot of motivation and some will. However, Ro has NOT. Her incredible optimism and work ethic has really helped carry my business through this mess. I now am in a new office and things are looking really good. If Ro wasn&#39;t there, who knows how my business would have looked right now. She is a total rockstar and has killer ideas. She plays more of a role in my business&#39; success than she may think. I am truly grateful. You guys rock!&quot;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                412 => 
                array (
                    'client_id' => 682,
                    'created_at' => '2020-05-27 16:08:17',
                    'deleted_at' => NULL,
                    'id' => 416,
                    'interaction_type' => 'email',
                    'note' => '<p>Created link and setting up of expectations regarding appointments.</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'First day meeting',
                    'status' => 'acknowleged',
                ),
                413 => 
                array (
                    'client_id' => 497,
                    'created_at' => '2020-05-27 16:47:11',
                    'deleted_at' => NULL,
                    'id' => 417,
                    'interaction_type' => 'call',
                    'note' => '<p>Replacement CI for GVA done at 10AM CST</p>

<p>Appllicants:</p>

<p>Keishon, Lyka, Hanna and Bea</p>

<p>Client is asking for more samples from Hanna - then will be giving decision within the day or tomorrow</p>

<p>=========</p>

<p>May 28, 2020</p>

<p>Update - Shane has chosen Hanna Cobong as his new GVA</p>

<p>Start date: June 1, 2020</p>

<p>10AM-2pm CST</p>',
                    'note_type' => 'others',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Client Interview for Replacement GVA',
                    'status' => 'open',
                ),
                414 => 
                array (
                    'client_id' => 599,
                    'created_at' => '2020-05-27 16:54:44',
                    'deleted_at' => NULL,
                    'id' => 418,
                    'interaction_type' => 'call',
                    'note' => '<p>May 26 - touch base to confirm on end of contract, but client doesn&#39;t wnt to lose VA and would like to change schedule</p>

<p>May 27 - Adilah has opted to change schedule to 1pm-3pm CST.</p>

<p>Mode of Comms: call and text</p>',
                    'note_type' => 'followup',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                415 => 
                array (
                    'client_id' => 672,
                    'created_at' => '2020-05-27 16:56:59',
                    'deleted_at' => NULL,
                    'id' => 419,
                    'interaction_type' => 'email',
                    'note' => '<p>Lindsay has scheduled CI for replacement GVA on Friday at 12NN EST</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'open',
                ),
                416 => 
                array (
                    'client_id' => 676,
                    'created_at' => '2020-05-27 17:02:55',
                    'deleted_at' => NULL,
                    'id' => 420,
                    'interaction_type' => 'others',
                    'note' => '<p>Requested start date to be moved tomorrow, May 28th.</p>

<p>&nbsp;</p>',
                    'note_type' => 'notification',
                    'noted_by' => 23,
                    'others_interaction' => 'Call , email',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                417 => 
                array (
                    'client_id' => 359,
                    'created_at' => '2020-05-27 18:56:34',
                    'deleted_at' => NULL,
                    'id' => 421,
                    'interaction_type' => 'text',
                    'note' => '<p>Follow up on approval for Make up shift proposal sent by Rich for 2 hr absence due to system issue</p>

<p>Kirk advd that he doesn&#39;t have enough volume or need to.</p>

<p>Comms: email, text</p>',
                    'note_type' => 'followup',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                418 => 
                array (
                    'client_id' => 613,
                    'created_at' => '2020-05-27 20:57:19',
                    'deleted_at' => NULL,
                    'id' => 422,
                    'interaction_type' => 'call',
                    'note' => '<p>May 26, 2020</p>

<p>Discussed cancellation because of financial issues - requested to waive the 2 weeks notice because she wouldn&#39;t have any money to pay for it anymore. She requested for me to tell Charlyn not to log in today anymore.</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Cancellation',
                    'status' => 'done',
                ),
                419 => 
                array (
                    'client_id' => 616,
                    'created_at' => '2020-05-27 20:59:06',
                    'deleted_at' => NULL,
                    'id' => 423,
                    'interaction_type' => 'email',
                    'note' => '<p>May 26, 2020</p>

<p>Sent him a final email that I will be sending his account for cancellation already due to non-response</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Cancellation',
                    'status' => 'done',
                ),
                420 => 
                array (
                    'client_id' => 572,
                    'created_at' => '2020-05-27 21:00:33',
                    'deleted_at' => NULL,
                    'id' => 424,
                    'interaction_type' => 'others',
                    'note' => '<p>May 26, 2020</p>

<p>He requested to extend the hold on the services for another 2weeks because their business is still not operational - resumption of services is moved to June 9, 2020</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'HangOuts',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'On-Hold Extension',
                    'status' => 'acknowleged',
                ),
                421 => 
                array (
                    'client_id' => 118,
                    'created_at' => '2020-05-27 21:01:41',
                    'deleted_at' => NULL,
                    'id' => 425,
                    'interaction_type' => 'email',
                    'note' => '<p>May 26, 2020</p>

<p>Sent him the proposed start date and schedule for Louize Castro - Start Date is May 28th (Thursday) and schedule is 9AM - 1PM EST</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Additional VA',
                    'status' => 'done',
                ),
                422 => 
                array (
                    'client_id' => 680,
                    'created_at' => '2020-05-27 21:02:40',
                    'deleted_at' => NULL,
                    'id' => 426,
                    'interaction_type' => 'email',
                    'note' => '<p>May 26, 2020</p>

<p>Discussed MUS schedule for both Christopher Dumao and Al Exconde:<br />
<br />
Christopher Dumao<br />
May 30 - 8AM - 1PM EST<br />
June 6 - 8AM - 1PM EST<br />
<br />
Al Norman Exconde<br />
May 31 - 9AM - 2PM EST<br />
June 7 - 9AM - 2PM EST</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                423 => 
                array (
                    'client_id' => 667,
                    'created_at' => '2020-05-27 21:03:32',
                    'deleted_at' => NULL,
                    'id' => 427,
                    'interaction_type' => 'others',
                    'note' => '<p>May 26, 2020</p>

<p>Conference call with both his VAs Villa Sandong and Zoe Guico - meeting was for calibration and discussion for process improvement</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'Google Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                424 => 
                array (
                    'client_id' => 652,
                    'created_at' => '2020-05-27 21:04:31',
                    'deleted_at' => NULL,
                    'id' => 428,
                    'interaction_type' => 'call',
                    'note' => '<p>May 26, 2020</p>

<p>Discussed VA Elvin Britanico&#39;s performance - she said she feels like the progress is slow and he&#39;s not functioning as an EVA so she requested for a replacement. She asked not to tell Elvin yet until we find a good VA to replace him.</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Replacement Request',
                    'status' => 'acknowleged',
                ),
                425 => 
                array (
                    'client_id' => 675,
                    'created_at' => '2020-05-27 21:05:43',
                    'deleted_at' => NULL,
                    'id' => 429,
                    'interaction_type' => 'others',
                    'note' => '<p>May 26, 2020</p>

<p>Approved Kenah&#39;s absence for the 2nd part of her shift today due to sickness - agreed to make up for the 2hrs tomorrow and the next day</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                426 => 
                array (
                    'client_id' => 690,
                    'created_at' => '2020-05-27 21:06:55',
                    'deleted_at' => NULL,
                    'id' => 430,
                    'interaction_type' => 'others',
                    'note' => '<p>May 26, 2020</p>

<p>Discussed moving Charmaen&#39;s schedule to Monday - Friday this week. He doesn&#39;t need her during weekends anymore</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Schedule Change',
                    'status' => 'done',
                ),
                427 => 
                array (
                    'client_id' => 662,
                    'created_at' => '2020-05-27 21:09:18',
                    'deleted_at' => NULL,
                    'id' => 432,
                    'interaction_type' => 'text',
                    'note' => '<p>Andrew wanted to know if his VA: Jem will still be reportting..</p>

<p>Advd that a notice was sent by Jem re shift time with screnshot advg of change of schedule&nbsp;</p>

<p>Adrew forgot about schedule change.</p>

<p>&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                428 => 
                array (
                    'client_id' => 659,
                    'created_at' => '2020-05-28 12:41:12',
                    'deleted_at' => NULL,
                    'id' => 433,
                    'interaction_type' => 'text',
                    'note' => '<p>Discussed issues with Elvin on attendance and issues on work productivity. Threatened on cancelling services. Will discuss on Monday on what comes next.</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Issues with VA',
                    'status' => 'open',
                ),
                429 => 
                array (
                    'client_id' => 563,
                    'created_at' => '2020-05-28 15:44:28',
                    'deleted_at' => NULL,
                    'id' => 434,
                    'interaction_type' => 'email',
                'note' => '<p>Joana Brown (POC)-Coordinated on day&#39;s activity for Jester as logins for the tools/systems are still being created.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                430 => 
                array (
                    'client_id' => 559,
                    'created_at' => '2020-05-28 15:47:27',
                    'deleted_at' => NULL,
                    'id' => 435,
                    'interaction_type' => 'others',
                    'note' => '<p>TJ Smith-&nbsp;Requested to have an earlier shift for VA Lady.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                431 => 
                array (
                    'client_id' => 598,
                    'created_at' => '2020-05-28 15:50:22',
                    'deleted_at' => NULL,
                    'id' => 436,
                    'interaction_type' => 'call',
                    'note' => '<p>Followed-up on resumption of VA services since PA is now open for RE businesses. Informed that they are still under the red zone part of PA but will be in the yellow zone after a week. Will just callback once ready as she is still waiting for her additional funds.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                432 => 
                array (
                    'client_id' => 649,
                    'created_at' => '2020-05-28 15:54:45',
                    'deleted_at' => NULL,
                    'id' => 437,
                    'interaction_type' => 'others',
                    'note' => '<p>Laura Wey-&nbsp;Meeting with the VAs today. Discussed lead management and focusing more on data clean-up aside from setting up appointments.</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 24,
                    'others_interaction' => 'calls and emails',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                433 => 
                array (
                    'client_id' => 693,
                    'created_at' => '2020-05-28 16:06:42',
                    'deleted_at' => NULL,
                    'id' => 438,
                    'interaction_type' => 'others',
                    'note' => '<p>May 27, 2020</p>

<p>Meet &amp; Greet with his new VA Lykalyn Espera:<br />
Schedule:<br />
Mon, Wed, Thur &amp; Fri 10AM - 2PM EST<br />
Tues 9:15AM - 1:15AM EST<br />
Start Date: May 28, 2020 (Thursday)<br />
CRM/Tools: KW Command, Referral Maker, Dotloop, Docusign, Buffini<br />
System Log Ins: sent via email<br />
Tasks: Social Media Marketing &amp; Postings, Sending Newsletters, E-card, Calendar Management, Email Management, Youtube video editing<br />
Mode of Communication: WhatsApp</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'RC Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Meet & Greet',
                    'status' => 'done',
                ),
                434 => 
                array (
                    'client_id' => 652,
                    'created_at' => '2020-05-28 16:07:56',
                    'deleted_at' => NULL,
                    'id' => 439,
                    'interaction_type' => 'others',
                    'note' => '<p>May 27, 2020</p>

<p>Complaint: VA Elvin Britanico - slow progress and attendance issues. She was fined by the compliance team the other of KW because Elvin was not able to finish and submit a document which is required for one of her transactions. 2 weeks ago, VA was 2 days absent, yesterday VA was undertime, and today VA was tagged No Call No Show. She wants to replace him already.<br />
<br />
She interviewed VA Michelle Santiago today as a replacement for Elvin. She liked her previous experiernce as a Transaction Coordinator and the fact that she was assisting 31 RE agents for his previous client John Rainville. Actually most of what she was doing for John is the same thing that Nalee is doing. We agreed to have Michelle start tomorrow and her schedule is Monday - Friday 9AM - 1PM CST.</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'RC Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Replacement',
                    'status' => 'done',
                ),
                435 => 
                array (
                    'client_id' => 118,
                    'created_at' => '2020-05-28 16:09:00',
                    'deleted_at' => NULL,
                    'id' => 440,
                    'interaction_type' => 'email',
                    'note' => '<p>May 27, 2020</p>

<p>Sent his new ISA&#39;s (Louize Castro) system log ins today and requested to have Gracee onboard Louize tomorrow about their process and system navigation</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                436 => 
                array (
                    'client_id' => 690,
                    'created_at' => '2020-05-28 16:10:28',
                    'deleted_at' => NULL,
                    'id' => 441,
                    'interaction_type' => 'others',
                    'note' => '<p>May 27, 2020</p>

<p>Touchbase - said that Charmaen needs more training for KVCore but so far, she&#39;s doing pretty good. Was able to set 1 appointment already yesterday. He also wanted to make sure that her schedule is already updated</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                437 => 
                array (
                    'client_id' => 677,
                    'created_at' => '2020-05-28 21:05:01',
                    'deleted_at' => NULL,
                    'id' => 442,
                    'interaction_type' => 'call',
                    'note' => '<p>expectation - need to have a conversation with Lalla</p>

<ul>
<li style="list-style-type:disc">Haven&rsquo;t rcvd a call - weekly &nbsp;</li>
<li style="list-style-type:disc">2. Conversation on help with Command - challenge around that&nbsp;</li>
<li style="list-style-type:disc">Other VA background -&nbsp;</li>
<li style="list-style-type:disc">Priority that was put out there</li>
<li style="list-style-type:disc">Don&rsquo;t see level of quality on perception - KW Command SME session
<ul>
<li style="list-style-type:disc">Disagreeing on level of knowledge/ experience of VA&nbsp;&nbsp;</li>
<li style="list-style-type:disc">Perception/ Expectations with Lalla - not getting what he initially state</li>
</ul>
</li>
<li style="list-style-type:disc">Weekly touch base - dn me - assumed that no need to touch base at that time as we&#39;ve had multiple discussios that week (start o fVA)</li>
<li style="list-style-type:disc">SMe session scheduled by end of shift today for KV Command</li>
<li style="list-style-type:disc">Will have a talk coaching session with Cielo</li>
<li style="list-style-type:disc">referred to Coach Lalla for call back as requested</li>
</ul>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                438 => 
                array (
                    'client_id' => 501,
                    'created_at' => '2020-05-28 21:35:00',
                    'deleted_at' => NULL,
                    'id' => 443,
                    'interaction_type' => 'call',
                    'note' => '<p>David has decided to let go of Aaron Sorne</p>

<p>- not much videos to be edited as Course Producer whose supposed to be creting the videos - refuses to go to the site due ot the Pandemic</p>

<p>- There are 2 VAs woking on video editing - Aaron&#39;s work is ok but Paola&#39;s &nbsp; work is very good</p>

<p>- Feedback on Paola - Paola will be the one to do the video editing. Paola&rsquo;s work is very good. She is a 1 stop shop.</p>

<p>- Aaron&#39;s last day today but no need to report for work</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                439 => 
                array (
                    'client_id' => 547,
                    'created_at' => '2020-05-29 14:47:43',
                    'deleted_at' => NULL,
                    'id' => 444,
                    'interaction_type' => 'others',
                    'note' => '<p>Provided positive feedback on Lara&#39;s performance. &quot;She is good. Everything is fine.&quot;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                440 => 
                array (
                    'client_id' => 487,
                    'created_at' => '2020-05-29 14:48:55',
                    'deleted_at' => NULL,
                    'id' => 445,
                    'interaction_type' => 'others',
                    'note' => '<p>Provided positive feedback on his VAs and VD services.<br />
&quot;Everything is going well. Very happy with your services.&quot;<br />
&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                441 => 
                array (
                    'client_id' => 464,
                    'created_at' => '2020-05-29 14:50:30',
                    'deleted_at' => NULL,
                    'id' => 446,
                    'interaction_type' => 'others',
                    'note' => '<p>Checked for updates/feedback on Keishon&#39;s tasks. Very satisfied with his consistency and being proactive at all times. &quot;...As each day passes, Keishon and I are forging a better working relationship.&quot;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                442 => 
                array (
                    'client_id' => 61,
                    'created_at' => '2020-05-29 14:51:42',
                    'deleted_at' => NULL,
                    'id' => 447,
                    'interaction_type' => 'others',
                'note' => '<p>Requested for the services to be placed on hold for 3-6 months. Mentioned that they will be lessening calls for the next few months. Provided positive&nbsp;feedback on Joe&#39;s performance and will get back once everything goes back to normal after Covid. Inquiry on set-up fee-(confirmed with OM). Agreed on Joe&#39;s last day on June 16th.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => 'email, text and call',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                443 => 
                array (
                    'client_id' => 652,
                    'created_at' => '2020-05-29 16:19:30',
                    'deleted_at' => NULL,
                    'id' => 448,
                    'interaction_type' => 'email',
                    'note' => '<p>May 28, 2020</p>

<p>Requested for copies of her past 2 invoices because VA accidentally deleted it from her email - she just wants to make sure that she was not billed for Elvin&#39;s absences 3 weeks ago. Also followed up on Michelle system log ins and access for RC</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                444 => 
                array (
                    'client_id' => 693,
                    'created_at' => '2020-05-29 16:20:20',
                    'deleted_at' => NULL,
                    'id' => 449,
                    'interaction_type' => 'email',
                    'note' => '<p>May 28, 2020</p>

<p>He required me and Lyka to attend the KW Command training today - he also had the VA work on his holiday cards and said that he&#39;s very happy with Lyka and he feels that this is a start of something great for the both of them</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                445 => 
                array (
                    'client_id' => 690,
                    'created_at' => '2020-05-29 16:21:20',
                    'deleted_at' => NULL,
                    'id' => 450,
                    'interaction_type' => 'others',
                    'note' => '<p>May 28, 2020</p>

<p>Provided feedback for his ISA Charmaen Espina - he said that she hasn&#39;t set an appointment yet but he understand because she&#39;s working on FSBO leads and it&#39;s a lot harder. Gave our commitment that we&#39;ll have the VA attend role play session to practice</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                446 => 
                array (
                    'client_id' => 548,
                    'created_at' => '2020-05-29 16:22:28',
                    'deleted_at' => NULL,
                    'id' => 451,
                    'interaction_type' => 'email',
                    'note' => '<p>May 28, 2020</p>

<p>Reached out to touchbase - said he&#39;s not available because he&#39;s in a showing. We scheduled a call tomorrow at 12PM EST</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                447 => 
                array (
                    'client_id' => 653,
                    'created_at' => '2020-06-01 12:19:46',
                    'deleted_at' => NULL,
                    'id' => 452,
                    'interaction_type' => 'email',
                    'note' => '<p>It has been a busy week. Nog is doing great!! She Is catching&nbsp;on and I think adding a few more hours is helping her.&nbsp;</p>

<p>&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                448 => 
                array (
                    'client_id' => 279,
                    'created_at' => '2020-06-01 16:57:22',
                    'deleted_at' => NULL,
                    'id' => 453,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Request to schedule a triad call with VA Rhony next week.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                449 => 
                array (
                    'client_id' => 649,
                    'created_at' => '2020-06-01 17:00:25',
                    'deleted_at' => NULL,
                    'id' => 454,
                    'interaction_type' => 'email',
                    'note' => '<p>Instructions on active tasks assigned to ACT team and ownership on assigning tasks per lead.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                450 => 
                array (
                    'client_id' => 274,
                    'created_at' => '2020-06-01 17:03:09',
                    'deleted_at' => NULL,
                    'id' => 455,
                    'interaction_type' => 'email',
                    'note' => '<p>Followed-up on TBS start date with Wena.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                451 => 
                array (
                    'client_id' => 672,
                    'created_at' => '2020-06-02 00:00:13',
                    'deleted_at' => NULL,
                    'id' => 456,
                    'interaction_type' => 'others',
                    'note' => '<p>GVA Replacement CI held at 10:30AM EST</p>

<p>Applicants: Keshion Aban, Lyka Espera, Bea Clare Ramos</p>

<p>Lindsay chose Keishon Aban - to start tom June 2, 2020 - 9am-1pm EST</p>',
                    'note_type' => 'others',
                    'noted_by' => 22,
                    'others_interaction' => 'Ring Central Meeting',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'GVA Replacement CI',
                    'status' => 'done',
                ),
                452 => 
                array (
                    'client_id' => 695,
                    'created_at' => '2020-06-02 13:07:40',
                    'deleted_at' => NULL,
                    'id' => 457,
                    'interaction_type' => 'email',
                    'note' => '<p>Booked: June 1<br />
Preferred shift: 9AM-1PM<br />
Start date: TBD<br />
Meet &amp; Greet: June 4, 11 AM EST | June 4, 11 PM MNL<br />
VA:&nbsp;Joshua&nbsp;Mu&ntilde;oz</p>

<p>Coach: Che Lamata</p>

<p>&nbsp;</p>',
                    'note_type' => 'others',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'M&G endorsement',
                    'status' => 'done',
                ),
                453 => 
                array (
                    'client_id' => 696,
                    'created_at' => '2020-06-02 13:10:55',
                    'deleted_at' => NULL,
                    'id' => 458,
                    'interaction_type' => 'email',
                    'note' => '<p>Booked: May 27<br />
Coach: Nica<br />
Preferred shift: 8-5 PM PST<br />
Start date: TBD<br />
Meet &amp; Greet: May 27, 11 AM PST | May 28, 2 AM MNL<br />
VA:&nbsp;Harvey&nbsp;Moro</p>',
                    'note_type' => 'others',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'M&G endorsement',
                    'status' => 'done',
                ),
                454 => 
                array (
                    'client_id' => 698,
                    'created_at' => '2020-06-02 13:16:19',
                    'deleted_at' => NULL,
                    'id' => 459,
                    'interaction_type' => 'email',
                    'note' => '<p>Booked: May 27<br />
Coach: Nica<br />
Preferred shift: 7-11 AM PST<br />
Start date: June 1, Monday<br />
Meet &amp; Greet: May 28, 5 PM PST | May 29, 8 AM MNL<br />
VA: Marinelle&nbsp;Ocot</p>',
                    'note_type' => 'others',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'M&G endorsement',
                    'status' => 'done',
                ),
                455 => 
                array (
                    'client_id' => 699,
                    'created_at' => '2020-06-02 13:19:24',
                    'deleted_at' => NULL,
                    'id' => 460,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Booked: June 1<br />
Coach: Che Lamata<br />
Preferred shift: flexible hours<br />
Start date: June 8 , Monday<br />
Meet &amp; Greet: June 4, 10 AM EST | June 4, 10 PM MNL<br />
VA:&nbsp;Joshua&nbsp;Mu&ntilde;oz</p>',
                    'note_type' => 'others',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'M&G endorsement',
                    'status' => 'done',
                ),
                456 => 
                array (
                    'client_id' => 700,
                    'created_at' => '2020-06-02 13:23:36',
                    'deleted_at' => NULL,
                    'id' => 461,
                    'interaction_type' => 'email',
                    'note' => '<p>Booked: May 29<br />
Coach: Che Lamata<br />
Preferred shift: M-F afternoon<br />
Meet &amp; Greet: May 30, 9 AM EST | May 30, 9 PM MNL<br />
VA:&nbsp;Russell&nbsp;Ulat</p>',
                    'note_type' => 'others',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'M&G endorsement',
                    'status' => 'done',
                ),
                457 => 
                array (
                    'client_id' => 701,
                    'created_at' => '2020-06-02 13:26:21',
                    'deleted_at' => NULL,
                    'id' => 462,
                    'interaction_type' => 'email',
                    'note' => '<p>Booked: May 29<br />
Coach: Che lamata<br />
Preferred shift: 9AM-1PM<br />
Start date: asap<br />
Meet &amp; Greet: May 29, 11 AM PST | May 30, 2 AM MNL<br />
VA:&nbsp;Russell&nbsp;Ulat</p>',
                    'note_type' => 'others',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'M&G endorsement',
                    'status' => 'done',
                ),
                458 => 
                array (
                    'client_id' => 366,
                    'created_at' => '2020-06-02 16:13:57',
                    'deleted_at' => NULL,
                    'id' => 463,
                    'interaction_type' => 'email',
                    'note' => '<p>June 01, 2020</p>

<p>Requested that we conduct a weekly performance review for Jefte with his Sales Manager David Rosenthal - agreed to have it every Friday, 8:30AM EST</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                459 => 
                array (
                    'client_id' => 692,
                    'created_at' => '2020-06-02 16:30:08',
                    'deleted_at' => NULL,
                    'id' => 464,
                    'interaction_type' => 'others',
                    'note' => '<p>June 01, 2020</p>

<p>Conference Call via RC Meetings with his business partner Robert - we discussed expectations/goals for Arjayson when it comes to appointment setting for the next 30-90 days. We also had a walkthrough with the VA for Podio</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'Ring Central Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                460 => 
                array (
                    'client_id' => 690,
                    'created_at' => '2020-06-02 16:32:36',
                    'deleted_at' => NULL,
                    'id' => 465,
                    'interaction_type' => 'others',
                    'note' => '<p>June 01, 2020</p>

<p>Informed him that his VA Charmaen won&#39;t be able to work today due to ISP issues and even back up ISP is not working. He also request for us to pull out some call recordings for Charmaen for review and feedback</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                461 => 
                array (
                    'client_id' => 702,
                    'created_at' => '2020-06-02 16:34:24',
                    'deleted_at' => NULL,
                    'id' => 466,
                    'interaction_type' => 'others',
                    'note' => '<p>June 01, 2020</p>

<p>Meet &amp; Greet with new VA Margaret Pineda:<br />
Start Date: June 3, 2020 (Wednesday)<br />
Schedule:<br />
T-W-Th 9AM - 11AM &amp; 5PM - 7AM EST<br />
Fri 5PM - 7PM EST<br />
Sat 1PM - 7PM EST<br />
CRM/Tools:Perfect Storm, Company Email, Skype<br />
System Log Ins: will be emailed tomorrow<br />
Expectations: 5 appts/week after 1 month<br />
Leads: Buyers, Expireds, Investments, Lease Purchases &amp; FSBOs<br />
Calendar Sharing: google calendar<br />
Mode of Communication: Trello</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'RC Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Meet & Greet',
                    'status' => 'done',
                ),
                462 => 
                array (
                    'client_id' => 671,
                    'created_at' => '2020-06-02 22:38:23',
                    'deleted_at' => NULL,
                    'id' => 467,
                    'interaction_type' => 'text',
                    'note' => '<p>Ronnie has been moving his Rizza Vejerano&#39;s start date.</p>

<p>Latest update is to move it to June 8 - advd that we can only hold the VA up to a certain time - if on June 8 - the start date will be moved again - VA will be endorsed back to placements so she can be booked by another client.</p>

<p>Ronnie assured us that it will be 100% ready to go with her.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                463 => 
                array (
                    'client_id' => 686,
                    'created_at' => '2020-06-02 22:42:14',
                    'deleted_at' => NULL,
                    'id' => 468,
                    'interaction_type' => 'text',
                    'note' => '<p>Scheduled ISA Replacement CI for today June 2 2pm EST</p>

<p>Toni, forgot about it - she was at the doctor&#39;s having an echogram</p>

<p>Rescheduled CI to tom, June 3 at 2pm EST. All good</p>

<p>Created and sent RC Meeting dtls to Toni</p>',
                    'note_type' => 'others',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'ISA Replacement ISA',
                    'status' => 'open',
                ),
                464 => 
                array (
                    'client_id' => 648,
                    'created_at' => '2020-06-02 22:51:44',
                    'deleted_at' => NULL,
                    'id' => 469,
                    'interaction_type' => 'call',
                    'note' => '<p>May 28, 2020</p>

<p>Scott provided feedback that there have been delays on reports from VA</p>

<p>Would indicate on SODs that he would be doing things but nothing submitted for approval</p>

<p>lesser work done by VA</p>

<p>Offered for replacement - Scott advd that he will be talkin got his partner - Neil about it</p>

<p>==============</p>

<p>June 2 - follow up</p>

<p>Scott has decided to pause/ put Kenneth&#39;s svcs on hold&nbsp;</p>

<p>Offered replacement GVA - advd we have a few available GVAs who have experience</p>

<p>Will provide update tom re replacement GVA after discussing things with Neil</p>

<p>===========</p>

<p>June 4, 2020 - follow up</p>

<p>Scott and Neil have decided to get a replacement VA</p>

<p>CI would either be Monday or Tuesday next week</p>

<p>I will be sending out CVs of GVAs for interview - Knowledgeable in KV Core is a plus (but not a requirement)</p>

<p>inclued Neil on email of CVs:&nbsp;neil@thehomebasegroup.com</p>',
                    'note_type' => 'followup',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                465 => 
                array (
                    'client_id' => 688,
                    'created_at' => '2020-06-03 12:41:04',
                    'deleted_at' => NULL,
                    'id' => 470,
                    'interaction_type' => 'others',
                    'note' => '<p>Touch base regarding Louis performance and everything is doing good. looking forward for good results</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => 'whats app',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                466 => 
                array (
                    'client_id' => 659,
                    'created_at' => '2020-06-03 13:11:09',
                    'deleted_at' => NULL,
                    'id' => 471,
                    'interaction_type' => 'call',
                    'note' => '<p>Tatiana threatens to cancel because of her&#39;s VA&#39;s absence. Offered replacement VA still thinking if she would continue services.</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'VA replacement',
                    'status' => 'open',
                ),
                467 => 
                array (
                    'client_id' => 146,
                    'created_at' => '2020-06-03 16:19:31',
                    'deleted_at' => NULL,
                    'id' => 472,
                    'interaction_type' => 'call',
                    'note' => '<p>Touchbase today at 1pm PST about the resumption to regular hours.</p>

<p>&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                468 => 
                array (
                    'client_id' => 705,
                    'created_at' => '2020-06-03 17:27:13',
                    'deleted_at' => NULL,
                    'id' => 473,
                    'interaction_type' => 'others',
                    'note' => '<p>&nbsp;First day assistance on setting up of tools and transitioning tasks to her VA-Russell. First few tasks include:<br />
-Zoom meeting (onboarding)<br />
-familiarization with KWLS<br />
-moving tasks and contacts to KW<br />
-FB ads</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                469 => 
                array (
                    'client_id' => 704,
                    'created_at' => '2020-06-03 17:28:10',
                    'deleted_at' => NULL,
                    'id' => 474,
                    'interaction_type' => 'others',
                    'note' => '<p>Systems and log-ins are not yet ready and set-up for his VA just yet. Requested to push back the start date tomorrow.<br />
&nbsp;</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                470 => 
                array (
                    'client_id' => 701,
                    'created_at' => '2020-06-03 17:30:00',
                    'deleted_at' => NULL,
                    'id' => 475,
                    'interaction_type' => 'others',
                    'note' => '<p>First day assistance on setting up of tools and transitioning tasks to her VA-Russell. First few tasks include:<br />
-Zoom meeting (onboarding)<br />
-Learn KW Command Listings<br />
-Facebook Ads<br />
-Advertise 1152 S Point View St Los Angeles</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                471 => 
                array (
                    'client_id' => 649,
                    'created_at' => '2020-06-03 17:30:55',
                    'deleted_at' => NULL,
                    'id' => 476,
                    'interaction_type' => 'others',
                    'note' => '<p>Laura Wey-&nbsp;Coordinated on a new calling list for the week. Shared recommended script to use.<br />
Reminded to always update their ISA daily tracking (from Flyhomes)</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 24,
                    'others_interaction' => 'call and email',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                472 => 
                array (
                    'client_id' => 563,
                    'created_at' => '2020-06-03 17:59:02',
                    'deleted_at' => NULL,
                    'id' => 477,
                    'interaction_type' => 'others',
                    'note' => '<p>Reached out to POC Lisa, reached VM and left message</p>

<p>Emailed that Vulcan 7 autodialer is working, asked for VOIP that Carl can use</p>',
                    'note_type' => 'followup',
                    'noted_by' => 23,
                    'others_interaction' => 'Call , email',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                473 => 
                array (
                    'client_id' => 686,
                    'created_at' => '2020-06-03 22:10:52',
                    'deleted_at' => NULL,
                    'id' => 478,
                    'interaction_type' => 'call',
                    'note' => '<p>Replacement ISA CI - June 3, 2020 at 3PM EST</p>

<p>Fritz Laigo, Monique Castro and Charlyn Hernando</p>

<p>Shortlisted: Charlyn and Toni</p>

<p>Asked for Charlyn&#39;s referrals from clients</p>

<p>- sent email advg that we only recommend experienced VAs with good track record and also attached commendations/ feedback of client</p>

<p>... Waiting for Toni&#39;s decision</p>

<p>June 4, 2020</p>

<p>Toni has chosen Charlyn - Shift time:&nbsp;10:30a - 12:30p then 6:30p-8:30p EST/ Start Date: Monday, June 8, 2020</p>

<p>** Comms via RC Meetings, phone call, email and text</p>',
                    'note_type' => 'others',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'ISA Replacement CI',
                    'status' => 'done',
                ),
                474 => 
                array (
                    'client_id' => 692,
                    'created_at' => '2020-06-03 22:27:43',
                    'deleted_at' => NULL,
                    'id' => 479,
                    'interaction_type' => 'text',
                    'note' => '<p>June 2, 2020</p>

<p>Confirmed with him that Arjayson can dial other campaigns from different areas other than Absentee Greensboro</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                475 => 
                array (
                    'client_id' => 667,
                    'created_at' => '2020-06-03 22:28:44',
                    'deleted_at' => NULL,
                    'id' => 480,
                    'interaction_type' => 'others',
                    'note' => '<p>June 2, 2020</p>

<p>Joined his team&#39;s quick team huddle with Zoe &amp; Villa via Google Meetings - he commended both VAs for their hardwork and diligence</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'Google Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                476 => 
                array (
                    'client_id' => 693,
                    'created_at' => '2020-06-03 22:31:40',
                    'deleted_at' => NULL,
                    'id' => 481,
                    'interaction_type' => 'email',
                    'note' => '<p>June 2, 2020</p>

<p>Inquired if Lyka can also do prospecting calls - scheduled a conference call tomorrow with Lyka at 10AM EST to discuss the process usng Cole&#39;s directory</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                477 => 
                array (
                    'client_id' => 160,
                    'created_at' => '2020-06-03 22:33:29',
                    'deleted_at' => NULL,
                    'id' => 482,
                    'interaction_type' => 'text',
                    'note' => '<p>June 2, 2020</p>

<p>Claudia Garza -Reached out for touch base and to discuss John&#39;s yearly increment but she&#39;s not available (sent her a text message as well)</p>',
                    'note_type' => 'followup',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                478 => 
                array (
                    'client_id' => 690,
                    'created_at' => '2020-06-03 22:34:44',
                    'deleted_at' => NULL,
                    'id' => 483,
                    'interaction_type' => 'others',
                    'note' => '<p>June 2, 2020</p>

<p>Informed him that his RedX subscription doesn&#39;t include a call recording feature, suggested that we&#39;ll have a third party app installed on Charmaen&#39;s computer so we can record her calls. We also discussed his VA&#39;s MUS schedule on Saturday for her absence yesterday</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                479 => 
                array (
                    'client_id' => 224,
                    'created_at' => '2020-06-03 22:36:50',
                    'deleted_at' => NULL,
                    'id' => 484,
                    'interaction_type' => 'email',
                    'note' => '<p>June 2, 2020</p>

<p>Followed up on resumption of services - no answer (email sent)</p>',
                    'note_type' => 'followup',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                480 => 
                array (
                    'client_id' => 614,
                    'created_at' => '2020-06-03 22:37:40',
                    'deleted_at' => NULL,
                    'id' => 485,
                    'interaction_type' => 'others',
                    'note' => '<p>June 2, 2020</p>

<p>Reached out to her to know her availability to interview ISA candidates in replacement for Gracee - no response yet (message sent via WhatsApp as well)</p>',
                    'note_type' => 'followup',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                481 => 
                array (
                    'client_id' => 702,
                    'created_at' => '2020-06-03 22:38:38',
                    'deleted_at' => NULL,
                    'id' => 486,
                    'interaction_type' => 'email',
                    'note' => '<p>June 2, 2020</p>

<p>Followed up on Margaret&#39;s system log in for tomorrow&#39;s shift - VA will start tomorrow. Also set up a quick conference call at 9AM EST to kick start the VA&#39;s calling</p>',
                    'note_type' => 'followup',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                482 => 
                array (
                    'client_id' => 466,
                    'created_at' => '2020-06-04 17:36:36',
                    'deleted_at' => NULL,
                    'id' => 487,
                    'interaction_type' => 'call',
                    'note' => '<p>Stephanie will be replaced and being handled already by the Client services team. Yet to be identified</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Replacement',
                    'status' => 'open',
                ),
                483 => 
                array (
                    'client_id' => 702,
                    'created_at' => '2020-06-04 21:03:01',
                    'deleted_at' => NULL,
                    'id' => 488,
                    'interaction_type' => 'others',
                    'note' => '<p>June 3, 2020</p>

<p>He required me and Margaret to attend training with him today for Perfect Storm CRM - he walked us through the process in updating lead info and how to input comments and create bombbomb videos from it</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'Zoom Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Training',
                    'status' => 'done',
                ),
                484 => 
                array (
                    'client_id' => 693,
                    'created_at' => '2020-06-04 21:03:55',
                    'deleted_at' => NULL,
                    'id' => 489,
                    'interaction_type' => 'others',
                    'note' => '<p>June 3, 2020</p>

<p>Conference call with his VA Lyka and discussed that she&#39;s going to do prospecting calls starting Monday and she&#39;ll be using Cole&#39;s directory and do manual dialing using skype subscription. He also wanted us to figure out how to change email block in eEdge and also how to import contacts from eEdge and transfer it to KW Command</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 19,
                    'others_interaction' => 'Zoom Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                485 => 
                array (
                    'client_id' => 160,
                    'created_at' => '2020-06-04 21:05:02',
                    'deleted_at' => NULL,
                    'id' => 490,
                    'interaction_type' => 'call',
                    'note' => '<p>June 3, 2020</p>

<p>Claudia Garza -&nbsp;She already approved VA John Tolentino&#39;s $0.50/hr 2nd anniversary increase - she said that John deserves it because he&#39;s been an integral part of their team and he&#39;s really a big help for them</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Rate Increase',
                    'status' => 'done',
                ),
                486 => 
                array (
                    'client_id' => 706,
                    'created_at' => '2020-06-04 21:06:12',
                    'deleted_at' => NULL,
                    'id' => 491,
                    'interaction_type' => 'others',
                    'note' => '<p>June 3, 2020</p>

<p>Meet &amp; Greet today with his new VA Christiane Eunice Romero:<br />
Start Date: June 4, 2020<br />
Schedule: M-F 9AM - 1PM EST<br />
CRM/Tools: KVCore, GSMLS, RedX/Vortex, Google Voice,&nbsp;Realtor.com, Zillow<br />
System Log ins: will be sent today<br />
Tasks: Appointment setting, CRM Management<br />
Leads: Buyer Leads (also needs to qualify them)<br />
Scripts: Tom Ferry Scripts<br />
Calendar: Google Calendar<br />
Mode of Communication: WhatsApp<br />
<br />
Also discussed Timedly Tracker</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'RC Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Meet & Greet',
                    'status' => 'done',
                ),
                487 => 
                array (
                    'client_id' => 118,
                    'created_at' => '2020-06-04 21:07:54',
                    'deleted_at' => NULL,
                    'id' => 492,
                    'interaction_type' => 'email',
                    'note' => '<p>June 3, 2020</p>

<p>We discussed having Leslie transition to FT since she&#39;s been rendering Overtime frequently lately - Robert approved and will move her to FT starting Monday</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Additional Hours',
                    'status' => 'done',
                ),
                488 => 
                array (
                    'client_id' => 651,
                    'created_at' => '2020-06-04 21:20:36',
                    'deleted_at' => NULL,
                    'id' => 493,
                    'interaction_type' => 'others',
                    'note' => '<p>June 3, 2020</p>

<p>Reached out to him to get feedback for Kavin Suan - he&#39;s been setting a lot of appointments (call and in-person appts) for FSBO leads</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                489 => 
                array (
                    'client_id' => 600,
                    'created_at' => '2020-06-04 21:27:58',
                    'deleted_at' => NULL,
                    'id' => 494,
                    'interaction_type' => 'text',
                    'note' => '<p>June 3, 2020</p>

<p>Reached out to him to touch base about Gracee and Charlyn&#39;s performance - no answer (sent him a text message as well)</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                490 => 
                array (
                    'client_id' => 706,
                    'created_at' => '2020-06-05 14:31:38',
                    'deleted_at' => NULL,
                    'id' => 495,
                    'interaction_type' => 'others',
                    'note' => '<p>June 4, 2020</p>

<p>Conference call via RC Meetings - I walked him through on how to navigate and check screenshot in Timedly Tracker. He said it was cool and very user friendly.</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'RC Meetings',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Timedly Walkthrough',
                    'status' => 'done',
                ),
                491 => 
                array (
                    'client_id' => 177,
                    'created_at' => '2020-06-05 14:32:59',
                    'deleted_at' => NULL,
                    'id' => 496,
                    'interaction_type' => 'text',
                    'note' => '<p>June 4, 2020</p>

<p>Disapproved Iryn&#39;s annual increase because she&#39;s currently in the hospital recovering from illness. She asked to revisit the discussion after a month or two</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Rate Increase',
                    'status' => 'done',
                ),
                492 => 
                array (
                    'client_id' => 651,
                    'created_at' => '2020-06-05 14:33:59',
                    'deleted_at' => NULL,
                    'id' => 497,
                    'interaction_type' => 'others',
                    'note' => '<p>June 4, 2020</p>

<p>Touchbase - gave a very positive feedback for his VA Kavin Suan:<br />
<br />
&quot;He&rsquo;s awesome! Scheduling appts now, I think he has the hang of it! I&rsquo;m satisfied now for sure! Thanks so much for the switch!&quot;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                493 => 
                array (
                    'client_id' => 693,
                    'created_at' => '2020-06-05 14:34:45',
                    'deleted_at' => NULL,
                    'id' => 498,
                    'interaction_type' => 'call',
                    'note' => '<p>June 4, 2020</p>

<p>Requested for me just ask OM his billing address so I can help Lyka set up her skype subscription</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                494 => 
                array (
                    'client_id' => 366,
                    'created_at' => '2020-06-08 14:59:31',
                    'deleted_at' => NULL,
                    'id' => 499,
                    'interaction_type' => 'others',
                    'note' => '<p>June 5, 2020</p>

<p>David Rosenthal -&nbsp;Weekly Performance Review for VA Jefte Rubang - Commended VA for having 2 warms leads and 1 important client appointment this week</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'Google Meeting',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Weekly Performance Review',
                    'status' => 'done',
                ),
                495 => 
                array (
                    'client_id' => 177,
                    'created_at' => '2020-06-08 15:01:01',
                    'deleted_at' => NULL,
                    'id' => 500,
                    'interaction_type' => 'call',
                    'note' => '<p>June 5, 2020</p>

<p>She wanted to revisit after a month and discuss her Iryn Ocampo&#39;s annual increase</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                496 => 
                array (
                    'client_id' => 680,
                    'created_at' => '2020-06-08 15:01:58',
                    'deleted_at' => NULL,
                    'id' => 501,
                    'interaction_type' => 'call',
                    'note' => '<p>June 5, 2020</p>

<p>Discussed Chris and Al&#39;s performance so far - she said Chris needs to step up more and be more careful in sending a list of houses to her client so they can avoid getting complaints. Also revised their schedules for tomorrow</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                497 => 
                array (
                    'client_id' => 672,
                    'created_at' => '2020-06-08 17:42:02',
                    'deleted_at' => NULL,
                    'id' => 502,
                    'interaction_type' => 'call',
                    'note' => '<p>Touchbase - feedback on Keishon</p>

<p>&quot;Keishon is fabulous. Complete game changer. He has done exactly what I needed him to do.&quot;</p>

<p>Lindsay is very happy with Keishon.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                498 => 
                array (
                    'client_id' => 682,
                    'created_at' => '2020-06-08 18:11:30',
                    'deleted_at' => NULL,
                    'id' => 503,
                    'interaction_type' => 'email',
                    'note' => '<p>Client happy with VA&#39;s current performance. Looking forward for VA&#39;s progress</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                499 => 
                array (
                    'client_id' => 466,
                    'created_at' => '2020-06-08 18:28:23',
                    'deleted_at' => NULL,
                    'id' => 504,
                    'interaction_type' => 'call',
                    'note' => '<p>Assisted Lisa on Imelda&#39;s transition. Requested codes for tools&nbsp;</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Assist new VA',
                    'status' => 'ongoing',
                ),
            ));
        \DB::table('client_notes')->insert(array (
            0 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-06-08 19:00:22',
                'deleted_at' => NULL,
                'id' => 505,
                'interaction_type' => 'call',
                'note' => '<p>Touch base:</p>

<p>Feedback for Jesa</p>

<p>&quot;She&rsquo;s been doing good. Still have not fully trained her. Overall, she&rsquo;s doing good.&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            1 => 
            array (
                'client_id' => 497,
                'created_at' => '2020-06-08 21:20:09',
                'deleted_at' => NULL,
                'id' => 506,
                'interaction_type' => 'text',
                'note' => '<p>Touch base</p>

<p>feedback on Hanna:&nbsp;</p>

<p>&quot;Last week was great&quot;</p>

<p>&nbsp;</p>

<p>VA still being observed by client</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            2 => 
            array (
                'client_id' => 487,
                'created_at' => '2020-06-08 21:42:01',
                'deleted_at' => NULL,
                'id' => 507,
                'interaction_type' => 'email',
                'note' => '<p>Updated on available leads for Kat and checked if she can cut her hours for today. Agreed to just call nurtured leads.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            3 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-06-08 21:58:16',
                'deleted_at' => NULL,
                'id' => 508,
                'interaction_type' => 'others',
                'note' => '<p>DJ Paris-<br />
Provided positive feedback on the team&#39;s performance. Informed of plans on having an incentive program for the team.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            4 => 
            array (
                'client_id' => 436,
                'created_at' => '2020-06-08 21:59:04',
                'deleted_at' => NULL,
                'id' => 509,
                'interaction_type' => 'email',
                'note' => '<p>Asked to coordinate with her husband for the details on ISA needed. Asked to send email first.</p>

<p>Tim Fraser-Sent email as Julie&#39;s directive to have preference check on additional VA needed. -awaiting response</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            5 => 
            array (
                'client_id' => 698,
                'created_at' => '2020-06-09 12:53:51',
                'deleted_at' => NULL,
                'id' => 510,
                'interaction_type' => 'call',
                'note' => '<p>Meet and greet with Jazzer set expectations tasks workflow and determined schedule. Start date June 9, 2020</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA',
                'status' => 'done',
            ),
            6 => 
            array (
                'client_id' => 695,
                'created_at' => '2020-06-09 15:30:15',
                'deleted_at' => NULL,
                'id' => 511,
                'interaction_type' => 'others',
                'note' => '<p>Sent requested sample buyers and seller scripts.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email, call and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            7 => 
            array (
                'client_id' => 704,
                'created_at' => '2020-06-09 15:31:42',
                'deleted_at' => NULL,
                'id' => 512,
                'interaction_type' => 'others',
                'note' => '<p><br />
First day assistance with his VA. Coordinated on dialer issues.</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => 'email, call and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            8 => 
            array (
                'client_id' => 705,
                'created_at' => '2020-06-09 15:38:18',
                'deleted_at' => NULL,
                'id' => 513,
                'interaction_type' => 'others',
                'note' => '<p><br />
Sent softphone/VOIP options as VA Russell is expected to do some follow-up calls with set appointments. Plans to have Russell into FT soon.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            9 => 
            array (
                'client_id' => 279,
                'created_at' => '2020-06-09 15:40:11',
                'deleted_at' => NULL,
                'id' => 514,
                'interaction_type' => 'others',
                'note' => '<p><br />
Had a conference call with Rhony today. Discussed updates on feedback on Rhony&#39;s tasks.<br />
Coordinated on tasks needed for the additional VA requested.</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            10 => 
            array (
                'client_id' => 508,
                'created_at' => '2020-06-09 18:34:55',
                'deleted_at' => NULL,
                'id' => 515,
                'interaction_type' => 'text',
                'note' => '<p>Touch base</p>

<p>Feedback:&nbsp;Thanks for checking Cheryl. Norleen is always fantastic.&nbsp;&nbsp;Can I add 2 more hours to Norleen&#39;s schedule? I&#39;d like 4 hours on Monday and 4 hours on Friday and keep Tuesday Wednesday and Thursday the same for right now, which is 2 hrs a day</p>

<p>- validating VAs availability -&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            11 => 
            array (
                'client_id' => 707,
                'created_at' => '2020-06-09 22:00:08',
                'deleted_at' => NULL,
                'id' => 516,
                'interaction_type' => 'call',
            'note' => '<p style="list-style-type:disc">Derek doesn&#39;t want his VA (Adrian) to train FRitz during Adrian&#39;s shift with Derek (understandable)</p>

<ul>
<li style="list-style-type:disc">Adrian to train Fritz on his own time</li>
<li style="list-style-type:disc">VPN - needs to be provided by Swapna for VA to access site&nbsp;</li>
<li style="list-style-type:disc">Willing to pay for it - will provide bank info tom.</li>
</ul>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            12 => 
            array (
                'client_id' => 628,
                'created_at' => '2020-06-09 22:07:26',
                'deleted_at' => NULL,
                'id' => 517,
                'interaction_type' => 'text',
                'note' => '<p>Annaliease gave Shannon a paid day off today</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Validation',
                'status' => 'done',
            ),
            13 => 
            array (
                'client_id' => 656,
                'created_at' => '2020-06-09 23:16:45',
                'deleted_at' => NULL,
                'id' => 518,
                'interaction_type' => 'others',
                'note' => '<p>Rescheduled m&amp;g tomorrow at 9am PST</p>

<p>&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            14 => 
            array (
                'client_id' => 675,
                'created_at' => '2020-06-09 23:34:16',
                'deleted_at' => NULL,
                'id' => 519,
                'interaction_type' => 'call',
                'note' => '<p>June 8, 2020</p>

<p>Touchbase - discussed VA Kenah Lequigan&#39;s performance. Kenah hasn&#39;t set any appointments yet. Catherine said that she needs to be more aggressive. Also discussed her attendance. She said she wants to give her a few more days to prove herself and then we&#39;ll explore the idea of having her replaced if she doesn&#39;t improve</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            15 => 
            array (
                'client_id' => 693,
                'created_at' => '2020-06-09 23:35:28',
                'deleted_at' => NULL,
                'id' => 520,
                'interaction_type' => 'email',
                'note' => '<p>June 8, 2020</p>

<p>He requested to change Lyka&#39;s schedule to 5hrs/day, 4days per week:<br />
New Schedule:<br />
Mon, Wed, Thur 10AM - 3PM &amp; Tues 9:15AM - 2:15PM<br />
VA will be on RD Friday, Saturday &amp; Sunday<br />
<br />
We also discussed about his referral Lee Kouri - he is happy to know that he will have a $150 referral fee once Lee signs up. He just wants it to be taken off from his next invoice</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Schedule Change',
                'status' => 'done',
            ),
            16 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-06-09 23:36:41',
                'deleted_at' => NULL,
                'id' => 521,
                'interaction_type' => 'call',
                'note' => '<p>June 8, 2020</p>

<p>He said we can revisit the discussion about Gracee&#39;s yearly increase after 90 days - their finances are just picking up and he doesn&#39;t want to make any financial mistakes at this moment so he doesn&#39;t want to commit yet</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            17 => 
            array (
                'client_id' => 680,
                'created_at' => '2020-06-09 23:37:40',
                'deleted_at' => NULL,
                'id' => 522,
                'interaction_type' => 'call',
                'note' => '<p>June 8, 2020</p>

<p>Expressed concerns about her VAs - she said she&#39;s not closing any deals from them and they&#39;re not making enough searches for their clients:<br />
<br />
I informed her that I am monitoring our GC in WhatsApp with Chris &amp; Al. Basing it on their correspondences there, the VAs are doing their job well. I can see that they are diligently calling their clients and the house owners to make sure that the properties they send out are still available for rent. the VAs are also reporting to me every time they have closed deals.<br />
<br />
Resolution: Explained to her that the VAs are fairly new and there&#39;s a learning curve so she can&#39;t compare them to her other VA outside VD that&#39;s already with her for more than 2yrs now. I assured her that Chris and Al are working diligently and we will have a conference call tomorrow morning for calibration and just so we can make sure that we are all on the same page</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            18 => 
            array (
                'client_id' => 605,
                'created_at' => '2020-06-09 23:41:16',
                'deleted_at' => NULL,
                'id' => 523,
                'interaction_type' => 'others',
                'note' => '<p>Additional hours for VA JF , preferred schedule , 10am-4pm PST</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => 'Call text',
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional Hours',
                'status' => 'done',
            ),
            19 => 
            array (
                'client_id' => 666,
                'created_at' => '2020-06-09 23:44:38',
                'deleted_at' => NULL,
                'id' => 524,
                'interaction_type' => 'call',
                'note' => '<p>Shekah is doing a great job , been a month and he&#39;s happy with her performance</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            20 => 
            array (
                'client_id' => 704,
                'created_at' => '2020-06-10 16:34:32',
                'deleted_at' => NULL,
                'id' => 525,
                'interaction_type' => 'others',
                'note' => '<p>Confirmed weekend shifts and number of hours completed by VA Brianna. Provided positive feedback on her performance.<br />
&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            21 => 
            array (
                'client_id' => 716,
                'created_at' => '2020-06-10 16:39:02',
                'deleted_at' => NULL,
                'id' => 526,
                'interaction_type' => 'call',
                'note' => '<p>Identified tasks and tools also discussed work flow and schedule preferred&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and greet',
                'status' => 'done',
            ),
            22 => 
            array (
                'client_id' => 466,
                'created_at' => '2020-06-10 16:42:08',
                'deleted_at' => NULL,
                'id' => 527,
                'interaction_type' => 'call',
                'note' => '<p>Notified of the process of transition. Harvey will have an SME session up until Friday to assist Imee with her tasks</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            23 => 
            array (
                'client_id' => 719,
                'created_at' => '2020-06-10 16:51:05',
                'deleted_at' => NULL,
                'id' => 528,
                'interaction_type' => 'others',
                'note' => '<p>Current VA is Sara Jane Martinez</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => 'M&G',
                'others_status' => 'Please specify status type',
                'others_type' => 'No Replacement has been made',
                'status' => 'done',
            ),
            24 => 
            array (
                'client_id' => 705,
                'created_at' => '2020-06-10 17:19:28',
                'deleted_at' => NULL,
                'id' => 529,
                'interaction_type' => 'others',
            'note' => '<p>Provided positive feedback on VA Russel&#39;s performance in his first week. Request for invoice and payment arrangement (weekly) as what was previously discussed with James.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            25 => 
            array (
                'client_id' => 695,
                'created_at' => '2020-06-10 17:21:00',
                'deleted_at' => NULL,
                'id' => 530,
                'interaction_type' => 'email',
                'note' => '<p>Sent requested scripts for review and forwarded VOIP/Softphone recommendation as the initialled does not work on VA Josh&#39;s end.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            26 => 
            array (
                'client_id' => 279,
                'created_at' => '2020-06-10 17:21:47',
                'deleted_at' => NULL,
                'id' => 531,
                'interaction_type' => 'others',
                'note' => '<p><br />
Forwarded 3 CVs to review for additional VA requests.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            27 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-06-10 17:22:47',
                'deleted_at' => NULL,
                'id' => 532,
                'interaction_type' => 'email',
                'note' => '<p>DJ Paris-Informed of bonus per set appointments of all VAs. Will coordinate on the process.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            28 => 
            array (
                'client_id' => 559,
                'created_at' => '2020-06-10 17:25:17',
                'deleted_at' => NULL,
                'id' => 533,
                'interaction_type' => 'email',
                'note' => '<p>Timothy Smith-Coordinated on his request for VA Lady to forward all invoices from last week.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            29 => 
            array (
                'client_id' => 675,
                'created_at' => '2020-06-11 00:43:41',
                'deleted_at' => NULL,
                'id' => 534,
                'interaction_type' => 'call',
                'note' => '<p>June 9, 2020</p>

<p>(Pending replacement) Reported the issue about her VA Kenah Lequigan - informed her that I don&#39;t think it&#39;s productive to keep Kenah anymore. We discussed replacement for her. She wanted for me to look for the guy that she interviewed with Kenah before and to send her profiles tomorrow to interview</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Replacement',
                'status' => 'done',
            ),
            30 => 
            array (
                'client_id' => 680,
                'created_at' => '2020-06-11 00:44:43',
                'deleted_at' => NULL,
                'id' => 535,
                'interaction_type' => 'call',
                'note' => '<p>June 9, 2020</p>

<p>Discussed issues with her VAs Al &amp; Chris - complaining that they are doing 10 calls to property owners for rentals per day.<br />
<br />
Explained to her and assured her again that her VAs are working. I told her that I instructed Al &amp; Chris to attach the list and screenshot of their call logs from GVoice on their EODs as proof.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            31 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-06-11 00:45:45',
                'deleted_at' => NULL,
                'id' => 536,
                'interaction_type' => 'others',
                'note' => '<p>June 9, 2020</p>

<p>Touchbase about her new hire Louize Castro - confirmed that he just got her RC account (phone number) that she&#39;s going to use for calling. He assigned some more admin tasks for Louize to finish this week and she can start dialing out on Monday next week</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'HangOuts',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            32 => 
            array (
                'client_id' => 652,
                'created_at' => '2020-06-11 00:46:40',
                'deleted_at' => NULL,
                'id' => 537,
                'interaction_type' => 'email',
                'note' => '<p>June 9, 2020</p>

<p>She requested to have VA Michelle Santiago&#39;s schedule be changed to:<br />
<br />
11AM - 3PM PST<br />
<br />
Tasks are more consistent in the afternoon that in the mornings. She is very satisfied with Michelle&#39;s performance and hardwork.</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Schedule Change',
                'status' => 'done',
            ),
            33 => 
            array (
                'client_id' => 690,
                'created_at' => '2020-06-11 00:48:42',
                'deleted_at' => NULL,
                'id' => 538,
                'interaction_type' => 'others',
                'note' => '<p>June 9, 2020</p>

<p>Approved VA Charmaen Espina&#39;s absence for today due to sickness - informed him that if the VA won&#39;t still be able to work tomorrow, I will require her to submit a medical certificate</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            34 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-06-11 00:49:35',
                'deleted_at' => NULL,
                'id' => 539,
                'interaction_type' => 'call',
                'note' => '<p>June 9, 2020</p>

<p>Touchbase - he said that he&#39;s very busy because of the appointments that Gracee &amp; Charlyn are setting for him. He&#39;s very happy and her 2 VAs are doing a good job. He said he might add another VA soon for Admin tasks</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            35 => 
            array (
                'client_id' => 428,
                'created_at' => '2020-06-11 12:28:11',
                'deleted_at' => NULL,
                'id' => 540,
                'interaction_type' => 'others',
                'note' => '<p>Checked the buy out process. Decided to stay and approved&nbsp; $.50 annual increase.&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => 'Buy out',
                'status' => 'acknowleged',
            ),
            36 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-06-11 17:45:11',
                'deleted_at' => NULL,
                'id' => 541,
                'interaction_type' => 'email',
                'note' => '<p>DJ Paris-Checked availability to discuss bonus process for the VAs.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            37 => 
            array (
                'client_id' => 705,
                'created_at' => '2020-06-11 17:50:47',
                'deleted_at' => NULL,
                'id' => 542,
                'interaction_type' => 'others',
                'note' => '<p><br />
Coordinated on her request to reset Timedly password. Provided walk-through process.<br />
&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'call, text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            38 => 
            array (
                'client_id' => 691,
                'created_at' => '2020-06-11 18:17:10',
                'deleted_at' => NULL,
                'id' => 543,
                'interaction_type' => 'call',
            'note' => '<p>Client asking about his invoice. Timedly attached video, has invoice in it while our current Timedly does not. Raised question to OM, OM forwarded the invoice to client. Client has another question about taxes. Explained that&nbsp;We are a Washington State company and we are required to pay B&amp;O (Business and occupation tax)</p>',
                'note_type' => 'consultation',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            39 => 
            array (
                'client_id' => 436,
                'created_at' => '2020-06-11 22:57:25',
                'deleted_at' => NULL,
                'id' => 544,
                'interaction_type' => 'others',
            'note' => '<p>Tim Fraser-Preference check on additional VA (ISA) request.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            40 => 
            array (
                'client_id' => 685,
                'created_at' => '2020-06-11 23:42:32',
                'deleted_at' => NULL,
                'id' => 545,
                'interaction_type' => 'call',
                'note' => '<p>Ted has expressed that he will only finish the contract due to finances</p>

<p>(enquired of add&#39;l discount last week )</p>

<p>Save offers:</p>

<p>offered GVA services</p>

<p>unable to give more discounts as he is currently on a $1 discount/ hour.</p>

<p>End of Contract is June 15, 2020</p>

<p>Advd that VA&#39;s last day is on Sat - June 13, 2020</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            41 => 
            array (
                'client_id' => 659,
                'created_at' => '2020-06-12 13:28:26',
                'deleted_at' => NULL,
                'id' => 546,
                'interaction_type' => 'email',
                'note' => '<p>Client follow up on cancellation form. Endorsed to OM for docusign submission</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            42 => 
            array (
                'client_id' => 633,
                'created_at' => '2020-06-12 16:45:00',
                'deleted_at' => NULL,
                'id' => 547,
                'interaction_type' => 'call',
                'note' => '<p>Clarified the bill breakdown for this cut off. Informed client to reach direct line for future use.</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Billing',
                'status' => 'acknowleged',
            ),
            43 => 
            array (
                'client_id' => 657,
                'created_at' => '2020-06-12 16:53:08',
                'deleted_at' => NULL,
                'id' => 548,
                'interaction_type' => 'email',
                'note' => '<p>Touch base request sent awaiting for response</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            44 => 
            array (
                'client_id' => 669,
                'created_at' => '2020-06-12 17:11:43',
                'deleted_at' => NULL,
                'id' => 549,
                'interaction_type' => 'call',
                'note' => '<p>June 11, 2020</p>

<p>Faria wants Bea to go on FT</p>

<p>Effectivity date - next week or week after next. Will be given an update&nbsp;</p>

<ul>
<li style="list-style-type:disc">Needs a lead manager - really likes working with Bea</li>
<li style="list-style-type:disc">Gets whatever is asked to do most of the time</li>
<li style="list-style-type:disc">Familiarized with CRM already&nbsp;</li>
<li style="list-style-type:disc">Advd of FT rate, once VA starts working 8hrs/day</li>
</ul>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            45 => 
            array (
                'client_id' => 635,
                'created_at' => '2020-06-12 17:25:33',
                'deleted_at' => NULL,
                'id' => 550,
                'interaction_type' => 'call',
                'note' => '<p>June 11, 2020</p>

<p>David is very busy that he is thinking of putting svcs on Hold for 1 or 2 wks&nbsp;as he needs to catch up with his listings and appointments</p>

<p>he would want Rich to start calling REAs/ Brokers</p>

<p>Advd that Rich can do admin work/ soc med marketing, etc</p>

<p>Sent tasks that our VAs can do and respective rates&nbsp;</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            46 => 
            array (
                'client_id' => 686,
                'created_at' => '2020-06-12 17:30:35',
                'deleted_at' => NULL,
                'id' => 551,
                'interaction_type' => 'call',
                'note' => '<p>June 11, 2020</p>

<p>Touch base -&nbsp;</p>

<ul>
<li style="list-style-type:disc">Feedback - she&rsquo;s alright - has set up about 4 appts (may have not seen latest EOD yet - total of 6 appts)</li>
<li style="list-style-type:disc">She&rsquo;s doing FSBOs and expireds right now</li>
<li style="list-style-type:disc">Got another ISA who calls leads that comes in to Mojo</li>
<li style="list-style-type:disc">she assigned Charlyn to&nbsp;Calling FSBOs and Expireds.</li>
<li style="list-style-type:disc">MLS - wants to know if she VA knows how to input info in MLS</li>
</ul>

<p style="list-style-type:disc">** Advd Toni, that Charlyn can farm leads in MLS</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            47 => 
            array (
                'client_id' => 683,
                'created_at' => '2020-06-12 23:50:43',
                'deleted_at' => NULL,
                'id' => 552,
                'interaction_type' => 'text',
                'note' => '<p>June 10, 2020</p>

<p>Feedback:</p>

<p>&quot;Irish is doing great.&quot;</p>

<p>Query on&nbsp;any other scripts to use for lead generation during coaching to gain more qualified listings?</p>

<p>To be discussed on phone appt on Monday, June 15 10AM EST</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            48 => 
            array (
                'client_id' => 296,
                'created_at' => '2020-06-16 02:30:39',
                'deleted_at' => NULL,
                'id' => 553,
                'interaction_type' => 'others',
                'note' => '<p>Additional hours for VA Giselle effective June 16, 2020</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => 'Call, email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            49 => 
            array (
                'client_id' => 170,
                'created_at' => '2020-06-16 14:40:18',
                'deleted_at' => NULL,
                'id' => 554,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Request to suspend service (will be out of the country for 30days) Set proper expectations and forwarded clients concerns on Louize dials</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            50 => 
            array (
                'client_id' => 653,
                'created_at' => '2020-06-16 14:41:11',
                'deleted_at' => NULL,
                'id' => 555,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Touch base on VA&#39;s performance and she is very much satisfied with Norleen&#39;s performance</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            51 => 
            array (
                'client_id' => 660,
                'created_at' => '2020-06-16 14:41:55',
                'deleted_at' => NULL,
                'id' => 556,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Concerns regarding the $1 less promo / verifying the contract signed</p>',
                'note_type' => 'consultation',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            52 => 
            array (
                'client_id' => 679,
                'created_at' => '2020-06-16 15:06:39',
                'deleted_at' => NULL,
                'id' => 557,
                'interaction_type' => 'call',
                'note' => '<p>Previous VA needed to transfer homes where internet is not available. VA resigned and offer a replacement to Gusty. Client agreed and VA was replaced.</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Replacement',
                'status' => 'done',
            ),
            53 => 
            array (
                'client_id' => 436,
                'created_at' => '2020-06-16 15:43:51',
                'deleted_at' => NULL,
                'id' => 558,
                'interaction_type' => 'email',
                'note' => '<p><br />
Update on profiles to be sent for review.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            54 => 
            array (
                'client_id' => 274,
                'created_at' => '2020-06-16 15:45:33',
                'deleted_at' => NULL,
                'id' => 559,
                'interaction_type' => 'others',
                'note' => '<p>Provided tasks that will be assigned for VA Aldrin on TBS.<br />
&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            55 => 
            array (
                'client_id' => 725,
                'created_at' => '2020-06-16 15:46:29',
                'deleted_at' => NULL,
                'id' => 560,
                'interaction_type' => 'others',
                'note' => '<p><br />
Joined first half of the training/onboarding with his VA.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            56 => 
            array (
                'client_id' => 538,
                'created_at' => '2020-06-16 15:47:14',
                'deleted_at' => NULL,
                'id' => 561,
                'interaction_type' => 'others',
                'note' => '<p>First day assistance on tasks and checking of systems and tools.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            57 => 
            array (
                'client_id' => 683,
                'created_at' => '2020-06-16 15:58:11',
                'deleted_at' => NULL,
                'id' => 562,
                'interaction_type' => 'call',
                'note' => '<p>June 15, 2020</p>

<p>Requested for other scripts that Irish could use to help her set up appointments</p>

<p>=======</p>

<p>June 16, 2020</p>

<p>Updated that review on scripts done with Irish and Role play (reversal) done as well.</p>

<p>VA was able to book an appt&nbsp;</p>

<p>Also approved to mine in Zillow once VA has exhausted leads in Redx</p>

<p>mode of Comms: call/ text</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            58 => 
            array (
                'client_id' => 712,
                'created_at' => '2020-06-16 16:03:00',
                'deleted_at' => NULL,
                'id' => 563,
                'interaction_type' => 'call',
                'note' => '<p>Client was threatened that we might charge his card $10,000 because of the statement added on the contract that we&#39;ll charge them if they absorb the VA. Offered cancellation of contract, however, client doesn&#39;t want to cancel. Will assist the VA on agent locator, social media posting and scripting.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            59 => 
            array (
                'client_id' => 686,
                'created_at' => '2020-06-16 16:20:40',
                'deleted_at' => NULL,
                'id' => 564,
                'interaction_type' => 'call',
                'note' => '<p>Advised Toni of Charlyn&#39;s absence today due to Family Emergency.</p>

<p>Confirmed that she isn&#39;t charged for today.</p>

<p>====</p>

<p>Advd Toni that VA Charlyn will not be able to report for a week due to the Domestic/ Family issue as she has to transfer fo another location and have inet, etc settled.</p>

<p>Toni agreed/ approved for as long as she isn&#39;t getting charged.</p>

<p>VA will be back by Tuesday. next week, June 23, 2020</p>

<p>=====</p>

<p>June 25, 2020</p>

<p>Toni agreed to interview Mace - only applicant at the moment. Sched of interview is 11:30AM EST</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            60 => 
            array (
                'client_id' => 715,
                'created_at' => '2020-06-16 16:27:39',
                'deleted_at' => NULL,
                'id' => 565,
                'interaction_type' => 'call',
                'note' => '<p>June 15, 2020</p>

<p>He wants to have someone with experience in Canada RE Contracts and with Managerial experience because he wants his VA to manage his whole operations - he said he is willing to wait for the replacement</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Replacement',
                'status' => 'done',
            ),
            61 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-06-16 16:28:41',
                'deleted_at' => NULL,
                'id' => 566,
                'interaction_type' => 'others',
                'note' => '<p>June 15, 2020</p>

<p>Agreed to transition Louize Castro from PT to FT - we scheduled another call tomorrow to discuss her FT schedule. He said he is busy today because he&#39;s onboarding new agents for Erik Stewart Group</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'HangOuts',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            62 => 
            array (
                'client_id' => 690,
                'created_at' => '2020-06-16 16:30:14',
                'deleted_at' => NULL,
                'id' => 567,
                'interaction_type' => 'others',
                'note' => '<p>June 15, 2020</p>

<p>Collection Call - he couldn&#39;t pay for the $468.66 pending invoice yet. He decided to cancel his contract already because he doesn&#39;t have funds anymore.</p>',
                'note_type' => 'collection',
                'noted_by' => 19,
            'others_interaction' => 'Call (WhatsApp)',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            63 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-06-16 16:32:08',
                'deleted_at' => NULL,
                'id' => 568,
                'interaction_type' => 'others',
                'note' => '<p>June 15, 2020</p>

<p>Informed him that one of his ISA, Villa already submitted her resignation - assured him that I will find a replacement for her or better yet we can have Zoe on FT if that&#39;s okay with him. We are just waiting for Zoe&#39;s confirmation if she&#39;s going to be available for FT</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Google Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            64 => 
            array (
                'client_id' => 672,
                'created_at' => '2020-06-16 16:32:27',
                'deleted_at' => NULL,
                'id' => 569,
                'interaction_type' => 'email',
                'note' => '<p>Lindsay has requested to lessen hours from 20 hrs/ week to 15 hrs/ week.</p>

<p>Avsd her that it we can do her request but we need to know length of time (waiting for response)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            65 => 
            array (
                'client_id' => 224,
                'created_at' => '2020-06-16 16:33:00',
                'deleted_at' => NULL,
                'id' => 570,
                'interaction_type' => 'email',
                'note' => '<p>June 15, 2020</p>

<p>He requested to have the hold on the services extended for 2 more weeks because they are slowing opening up in Pennsylvania and he will be back soon</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            66 => 
            array (
                'client_id' => 710,
                'created_at' => '2020-06-17 13:23:15',
                'deleted_at' => NULL,
                'id' => 571,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Touch base with her VA&#39;s performance . Satisfied with Toni and they are getting there</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            67 => 
            array (
                'client_id' => 698,
                'created_at' => '2020-06-17 13:23:47',
                'deleted_at' => NULL,
                'id' => 572,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Touch base with her VA&#39;s performance and they are very happy with the VA&#39;s performance (VA is very dependable)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            68 => 
            array (
                'client_id' => 733,
                'created_at' => '2020-06-17 13:25:39',
                'deleted_at' => NULL,
                'id' => 573,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Meet and greet with VA Christine / discussed schedule / showed how his system works / first day instructions</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and greet',
                'status' => 'done',
            ),
            69 => 
            array (
                'client_id' => 724,
                'created_at' => '2020-06-17 13:37:44',
                'deleted_at' => NULL,
                'id' => 574,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Meet and greet with VA Jester / discussed schedule / showed how her system works / first day instructions / Discussed Timedly</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'meet and greet',
                'status' => 'done',
            ),
            70 => 
            array (
                'client_id' => 699,
                'created_at' => '2020-06-17 17:27:29',
                'deleted_at' => NULL,
                'id' => 575,
                'interaction_type' => 'email',
                'note' => '<p><br />
Informed of VA Josh&#39;s medical emergency. Checked availability for a short call to discuss updates/feedback.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            71 => 
            array (
                'client_id' => 725,
                'created_at' => '2020-06-17 17:28:42',
                'deleted_at' => NULL,
                'id' => 576,
                'interaction_type' => 'others',
                'note' => '<p>Updated on RC set-up for VA Ella.</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            72 => 
            array (
                'client_id' => 695,
                'created_at' => '2020-06-17 17:30:14',
                'deleted_at' => NULL,
                'id' => 577,
                'interaction_type' => 'email',
                'note' => '<p>Approved of MUS schedule for VA Josh on his absence today.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            73 => 
            array (
                'client_id' => 696,
                'created_at' => '2020-06-17 20:12:23',
                'deleted_at' => NULL,
                'id' => 578,
                'interaction_type' => 'text',
                'note' => '<p>so far everything is great</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            74 => 
            array (
                'client_id' => 652,
                'created_at' => '2020-06-18 00:22:22',
                'deleted_at' => NULL,
                'id' => 579,
                'interaction_type' => 'call',
                'note' => '<p>June 16, 2020</p>

<p>Touchbase - gave feedback and commendation for her VA Michelle Santiago:<br />
<br />
&quot;Oh my gosh! I love her. She&#39;s really good and I felt like I could just let her do a self learning and I sent her things I needed. She just picks up quickly and give me whatever I needed. And those times that I couldn&#39;t train her she just did it herself. I felt like she was actually what I was looking for so thank you so much for finding her. I have less stress now and I feel like there&#39;s someone to provide support if I need it. It&#39;s been going really, really well and I am very satisfied.&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            75 => 
            array (
                'client_id' => 224,
                'created_at' => '2020-06-18 00:23:07',
                'deleted_at' => NULL,
                'id' => 580,
                'interaction_type' => 'email',
                'note' => '<p>June 16, 2020</p>

<p>Confirmed the extension on the hold of the services - informed him that his expected resumption will be on June 29, 2020</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            76 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-06-18 00:24:04',
                'deleted_at' => NULL,
                'id' => 581,
                'interaction_type' => 'others',
                'note' => '<p>June 16, 2020</p>

<p>Discussed Louize Castro&#39;s FT schedule starting tomorrow - he approved Louize&#39;s preferred schedule of 9AM - 5PM EST, Mon - Fri</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'HangOuts',
                'others_status' => 'Please specify status type',
                'others_type' => 'Upsell',
                'status' => 'done',
            ),
            77 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-06-18 00:25:39',
                'deleted_at' => NULL,
                'id' => 582,
                'interaction_type' => 'others',
                'note' => '<p>June 16, 2020</p>

<p>Matt Zeigler (his Office Manager represented him today) - weekly performance review for VA Zoe Guico. Addressed some concerns of agents that they&#39;re recruiting (about COVID). Also announce that they have acquired a small firm in Manhattan as well. Listened to 3 calls of Zoe this week and they were all good as per Matt</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'Google Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            78 => 
            array (
                'client_id' => 734,
                'created_at' => '2020-06-18 00:30:39',
                'deleted_at' => NULL,
                'id' => 583,
                'interaction_type' => 'others',
                'note' => '<p>June 16, 2020</p>

<p>Meet &amp; Greet today with VAs Val Angelo Puedan (ISA) &amp; Joseph Benjamin (TC)<br />
<br />
Angelo:<br />
Start Date: June 17, 2020<br />
Schedule: Wed - Fri 10:30AM - 7:30PM EST &amp; Sat - Sun 9AM - 6PM EST<br />
CRM/Tools: AppFiles | Follow-Up Boss | Ylopo<br />
System Log Ins: will be sent via email<br />
Training: will be done by Kaitie<br />
<br />
Joseph:<br />
Start Date: June 22, 2020<br />
Schedule: Mon - Fri 9AM - 6PM EST<br />
CRM/Tools: AppFiles | Jive | Follow-Up Boss | Ylopo<br />
System Log ins: will be sent via email<br />
Training: will be done by Donna</p>',
                'note_type' => 'others',
                'noted_by' => 19,
                'others_interaction' => 'RC Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet & Greet',
                'status' => 'done',
            ),
            79 => 
            array (
                'client_id' => 715,
                'created_at' => '2020-06-18 00:31:50',
                'deleted_at' => NULL,
                'id' => 584,
                'interaction_type' => 'call',
                'note' => '<p>June 16, 2020</p>

<p>He decided to move forward with Monique but as a GVA instead of EVA - he would just assigned Social Media Management and Marketing tasks to her. She will start on Thursda, June 18, 2020v</p>',
                'note_type' => 'followup',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            80 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-06-18 00:32:36',
                'deleted_at' => NULL,
                'id' => 585,
                'interaction_type' => 'text',
                'note' => '<p>June 16, 2020</p>

<p>Informed him that his VA Charlyn Hernando is requesting for 1 week leave due to family issues and relocation - he said he is busy today and would like to discuss tomorrow</p>',
                'note_type' => 'notification',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            81 => 
            array (
                'client_id' => 712,
                'created_at' => '2020-06-18 22:16:38',
                'deleted_at' => NULL,
                'id' => 586,
                'interaction_type' => 'call',
                'note' => '<p>Called client to touch-base. Currently showing home. Will call back&nbsp;tomorrow morning.</p>

<p>&nbsp;</p>

<p>-Was able to call the client to touchbase.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            82 => 
            array (
                'client_id' => 671,
                'created_at' => '2020-06-18 22:31:46',
                'deleted_at' => NULL,
                'id' => 587,
                'interaction_type' => 'others',
                'note' => '<p>Texted and emailed Ronnie for VA&#39;s tools and VOIP. Client was able to setup Mojo, however, it doesn&#39;t have leads and VA is still not able to work properly. Informed client that VA is always logging-in on her assigned shift and we are continuously billing him for that. Concerned about his charges that are still going on, while our VA cannot provide him a full service because of incomplete tools.</p>',
                'note_type' => 'followup',
                'noted_by' => 29,
                'others_interaction' => 'Called-VM/Texted/Emailed',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            83 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-06-18 22:50:54',
                'deleted_at' => NULL,
                'id' => 588,
                'interaction_type' => 'others',
                'note' => '<p>Client Interview&nbsp;</p>

<p>Rizza Vejerano, Jen Marzan</p>

<p>Charlene Hilario and David Laxina</p>

<p>===</p>

<p>Adilah has chosen David Laxina for add&#39;l ISA - for her client (2 hr shift)</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => 'Ring Central Meetings',
                'others_status' => 'Please specify status type',
            'others_type' => 'CI for Additional ISA (2 hr shifts)',
                'status' => 'done',
            ),
            84 => 
            array (
                'client_id' => 662,
                'created_at' => '2020-06-18 23:06:40',
                'deleted_at' => NULL,
                'id' => 589,
                'interaction_type' => 'others',
                'note' => '<p>Interview for additional EVA</p>

<p>Nina - fresh from Final interview</p>

<p>Vernadette - EVA 20</p>

<p>=====</p>

<p>Waiting for Final Decision from Andrew</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => 'Ring Central Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'CI for Additional EVA',
                'status' => 'pending',
            ),
            85 => 
            array (
                'client_id' => 712,
                'created_at' => '2020-06-19 22:19:07',
                'deleted_at' => NULL,
                'id' => 590,
                'interaction_type' => 'call',
                'note' => '<p>Client is very happy with the VA. As per client, VA is doing a great job. Looking for more opportunities to help him touchbase with his clients. Suggested that I will ask the VA to compose text messages and email to nurture the leads. Will also be working on his social media accounts and instagram.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            86 => 
            array (
                'client_id' => 698,
                'created_at' => '2020-06-22 12:05:44',
                'deleted_at' => NULL,
                'id' => 591,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Concerns on quickbooks expect that could help May. Coordinated request with someone with quickbooks experience</p>',
                'note_type' => 'consultation',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            87 => 
            array (
                'client_id' => 738,
                'created_at' => '2020-06-22 12:06:23',
                'deleted_at' => NULL,
                'id' => 592,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Meet and greet with Aran discussed preferred schedule and target start date / Tools and tasks discussed / Introduced timedly and how to use it</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and greet',
                'status' => 'done',
            ),
            88 => 
            array (
                'client_id' => 274,
                'created_at' => '2020-06-22 18:28:36',
                'deleted_at' => NULL,
                'id' => 593,
                'interaction_type' => 'others',
                'note' => '<p><br />
Confirmed TBS project with Aldrin. Start date and schedules discussed.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            89 => 
            array (
                'client_id' => 478,
                'created_at' => '2020-06-22 18:29:40',
                'deleted_at' => NULL,
                'id' => 594,
                'interaction_type' => 'text',
                'note' => '<p><br />
Confirmed call schedule on Monday at 2pm (PST) to discuss other options.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            90 => 
            array (
                'client_id' => 538,
                'created_at' => '2020-06-22 18:30:54',
                'deleted_at' => NULL,
                'id' => 595,
                'interaction_type' => 'others',
                'note' => '<p><br />
Meet and greet with VA Richelle today. Confirmed start date and schedule. Set expectations on tasks, tools and systems to use.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            91 => 
            array (
                'client_id' => 730,
                'created_at' => '2020-06-22 18:31:55',
                'deleted_at' => NULL,
                'id' => 596,
                'interaction_type' => 'text',
                'note' => '<p><br />
Request to move start date on June 24.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            92 => 
            array (
                'client_id' => 279,
                'created_at' => '2020-06-22 18:32:38',
                'deleted_at' => NULL,
                'id' => 597,
                'interaction_type' => 'email',
                'note' => '<p>Approved recommended schedule for VA Rhony starting June 26.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            93 => 
            array (
                'client_id' => 650,
                'created_at' => '2020-06-22 19:32:02',
                'deleted_at' => NULL,
                'id' => 598,
                'interaction_type' => 'call',
                'note' => '<p>Latishia called about the resumption of VA services. Discussed TB, she will just talk to her husband and will get back to me today on if she will resume to PT or switch to TB.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            94 => 
            array (
                'client_id' => 51,
                'created_at' => '2020-06-22 19:39:40',
                'deleted_at' => NULL,
                'id' => 599,
                'interaction_type' => 'others',
                'note' => '<p>Touchbase request , awaiting response</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            95 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-06-23 22:41:31',
                'deleted_at' => NULL,
                'id' => 600,
                'interaction_type' => 'call',
                'note' => '<p>Meet and Greet with Dave and Ken - Adilah&#39;s client on June 22, 2020</p>

<p>- Schedule - M-F 10am-12nn EST</p>

<p>- Effectivity - June 29, 2020 Estimated&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p>CRM Tools - Mojo dialer, if we can set up appts.&nbsp;</p>

<p>- System Logins - to be sent via email</p>

<p>- Expectations - focus on lead generation and ff up/ personal info on residences and manager will go to location.</p>

<p>Creating system on lead generation and ff up</p>

<p>- Leads to call - readily available&nbsp;</p>

<p>- Selling points/Track record/Fact sheet - available/ Template, run a system</p>

<p>- Scripts - scripts to use</p>

<p>- Calendar - shared</p>

<p>- Mode of Communication - WhatsApp/ Skype</p>

<p>Will be sending David&#39;s contact information to Adilah</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            96 => 
            array (
                'client_id' => 662,
                'created_at' => '2020-06-23 22:51:57',
                'deleted_at' => NULL,
                'id' => 601,
                'interaction_type' => 'text',
                'note' => '<p>June 22, 2020</p>

<p>Day 1 of Nina&nbsp;</p>

<p>followed up on log in details, etc</p>

<p>Was advised that Jem will do the Onboarding</p>

<p>Connected both VAs for Onboarding.</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            97 => 
            array (
                'client_id' => 680,
                'created_at' => '2020-06-23 23:46:13',
                'deleted_at' => NULL,
                'id' => 602,
                'interaction_type' => 'others',
                'note' => '<p>June 22, 2020</p>

<p>Complaining about VAs Al and Chris again - she said their inefficient.<br />
<br />
Explained to her that I am constantly monitoring the VAs call logs and their emails and house searches. I am also informed once there&#39;s a deal that closed and also if there are clients that are hard to reach. Explained to her that we have assigned working hours for the VAs and I noticed that sometimes she&#39;s messaging them on GC in whatsapp way after their working hours.<br />
<br />
In the end she didn&#39;t cancel on Chris and just wants him to focus on calls, emails and texts and Al will be the one doing house rental searches for their clients (Chris will be the one confirming their availability).</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            98 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-06-23 23:46:53',
                'deleted_at' => NULL,
                'id' => 603,
                'interaction_type' => 'text',
                'note' => '<p>June 22, 2020</p>

<p>Reached out to him about VA Charlyn Hernando&#39;s intent to resign - he said he&#39;s aware because Charlyn messaged him. He&#39;s busy today because he has an event to attend to and we will talk tomorrow</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            99 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-06-23 23:47:37',
                'deleted_at' => NULL,
                'id' => 604,
                'interaction_type' => 'email',
                'note' => '<p>June 22, 2020</p>

<p>Matt Zeigler -&nbsp;Adam Mahfouda&#39;s Office Manager - following up on the replacement VA for Villa Candong. Sent JC Reyes&#39; updated CV. Informed him that JC has experience in doing recruiting calls for Jet Closing</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            100 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-06-23 23:48:34',
                'deleted_at' => NULL,
                'id' => 605,
                'interaction_type' => 'email',
                'note' => '<p>June 22, 2020</p>

<p>Confirmed of having Kavin Suan back to FT after June 30th - they are now venturing on property management so he has a lot of things right now to keep Kavin busy</p>',
                'note_type' => 'touchbase',
                'noted_by' => 19,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            101 => 
            array (
                'client_id' => 362,
                'created_at' => '2020-06-24 16:07:49',
                'deleted_at' => NULL,
                'id' => 606,
                'interaction_type' => 'email',
                'note' => '<p>Discussed usual price of decent laptops that his VA could purchase and incentive process&nbsp;</p>

<hr />
<p>Provided $500 (for VA to buy a new Laptop)</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Incentive',
                'status' => 'acknowleged',
            ),
            102 => 
            array (
                'client_id' => 576,
                'created_at' => '2020-06-24 16:09:20',
                'deleted_at' => NULL,
                'id' => 607,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Resumption of service / Discussed working hours and checked if there is any documents that he needs to sign.&nbsp; Service to resume on June 29th</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Resumption of Service',
                'status' => 'acknowleged',
            ),
            103 => 
            array (
                'client_id' => 663,
                'created_at' => '2020-06-24 16:11:06',
                'deleted_at' => NULL,
                'id' => 608,
                'interaction_type' => 'call',
            'note' => '<p>Threatened to cancel service ( Bad Leads) Offered admin tasks. Will discussed tasks for the VA</p>

<hr />
<p>Decided to discuss scope of admin tasks with the VA and keep service.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            104 => 
            array (
                'client_id' => 720,
                'created_at' => '2020-06-24 16:12:47',
                'deleted_at' => NULL,
                'id' => 609,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Touch base on Irish performance / they are very happy with their choice</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            105 => 
            array (
                'client_id' => 279,
                'created_at' => '2020-06-24 17:02:38',
                'deleted_at' => NULL,
                'id' => 610,
                'interaction_type' => 'email',
                'note' => '<p><br />
Confirmed Rhony&#39;s off every Tuesday on their new 60-hour work week schedule starting June 26 until July 7.<br />
&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            106 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-06-24 17:06:58',
                'deleted_at' => NULL,
                'id' => 611,
                'interaction_type' => 'email',
            'note' => '<p>Laura Wey-&nbsp;Updated on new leads to work on this week (concentrating on Boston)</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            107 => 
            array (
                'client_id' => 538,
                'created_at' => '2020-06-24 17:11:31',
                'deleted_at' => NULL,
                'id' => 612,
                'interaction_type' => 'others',
                'note' => '<p><br />
First day assistance with his TC today. Discussed and set expectations on her future tasks.</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            108 => 
            array (
                'client_id' => 114,
                'created_at' => '2020-06-24 19:07:58',
                'deleted_at' => NULL,
                'id' => 613,
                'interaction_type' => 'others',
                'note' => '<p>Inquiring about hiring another VA just for transaction&nbsp;management. I&#39;m looking for a &quot;part-time&quot; MEGA ROCK STAR...needing someone who does not need much training, great follow up, being proactive, computer/tech-savvy, and professional. This is all this individual will be doing.&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => 'email text',
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA',
                'status' => 'done',
            ),
            109 => 
            array (
                'client_id' => 662,
                'created_at' => '2020-06-24 21:58:41',
                'deleted_at' => NULL,
                'id' => 614,
                'interaction_type' => 'text',
                'note' => '<p>Informed Andrew that there is power outage at Jem&#39;s location. VA will be late.</p>

<p>Also sent estimated amount for 1 month of VA svcs of Jem and Nina.</p>

<p>advd the ff:</p>

<ul>
<li style="list-style-type:disc">20-22 working days (depending on when the business days fall in the calendar</li>
<li style="list-style-type:disc">Jem: 20 days for 8 hrs/ day at GVA: $8.60 (FT rate) = $1376.00</li>
<li style="list-style-type:disc">Nina: 20 days for 8 hrs at EVA: $11.25 (FT rate) = $1800.00</li>
<li style="list-style-type:disc">TOTAL: $3,176.00 plus tax</li>
</ul>

<p>Andrew acknowledged.</p>

<p>comms: text/ email</p>',
                    'note_type' => 'notification',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                110 => 
                array (
                    'client_id' => 677,
                    'created_at' => '2020-06-24 22:12:59',
                    'deleted_at' => NULL,
                    'id' => 615,
                    'interaction_type' => 'text',
                'note' => '<p>Full time Schedule for Cielo effective on June 15, 2020 (as approved by Pavel) - confirmation via ph call</p>

<p>In communication with OM Tima re KW Command Training program with VD.</p>

<p>Schedule change from 9am-6pm EST to 11am-8pm EST - effective June 25, 2020 - confirmation via text</p>',
                    'note_type' => 'followup',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                111 => 
                array (
                    'client_id' => 361,
                    'created_at' => '2020-06-24 22:15:53',
                    'deleted_at' => NULL,
                    'id' => 616,
                    'interaction_type' => 'email',
                    'note' => '<p>Touch base:</p>

<p>Feedback for Aldrin Maghirang:</p>

<p>&quot;Yes, he has been doing great as always. &quot; - from Kierra</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                112 => 
                array (
                    'client_id' => 745,
                    'created_at' => '2020-06-24 22:56:29',
                    'deleted_at' => NULL,
                    'id' => 617,
                    'interaction_type' => 'call',
                    'note' => '<p>CI Schedule: June 26, &nbsp;11:30 pm MNL</p>

<p>Client: Stephanie Kane<br />
Company: https://www.stephaniekanehomes.com/<br />
Timezone: NH - EST<br />
PT EVA</p>

<p>Preferences/Tasks:<br />
- &nbsp;KW Command management; setting up smart plans for leads<br />
- Database management; data entry<br />
- Setting appointments and nurture Expireds and FSBO leads&nbsp;<br />
- Circle prospecting seller leads<br />
- Social Media management<br />
- Marketing to listings campaign<br />
- Listings management from contract to close for New Hampshire contracts&nbsp;</p>

<p>CRM and Tools: KW COmmand, Zillow, MLS, Realtor.com, FB ads manager</p>

<p>Shift sched: 12-4pm EST</p>',
                    'note_type' => 'others',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Preference Call',
                    'status' => 'done',
                ),
                113 => 
                array (
                    'client_id' => 744,
                    'created_at' => '2020-06-24 22:58:05',
                    'deleted_at' => NULL,
                    'id' => 618,
                    'interaction_type' => 'text',
                    'note' => '<p>call, text and email to client; Preference call set for June 25, 1 pm EST confirmed via text; calendar invite sent</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                114 => 
                array (
                    'client_id' => 744,
                    'created_at' => '2020-06-24 22:58:05',
                    'deleted_at' => NULL,
                    'id' => 619,
                    'interaction_type' => 'text',
                    'note' => '<p>call, text and email to client; Preference call set for June 25, 1 pm EST confirmed via text; calendar invite sent</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                115 => 
                array (
                    'client_id' => 650,
                    'created_at' => '2020-06-24 23:18:30',
                    'deleted_at' => NULL,
                    'id' => 620,
                    'interaction_type' => 'others',
                    'note' => '<p>Resumption of VA services effective June 25th , 2020&nbsp;</p>',
                    'note_type' => 'notification',
                    'noted_by' => 23,
                    'others_interaction' => 'Call text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                116 => 
                array (
                    'client_id' => 528,
                    'created_at' => '2020-06-25 19:06:54',
                    'deleted_at' => NULL,
                    'id' => 621,
                    'interaction_type' => 'call',
                    'note' => '<p>Shannon has missed hours of 1.5 hrs due to power outage/inet connection due to heavy rainfall.</p>

<p>Elizabeth ok for Shannon not to do MUS</p>

<p>Explained to Elizabeth that since today is last day of billing cut off - she has already been billed/ charged for today. Advd that we will be adjusting next invoice instead - Elizabeth agreed</p>

<p>Feedback for Shannon&nbsp;&ldquo;She&rsquo;s excellent, She&rsquo;s doing a great job. She&rsquo;s been on top of everything. She does really good stuff, I am very grateful for her&rdquo;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                117 => 
                array (
                    'client_id' => 686,
                    'created_at' => '2020-06-25 21:08:50',
                    'deleted_at' => NULL,
                    'id' => 622,
                    'interaction_type' => 'text',
                    'note' => '<p>Toni has advised that she isn&#39;t paying th current invoice she received.</p>

<p>Explained to her that charges were for the last 3 days rendered by Charlyn - June 11, 12 and 15 - sent her the screen shot of invoice with specific dates.</p>

<p>Toni understood</p>

<p>*** I have been calling Toni - but she never answered - only replies via text</p>',
                    'note_type' => 'notification',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                118 => 
                array (
                    'client_id' => 599,
                    'created_at' => '2020-06-25 21:18:26',
                    'deleted_at' => NULL,
                    'id' => 623,
                    'interaction_type' => 'text',
                    'note' => '<p>Confirming start date of DAve with her client - Ken</p>

<p>Adilah will follow up with her client and will let us know</p>',
                    'note_type' => 'followup',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'open',
                ),
                119 => 
                array (
                    'client_id' => 737,
                    'created_at' => '2020-06-25 21:32:45',
                    'deleted_at' => NULL,
                    'id' => 624,
                    'interaction_type' => 'call',
                    'note' => '<p>Processed payment today for June 11-25 cut-off - cc on file error message: Do Not Honor</p>

<p>Christine provided new cc dtls</p>',
                    'note_type' => 'collection',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                120 => 
                array (
                    'client_id' => 734,
                    'created_at' => '2020-06-26 00:29:40',
                    'deleted_at' => NULL,
                    'id' => 625,
                    'interaction_type' => 'call',
                    'note' => '<p>June 24, 2020</p>

<p>Addressed concern about his hired TC - he said that when they called Joseph, there were dogs barking in the background and a baby crying. He also said that he expects that by now, Joseph can already help Donna (their in-house TC) in executing contracts because they currently have 57 pending contracts.<br />
<br />
Spoke to Donna and she said that she doesn&#39;t want to throw Joseph yet into a pit fire. They&#39;re working on active contracts for training. Informed her that as per Eric, he wants her to assign tasks to Joseph already. Spoke to Eric as well and asked if he can give Joseph a chance to go through a bit of learning curve because TC process for KW is a bit different that what they are doing now in REMAX.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                121 => 
                array (
                    'client_id' => 680,
                    'created_at' => '2020-06-26 00:46:06',
                    'deleted_at' => NULL,
                    'id' => 626,
                    'interaction_type' => 'others',
                    'note' => '<p>June 24, 2020</p>

<p>Complaining again about Chris outputs and productivity:<br />
<br />
Sent her screenshots of all the rental houses that Chris was able to search for the day and the call logs. OM Tima also called her but she&#39;s too busy to answer our phone calls.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                122 => 
                array (
                    'client_id' => 667,
                    'created_at' => '2020-06-26 00:52:16',
                    'deleted_at' => NULL,
                    'id' => 627,
                    'interaction_type' => 'others',
                    'note' => '<p>June 24, 2020</p>

<p>Adam Mahfouda&#39;s Business Development Manager - he interviewed VA Mace Narceda and her liked her. He said that Mace seems to be a good fit for their company. We agreed on having her shadow with Zoe tomorrow for 4hrs before she starts doing recruiting calls on Tuesday next week.<br />
<br />
Schedule:&nbsp;9AM - 1PM EST | Tues - Sat</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => 'Google Meeting',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'CI for Replacement',
                    'status' => 'done',
                ),
                123 => 
                array (
                    'client_id' => 693,
                    'created_at' => '2020-06-26 00:53:32',
                    'deleted_at' => NULL,
                    'id' => 628,
                    'interaction_type' => 'others',
                    'note' => '<p>June 24, 2020</p>

<p>Touchbase - he&#39;s all praises for Lyka Espera and we discussed a bit about Melanie. Informed her that I tried to call Melanie yesterday and today but I am being routed to voicemail. Also sent her text messages. He said he will also reach out to her and follow up.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                124 => 
                array (
                    'client_id' => 641,
                    'created_at' => '2020-06-26 01:05:14',
                    'deleted_at' => NULL,
                    'id' => 629,
                    'interaction_type' => 'email',
                    'note' => '<p>June 24, 2020</p>

<p>Approved leave for Marie Chiello Pena on July 15th - he requested for it to be a Paid Time Off because Chiello is working really hard and she deserves it</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Leave Approval',
                    'status' => 'done',
                ),
                125 => 
                array (
                    'client_id' => 118,
                    'created_at' => '2020-06-26 01:15:33',
                    'deleted_at' => NULL,
                    'id' => 630,
                    'interaction_type' => 'others',
                    'note' => '<p>June 24, 2020</p>

<p>Informed him that Louize experienced Power Outage for 40mins - told him that we already required Louize to extend for 45mins after her shift to compensate for the lost hrs/mins</p>',
                    'note_type' => 'notification',
                    'noted_by' => 19,
                    'others_interaction' => 'HangOuts',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                126 => 
                array (
                    'client_id' => 702,
                    'created_at' => '2020-06-26 01:17:10',
                    'deleted_at' => NULL,
                    'id' => 631,
                    'interaction_type' => 'call',
                    'note' => '<p>June 24, 2020</p>

<p>Touchbase - he said so far, Margaret Pineda is doing great. She has set up appointments for him already and some are soon to be converted as their listings and some are buyers</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                127 => 
                array (
                    'client_id' => 567,
                    'created_at' => '2020-06-26 01:17:54',
                    'deleted_at' => NULL,
                    'id' => 632,
                    'interaction_type' => 'email',
                    'note' => '<p>June 24, 2020</p>

<p>Approved leave for Valerie Regalario for July 17th and 18th - VA will be rendering an MUS for June 29th July 6th and 13th</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                128 => 
                array (
                    'client_id' => 715,
                    'created_at' => '2020-06-26 01:18:42',
                    'deleted_at' => NULL,
                    'id' => 633,
                    'interaction_type' => 'others',
                    'note' => '<p>June 24, 2020</p>

<p>Touchbase - trained Marion again today and he said that he thinks VA is already ready to work on his own starting tomorrow. He said he is more at ease working with him because he knows his stuff and he also gives him inputs and ideas which what he is looking for.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 19,
                    'others_interaction' => 'WhatsApp',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                129 => 
                array (
                    'client_id' => 734,
                    'created_at' => '2020-06-26 20:40:38',
                    'deleted_at' => NULL,
                    'id' => 634,
                    'interaction_type' => 'call',
                    'note' => '<p>June 25, 2020</p>

<p>Discussed reason for resignation of TC Joseph Benjamin Baluyot.<br />
<br />
Client requested to adjust his invoice and not bill him for the 3 days that they trained Joseph. Also discussed replacement process. VA replacement not yet identified.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                130 => 
                array (
                    'client_id' => 427,
                    'created_at' => '2020-06-26 20:41:36',
                    'deleted_at' => NULL,
                    'id' => 635,
                    'interaction_type' => 'call',
                    'note' => '<p>June 25, 2020</p>

<p>Collection Call - followed up on his pending invoice of $981.95. He said he has to call Bank of America to authorize the charge and we can try running it again tomorrow.<br />
<br />
He also approved VA Allen Salindong&#39;s yearly increase of $0.50/hr.</p>',
                    'note_type' => 'others',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Rate Increase',
                    'status' => 'done',
                ),
                131 => 
                array (
                    'client_id' => 667,
                    'created_at' => '2020-06-26 20:42:41',
                    'deleted_at' => NULL,
                    'id' => 636,
                    'interaction_type' => 'call',
                    'note' => '<p>June 25, 2020</p>

<p>Collection Call - called to follow up on his credit card information. Call routed to voicemail. Email and text sent - awaiting response</p>',
                    'note_type' => 'collection',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                132 => 
                array (
                    'client_id' => 443,
                    'created_at' => '2020-06-26 20:43:32',
                    'deleted_at' => NULL,
                    'id' => 637,
                    'interaction_type' => 'email',
                    'note' => '<p>June 25, 2020</p>

<p>Collection Call - followed up on his pending invoice of $223.17. He said we can try running the card again tomorrow</p>',
                    'note_type' => 'collection',
                    'noted_by' => 19,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                133 => 
                array (
                    'client_id' => 710,
                    'created_at' => '2020-06-29 16:08:42',
                    'deleted_at' => NULL,
                    'id' => 638,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Clarification on tracker hours and Toni&#39;s logins</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                134 => 
                array (
                    'client_id' => 466,
                    'created_at' => '2020-06-29 16:09:34',
                    'deleted_at' => NULL,
                    'id' => 639,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>Explained the breakdown of her bill and provided cut off details</p>',
                    'note_type' => 'collection',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                135 => 
                array (
                    'client_id' => 698,
                    'created_at' => '2020-06-29 16:10:35',
                    'deleted_at' => NULL,
                    'id' => 640,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>Assured that we will be helping her VA with QB</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                136 => 
                array (
                    'client_id' => 636,
                    'created_at' => '2020-06-29 16:33:15',
                    'deleted_at' => NULL,
                    'id' => 641,
                    'interaction_type' => 'text',
                    'note' => '<p>&nbsp;</p>

<p>Communicated usual errors encountered and advised if we could create a group chat for real time concerns</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Coaching for VA',
                    'status' => 'acknowleged',
                ),
                137 => 
                array (
                    'client_id' => 708,
                    'created_at' => '2020-06-29 18:37:46',
                    'deleted_at' => NULL,
                    'id' => 642,
                    'interaction_type' => 'email',
                    'note' => '<p>VA incentive for Rocel Andico, $100.&nbsp;</p>

<p>Forwarded to Om and Billing.&nbsp;</p>',
                    'note_type' => 'others',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'VA Incentive',
                    'status' => 'done',
                ),
                138 => 
                array (
                    'client_id' => 666,
                    'created_at' => '2020-06-29 18:39:43',
                    'deleted_at' => NULL,
                    'id' => 643,
                    'interaction_type' => 'email',
                    'note' => '<p>VA incentive for Shekah Marquez, $75&nbsp;</p>

<p>Forwarded to Om and Billing.&nbsp;</p>',
                    'note_type' => 'others',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'VA Incentive',
                    'status' => 'done',
                ),
                139 => 
                array (
                    'client_id' => 656,
                    'created_at' => '2020-06-29 19:37:48',
                    'deleted_at' => NULL,
                    'id' => 644,
                    'interaction_type' => 'others',
                    'note' => '<p>Meet and greet schedule follow up with VA Shekah</p>

<p>Informed invoice adjustment of $32.25 is already approved by Om, it will be adjusted on her next invoice</p>',
                    'note_type' => 'followup',
                    'noted_by' => 23,
                    'others_interaction' => 'Call text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                140 => 
                array (
                    'client_id' => 701,
                    'created_at' => '2020-06-29 19:47:21',
                    'deleted_at' => NULL,
                    'id' => 645,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Update on selection of ISA. Asked to be followed-up on Monday.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'call and email',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                141 => 
                array (
                    'client_id' => 714,
                    'created_at' => '2020-06-29 19:47:28',
                    'deleted_at' => NULL,
                    'id' => 646,
                    'interaction_type' => 'call',
                    'note' => '<p>June 24, 2020</p>

<p>VA needs more attention to details/Don&#39;t upload incomplete documents/Learn KWC/Will touch base again with Client on Monday to know how if the VA improved from just knowing an overview of US RealEstate/Will do a TC process discussion with the VA</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                142 => 
                array (
                    'client_id' => 549,
                    'created_at' => '2020-06-29 19:49:38',
                    'deleted_at' => NULL,
                    'id' => 647,
                    'interaction_type' => 'others',
                    'note' => '<p>Follow up on invoice/payment collection.</p>',
                    'note_type' => 'collection',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                143 => 
                array (
                    'client_id' => 538,
                    'created_at' => '2020-06-29 20:00:31',
                    'deleted_at' => NULL,
                    'id' => 648,
                    'interaction_type' => 'email',
                    'note' => '<p><br />
Request for payment scheme-coordinated with OM.</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                144 => 
                array (
                    'client_id' => 695,
                    'created_at' => '2020-06-29 20:01:15',
                    'deleted_at' => NULL,
                    'id' => 649,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Confirmed resumption of ISA services with Joshua.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                145 => 
                array (
                    'client_id' => 752,
                    'created_at' => '2020-06-30 22:05:43',
                    'deleted_at' => NULL,
                    'id' => 650,
                    'interaction_type' => 'call',
                    'note' => '<p>Advised Isabelle that Jason is experiencing inet connetivity issues and will make up for the lost time.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                146 => 
                array (
                    'client_id' => 738,
                    'created_at' => '2020-07-01 15:31:51',
                    'deleted_at' => NULL,
                    'id' => 651,
                    'interaction_type' => 'email',
                'note' => '<p>Touch base on VA&#39;s performance and have a different tasks for the VA. Wants to discuss a few things over the phone(schedule call tomorrow)<br />
&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                147 => 
                array (
                    'client_id' => 753,
                    'created_at' => '2020-07-01 16:21:57',
                    'deleted_at' => NULL,
                    'id' => 652,
                    'interaction_type' => 'call',
                    'note' => '<p>Touch base regarding VA&#39;s performance and showed concerns on contacts and expertise on FB ads / VA will be monitored<br />
&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                148 => 
                array (
                    'client_id' => 114,
                    'created_at' => '2020-07-01 16:22:34',
                    'deleted_at' => NULL,
                    'id' => 653,
                    'interaction_type' => 'email',
                    'note' => '<p>Heads up on Cherish&#39;s absence due to family emergency<br />
&nbsp;</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                149 => 
                array (
                    'client_id' => 504,
                    'created_at' => '2020-07-01 16:23:12',
                    'deleted_at' => NULL,
                    'id' => 654,
                    'interaction_type' => 'email',
                    'note' => '<p>Heads up on Cherish&#39;s absence due to family emergency<br />
&nbsp;</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                150 => 
                array (
                    'client_id' => 714,
                    'created_at' => '2020-07-01 19:41:43',
                    'deleted_at' => NULL,
                    'id' => 655,
                    'interaction_type' => 'text',
                    'note' => '<p>Client Texted:&nbsp;Hello Alecs, Thank you for following up. Verna is trying. However since she does not know anything about Real Estate I am having to micro manage her throughout the day. This is not vision I had in mind. It is adding a lot more to my plate with having to do the job I hired a VA for on the transaction coordinating. I am at a loss as I am unable to keep up this schedule much longer as I already behind. Please advise.<br />
<br />
Reply:&nbsp;Hi Malynda, Thank you for sending us your feedback. I&#39;ve already discussed the Real Estate process and the transaction flow to Verna. I&#39;ll work with her side by side. If it&#39;s okay with you, please send her more tasks then I&#39;ll personally walk her through on how to do it until she becomes more confident to work on it on her own.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'open',
                ),
                151 => 
                array (
                    'client_id' => 706,
                    'created_at' => '2020-07-01 20:03:56',
                    'deleted_at' => NULL,
                    'id' => 656,
                    'interaction_type' => 'call',
                    'note' => '<p>The VA is doing great. She&#39;s able to complete then tasks that I&#39;ve been sending her. Client is happy and satisfied.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                152 => 
                array (
                    'client_id' => 671,
                    'created_at' => '2020-07-01 20:08:36',
                    'deleted_at' => NULL,
                    'id' => 657,
                    'interaction_type' => 'email',
                    'note' => '<p>June 24, 2020 -&nbsp;<br />
Replied to Ronnie&#39;s email/Client wanted to pause the VA&#39;s activities until they&#39;re all set up/Sent an email requesting for a once and for all meeting</p>',
                    'note_type' => 'followup',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                153 => 
                array (
                    'client_id' => 671,
                    'created_at' => '2020-07-01 20:20:09',
                    'deleted_at' => NULL,
                    'id' => 658,
                    'interaction_type' => 'email',
                    'note' => '<p>Received an email from client/ would like to set the appointment next Tuesday, July 7, 2020 to help him get a VA and setup tools.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                154 => 
                array (
                    'client_id' => 677,
                    'created_at' => '2020-07-01 22:04:56',
                    'deleted_at' => NULL,
                    'id' => 659,
                    'interaction_type' => 'call',
                    'note' => '<p>It&rsquo;s all good.&nbsp;</p>

<ul>
<li style="list-style-type:disc">Communicate a lot - talk about the good, the bad and the ugly</li>
<li style="list-style-type:disc">Talked about where they are in the process</li>
<li style="list-style-type:disc">Expectations are discussed</li>
<li style="list-style-type:disc">Going good</li>
</ul>

<p style="list-style-type:disc">Asked for Transaction Coordination SME - would like to have session for Cielo&nbsp;</p>

<p style="list-style-type:disc">- Eric mentioned that he isn&#39;t good at it</p>

<ul>
<li style="list-style-type:disc">Advd that Coach Alecs is good with TC -</li>
</ul>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                155 => 
                array (
                    'client_id' => 691,
                    'created_at' => '2020-07-01 22:05:54',
                    'deleted_at' => NULL,
                    'id' => 660,
                    'interaction_type' => 'call',
                    'note' => '<p>Client would like to be on hold for 1 to 2 months since he felt uneasy to why the VA resigned with him. Reported the client&#39;s decision. Will follow up at the end of July.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                156 => 
                array (
                    'client_id' => 719,
                    'created_at' => '2020-07-01 22:07:46',
                    'deleted_at' => NULL,
                    'id' => 661,
                    'interaction_type' => 'call',
                    'note' => '<p>As per client, VA is really good.&nbsp;Easy to understand and execute the tasks.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                157 => 
                array (
                    'client_id' => 717,
                    'created_at' => '2020-07-01 22:11:20',
                    'deleted_at' => NULL,
                    'id' => 662,
                    'interaction_type' => 'call',
                    'note' => '<p>VA is very good and pro-active, however, Virtudesk gave me a false promise that they are going to give me a VA who has KW Command experience/ VA needs to work on the KWCommand/Transfer all Dotloop transactions/Docusign/Will touch base again on July 3rd&nbsp;to determine if we will go full time with VA.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                158 => 
                array (
                    'client_id' => 712,
                    'created_at' => '2020-07-01 22:28:24',
                    'deleted_at' => NULL,
                    'id' => 663,
                    'interaction_type' => 'call',
                    'note' => '<p>Client got mad since the VA wasn&#39;t able to log-in last Friday due to power interruption and 3 more day due to vertigo. Client wanted to cancel and would like to have a full refund. Client will be refunded as per OM and will have a new VA for replacement. Client is currently looking forward to have a better business with us.</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                159 => 
                array (
                    'client_id' => 694,
                    'created_at' => '2020-07-01 22:33:36',
                    'deleted_at' => NULL,
                    'id' => 664,
                    'interaction_type' => 'call',
                    'note' => '<p>Client and VA are taking everything in slow process/ haven&#39;t delegated any other task yet /More on social media tasks/will delegate mo responsibilities like handling transactions using KW Command</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                160 => 
                array (
                    'client_id' => 670,
                    'created_at' => '2020-07-01 22:35:21',
                    'deleted_at' => NULL,
                    'id' => 665,
                    'interaction_type' => 'text',
                    'note' => '<p>Texted Steve/Touchbase session was moved on Monday/Will send a text message again on Monday morning/Texted client - no response yet/ will follow up on him this week.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                161 => 
                array (
                    'client_id' => 735,
                    'created_at' => '2020-07-01 22:38:07',
                    'deleted_at' => NULL,
                    'id' => 666,
                    'interaction_type' => 'others',
                    'note' => '<p>Texted Amy for touch base session/ Client agreed accepted the call/VA do more script practicing with her partner/as per client to the VA, the ability is there and was impressed with their integrity.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => 'Text and Call',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                162 => 
                array (
                    'client_id' => 739,
                    'created_at' => '2020-07-01 22:47:28',
                    'deleted_at' => NULL,
                    'id' => 667,
                    'interaction_type' => 'call',
                    'note' => '<p>So far so good/ VA will focus on creating newsletter/creat buyer ans seller campaign/gather leads from email/too much with&nbsp;mailchimp/switching boomtown to Brivity/ organize boomtown/TC tasks<br />
&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 29,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                163 => 
                array (
                    'client_id' => 538,
                    'created_at' => '2020-07-02 02:37:48',
                    'deleted_at' => NULL,
                    'id' => 668,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Sent ACH/wire transfer information for payment processing.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'call and email',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                164 => 
                array (
                    'client_id' => 478,
                    'created_at' => '2020-07-02 02:38:29',
                    'deleted_at' => NULL,
                    'id' => 669,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Last day with VA Sam today.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => 'call and email',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                165 => 
                array (
                    'client_id' => 731,
                    'created_at' => '2020-07-02 02:45:08',
                    'deleted_at' => NULL,
                    'id' => 670,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Moved CI date tomorrow at 11AM PST.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                166 => 
                array (
                    'client_id' => 482,
                    'created_at' => '2020-07-02 02:47:28',
                    'deleted_at' => NULL,
                    'id' => 671,
                    'interaction_type' => 'email',
                    'note' => '<p><br />
Confirmed paid time-off on July 3 for all their VAs.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                167 => 
                array (
                    'client_id' => 701,
                    'created_at' => '2020-07-02 02:50:38',
                    'deleted_at' => NULL,
                    'id' => 672,
                    'interaction_type' => 'others',
                    'note' => '<p>Confirmed rate for FT and PT ISA services.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'call and email',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                168 => 
                array (
                    'client_id' => 677,
                    'created_at' => '2020-07-02 23:00:09',
                    'deleted_at' => NULL,
                    'id' => 673,
                    'interaction_type' => 'others',
                    'note' => '<p>Pilot session for KW Command Training with Coaches</p>

<p>Training Schedule is every Tues and Thur 1pm-2pm EST for Coaches, Trainers and VAs&nbsp;</p>',
                    'note_type' => 'others',
                    'noted_by' => 22,
                    'others_interaction' => 'Zoom',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Training',
                    'status' => 'ongoing',
                ),
                169 => 
                array (
                    'client_id' => 660,
                    'created_at' => '2020-07-03 16:24:49',
                    'deleted_at' => NULL,
                    'id' => 674,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Checked initial tasks of his new VA&#39;s and forwarded VA&#39;s emails</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Additional VA request',
                    'status' => 'acknowleged',
                ),
                170 => 
                array (
                    'client_id' => 668,
                    'created_at' => '2020-07-03 20:33:50',
                    'deleted_at' => NULL,
                    'id' => 675,
                    'interaction_type' => 'call',
                    'note' => '<p>Raised concerns on her&#39;s VA&#39;s concerns and how he is being coached asked client to set goals specially time sensitive tasks</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                171 => 
                array (
                    'client_id' => 231,
                    'created_at' => '2020-07-07 04:32:27',
                    'deleted_at' => NULL,
                    'id' => 676,
                    'interaction_type' => 'others',
                    'note' => '<p>Followed up if she will add hours for Mike</p>',
                    'note_type' => 'followup',
                    'noted_by' => 23,
                    'others_interaction' => 'Call text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                172 => 
                array (
                    'client_id' => 718,
                    'created_at' => '2020-07-07 04:33:16',
                    'deleted_at' => NULL,
                    'id' => 677,
                    'interaction_type' => 'email',
                    'note' => '<p>Meet and greet tomorrow with replacement VA, Grace at 11am PST</p>',
                    'note_type' => 'others',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Meet and greet',
                    'status' => 'done',
                ),
                173 => 
                array (
                    'client_id' => 656,
                    'created_at' => '2020-07-07 04:34:10',
                    'deleted_at' => NULL,
                    'id' => 678,
                    'interaction_type' => 'text',
                    'note' => '<p>Followed up updated Chime CRM password, informed Shekah started prospecting expired leads</p>',
                    'note_type' => 'followup',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                174 => 
                array (
                    'client_id' => 626,
                    'created_at' => '2020-07-07 04:35:02',
                    'deleted_at' => NULL,
                    'id' => 679,
                    'interaction_type' => 'call',
                    'note' => '<p>Touchbase , feedback for VA Jhon<br />
Everything is working out well. He&#39;s been doing the jobs that I&#39;ve been keeping him to do. We seem to have a good rapport going, we can understand each other pretty well and I think it&#39;s going good.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                175 => 
                array (
                    'client_id' => 533,
                    'created_at' => '2020-07-07 04:36:18',
                    'deleted_at' => NULL,
                    'id' => 680,
                    'interaction_type' => 'email',
                    'note' => '<p>Sent another VA incentove bonus for VA Stephanie for every 10 appts booked, forwarded to billing, $38.40&nbsp;</p>',
                    'note_type' => 'others',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'VA Incentive',
                    'status' => 'done',
                ),
                176 => 
                array (
                    'client_id' => 686,
                    'created_at' => '2020-07-07 15:35:38',
                    'deleted_at' => NULL,
                    'id' => 681,
                    'interaction_type' => 'call',
                    'note' => '<p>Toni has advised that she wants to cancel svcs as she has found a cheaper VA</p>

<p>Called - no answer</p>

<p>====</p>

<p>Spoke to Toni - Feedback given:</p>

<p>no notes on FUB only in Mojo</p>

<ul>
<li style="list-style-type:disc">Appt or today was for REagent</li>
<li style="list-style-type:disc">Tutorials given</li>
<li style="list-style-type:disc">Only uses Mojo&nbsp;</li>
<li style="list-style-type:disc">Appt - wouldn&rsquo;t confirm with her</li>
<li style="list-style-type:disc">Same time appt for 2 diff people</li>
<li style="list-style-type:disc">Invite to calendar, so VA could see</li>
<li style="list-style-type:disc">Only asked yday - if sched is open.</li>
</ul>

<p>*** Advd will update once done with coaching/ talking to VA - Mace Narceda</p>

<p>Called back - no answer</p>

<p>Email and SMS sent:</p>

<ol>
<li style="list-style-type:decimal">Appointment set with REA - Mace, has been following the script that you have given her and has always made a point to ask if lead is already working with an agent. Though, there is a possibility that she may have missed asking that question with this lead.</li>
<li style="list-style-type:decimal">Appointment for 2 diff people at the same time - Mace has no access to the Calendar at the time and doesn&rsquo;t remember all appointment time off of her head.
<ol>
<li style="list-style-type:lower-alpha">VA has been advd to check on notes and to jot down appointments on diff notepad</li>
</ol>
</li>
<li style="list-style-type:decimal">Calendar has nothing on it. Nothing Scheduled</li>
<li style="list-style-type:decimal">We can offer a lower rate for you. Please let me know what time i can call you.</li>
</ol>

<p>No reply. Advd VA not to log in (approved by OM)</p>

<p>=========</p>

<p>July 8, 2020</p>

<p>Was able to talk to Toni Jennings - she will continue with services. will be observing Mace.</p>

<p>Client says she was able to get a VA with $7/hr rate.</p>

<p>Save offer of -$1/hr: $9.75/hr</p>',
                    'note_type' => 'notification',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                177 => 
                array (
                    'client_id' => 683,
                    'created_at' => '2020-07-08 22:17:42',
                    'deleted_at' => NULL,
                    'id' => 682,
                    'interaction_type' => 'call',
                    'note' => '<p>July 7</p>

<p>rish is doing perfectly fine and doing the business elsewhere. When he talked to James yday -&nbsp;</p>

<p>Not going to do prospecting - not going to need a VA</p>

<p>Haven&rsquo;t gotten any business at this point</p>

<p>Irish is doing amazing</p>

<p>No return.&nbsp;</p>

<p>Covid 19 - alot is going on - market is changing.</p>

<p>Doing all soc media marketing - not interested at the moment - Financial</p>

<p>Re-organize.</p>

<p>========</p>

<p>July 8, 2020</p>

<p>Conference Call with Irish to advs of decision.</p>

<p>Stanley wanted to inform VA himself and that it has nothing to do with her performance.</p>

<p>VA&#39;s last day is July 23, 2020</p>',
                    'note_type' => 'notification',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                178 => 
                array (
                    'client_id' => 662,
                    'created_at' => '2020-07-08 22:26:29',
                    'deleted_at' => NULL,
                    'id' => 683,
                    'interaction_type' => 'text',
                    'note' => '<p>Reminded Andrew of Jem&#39;s scheduled leave for July 23 and 24 which wsa discussed during M&amp;G.</p>

<p>Advd that flights were cancelled and VA will just take July 24 leave.</p>

<p>Andrew also introduced me to his POC: Jean.</p>

<p>provided feedback: &quot;Appreciate you and both VA&#39;s they are very efficient and do great work&quot;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                179 => 
                array (
                    'client_id' => 733,
                    'created_at' => '2020-07-13 13:05:28',
                    'deleted_at' => NULL,
                    'id' => 684,
                    'interaction_type' => 'others',
                    'note' => '<p>&nbsp;</p>

<p>Discussed Additional VA benefits roles and tasks / Endorsed Ranelle Orias start date monday</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => 'Whats app',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Additional VA',
                    'status' => 'done',
                ),
                180 => 
                array (
                    'client_id' => 731,
                    'created_at' => '2020-07-13 14:04:44',
                    'deleted_at' => NULL,
                    'id' => 685,
                    'interaction_type' => 'email',
                    'note' => '<p>Inquiry on FT status with VA Jenielou. Provided information on the new rate.</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                181 => 
                array (
                    'client_id' => 704,
                    'created_at' => '2020-07-13 14:07:33',
                    'deleted_at' => NULL,
                    'id' => 686,
                    'interaction_type' => 'email',
                    'note' => '<p><br />
Provided positive feedback on VA Brianna.<br />
Will send confirmation if they can start FT with Brianna next week.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                182 => 
                array (
                    'client_id' => 747,
                    'created_at' => '2020-07-13 14:09:28',
                    'deleted_at' => NULL,
                    'id' => 687,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
First day with Charmain today. Provided her first few information on what to do for 3 days. Assured that Charmain will be provided assistance on most of his tools.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                183 => 
                array (
                    'client_id' => 755,
                    'created_at' => '2020-07-13 14:10:42',
                    'deleted_at' => NULL,
                    'id' => 688,
                    'interaction_type' => 'email',
                    'note' => '<p>Riza has been wonderful!&nbsp; She is super! Thank you!!</p>

<p>&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                184 => 
                array (
                    'client_id' => 274,
                    'created_at' => '2020-07-13 14:11:09',
                    'deleted_at' => NULL,
                    'id' => 689,
                    'interaction_type' => 'others',
                    'note' => '<p>Provided positive feedback on Aldrin&#39;s tasks.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                185 => 
                array (
                    'client_id' => 637,
                    'created_at' => '2020-07-13 14:23:18',
                    'deleted_at' => NULL,
                    'id' => 690,
                    'interaction_type' => 'text',
                    'note' => '<p>&nbsp;</p>

<p>Invoice collection / advised to notify the bank to relase the charges</p>',
                    'note_type' => 'collection',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                186 => 
                array (
                    'client_id' => 634,
                    'created_at' => '2020-07-13 14:46:21',
                    'deleted_at' => NULL,
                    'id' => 691,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>Renew another 20hrs for Shannon start date today</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Renew hours',
                    'status' => 'done',
                ),
                187 => 
                array (
                    'client_id' => 660,
                    'created_at' => '2020-07-13 14:48:24',
                    'deleted_at' => NULL,
                    'id' => 692,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Provided assistance on their billing and requested for per VA breakdown in the future bills</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                188 => 
                array (
                    'client_id' => 224,
                    'created_at' => '2020-07-13 19:03:26',
                    'deleted_at' => NULL,
                    'id' => 693,
                    'interaction_type' => 'call',
                    'note' => '<p>Called Client John Rainville to see if he is ready to restart VA Mitch&#39;s hours. Voicemail, left message. Text sent. He responded saying to call him on Friday.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                189 => 
                array (
                    'client_id' => 692,
                    'created_at' => '2020-07-13 19:04:57',
                    'deleted_at' => NULL,
                    'id' => 694,
                    'interaction_type' => 'call',
                    'note' => '<p>Called Client Brad Lowery - VA Arjayson Solis is doing really good. He is doing exactly what he is asked and learning really well. He has no referral right now but would definitely let us know if he knows somebody that would benefit from our services.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                190 => 
                array (
                    'client_id' => 758,
                    'created_at' => '2020-07-13 19:06:07',
                    'deleted_at' => NULL,
                    'id' => 695,
                    'interaction_type' => 'call',
                'note' => '<p>Called Client John Boozer - VA Margaret Pineda&#39;s first day of calling (today) went great. She sat 5 appointments.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                191 => 
                array (
                    'client_id' => 693,
                    'created_at' => '2020-07-13 19:08:26',
                    'deleted_at' => NULL,
                    'id' => 696,
                    'interaction_type' => 'call',
                    'note' => '<p>Called Client Seth Sollman - VA Lykalyn Espera is doing great. Whatever she is doing with marketing is working because he is getting a lot of inquiries from possible home sellers and buyers. He loves our company and would like to send more referrals. I emailed him my contact information for the referrals and for anything he would need regarding his VA. He also wants Lyka to get trained on KW Command since he is very busy and doesn&rsquo;t have time to learn it. Dotloop has been cancelled by KW but he is still using it and paying additional until Lyka and him has learned to use KW Command. According to VA Lyka she has attended training with Eric Thomas and did walkthrough of the KW Command. They&#39;re are also doing role plays before she makes calls on circle prospecting. Lyka and I will do our meet and greet tomorrow as well as role play on circle prospecting and will go over KW Command.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                192 => 
                array (
                    'client_id' => 680,
                    'created_at' => '2020-07-13 19:08:59',
                    'deleted_at' => NULL,
                    'id' => 697,
                    'interaction_type' => 'call',
                    'note' => '<p>Called Client Sara Burrack - She fired Chris he wasn&#39;t doing anything she claims and wants to dispute her bill. Aiko offered replacement but declined. Informed OM. With VA Al he is doing good but she thinks he can be better. She said she is not happy because Al is not on top of things, that he should be converting more. She basically wants Al to track all the clients and nurture them even if they say No or even if the house is already rented. He also wants Al to be friendlier on the emails and respond accordingly even if he got a No. She is not thinking of replacing Al but would just want him to improve. Al is out today and will be back tomorrow. I will Coach him once he is back to make Sara happy.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                193 => 
                array (
                    'client_id' => 715,
                    'created_at' => '2020-07-13 19:09:43',
                    'deleted_at' => NULL,
                    'id' => 698,
                    'interaction_type' => 'call',
                    'note' => '<p>Spoke with Client John Toublaris he wanted to know how to get access to his VA&#39;s screenshots via Timedly. Informed him that I will send him an email with the instructions on how and the log-ins. He then asked if his VA Marion Realon able to do follow up calls. Clarified what he means by follow up calls, he said follow up calls on online leads. I then clarified if it&#39;s when a possible home owner fills out an online form on his website and he gets the notification then the VA will call to gather additional information and set an appointment. He said yes, explained that GVA&#39;s can do some calling but the best people for the job are the ISAs. Explained the difference between GVA, ISA, EVA and TC. Offered to send some ISA&#39;s resumes, he is looking to hire a PT ISA but not in a rush, he has someone doing the calls right now but this will be shifting soon. Told him we will give him some ISA options and whoever he wants to interview we will make time to accommodate it. He initially did not know if PT or FT but I told him for me to be able to give him the best people for the job - I need to know because there are VAs that are only available for PT. Gave the price difference between PT and Ft. Wants someone with calling experience, he will provide the scripts, sample dialogues and objection handling. Also, wanted to know the status of his 2 referrals he provided Aiko. Informed him that I will email him everything within the day and will send my contact information along with it. Email sent</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                194 => 
                array (
                    'client_id' => 680,
                    'created_at' => '2020-07-13 19:11:45',
                    'deleted_at' => NULL,
                    'id' => 699,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed her to let he know that Al and I had a coaching session about the improvements she was asking Al to do. Informed her that I gave Al some example and tips on email handling, nurturing of leads, and tracking of leads. Let her know that Al has commited to make improvements.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                195 => 
                array (
                    'client_id' => 667,
                    'created_at' => '2020-07-13 19:12:20',
                    'deleted_at' => NULL,
                    'id' => 700,
                    'interaction_type' => 'call',
                    'note' => '<p>Called Adam Mahfouda for meet and greet and his invoice. For his VAs performace call Matt his Director because he is the one managing them, Call with Matt is scheduld today at 2PM. Reagrding his invoice he said to send him a credit cardf form and he will fill it out and would pay for his invoice today. Form requested from OM and sent via email</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                196 => 
                array (
                    'client_id' => 715,
                    'created_at' => '2020-07-13 19:14:39',
                    'deleted_at' => NULL,
                    'id' => 701,
                    'interaction_type' => 'email',
                    'note' => '<p>Spoke with John to follow up on my email yesterday. He was able to access Timedly and updated him that his referral Quinn has already signed yesteday. He said he doesn&#39;t need an additonal ISA for npw but would definitely let me know once he needs one.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                197 => 
                array (
                    'client_id' => 693,
                    'created_at' => '2020-07-13 19:15:18',
                    'deleted_at' => NULL,
                    'id' => 702,
                    'interaction_type' => 'call',
                    'note' => '<p>Follow up to see if he received my email yesterday. He appreciate that Lyka and I has done coaching in KW Command and Circle prospecting.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                198 => 
                array (
                    'client_id' => 224,
                    'created_at' => '2020-07-13 19:16:37',
                    'deleted_at' => NULL,
                    'id' => 703,
                    'interaction_type' => 'call',
                    'note' => '<p>Called Client John Rainville to see if he is ready to restart VA Mitch&#39;s hours. Voicemail, left message. Text sent. He said call at past 1:30 PM EST. Called again but vm, text sent. He texted back saying he got stuck in a meeting and to call him on Monday instead at 11AM</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                199 => 
                array (
                    'client_id' => 702,
                    'created_at' => '2020-07-13 19:17:12',
                    'deleted_at' => NULL,
                    'id' => 704,
                    'interaction_type' => 'call',
                    'note' => '<p>Called for meet and greet and inform about transition. Voicemail, left message. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                200 => 
                array (
                    'client_id' => 690,
                    'created_at' => '2020-07-13 19:18:11',
                    'deleted_at' => NULL,
                    'id' => 705,
                    'interaction_type' => 'text',
                    'note' => '<p>Reminded of his oustanding balance.</p>',
                    'note_type' => 'collection',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                201 => 
                array (
                    'client_id' => 546,
                    'created_at' => '2020-07-13 19:22:05',
                    'deleted_at' => NULL,
                    'id' => 706,
                    'interaction_type' => 'email',
                    'note' => '<p>Texted to let him know that we tried charging his card today but it was declined. Awaiting respone</p>

<p>Molly (assisant) has responded: Thank you! Got the new invoice. Just paid this, you should receive it tomorrow.</p>',
                    'note_type' => 'collection',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                202 => 
                array (
                    'client_id' => 366,
                    'created_at' => '2020-07-13 19:25:40',
                    'deleted_at' => NULL,
                    'id' => 707,
                    'interaction_type' => 'call',
                    'note' => '<p>Spoke to Client Vitaly Vishnepolsky. He said Jefte&#39;s performance is not exactly was they expected. She has been doing a lot of call but not booking appointment or qualifying leads. When they has Coaching with Aiko before she improved but then again she is back to not converting again. The leads she is sending are not yet ready and not really qualified, leads have no value. He is thinking of discontinuing the service is she will not improve. He said he is glad that I will take over now because whatever the previous Coach was doing is not working. Told him that Jefte and I will be doing coaching today and I will make sure that she understand the importance of getting qualified leads . Asked client to give Jefter another chance and I will make sure that I will check her progress from time to time to make sure she is converting. Reminded him about his invoice and that we tried to charge his card but it was not honored. He gave me a new card over the phone. Card details was sent to OM.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                203 => 
                array (
                    'client_id' => 443,
                    'created_at' => '2020-07-13 19:26:33',
                    'deleted_at' => NULL,
                    'id' => 708,
                    'interaction_type' => 'text',
                    'note' => '<p>Texted to let him know that we tried charging his card today but it was declined. Awaiting respone<br />
<br />
Client has paid.</p>',
                    'note_type' => 'collection',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                204 => 
                array (
                    'client_id' => 160,
                    'created_at' => '2020-07-13 19:27:14',
                    'deleted_at' => NULL,
                    'id' => 709,
                    'interaction_type' => 'text',
                    'note' => '<p>Texted to let him know that we tried charging his card today but it was declined. Credit card has expire. He request a credit card form. Requested from OM</p>

<p>&nbsp;</p>

<p>Client has paid.</p>',
                    'note_type' => 'collection',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                205 => 
                array (
                    'client_id' => 641,
                    'created_at' => '2020-07-13 19:28:47',
                    'deleted_at' => NULL,
                    'id' => 710,
                    'interaction_type' => 'text',
                    'note' => '<p>Texted to let him know that we tried charging his card today but it was declined. Awaiting respone</p>

<p>Client has paid.</p>',
                    'note_type' => 'collection',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                206 => 
                array (
                    'client_id' => 177,
                    'created_at' => '2020-07-13 19:31:13',
                    'deleted_at' => NULL,
                    'id' => 711,
                    'interaction_type' => 'call',
                    'note' => '<p>Called for meet and greet and inform about transition. Voicemail, left message. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                207 => 
                array (
                    'client_id' => 675,
                    'created_at' => '2020-07-13 19:31:47',
                    'deleted_at' => NULL,
                    'id' => 712,
                    'interaction_type' => 'call',
                    'note' => '<p>Valerie Regalario is doing better than the previous VA. She wants her to get appointments from FSBO&#39;s but she needs to make sure that they are interested in listing a realtor. Some of the appointments she sat - the home owner is in no way interested in signing up with a realtor but she appreciates that Valerie is getting her foot on the door it&#39;s just sometimes the appointment is too far. Valerie should be asking - Are interested in working with a realtor? She is also thinking of having Valerie call her previous client to get referrals or if they themselves are interested in selling so she could get more listing. Told her to that definitely Valerie could call them and I asked for her data base of her previous clients. She will send them over. She wants 5 appointments per week but happy with 3 appointments per week. Told her that I will be coaching Valerie about what we talked about and will also come up with a script with Valerie</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                208 => 
                array (
                    'client_id' => 751,
                    'created_at' => '2020-07-13 19:32:23',
                    'deleted_at' => NULL,
                    'id' => 713,
                    'interaction_type' => 'call',
                    'note' => '<p>Additional hours for VA Melvin, effective July 15th</p>

<p>Transition from PT to FT</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                209 => 
                array (
                    'client_id' => 572,
                    'created_at' => '2020-07-13 20:03:58',
                    'deleted_at' => NULL,
                    'id' => 714,
                    'interaction_type' => 'email',
                    'note' => '<p>Emails sent to ask if he is already ready to resume servies or he needs addtional time.</p>

<p>&nbsp;</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                210 => 
                array (
                    'client_id' => 224,
                    'created_at' => '2020-07-13 20:04:48',
                    'deleted_at' => NULL,
                    'id' => 715,
                    'interaction_type' => 'email',
                    'note' => '<p>Emails sent to ask if he is already ready to resume servies or he needs addtional time. Responsed saying: we are still not back to business as normal. I&#39;ve had to consolidate two offices into one shared office space and I am going to close another in August. Right now we are trying to recalibrate the business and now expect to be shut down again due to the virus spiking. We re still on hold.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                211 => 
                array (
                    'client_id' => 614,
                    'created_at' => '2020-07-13 20:05:45',
                    'deleted_at' => NULL,
                    'id' => 716,
                    'interaction_type' => 'email',
                    'note' => '<p>Emails sent to ask if she is already ready to resume servies or she needs addtional time.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                212 => 
                array (
                    'client_id' => 565,
                    'created_at' => '2020-07-13 20:06:42',
                    'deleted_at' => NULL,
                    'id' => 717,
                    'interaction_type' => 'email',
                    'note' => '<p>Emails sent to ask if he is already ready to resume servies or he needs addtional time.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                213 => 
                array (
                    'client_id' => 641,
                    'created_at' => '2020-07-13 20:53:23',
                    'deleted_at' => NULL,
                    'id' => 718,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed to ask for feedback on his VA.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                214 => 
                array (
                    'client_id' => 410,
                    'created_at' => '2020-07-13 20:54:35',
                    'deleted_at' => NULL,
                    'id' => 719,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed to ask for feedback on his VA.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                215 => 
                array (
                    'client_id' => 427,
                    'created_at' => '2020-07-13 20:55:31',
                    'deleted_at' => NULL,
                    'id' => 720,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed to ask for feedback on his VA.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                216 => 
                array (
                    'client_id' => 301,
                    'created_at' => '2020-07-13 20:56:02',
                    'deleted_at' => NULL,
                    'id' => 721,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed to ask for feedback on his VA.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                217 => 
                array (
                    'client_id' => 680,
                    'created_at' => '2020-07-14 14:22:08',
                    'deleted_at' => NULL,
                    'id' => 722,
                    'interaction_type' => 'call',
                    'note' => '<p>Sent me Chris&#39; dial pad log in for her dispute. Called about VA Al&#39;s performance, she said he is a little better but she still wants him to be more productive. Asked what other else she thinks Al needs to improve on but she said to call her back since she is already late for an appointment.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                218 => 
                array (
                    'client_id' => 715,
                    'created_at' => '2020-07-14 16:43:52',
                    'deleted_at' => NULL,
                    'id' => 723,
                    'interaction_type' => 'call',
                    'note' => '<p>Called him to let him know since he already has a GVA that we can offer FT rate if he hires a new ISA for PT. As per OM. Voicemail, left message.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                219 => 
                array (
                    'client_id' => 410,
                    'created_at' => '2020-07-14 16:44:31',
                    'deleted_at' => NULL,
                    'id' => 724,
                    'interaction_type' => 'call',
                    'note' => '<p>Called for meet and greet and to touch base but vm, left message. Email sent</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                220 => 
                array (
                    'client_id' => 702,
                    'created_at' => '2020-07-14 16:45:03',
                    'deleted_at' => NULL,
                    'id' => 725,
                    'interaction_type' => 'call',
                    'note' => '<p>Called for meet and greet and to touch base but vm, left message. Email sent</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                221 => 
                array (
                    'client_id' => 177,
                    'created_at' => '2020-07-14 16:45:31',
                    'deleted_at' => NULL,
                    'id' => 726,
                    'interaction_type' => 'call',
                    'note' => '<p>Called for meet and greet and to touch base but vm, left message. Email sent</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                222 => 
                array (
                    'client_id' => 366,
                    'created_at' => '2020-07-14 16:46:32',
                    'deleted_at' => NULL,
                    'id' => 727,
                    'interaction_type' => 'call',
                    'note' => '<p>Called David the POC for Vitaly Vishnepolsky - Jefte is doing a good job so far. She was moved to other accounts this past week because they need additional help there. They would like continue using her for the near future.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                223 => 
                array (
                    'client_id' => 667,
                    'created_at' => '2020-07-14 18:45:45',
                    'deleted_at' => NULL,
                    'id' => 728,
                    'interaction_type' => 'call',
                    'note' => '<p>Called Matt the POC for Adam Mahfouda - both Zoe and Mace are doing really great. They are setting up a lot of appointment and are receptive to new information. They are both very adaptable and are able to express to information to agents.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                224 => 
                array (
                    'client_id' => 160,
                    'created_at' => '2020-07-14 20:12:17',
                    'deleted_at' => NULL,
                    'id' => 729,
                    'interaction_type' => 'call',
                'note' => '<p>Call Claudia (210) 837-5070 POC for Rick Garza - John is amazing and they love John. They have no complaints and they are very very happy.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                225 => 
                array (
                    'client_id' => 680,
                    'created_at' => '2020-07-15 19:10:16',
                    'deleted_at' => NULL,
                    'id' => 730,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed her regarding her disupute on VA Chris&#39; hours. Informed her that we will be crediting June 26th-28th only because VA Chris was working and to only show good faith. Also attached on the email are VA Chris&#39; eod for the period questioned. Credit is OM approved. She has responded she needs another VA as a replacement for VA Chris.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                226 => 
                array (
                    'client_id' => 177,
                    'created_at' => '2020-07-15 19:10:48',
                    'deleted_at' => NULL,
                    'id' => 731,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and sent a whatsapp message to touch base about her VA.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                227 => 
                array (
                    'client_id' => 301,
                    'created_at' => '2020-07-15 19:11:24',
                    'deleted_at' => NULL,
                    'id' => 732,
                    'interaction_type' => 'call',
                    'note' => '<p>Meet and greet. VA Mia is doing really great. No concerns.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                228 => 
                array (
                    'client_id' => 118,
                    'created_at' => '2020-07-15 19:11:52',
                    'deleted_at' => NULL,
                    'id' => 733,
                    'interaction_type' => 'call',
                    'note' => '<p>Call to touch. Voicemail, left message and email sent.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                229 => 
                array (
                    'client_id' => 641,
                    'created_at' => '2020-07-15 19:12:20',
                    'deleted_at' => NULL,
                    'id' => 734,
                    'interaction_type' => 'call',
                    'note' => '<p>Call to touch. Voicemail, left message and email sent.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                230 => 
                array (
                    'client_id' => 669,
                    'created_at' => '2020-07-15 20:27:34',
                    'deleted_at' => NULL,
                    'id' => 735,
                    'interaction_type' => 'call',
                    'note' => '<p><br />
Fariah&#39;s feedbak on Bea&#39;s performance:</p>

<ul>
<li style="list-style-type:disc">&ldquo;She&rsquo;s doing great. I have no complaints. She&rsquo;s on top of things and I am actually impressed! She&rsquo;s awesome!&rdquo;</li>
</ul>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                231 => 
                array (
                    'client_id' => 599,
                    'created_at' => '2020-07-15 20:38:37',
                    'deleted_at' => NULL,
                    'id' => 736,
                    'interaction_type' => 'call',
                    'note' => '<p>Adilah&#39; s Perfomance feedback for Jesa</p>

<ul>
<li style="list-style-type:disc">&ldquo;She&rsquo;s doing okay.&nbsp; Nobody&rsquo;s perfect. She&rsquo;s doing good. A few hiccups here and there but overall - She&rsquo;s been doing fine.&rdquo;</li>
</ul>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                232 => 
                array (
                    'client_id' => 536,
                    'created_at' => '2020-07-16 14:02:26',
                    'deleted_at' => NULL,
                    'id' => 737,
                    'interaction_type' => 'others',
                    'note' => '<p>Invoice collection successful. Asked for availability to discuss temporary replacement of Apple due to maternity.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                233 => 
                array (
                    'client_id' => 701,
                    'created_at' => '2020-07-16 14:03:39',
                    'deleted_at' => NULL,
                    'id' => 738,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Inquiry on the last invoice sent. Explained and discussed cut-off dates.</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                234 => 
                array (
                    'client_id' => 760,
                    'created_at' => '2020-07-16 14:04:30',
                    'deleted_at' => NULL,
                    'id' => 739,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Should be her first day with VA Erwin but was stuck on meetings the whole morning. Informed that VA was advised to do research on tasks and tools to use.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                235 => 
                array (
                    'client_id' => 756,
                    'created_at' => '2020-07-16 14:10:24',
                    'deleted_at' => NULL,
                    'id' => 740,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
First day with VA Jayson today. Provided RC link to use for the training/onboarding. Also provided references on softphone/VOIP options.</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                236 => 
                array (
                    'client_id' => 754,
                    'created_at' => '2020-07-16 14:12:29',
                    'deleted_at' => NULL,
                    'id' => 741,
                    'interaction_type' => 'others',
                    'note' => '<p>First day with VA Chris today. Training/onboarding today to discuss tasks, systems and tools to use.</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                237 => 
                array (
                    'client_id' => 538,
                    'created_at' => '2020-07-16 14:13:58',
                    'deleted_at' => NULL,
                    'id' => 742,
                    'interaction_type' => 'others',
                    'note' => '<p>Followed-up on invoice-awaiting response.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                238 => 
                array (
                    'client_id' => 730,
                    'created_at' => '2020-07-16 14:14:51',
                    'deleted_at' => NULL,
                    'id' => 743,
                    'interaction_type' => 'email',
                    'note' => '<p>Advised of paid-time of VA Lady for today.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                239 => 
                array (
                    'client_id' => 747,
                    'created_at' => '2020-07-16 14:16:02',
                    'deleted_at' => NULL,
                    'id' => 744,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Request to update Charmain&#39;s schedule starting tomorrow.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                240 => 
                array (
                    'client_id' => 536,
                    'created_at' => '2020-07-16 14:17:37',
                    'deleted_at' => NULL,
                    'id' => 745,
                    'interaction_type' => 'email',
                    'note' => '<p><br />
Decided to just wait for VA Apple until she gets back after delivery. Will temporarily have her services on hold.</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                241 => 
                array (
                    'client_id' => 680,
                    'created_at' => '2020-07-16 21:15:08',
                    'deleted_at' => NULL,
                    'id' => 746,
                    'interaction_type' => 'email',
                    'note' => '<p>Sent the resume of VA Ni&ntilde;o Ridhen Anies because she is looking to add a FT ISA.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                242 => 
                array (
                    'client_id' => 546,
                    'created_at' => '2020-07-16 21:15:55',
                    'deleted_at' => NULL,
                    'id' => 747,
                    'interaction_type' => 'call',
                    'note' => '<p>Called but vm is full. Email sent, text sent and Whatsapp message sent to follow up on his pending invioce. Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'collection',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                243 => 
                array (
                    'client_id' => 118,
                    'created_at' => '2020-07-16 21:16:31',
                    'deleted_at' => NULL,
                    'id' => 748,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                244 => 
                array (
                    'client_id' => 641,
                    'created_at' => '2020-07-16 21:17:15',
                    'deleted_at' => NULL,
                    'id' => 749,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                245 => 
                array (
                    'client_id' => 177,
                    'created_at' => '2020-07-16 21:17:46',
                    'deleted_at' => NULL,
                    'id' => 750,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition.She said she will call me next week.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                246 => 
                array (
                    'client_id' => 376,
                    'created_at' => '2020-07-16 21:18:11',
                    'deleted_at' => NULL,
                    'id' => 751,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                247 => 
                array (
                    'client_id' => 410,
                    'created_at' => '2020-07-16 21:18:36',
                    'deleted_at' => NULL,
                    'id' => 752,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                248 => 
                array (
                    'client_id' => 427,
                    'created_at' => '2020-07-16 21:19:08',
                    'deleted_at' => NULL,
                    'id' => 753,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                249 => 
                array (
                    'client_id' => 534,
                    'created_at' => '2020-07-16 21:19:34',
                    'deleted_at' => NULL,
                    'id' => 754,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                250 => 
                array (
                    'client_id' => 542,
                    'created_at' => '2020-07-16 21:20:47',
                    'deleted_at' => NULL,
                    'id' => 755,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                251 => 
                array (
                    'client_id' => 548,
                    'created_at' => '2020-07-16 21:21:09',
                    'deleted_at' => NULL,
                    'id' => 756,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                252 => 
                array (
                    'client_id' => 562,
                    'created_at' => '2020-07-16 21:23:19',
                    'deleted_at' => NULL,
                    'id' => 757,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                253 => 
                array (
                    'client_id' => 567,
                    'created_at' => '2020-07-16 21:23:40',
                    'deleted_at' => NULL,
                    'id' => 758,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                254 => 
                array (
                    'client_id' => 600,
                    'created_at' => '2020-07-16 21:24:03',
                    'deleted_at' => NULL,
                    'id' => 759,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                255 => 
                array (
                    'client_id' => 652,
                    'created_at' => '2020-07-16 21:24:30',
                    'deleted_at' => NULL,
                    'id' => 760,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition Feedback received: Michelle is doing a phenomenal job so far! We love having her on our team as our EVA. I&#39;ll definitely provide you with feedback and suggestions as they come up. Thank you!</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                256 => 
                array (
                    'client_id' => 734,
                    'created_at' => '2020-07-16 21:25:01',
                    'deleted_at' => NULL,
                    'id' => 761,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                257 => 
                array (
                    'client_id' => 443,
                    'created_at' => '2020-07-16 21:26:37',
                    'deleted_at' => NULL,
                    'id' => 762,
                    'interaction_type' => 'email',
                    'note' => '<p>Emailed and texted for meet and greet and inform about transition Feedback received: We are extremely happy with Jay&#39;s performance thus far and will let you know if we ever have any concerns in the future. Thanks for checking</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                258 => 
                array (
                    'client_id' => 710,
                    'created_at' => '2020-07-17 12:11:08',
                    'deleted_at' => NULL,
                    'id' => 763,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Discussed options when she hires another VA for Video editing / discussed current roles of her EVA</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                259 => 
                array (
                    'client_id' => 657,
                    'created_at' => '2020-07-17 12:11:42',
                    'deleted_at' => NULL,
                    'id' => 764,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Sent touch base request (awaiting response)</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'open',
                ),
                260 => 
                array (
                    'client_id' => 466,
                    'created_at' => '2020-07-17 12:12:24',
                    'deleted_at' => NULL,
                    'id' => 765,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>Discussed her referral&#39;s needs and a few billing concerns / Advised of her referral fee when referral signs up</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                261 => 
                array (
                    'client_id' => 636,
                    'created_at' => '2020-07-17 12:13:52',
                    'deleted_at' => NULL,
                    'id' => 766,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>Touch base on Toni&#39;s performance everything is ok but with minimal issues that he knows could be worked on enrolled Toni for a KW Training sponsored by him</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                262 => 
                array (
                    'client_id' => 696,
                    'created_at' => '2020-07-17 12:23:35',
                    'deleted_at' => NULL,
                    'id' => 767,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>Confirmed changes on VA rate due to update on his tasks</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                263 => 
                array (
                    'client_id' => 376,
                    'created_at' => '2020-07-17 12:40:45',
                    'deleted_at' => NULL,
                    'id' => 768,
                    'interaction_type' => 'text',
                    'note' => '<p>Feedback received: Thank you for checking in - I&#39;m very happy with Kavin. He does fantastic work. If I have any issues I will reach out to him directly then to you</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                264 => 
                array (
                    'client_id' => 548,
                    'created_at' => '2020-07-17 12:41:11',
                    'deleted_at' => NULL,
                    'id' => 769,
                    'interaction_type' => 'email',
                    'note' => '<p>Feedback received: Sheryl is doing great! Thanks for checking in!</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                265 => 
                array (
                    'client_id' => 680,
                    'created_at' => '2020-07-17 16:45:01',
                    'deleted_at' => NULL,
                    'id' => 770,
                    'interaction_type' => 'text',
                    'note' => '<p>Followed up if she received the resume for the FT ISA she is looking to add Called but voicemail, left message. WhatsApp message send and text sent She responded via WhatsApp saying Chris was not working a week prior to when she fired him. Told her that he was and she can check the EOD&#39;s I sent to her email, it contains the addresses and all the details Chris worked on. Asked her she is interested in talking to VA Nino.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                266 => 
                array (
                    'client_id' => 546,
                    'created_at' => '2020-07-17 16:45:58',
                    'deleted_at' => NULL,
                    'id' => 771,
                    'interaction_type' => 'call',
                    'note' => '<p>Called but vm is full. Email sent, text sent and Whatsapp message sent to follow up on his pending invioce. Emailed and texted for meet and greet and inform about transition. No response</p>',
                    'note_type' => 'collection',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                267 => 
                array (
                    'client_id' => 763,
                    'created_at' => '2020-07-17 21:17:48',
                    'deleted_at' => NULL,
                    'id' => 772,
                    'interaction_type' => 'call',
                    'note' => '<p>July 16, 2020</p>

<p>Saeed has advd of cancellation&nbsp;</p>

<p>- he feels that ti&#39;s too early for his buisness to get a VA</p>

<p>Has no &nbsp;more tasks for VA</p>

<p>Wants to have.a refund</p>

<p>informed of cotract -</p>

<p>will talk about concern by tom</p>

<p>========</p>

<p>July 17. 2020</p>

<p>Advd. that we will accept cancellatoni (approved by Pavel)</p>

<p>no refund as he only paid $99 for SUF</p>

<p>will only need to pay 2 days of Mitch&#39;s svc + tax. He agreed</p>',
                    'note_type' => 'notification',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                268 => 
                array (
                    'client_id' => 748,
                    'created_at' => '2020-07-17 21:24:28',
                    'deleted_at' => NULL,
                    'id' => 773,
                    'interaction_type' => 'text',
                    'note' => '<p>Added 2 hr shif every Sat for Aileen, effective tomorrow.</p>

<p>&nbsp;</p>',
                    'note_type' => 'notification',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                269 => 
                array (
                    'client_id' => 708,
                    'created_at' => '2020-07-20 16:57:31',
                    'deleted_at' => NULL,
                    'id' => 774,
                    'interaction_type' => 'email',
                    'note' => '<p>Schedule change for Wena effecitve today , July 10th&nbsp;</p>

<p>Mon - Fri&nbsp;2pm-6pm and 10pm-2am PST</p>',
                    'note_type' => 'notification',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                270 => 
                array (
                    'client_id' => 683,
                    'created_at' => '2020-07-21 18:03:44',
                    'deleted_at' => NULL,
                    'id' => 775,
                    'interaction_type' => 'text',
                    'note' => '<p>Asking when would Irish&#39;s last day be and also he hasn&#39;t received documentation on end date of contract</p>

<p>Advd that he will be receiving Cancellation docs either on last day of VA July 23 or after.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'open',
                ),
                271 => 
                array (
                    'client_id' => 674,
                    'created_at' => '2020-07-21 18:11:45',
                    'deleted_at' => NULL,
                    'id' => 776,
                    'interaction_type' => 'call',
                    'note' => '<p>Advised Derek that Adrian&#39;s i-net connection is very slow and VA is unable to make calls - VA will do MUS on Monday shfit.</p>

<p>Derek agreed.&nbsp;</p>

<p>Reminded that Adrian needs to join the 9:30AM EST meeting tom</p>',
                    'note_type' => 'notification',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                272 => 
                array (
                    'client_id' => 274,
                    'created_at' => '2020-07-21 20:31:04',
                    'deleted_at' => NULL,
                    'id' => 777,
                    'interaction_type' => 'others',
                    'note' => '<p><br />
Confirmation on timeblock schedules until December.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => 'email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                273 => 
                array (
                    'client_id' => 725,
                    'created_at' => '2020-07-21 20:31:55',
                    'deleted_at' => NULL,
                    'id' => 778,
                    'interaction_type' => 'others',
                    'note' => '<p>Client touch base; discussed updates and feedback.<br />
Informed of prioritizing coaching and calibration sessions with Ella this week. Also requested to have Ella&#39;s 10 hours be moved for next week as they are not available for appointments starting Wednesday until Saturday.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 24,
                    'others_interaction' => 'call, email and text',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                274 => 
                array (
                    'client_id' => 436,
                    'created_at' => '2020-07-21 20:32:45',
                    'deleted_at' => NULL,
                    'id' => 779,
                    'interaction_type' => 'email',
                    'note' => '<p><br />
Confirmed meeting on Wednesday to discuss annual evaluation of Clifford and her need to get another ISA.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                275 => 
                array (
                    'client_id' => 279,
                    'created_at' => '2020-07-21 20:33:14',
                    'deleted_at' => NULL,
                    'id' => 780,
                    'interaction_type' => 'email',
                    'note' => '<p>Request to update card on file.</p>',
                    'note_type' => 'notification',
                    'noted_by' => 24,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                276 => 
                array (
                    'client_id' => 770,
                    'created_at' => '2020-07-22 15:46:11',
                    'deleted_at' => NULL,
                    'id' => 781,
                    'interaction_type' => 'text',
                    'note' => '<p>&nbsp;</p>

<p>Discussed VA&#39;s need for Voip and what is needed for calls and list of VA&#39;s tasks</p>',
                    'note_type' => 'followup',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                277 => 
                array (
                    'client_id' => 653,
                    'created_at' => '2020-07-22 15:46:53',
                    'deleted_at' => NULL,
                    'id' => 782,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Discussed on Norleen&#39;s Availability and checked possibilities of adding another GVA instead</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'open',
                ),
                278 => 
                array (
                    'client_id' => 688,
                    'created_at' => '2020-07-22 15:55:54',
                    'deleted_at' => NULL,
                    'id' => 783,
                    'interaction_type' => 'others',
                    'note' => '<p>Positive feedback on Louis and addressed billing concerns and cut off periods</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => 'Whats app',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                279 => 
                array (
                    'client_id' => 428,
                    'created_at' => '2020-07-22 15:57:22',
                    'deleted_at' => NULL,
                    'id' => 784,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Touch base on Sarah&#39;s performance everything is ok and no issues with their VA</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                280 => 
                array (
                    'client_id' => 347,
                    'created_at' => '2020-07-22 15:58:03',
                    'deleted_at' => NULL,
                    'id' => 785,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Have some concerns on the scope of tasks of Margie / Call scheduled on Friday</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                281 => 
                array (
                    'client_id' => 177,
                    'created_at' => '2020-07-27 12:49:01',
                    'deleted_at' => NULL,
                    'id' => 786,
                    'interaction_type' => 'text',
                    'note' => '<p>Dawn provided positive feedback on her VA and has increased her rate to plus $0.50/hr</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                282 => 
                array (
                    'client_id' => 427,
                    'created_at' => '2020-07-27 12:53:17',
                    'deleted_at' => NULL,
                    'id' => 787,
                    'interaction_type' => 'email',
                    'note' => '<p>Client feedback: Allen has been with us 1 year, 1 month...he is doing fine...</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                283 => 
                array (
                    'client_id' => 542,
                    'created_at' => '2020-07-27 12:54:15',
                    'deleted_at' => NULL,
                    'id' => 788,
                    'interaction_type' => 'call',
                    'note' => '<p>Client feedback: Kristine is doing great. We love Kristine!</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                284 => 
                array (
                    'client_id' => 562,
                    'created_at' => '2020-07-27 12:58:20',
                    'deleted_at' => NULL,
                    'id' => 789,
                    'interaction_type' => 'call',
                    'note' => '<p>Client feedback:&nbsp;Everything is going well. He is doing a lot of things.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                285 => 
                array (
                    'client_id' => 702,
                    'created_at' => '2020-07-27 13:02:19',
                    'deleted_at' => NULL,
                    'id' => 790,
                    'interaction_type' => 'call',
                    'note' => '<p>Client feedback:&nbsp;They have been very pleased with your services as a VA. You have been great to work with, and are truly a great VA!</p>

<p>Responded to his email about his official notice of intent to terminate in 14 business days because they are going to a slightly different direction. He provided good feedback for VA Meg Spoke to Michael about his 14 days notice with intent to terminate. He said they are a new company and has recently joined Keller Williams and now on KW Half cap. They were also required to hire a local person for support by KW and due to limited funds and resources they have to take a step back. Told him that VA Meg is very skilled and they can use her as a GVA or even as a personal assistant. He said they are growing and it is not yet 100% that they are letting Meg go. They just sent in the 14 day notice because it is stated on their contract with VD. He is even thinking of getting someone to split Meg&#39;s hours with him so they could keep her. We will circle back next week.</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                286 => 
                array (
                    'client_id' => 641,
                    'created_at' => '2020-07-27 14:03:37',
                    'deleted_at' => NULL,
                    'id' => 791,
                    'interaction_type' => 'email',
                    'note' => '<p>Client feedback: She is AMAZING!!!</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                287 => 
                array (
                    'client_id' => 632,
                    'created_at' => '2020-07-27 18:45:11',
                    'deleted_at' => NULL,
                    'id' => 792,
                    'interaction_type' => 'call',
                    'note' => '<ul>
<li style="list-style-type:disc">Payments have been have been adjusted - refunded $276.56 to Jeff and charged $785.18 (total for 3 pay periods/client (50%)</li>
</ul>',
                    'note_type' => 'notification',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                288 => 
                array (
                    'client_id' => 687,
                    'created_at' => '2020-07-27 21:00:41',
                    'deleted_at' => NULL,
                    'id' => 793,
                    'interaction_type' => 'call',
                    'note' => '<p>JoAnn received CVs sent for replacement ISA.&nbsp;</p>

<p>Would like to have the interview tom at 10AMCST.</p>

<p>Advd i will message her the confirmation once i have reached out to the VAs</p>',
                    'note_type' => 'followup',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                289 => 
                array (
                    'client_id' => 777,
                    'created_at' => '2020-07-27 22:31:45',
                    'deleted_at' => NULL,
                    'id' => 794,
                    'interaction_type' => 'others',
                    'note' => '<p>Meet and greet done on Jully 20 2PM EST</p>

<p>- Schedule: M-F 9am-1pm (MAry) EST Red M-f 2:30pm-6:30pm EST (Red)</p>

<p>- Start date: July 21, 2020 shift - CRM/Tools - FMLS/ Ardox/ e-sign (won&#39;t get too many of those) TCdocs - itinerary/ tasklist; skyslope - System Logins - will be sent via email - Expectations - has some level of figure it out &lsquo;ness&rsquo;; resourceful, independent. Willingness - enjoy what they are doing. Feedback. How to&rsquo;s/ SOP - Task / Job Description - Contract specialist/ Transaction Coordinator. (8 new contracts in 3 days last week) atty/ lender/ coop agent/ sent by client&rsquo;s agent/ - Tasklist / Daily agenda - to be given to them - from TCDocs - Calendar sharing - if there&rsquo;s a need - probably will - Mode of Communication - email/ WhatsApp - - Touch base - weekly - level off - Goals - onboarded and comfortable enough - well oiled machine - for Courtney to be hands off as possible - Others - Break schedule - 15 mins - Timedly - demo done</p>',
                    'note_type' => 'others',
                    'noted_by' => 22,
                    'others_interaction' => 'Ring Centra Meeting',
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Meet and Greet',
                    'status' => 'done',
                ),
                290 => 
                array (
                    'client_id' => 733,
                    'created_at' => '2020-07-28 13:47:49',
                    'deleted_at' => NULL,
                    'id' => 795,
                    'interaction_type' => 'others',
                    'note' => '<p>&nbsp;</p>

<p>Coordinated current leads checked and possible schedule change for Ranelle. Checked if Christine is already available (for possible additional VA)</p>',
                    'note_type' => 'followup',
                    'noted_by' => 21,
                    'others_interaction' => 'Whats app',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                291 => 
                array (
                    'client_id' => 657,
                    'created_at' => '2020-07-28 13:49:04',
                    'deleted_at' => NULL,
                    'id' => 796,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Reached out for additional VA requests. Gathering profiles and asking for preference.</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Additional VA request',
                    'status' => 'ongoing',
                ),
                292 => 
                array (
                    'client_id' => 753,
                    'created_at' => '2020-07-28 13:50:02',
                    'deleted_at' => NULL,
                    'id' => 797,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Sent invoice reminder (awaiting response)/ VA on hold until invoice is settled</p>

<hr />
<p>&nbsp;</p>

<p>Sent invoice reminder (awaiting response)/ VA on hold until invoice is settled</p>',
                    'note_type' => 'collection',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'open',
                ),
                293 => 
                array (
                    'client_id' => 362,
                    'created_at' => '2020-07-28 13:51:54',
                    'deleted_at' => NULL,
                    'id' => 798,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Touch based on Derren&#39;s performance. Derren is excellent. Very dedicated and helpful.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                294 => 
                array (
                    'client_id' => 770,
                    'created_at' => '2020-07-28 13:52:35',
                    'deleted_at' => NULL,
                    'id' => 799,
                    'interaction_type' => 'text',
                    'note' => '<p>&nbsp;</p>

<p>U[dated card. New card is still not good .Attempt to charge the card(coordinated with bank )</p>',
                    'note_type' => 'collection',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                295 => 
                array (
                    'client_id' => 734,
                    'created_at' => '2020-07-29 12:25:26',
                    'deleted_at' => NULL,
                    'id' => 800,
                    'interaction_type' => 'email',
                    'note' => '<p>Client feedback: The next couple of weeks will be the real test. I keep seeing a lot of mistakes on what I need to be done but I also know he isn&#39;t doing 100% of what I really need at the moment. Kaitie has him doing some other items that need to stop. He needs to get into my role in what I need to be done ASAP. This week I will make sure he is doing the role I want and there shouldn&#39;t be any issues with a month of training if there is then I will not be happy. Informed client that I will be monitoring his VA closely this week and will schedule a coaching sesion with him too. Asked what time he is available to touch base on Monday to see if there&#39;s progress.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                296 => 
                array (
                    'client_id' => 784,
                    'created_at' => '2020-07-29 12:27:42',
                    'deleted_at' => NULL,
                    'id' => 801,
                    'interaction_type' => 'text',
                    'note' => '<p>Feedback received: Ivan is doing good thanks for asking. We are currently streamlining command and it&#39;s website.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                297 => 
                array (
                    'client_id' => 534,
                    'created_at' => '2020-07-29 12:29:52',
                    'deleted_at' => NULL,
                    'id' => 802,
                    'interaction_type' => 'call',
                    'note' => '<p>Client feedback: She is doing fine. No major concerns.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                298 => 
                array (
                    'client_id' => 641,
                    'created_at' => '2020-07-29 12:36:19',
                    'deleted_at' => NULL,
                    'id' => 803,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Client feedback: She is AMAZING!!!</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                299 => 
                array (
                    'client_id' => 776,
                    'created_at' => '2020-07-29 12:38:53',
                    'deleted_at' => NULL,
                    'id' => 804,
                    'interaction_type' => 'call',
                    'note' => '<p>Called to touch base on VA&#39;s first week. VM, left message. Email sent and text sent. Client feedback: She is doing realy great! She has been setting up appointments.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                300 => 
                array (
                    'client_id' => 781,
                    'created_at' => '2020-07-29 12:40:52',
                    'deleted_at' => NULL,
                    'id' => 805,
                    'interaction_type' => 'call',
                    'note' => '<p>Called to touch base on VA&#39;s first week. VM, left message. Email sent and text sent. Client feedback: Thanks for checking in. Things are going well so far. We are both getting acclimated with each other and the new roles.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                301 => 
                array (
                    'client_id' => 296,
                    'created_at' => '2020-07-29 19:04:16',
                    'deleted_at' => NULL,
                    'id' => 806,
                    'interaction_type' => 'email',
                    'note' => '<p>He was charged twice for the same amount on the same day</p>

<p>as per Om ask approval to deduct this on his future invoices.</p>',
                    'note_type' => 'others',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Billing inquiry',
                    'status' => 'done',
                ),
                302 => 
                array (
                    'client_id' => 504,
                    'created_at' => '2020-07-29 19:12:10',
                    'deleted_at' => NULL,
                    'id' => 807,
                    'interaction_type' => 'others',
                    'note' => '<p>Sent revised invoices from billing.</p>',
                    'note_type' => 'collection',
                    'noted_by' => 23,
                    'others_interaction' => 'call, email',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                303 => 
                array (
                    'client_id' => 775,
                    'created_at' => '2020-07-30 15:38:49',
                    'deleted_at' => NULL,
                    'id' => 808,
                    'interaction_type' => 'email',
                    'note' => '<p>John is doing well. Thanks for following up. I would like for you to work on closing the appointment with John more. He&rsquo;s done an excellent job with setting appointments for me to close the face to face listing appointment. I would like for him to become more comfortable with closing the entire sale so my only job is to show up at the listing appointment. He will need to become more familiar with landmarks in the areas we are servicing to provide context to his conversation. Possibly googling known businesses in the areas we serve would help. I&rsquo;ll also create a cheat sheet for him.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                304 => 
                array (
                    'client_id' => 702,
                    'created_at' => '2020-08-03 13:51:45',
                    'deleted_at' => NULL,
                    'id' => 809,
                    'interaction_type' => 'call',
                    'note' => '<p>Followed up on his 14-day notice with the intent of termination. He said they are leaning towards putting the contract on hold for 1 month but we will circle back on Thrusday to see what would be the final decision. He is thinking of getting someone to split his time. They have more man power now and the realtor will now to do more of the ISA work. I will cal him again on Thursday</p>',
                    'note_type' => 'followup',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                305 => 
                array (
                    'client_id' => 118,
                    'created_at' => '2020-08-03 14:34:33',
                    'deleted_at' => NULL,
                    'id' => 810,
                    'interaction_type' => 'others',
                    'note' => '<p>Messaged in google hangouts to get feedback on his VAs. Client feedback: GM! the VAs are doing well. We are very happy.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => 'Google Hangouts',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                306 => 
                array (
                    'client_id' => 789,
                    'created_at' => '2020-08-03 18:23:04',
                    'deleted_at' => NULL,
                    'id' => 811,
                    'interaction_type' => 'call',
                'note' => '<p>Maria Tubil (FT)<br />
Bartalome Inalisan (FT)<br />
Rimel Rodriguez (PT)<br />
Abigael Bulanduz (PT)</p>',
                    'note_type' => 'others',
                    'noted_by' => 18,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'M&G endorsement',
                    'status' => 'done',
                ),
                307 => 
                array (
                    'client_id' => 634,
                    'created_at' => '2020-08-03 18:54:39',
                    'deleted_at' => NULL,
                    'id' => 812,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>Raised concerns on her VA / Having difficulty on doing the conference call with her VA</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                308 => 
                array (
                    'client_id' => 751,
                    'created_at' => '2020-08-04 01:44:24',
                    'deleted_at' => NULL,
                    'id' => 813,
                    'interaction_type' => 'call',
                    'note' => '<p>Confirms CI schedule tomorrow at 10am PST. Advised candidates</p>',
                    'note_type' => 'others',
                    'noted_by' => 23,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'CI - Additional VA',
                    'status' => 'done',
                ),
                309 => 
                array (
                    'client_id' => 777,
                    'created_at' => '2020-08-05 20:51:05',
                    'deleted_at' => NULL,
                    'id' => 814,
                    'interaction_type' => 'call',
                    'note' => '<p>Aug 4, 2020 Touch base</p>

<p>Both VAs are great, however</p>

<p>Mary - in terms of admin tasks (better)&nbsp;vs TC tasks, learning curve is taking long for VA to learn TC tasls. VAs last day is Aug 5, 2020</p>

<p>Wants Red to go on Full time and has agreed to M-F&nbsp;8am-10am EST/ 2:30-8:30pm EST sched, effective on August 6, 2020</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                310 => 
                array (
                    'client_id' => 562,
                    'created_at' => '2020-08-06 12:17:50',
                    'deleted_at' => NULL,
                    'id' => 815,
                    'interaction_type' => 'call',
                    'note' => '<p>Client called to notify about terminating her contract. She likes her VA however he is not a good fit. The VA is better in marketing and social media and not very strong in Admin tasks. Until now VA does not have checklist and client is still telling him what to do. Client thinks his VA&#39;sheart is more on marketing and social media. Offered repalcement but client decided to pass but she said she will keep us mind. Client agteed to 14 days notification to give ample amount of time for Mark to get another client. Instructed client to send her 14 day notice via email. Received 14 days notification via email. Acknowledge. VA&#39;s end date is August 24th. Text from Client received: Ashley I just informed Mark, I just wanted to make sure I tell you that I told him that if in two weeks you don&#39;t find a new client for him, we could keep him for another week or so part time to hopefully give him some more time</p>',
                    'note_type' => 'notification',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                311 => 
                array (
                    'client_id' => 698,
                    'created_at' => '2020-08-07 15:32:17',
                    'deleted_at' => NULL,
                    'id' => 816,
                    'interaction_type' => 'others',
                    'note' => '<p>&nbsp;</p>

<p>Meet and greet with Aira / expectations set / initial tasks discussed and Goals for them</p>',
                    'note_type' => 'followup',
                    'noted_by' => 21,
                    'others_interaction' => 'Zoom Call',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                312 => 
                array (
                    'client_id' => 634,
                    'created_at' => '2020-08-07 15:49:21',
                    'deleted_at' => NULL,
                    'id' => 817,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Coordinated VA&#39;s condition / checked if she will be renewing TB hours or get an ISA instead .</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Renew hours',
                    'status' => 'acknowleged',
                ),
                313 => 
                array (
                    'client_id' => 567,
                    'created_at' => '2020-08-10 14:04:15',
                    'deleted_at' => NULL,
                    'id' => 818,
                    'interaction_type' => 'email',
                    'note' => '<p>Client feedback: Sorry for the delay in communication back to you it&#39;s been a whirlwind couple of weeks. With that said, Valerie has done a great job for us and we&#39;re very happy with her. She&#39;s prompt, stays on task, and has been an excellent learner of our process and systems.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                314 => 
                array (
                    'client_id' => 698,
                    'created_at' => '2020-08-12 13:54:55',
                    'deleted_at' => NULL,
                    'id' => 819,
                    'interaction_type' => 'others',
                    'note' => '<p>&nbsp;</p>

<p>New VA replacement decided not to continue and Monee (referral) not knowledgeable in online quickbooks</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => 'Whats app',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'ongoing',
                ),
                315 => 
                array (
                    'client_id' => 793,
                    'created_at' => '2020-08-12 13:56:04',
                    'deleted_at' => NULL,
                    'id' => 820,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Addressed client disputes on the charges and coordinated with OM Tima</p>',
                    'note_type' => 'collection',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                316 => 
                array (
                    'client_id' => 770,
                    'created_at' => '2020-08-12 14:00:31',
                    'deleted_at' => NULL,
                    'id' => 821,
                    'interaction_type' => 'text',
                    'note' => '<p>GM, WF is having the limit issue. I&#39;m moving funds over this morning to another account. Once that&#39;s done, I&#39;ll update you. Please forward the draft dates to ensure this doesn&#39;t happen again thanks</p>',
                    'note_type' => 'collection',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                317 => 
                array (
                    'client_id' => 752,
                    'created_at' => '2020-08-13 22:31:02',
                    'deleted_at' => NULL,
                    'id' => 822,
                    'interaction_type' => 'text',
                    'note' => '<p>8/12/2020</p>

<p>Feedback:</p>

<p>Hi. We are happy with Jason. Not too many appointments unfortunately. Will have to discuss with Donna and get back to you.</p>

<p>VA has been coached 8/13/2020</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                318 => 
                array (
                    'client_id' => 698,
                    'created_at' => '2020-08-14 12:55:15',
                    'deleted_at' => NULL,
                    'id' => 823,
                    'interaction_type' => 'others',
                    'note' => '<p>&nbsp;</p>

<p>Resolved issues on inventory / New VA was assigned with new tasks and inventories fixed / Approval on VA&#39;s shift</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => 'Whats app',
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                319 => 
                array (
                    'client_id' => 738,
                    'created_at' => '2020-08-14 12:57:34',
                    'deleted_at' => NULL,
                    'id' => 824,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Coordinated other tasks that their VA could do / requested for a touch base to discuss (awaiting response)</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                320 => 
                array (
                    'client_id' => 657,
                    'created_at' => '2020-08-14 12:58:55',
                    'deleted_at' => NULL,
                    'id' => 825,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Request for another FT ISA / will gather profiles and set for an Interview)</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA request',
                'status' => 'ongoing',
            ),
            321 => 
            array (
                'client_id' => 710,
                'created_at' => '2020-08-14 12:59:44',
                'deleted_at' => NULL,
                'id' => 826,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Submitted profiles for additional VA request with Video editing skills / will review profiles first and short list candidates</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'additional VA request',
                'status' => 'ongoing',
            ),
            322 => 
            array (
                'client_id' => 637,
                'created_at' => '2020-08-14 13:03:25',
                'deleted_at' => NULL,
                'id' => 827,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Requested for contract copy</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Contract Copy',
                'status' => 'acknowleged',
            ),
            323 => 
            array (
                'client_id' => 706,
                'created_at' => '2020-08-17 20:09:32',
                'deleted_at' => NULL,
                'id' => 828,
                'interaction_type' => 'others',
                'note' => '<p>August 12, 2020 - Touchbase with client / Leads are very few and and client cannot convert, thinking to cancel but not yet final. Informed client that VA needs to log-out for CI during her shift with him. client agreed.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => 'Call / Text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            324 => 
            array (
                'client_id' => 706,
                'created_at' => '2020-08-17 20:15:58',
                'deleted_at' => NULL,
                'id' => 829,
                'interaction_type' => 'call',
                'note' => '<p>Client applied for a different job because he&#39;s not making enough money as a real estate agent. He needs to lower down his expenses&nbsp;since he&#39;s only working part time. /&nbsp;<strong>ISA Name:</strong>&nbsp;Christiane Eunice Romero /&nbsp;<strong>Effectivity date:</strong>&nbsp;&nbsp;August 17, 2020&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation',
                'status' => 'done',
            ),
            325 => 
            array (
                'client_id' => 773,
                'created_at' => '2020-08-17 21:01:01',
                'deleted_at' => NULL,
                'id' => 830,
                'interaction_type' => 'others',
                'note' => '<p>Had a scheduled Touchbase today, however, client is not available. Called-VM/Texted - client replied to reschedule again tomorrow. no specific time yet. Will contact the client first thing tomorrow.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => 'Call / Text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            326 => 
            array (
                'client_id' => 812,
                'created_at' => '2020-08-17 22:00:48',
                'deleted_at' => NULL,
                'id' => 831,
                'interaction_type' => 'others',
                'note' => '<p>Called-VM / Texted - informed client that we will be setting up TYMBL to be his dialer and will assist his VA on how to use it / Started using the dialer today / uploaded contacts.</p>',
                'note_type' => 'followup',
                'noted_by' => 29,
                'others_interaction' => 'Called-VM / Texted',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            327 => 
            array (
                'client_id' => 698,
                'created_at' => '2020-08-18 15:52:24',
                'deleted_at' => NULL,
                'id' => 832,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Concerns on her VA&#39;s tasks for today. VA called her and explained the process (Resolved)</p>',
                'note_type' => 'consultation',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            328 => 
            array (
                'client_id' => 688,
                'created_at' => '2020-08-18 15:53:19',
                'deleted_at' => NULL,
                'id' => 833,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Requested to suspend services. (quitting his first job and transition to the new one)</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            329 => 
            array (
                'client_id' => 466,
                'created_at' => '2020-08-18 15:54:16',
                'deleted_at' => NULL,
                'id' => 834,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Awarded $50 bonus for Imee. Follow up on her other referral(Cesar G.)</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            330 => 
            array (
                'client_id' => 637,
                'created_at' => '2020-08-18 15:55:09',
                'deleted_at' => NULL,
                'id' => 835,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Threatened to cancel due to some errors committed by the VA. Checked and asked why was did not communicated during the past touch bases (opt for a replacement)</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            331 => 
            array (
                'client_id' => 770,
                'created_at' => '2020-08-18 15:56:16',
                'deleted_at' => NULL,
                'id' => 836,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Offered replacement as Nhick is resigning / wants to discuss tomorrow at 9am an looking for someone with high C on disc</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            332 => 
            array (
                'client_id' => 508,
                'created_at' => '2020-08-18 23:09:30',
                'deleted_at' => NULL,
                'id' => 837,
                'interaction_type' => 'call',
            'note' => '<p>Gina is upset &nbsp;Replacement VA&nbsp;sent out wrong email. She feels that the VA isn&#39;t as smart as Norleen (prev VA)</p>

<p>Comparing Aileen from Nog</p>

<p>Explained to her that VA wasn&#39;t fully trained/ endorsement not fully done/&nbsp;</p>

<p>There were tasks that Aileen weren&#39;t informed of</p>

<p>Gina has agreed to help train her for the next 2 weeks&nbsp;</p>

<p>Would prefer for Aileen to reach out and send her a message on questions she wanted to ask.</p>

<p>Referrals:&nbsp;</p>

<p>Brad Finsilver-</p>

<p>Lynne Gewant - Coldwell</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            333 => 
            array (
                'client_id' => 397,
                'created_at' => '2020-08-19 19:41:45',
                'deleted_at' => NULL,
                'id' => 838,
                'interaction_type' => 'text',
                'note' => '<p>August 18, 2020</p>

<p>Ff up with Janki on add&#39;l VA &nbsp;</p>

<p>Offered to put Rich on FT instead as he now has freed up hour.</p>

<p>Discussed rates</p>

<p>Aug 19, 2020</p>

<p>Janki agreed to put Rich on FT - effetive tom, Aug 20, 2020 shift</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            334 => 
            array (
                'client_id' => 802,
                'created_at' => '2020-08-19 19:48:01',
                'deleted_at' => NULL,
                'id' => 839,
                'interaction_type' => 'email',
                'note' => '<p>Bindya has requested to have a group call with Janki&#39;s VA - for sharing of Best practices.</p>

<p>Scheduled for Friday, August 21, 2020</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Group Call - Sharing of Best practices',
                'status' => 'pending',
            ),
            335 => 
            array (
                'client_id' => 740,
                'created_at' => '2020-08-20 00:27:59',
                'deleted_at' => NULL,
                'id' => 840,
                'interaction_type' => 'others',
                'note' => '<p>Touch base with Client / went on a vacation last week that&#39;s why she was not so responsive / Planning to have a weekly meeting with her VA / Everything is doing great and the VA never failed to send her a report / Talked about Sept. 7 Holiday / Client chose Option 3 - No work with PAY</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => 'Call / Text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            336 => 
            array (
                'client_id' => 735,
                'created_at' => '2020-08-20 00:31:59',
                'deleted_at' => NULL,
                'id' => 841,
                'interaction_type' => 'call',
                'note' => '<p>Talked to Client and would like to cancel one of her VA since he cannot get 5 appointments per week from circle prospecting but she has nothing against the VAs attitude and work etiquette - Jester Carl Muldong / Client doesn&#39;t have any problems with the other VA - Jayson Javier / VA Cancellation will be effective on Monday, August 24, 2020</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            337 => 
            array (
                'client_id' => 773,
                'created_at' => '2020-08-20 00:33:22',
                'deleted_at' => NULL,
                'id' => 842,
                'interaction_type' => 'others',
                'note' => '<p>Touch base with client / Very happy with his VA / Client&#39;s main concern is how can he transfer all the task that he has on his mind to his VA / Set an action plan and client will also figure out if they need to make changes on the VA&#39;s schedule.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => 'Call / Text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            338 => 
            array (
                'client_id' => 762,
                'created_at' => '2020-08-20 02:00:56',
                'deleted_at' => NULL,
                'id' => 843,
                'interaction_type' => 'call',
                'note' => '<p>Touch base with client / They are very happy with the VA / &quot;It was a very good decission in hiring Jackie. I have trained dozens of dozens of agents and we have 80 people using our system. And if I were to think of who is the gold star performer, Jackie is our newest member and she&#39;s already one of the toppest user of our system. She understands it, she learn quickly, she&#39;s already knowing what to do without me having to repeat myelf several times like I have to do with a lot of our agents. She also became very skilled on the telephone with our clients.&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            339 => 
            array (
                'client_id' => 778,
                'created_at' => '2020-08-20 02:04:18',
                'deleted_at' => NULL,
                'id' => 844,
                'interaction_type' => 'text',
                'note' => '<p>Called Quinn several times and we&#39;re trying to create an action plan to make a very goo team work with her VA, however, Quinn sound like she&#39;s still not satisfied with how the VA works for her&nbsp;/ Received a text message / asking if I can find her another candidate who is detail oriented, self starter, who can follow instructions, productive and efficient, can prepare offer, lease, listing packet, can do some marketing as well / informed the client that we&#39;ll be looking for a replacement for her VA</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            340 => 
            array (
                'client_id' => 669,
                'created_at' => '2020-08-20 02:16:28',
                'deleted_at' => NULL,
                'id' => 845,
                'interaction_type' => 'call',
                'note' => '<p>Faria has advd that she will be ending her contract</p>

<p>- restucturing</p>

<p>- hired an in house assistant</p>

<p>aware of 14 biz days notif</p>

<p>VA to work until Sept 9, 2020</p>

<p>HAs referred Sunny Chen with ph #: 610-5001091</p>

<p>Will be goving 2 more referrals</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            341 => 
            array (
                'client_id' => 751,
                'created_at' => '2020-08-21 03:20:15',
                'deleted_at' => NULL,
                'id' => 846,
                'interaction_type' => 'call',
                'note' => '<p>Additional tasks for VAs Melvin and Joshua, contacting firms to set up showings for the agents in the team</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            342 => 
            array (
                'client_id' => 665,
                'created_at' => '2020-08-21 03:20:51',
                'deleted_at' => NULL,
                'id' => 847,
                'interaction_type' => 'email',
                'note' => '<p>Confirms additional hours for Jannesse effective on Mon, Aug 24th<br />
M-F: 11am - 6pm<br />
Saturday: 9 am &ndash; 2 PM</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            343 => 
            array (
                'client_id' => 806,
                'created_at' => '2020-08-21 03:21:36',
                'deleted_at' => NULL,
                'id' => 848,
                'interaction_type' => 'call',
                'note' => '<p>CI schedule tomorrow with VA Shekah at 10am PST</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            344 => 
            array (
                'client_id' => 650,
                'created_at' => '2020-08-21 03:22:20',
                'deleted_at' => NULL,
                'id' => 849,
                'interaction_type' => 'text',
                'note' => '<p>Invoice inquiry, informed next payment be due on the 25th. She will give me her updated card tomorrow</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            345 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-08-24 15:02:48',
                'deleted_at' => NULL,
                'id' => 850,
                'interaction_type' => 'call',
                'note' => '<p>Asked me to call her. She is very frustrated with VA Iryn. She doesn&#39;t check her work, a card that can be finished in 1 day took her 1 week to finish because she doesn&#39;t follow instructions. Dana sent a layout and all she has to do change pictures of the houses with the 2020 listings that they did but VA Iryn changed the entire layout and added 2019 listings. Dawn had to tell her again in bold letters what she wants done. She also sent a ust sold card as a template with her branding but again Iryn created a new card. Iryn also has not done sold card for the last 3 transactions. Dawn is reminding her VA on what to do instead of the other way around. Dawn is recently diagnosed with cancer and when got hospitalized the more she realized Iryn has not been doing a lot of her tasks. Dawn always need to remind VA to take signs down on each and every property sold. She need the VA to be on her game now that she is sick and she needs to do her tasks, she has been with the client for 1 year already. She is giving Iryn 30 days to prove herself or else she will be replaced.<br />
<br />
Informed Dawn that I will do a coaching session with Iryn. We will set up trackers, checklist and reminders.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            346 => 
            array (
                'client_id' => 534,
                'created_at' => '2020-08-25 14:57:25',
                'deleted_at' => NULL,
                'id' => 851,
                'interaction_type' => 'call',
                'note' => '<p>Client touch base. She said VA Ro is working slow during the days that they are not busy, there&#39;s a task that she is supposed to finish faster though it&#39;s detious. Asked when SODs and EODs should be sent, explained. She said she just wants to make sure that the VA is busy and does not want to get her in trouble. Does not plan to cancel or replace her. Informed that I will schedule a coaching session with VA Ro to discuss her feedback.<br />
<br />
Informed that I have spoken to her VA Ro. Ro&#39;s client before is from KW and the screenshot she saw was because VA was not able to close the tabs since there&#39;s no break from her 1st shift to 2nd shift. VA commited to send her SODs and EODs on tine and consitently. Her BombBomb tasks incudes delteing 1,300 contacts manually and includes 3 clicks each contact before she can sucessfully delete them. Informed that I will keep an eye on VA&#39;s tasks to make sure she is accomplshing more during her shift.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            347 => 
            array (
                'client_id' => 680,
                'created_at' => '2020-08-25 17:52:05',
                'deleted_at' => NULL,
                'id' => 852,
                'interaction_type' => 'others',
                'note' => '<p>Sent a Whatsapp message saying VA Nino only made 6 phone calls yesterday and VA Al is not converting leases.</p>

<p>Here is VA Nino&rsquo;s explanation: -- Firstly, just want to say thank you for giving me the opportunity and time to explain myself. Just want to say that whatever tasks that Sara gave me over the course of 3 weeks, I accepted it and worked on it, wholeheartedly. The first week was a little bit shaky because I was new. Without any training or orientation of the tasks that I need to work on I was able to adjust right away. My tasks for that week was to call property owners, to know their home availabilities, and to make follow-ups on inquiries and to homeowners for leases, and send emails for inquiries as well. In my second week, I am still doing the same tasks from the previous week, and I was given the login credentials for RedX/Vortex, was able to export expired leads, and was able to work on some of them. We were also given an additional task to work on &quot;Not listed with NS&quot; file to call and email for potential new listings. Now in my third week, Sara, together with TL Ashley, and I had a call. On that conversation, she was able to specify the tasks that I need to focus on, including the cities I should be focusing on. From that point on, it was made clear to me that I need to call potential home seller leads. I was able to book a couple of appointments to Sara form the &quot;Not listed on NS&quot; file, and I was able to call a handful of numbers, given the fact that I need to do the skip tracing manually. Note that the table included in my EOD reports are just properties that I was able to call, talks to the right person, and send emails to. You can check my dialpad&#39;s call logs if you wish to. Last week, Friday, was the time Sara told me to look for fancier, pricier homes. I raised that those properties are mostly registered under an LLC ownership, and told her I was having a hard time searching for a valid contact reference. She then told Saiza regarding the concern, and Saiza just told me she does it like how I do it. and there are no tools that I can use to make the searching easier. Sara then sent me another file for properties that are over 5 Million dollars that I need to call. This file, the same as the &quot;Not listed in NS&quot; file, doesn&#39;t have any contact info yet. So I&#39;ve been doing manual skip tracing for these two files. I&#39;ve been using BeenVerified, tax mapping website, whitepages, and any website I came across google to help me get a valid contact, both for individual and LLC ownerships. Yesterday, I was given an extra task to look for a butler/housekeeper for one of Sara&#39;s clients. For the first half of my shift, I worked on google to search for possible agencies and cleaning services for the said client. I was able to confirm, 6 agencies/cleaning services companies, and that information includes the pricing and contact information for them. Check on EOD 8/18/2020 for reference. You can also check my dialpad&#39;s call logs for numbers that I was able to call but was not included on my EOD as those were just VMs. In the second half of my shift, after lunch, Sara already knew that I will be working on the &quot;over 5 mil&quot; file. I already told her that before I took my lunch because I needed to make sure if the choices for the cleaning services company was enough for her client. She said yes, and told her that I will be working then on our listings. Yesterday I listed 6 properties on my EOD form our file. I listed those properties because, those are the properties that I was able to leave a VM, sent an email, or I was able to talk to a person. Also note that for my EOD report, I didn&#39;t include those numbers that I wasn&#39;t sure I got a correct phone number, correct email, or I wasn&#39;t able to get any contact information. You can still check my dialpad&#39;s call logs for your reference. I hope I was able to explain myself. If you have any additional questions or clarifications, please, do not hesitate to contact me. Thanks again for your time. I hope you all stay safe. -- Here is VA Al&rsquo;s explanation: The 4 searches that she is referring to might be the client named Chirag. I sent emails to homes for their availabilities for 1 week in Thanksgiving week and only 4 homes are available. Most of the homes would only do a minimum of 2 weeks. It might look like I&#39;m sending a few searches for old clients just because it is just an update of what available properties came in during the day. I am sending now 10-20 searches per new client inquiry depending on price and location because we already have a pre-made folder for them. I am already informed by Saiza that I was sending a few properties and asked her how can I improve. I was told to send properties above the price range of the client which I am doing now. Clients and homeowners sometimes doesn&#39;t meet their criteria for rentals that&#39;s why rentals for both properties do not push through. Clients do not want to give the asking price of the owner, some clients have pets that the owner doesn&#39;t want in his home. The owner wants only a family rental and not a group or shared house but the client is with his friends during the rental. Those are some reasons that are why deals fell. Some clients already have a broker in place and just checking if we can send them available properties, or they are already getting something in other sites like Airbnb. -- I have checked both their End of Reports and Screenshots and they are both working and very busy with their tasks. Moving forward, I have instructed them to do a time and motion which they would need to include time stamps on when they started a task and ended it on the End of Day Reports they will be sending you. Here in Virtudesk, we make sure that your VAs are working and you can always check their call logs and screenshots. I can offer you a replacement for both VAs but if you take this route you would need to train the VAs again from the ground up. Whatever decision you might arrive at, please let me know and I&rsquo;ll be happy to assist you.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => 'WhatsApp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            348 => 
            array (
                'client_id' => 674,
                'created_at' => '2020-08-25 20:15:52',
                'deleted_at' => NULL,
                'id' => 853,
                'interaction_type' => 'call',
                'note' => '<p>Aug 25, 2020</p>

<p>Replacemet ISA CI done today with Lizzy and Meg at 1pm EST</p>

<p>Continuation CI tom Aug 26 at 2pm EST</p>

<p>Erwin, Jester, Fritz</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Replacement ISA Interview',
                'status' => 'done',
            ),
            349 => 
            array (
                'client_id' => 635,
                'created_at' => '2020-08-25 22:32:28',
                'deleted_at' => NULL,
                'id' => 854,
                'interaction_type' => 'text',
                'note' => '<p>Placed the account on temporary&nbsp;suspension on Aug 11 advg of resumption in a week&#39;s time - leads aren&#39;t good</p>

<p>ff up don on Aug 17 - asked for extension on hold. Advd that VA Rich may not be available as we can&#39;t allow our VAs to have no client for more than a week</p>

<p>Aug 25 - advd that he would like to finish off the month and is aware that VA Rich may not be available when he decides to resume svcs</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            350 => 
            array (
                'client_id' => 768,
                'created_at' => '2020-08-26 16:40:09',
                'deleted_at' => NULL,
                'id' => 855,
                'interaction_type' => 'email',
                'note' => '<p>Rebecca Williams-&nbsp;&nbsp; &nbsp;Coordinated on pending tasks from Bryan. Would like to check if I can reach out to him. Updated on removing Bryan&#39;s number of hours last week.</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            351 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-08-26 16:40:55',
                'deleted_at' => NULL,
                'id' => 856,
                'interaction_type' => 'email',
                'note' => '<p>Approved VAs bonuses for this cut-off. Followed-up on Labor Day attendance.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            352 => 
            array (
                'client_id' => 785,
                'created_at' => '2020-08-26 16:43:21',
                'deleted_at' => NULL,
                'id' => 857,
                'interaction_type' => 'others',
            'note' => '<p>Updated on the new lead source (RedX) that she signed up for Brianna.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            353 => 
            array (
                'client_id' => 184,
                'created_at' => '2020-08-26 16:44:27',
                'deleted_at' => NULL,
                'id' => 858,
                'interaction_type' => 'others',
                'note' => '<p>Client touch base; provided positive feedback on Laradeth&#39;s tasks</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            354 => 
            array (
                'client_id' => 805,
                'created_at' => '2020-08-26 16:52:41',
                'deleted_at' => NULL,
                'id' => 859,
                'interaction_type' => 'others',
                'note' => '<p>Positive feedback on VA LJ: &quot;She&#39;s been excellent. Better than expected! It&#39;s making me realize I don&#39;t have my own systems in place&quot;</p>

<p>Request to have continuous script practice.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            355 => 
            array (
                'client_id' => 725,
                'created_at' => '2020-08-26 16:54:17',
                'deleted_at' => NULL,
                'id' => 860,
                'interaction_type' => 'others',
                'note' => '<p>Unhappy with his VA&#39;s performance. Discussed ways on how we are helping her VA on getting successful appointments. Provided options including the VA profiles that were sent to him for review. Will talk to his son to check the best option for them.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            356 => 
            array (
                'client_id' => 559,
                'created_at' => '2020-08-26 16:55:18',
                'deleted_at' => NULL,
                'id' => 861,
                'interaction_type' => 'email',
                'note' => '<p>Approved paid holiday on September 7 for VA Lady.<br />
&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            357 => 
            array (
                'client_id' => 681,
                'created_at' => '2020-08-26 23:14:06',
                'deleted_at' => NULL,
                'id' => 862,
                'interaction_type' => 'call',
                'note' => '<p>August 26, 2020</p>

<p>Replacement ISA CI done today</p>

<p>Ranked interviewees:</p>

<p>1. Meg - MArgaret Pineda</p>

<p>2. Erwin Olivas</p>

<p>3. Fritz Laigo</p>

<p>4. Jester</p>

<p>====</p>

<p>Meg attended another CI and since no decision has been given yet - Meg was booked by another client &nbsp;- advd Oliver</p>

<p>==========</p>

<p>Oliver doesn&#39;t want to choose among the remaining ISA applicants</p>

<p>Sent another batch of CVs for repalcement ISA, no update on CI date yet</p>

<p>multiple follow up on date and time via text and call (routed to VM)</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Replacement ISA Interview',
                'status' => 'open',
            ),
            358 => 
            array (
                'client_id' => 824,
                'created_at' => '2020-08-27 15:50:19',
                'deleted_at' => NULL,
                'id' => 863,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Meet and greet with VA Angelo / discuss the initial tasks / tools and logins / setting of expectations / discussed Timedly ans sent credentials</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Meet and greet',
                'status' => 'done',
            ),
            359 => 
            array (
                'client_id' => 162,
                'created_at' => '2020-08-27 15:50:59',
                'deleted_at' => NULL,
                'id' => 864,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Invoice collection / coordinated error with the bank /</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            360 => 
            array (
                'client_id' => 637,
                'created_at' => '2020-08-27 15:52:00',
                'deleted_at' => NULL,
                'id' => 865,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Provided Kris&#39;s last tasks and provided feedback</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            361 => 
            array (
                'client_id' => 696,
                'created_at' => '2020-08-27 15:52:35',
                'deleted_at' => NULL,
                'id' => 866,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Invoice collection / successful</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            362 => 
            array (
                'client_id' => 634,
                'created_at' => '2020-08-27 15:54:34',
                'deleted_at' => NULL,
                'id' => 867,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<hr />
<p>Interviewed Kris hired decided to have start date on Monday</p>

<hr />
<p>Coordinated VA logins and provided important reminders prior to Kris start date</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Conversion from TB to PT EVA',
                'status' => 'done',
            ),
            363 => 
            array (
                'client_id' => 761,
                'created_at' => '2020-08-27 19:37:45',
                'deleted_at' => NULL,
                'id' => 868,
                'interaction_type' => 'others',
                'note' => '<p>Client paid her remaining balance / Will send a cancellation email / Client will hire an onshore assistant.</p>',
                'note_type' => 'collection',
                'noted_by' => 29,
                'others_interaction' => 'Email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            364 => 
            array (
                'client_id' => 712,
                'created_at' => '2020-08-27 19:45:15',
                'deleted_at' => NULL,
                'id' => 869,
                'interaction_type' => 'call',
                'note' => '<p>August 26, 2020 - Client will buy more leads with Agent Locator / US lead generators cannot cater Canada / Client needs to lower the hours of the VA in order for him to pay more leads / Toronto charges $15 to $25 per lead / We&#39;ll talk to him again tomorrow to finalize his plans</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            365 => 
            array (
                'client_id' => 792,
                'created_at' => '2020-08-27 19:50:07',
                'deleted_at' => NULL,
                'id' => 870,
                'interaction_type' => 'others',
                'note' => '<p>Touch base with client / Reported the plans that we have for her VA / Client thinks that the VA has a lot of potential, however, she cannot train her in TC / She requested us to train her VA outside her shift</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => 'Called-VM/Texted/Emailed',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            366 => 
            array (
                'client_id' => 780,
                'created_at' => '2020-08-27 22:43:21',
                'deleted_at' => NULL,
                'id' => 871,
                'interaction_type' => 'call',
                'note' => '<p>Touch base with Client / Able to talk with her business partner Mike Ugarte /&nbsp;Wanted to go Fulltime starting on Monday / Explained the process and rates / Got a great feedback - &quot;We feel that she&#39;s doing great, she does her work, we tell her what we need to get done and she get&#39;s it done, she&#39;s actually exceeded our expectation like us setting appointments, she&#39;s an eager learner which is good and we&#39;re very happy with her.&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            367 => 
            array (
                'client_id' => 778,
                'created_at' => '2020-08-27 22:45:24',
                'deleted_at' => NULL,
                'id' => 872,
                'interaction_type' => 'others',
                'note' => '<p>Supposed to interview the VA - did not showed up / Called-VM/texted/emailed / no response</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => 'Called-VM/Texted/Emailed',
                'others_status' => 'Please specify status type',
            'others_type' => 'Client Interview (replacement)',
                'status' => 'done',
            ),
            368 => 
            array (
                'client_id' => 736,
                'created_at' => '2020-08-27 22:47:02',
                'deleted_at' => NULL,
                'id' => 873,
                'interaction_type' => 'call',
                'note' => '<p>Touchbase with Client / Called in and got a great feedback - &quot;She&#39;s doing really well, she&#39;s been learning our system, she works very hard and I&#39;m very happy with it.&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            369 => 
            array (
                'client_id' => 371,
                'created_at' => '2020-08-28 15:40:52',
                'deleted_at' => NULL,
                'id' => 874,
                'interaction_type' => 'others',
                'note' => '<p>Notification on cancellation as there were no tasks available for Aisha. Provided positive feedback on Aisha and VD services.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            370 => 
            array (
                'client_id' => 768,
                'created_at' => '2020-08-28 15:42:15',
                'deleted_at' => NULL,
                'id' => 875,
                'interaction_type' => 'others',
                'note' => '<p>Requested to move touch base next week as she is still identifying if she will get another VA or will just add hours for Norwin.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            371 => 
            array (
                'client_id' => 774,
                'created_at' => '2020-08-28 21:24:30',
                'deleted_at' => NULL,
                'id' => 876,
                'interaction_type' => 'email',
                'note' => '<p>August 27, 2020</p>

<p>Feedback:</p>

<p>&quot;She&rsquo;s been great.&quot;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            372 => 
            array (
                'client_id' => 777,
                'created_at' => '2020-08-28 21:29:02',
                'deleted_at' => NULL,
                'id' => 877,
                'interaction_type' => 'text',
                'note' => '<p>Feedback for VA:</p>

<p>&quot;Going well thanks&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            373 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-08-28 21:32:02',
                'deleted_at' => NULL,
                'id' => 878,
                'interaction_type' => 'text',
                'note' => '<p>Touch base and ff up on add&#39;l hours:</p>

<p>Feeback:</p>

<p>&quot;Jesa&#39;s been doing good. Yes, I do plan on adding hours. The client said he&#39;ll be getting started in the second week of Sept. I&#39;m planning on doing a meet &amp; greet with him and Jesa next week on a Zoom.&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            374 => 
            array (
                'client_id' => 494,
                'created_at' => '2020-09-01 03:06:19',
                'deleted_at' => NULL,
                'id' => 879,
                'interaction_type' => 'email',
                'note' => '<p>Will add a part time GVA&nbsp;</p>

<p>&quot;I will need marketing expert for google business and social media&quot;</p>

<p>Sent VA Hanna&#39;s profile</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            375 => 
            array (
                'client_id' => 605,
                'created_at' => '2020-09-01 03:07:33',
                'deleted_at' => NULL,
                'id' => 880,
                'interaction_type' => 'call',
                'note' => '<p>Rescheduled Ci tomorrow with TC Redeemer at 2pm EST</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            376 => 
            array (
                'client_id' => 657,
                'created_at' => '2020-09-02 13:31:43',
                'deleted_at' => NULL,
                'id' => 881,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>VA interview (Harrold and Jayson) will provide their chosen VA tomorrow.</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Interview',
                'status' => 'done',
            ),
            377 => 
            array (
                'client_id' => 755,
                'created_at' => '2020-09-02 13:36:26',
                'deleted_at' => NULL,
                'id' => 882,
                'interaction_type' => 'email',
                'note' => '<p>She has been amazing!! Thank you so much!!</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            378 => 
            array (
                'client_id' => 733,
                'created_at' => '2020-09-02 13:40:16',
                'deleted_at' => NULL,
                'id' => 883,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Checked if he have decided on the profiles sent</p>

<hr />
<p>I just invested in a expansion for KV core. They are setting up. This will give us some more managing capabilities. This is a long weekend here , let&rsquo;s talk next week and discuss the possibility of working leads</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            379 => 
            array (
                'client_id' => 674,
                'created_at' => '2020-09-02 18:06:31',
                'deleted_at' => NULL,
                'id' => 884,
                'interaction_type' => 'call',
                'note' => '<p>Continuation of Replacement ISA</p>

<p>Derek interviewed:</p>

<p>Fritz Laigo, Jester Muldong and Erwin Olivas</p>

<p>Derek chose Erwin for replacement ISA -&nbsp;</p>

<p>To start on Monday, Aug 31, 2020</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Replacement CI Continuation - Aug 27',
                'status' => 'done',
            ),
            380 => 
            array (
                'client_id' => 779,
                'created_at' => '2020-09-02 18:42:30',
                'deleted_at' => NULL,
                'id' => 885,
                'interaction_type' => 'email',
                'note' => '<p>Aug 27, 2020</p>

<p>&quot;Great better than I expected. We make a good team. By the way Katrina and I noticed that the last invoice charge us 10.60 let hr. We were under the impression that we would be charged 9.60 per hr since we have JoJo working with us.&quot;</p>

<p>Acknowledged feedback - provided to VAs</p>

<p>query on rate has been forwarded to OM - advd client that it would be adjuste on next invoice</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            381 => 
            array (
                'client_id' => 793,
                'created_at' => '2020-09-04 15:26:58',
                'deleted_at' => NULL,
                'id' => 886,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Discussed on the 14th day notice as we do not offer spanish speaking agents and informed that spanish speaking VA&#39;s rate may be different from the usual ISA rate (awaiting response)</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation',
                'status' => 'ongoing',
            ),
            382 => 
            array (
                'client_id' => 797,
                'created_at' => '2020-09-04 22:42:43',
                'deleted_at' => NULL,
                'id' => 887,
                'interaction_type' => 'call',
                'note' => '<p>Requested for an additional ISA that would be doing similar tasks that Vange and MAe are doing.</p>

<p>Ok for newbies but better with hubspot experience</p>

<p>Confirmed that this is an additiona ISA different from what she asked from James. total of 4 VAs (including Vange and Mae)</p>

<p>Will be under a diff company name (included in grp of companies) for invoice/ billing purposes</p>

<p>Will be sending CVs of ISAs. for CI nexr week</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional ISA request',
                'status' => 'pending',
            ),
            383 => 
            array (
                'client_id' => 362,
                'created_at' => '2020-09-08 17:42:18',
                'deleted_at' => NULL,
                'id' => 888,
                'interaction_type' => 'email',
                'note' => '<p>Sent touch base and heads up on referral signed up.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            384 => 
            array (
                'client_id' => 641,
                'created_at' => '2020-09-08 18:11:53',
                'deleted_at' => NULL,
                'id' => 889,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: She is doing amazingly well! Thank you for the follow up.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            385 => 
            array (
                'client_id' => 160,
                'created_at' => '2020-09-08 18:12:31',
                'deleted_at' => NULL,
                'id' => 890,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: I hope your afternoon is going well.<br />
We have nothing but good things to say about John!<br />
He is amazing and a huge part of our team.<br />
He is always willing to help and go above and beyond for us and our clients.<br />
We are grateful for him and his professionalism.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            386 => 
            array (
                'client_id' => 443,
                'created_at' => '2020-09-08 18:13:13',
                'deleted_at' => NULL,
                'id' => 891,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Super, my VA is the best</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            387 => 
            array (
                'client_id' => 775,
                'created_at' => '2020-09-08 18:13:54',
                'deleted_at' => NULL,
                'id' => 892,
                'interaction_type' => 'call',
                'note' => '<p>Check-in. She said she likes her VA very much but he is having a hard time coverting leads. Goal set is 10/week and the VA is not reaching the goal. She said by now he should already have learned how to handle objections and has tweaked the scripts to make it on his own. Informed that I will have the VA train in handling objections and I will check in with her again after a couple of days to see if there&#39;s any improvement.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            388 => 
            array (
                'client_id' => 667,
                'created_at' => '2020-09-08 18:14:35',
                'deleted_at' => NULL,
                'id' => 893,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Both Mace and Zoe are doing great! They are both very professional and are great at communicating with me and the agents they&#39;re contacting. Very happy to have them both!</p>',
                'note_type' => 'followup',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            389 => 
            array (
                'client_id' => 427,
                'created_at' => '2020-09-08 18:15:19',
                'deleted_at' => NULL,
                'id' => 894,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: he is doing fine..thanks</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            390 => 
            array (
                'client_id' => 692,
                'created_at' => '2020-09-08 18:16:30',
                'deleted_at' => NULL,
                'id' => 895,
                'interaction_type' => 'email',
                'note' => '<p>Check in. Client feedback: Everything is going great with RJ. He&rsquo;s exceeded our expectations. No training or improvements are needed at this moment.Everything is going great with RJ. He&rsquo;s exceeded our expectations. No training or improvements are needed at this moment.</p>',
                'note_type' => 'followup',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            391 => 
            array (
                'client_id' => 565,
                'created_at' => '2020-09-08 18:17:26',
                'deleted_at' => NULL,
                'id' => 896,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. He is very happy with his VA but the only things is his system is a bit slow. He asked if how much if he upgrades the VAs system, told him I will ask the VA and update him.</p>',
                'note_type' => 'followup',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            392 => 
            array (
                'client_id' => 652,
                'created_at' => '2020-09-08 18:18:22',
                'deleted_at' => NULL,
                'id' => 897,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: I apologize I was not able to answer you call today. I cannot be more satisfied with Michelle&#39;s work ethics, abilities, and professionalism. She has been a great asset to my Real Estate Team and I plan to keep her on board.<br />
<br />
I will definitely be giving her an appreciation bonus at the end of the year. Thank you!<br />
<br />
Yes I have considered hiring her full-time.<br />
<br />
Depending on if we hit over half of our Team Volume Goal, I would like to hire her on full-time next year. I will keep you posted!<br />
&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            393 => 
            array (
                'client_id' => 784,
                'created_at' => '2020-09-08 18:36:07',
                'deleted_at' => NULL,
                'id' => 898,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: My VA is doing great.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            394 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-09-08 18:36:46',
                'deleted_at' => NULL,
                'id' => 899,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: I think we&rsquo;re going to have to find me a new assistant. Once again, she asked me about a form signature. This form was sent to her At the beginning of the week for HER to execute. She then asks me this morning that the buyer still needs to sign it. Really???? Don&rsquo;t you READ what your executing. I would think that since you&rsquo;ve been put on notice for this behavior, you would take extra care to do your work correctly.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            395 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-09-08 18:37:15',
                'deleted_at' => NULL,
                'id' => 900,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: All of our VA start doing an amazing job. Thank you for following up.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            396 => 
            array (
                'client_id' => 693,
                'created_at' => '2020-09-08 18:37:49',
                'deleted_at' => NULL,
                'id' => 901,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: She is doing great</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            397 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-09-09 18:39:49',
                'deleted_at' => NULL,
                'id' => 902,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Margaret&#39;s doing a good job for me. Thanks for the check in.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            398 => 
            array (
                'client_id' => 734,
                'created_at' => '2020-09-09 18:40:44',
                'deleted_at' => NULL,
                'id' => 903,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Sorry for the late reply. He is doing well, I will have to work with him a little more. Still a few things I keep having to fix but Kaitie who I fired didn&#39;t give him the training I needed. Overall very happy with him.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            399 => 
            array (
                'client_id' => 797,
                'created_at' => '2020-09-09 22:11:37',
                'deleted_at' => NULL,
                'id' => 904,
                'interaction_type' => 'email',
            'note' => '<p>will be adding another ISA but will be under a different company&#39;s name (part of grp of companies) - for billing/ invoice purposes.</p>

<p>CI on thursday, Sept 10 at 4pm EST</p>

<p>RC meeting invite sent</p>

<p>=======</p>

<p>Added Jaela Ebrada for GVA</p>

<p>Added Carla Monique Fernandez for ISA</p>

<p>Both VAs stated training with client on Sept 14, 2020</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            400 => 
            array (
                'client_id' => 824,
                'created_at' => '2020-09-10 14:06:27',
                'deleted_at' => NULL,
                'id' => 905,
                'interaction_type' => 'others',
                'note' => '<p>Hi Nica! I saw your email also, just haven&#39;t had a chance to respond!&nbsp;<br />
Angelo has been doing great! It is a learning curve for both of us! He has edited some video for me. Done some PDF forms. Set up some email templates. &nbsp;Very polite. Very attentive. &nbsp;As we both develop into our roles, he will soon be so good I won&#39;t know what to do!!!</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            401 => 
            array (
                'client_id' => 716,
                'created_at' => '2020-09-10 14:07:41',
                'deleted_at' => NULL,
                'id' => 906,
                'interaction_type' => 'email',
                'note' => '<p>Billing clarfication</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            402 => 
            array (
                'client_id' => 508,
                'created_at' => '2020-09-14 21:42:00',
                'deleted_at' => NULL,
                'id' => 907,
                'interaction_type' => 'call',
                'note' => '<p>Gina has advised that she will be cancelling svcs.&nbsp;</p>

<p>She finds Aileen to be an enthusiastic learner and is able to do tasks being assigned to her but is still not as good as Norleen.</p>

<p>Replacement GVA has been offered previously and is offered again but she doesn&#39;t need/ want a replacement as there are very minimal tasks for now.&nbsp;</p>

<p>Advd Gina of the 14 biz day notification pd and advd that Cancellation will be effective on Oct 1, 2020</p>

<p>Gina would like to resume svcs in December or January once her selling season starts</p>

<p>email and call</p>

<p>OM informed and approved</p>',
                'note_type' => 'notification',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            403 => 
            array (
                'client_id' => 397,
                'created_at' => '2020-09-14 21:56:32',
                'deleted_at' => NULL,
                'id' => 908,
                'interaction_type' => 'text',
                'note' => '<p>Janki has advised that she will be adding 1 FT ISA and a PT EVA/ GVA.</p>

<p>She is currently building her expansion team. Will&nbsp;be getting add&#39;l ISA and EVA within a month&#39;s time.</p>

<p>Janki has also referred Amit Illa - messaged via FB lab coats group page (comments)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            404 => 
            array (
                'client_id' => 755,
                'created_at' => '2020-09-15 14:08:07',
                'deleted_at' => NULL,
                'id' => 909,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>card successfully charged</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            405 => 
            array (
                'client_id' => 509,
                'created_at' => '2020-09-15 14:09:02',
                'deleted_at' => NULL,
                'id' => 910,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Confirmed and approved Rome&#39;s annual increase / Discussed rates and checked possibility to convert full time</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Annual Tenure',
                'status' => 'done',
            ),
            406 => 
            array (
                'client_id' => 777,
                'created_at' => '2020-09-16 16:18:22',
                'deleted_at' => NULL,
                'id' => 911,
                'interaction_type' => 'call',
                'note' => '<p>Coutney wanted a straight shift for Red</p>

<p>New schedule: 9AM-5pm EST, effective tom.</p>

<p>All good</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            407 => 
            array (
                'client_id' => 657,
                'created_at' => '2020-09-16 17:02:56',
                'deleted_at' => NULL,
                'id' => 912,
                'interaction_type' => 'others',
                'note' => '<p>Christian Baker / Steffan Hutchinson</p>

<p>&nbsp;</p>

<p>Hired their 4th VA Jayson Balmes ISA tasks</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => 'Ring Central',
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA',
                'status' => 'done',
            ),
            408 => 
            array (
                'client_id' => 797,
                'created_at' => '2020-09-16 18:54:01',
                'deleted_at' => NULL,
                'id' => 913,
                'interaction_type' => 'call',
                'note' => '<p>Beatrice will be making changes on what Vange and Mae are doing.</p>

<p>Current process - when they send the quote - it goes to save team and it is processed by Save team until it is closed</p>

<p>update - once quotes and deivery are done - Beatrice wants them to handle the clients/ customers until closing</p>

<p>Also wants to give an incentive/ bonus to Vange and Mae. (would like Carla (new ISA) to get incentives as well)</p>

<p>Feedback: &quot; They are awesome. Evangeline and Mae are great!&quot;</p>

<p>=======</p>

<p>update on Sept 17, 2020</p>

<p>Shared google link to place a review</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            409 => 
            array (
                'client_id' => 779,
                'created_at' => '2020-09-16 19:30:58',
                'deleted_at' => NULL,
                'id' => 914,
                'interaction_type' => 'text',
                'note' => '<p>Touch base:</p>

<p>Feedback on Oliver and Joana: &quot; They&#39;ve been doing a great job I&#39;m beginning to make listing appointments&quot;</p>

<p>Yvonne still has a dipute on rate - it is still at $10.75 but was informed it will be at $9.60/ hr once Joana is added (becomes FT rate)</p>

<p>Advd that i was informed that it was adjusted on last invoice.&nbsp;</p>

<p>Advd i will validate with manager and billing dept</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            410 => 
            array (
                'client_id' => 359,
                'created_at' => '2020-09-17 17:45:43',
                'deleted_at' => NULL,
                'id' => 915,
                'interaction_type' => 'text',
                'note' => '<p>touch base:</p>

<p>Feedback:&nbsp;&quot;Rich is great&quot;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            411 => 
            array (
                'client_id' => 845,
                'created_at' => '2020-09-17 17:51:09',
                'deleted_at' => NULL,
                'id' => 916,
                'interaction_type' => 'email',
                'note' => '<p>New TB tasks given</p>

<p>I need to make a modification to the Mailchimp Email Template.&nbsp; I need the footer changed. &nbsp;</p>

<p>&nbsp;</p>

<p>Above my phone number in the Footer,&nbsp;<strong>Delete</strong>:&nbsp; The Luxury Real Estate Authority</p>

<p>Please make my footer smaller and delete my photograph and center it all so it doesn&rsquo;t look lopsided.&nbsp; Also, instead of my email, please add:&nbsp;www.BonnieHeatzig.com</p>

<p>&nbsp;</p>

<p>Also, I would like the colors of my logo and all the text to be the same navy blue of the BEX LOGO (the round circle).</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            412 => 
            array (
                'client_id' => 654,
                'created_at' => '2020-09-18 00:37:35',
                'deleted_at' => NULL,
                'id' => 917,
                'interaction_type' => 'others',
                'note' => '<p>She will assign new tasks for Shekah</p>

<p>KW Command database management</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => 'email , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            413 => 
            array (
                'client_id' => 852,
                'created_at' => '2020-09-18 14:24:27',
                'deleted_at' => NULL,
                'id' => 918,
                'interaction_type' => 'email',
                'note' => '<p>No complaints.&nbsp;RJ has been on time, willing and ready to work, and has done a good job with what I&#39;ve asked so far.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            414 => 
            array (
                'client_id' => 600,
                'created_at' => '2020-09-18 14:28:10',
                'deleted_at' => NULL,
                'id' => 919,
                'interaction_type' => 'text',
                'note' => '<p>Sorry for the lack of response. Everything is fine. I&#39;m extremely busy and have limited time but thanks for attempting to gather feedback.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            415 => 
            array (
                'client_id' => 857,
                'created_at' => '2020-09-21 17:43:59',
                'deleted_at' => NULL,
                'id' => 920,
                'interaction_type' => 'others',
                'note' => '<p>First day with VA Cess&nbsp;today. No responses on all communications made.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            416 => 
            array (
                'client_id' => 730,
                'created_at' => '2020-09-21 17:46:28',
                'deleted_at' => NULL,
                'id' => 921,
                'interaction_type' => 'others',
                'note' => '<p>September 14-&nbsp;Notification on cancellation after 14 days as she hired an onshore agent already. She mentioned that she needed someone to be there physically to perform her tasks.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            417 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-09-21 17:47:56',
                'deleted_at' => NULL,
                'id' => 922,
                'interaction_type' => 'email',
                'note' => '<p>September 15-Coordinated on Jan&#39;s request to render half day only today as he was not feeling well. Approved MUS schedule.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            418 => 
            array (
                'client_id' => 730,
                'created_at' => '2020-09-21 17:49:09',
                'deleted_at' => NULL,
                'id' => 923,
                'interaction_type' => 'others',
                'note' => '<p>September 15-Informed that cancellation with VA Lady will just be a temporary one as she might be coming back after a month or two.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            419 => 
            array (
                'client_id' => 857,
                'created_at' => '2020-09-21 17:50:34',
                'deleted_at' => NULL,
                'id' => 924,
                'interaction_type' => 'others',
                'note' => '<p>September 15-Responded after multiple communications were made. Informed that they will start with the service tomorrow.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            420 => 
            array (
                'client_id' => 805,
                'created_at' => '2020-09-21 17:52:10',
                'deleted_at' => NULL,
                'id' => 925,
                'interaction_type' => 'email',
                'note' => '<p>September 15-Coordinated on her inquiry regarding her last invoice.</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            421 => 
            array (
                'client_id' => 785,
                'created_at' => '2020-09-21 17:54:21',
                'deleted_at' => NULL,
                'id' => 926,
                'interaction_type' => 'others',
                'note' => '<p>September 16-Decided to go with another VA. Will send profiles to review so we can proceed with the CI. Requested to get at least 3-5 VA profiles.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            422 => 
            array (
                'client_id' => 865,
                'created_at' => '2020-09-21 17:58:12',
                'deleted_at' => NULL,
                'id' => 927,
                'interaction_type' => 'others',
                'note' => '<p>September 18-<br />
First day assistance with their new VA Kristine. Forwarded information on where to get US numbers and Google Voice set-up.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            423 => 
            array (
                'client_id' => 399,
                'created_at' => '2020-09-21 17:59:33',
                'deleted_at' => NULL,
                'id' => 928,
                'interaction_type' => 'others',
                'note' => '<p><br />
September 18-Confirmed resumption of VA services in the first week of October. Requested to have VA Apple back.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            424 => 
            array (
                'client_id' => 725,
                'created_at' => '2020-09-21 18:02:49',
                'deleted_at' => NULL,
                'id' => 929,
                'interaction_type' => 'others',
                'note' => '<p><br />
September 18-Sent requested call recordings of VA Ella.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            425 => 
            array (
                'client_id' => 788,
                'created_at' => '2020-09-21 18:03:39',
                'deleted_at' => NULL,
                'id' => 930,
                'interaction_type' => 'email',
                'note' => '<p><br />
September 18-Confirmed touch base schedule for next week. Inform about possible call over the weekend to ask for feedback regarding Brianna&#39;s task.-client acknowledged</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            426 => 
            array (
                'client_id' => 162,
                'created_at' => '2020-09-23 17:17:11',
                'deleted_at' => NULL,
                'id' => 931,
                'interaction_type' => 'text',
                'note' => '<p>Hired their 5th VA - Jester Muldong</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA',
                'status' => 'acknowleged',
            ),
            427 => 
            array (
                'client_id' => 858,
                'created_at' => '2020-09-23 17:22:05',
                'deleted_at' => NULL,
                'id' => 932,
                'interaction_type' => 'others',
                'note' => '<p>He is on the most urgent task right now. I need that done right away.<br />
I can&rsquo;t send him another task without completing the one he is on.<br />
Thanks for checking in.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            428 => 
            array (
                'client_id' => 738,
                'created_at' => '2020-09-23 17:27:06',
                'deleted_at' => NULL,
                'id' => 933,
                'interaction_type' => 'email',
                'note' => '<p>Confirmed cancellation as to they could no longer think or provide any other tasks for the VA.</p>',
                'note_type' => 'consultation',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            429 => 
            array (
                'client_id' => 875,
                'created_at' => '2020-09-23 17:48:36',
                'deleted_at' => NULL,
                'id' => 934,
                'interaction_type' => 'call',
                'note' => '<p>Sept 18</p>

<p>- Shortlisted Briana. wnated to see her artworks and get referrences<br />
- decided not to hire Briana because Michael Hassel told her that he wants Briana to do FT when in fact Michael has not expressed any interest to bring Briana to a FT post.<br />
- Over promised that she will get a VA that is using Mac products. Required that the VA is using iphone and Mac computer.<br />
- Sandra decided not to move forward with VD.</p>',
                'note_type' => 'notification',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            430 => 
            array (
                'client_id' => 872,
                'created_at' => '2020-09-24 18:48:56',
                'deleted_at' => NULL,
                'id' => 935,
                'interaction_type' => 'call',
                'note' => '<p>Successfully hired Keishon Aban and discussed target start date on Sept 28 and meet and greet Sept 25</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Interview',
                'status' => 'done',
            ),
            431 => 
            array (
                'client_id' => 162,
                'created_at' => '2020-09-24 18:55:24',
                'deleted_at' => NULL,
                'id' => 936,
                'interaction_type' => 'text',
                'note' => '<p>Confirmed start date and discussed schedule of their 5th VA</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Schedule',
                'status' => 'acknowleged',
            ),
            432 => 
            array (
                'client_id' => 733,
                'created_at' => '2020-09-24 18:57:39',
                'deleted_at' => NULL,
                'id' => 937,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Checked when he would like to interview his VA replacement</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => 'Replacement VA follow up',
                'status' => 'pending',
            ),
            433 => 
            array (
                'client_id' => 867,
                'created_at' => '2020-09-28 18:30:13',
                'deleted_at' => NULL,
                'id' => 938,
                'interaction_type' => 'others',
                'note' => '<p>September 21-&nbsp;First day onboarding with VA Mary. Discussed their process and system.</p>

<p>September 22-Request to offset VA Mary&#39;s schedule tomorrow for Friday.</p>

<p>September 24-Coordinated on her requested overtime for VA Mary Anne.</p>

<p>September 25-Checked first week&#39;s performance of VA Mary Anne-very satisfied.</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            434 => 
            array (
                'client_id' => 785,
                'created_at' => '2020-09-28 18:32:09',
                'deleted_at' => NULL,
                'id' => 939,
                'interaction_type' => 'others',
                'note' => '<p>September 21-Followed-up on sent VA profiles for replacement.</p>

<p>September 25-Followed-up on VA profiles sent. Checked if she&#39;s ready to proceed with the CI.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            435 => 
            array (
                'client_id' => 843,
                'created_at' => '2020-09-28 18:34:39',
                'deleted_at' => NULL,
                'id' => 940,
                'interaction_type' => 'others',
                'note' => '<p>September 21-&nbsp;Followed-up on RJ&#39;s tasks. Send GVA tasks recommendations.</p>

<p>September 22-Informed of his regular tasks for VA RJ on video editing tasks and logos/brochures. Acknowledged receiving GVA tasks recommendation.</p>

<p>September 25-Followed-up tasks for VA RJ. Sent text and email to check her availability for a call to discuss other services.</p>

<p>&nbsp;</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => 'call, text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            436 => 
            array (
                'client_id' => 788,
                'created_at' => '2020-09-28 18:36:51',
                'deleted_at' => NULL,
                'id' => 941,
                'interaction_type' => 'others',
                'note' => '<p>September 21-&nbsp;Requested to get his invoice last cut-off to compare if he&#39;s going to switch VA Brianna into full time.</p>

<p>September 22-Sent requested invoices. Followed-up on his decision to either add more hours for VA Brianna or have her on FT</p>

<p>September 23-Requested to update Brianna&#39;s schedule.</p>

<p>September 25-Informed that Brianna is not available to add more hours as she was already hired by another client. Follow-up call next week to check if he will still be needing an additional VA instead</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            437 => 
            array (
                'client_id' => 482,
                'created_at' => '2020-09-28 18:39:16',
                'deleted_at' => NULL,
                'id' => 942,
                'interaction_type' => 'email',
                'note' => '<p>September 22-Confirmed commission bonuses for Kumi, Kat and JC.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            438 => 
            array (
                'client_id' => 760,
                'created_at' => '2020-09-28 18:40:34',
                'deleted_at' => NULL,
                'id' => 943,
                'interaction_type' => 'others',
                'note' => '<p>September 23-Request to offset Erwin&#39;s schedule on September 29 to October 3. Provided positive feedback. &quot;He is doing awesome, great feedback from my clients/leads on his calls. His canva/instagram skills are amazing, and my instagram following is increasing dramatically!&quot;</p>',
                'note_type' => 'consultation',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            439 => 
            array (
                'client_id' => 839,
                'created_at' => '2020-09-28 18:42:31',
                'deleted_at' => NULL,
                'id' => 944,
                'interaction_type' => 'others',
                'note' => '<p>September 23-&nbsp;Reached out to discuss VA Vanessa&#39;s issue on their dialer. Informed of support&#39;s instructions on how to fix the issue.</p>

<p>September 24-Informed of positive feedback on Vanessa&#39;s performance: &quot;Vanessa has done an exceptional job. She has provided valuable input in the creation of my business process. She is flexible and always willing to help.&quot;<br />
Checked for his schedule availability to discuss dialer recommendations (awaiting response).</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            440 => 
            array (
                'client_id' => 857,
                'created_at' => '2020-09-28 18:43:35',
                'deleted_at' => NULL,
                'id' => 945,
                'interaction_type' => 'others',
                'note' => '<p>September 24- Checked for feedback on his VA&#39;s performance. &quot;She is very persistent and willing to learn.&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            441 => 
            array (
                'client_id' => 767,
                'created_at' => '2020-09-28 18:44:44',
                'deleted_at' => NULL,
                'id' => 946,
                'interaction_type' => 'others',
                'note' => '<p>September 24-Notification on Jenielou&#39;s absence due to internet outage. Approved for MUS.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            442 => 
            array (
                'client_id' => 826,
                'created_at' => '2020-09-28 18:46:08',
                'deleted_at' => NULL,
                'id' => 947,
                'interaction_type' => 'others',
                'note' => '<p>September 24-Provided positive feedback on Inna&#39;s performance: &quot;She has been doing well ,responding to all my calls and texts.&quot; Inna has been promptly with her job.&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            443 => 
            array (
                'client_id' => 731,
                'created_at' => '2020-09-28 18:47:10',
                'deleted_at' => NULL,
                'id' => 948,
                'interaction_type' => 'others',
                'note' => '<p>September 24-Notification on Jenielou&#39;s absence due to internet outage. Approved for MUS.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            444 => 
            array (
                'client_id' => 777,
                'created_at' => '2020-09-28 20:14:31',
                'deleted_at' => NULL,
                'id' => 949,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;ff up on payment $897.87&nbsp;</p>

<p>no other cc to be used</p>

<p>was flagged a few days ago and had it lifted. Courtney asked ot process the payment again.</p>

<p>Payment processed - all good</p>

<p><br />
&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            445 => 
            array (
                'client_id' => 508,
                'created_at' => '2020-09-28 20:24:33',
                'deleted_at' => NULL,
                'id' => 950,
                'interaction_type' => 'email',
                'note' => '<p>validation of VAs last day</p>

<ul>
<li style="list-style-type:disc">Advd per 14 biz day notif - VA&rsquo;s last day is on Sept 30</li>
<li style="list-style-type:disc">Doesn&rsquo;t want VA to log in anymore today until cancellation date</li>
</ul>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            446 => 
            array (
                'client_id' => 381,
                'created_at' => '2020-09-28 20:28:15',
                'deleted_at' => NULL,
                'id' => 951,
                'interaction_type' => 'text',
                'note' => '<p>Followed up payment -</p>

<p>Erin&#39;s card was put on hold due to what she seemed to be suspicious activity, but has already been fixed</p>

<p>payment has been processed and confirmed today.</p>

<p>All good</p>

<p><br />
&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            447 => 
            array (
                'client_id' => 632,
                'created_at' => '2020-09-28 20:29:45',
                'deleted_at' => NULL,
                'id' => 952,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;ff up on payment for $276.56&nbsp;</p>

<ul>
<li style="list-style-type:disc">Will have it fixed ASAP</li>
<li style="list-style-type:disc">VAlidated which credit card</li>
<li style="list-style-type:disc">Payment has been processed</li>
</ul>

<p style="list-style-type:disc">All good</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            448 => 
            array (
                'client_id' => 528,
                'created_at' => '2020-09-28 21:48:25',
                'deleted_at' => NULL,
                'id' => 953,
                'interaction_type' => 'call',
                'note' => '<p>Follow up on payment</p>

<p>provided temp cc information</p>

<p>All good</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            449 => 
            array (
                'client_id' => 657,
                'created_at' => '2020-09-29 15:25:11',
                'deleted_at' => NULL,
                'id' => 954,
                'interaction_type' => 'email',
                'note' => '<p>Steffan HutchinsonTouch base with Jayson B&#39;s performance and happy with Jayson&#39;s performance and progress</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            450 => 
            array (
                'client_id' => 428,
                'created_at' => '2020-09-29 15:25:49',
                'deleted_at' => NULL,
                'id' => 955,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Provided new referral that needs a marketing VA / contacted referral</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            451 => 
            array (
                'client_id' => 864,
                'created_at' => '2020-09-29 15:26:31',
                'deleted_at' => NULL,
                'id' => 956,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Sent touch base request (awaiting request)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            452 => 
            array (
                'client_id' => 634,
                'created_at' => '2020-09-29 15:27:39',
                'deleted_at' => NULL,
                'id' => 957,
                'interaction_type' => 'call',
                'note' => '<p>explain about bill and pay periods. Settled invoice.</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            453 => 
            array (
                'client_id' => 668,
                'created_at' => '2020-09-29 15:28:47',
                'deleted_at' => NULL,
                'id' => 958,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Sent touch base request (awaiting request)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            454 => 
            array (
                'client_id' => 751,
                'created_at' => '2020-09-30 00:50:48',
                'deleted_at' => NULL,
                'id' => 959,
                'interaction_type' => 'others',
                'note' => '<p>CI schedule tomorrow at 4pm PST</p>

<p>Candidates:</p>

<p>Rhey Kenneth Ico&nbsp;</p>

<p>Hanna Cobong</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => 'Call , text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            455 => 
            array (
                'client_id' => 660,
                'created_at' => '2020-09-30 13:59:22',
                'deleted_at' => NULL,
                'id' => 960,
                'interaction_type' => 'email',
                'note' => '<p>Sent total VA hours for Ore Living VA&#39;s</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            456 => 
            array (
                'client_id' => 866,
                'created_at' => '2020-09-30 14:00:26',
                'deleted_at' => NULL,
                'id' => 961,
                'interaction_type' => 'email',
                'note' => '<p>Burt is doing great...a very hard, caring, and thoughtful VA!&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p>Aside from a little bit of a learning curve with getting him familiar&nbsp;with our goals and processes, we have no complaints.</p>

<p>&nbsp;</p>

<p>He has been able to set a few appointments for us, so we&#39;re happy with the results, so far.&nbsp;&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            457 => 
            array (
                'client_id' => 682,
                'created_at' => '2020-10-01 16:43:38',
                'deleted_at' => NULL,
                'id' => 962,
                'interaction_type' => 'email',
                'note' => '<p>Converted Va from Part time to full time</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Conversion',
                'status' => 'acknowleged',
            ),
            458 => 
            array (
                'client_id' => 797,
                'created_at' => '2020-10-01 23:23:53',
                'deleted_at' => NULL,
                'id' => 963,
                'interaction_type' => 'text',
                'note' => '<p>Feedback:</p>

<ul>
<li style="list-style-type:disc">&ldquo;Both are great! I am very happy with my 4 VA&#39;s. Have a great day/ night&rdquo;</li>
</ul>

<p style="list-style-type:disc">Invoicing for all 4 VAs</p>

<p>Mae + Evangeline = NFI</p>

<p>Jaela = NFI but separate invoice, same CC - please. process payment separately</p>

<p>Carla = Design Mark (company name) with different cc (please send cc authorization form)</p>

<p style="list-style-type:disc">&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            459 => 
            array (
                'client_id' => 882,
                'created_at' => '2020-10-01 23:27:16',
                'deleted_at' => NULL,
                'id' => 964,
                'interaction_type' => 'call',
                'note' => '<p>Feedback:</p>

<ul>
<li style="list-style-type:disc">&ldquo;She&rsquo;s been doing great! She&rsquo;s booked 8 appts. She&rsquo;s doing an awesome job! We&rsquo;re tracking everything.&rdquo;&nbsp;</li>
<li style="list-style-type:disc">I would love to do a gift or bonus as a surprise (once an actual deal goes thru)</li>
<li style="list-style-type:disc">Advd that on bonus - amt would depend on her and it will be charged on her cc. It will be received by VA in full</li>
</ul>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            460 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-10-05 13:31:40',
                'deleted_at' => NULL,
                'id' => 965,
                'interaction_type' => 'text',
                'note' => '<p>Check-in. Client feedback: He&#39;s great!</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            461 => 
            array (
                'client_id' => 864,
                'created_at' => '2020-10-05 13:32:17',
                'deleted_at' => NULL,
                'id' => 966,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Touch base on Jenyly&#39;s performance and said everything is great so far with her VA</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            462 => 
            array (
                'client_id' => 542,
                'created_at' => '2020-10-05 13:32:22',
                'deleted_at' => NULL,
                'id' => 967,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Annual increase approved. We love Kristine!</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            463 => 
            array (
                'client_id' => 868,
                'created_at' => '2020-10-05 13:34:08',
                'deleted_at' => NULL,
                'id' => 968,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Meet and greet facilitated with VA David discussed process and Timedly.</p>

<p>- Schedule: 10am to 2pm EST - Start date: Oct 5, 2020 - CRM Tools - will be sent thru gdrive tasks: Help with gsuite / assist on Social Media posts future tasks to be discussed</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => 'Ring Central Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'meet and greet',
                'status' => 'done',
            ),
            464 => 
            array (
                'client_id' => 395,
                'created_at' => '2020-10-05 21:38:22',
                'deleted_at' => NULL,
                'id' => 969,
                'interaction_type' => 'call',
                'note' => '<p>Meet and Greet with Reynoir</p>

<ul>
<li style="list-style-type:disc">Schedule: 9am-1pm CST</li>
<li style="list-style-type:disc">Start date: October 6, Tuesday</li>
<li style="list-style-type:disc">CRM/Tools: RC; MLS; active campaign - CRM; Sierra Interactive; Canva; Soc/MEd (FB, Linked in/ IG) Back End, Intranet, Docusign</li>
<li style="list-style-type:disc">System Logins - to be sent via email</li>
<li style="list-style-type:disc">Expectations - half the time - to be dialling, circle prospecting, lead ff up, feedback on showings/ other half - to organize soc/ med posts/ listings everyday/ check buyers if there&rsquo;s any properties, make sure that all contacts are moved to specific categories</li>
<li style="list-style-type:disc">Role play 15 mins per day</li>
<li style="list-style-type:disc">Task / Job Description - Executive Assistant</li>
<li style="list-style-type:disc">Tasklist / Daily agenda - to be discussed - touch base for 15 mins</li>
<li style="list-style-type:disc">Leads to call - leads would be in Sierra Interactive (google paper click leads) - top funnel leads/ ff up and communication - circle prospecting - Cole REalty resource - for Prospecting leads</li>
<li style="list-style-type:disc">Selling points/Track record/Fact sheet - to be created</li>
<li style="list-style-type:disc">Scripts - will be available - videos of scripts that Rey can watch</li>
<li style="list-style-type:disc">Time in between appointments-&nbsp; 2 hrs in between</li>
<li style="list-style-type:disc">VM strategy - not leave any messages for the time being</li>
<li style="list-style-type:disc">Calendar sharing - to be shared</li>
<li style="list-style-type:disc">Mode of Communication - text and call/ WhatsApp</li>
<li style="list-style-type:disc">Touch base - weekly -&nbsp;</li>
<li style="list-style-type:disc">Goals - ideally - 1 appt/ day/ bet 5 and 7 a week</li>
<li style="list-style-type:disc">15 min break</li>
<li style="list-style-type:disc">Timedly demo done</li>
</ul>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            465 => 
            array (
                'client_id' => 885,
                'created_at' => '2020-10-05 21:42:16',
                'deleted_at' => NULL,
                'id' => 970,
                'interaction_type' => 'call',
                'note' => '<p>M&amp;G with Ashley and Venus</p>

<ul>
<li style="list-style-type:disc">Schedule: M-F 1pm-5pm CST</li>
</ul>

<ul>
<li style="list-style-type:disc">Start date: Oct 12, 2020</li>
<li style="list-style-type:disc">CRM/Tools: TBD</li>
<li style="list-style-type:disc">System Logins - to be sent via email</li>
<li style="list-style-type:disc">Expectations - client data to be placed on new CRM/ soc med aspect</li>
<li style="list-style-type:disc">Task/ Job Description - Transaction Coordination and Marketing (Soc Med)</li>
<li style="list-style-type:disc">Tasklist / Daily agenda - to be given</li>
<li style="list-style-type:disc">Calendar sharing - g suite</li>
<li style="list-style-type:disc">Mode of Communication - WhatsApp/ google hangouts/ email</li>
<li style="list-style-type:disc">Touch base - weekly (month) monthly - text</li>
<li style="list-style-type:disc">Goals - to get software up and running and input data/ soc med marketing/ mail goal: get transaction check list set up</li>
<li style="list-style-type:disc">15 min break</li>
<li style="list-style-type:disc">Timedly demo done</li>
</ul>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            466 => 
            array (
                'client_id' => 710,
                'created_at' => '2020-10-06 18:01:21',
                'deleted_at' => NULL,
                'id' => 971,
                'interaction_type' => 'call',
                'note' => '<p>Concerns on Toni&#39;s performance and challenges. Submitted game plan and provided sample of the list.</p>',
                'note_type' => 'consultation',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            467 => 
            array (
                'client_id' => 397,
                'created_at' => '2020-10-06 21:59:30',
                'deleted_at' => NULL,
                'id' => 972,
                'interaction_type' => 'call',
                'note' => '<p>Feedback: no notes left in CRM</p>

<ul>
<li style="list-style-type:disc">Rich not leaving comments in CRM</li>
<li style="list-style-type:disc">Used to convert good - not seeing that right now</li>
<li style="list-style-type:disc">When Rich Keeps saying - this is not working and that&rsquo;s not working - Janki is flexible and able to cancel subscriptions and get what is working with him but</li>
<li style="list-style-type:disc">When Janki calls - able to convert them</li>
<li style="list-style-type:disc">Janki is very flexible on what he wants -&nbsp;</li>
<li style="list-style-type:disc">if there are no answers - How often do you call them?</li>
<li style="list-style-type:disc">If not a 100% better - would prefer not to stop svc with VD and will just make calls&nbsp;</li>
</ul>

<p style="list-style-type:disc">Advd that for yday&#39;s issue - VA was looking for an appt to make hence, he forgot to leave notes</p>

<p style="list-style-type:disc">But needs to be consistent - coaching will be done</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            468 => 
            array (
                'client_id' => 184,
                'created_at' => '2020-10-07 16:39:18',
                'deleted_at' => NULL,
                'id' => 973,
                'interaction_type' => 'others',
                'note' => '<p>9/30-&nbsp;Payment collection successful through Venmo.</p>',
                'note_type' => 'collection',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            469 => 
            array (
                'client_id' => 785,
                'created_at' => '2020-10-07 16:40:21',
                'deleted_at' => NULL,
                'id' => 974,
                'interaction_type' => 'others',
                'note' => '<p>9/28-&nbsp;Followed-up on VA profiles sent. Checked if she&#39;s ready to proceed with the CI.</p>

<p>9/29-Sent cancellation today. Approved by OM Tima.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            470 => 
            array (
                'client_id' => 730,
                'created_at' => '2020-10-07 16:43:19',
                'deleted_at' => NULL,
                'id' => 975,
                'interaction_type' => 'others',
                'note' => '<p>9/28 - Austin Ratcliff-Confirmed that VA will be doing tasks with Kelsey until October 1. Informed of cancellation process.</p>

<p>10/1-Last day with VA Lady. Informed that she is happy with VA Lady and that she might be back soon once her system is already in place.</p>

<p>10/2-Sent a thank you letter and confirmed if all tasks are transitioned by Lady.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            471 => 
            array (
                'client_id' => 843,
                'created_at' => '2020-10-07 16:45:34',
                'deleted_at' => NULL,
                'id' => 976,
                'interaction_type' => 'others',
                'note' => '<p>9/28-Reached out to confirm RJ&#39;s tasks on logos and designs.</p>

<p>&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            472 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-10-07 16:48:52',
                'deleted_at' => NULL,
                'id' => 977,
                'interaction_type' => 'email',
            'note' => '<p>Laura Wey (9/28)-Acknowledged Jan&#39;s notification on connectivity issues. Approved MUS if needed.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            473 => 
            array (
                'client_id' => 274,
                'created_at' => '2020-10-07 16:50:43',
                'deleted_at' => NULL,
                'id' => 978,
                'interaction_type' => 'email',
                'note' => '<p>9/28-Renewal for another 20 hours timeblock services with Aldrin.</p>',
                'note_type' => 'collection',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            474 => 
            array (
                'client_id' => 839,
                'created_at' => '2020-10-07 16:54:17',
                'deleted_at' => NULL,
                'id' => 979,
                'interaction_type' => 'others',
                'note' => '<p>9/29-Provided Voip/softphone options including Tymbl for review.</p>

<p>10/2-Requested to update Vanessa&#39;s schedule effective Monday.</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            475 => 
            array (
                'client_id' => 436,
                'created_at' => '2020-10-07 16:56:19',
                'deleted_at' => NULL,
                'id' => 980,
                'interaction_type' => 'email',
                'note' => '<p>9/29-Request to give $100 birthday gift to VA Clifford.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            476 => 
            array (
                'client_id' => 608,
                'created_at' => '2020-10-07 16:58:22',
                'deleted_at' => NULL,
                'id' => 981,
                'interaction_type' => 'others',
                'note' => '<p>9/29-Acknowledged VA Ro&#39;s request for time-off. / Provided VA profiles for review. Tentative CI schedule on Thursday.</p>

<p>9/30-Confirmed interviewing all candidates sent to him for additional VA. Requested to have Ro participate on the CI.</p>

<p>10/1-Had a CI to identify his next VA. Shortlisted 2 candidates.</p>

<p>10/2-Had a second round of interview with the shortlisted candidates. Proceed with Charmain as his new VA. Discussed schedule and start date. Also confirmed the next process.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            477 => 
            array (
                'client_id' => 788,
                'created_at' => '2020-10-07 17:00:51',
                'deleted_at' => NULL,
                'id' => 982,
                'interaction_type' => 'others',
                'note' => '<p>9/30-Request for touch base sent. Will discuss updates and feedback on Brianna&#39;s tasks and to confirm approval on Brianna&#39;s leave request.</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            478 => 
            array (
                'client_id' => 867,
                'created_at' => '2020-10-07 17:02:38',
                'deleted_at' => NULL,
                'id' => 983,
                'interaction_type' => 'others',
                'note' => '<p>9/30-Confirmed her request for additional 5 hours for VA Mary Anne to start this week.</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            479 => 
            array (
                'client_id' => 756,
                'created_at' => '2020-10-07 17:11:29',
                'deleted_at' => NULL,
                'id' => 984,
                'interaction_type' => 'others',
                'note' => '<p>10/1-&nbsp;Considering to cancel her services as calls will now be made by his buyer agent. Recommended other tasks that the VA can still do for him. Provided list of GVA tasks. Asked to be called back tomorrow.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            480 => 
            array (
                'client_id' => 887,
                'created_at' => '2020-10-07 17:13:12',
                'deleted_at' => NULL,
                'id' => 985,
                'interaction_type' => 'others',
                'note' => '<p>10/1-Had to reschedule Brianna&#39;s start date to take care of their new CRM with built in dialer.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            481 => 
            array (
                'client_id' => 731,
                'created_at' => '2020-10-07 17:17:02',
                'deleted_at' => NULL,
                'id' => 986,
                'interaction_type' => 'others',
                'note' => '<p>10/1-Request to update her cc on file. Sent CC form.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'text and email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            482 => 
            array (
                'client_id' => 487,
                'created_at' => '2020-10-07 17:19:23',
                'deleted_at' => NULL,
                'id' => 987,
                'interaction_type' => 'others',
                'note' => '<p>10/2-Very satisfied with Kat and Lady as his VAs. Will possibly add more hours for Lady as she will now be doing social media tasks as well.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            483 => 
            array (
                'client_id' => 399,
                'created_at' => '2020-10-07 17:23:32',
                'deleted_at' => NULL,
                'id' => 988,
                'interaction_type' => 'others',
                'note' => '<p>10/2-Request for a discounted rate. Coordinated with OM and Pavel. Confirmed resumption of services on October 12.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            484 => 
            array (
                'client_id' => 866,
                'created_at' => '2020-10-08 13:49:59',
                'deleted_at' => NULL,
                'id' => 989,
                'interaction_type' => 'email',
            'note' => '<p>Initially requested to hold the services off (Does Not have enough resources / leads for Burt) Decided to just cut his hours so he could call the list on their spreadsheet.</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Suspend Service',
                'status' => 'acknowleged',
            ),
            485 => 
            array (
                'client_id' => 634,
                'created_at' => '2020-10-08 13:50:45',
                'deleted_at' => NULL,
                'id' => 990,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Have a few concerns on postings and revisit request on going full time with Kris</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            486 => 
            array (
                'client_id' => 733,
                'created_at' => '2020-10-08 13:51:29',
                'deleted_at' => NULL,
                'id' => 991,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Asked additional assistance on his spreadsheets</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            487 => 
            array (
                'client_id' => 710,
                'created_at' => '2020-10-13 16:14:39',
                'deleted_at' => NULL,
                'id' => 992,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Invoice reminder / sent credit card update form (settled)</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            488 => 
            array (
                'client_id' => 634,
                'created_at' => '2020-10-13 16:19:31',
                'deleted_at' => NULL,
                'id' => 993,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Sent invoice for this cut off / bill breakdown</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            489 => 
            array (
                'client_id' => 674,
                'created_at' => '2020-10-13 23:29:26',
                'deleted_at' => NULL,
                'id' => 994,
                'interaction_type' => 'call',
                'note' => '<p>Feedback:&nbsp;</p>

<p>&quot;Ella&rsquo;s a fast learner, people like her. She doing a great job. So glad I have her. She&rsquo;s helping keep me straight a little bit - keeping me organized.... A big kudos to her!&quot;</p>

<p>New sched 9am-1pm EST effective tome Oct 14</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            490 => 
            array (
                'client_id' => 880,
                'created_at' => '2020-10-14 16:29:44',
                'deleted_at' => NULL,
                'id' => 995,
                'interaction_type' => 'call',
                'note' => '<p>Client requested to reverse the charges that his VA Kavin attended. The&nbsp; VA wasn&#39;t able to attend the 5 day training that the client assigned them. VA was removed. Reversal of charges was approved. Informed the VA about the reversal from his payout.</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Updates',
                'status' => 'done',
            ),
            491 => 
            array (
                'client_id' => 720,
                'created_at' => '2020-10-14 17:14:20',
                'deleted_at' => NULL,
                'id' => 996,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Touch base on Irish&#39;s performance they are very happy and she was able to meet expectations</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            492 => 
            array (
                'client_id' => 818,
                'created_at' => '2020-10-14 17:15:49',
                'deleted_at' => NULL,
                'id' => 997,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Provided credit card information for future payments</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            493 => 
            array (
                'client_id' => 792,
                'created_at' => '2020-10-14 17:44:01',
                'deleted_at' => NULL,
                'id' => 998,
                'interaction_type' => 'others',
                'note' => '<p>Interviewed TC-VA today. Did not hire the VA since the client doesn&#39;t feel any connections with the VA. Corinne would like to pause interviewing VAs this week and she asked me to look for a VA with TC experience in Seattle, WA - non negotiable.</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => 'Called/RC Meeting',
                'others_status' => 'Please specify status type',
                'others_type' => 'Client Interview/Updates',
                'status' => 'done',
            ),
            494 => 
            array (
                'client_id' => 780,
                'created_at' => '2020-10-14 19:23:00',
                'deleted_at' => NULL,
                'id' => 999,
                'interaction_type' => 'call',
            'note' => '<p>Touch base with Mike Ugarte (Judy&#39;s business partner). They are very happy with Rizza&#39;s perforfance and she&#39;s very enthusiastic. They are always referring agents/students to our website to those who are interested to have a VA. Mike agreed to send me a list of names and contact information of those who are interested.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            495 => 
            array (
                'client_id' => 907,
                'created_at' => '2020-10-15 14:01:31',
                'deleted_at' => NULL,
                'id' => 1000,
                'interaction_type' => 'call',
                'note' => '<p>Jacqueline interviewed Greg , Nice and Ivan . Decided to choose Nice. Start date discussed and schedule</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA interview',
                'status' => 'acknowleged',
            ),
            496 => 
            array (
                'client_id' => 710,
                'created_at' => '2020-10-15 16:27:24',
                'deleted_at' => NULL,
                'id' => 1001,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Concerns on CMA and asked options for Toni/ provided game plan and options for her.</p>',
                'note_type' => 'consultation',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            497 => 
            array (
                'client_id' => 797,
                'created_at' => '2020-10-15 21:48:57',
                'deleted_at' => NULL,
                'id' => 1002,
                'interaction_type' => 'call',
                'note' => '<ul>
<li style="list-style-type:disc">Invoice query - client&#39;s cc was charged extra on Sept 10. 2020 for the amt of $535.61</li>
<li style="list-style-type:disc">last 4 digits of ccs for&nbsp;NFI - 1046 and&nbsp;Design Mark: Visa - 2316</li>
<li style="list-style-type:disc">$535.61 - to be refunded today - advd of sys glitch</li>
<li style="list-style-type:disc">Adjustment on next cut off $<strong>1801.45</strong> - to be advd tom (per OM&#39;s instructions) - 2 diff clients with same last 4 of cc and similar exp year</li>
</ul>

<p style="list-style-type:disc">Oct 16 - have informed her of the $1801.45 amoutn to be credited on next invoice</p>

<p style="list-style-type:disc">She requested for OM to send her an email re the matter - informed OM re email</p>

<p style="list-style-type:disc">&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            498 => 
            array (
                'client_id' => 501,
                'created_at' => '2020-10-15 21:56:55',
                'deleted_at' => NULL,
                'id' => 1003,
                'interaction_type' => 'call',
            'note' => '<p>Advised that call re Buy -out on jayson JAvier with OM Tima is at 2pm PST (oct 15, 2020)</p>

<p>- Negotiated buy- out of Jayson Javier and of rate with Pavel and OM Tima</p>

<p>Effective immediately - Transfer agreement being processed</p>

<p>negotiated buy out rate - confidential</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            499 => 
            array (
                'client_id' => 677,
                'created_at' => '2020-10-16 21:36:56',
                'deleted_at' => NULL,
                'id' => 1004,
                'interaction_type' => 'call',
                'note' => '<p>Agreed to do a 30 min Webinar for KW command with Pavel - except Tues/ Fridays</p>

<p>Will email or text me availability for next week.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
        ));
        \DB::table('client_notes')->insert(array (
            0 => 
            array (
                'client_id' => 856,
                'created_at' => '2020-10-19 15:50:13',
                'deleted_at' => NULL,
                'id' => 1005,
                'interaction_type' => 'email',
                'note' => '<p>9/11/2020 None of the candidates are a good or right fit. I do not wish to pursue moving forward with this company and waste more of my time. I will look into other alternatives and get back to you.</p>',
                'note_type' => 'notification',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            1 => 
            array (
                'client_id' => 874,
                'created_at' => '2020-10-19 15:52:07',
                'deleted_at' => NULL,
                'id' => 1006,
                'interaction_type' => 'email',
                'note' => '<p>9/29 California Association of Realtors advised him that he needs a California licensed agent to write offers and also others like the CARealtors Covid form; he also needs to generate income and will circle back when he can afford to add a marketing assistant</p>',
                'note_type' => 'notification',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            2 => 
            array (
                'client_id' => 822,
                'created_at' => '2020-10-19 15:53:39',
                'deleted_at' => NULL,
                'id' => 1007,
                'interaction_type' => 'email',
                'note' => '<p>9/03 3rd batch of profiles sent; Andrea acknowledged with the directive to take the assessment; text sent to Steve for heads up on profiles sent</p>',
                'note_type' => 'touchbase',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            3 => 
            array (
                'client_id' => 821,
                'created_at' => '2020-10-19 15:57:20',
                'deleted_at' => NULL,
                'id' => 1008,
                'interaction_type' => 'email',
                'note' => '<p>9/16/2020, c/o&nbsp;Sharon Fabian<br />
Please be advised that Sasha Smith has requested to put her account on a temporary hold for 1 month, effective today, September 16, 2020.<br />
<br />
VA Nina Alexandra Palmares resigned due to health concerns. Sasha is very busy and not yet ready to hire the replacement VA.<br />
Will follow up with her next month.</p>',
                'note_type' => 'notification',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            4 => 
            array (
                'client_id' => 806,
                'created_at' => '2020-10-19 16:01:19',
                'deleted_at' => NULL,
                'id' => 1009,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>9/28/202020 c/o Sharon</p>

<p>Followed up the resumption of VA services<br />
I&#39;m just not ready yet. I need to make sure I have the right systems in place for her and right not I don&#39;t unfortunately<br />
Informed that Shekah is already endorsed to other clients</p>',
                'note_type' => 'followup',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            5 => 
            array (
                'client_id' => 641,
                'created_at' => '2020-10-19 16:57:34',
                'deleted_at' => NULL,
                'id' => 1010,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Things are great. Thank you for checking in.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            6 => 
            array (
                'client_id' => 160,
                'created_at' => '2020-10-19 16:58:05',
                'deleted_at' => NULL,
                'id' => 1011,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Good morning! I don&#39;t have any complaints or concerns. John is beyond amazing. He is a team player and goes above and beyond for us and our clients. He&#39;s always willing to learn and open minded</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            7 => 
            array (
                'client_id' => 443,
                'created_at' => '2020-10-19 16:58:36',
                'deleted_at' => NULL,
                'id' => 1012,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Everything is great</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            8 => 
            array (
                'client_id' => 775,
                'created_at' => '2020-10-19 16:59:06',
                'deleted_at' => NULL,
                'id' => 1013,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: He&#39;s doing well! Much better</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            9 => 
            array (
                'client_id' => 565,
                'created_at' => '2020-10-19 16:59:51',
                'deleted_at' => NULL,
                'id' => 1014,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Thank you to checking in with Steven and I regarding Aaron.<br />
<br />
Since I joined the team, Aaron has been a phenomenal asset to the Ascend Real Estate team. He&rsquo;s prompt, gets his work done, open to suggestions for improvements, flexible, and is always attempting to improve the quality of service he provides. He and I are working together to work on a few things, but nothing too major.<br />
<br />
I speak for myself when I say that I look forward to continuing having Aaron being a member of our team. In the future, if I have any concerns, I&rsquo;ll contact you directly to address issues.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            10 => 
            array (
                'client_id' => 652,
                'created_at' => '2020-10-19 17:00:34',
                'deleted_at' => NULL,
                'id' => 1015,
                'interaction_type' => 'email',
                'note' => '<p>Michelle has been doing a great job&nbsp;<br />
<br />
I am following up with her to see how her evening tasks are coming along if it&#39;s busy or slow.<br />
<br />
Thank you for checking in, it&#39;s appreciated.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            11 => 
            array (
                'client_id' => 693,
                'created_at' => '2020-10-19 17:01:02',
                'deleted_at' => NULL,
                'id' => 1016,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Lyka has been terrific. great attention to detail and gets everything done.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            12 => 
            array (
                'client_id' => 177,
                'created_at' => '2020-10-19 17:01:29',
                'deleted_at' => NULL,
                'id' => 1017,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: I can&rsquo;t really give any feedback right now because she is still getting used to what I want done and how to do it. Let&rsquo;s chat in a couple of weeks and then I&rsquo;ll have a better idea.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            13 => 
            array (
                'client_id' => 118,
                'created_at' => '2020-10-19 17:02:00',
                'deleted_at' => NULL,
                'id' => 1018,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Everything is going well. We love our VA&rsquo;s.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            14 => 
            array (
                'client_id' => 366,
                'created_at' => '2020-10-19 17:03:04',
                'deleted_at' => NULL,
                'id' => 1019,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: I&#39;m happy with Margaret thus far. Good communicator, sticking to the plan and is reporting well.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            15 => 
            array (
                'client_id' => 376,
                'created_at' => '2020-10-19 17:03:34',
                'deleted_at' => NULL,
                'id' => 1020,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: He is doing well and if I ever have any concerns I will address this with him directly. Have a wonderful weekend.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            16 => 
            array (
                'client_id' => 542,
                'created_at' => '2020-10-19 17:04:01',
                'deleted_at' => NULL,
                'id' => 1021,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: All Good!</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            17 => 
            array (
                'client_id' => 301,
                'created_at' => '2020-10-19 17:04:35',
                'deleted_at' => NULL,
                'id' => 1022,
                'interaction_type' => 'email',
                'note' => '<p>As one of the owners of the company, I can tell you we are extremely happy with Mia. She is great and our managers are very happy - Mike Depaola<br />
<br />
Hope all is well. As Mike said, everything has been great with Mia &amp; Sara. In fact, we&#39;re looking into the possibility of adding a third virtual assistant for some additional support. -<br />
Craig Cirella</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            18 => 
            array (
                'client_id' => 794,
                'created_at' => '2020-10-19 17:05:06',
                'deleted_at' => NULL,
                'id' => 1023,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: We are very happy with Joseph actually. He has proven to be a great fit and a very smart, hard worker and we appreciate him greatly. Thank you for continuing to check in.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            19 => 
            array (
                'client_id' => 781,
                'created_at' => '2020-10-19 17:05:36',
                'deleted_at' => NULL,
                'id' => 1024,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Things are going well. Thank you for checking in</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            20 => 
            array (
                'client_id' => 534,
                'created_at' => '2020-10-19 17:06:07',
                'deleted_at' => NULL,
                'id' => 1025,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: So far so good, thanks.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            21 => 
            array (
                'client_id' => 776,
                'created_at' => '2020-10-19 17:06:44',
                'deleted_at' => NULL,
                'id' => 1026,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Thank you for reaching out. Maria is doing a great job. We are working on training this week to work on some scripts.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            22 => 
            array (
                'client_id' => 657,
                'created_at' => '2020-10-19 17:48:00',
                'deleted_at' => NULL,
                'id' => 1027,
                'interaction_type' => 'email',
                'note' => '<p>Hi there. this will be the first time I respond to you for this request, so I hope I do so in a productive manner. Anj is doing well overall, and I touch base with her pretty regularly. I think her ability to both schedule and maintain her growing workflow&nbsp;is superior, and she is always communicative&nbsp;with questions and concerns. Now that I am her direct point of contact, I hope this alleviates any stress she has with issues regarding her work.&nbsp;</p>

<p>&nbsp;</p>

<p>2 things that came up recently that I will mention. I did a deep audit into our quarterly lead flow and realized we had missed some steps in updating these leads. I had a meeting with Anj to reiterate the importance of hitting all the subsequent stages of each lead, as we are measured on each step of the process. She seems to understand, and I am committed to following up and checking more frequently for misses to provide feedback in the moment.</p>

<p>&nbsp;</p>

<p>She also came to me with a communication break down and issues in her compliance duties. This is a part of her role I am unfamiliar with, so we both attended a training with Safwa, and she walked me through her work flow and her pain points with it. I am monitoring the responsiveness of the agents she is contacting for documents she needs to complete her work, and have elevated concerns to those agents and their supervisors.</p>

<p>&nbsp;</p>

<p>Anj has responded very positively to some process changes we have taken recently and is always eager to help. She is a joy to have on my team, and I look forward to continuing to work with her.</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            23 => 
            array (
                'client_id' => 362,
                'created_at' => '2020-10-19 18:20:19',
                'deleted_at' => NULL,
                'id' => 1028,
                'interaction_type' => 'email',
                'note' => '<p>Touch base regarding Derren&#39;s performance - He is doing great and always get the work done<br />
very responsive super happy with his work</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            24 => 
            array (
                'client_id' => 907,
                'created_at' => '2020-10-19 18:21:29',
                'deleted_at' => NULL,
                'id' => 1029,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Wants to check if Nice is available for training during weekends (coordinated with VA)</p>

<hr />
<p>Move start date to Oct 22</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            25 => 
            array (
                'client_id' => 397,
                'created_at' => '2020-10-19 22:28:28',
                'deleted_at' => NULL,
                'id' => 1030,
                'interaction_type' => 'call',
                'note' => '<p>Janki and HIren had interviewed applicants for additional GVA</p>

<p>Chose vA Geraldine Carla Purong</p>

<p>Starts date: Oct 21, 2020</p>

<p>Shift: 9am-5pm</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Interview for Additional GVA',
                'status' => 'pending',
            ),
            26 => 
            array (
                'client_id' => 893,
                'created_at' => '2020-10-20 00:32:32',
                'deleted_at' => NULL,
                'id' => 1031,
                'interaction_type' => 'call',
                'note' => '<p>Called him about his VA&#39;s change of schedule to the match the other clients sched / CLient approved to have a split shift every Tuesday</p>',
                'note_type' => 'consultation',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            27 => 
            array (
                'client_id' => 769,
                'created_at' => '2020-10-20 00:33:33',
                'deleted_at' => NULL,
                'id' => 1032,
                'interaction_type' => 'call',
                'note' => '<p>Informed him that his VA did not showed up to complete his shift / client needs the VA&#39;s help to complete processing the orders / started to look for VA replacement since his current VA did not mention anything about his absence</p>',
                'note_type' => 'notification',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            28 => 
            array (
                'client_id' => 736,
                'created_at' => '2020-10-20 00:35:26',
                'deleted_at' => NULL,
                'id' => 1033,
                'interaction_type' => 'email',
                'note' => '<p>Expressed his interest to get another VA endorse the searching to his team.&nbsp;</p>

<p>Jeremy/Kate -&nbsp;Sent there preferences / looking for an EVA to provide chat and phone support for their new product / VA will also work directly with their CEO</p>

<p>-Sent VA profiles / waiting for CI sched</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA',
                'status' => 'ongoing',
            ),
            29 => 
            array (
                'client_id' => 824,
                'created_at' => '2020-10-20 15:31:23',
                'deleted_at' => NULL,
                'id' => 1034,
                'interaction_type' => 'email',
                'note' => '<p>Angelo is doing great! Always eager to try something&nbsp;new!&nbsp;<br />
&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            30 => 
            array (
                'client_id' => 919,
                'created_at' => '2020-10-20 16:39:23',
                'deleted_at' => NULL,
                'id' => 1035,
                'interaction_type' => 'call',
                'note' => '<p>Called client for Placements VA preference. Asked to call back later at 4 pm EST.</p>',
                'note_type' => 'consultation',
                'noted_by' => 18,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            31 => 
            array (
                'client_id' => 882,
                'created_at' => '2020-10-21 00:21:58',
                'deleted_at' => NULL,
                'id' => 1036,
                'interaction_type' => 'text',
                'note' => '<ul>
<li style="list-style-type:disc">Feedback: &ldquo;She&#39;s amazing very happy with her&rdquo;</li>
</ul>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            32 => 
            array (
                'client_id' => 771,
                'created_at' => '2020-10-21 01:03:11',
                'deleted_at' => NULL,
                'id' => 1037,
                'interaction_type' => 'call',
                'note' => '<p>Requested to turn back to Part time since she&#39;s very busy and cannot attend to her VA Full time, however, she will request to upgrade again as soon as she&#39;s able to organize her assignments.</p>',
                'note_type' => 'consultation',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            33 => 
            array (
                'client_id' => 642,
                'created_at' => '2020-10-21 20:59:00',
                'deleted_at' => NULL,
                'id' => 1038,
                'interaction_type' => 'call',
                'note' => '<p>Referral: &nbsp;Wendy Ivanov has called and wanted the same VA svcs with Lee Ann</p>

<p>Advd of rates adn she mentioned that she would prefer a lower rate</p>

<p>Wendy wants&nbsp;a GVA instead</p>

<p>Contract has been sent - waiting for her to sign the contract</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Referral',
                'status' => 'pending',
            ),
            34 => 
            array (
                'client_id' => 670,
                'created_at' => '2020-10-21 23:28:43',
                'deleted_at' => NULL,
                'id' => 1039,
                'interaction_type' => 'others',
                'note' => '<p>Requested to cancel his contract with us since their business is running slow and they don&#39;t have anymore tasks for Bianca / Client is very thankful that they were able to work with us and with their VA</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => 'Call / Email',
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation',
                'status' => 'done',
            ),
            35 => 
            array (
                'client_id' => 769,
                'created_at' => '2020-10-21 23:30:15',
                'deleted_at' => NULL,
                'id' => 1040,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Informed client that we&#39;ll have to continue to look for a replacement for his VA since his VA did not showed up again&nbsp; (NCNS for 3 days)/ Sent more&nbsp;VA profiles to the client / Client requested for CI sched on Friday at 1:30PM CST</p>',
                'note_type' => 'followup',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            36 => 
            array (
                'client_id' => 918,
                'created_at' => '2020-10-21 23:31:49',
                'deleted_at' => NULL,
                'id' => 1041,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Interviewed VAs together with Tim(Lender) / shortlisted 2 VAs will continue interviewing 2 to 3 more VAs tomorrow at 11AM EST</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => 'Call / RC Meeting',
                'others_status' => 'Please specify status type',
                'others_type' => 'Client Interview/Updates',
                'status' => 'ongoing',
            ),
            37 => 
            array (
                'client_id' => 736,
                'created_at' => '2020-10-21 23:32:49',
                'deleted_at' => NULL,
                'id' => 1042,
                'interaction_type' => 'email',
                'note' => '<p>Followed up with his sales team Jeremy and Kate for the CI&nbsp;schedule - no response yet</p>',
                'note_type' => 'followup',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            38 => 
            array (
                'client_id' => 917,
                'created_at' => '2020-10-23 15:39:06',
                'deleted_at' => NULL,
                'id' => 1043,
                'interaction_type' => 'email',
                'note' => '<p>meet and greet with Kris&nbsp;</p>

<hr />
<p>Clarification on his schedule</p>',
                'note_type' => 'consultation',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            39 => 
            array (
                'client_id' => 634,
                'created_at' => '2020-10-23 16:03:29',
                'deleted_at' => NULL,
                'id' => 1044,
                'interaction_type' => 'email',
                'note' => '<p>Notice of cancellation due to financial issues and will be hiring interns but she is very happy with his performance</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation Notification',
                'status' => 'acknowleged',
            ),
            40 => 
            array (
                'client_id' => 769,
                'created_at' => '2020-10-24 01:10:33',
                'deleted_at' => NULL,
                'id' => 1045,
                'interaction_type' => 'others',
                'note' => '<p>Conducted Client interview @ 10:30AM / Client would like to review the VA&#39;s resumes first since the tasks that he&#39;ll assign them are more on maintenance, follow ups and billing / Client will give his decision on Monday</p>',
                'note_type' => 'others',
                'noted_by' => 29,
                'others_interaction' => 'RC Meeting',
                'others_status' => 'Please specify status type',
                'others_type' => 'Client Interview/Updates',
                'status' => 'ongoing',
            ),
            41 => 
            array (
                'client_id' => 893,
                'created_at' => '2020-10-24 01:11:22',
                'deleted_at' => NULL,
                'id' => 1046,
                'interaction_type' => 'email',
                'note' => '<p>Client is very happy with his new VA / Impressed that VA was able to complete all the tasks he assigned for the day</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            42 => 
            array (
                'client_id' => 290,
                'created_at' => '2020-10-24 01:15:08',
                'deleted_at' => NULL,
                'id' => 1047,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Client subscribed to TB last January of 2019 / Client would like to use his ours since he wasn&#39;t able to use them before / client requested to have someone create flyers for him / informed client that he only have 16 hours left since the previous VA used his 4 hours / client declined that he gave the previous VA any task, however we were able to find some proof that the VA informed the client that she logged in, communicated with him, did a research and sent a client list</p>',
                'note_type' => 'consultation',
                'noted_by' => 29,
                'others_interaction' => 'Call / Email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            43 => 
            array (
                'client_id' => 162,
                'created_at' => '2020-10-26 16:49:35',
                'deleted_at' => NULL,
                'id' => 1048,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>invoice collection/ will check with the bank and provide feedback within the day</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            44 => 
            array (
                'client_id' => 871,
                'created_at' => '2020-10-26 16:51:06',
                'deleted_at' => NULL,
                'id' => 1049,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Decided to remove her second VA due to financial reasons. Everything is good with Joey</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA Cancellation',
                'status' => 'acknowleged',
            ),
            45 => 
            array (
                'client_id' => 907,
                'created_at' => '2020-10-26 17:03:46',
                'deleted_at' => NULL,
                'id' => 1050,
                'interaction_type' => 'text',
                'note' => '<p>Verified and fixed dispute</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Rate dispute',
                'status' => 'acknowleged',
            ),
            46 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-10-26 20:12:50',
                'deleted_at' => NULL,
                'id' => 1051,
                'interaction_type' => 'text',
                'note' => '<p>Payment follow up set on Oct 23</p>

<p>Reply received from Adilah on Oct 25</p>

<p>Just a heads up for you, the bank has been sending me fraud alerts from merchants out of the country and payments are being declined until I review and confirm the transactions.<br />
<br />
If that happens when you process payment, my new bank will notify me immediately with a text and phone call to approve or dispute the transaction.<br />
<br />
I&#39;ll be on the lookout if that happens with your transaction and I&#39;ll go ahead and approve the transaction and ask you to retry the payment in the event my fraud protection blocks the transaction this time. So, just let me know if you have issues.<br />
<br />
Again, apologies for any inconvenience. Moving into Nov., I&#39;m hoping to get everything straightened out with all of my new and old accounts so that you don&#39;t have any further issues with processing payments from me.</p>

<p>&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            47 => 
            array (
                'client_id' => 779,
                'created_at' => '2020-10-27 22:22:37',
                'deleted_at' => NULL,
                'id' => 1052,
                'interaction_type' => 'call',
                'note' => '<p>approved for Oliver and Jojho to retain their MNL time schedule 2am.</p>

<p>So, DST - Nov 2 their shift will be 10am PST (from 11am PST)</p>

<p>All good</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            48 => 
            array (
                'client_id' => 797,
                'created_at' => '2020-10-27 22:25:31',
                'deleted_at' => NULL,
                'id' => 1053,
                'interaction_type' => 'text',
                'note' => '<p>Advd that we still haven&#39;t heard from Jaela - location Camarines Sur wsa one of location that was hardest hit by the Typhoon</p>

<p>Mobile can&#39;t be reached</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            49 => 
            array (
                'client_id' => 783,
                'created_at' => '2020-10-28 00:23:32',
                'deleted_at' => NULL,
                'id' => 1054,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Touchbased with client / got a very good feedback &quot;Overall he&#39;s doing a great job and he thinks very fast. Also he has a great attitude. So far so good.&quot; / Asked for referrals / as per client, she will send me the names / Also asked about our security if she&#39;s going to provide confidential information to her VA / Informed the client that VAs signed a contract with Virtudesk and that includes keeping client&#39;s confidential information / also explained that our business has been running for 4 years and we haven&#39;t experience any issues with VAs messing up with client&#39;s confidentialities, credit card details, SS numbers and credentials.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            50 => 
            array (
                'client_id' => 886,
                'created_at' => '2020-10-28 00:25:39',
                'deleted_at' => NULL,
                'id' => 1055,
                'interaction_type' => 'call',
                'note' => '<p>Touchbased with client / happy with her VA, however, she would like to see the result of the organic posting asap / client is targeting to have more followers on their facebook and instagram</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            51 => 
            array (
                'client_id' => 565,
                'created_at' => '2020-10-29 15:36:28',
                'deleted_at' => NULL,
                'id' => 1056,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Thank you for reaching out to me regarding Marion. Yes that is correct! Starting Monday, we would like Marion to transition to a full time role. He&rsquo;s been exceeding expectations and the team is very happy to have him.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            52 => 
            array (
                'client_id' => 301,
                'created_at' => '2020-10-29 15:37:12',
                'deleted_at' => NULL,
                'id' => 1057,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: She&#39;s been great!</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            53 => 
            array (
                'client_id' => 905,
                'created_at' => '2020-10-29 23:33:57',
                'deleted_at' => NULL,
                'id' => 1058,
                'interaction_type' => 'call',
                'note' => '<p>POC Wilson provided feedback to Coach Aran</p>

<p>Received a call from him regarding RoseAnne&#39;s performance. He mentioned that Rose needs to reach the goal of 200 dials per day. Informed him that I coached Rose about it, and provided him the reasons why Rose was not able to reach the goal. Also informed him that we created an action plan so that Rose would be able to pick-up the pace and reach the dials per day.</p>

<p>Wilson agreed on&nbsp;action plan for Rose and he also said that it was his fault that he didnt set the goal during training re # of &nbsp;calls/dials per day</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            54 => 
            array (
                'client_id' => 634,
                'created_at' => '2020-11-02 18:42:34',
                'deleted_at' => NULL,
                'id' => 1059,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Follow up on invoice call set up by 3 pm PST</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            55 => 
            array (
                'client_id' => 660,
                'created_at' => '2020-11-02 18:54:03',
                'deleted_at' => NULL,
                'id' => 1060,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Sent Oct 10 - Oct 25 hours for Ore Living team / New schedule will be implemented next week</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            56 => 
            array (
                'client_id' => 660,
                'created_at' => '2020-11-03 17:38:53',
                'deleted_at' => NULL,
                'id' => 1061,
                'interaction_type' => 'email',
                'note' => '<p>Updated and sent their working hours</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Invoice',
                'status' => 'acknowleged',
            ),
            57 => 
            array (
                'client_id' => 397,
                'created_at' => '2020-11-03 22:23:22',
                'deleted_at' => NULL,
                'id' => 1062,
                'interaction_type' => 'email',
                'note' => '<p>Janki has added Neneth Montalba on a CI on Oct 31 - VA started onOv 2, 2020</p>

<p>=====</p>

<p>Nov 3, 2020</p>

<p>Janki has submitted her Video Testimonial for Virtudesk</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            58 => 
            array (
                'client_id' => 397,
                'created_at' => '2020-11-05 00:15:46',
                'deleted_at' => NULL,
                'id' => 1063,
                'interaction_type' => 'call',
                'note' => '<p>Feedback for Neneth:</p>

<p>&quot;Excellent!&quot;</p>

<p>Referral:&nbsp;Fran Hwang - will give contact info once done with her Showing.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            59 => 
            array (
                'client_id' => 736,
                'created_at' => '2020-11-05 00:48:33',
                'deleted_at' => NULL,
                'id' => 1064,
                'interaction_type' => 'others',
                'note' => '<p>Selected their new VA / Started last Monday, Nov. 2, 2020 - Maria Regina Navarez</p>',
                'note_type' => 'followup',
                'noted_by' => 29,
                'others_interaction' => 'RC Meeting',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            60 => 
            array (
                'client_id' => 783,
                'created_at' => '2020-11-05 00:49:43',
                'deleted_at' => NULL,
                'id' => 1065,
                'interaction_type' => 'call',
                'note' => '<p>Happy with her VA&#39;s performance / VA can pick up things very fast, very effective, attitude is good and always open to communicate / Client would just like to address the VA&#39;s health and continue to pay more attention to details</p>',
                'note_type' => 'touchbase',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            61 => 
            array (
                'client_id' => 769,
                'created_at' => '2020-11-05 00:51:13',
                'deleted_at' => NULL,
                'id' => 1066,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Will sent VA profiles of experienced VAs / still collecting CVs from coaches / Client prefer VAs with RE experince</p>',
                'note_type' => 'followup',
                'noted_by' => 29,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            62 => 
            array (
                'client_id' => 636,
                'created_at' => '2020-11-05 21:53:30',
                'deleted_at' => NULL,
                'id' => 1067,
                'interaction_type' => 'call',
                'note' => '<p>Triad call with Mark and Mary addressed issues and created a game plan to make sure that everything is seamless</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            63 => 
            array (
                'client_id' => 720,
                'created_at' => '2020-11-09 15:54:19',
                'deleted_at' => NULL,
                'id' => 1068,
                'interaction_type' => 'email',
                'note' => '<p>Yes, everything is fine. We are enjoying working with her.&nbsp;<br />
&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            64 => 
            array (
                'client_id' => 755,
                'created_at' => '2020-11-09 15:56:19',
                'deleted_at' => NULL,
                'id' => 1069,
                'interaction_type' => 'email',
                'note' => '<p>She is still amazing!!</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            65 => 
            array (
                'client_id' => 707,
                'created_at' => '2020-11-10 23:58:40',
                'deleted_at' => NULL,
                'id' => 1070,
                'interaction_type' => 'call',
                'note' => '<p>Payment reminder sent</p>

<p>Swapna provided temporary debit card info - for 1 x payment only</p>',
                'note_type' => 'collection',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            66 => 
            array (
                'client_id' => 599,
                'created_at' => '2020-11-11 00:01:05',
                'deleted_at' => NULL,
                'id' => 1071,
                'interaction_type' => 'call',
                'note' => '<p>Payment Reminder sent</p>

<p>Adilah provided new card information&nbsp;</p>

<p>Client left her card at the ATM machine</p>',
                'note_type' => 'collection',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            67 => 
            array (
                'client_id' => 784,
                'created_at' => '2020-11-11 13:57:11',
                'deleted_at' => NULL,
                'id' => 1072,
                'interaction_type' => 'email',
                'note' => '<p>Ivan is doing great and he is valued at CIG. We are considering the increase and will advise as to when to initiate any pay increase.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            68 => 
            array (
                'client_id' => 867,
                'created_at' => '2020-11-17 16:27:16',
                'deleted_at' => NULL,
                'id' => 1073,
                'interaction_type' => 'email',
                'note' => '<p>11/9-&nbsp;Advised of schedule adjustment only for tomorrow.</p>

<p>11/10-Acknowledged request to update schedule for today.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            69 => 
            array (
                'client_id' => 649,
                'created_at' => '2020-11-17 16:30:07',
                'deleted_at' => NULL,
                'id' => 1074,
                'interaction_type' => 'others',
                'note' => '<p>Laura Wey-11/9-Provided updates and feedback on all VA&#39;s performances. Set expectations on next week&#39;s tasks. Forwarded webinar schedules of Jannesse and Jean. Had a validation call for the next 2 VAs.</p>

<p>11/10-Informed of Apple&#39;s current issue. Awaiting approval for proposed MUS.</p>

<p>11/11-Informed outage issue of Paula. Approved MUS schedule of Apple from yesterday&#39;s lost hours.</p>

<p>Caleb Brewster-11/12-nformed of VA Juan and Jannesse&#39;s absence due to power outage.</p>

<p>11/13-Coordinated on Juan&#39;s notification on absence today and Jan&#39;s schedule for today.</p>

<p>Jared Berman-11/11-Provided positive feedback on the additional VAs. Awaiting for the next process from Laura.</p>

<p>11/12-Provided an update that VA will start on Tuesday. Checking for the next update on schedule details.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            70 => 
            array (
                'client_id' => 913,
                'created_at' => '2020-11-17 16:34:15',
                'deleted_at' => NULL,
                'id' => 1075,
                'interaction_type' => 'others',
                'note' => '<p>11/9-Followed-up on resumption of VA services. Requested to get more VA profiles for review. Asked to give her a call on Wednesday.</p>

<p>11/10-Sent additional VA profiles for review. Confirmed meeting tomorrow.</p>

<p>11/11-Requested to speak with the 3 candidates tomorrow. Awaiting her confirmation of her availability in the afternoon.</p>

<p>11/12-Interviewed 3 candidates today. Requested to get recommendations from their clients.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            71 => 
            array (
                'client_id' => 898,
                'created_at' => '2020-11-17 16:37:30',
                'deleted_at' => NULL,
                'id' => 1076,
                'interaction_type' => 'email',
                'note' => '<p>Informed of VA Erwin&#39;s outage issue. MUS confirmed and approved.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            72 => 
            array (
                'client_id' => 760,
                'created_at' => '2020-11-17 16:40:11',
                'deleted_at' => NULL,
                'id' => 1077,
                'interaction_type' => 'email',
                'note' => '<p>Informed of VA Erwin&#39;s outage issue. MUS confirmed and approved.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            73 => 
            array (
                'client_id' => 922,
                'created_at' => '2020-11-17 16:41:39',
                'deleted_at' => NULL,
                'id' => 1078,
                'interaction_type' => 'others',
                'note' => '<p>Complied with all the requirements needed for the VA to get his logins ready from Compass.<br />
&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            74 => 
            array (
                'client_id' => 274,
                'created_at' => '2020-11-17 16:43:37',
                'deleted_at' => NULL,
                'id' => 1079,
                'interaction_type' => 'others',
                'note' => '<p>-Informed of VA Wena&#39;s start date today and confirmed her first week&#39;s tasks.</p>

<p>-Acknowledged her request to send flowers for Wena.</p>

<p>-nformed of Wena&#39;s absence as her father died. Followed-up on invoice. She requested to get CC form to update new card information.</p>

<p>Followed-up on invoice. Asked to resent CC form-invoice collection successful.</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            75 => 
            array (
                'client_id' => 788,
                'created_at' => '2020-11-17 16:44:55',
                'deleted_at' => NULL,
                'id' => 1080,
                'interaction_type' => 'others',
                'note' => '<p>11/9-Asked to get more VA profiles for review for additional VA requests.</p>

<p>11/10-Sent profile of VA Kavin as additional VA request.<br />
wednesday-Requested to speak with Kavin to confirm his availability</p>

<p>11/12-Had his interview with Kavin today. Requested to get more VA profiles for review.</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            76 => 
            array (
                'client_id' => 725,
                'created_at' => '2020-11-17 16:46:15',
                'deleted_at' => NULL,
                'id' => 1081,
                'interaction_type' => 'others',
                'note' => '<p>11/10-Dispute on latest invoice addressed. Notified of cancellation today. Coordinated with OM regarding his concerns.</p>

<p>11/11-Followed up on his request for a manager&#39;s call. Reminded OM.</p>

<p>11/12-Was able to speak with OM to check his request and get feedback. Reached out to him after and informed of the courtesy credit.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            77 => 
            array (
                'client_id' => 863,
                'created_at' => '2020-11-17 16:48:25',
                'deleted_at' => NULL,
                'id' => 1082,
                'interaction_type' => 'others',
            'note' => '<p>11/12-Confirmed cancellation of service (with 14 days notification). Informed that they will just be utilizing their onshore agents.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => 'call, email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            78 => 
            array (
                'client_id' => 547,
                'created_at' => '2020-11-17 16:49:41',
                'deleted_at' => NULL,
                'id' => 1083,
                'interaction_type' => 'email',
                'note' => '<p>11/10--Request for touch base to discuss VA rate increase on her anniversary.</p>

<p>11/11-Reached out to confirm the rate increase of Laradeth.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            79 => 
            array (
                'client_id' => 805,
                'created_at' => '2020-11-17 16:51:10',
                'deleted_at' => NULL,
                'id' => 1084,
                'interaction_type' => 'email',
                'note' => '<p>Provided feedback on Lizabel&#39;s performance. Asked assistance in helping her out in handling objections.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            80 => 
            array (
                'client_id' => 487,
                'created_at' => '2020-11-17 16:52:04',
                'deleted_at' => NULL,
                'id' => 1085,
                'interaction_type' => 'email',
                'note' => '<p>Approved of request to get recommendation for VA Lady.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            81 => 
            array (
                'client_id' => 747,
                'created_at' => '2020-11-17 16:53:02',
                'deleted_at' => NULL,
                'id' => 1086,
                'interaction_type' => 'email',
                'note' => '<p>Approved slide shift for VA Charmain as she is still having issues with their connection and power interruptions.<br />
&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            82 => 
            array (
                'client_id' => 754,
                'created_at' => '2020-11-17 16:56:12',
                'deleted_at' => NULL,
                'id' => 1087,
                'interaction_type' => 'email',
                'note' => '<p>11/13-Reached out to follow up on the code needed for Chris to access her Gmail account. Approved shift extension of her VA today.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            83 => 
            array (
                'client_id' => 839,
                'created_at' => '2020-11-17 16:57:24',
                'deleted_at' => NULL,
                'id' => 1088,
                'interaction_type' => 'email',
                'note' => '<p>Coordinated on VA Vanessa&#39;s current situation. Approved MUS whenever she&#39;s ready.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            84 => 
            array (
                'client_id' => 887,
                'created_at' => '2020-11-17 16:58:21',
                'deleted_at' => NULL,
                'id' => 1089,
                'interaction_type' => 'email',
                'note' => '<p>Approved schedule extension of 1 hour everyday to compensate for the lost hours.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            85 => 
            array (
                'client_id' => 894,
                'created_at' => '2020-11-18 00:11:17',
                'deleted_at' => NULL,
                'id' => 1090,
                'interaction_type' => 'call',
                'note' => '<p>advd that Cesar&rsquo;s main ISP is doing maintenance work - no inet.</p>

<ul>
<li style="list-style-type:disc">Advd VA will do a MUS for the lost time</li>
<li style="list-style-type:disc">Wants to place services on hold at the moment and wants to see what VA has been doing and know if it is helpful and consistent with hours that&rsquo;s been billed</li>
<li style="list-style-type:disc">Resent&nbsp;Timedly email</li>
<li style="list-style-type:disc">Wants to validate # of hours billed and validate work being done by VA</li>
<li style="list-style-type:disc">not much tasks being done or given to VA</li>
<li style="list-style-type:disc">advd that he would need to inform us if he doesn&#39;t want VA to work due to no tasks or we can shorten hours</li>
<li style="list-style-type:disc">will still review timedly and will update by Friday</li>
<li style="list-style-type:disc">Website went down</li>
<li style="list-style-type:disc">No work on wed/ thurs and to report on friday for 4 hrs only - afternoon 11am - 3pm EST</li>
<li style="list-style-type:disc">Admittedly said that he wasn&rsquo;t supervising Cesar properly as he hasn&rsquo;t replied ot his emails/ msgs yet</li>
</ul>

<p style="list-style-type:disc">===========</p>

<p style="list-style-type:disc">VA will work tom moring 9:30-1:30pm EST to fix website</p>

<p style="list-style-type:disc">no work on thurs - Nov 19, 2020</p>

<p style="list-style-type:disc">Friday shift 11AM-3pm EST</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            86 => 
            array (
                'client_id' => 871,
                'created_at' => '2020-11-18 15:30:21',
                'deleted_at' => NULL,
                'id' => 1091,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Called regarding incentive process (happy with her VA and would love to give a holiday bonus)</p>',
                'note_type' => 'consultation',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            87 => 
            array (
                'client_id' => 907,
                'created_at' => '2020-11-18 18:55:41',
                'deleted_at' => NULL,
                'id' => 1092,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Confirmed cancellation discussed last rate and cancellation form</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'cancellation',
                'status' => 'acknowleged',
            ),
            88 => 
            array (
                'client_id' => 466,
                'created_at' => '2020-11-18 19:01:50',
                'deleted_at' => NULL,
                'id' => 1093,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Notification on Leverage group page and confirmed</p>

<hr />
<p>Please be advised that Lisa Lombardi would like to give a $25 incentive this coming November 25th to Imelda Ramos&#39; pay.</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            89 => 
            array (
                'client_id' => 397,
                'created_at' => '2020-11-18 23:42:17',
                'deleted_at' => NULL,
                'id' => 1094,
                'interaction_type' => 'call',
                'note' => '<p>Touch base with Janki and Hiren</p>

<p>Main Concern is Rich&#39;s performance (not appts) - online presence/ interaction/ instances of MIA/ sleeping during meeting</p>

<ul>
<li style="list-style-type:disc">Last monday - missed team meeting due to&nbsp; i net issues</li>
<li style="list-style-type:disc">Other monday - dozing off per Hiren</li>
<li style="list-style-type:disc">Less participation in meetings - no response from Rich - from 11am to 2pm&nbsp;</li>
<li style="list-style-type:disc">Decrease in interaction and motivation</li>
<li style="list-style-type:disc">Messages are so - don&rsquo;t feel that he is part of the team</li>
<li style="list-style-type:disc">Must have a personal issue -&nbsp;</li>
<li style="list-style-type:disc">If a team member - drifting off to lalaland&hellip;</li>
<li style="list-style-type:disc">Already thinking about giving VAs a bonus</li>
<li style="list-style-type:disc">Was a very productive member - sudden drop off on his performance</li>
<li style="list-style-type:disc">Aloof - feels that he&rsquo;s going through personal issues</li>
<li style="list-style-type:disc">If Rich needs a day or 2 rest day - if he needs to fix his personal issues</li>
<li style="list-style-type:disc">Geraldine is a good gatekeeper</li>
<li style="list-style-type:disc">Wants someone like Neneth - motivation - open mindset - held Janki accountable</li>
</ul>

<p>======</p>

<p>Update Nov 19&nbsp;</p>

<p>advd Janki that Rich has trouble sleeping - lacks sleep&nbsp;</p>

<p>VA has even purchased scented oils to help him fall asleep - VA also advd to seek doctor&#39;s help for sleeping pill prescription</p>

<p>at&nbsp;4:20AM MNL - Janki sent a message that Rich is missing - didn&#39;t attend the meeting.</p>

<p>after more than an hour update Janki that VA fell asleep</p>

<p>client is very upset and sad - will lessen hour to 4 hrs effective tom Nov 20 - 10AM-2PM PST</p>

<p>Janki wants another ISA for PT replacement - who is also good in setting up appts - someone like Neneth&#39;s disposition</p>

<p>=======</p>

<p>additional ISA started on Nov 24, 2020 - Vanessa Alba</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            90 => 
            array (
                'client_id' => 654,
                'created_at' => '2020-11-19 02:58:14',
                'deleted_at' => NULL,
                'id' => 1095,
                'interaction_type' => 'call',
                'note' => '<p>CI with VA Mary Jane, callback at 4:30pm for her decision&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Client Interview',
                'status' => 'done',
            ),
            91 => 
            array (
                'client_id' => 296,
                'created_at' => '2020-11-19 03:00:01',
                'deleted_at' => NULL,
                'id' => 1096,
                'interaction_type' => 'email',
                'note' => '<p>Christmas bonus for Giselle, $350&nbsp;</p>

<p>&nbsp;&quot;please wait until&nbsp;Christmas&nbsp;for it&quot; as per Graeham</p>',
                'note_type' => 'notification',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            92 => 
            array (
                'client_id' => 947,
                'created_at' => '2020-11-20 17:21:55',
                'deleted_at' => NULL,
                'id' => 1097,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Coordinated concerns on their phone system / provided recommendations</p>

<hr />
<p>&nbsp;</p>

<p>Coordinated and informed that we need the logins for her g voice account (start date requested to be moved tomorrow)</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            93 => 
            array (
                'client_id' => 938,
                'created_at' => '2020-11-23 19:19:59',
                'deleted_at' => NULL,
                'id' => 1098,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Client feedback: Rizza has been attentive and eager to learn.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            94 => 
            array (
                'client_id' => 824,
                'created_at' => '2020-11-25 19:26:34',
                'deleted_at' => NULL,
                'id' => 1099,
                'interaction_type' => 'text',
                'note' => '<p>Touch base regarding Angelo&#39;s performance - No concerns at all! I have been slower this month so trying to find things to keep him busy! He is doing a great job with my video edits and boosting up my youtube channel, making forms and docs! He is doing great!</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            95 => 
            array (
                'client_id' => 733,
                'created_at' => '2020-11-25 19:29:42',
                'deleted_at' => NULL,
                'id' => 1100,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Confirmed service reactivation for Ranelle / Discussed and provided an overview of Ranelle&#39;s tasks</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            96 => 
            array (
                'client_id' => 864,
                'created_at' => '2020-11-25 19:34:16',
                'deleted_at' => NULL,
                'id' => 1101,
                'interaction_type' => 'others',
                'note' => '<p>She&rsquo;s working hard, needs to focus sometimes when I point things out, but she&rsquo;s learning</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            97 => 
            array (
                'client_id' => 949,
                'created_at' => '2020-11-25 19:38:39',
                'deleted_at' => NULL,
                'id' => 1102,
                'interaction_type' => 'others',
            'note' => '<p>Meet and greet facilitated discussed tools that will be used ( KWCommand / Mojo ) Discussed start date and schedule. Discussed fact sheet and type of leads they will work on in the future. Mode of communication (whats app/ calls and text)</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => 'Ring Central Meetings',
                'others_status' => 'Please specify status type',
                'others_type' => 'meet and greet',
                'status' => 'done',
            ),
            98 => 
            array (
                'client_id' => 674,
                'created_at' => '2020-11-27 23:48:44',
                'deleted_at' => NULL,
                'id' => 1103,
                'interaction_type' => 'call',
                'note' => '<p>Nov 25, 2020</p>

<p>Touch base:</p>

<p>&quot;Ella is doing a fantastic job - I can&rsquo;t imagine ever having another VA.&nbsp;I am in the process of cross training her to make her completely indispensable.&quot;</p>

<ul>
<li style="list-style-type:disc">Jan 1st - raise - $2/hr raise</li>
<li style="list-style-type:disc">Christmas Bonus&nbsp; $1000 - for dec 5 pay out - to be charged on nov 30 - don&#39;t disclose to VA yet - Derek will be the one to tell her.</li>
</ul>

<p style="list-style-type:disc">&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            99 => 
            array (
                'client_id' => 909,
                'created_at' => '2020-11-27 23:53:31',
                'deleted_at' => NULL,
                'id' => 1104,
                'interaction_type' => 'text',
                'note' => '<p>sent payment reminder/ff up on nov 25</p>

<p>Emo sent reply on Nov 26, 2020&nbsp;- advg that cc was stolen and will update payment info after Thanksgiving</p>',
                'note_type' => 'followup',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
            100 => 
            array (
                'client_id' => 733,
                'created_at' => '2020-12-02 14:08:14',
                'deleted_at' => NULL,
                'id' => 1105,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Checked Ranelle&#39;s availability and some concerns on the spreadsheet that Ranelle is working on.</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            101 => 
            array (
                'client_id' => 720,
                'created_at' => '2020-12-02 14:12:01',
                'deleted_at' => NULL,
                'id' => 1106,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>sent email regarding holiday incentive</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Holiday incentive',
                'status' => 'acknowleged',
            ),
            102 => 
            array (
                'client_id' => 636,
                'created_at' => '2020-12-02 14:13:55',
                'deleted_at' => NULL,
                'id' => 1107,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Offered the VA shadow program (for VA&#39;s without experience) will discuss how the program works and may be interested in the said program</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'VA shadow program',
                'status' => 'open',
            ),
            103 => 
            array (
                'client_id' => 909,
                'created_at' => '2020-12-02 16:13:51',
                'deleted_at' => NULL,
                'id' => 1108,
                'interaction_type' => 'text',
                'note' => '<p>Nov 30</p>

<p>Has been following up on current invoice payment for the amount of&nbsp;$540.41.</p>

<p>cc was stolen and not secure to provide a diff card info</p>

<p>CC form sent to his email to update cc info</p>

<p>dec 1 - cc info updated and payment made</p>',
                'note_type' => 'collection',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            104 => 
            array (
                'client_id' => 909,
                'created_at' => '2020-12-02 16:18:43',
                'deleted_at' => NULL,
                'id' => 1109,
                'interaction_type' => 'email',
                'note' => '<p>Dec 2, 2020</p>

<p>email received - Emo wants to know the details on how many contacts VA has updated</p>

<p>VA - Yvette replied and advd that she has requested for an access in FUB where she can get dtls on which she has updated. She&nbsp;does 100+ contacts for her 4 hr shift. She searches for other contact info of each contact in FUB</p>

<p>AP sent as well - VA to include the list of names on her EOD report</p>

<p>VA to place comments/ notes on every contact she updates</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            105 => 
            array (
                'client_id' => 674,
                'created_at' => '2020-12-02 16:31:20',
                'deleted_at' => NULL,
                'id' => 1110,
                'interaction_type' => 'text',
                'note' => '<p>Dec 1, 2020</p>

<p>Advd that we&#39;ve just charged his cc for the $1000 incentive/ bonus for Ella.</p>

<p>Feedback: &quot;she works really hard and is getting lots of results.&nbsp;&nbsp;She is becoming indispensable!&quot;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            106 => 
            array (
                'client_id' => 359,
                'created_at' => '2020-12-02 22:09:56',
                'deleted_at' => NULL,
                'id' => 1111,
                'interaction_type' => 'text',
                'note' => '<p>Dec 1, 2020</p>

<p>Sent Christmas Incentive email&nbsp;</p>

<p>Dec 2, 2020</p>

<p>Touch base/ feedback&quot;</p>

<p>Feedback: &quot;Rich is great! Thanks&quot;</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            107 => 
            array (
                'client_id' => 797,
                'created_at' => '2020-12-03 19:33:19',
                'deleted_at' => NULL,
                'id' => 1112,
                'interaction_type' => 'email',
                'note' => '<p>VA Christmas Incentive</p>

<p>$50 each VA (4 VAs)</p>

<p>November Sales incentive received as well</p>

<p>Mae - $ 30</p>

<p>Evangeline- $130</p>

<p>INCENTIVES ARE TO BE BILLED THIS CUT-OFF, Dec 10, 2020</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            108 => 
            array (
                'client_id' => 381,
                'created_at' => '2020-12-03 19:42:06',
                'deleted_at' => NULL,
                'id' => 1113,
                'interaction_type' => 'text',
                'note' => '<p>Dec 2, 2020</p>

<p>VA Christmas Incentive&nbsp;</p>

<p>$250 for Aileen Pormento</p>

<p>Advd that it will be included in her bill for the coming billdate - Dec 10 and VA will receive the amt in full on Dec 20, 2020</p>',
                'note_type' => 'touchbase',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            109 => 
            array (
                'client_id' => 843,
                'created_at' => '2020-12-04 16:42:47',
                'deleted_at' => NULL,
                'id' => 1114,
                'interaction_type' => 'others',
                'note' => '<p>11/30-Confirmed meeting tomorrow. Sent link to use.</p>

<p>12/1-Provided positive feedback on RJ. Informed that she won&#39;t be cancelling soon and will check GVA tasks recommendation.</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            110 => 
            array (
                'client_id' => 948,
                'created_at' => '2020-12-04 16:44:46',
                'deleted_at' => NULL,
                'id' => 1115,
                'interaction_type' => 'others',
                'note' => '<p>12/1-Confirmed meet and greet schedule for tomorrow.</p>

<p>12/3-Asked for feedback on her first day with VA Eden. Client is very happy and excited as she thinks that she found the best fit for her VA needs.</p>

<p>&nbsp;</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => 'email and text',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            111 => 
            array (
                'client_id' => 559,
                'created_at' => '2020-12-04 16:47:14',
                'deleted_at' => NULL,
                'id' => 1116,
                'interaction_type' => 'email',
                'note' => '<p>11/30-Sent request for a touch base to discuss updates and feedback with Lady as she will be reaching her first year of VA services with him.&nbsp;&nbsp; &nbsp;</p>

<p>&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            112 => 
            array (
                'client_id' => 913,
                'created_at' => '2020-12-04 16:56:30',
                'deleted_at' => NULL,
                'id' => 1117,
                'interaction_type' => 'email',
                'note' => '<p>Followed up on her decision on hiring her new VA. Informed that she is not yet ready to decide as she is currently sick and asked to be followed up next week.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            113 => 
            array (
                'client_id' => 788,
                'created_at' => '2020-12-04 17:01:58',
                'deleted_at' => NULL,
                'id' => 1118,
                'interaction_type' => 'email',
                'note' => '<p>11/30-Requested to update schedule of VA Brianna.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            114 => 
            array (
                'client_id' => 747,
                'created_at' => '2020-12-04 17:04:08',
                'deleted_at' => NULL,
                'id' => 1119,
                'interaction_type' => 'email',
                'note' => '<p>11/30-Requested to update schedule of VA Charmain.</p>',
                'note_type' => 'notification',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            115 => 
            array (
                'client_id' => 902,
                'created_at' => '2020-12-08 15:55:55',
                'deleted_at' => NULL,
                'id' => 1120,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Coordinated possible schedule changes / asked for feedback regarding Kyra&#39;s performance</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            116 => 
            array (
                'client_id' => 947,
                'created_at' => '2020-12-08 15:57:02',
                'deleted_at' => NULL,
                'id' => 1121,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Touch base on Margie&#39;s performance everything is ok so far. Just on the adjustment period and still finding options on resolving issues with gvoice account.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            117 => 
            array (
                'client_id' => 733,
                'created_at' => '2020-12-08 15:57:50',
                'deleted_at' => NULL,
                'id' => 1122,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Shared and asked assistance on how he could view his spreadsheets using a different email</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            118 => 
            array (
                'client_id' => 858,
                'created_at' => '2020-12-08 16:00:06',
                'deleted_at' => NULL,
                'id' => 1123,
                'interaction_type' => 'call',
                'note' => '<p>Invoice settled and discussed resumption of services by Dec 14th</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            119 => 
            array (
                'client_id' => 950,
                'created_at' => '2020-12-08 17:03:26',
                'deleted_at' => NULL,
                'id' => 1124,
                'interaction_type' => 'email',
                'note' => '<p>First day assistance for VA Maria. Will do admin tasks for now.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            120 => 
            array (
                'client_id' => 913,
                'created_at' => '2020-12-08 17:06:01',
                'deleted_at' => NULL,
                'id' => 1125,
                'interaction_type' => 'email',
                'note' => '<p><br />
Followed-up on her decision on who to hire. Still sick but will decide this week.</p>',
                'note_type' => 'followup',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'ongoing',
            ),
            121 => 
            array (
                'client_id' => 888,
                'created_at' => '2020-12-11 17:38:55',
                'deleted_at' => NULL,
                'id' => 1126,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Notification of change coach assignment and any feedback so far. ( We appreciate the switch. )</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            122 => 
            array (
                'client_id' => 657,
                'created_at' => '2020-12-11 17:41:51',
                'deleted_at' => NULL,
                'id' => 1127,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Christmas incentive of $250/ VA for all 4 VA / Ver happy with their performance (From Holly Britner)&nbsp;</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            123 => 
            array (
                'client_id' => 347,
                'created_at' => '2020-12-11 17:42:54',
                'deleted_at' => NULL,
                'id' => 1128,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Interest in adding an additional VA and request for VA pricing for a potential referee</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'pending',
            ),
            124 => 
            array (
                'client_id' => 949,
                'created_at' => '2020-12-11 17:52:11',
                'deleted_at' => NULL,
                'id' => 1129,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Touch base on VA&#39;s performance and requested daily feedback about her calls and have decided o increase her leads</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            125 => 
            array (
                'client_id' => 871,
                'created_at' => '2020-12-11 17:53:16',
                'deleted_at' => NULL,
                'id' => 1130,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Invoice collection ( said that she will deposit the check and will be on the card around Tuesday</p>',
                    'note_type' => 'collection',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                126 => 
                array (
                    'client_id' => 947,
                    'created_at' => '2020-12-15 15:53:26',
                    'deleted_at' => NULL,
                    'id' => 1131,
                    'interaction_type' => 'text',
                    'note' => '<p>&nbsp;</p>

<p>Coordinated VA issues with Gmail and gvoice (client said if issue won&#39;t be resolved in an hour VA could log off and do MUS instead)</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                127 => 
                array (
                    'client_id' => 716,
                    'created_at' => '2020-12-15 15:55:46',
                    'deleted_at' => NULL,
                    'id' => 1132,
                    'interaction_type' => 'text',
                    'note' => '<p>&nbsp;</p>

<p>Coordinated issues with the expired logins / Troubleshoot and issue was resolved</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                128 => 
                array (
                    'client_id' => 501,
                    'created_at' => '2020-12-21 22:55:44',
                    'deleted_at' => NULL,
                    'id' => 1133,
                    'interaction_type' => 'email',
                    'note' => '<p>Dec 12, 2020</p>

<p>Lauren has advised via email that they will be replacing Anne MArgarette Suetos by January</p>

<p>Dec 18, 2020 - Update</p>

<p>Lauren has advised that they will not get a replacement for Anne Suetos but will be ending/ cancelling her services - last day of work is Dec 30th</p>

<p>Dec 21 - advd that I will be informine Anne MArgarette Suetos of their decision on her Dec 30 shift.</p>

<p>Lauren has advised that she will give Anne a glowing reference if needed.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'pending',
                ),
                129 => 
                array (
                    'client_id' => 864,
                    'created_at' => '2020-12-22 17:52:05',
                    'deleted_at' => NULL,
                    'id' => 1134,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Thanks, Jenica! Jenyly is doing a great job and I am very happy with her work.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                130 => 
                array (
                    'client_id' => 641,
                    'created_at' => '2020-12-22 18:08:14',
                    'deleted_at' => NULL,
                    'id' => 1135,
                    'interaction_type' => 'text',
                    'note' => '<p>She is amazing!</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                131 => 
                array (
                    'client_id' => 427,
                    'created_at' => '2020-12-22 18:10:03',
                    'deleted_at' => NULL,
                    'id' => 1136,
                    'interaction_type' => 'text',
                    'note' => '<p>I have had no problems with Allen. He is well liked.&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                132 => 
                array (
                    'client_id' => 301,
                    'created_at' => '2020-12-22 18:10:49',
                    'deleted_at' => NULL,
                    'id' => 1137,
                    'interaction_type' => 'email',
                    'note' => '<p>They are great and appreciate the follow up&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                133 => 
                array (
                    'client_id' => 313,
                    'created_at' => '2020-12-23 22:05:30',
                    'deleted_at' => NULL,
                    'id' => 1138,
                    'interaction_type' => 'text',
                    'note' => '<p>Validate sched for tom - Dec 24</p>

<p>&quot;She can take the day off with no pay. Well pay her for her day off on Friday&quot;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 22,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                134 => 
                array (
                    'client_id' => 636,
                    'created_at' => '2020-12-24 15:36:00',
                    'deleted_at' => NULL,
                    'id' => 1139,
                    'interaction_type' => 'email',
                    'note' => '<p>&nbsp;</p>

<p>Concern&#39;s on Toni&#39;s recent issues on follow-ups/ created a new game plan and coached VA</p>

<div style="page-break-after:always"><span style="display:none">&nbsp;</span></div>

<div style="page-break-after:always"><span style="display:none">&nbsp;</span></div>

<p>Called and confirmed concerns on VA and advised of the steps that his VA will do to address the issue.</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                135 => 
                array (
                    'client_id' => 682,
                    'created_at' => '2020-12-24 15:48:31',
                    'deleted_at' => NULL,
                    'id' => 1140,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>Expressed issues financially due to sudden business changes/wife unemployed and changed realty companies</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                136 => 
                array (
                    'client_id' => 871,
                    'created_at' => '2020-12-24 15:49:43',
                    'deleted_at' => NULL,
                    'id' => 1141,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Billed and confirmed that she was already charged for this cut-off</p>',
                    'note_type' => 'others',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => 'Payment',
                    'status' => 'done',
                ),
                137 => 
                array (
                    'client_id' => 696,
                    'created_at' => '2020-12-30 14:24:53',
                    'deleted_at' => NULL,
                    'id' => 1142,
                    'interaction_type' => 'text',
                    'note' => '<p>&nbsp;</p>

<p>Invoice reminder and temporary account suspension (for 1 week)</p>

<hr />
<p>Invoice already settled</p>',
                    'note_type' => 'notification',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                138 => 
                array (
                    'client_id' => 973,
                    'created_at' => '2021-01-06 18:13:00',
                    'deleted_at' => NULL,
                    'id' => 1143,
                    'interaction_type' => 'email',
                    'note' => '<p>Hi Jenica,&nbsp;</p>

<p>&nbsp;</p>

<p>Thank you so much for checking in! So far, so good! I know we are all new to this so it will take some time to get adjusted.&nbsp;</p>

<p>&nbsp;</p>

<p>I want to start off by saying that John is very talented and creative. I love that he is willing to put in extra time to make sure he can perform a task; however, I would appreciate it if he would follow instructions better and put more thought into his work. In the last week, there are a couple of instances where things could have been better.&nbsp;</p>

<p>&nbsp;</p>

<p>1. I had asked JP to engage with the comments on my recent post by liking them, and he liked them all. However, I later saw that some of the comments he had &quot;liked&quot; were not very kind or relevant to the&nbsp;post. I spoke to him about this later and the importance of not just performing a given task, but putting thought behind it. My profile is my reputation, and slip-ups like this can have massive consequences when it comes to the brand I have spent years trying to build online.&nbsp;</p>

<p>&nbsp;</p>

<p>2. I asked JP to edit a photo and video (with examples and a tutorial provided), but still had to communicate back and forth a lot to get the desired result. At first, I asked him if he could edit a part of a photo out and replace it with another dome. He said he could but when he did, I was not happy with the editing he sent my way. The dome he edited (images attached) looked very out of place and unnatural, so I told him to keep the original version. I need someone who is very skilled at editing and takes pride in the work that they create. I did not get that feeling from this instance.&nbsp;</p>

<p>&nbsp;</p>

<p>3. One of JP&#39;s tasks was to create and organize the highlights on my Instagram profile. While he did a good job designing graphics when it came to sorting the highlights, he didn&#39;t follow instructions exactly as I had asked but he also didn&#39;t ask for clarification if there was any confusion.&nbsp;</p>

<p>&nbsp;</p>

<p>Overall, while JP is great to work with and very efficient, I feel like I have more work on my plate now trying to manage everything instead of less. I understand this may be part of the initial adjustment period, but I&#39;ve been getting very overwhelmed with the amount of overseeing I need to do. I need a virtual assistant who puts thought behind his/her work rather than plainly execute, really uses his/her creativity in figuring out what would be best for the brand/page, is highly skilled at photoshop and editing, takes pride in his/her work, and asks for clarification when needed instead of spending time doing something incorrectly.&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                139 => 
                array (
                    'client_id' => 362,
                    'created_at' => '2021-01-06 18:50:14',
                    'deleted_at' => NULL,
                    'id' => 1144,
                    'interaction_type' => 'call',
                    'note' => '<p>&nbsp;</p>

<p>Feedback on Derren&#39;s performance (everything is good and they are very happy with Derren&#39;s performance) Asked what would be the best application for communication</p>',
                    'note_type' => 'consultation',
                    'noted_by' => 21,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'acknowleged',
                ),
                140 => 
                array (
                    'client_id' => 160,
                    'created_at' => '2021-01-12 14:44:06',
                    'deleted_at' => NULL,
                    'id' => 1145,
                    'interaction_type' => 'email',
                    'note' => '<p>Happy Wednesday. I hope your morning is off to a great start.&nbsp;<br />
Everything is going great with Kevin. Rick and John are currently training him and he seems like a great fit!<br />
I was looking at our invoice that now includes both of our VA&rsquo;s. I wanted to confirm that John&rsquo;s pay is currently at $10.60?&nbsp;<br />
If this is so I want to make sure we raise it effective now to $11.00.&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                141 => 
                array (
                    'client_id' => 118,
                    'created_at' => '2021-01-15 16:19:18',
                    'deleted_at' => NULL,
                    'id' => 1146,
                    'interaction_type' => 'call',
                    'note' => '<p>Check-in. Client feedback: We want to give a Xmas bonus to all the Va&#39;s and here is the info you need for each assistant&nbsp;<br />
Leslie $100<br />
Grace $150<br />
Louize $150<br />
Please make this payment to them closer to Christmas.&nbsp;</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                142 => 
                array (
                    'client_id' => 177,
                    'created_at' => '2021-01-15 16:20:36',
                    'deleted_at' => NULL,
                    'id' => 1147,
                    'interaction_type' => 'call',
                    'note' => '<p>So far so good with Venus. She is still learning of course, but overall she has done a good job. Get back to me in 3 months and lets see how it goes</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                143 => 
                array (
                    'client_id' => 366,
                    'created_at' => '2021-01-15 16:21:12',
                    'deleted_at' => NULL,
                    'id' => 1148,
                    'interaction_type' => 'call',
                    'note' => '<p>Margaret is doing a good job for me. Punctual, communicating well, giving me the detail needed in the daily reports. All good from my side.</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                144 => 
                array (
                    'client_id' => 366,
                    'created_at' => '2021-01-15 16:21:45',
                    'deleted_at' => NULL,
                    'id' => 1149,
                    'interaction_type' => 'call',
                    'note' => '<p>Hi thank you. Kavin is doing just fine. I will let you know if any issues ever arise</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                145 => 
                array (
                    'client_id' => 652,
                    'created_at' => '2021-01-15 16:22:19',
                    'deleted_at' => NULL,
                    'id' => 1150,
                    'interaction_type' => 'call',
                    'note' => '<p>Michelle is doing great overall!</p>',
                    'note_type' => 'touchbase',
                    'noted_by' => 30,
                    'others_interaction' => NULL,
                    'others_status' => 'Please specify status type',
                    'others_type' => NULL,
                    'status' => 'done',
                ),
                146 => 
                array (
                    'client_id' => 667,
                    'created_at' => '2021-01-15 16:23:33',
                    'deleted_at' => NULL,
                    'id' => 1151,
                    'interaction_type' => 'call',
                    'note' => '<p>Thanks for reaching out!<br />
<br />
Zoe is doing great! She is very professional and takes all directions very well. Even despite her husband having to be rushed to the hospital and also the severe weather that you all have been having over there in the last month, Zoe has consistently made herself available to make calls and has kept me updated with everything. We&#39;re lucky to have her helping us :)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            147 => 
            array (
                'client_id' => 542,
                'created_at' => '2021-01-15 16:24:12',
                'deleted_at' => NULL,
                'id' => 1152,
                'interaction_type' => 'call',
                'note' => '<p>She is amazing!</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            148 => 
            array (
                'client_id' => 693,
                'created_at' => '2021-01-15 16:24:50',
                'deleted_at' => NULL,
                'id' => 1153,
                'interaction_type' => 'call',
                'note' => '<p>She is doing great</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            149 => 
            array (
                'client_id' => 692,
                'created_at' => '2021-01-15 16:25:21',
                'deleted_at' => NULL,
                'id' => 1154,
                'interaction_type' => 'call',
                'note' => '<p>Hey, thanks for checking in. Arjay is doing a great job. No issues on our end. Also, you have been doing a great job of checking in. Thanks again.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            150 => 
            array (
                'client_id' => 715,
                'created_at' => '2021-01-15 16:26:04',
                'deleted_at' => NULL,
                'id' => 1155,
                'interaction_type' => 'call',
                'note' => '<p>Doing well thanks</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            151 => 
            array (
                'client_id' => 781,
                'created_at' => '2021-01-15 16:26:40',
                'deleted_at' => NULL,
                'id' => 1156,
                'interaction_type' => 'call',
                'note' => '<p>Things are going well. I would like to work on procedures or a checklist of the items Mac is working on. He is very talented and I would like to have a better idea of some of the steps he is taking should I ever need to work on something and he is unavailable to do the work.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            152 => 
            array (
                'client_id' => 794,
                'created_at' => '2021-01-15 16:27:18',
                'deleted_at' => NULL,
                'id' => 1157,
                'interaction_type' => 'call',
                'note' => '<p>We are very happy with Joseph actually. He has proven to be a great fit and very smart, hard worker and we appreciate him greatly.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            153 => 
            array (
                'client_id' => 565,
                'created_at' => '2021-01-15 16:28:18',
                'deleted_at' => NULL,
                'id' => 1158,
                'interaction_type' => 'call',
                'note' => '<p>As always, both individuals are doing a great job! Aaron focuses on videos and is always on time with expectations. Marion is handling an array of assignments and we&rsquo;re actually getting him trained up on a few more systems to help us out more. He&rsquo;s fast and efficient and everything the team needs to continue to move forward.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            154 => 
            array (
                'client_id' => 870,
                'created_at' => '2021-01-15 16:28:57',
                'deleted_at' => NULL,
                'id' => 1159,
                'interaction_type' => 'call',
                'note' => '<p>She is doing well on the tasks. Just a little bit of issues with grammar, but I am sure it is the language barrier. But overall well.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            155 => 
            array (
                'client_id' => 888,
                'created_at' => '2021-01-15 16:29:32',
                'deleted_at' => NULL,
                'id' => 1160,
                'interaction_type' => 'call',
                'note' => '<p>Check-in. Client feedback: Aileen is doing a great job. She&#39;s responsive and eager to take on new responsibilities. She&#39;s a quick learner and a good team member.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            156 => 
            array (
                'client_id' => 910,
                'created_at' => '2021-01-15 16:30:03',
                'deleted_at' => NULL,
                'id' => 1161,
                'interaction_type' => 'call',
                'note' => '<p>So far going great.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            157 => 
            array (
                'client_id' => 915,
                'created_at' => '2021-01-15 16:30:57',
                'deleted_at' => NULL,
                'id' => 1162,
                'interaction_type' => 'call',
                'note' => '<p>Mary Jane is doing good, we like working with her. She is on top of her tasks and give us good ideas, she is very proactive and easy to work with</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            158 => 
            array (
                'client_id' => 945,
                'created_at' => '2021-01-15 16:31:31',
                'deleted_at' => NULL,
                'id' => 1163,
                'interaction_type' => 'call',
                'note' => '<p>Hanna is performing well. I am so so pleased with her performance and eagerness to improve. Thank you for checking in</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            159 => 
            array (
                'client_id' => 951,
                'created_at' => '2021-01-15 16:32:35',
                'deleted_at' => NULL,
                'id' => 1164,
                'interaction_type' => 'call',
                'note' => '<p>Been great so far</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            160 => 
            array (
                'client_id' => 973,
                'created_at' => '2021-01-22 15:39:41',
                'deleted_at' => NULL,
                'id' => 1165,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Follow up feedback on her VA&#39;s performance still on the learning curve and making adjustments</p>',
                'note_type' => 'followup',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            161 => 
            array (
                'client_id' => 902,
                'created_at' => '2021-01-22 15:52:27',
                'deleted_at' => NULL,
                'id' => 1166,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Transfer of supervisory and endorsed him to his new coach</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            162 => 
            array (
                'client_id' => 755,
                'created_at' => '2021-01-26 22:35:50',
                'deleted_at' => NULL,
                'id' => 1167,
                'interaction_type' => 'others',
            'note' => '<p>She (Riza Alojipan) is doing good... her performance has slipped a bit recently, but still acceptable.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 35,
                'others_interaction' => 'Whatsapp',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            163 => 
            array (
                'client_id' => 947,
                'created_at' => '2021-01-29 16:58:41',
                'deleted_at' => NULL,
                'id' => 1168,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Notified of Margie&#39;s absence and offered MUS upon return to work</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            164 => 
            array (
                'client_id' => 973,
                'created_at' => '2021-01-29 17:07:15',
                'deleted_at' => NULL,
                'id' => 1169,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Recommended VA accepted start date on Monday (Transition will start tonight)</p>',
                'note_type' => 'notification',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            165 => 
            array (
                'client_id' => 162,
                'created_at' => '2021-01-29 17:08:48',
                'deleted_at' => NULL,
                'id' => 1170,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Invoice collection / will be providing a new card request forwarded to his accounting team</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            166 => 
            array (
                'client_id' => 696,
                'created_at' => '2021-01-29 17:12:36',
                'deleted_at' => NULL,
                'id' => 1171,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>Said that he will call tomorrow to settle his invoice</p>',
                'note_type' => 'collection',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            167 => 
            array (
                'client_id' => 696,
                'created_at' => '2021-01-29 17:13:27',
                'deleted_at' => NULL,
                'id' => 1172,
                'interaction_type' => 'text',
                'note' => '<p>Service suspension request</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Temporary Suspension',
                'status' => 'done',
            ),
            168 => 
            array (
                'client_id' => 975,
                'created_at' => '2021-02-08 15:24:08',
                'deleted_at' => NULL,
                'id' => 1173,
                'interaction_type' => 'call',
                'note' => '<p>Chris is very happy with Brenda. He just needs to make sure that he is on top of things when it comes to providing tasks to his GVA.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 35,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            169 => 
            array (
                'client_id' => 912,
                'created_at' => '2021-02-08 17:48:19',
                'deleted_at' => NULL,
                'id' => 1174,
                'interaction_type' => 'email',
                'note' => '<p>Followed up tasks, will have a meeting today with Eden&nbsp;</p>

<p>He also approved the proposed schedule effective tomorrow</p>

<p>M-F 2pm-6pm PST</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            170 => 
            array (
                'client_id' => 605,
                'created_at' => '2021-02-08 19:48:48',
                'deleted_at' => NULL,
                'id' => 1175,
                'interaction_type' => 'call',
                'note' => '<p>Spoke with POC Shannon</p>

<p>VA Rhey is doing just fine, he is proactive and able to finish his tasks quickly. Just make sure not to miss any details.&nbsp;</p>

<p>They will also transition to a new accounting system and they&#39;ll have more tasks for him</p>',
                'note_type' => 'touchbase',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            171 => 
            array (
                'client_id' => 862,
                'created_at' => '2021-02-08 23:47:36',
                'deleted_at' => NULL,
                'id' => 1176,
                'interaction_type' => 'text',
                'note' => '<p>Rescheduled touchbase</p>

<p>as per client , I&#39;m sorry but I can&#39;t make our 2:00 call today. I have a meeting running late. Let&#39;s do later this week.</p>',
                'note_type' => 'followup',
                'noted_by' => 23,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            172 => 
            array (
                'client_id' => 973,
                'created_at' => '2021-02-09 15:49:58',
                'deleted_at' => NULL,
                'id' => 1177,
                'interaction_type' => 'email',
                'note' => '<p>Thanks for checking in! Cholo is very talented, but I had a couple of concerns regarding timing.&nbsp;</p>

<p>&nbsp;</p>

<p>Honestly, 9 am to 1 pm EST is the best for me. I am unable to give the proper instruction to my VA at 7 am EST because I am still getting organized around that time. Also, today, there was some internet issue on Cholo&#39;s end today so he wasn&#39;t able to work, even though he kindly offered to make that up.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            173 => 
            array (
                'client_id' => 976,
                'created_at' => '2021-02-09 16:00:48',
                'deleted_at' => NULL,
                'id' => 1178,
                'interaction_type' => 'call',
                'note' => '<p>Excited on current VAs. Tasks had been assigned.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 35,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            174 => 
            array (
                'client_id' => 980,
                'created_at' => '2021-02-09 16:17:00',
                'deleted_at' => NULL,
                'id' => 1179,
                'interaction_type' => 'call',
                'note' => '<p>Linda was very optimistic and happy with Maces Performance so far.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 35,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            175 => 
            array (
                'client_id' => 979,
                'created_at' => '2021-02-11 17:44:42',
                'deleted_at' => NULL,
                'id' => 1180,
                'interaction_type' => 'text',
                'note' => '<p>Her VA is doing great. Had already set 2 appointments for her.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 35,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            176 => 
            array (
                'client_id' => 976,
                'created_at' => '2021-02-11 17:46:47',
                'deleted_at' => NULL,
                'id' => 1181,
                'interaction_type' => 'others',
                'note' => '<p>Rachel is happy with her team so far. She has questions about her invoice. A copy of her contract was sent bact to her and also gave her information on her client referral Renee Grossman.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 35,
                'others_interaction' => 'Call and Email',
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            177 => 
            array (
                'client_id' => 978,
                'created_at' => '2021-02-11 18:29:46',
                'deleted_at' => NULL,
                'id' => 1182,
                'interaction_type' => 'call',
                'note' => '<p>Currently please with current VA. She does need to work on getting dialler and better leads.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 35,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            178 => 
            array (
                'client_id' => 981,
                'created_at' => '2021-02-16 21:49:20',
                'deleted_at' => NULL,
                'id' => 1183,
                'interaction_type' => 'call',
                'note' => '<p>Recommended Replacement VA for Briann Tripoli, Client agreed and hired Julius Opinion.&nbsp;</p>',
                'note_type' => 'consultation',
                'noted_by' => 35,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            179 => 
            array (
                'client_id' => 160,
                'created_at' => '2021-02-26 15:58:02',
                'deleted_at' => NULL,
                'id' => 1184,
                'interaction_type' => 'email',
                'note' => '<p>Good evening Ashley! Everything is going great! John is awesome as always and Kevin is fitting in beautifully with our team.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            180 => 
            array (
                'client_id' => 395,
                'created_at' => '2021-04-06 16:56:17',
                'deleted_at' => NULL,
                'id' => 1185,
                'interaction_type' => 'call',
                'note' => '<p>April 5, 2021</p>

<p>Informed Young that the $100 bonus wasn&#39;t charged on prev cut-off.&nbsp;</p>

<p>Authorized to charge his card of the $100 bonus for Quevin for Va to receive today.</p>

<p>&nbsp;</p>',
                'note_type' => 'others',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Bonus for Quevin',
                'status' => 'done',
            ),
            181 => 
            array (
                'client_id' => 1001,
                'created_at' => '2021-05-24 19:57:06',
                'deleted_at' => NULL,
                'id' => 1186,
                'interaction_type' => 'others',
                'note' => '<p>PT for FT Rate</p>

<p>Mac Wheeler Amora</p>

<p>Charles Estudillo</p>',
                'note_type' => 'others',
                'noted_by' => 17,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            182 => 
            array (
                'client_id' => 534,
                'created_at' => '2021-05-25 16:10:07',
                'deleted_at' => NULL,
                'id' => 1187,
                'interaction_type' => 'email',
                'note' => '<p>Check-in. Michelle and very good and has been very busy.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            183 => 
            array (
                'client_id' => 868,
                'created_at' => '2021-05-25 16:10:56',
                'deleted_at' => NULL,
                'id' => 1188,
                'interaction_type' => 'call',
                'note' => '<p>Check-in. Mo&#39;s videos are very good. No bad feedback at all.</p>',
                'note_type' => 'touchbase',
                'noted_by' => 30,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            184 => 
            array (
                'client_id' => 906,
                'created_at' => '2021-06-02 14:34:58',
                'deleted_at' => NULL,
                'id' => 1189,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Would like to cancel services (project intended to last for 6 months and project already ended) Offered 2hrs declined and said that her onshore assistant will take over instead. Advised of the 10 days notice</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation Request',
                'status' => 'open',
            ),
            185 => 
            array (
                'client_id' => 1113,
                'created_at' => '2021-06-02 14:36:28',
                'deleted_at' => NULL,
                'id' => 1190,
                'interaction_type' => 'others',
                'note' => '<p>&nbsp;</p>

<p>Email sent regarding service cancellation VA&#39;s credentials were deactivated prior to the 10 business days</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => 'Whats app',
                'others_status' => 'Please specify status type',
                'others_type' => 'End of service',
                'status' => 'done',
            ),
            186 => 
            array (
                'client_id' => 837,
                'created_at' => '2021-06-02 14:42:40',
                'deleted_at' => NULL,
                'id' => 1191,
                'interaction_type' => 'call',
                'note' => '<p>&nbsp;</p>

<p>Confirmed Acia&#39;s replacement (Hired Nice ) discussed target start date and meet and greet</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Replacement',
                'status' => 'done',
            ),
            187 => 
            array (
                'client_id' => 1201,
                'created_at' => '2021-06-13 01:09:43',
                'deleted_at' => NULL,
                'id' => 1192,
                'interaction_type' => 'call',
                'note' => '<p>Start date: June 14</p>

<p>Schedule: M-F 7:30-3:30pm EST</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            188 => 
            array (
                'client_id' => 942,
                'created_at' => '2021-06-13 01:22:58',
                'deleted_at' => NULL,
                'id' => 1193,
                'interaction_type' => 'email',
            'note' => '<p>Reduction of Hours (from 8 hours to 5)</p>

<p>Effectivity: June 14, 2021</p>

<p>New Schedule: M-F 1pm-6pm PST</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            189 => 
            array (
                'client_id' => 985,
                'created_at' => '2021-06-13 01:30:22',
                'deleted_at' => NULL,
                'id' => 1194,
                'interaction_type' => 'call',
                'note' => '<p>Resumption of service</p>

<p>June 14, 2021</p>',
                'note_type' => 'touchbase',
                'noted_by' => 24,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            190 => 
            array (
                'client_id' => 1139,
                'created_at' => '2021-07-06 15:27:54',
                'deleted_at' => NULL,
                'id' => 1195,
                'interaction_type' => 'email',
                'note' => '<p>All is great with Clark, very happy with him.</p>

<p>&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            191 => 
            array (
                'client_id' => 362,
                'created_at' => '2021-07-16 14:25:39',
                'deleted_at' => NULL,
                'id' => 1196,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Referred 2 co compass clients (schedule call next week and tonight)</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Referral',
                'status' => 'acknowleged',
            ),
            192 => 
            array (
                'client_id' => 973,
                'created_at' => '2021-07-16 14:26:23',
                'deleted_at' => NULL,
                'id' => 1197,
                'interaction_type' => 'email',
                'note' => '<p>&nbsp;</p>

<p>Shared positive VA feedback / Follow up on her referral credit for referring Meera Verg (her mother)</p>',
                'note_type' => 'touchbase',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'acknowleged',
            ),
            193 => 
            array (
                'client_id' => 567,
                'created_at' => '2021-07-16 14:28:18',
                'deleted_at' => NULL,
                'id' => 1198,
                'interaction_type' => 'text',
                'note' => '<p>&nbsp;</p>

<p>&nbsp;</p>

<p>Asked the reason for the service cancellation request / his expectations were not set properly and the notification was sent last Monday. Decided to cancel and refuses to discuss</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => 'Cancellation',
                'status' => 'acknowleged',
            ),
            194 => 
            array (
                'client_id' => 160,
                'created_at' => '2021-07-16 15:16:40',
                'deleted_at' => NULL,
                'id' => 1199,
                'interaction_type' => 'others',
                'note' => '<p>additional VA request Alfrex Nervida</p>',
                'note_type' => 'others',
                'noted_by' => 21,
                'others_interaction' => 'RC Meet',
                'others_status' => 'Please specify status type',
                'others_type' => 'Additional VA request',
                'status' => 'done',
            ),
            195 => 
            array (
                'client_id' => 969,
                'created_at' => '2021-07-21 13:59:48',
                'deleted_at' => NULL,
                'id' => 1200,
                'interaction_type' => 'email',
                'note' => '<p>Get in touch with the client regarding VA replacement. Confirm schedule to restart the service on 7/26. VA kelly will be assigned to her. Schedule is 10am - 2pm EST.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 59,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            196 => 
            array (
                'client_id' => 160,
                'created_at' => '2021-07-21 14:55:23',
                'deleted_at' => NULL,
                'id' => 1201,
                'interaction_type' => 'call',
                'note' => '<p>Client called in to provide feedback to his VA. John and Kevin are amazing with their performance. No issues. He requested to have visibility on their daily report so he would know if we need to get a new VA to assist them and manage task. He also provided $150 bonus to John and $50 to Kevin. Advised the VA to CC the client on their SOD and EOD report moving forward.&nbsp;</p>',
                'note_type' => 'touchbase',
                'noted_by' => 59,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            197 => 
            array (
                'client_id' => 975,
                'created_at' => '2021-07-21 23:04:08',
                'deleted_at' => NULL,
                'id' => 1202,
                'interaction_type' => 'text',
                'note' => '<p>Hey! She&#39;s great! I just haven&#39;t really had anything for her to do for the last month or so</p>',
                'note_type' => 'touchbase',
                'noted_by' => 35,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'done',
            ),
            198 => 
            array (
                'client_id' => 313,
                'created_at' => '2021-08-23 19:07:34',
                'deleted_at' => NULL,
                'id' => 1203,
                'interaction_type' => 'text',
                'note' => '<p>wanted to have an earlier schedule for Jesa then add more hours in the long run. Advd that I will have to check with Jesa and will get back to him later today</p>',
                'note_type' => 'notification',
                'noted_by' => 22,
                'others_interaction' => NULL,
                'others_status' => 'Please specify status type',
                'others_type' => NULL,
                'status' => 'open',
            ),
        ));
        
        
    }
}