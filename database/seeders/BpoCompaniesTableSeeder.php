<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BpoCompaniesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('bpo_companies')->delete();
        
        \DB::table('bpo_companies')->insert(array (
            0 => 
            array (
                'bpo_name' => '24-7 intouch',
                'created_at' => '2021-08-17 17:45:52',
                'id' => 1,
                'slug' => '24-7,intouch',
            ),
            1 => 
            array (
                'bpo_name' => 'accenture inc.',
                'created_at' => '2021-08-17 17:45:53',
                'id' => 2,
                'slug' => 'accenture',
            ),
            2 => 
            array (
                'bpo_name' => 'acquire bpo',
                'created_at' => '2021-08-17 17:45:54',
                'id' => 3,
                'slug' => 'acquire',
            ),
            3 => 
            array (
                'bpo_name' => 'action labs it services phils. corp.',
                'created_at' => '2021-08-17 17:45:55',
                'id' => 4,
                'slug' => 'action labs',
            ),
            4 => 
            array (
                'bpo_name' => 'admerex solutions inc.',
                'created_at' => '2021-08-17 17:45:56',
                'id' => 5,
                'slug' => 'admerex',
            ),
            5 => 
            array (
                'bpo_name' => 'afni philippines inc.',
                'created_at' => '2021-08-17 17:45:57',
                'id' => 6,
                'slug' => 'afni',
            ),
            6 => 
            array (
                'bpo_name' => 'agent infinity inc.',
                'created_at' => '2021-08-17 17:45:58',
                'id' => 7,
                'slug' => 'agent infinity',
            ),
            7 => 
            array (
                'bpo_name' => 'aig shared services – business processing inc.',
                'created_at' => '2021-08-17 17:46:00',
                'id' => 8,
                'slug' => 'aig',
            ),
            8 => 
            array (
                'bpo_name' => 'alere philippines inc.',
                'created_at' => '2021-08-17 17:46:01',
                'id' => 9,
                'slug' => 'alere',
            ),
            9 => 
            array (
                'bpo_name' => 'alorica philippines, inc',
                'created_at' => '2021-08-17 17:46:02',
                'id' => 10,
                'slug' => 'alorica',
            ),
            10 => 
            array (
                'bpo_name' => 'american express',
                'created_at' => '2021-08-17 17:46:03',
                'id' => 11,
                'slug' => 'american express,amex',
            ),
            11 => 
            array (
            'bpo_name' => 'anz global services & operations (manila), inc.',
                'created_at' => '2021-08-17 17:46:04',
                'id' => 12,
                'slug' => 'anz',
            ),
            12 => 
            array (
                'bpo_name' => 'arb call facilities',
                'created_at' => '2021-08-17 17:46:05',
                'id' => 13,
                'slug' => 'arb',
            ),
            13 => 
            array (
                'bpo_name' => 'atos philippines',
                'created_at' => '2021-08-17 17:46:06',
                'id' => 14,
                'slug' => 'atos',
            ),
            14 => 
            array (
                'bpo_name' => 'balboa digital center services inc.',
                'created_at' => '2021-08-17 17:46:07',
                'id' => 15,
                'slug' => 'balboa',
            ),
            15 => 
            array (
                'bpo_name' => 'bgcomm contact solutions, inc.',
                'created_at' => '2021-08-17 17:46:09',
                'id' => 16,
                'slug' => 'bgcomm',
            ),
            16 => 
            array (
                'bpo_name' => 'booth and partners',
                'created_at' => '2021-08-17 17:46:10',
                'id' => 17,
                'slug' => 'booth and partners',
            ),
            17 => 
            array (
                'bpo_name' => 'bosch service solutions, inc.',
                'created_at' => '2021-08-17 17:46:11',
                'id' => 18,
                'slug' => 'bosch service,bosch',
            ),
            18 => 
            array (
                'bpo_name' => 'carparts.com philippines inc.',
                'created_at' => '2021-08-17 17:46:12',
                'id' => 19,
                'slug' => 'carparts.com',
            ),
            19 => 
            array (
                'bpo_name' => 'concentrix',
                'created_at' => '2021-08-17 17:46:13',
                'id' => 20,
                'slug' => 'concentrix',
            ),
            20 => 
            array (
                'bpo_name' => 'cognizant tech. solution phil. inc',
                'created_at' => '2021-08-17 17:46:14',
                'id' => 21,
                'slug' => 'cognizant',
            ),
            21 => 
            array (
                'bpo_name' => 'collabera solutions private limited',
                'created_at' => '2021-08-17 17:46:15',
                'id' => 22,
                'slug' => 'collabera',
            ),
            22 => 
            array (
                'bpo_name' => 'collection house',
                'created_at' => '2021-08-17 17:46:16',
                'id' => 23,
                'slug' => 'collection house',
            ),
            23 => 
            array (
                'bpo_name' => 'conduit by kgb',
                'created_at' => '2021-08-17 17:46:17',
                'id' => 24,
                'slug' => 'conduit,kgb',
            ),
            24 => 
            array (
                'bpo_name' => 'conduent business services phils., inc.',
                'created_at' => '2021-08-17 17:46:19',
                'id' => 25,
                'slug' => 'conduent',
            ),
            25 => 
            array (
                'bpo_name' => 'converge information and communication technology',
                'created_at' => '2021-08-17 17:46:20',
                'id' => 26,
                'slug' => 'converge',
            ),
            26 => 
            array (
                'bpo_name' => 'css corporation ict services inc.',
                'created_at' => '2021-08-17 17:46:21',
                'id' => 27,
                'slug' => 'css',
            ),
            27 => 
            array (
                'bpo_name' => 'curoteknika / i-plus intelligent network inc.',
                'created_at' => '2021-08-17 17:46:22',
                'id' => 28,
                'slug' => 'curoteknika,i-plus',
            ),
            28 => 
            array (
                'bpo_name' => 'dexcom philippines inc.',
                'created_at' => '2021-08-17 17:46:23',
                'id' => 29,
                'slug' => 'dexcom',
            ),
            29 => 
            array (
                'bpo_name' => 'dxc technology',
                'created_at' => '2021-08-17 17:46:24',
                'id' => 30,
                'slug' => 'dxc',
            ),
            30 => 
            array (
                'bpo_name' => 'e-teleconnect, inc.',
                'created_at' => '2021-08-17 17:46:25',
                'id' => 31,
                'slug' => 'e-teleconnect',
            ),
            31 => 
            array (
                'bpo_name' => 'eastern communications',
                'created_at' => '2021-08-17 17:46:26',
                'id' => 32,
                'slug' => 'eastern communications',
            ),
            32 => 
            array (
                'bpo_name' => 'eclaro international philippines',
                'created_at' => '2021-08-17 17:46:28',
                'id' => 33,
                'slug' => 'eclaro',
            ),
            33 => 
            array (
                'bpo_name' => 'enshored inc',
                'created_at' => '2021-08-17 17:46:29',
                'id' => 34,
                'slug' => 'enshored',
            ),
            34 => 
            array (
                'bpo_name' => 'eperformax',
                'created_at' => '2021-08-17 17:46:30',
                'id' => 35,
                'slug' => 'eperformax',
            ),
            35 => 
            array (
                'bpo_name' => 'etrade information services llc',
                'created_at' => '2021-08-17 17:46:31',
                'id' => 36,
                'slug' => 'etrade',
            ),
            36 => 
            array (
                'bpo_name' => 'everise',
                'created_at' => '2021-08-17 17:46:32',
                'id' => 37,
                'slug' => 'everise',
            ),
            37 => 
            array (
                'bpo_name' => 'excelity philippines inc.',
                'created_at' => '2021-08-17 17:46:33',
                'id' => 38,
                'slug' => 'excelity',
            ),
            38 => 
            array (
                'bpo_name' => 'excelsia bpo services inc.',
                'created_at' => '2021-08-17 17:46:34',
                'id' => 39,
                'slug' => 'excelsia',
            ),
            39 => 
            array (
                'bpo_name' => 'exl',
                'created_at' => '2021-08-17 17:46:35',
                'id' => 40,
                'slug' => 'exl',
            ),
            40 => 
            array (
                'bpo_name' => 'fis global solutions philippines inc.',
                'created_at' => '2021-08-17 17:46:37',
                'id' => 41,
                'slug' => 'fis global solutions',
            ),
            41 => 
            array (
                'bpo_name' => 'full potential bpo, inc.',
                'created_at' => '2021-08-17 17:46:38',
                'id' => 42,
                'slug' => 'full potential bpo',
            ),
            42 => 
            array (
                'bpo_name' => 'generali life assurance phils. inc.',
                'created_at' => '2021-08-17 17:46:39',
                'id' => 43,
                'slug' => 'generali life assurance ',
            ),
            43 => 
            array (
                'bpo_name' => 'global integrated contact facilities inc',
                'created_at' => '2021-08-17 17:46:40',
                'id' => 44,
                'slug' => 'generali,generali life assurance',
            ),
            44 => 
            array (
                'bpo_name' => 'global payments process centre inc',
                'created_at' => '2021-08-17 17:46:41',
                'id' => 45,
                'slug' => 'global payments process centre',
            ),
            45 => 
            array (
                'bpo_name' => 'globe business',
                'created_at' => '2021-08-17 17:46:42',
                'id' => 46,
                'slug' => 'globe business, globe',
            ),
            46 => 
            array (
                'bpo_name' => 'hcl',
                'created_at' => '2021-08-17 17:46:43',
                'id' => 47,
                'slug' => 'hcl',
            ),
            47 => 
            array (
                'bpo_name' => 'hexaware technologies ltd.',
                'created_at' => '2021-08-17 17:46:44',
                'id' => 48,
                'slug' => 'hexaware',
            ),
            48 => 
            array (
                'bpo_name' => 'hinduja global solutions limited',
                'created_at' => '2021-08-17 17:46:46',
                'id' => 49,
                'slug' => 'hinduja',
            ),
            49 => 
            array (
                'bpo_name' => 'hkt teleservices philippines inc',
                'created_at' => '2021-08-17 17:46:47',
                'id' => 50,
                'slug' => 'hkt',
            ),
            50 => 
            array (
                'bpo_name' => 'hp inc.',
                'created_at' => '2021-08-17 17:46:48',
                'id' => 51,
                'slug' => 'hp',
            ),
            51 => 
            array (
                'bpo_name' => 'ibex global',
                'created_at' => '2021-08-17 17:46:49',
                'id' => 52,
                'slug' => 'ibex global,ibex',
            ),
            52 => 
            array (
                'bpo_name' => 'ifive global',
                'created_at' => '2021-08-17 17:46:50',
                'id' => 53,
                'slug' => 'ifive,ifive global',
            ),
            53 => 
            array (
                'bpo_name' => 'infinit-o',
                'created_at' => '2021-08-17 17:46:51',
                'id' => 54,
                'slug' => 'infinit-o',
            ),
            54 => 
            array (
                'bpo_name' => 'infosys',
                'created_at' => '2021-08-17 17:46:52',
                'id' => 55,
                'slug' => 'infosys',
            ),
            55 => 
            array (
                'bpo_name' => 'ingrammicro',
                'created_at' => '2021-08-17 17:46:53',
                'id' => 56,
                'slug' => 'ingrammicro',
            ),
            56 => 
            array (
            'bpo_name' => 'inspiro (used to be spi crm)',
                'created_at' => '2021-08-17 17:46:54',
                'id' => 57,
                'slug' => 'inspiro',
            ),
            57 => 
            array (
                'bpo_name' => 'intellicare',
                'created_at' => '2021-08-17 17:46:56',
                'id' => 58,
                'slug' => 'intellicare',
            ),
            58 => 
            array (
                'bpo_name' => 'isupport world wide',
                'created_at' => '2021-08-17 17:46:57',
                'id' => 59,
                'slug' => 'isupport',
            ),
            59 => 
            array (
                'bpo_name' => 'majorel philippines corp.',
                'created_at' => '2021-08-17 17:46:58',
                'id' => 60,
                'slug' => 'majorel',
            ),
            60 => 
            array (
                'bpo_name' => 'masterpiece group philippines, inc.',
                'created_at' => '2021-08-17 17:46:59',
                'id' => 61,
                'slug' => 'masterpiece group philippines',
            ),
            61 => 
            array (
                'bpo_name' => 'max’s group inc.',
                'created_at' => '2021-08-17 17:47:00',
                'id' => 62,
                'slug' => 'max’s',
            ),
            62 => 
            array (
                'bpo_name' => 'maxicare healthcare corporation',
                'created_at' => '2021-08-17 17:47:01',
                'id' => 63,
                'slug' => 'maxicare',
            ),
            63 => 
            array (
                'bpo_name' => 'mds call solutions inc.',
                'created_at' => '2021-08-17 17:47:02',
                'id' => 64,
                'slug' => 'mds',
            ),
            64 => 
            array (
                'bpo_name' => 'meralco',
                'created_at' => '2021-08-17 17:47:03',
                'id' => 65,
                'slug' => 'meralco',
            ),
            65 => 
            array (
                'bpo_name' => 'nadela business center inc',
                'created_at' => '2021-08-17 17:47:05',
                'id' => 66,
                'slug' => 'nadela',
            ),
            66 => 
            array (
                'bpo_name' => 'nezda technologies inc.',
                'created_at' => '2021-08-17 17:47:06',
                'id' => 67,
                'slug' => 'nezda',
            ),
            67 => 
            array (
                'bpo_name' => 'offshore business processing inc.',
                'created_at' => '2021-08-17 17:47:07',
                'id' => 68,
                'slug' => 'offshore business processing',
            ),
            68 => 
            array (
                'bpo_name' => 'one contact center inc.',
                'created_at' => '2021-08-17 17:47:08',
                'id' => 69,
                'slug' => 'one contact center ',
            ),
            69 => 
            array (
                'bpo_name' => 'open access bpo',
                'created_at' => '2021-08-17 17:47:09',
                'id' => 70,
                'slug' => 'open access',
            ),
            70 => 
            array (
                'bpo_name' => 'optum global services',
                'created_at' => '2021-08-17 17:47:10',
                'id' => 71,
                'slug' => 'optum',
            ),
            71 => 
            array (
                'bpo_name' => 'outsource network',
                'created_at' => '2021-08-17 17:47:11',
                'id' => 72,
                'slug' => 'outsource network',
            ),
            72 => 
            array (
                'bpo_name' => 'p.j. lhuillier inc.',
                'created_at' => '2021-08-17 17:47:12',
                'id' => 73,
                'slug' => 'lhuillier',
            ),
            73 => 
            array (
                'bpo_name' => 'paypal philippines inc',
                'created_at' => '2021-08-17 17:47:14',
                'id' => 74,
                'slug' => 'paypal',
            ),
            74 => 
            array (
                'bpo_name' => 'peak outsourcing',
                'created_at' => '2021-08-17 17:47:15',
                'id' => 75,
                'slug' => 'peak outsourcing',
            ),
            75 => 
            array (
                'bpo_name' => 'pearson management services phils. inc.',
                'created_at' => '2021-08-17 17:47:16',
                'id' => 76,
                'slug' => 'pearson',
            ),
            76 => 
            array (
                'bpo_name' => 'personiv',
                'created_at' => '2021-08-17 17:47:17',
                'id' => 77,
                'slug' => 'personiv',
            ),
            77 => 
            array (
                'bpo_name' => 'pilipinas teleserv inc.',
                'created_at' => '2021-08-17 17:47:18',
                'id' => 78,
                'slug' => 'pilipinas teleserv',
            ),
            78 => 
            array (
                'bpo_name' => 'pmftc inc.',
                'created_at' => '2021-08-17 17:47:20',
                'id' => 79,
                'slug' => 'pmftc',
            ),
            79 => 
            array (
                'bpo_name' => 'probe philippines',
                'created_at' => '2021-08-17 17:47:21',
                'id' => 80,
                'slug' => 'probe philippines',
            ),
            80 => 
            array (
                'bpo_name' => 'qbe group shared services limited',
                'created_at' => '2021-08-17 17:47:22',
                'id' => 81,
                'slug' => 'qbe',
            ),
            81 => 
            array (
                'bpo_name' => 'qualfon philippines',
                'created_at' => '2021-08-17 17:47:23',
                'id' => 82,
                'slug' => 'qualfon',
            ),
            82 => 
            array (
                'bpo_name' => 'quantrics enterprises inc.',
                'created_at' => '2021-08-17 17:47:24',
                'id' => 83,
                'slug' => 'quantrics',
            ),
            83 => 
            array (
                'bpo_name' => 'real page',
                'created_at' => '2021-08-17 17:47:25',
                'id' => 84,
                'slug' => 'real page',
            ),
            84 => 
            array (
                'bpo_name' => 'reed elsevier shared services inc',
                'created_at' => '2021-08-17 17:47:26',
                'id' => 85,
                'slug' => 'reed elsevier',
            ),
            85 => 
            array (
                'bpo_name' => 'results manila inc',
                'created_at' => '2021-08-17 17:47:27',
                'id' => 86,
                'slug' => 'results manila',
            ),
            86 => 
            array (
                'bpo_name' => 'rj globus solutions',
                'created_at' => '2021-08-17 17:47:28',
                'id' => 87,
                'slug' => 'rj globlus',
            ),
            87 => 
            array (
                'bpo_name' => 'select voicecom',
                'created_at' => '2021-08-17 17:47:30',
                'id' => 88,
                'slug' => 'voicecom',
            ),
            88 => 
            array (
                'bpo_name' => 'sequential technology international',
                'created_at' => '2021-08-17 17:47:31',
                'id' => 89,
                'slug' => 'sequential technology',
            ),
            89 => 
            array (
            'bpo_name' => 'shell shared services (asia), b.v.',
                'created_at' => '2021-08-17 17:47:32',
                'id' => 90,
                'slug' => 'shell,shell shared services',
            ),
            90 => 
            array (
                'bpo_name' => 'sitel philippines corporation',
                'created_at' => '2021-08-17 17:47:33',
                'id' => 91,
                'slug' => 'sitel',
            ),
            91 => 
            array (
                'bpo_name' => 'sky cable corporation',
                'created_at' => '2021-08-17 17:47:34',
                'id' => 92,
                'slug' => 'sky cable',
            ),
            92 => 
            array (
                'bpo_name' => 'source telecoms',
                'created_at' => '2021-08-17 17:47:35',
                'id' => 93,
                'slug' => 'source telecoms',
            ),
            93 => 
            array (
                'bpo_name' => 'sourcefit philippines, inc.',
                'created_at' => '2021-08-17 17:47:36',
                'id' => 94,
                'slug' => 'sourcefit',
            ),
            94 => 
            array (
                'bpo_name' => 'startek philippines inc',
                'created_at' => '2021-08-17 17:47:37',
                'id' => 95,
                'slug' => 'startek',
            ),
            95 => 
            array (
                'bpo_name' => 'sterling global',
                'created_at' => '2021-08-17 17:47:39',
                'id' => 96,
                'slug' => 'sterling global',
            ),
            96 => 
            array (
                'bpo_name' => 'sutherland',
                'created_at' => '2021-08-17 17:47:40',
                'id' => 97,
                'slug' => 'sutherland',
            ),
            97 => 
            array (
                'bpo_name' => 'sykes asia, inc.',
                'created_at' => '2021-08-17 17:47:41',
                'id' => 98,
                'slug' => 'sykes',
            ),
            98 => 
            array (
                'bpo_name' => 'synchrony global services philippines',
                'created_at' => '2021-08-17 17:47:42',
                'id' => 99,
                'slug' => 'synchrony global',
            ),
            99 => 
            array (
                'bpo_name' => 'talkpush philippines',
                'created_at' => '2021-08-17 17:47:43',
                'id' => 100,
                'slug' => 'talkpush',
            ),
            100 => 
            array (
            'bpo_name' => 'tdcx (ph) inc.',
                'created_at' => '2021-08-17 17:47:44',
                'id' => 101,
                'slug' => 'tdcx',
            ),
            101 => 
            array (
                'bpo_name' => 'tech mahindra',
                'created_at' => '2021-08-17 17:47:45',
                'id' => 102,
                'slug' => 'mahindra',
            ),
            102 => 
            array (
                'bpo_name' => 'telco services australia',
                'created_at' => '2021-08-17 17:47:46',
                'id' => 103,
                'slug' => 'telco services australia',
            ),
            103 => 
            array (
                'bpo_name' => 'teleperformance',
                'created_at' => '2021-08-17 17:47:47',
                'id' => 104,
                'slug' => 'teleperformance',
            ),
            104 => 
            array (
                'bpo_name' => 'telus international',
                'created_at' => '2021-08-17 17:47:49',
                'id' => 105,
                'slug' => 'telus',
            ),
            105 => 
            array (
                'bpo_name' => 'tmx philippines inc.',
                'created_at' => '2021-08-17 17:47:50',
                'id' => 106,
                'slug' => 'tmx',
            ),
            106 => 
            array (
                'bpo_name' => 'transcom world wide phil. inc',
                'created_at' => '2021-08-17 17:47:51',
                'id' => 107,
                'slug' => 'transcom',
            ),
            107 => 
            array (
                'bpo_name' => 'transcosmos information systems phils.',
                'created_at' => '2021-08-17 17:47:52',
                'id' => 108,
                'slug' => 'transcosmos',
            ),
            108 => 
            array (
            'bpo_name' => 'uas (universal access and systems solutions)',
                'created_at' => '2021-08-17 17:47:53',
                'id' => 109,
                'slug' => 'uas',
            ),
            109 => 
            array (
                'bpo_name' => 'ubiquity',
                'created_at' => '2021-08-17 17:47:54',
                'id' => 110,
                'slug' => 'ubiquity',
            ),
            110 => 
            array (
                'bpo_name' => 'uploan',
                'created_at' => '2021-08-17 17:47:55',
                'id' => 111,
                'slug' => 'uploan',
            ),
            111 => 
            array (
                'bpo_name' => 'verto communications',
                'created_at' => '2021-08-17 17:47:56',
                'id' => 112,
                'slug' => 'verto',
            ),
            112 => 
            array (
                'bpo_name' => 'visaya knowledge process outsourcing corporation',
                'created_at' => '2021-08-17 17:47:58',
                'id' => 113,
                'slug' => 'visaya knowledge process outsourcing',
            ),
            113 => 
            array (
                'bpo_name' => 'vxi global holdings b.v. philippines',
                'created_at' => '2021-08-17 17:47:59',
                'id' => 114,
                'slug' => 'vxi',
            ),
            114 => 
            array (
                'bpo_name' => 'wellsfargo philippines solutions inc',
                'created_at' => '2021-08-17 17:48:00',
                'id' => 115,
                'slug' => 'wellsfargo',
            ),
            115 => 
            array (
            'bpo_name' => 'western union services (philippines) inc.',
                'created_at' => '2021-08-17 17:48:01',
                'id' => 116,
                'slug' => 'western union',
            ),
            116 => 
            array (
                'bpo_name' => 'william hill group',
                'created_at' => '2021-08-17 17:48:02',
                'id' => 117,
                'slug' => 'william hill ',
            ),
            117 => 
            array (
                'bpo_name' => 'wipro bpo philippines ltd inc..',
                'created_at' => '2021-08-17 17:48:03',
                'id' => 118,
                'slug' => 'wipro',
            ),
            118 => 
            array (
                'bpo_name' => 'wns philippines inc.',
                'created_at' => '2021-08-17 17:48:04',
                'id' => 119,
                'slug' => 'wns',
            ),
            119 => 
            array (
            'bpo_name' => 'adp (philippines)',
                'created_at' => '2021-08-17 17:48:05',
                'id' => 120,
                'slug' => 'adp',
            ),
            120 => 
            array (
                'bpo_name' => 'anz global services and operations',
                'created_at' => '2021-08-17 17:48:07',
                'id' => 121,
                'slug' => 'anz,anz global',
            ),
            121 => 
            array (
                'bpo_name' => 'chevron holdings inc.',
                'created_at' => '2021-08-17 17:48:08',
                'id' => 122,
                'slug' => 'chevron holdings',
            ),
            122 => 
            array (
                'bpo_name' => 'citigroup business process solutions pte. ltd.',
                'created_at' => '2021-08-17 17:48:09',
                'id' => 123,
                'slug' => 'citigroup',
            ),
            123 => 
            array (
                'bpo_name' => 'deutsche knowledge services pte. ltd.',
                'created_at' => '2021-08-17 17:48:10',
                'id' => 124,
                'slug' => 'deutsche',
            ),
            124 => 
            array (
                'bpo_name' => 'deltek inc',
                'created_at' => '2021-08-17 17:48:11',
                'id' => 125,
                'slug' => 'deltek',
            ),
            125 => 
            array (
                'bpo_name' => 'ericsson telecommunications–global shared services center',
                'created_at' => '2021-08-17 17:48:12',
                'id' => 126,
                'slug' => 'ericsson,ericsson telecommunications',
            ),
            126 => 
            array (
                'bpo_name' => 'financial rescue, llc',
                'created_at' => '2021-08-17 17:48:13',
                'id' => 127,
                'slug' => 'financial rescue',
            ),
            127 => 
            array (
            'bpo_name' => 'hewlett-packard (philippines) corporation',
                'created_at' => '2021-08-17 17:48:14',
                'id' => 128,
                'slug' => 'hewlett-packard',
            ),
            128 => 
            array (
                'bpo_name' => 'ing global services and operations, incintegreon',
                'created_at' => '2021-08-17 17:48:15',
                'id' => 129,
                'slug' => 'ing global',
            ),
            129 => 
            array (
                'bpo_name' => 'jpmorgan chase bank–philippine global service center',
                'created_at' => '2021-08-17 17:48:17',
                'id' => 130,
                'slug' => 'pmorgan chase,pmorgan',
            ),
            130 => 
            array (
                'bpo_name' => 'loanworks technologies',
                'created_at' => '2021-08-17 17:48:18',
                'id' => 131,
                'slug' => 'loanworks',
            ),
            131 => 
            array (
            'bpo_name' => 'maersk global service centres (philippines) ltd.',
                'created_at' => '2021-08-17 17:48:19',
                'id' => 132,
                'slug' => 'maersk',
            ),
            132 => 
            array (
                'bpo_name' => 'nasdaq',
                'created_at' => '2021-08-17 17:48:20',
                'id' => 133,
                'slug' => 'nasdaq',
            ),
            133 => 
            array (
                'bpo_name' => 'pcm bpo, llc.',
                'created_at' => '2021-08-17 17:48:21',
                'id' => 134,
                'slug' => 'pcm',
            ),
            134 => 
            array (
            'bpo_name' => 'shell shared services (asia) b.v.',
                'created_at' => '2021-08-17 17:48:22',
                'id' => 135,
                'slug' => 'shell shared services',
            ),
            135 => 
            array (
                'bpo_name' => 'thomson reuters',
                'created_at' => '2021-08-17 17:48:23',
                'id' => 136,
                'slug' => 'thomson reuters',
            ),
            136 => 
            array (
                'bpo_name' => 'united health group global services, inc.',
                'created_at' => '2021-08-17 17:48:24',
                'id' => 137,
                'slug' => 'united health group',
            ),
            137 => 
            array (
                'bpo_name' => 'refinitiv inc',
                'created_at' => '2021-08-17 17:48:26',
                'id' => 138,
                'slug' => 'refinitiv',
            ),
            138 => 
            array (
                'bpo_name' => 'dtcc manila',
                'created_at' => '2021-08-17 17:48:27',
                'id' => 139,
                'slug' => 'dtcc manila,dtcc',
            ),
        ));
        
        
    }
}