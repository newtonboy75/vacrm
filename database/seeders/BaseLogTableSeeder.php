<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BaseLogTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('base_log')->delete();
        
        
        
    }
}