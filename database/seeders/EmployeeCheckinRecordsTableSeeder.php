<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EmployeeCheckinRecordsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('employee_checkin_records')->delete();
        
        \DB::table('employee_checkin_records')->insert(array (
            0 => 
            array (
                'checkin_id' => 24,
                'employee_id' => 13306,
            ),
            1 => 
            array (
                'checkin_id' => 26,
                'employee_id' => 13306,
            ),
            2 => 
            array (
                'checkin_id' => 123,
                'employee_id' => 13323,
            ),
            3 => 
            array (
                'checkin_id' => 589,
                'employee_id' => 13641,
            ),
            4 => 
            array (
                'checkin_id' => 271,
                'employee_id' => 13643,
            ),
            5 => 
            array (
                'checkin_id' => 164,
                'employee_id' => 13644,
            ),
            6 => 
            array (
                'checkin_id' => 487,
                'employee_id' => 13644,
            ),
            7 => 
            array (
                'checkin_id' => 520,
                'employee_id' => 13644,
            ),
            8 => 
            array (
                'checkin_id' => 581,
                'employee_id' => 13644,
            ),
            9 => 
            array (
                'checkin_id' => 40,
                'employee_id' => 13647,
            ),
            10 => 
            array (
                'checkin_id' => 34,
                'employee_id' => 13649,
            ),
            11 => 
            array (
                'checkin_id' => 601,
                'employee_id' => 13649,
            ),
            12 => 
            array (
                'checkin_id' => 603,
                'employee_id' => 13649,
            ),
            13 => 
            array (
                'checkin_id' => 29,
                'employee_id' => 13650,
            ),
            14 => 
            array (
                'checkin_id' => 28,
                'employee_id' => 13654,
            ),
            15 => 
            array (
                'checkin_id' => 57,
                'employee_id' => 13654,
            ),
            16 => 
            array (
                'checkin_id' => 391,
                'employee_id' => 13655,
            ),
            17 => 
            array (
                'checkin_id' => 468,
                'employee_id' => 13655,
            ),
            18 => 
            array (
                'checkin_id' => 478,
                'employee_id' => 13655,
            ),
            19 => 
            array (
                'checkin_id' => 17,
                'employee_id' => 13656,
            ),
            20 => 
            array (
                'checkin_id' => 80,
                'employee_id' => 13657,
            ),
            21 => 
            array (
                'checkin_id' => 81,
                'employee_id' => 13657,
            ),
            22 => 
            array (
                'checkin_id' => 128,
                'employee_id' => 13657,
            ),
            23 => 
            array (
                'checkin_id' => 133,
                'employee_id' => 13657,
            ),
            24 => 
            array (
                'checkin_id' => 30,
                'employee_id' => 13659,
            ),
            25 => 
            array (
                'checkin_id' => 43,
                'employee_id' => 13660,
            ),
            26 => 
            array (
                'checkin_id' => 69,
                'employee_id' => 13661,
            ),
            27 => 
            array (
                'checkin_id' => 352,
                'employee_id' => 13661,
            ),
            28 => 
            array (
                'checkin_id' => 392,
                'employee_id' => 13661,
            ),
            29 => 
            array (
                'checkin_id' => 479,
                'employee_id' => 13661,
            ),
            30 => 
            array (
                'checkin_id' => 82,
                'employee_id' => 13662,
            ),
            31 => 
            array (
                'checkin_id' => 96,
                'employee_id' => 13662,
            ),
            32 => 
            array (
                'checkin_id' => 105,
                'employee_id' => 13662,
            ),
            33 => 
            array (
                'checkin_id' => 118,
                'employee_id' => 13662,
            ),
            34 => 
            array (
                'checkin_id' => 134,
                'employee_id' => 13662,
            ),
            35 => 
            array (
                'checkin_id' => 138,
                'employee_id' => 13662,
            ),
            36 => 
            array (
                'checkin_id' => 153,
                'employee_id' => 13662,
            ),
            37 => 
            array (
                'checkin_id' => 244,
                'employee_id' => 13662,
            ),
            38 => 
            array (
                'checkin_id' => 380,
                'employee_id' => 13662,
            ),
            39 => 
            array (
                'checkin_id' => 402,
                'employee_id' => 13662,
            ),
            40 => 
            array (
                'checkin_id' => 444,
                'employee_id' => 13662,
            ),
            41 => 
            array (
                'checkin_id' => 486,
                'employee_id' => 13662,
            ),
            42 => 
            array (
                'checkin_id' => 564,
                'employee_id' => 13662,
            ),
            43 => 
            array (
                'checkin_id' => 565,
                'employee_id' => 13662,
            ),
            44 => 
            array (
                'checkin_id' => 45,
                'employee_id' => 13663,
            ),
            45 => 
            array (
                'checkin_id' => 94,
                'employee_id' => 13663,
            ),
            46 => 
            array (
                'checkin_id' => 109,
                'employee_id' => 13663,
            ),
            47 => 
            array (
                'checkin_id' => 132,
                'employee_id' => 13663,
            ),
            48 => 
            array (
                'checkin_id' => 195,
                'employee_id' => 13663,
            ),
            49 => 
            array (
                'checkin_id' => 239,
                'employee_id' => 13663,
            ),
            50 => 
            array (
                'checkin_id' => 343,
                'employee_id' => 13663,
            ),
            51 => 
            array (
                'checkin_id' => 389,
                'employee_id' => 13663,
            ),
            52 => 
            array (
                'checkin_id' => 403,
                'employee_id' => 13663,
            ),
            53 => 
            array (
                'checkin_id' => 418,
                'employee_id' => 13663,
            ),
            54 => 
            array (
                'checkin_id' => 460,
                'employee_id' => 13663,
            ),
            55 => 
            array (
                'checkin_id' => 493,
                'employee_id' => 13663,
            ),
            56 => 
            array (
                'checkin_id' => 505,
                'employee_id' => 13663,
            ),
            57 => 
            array (
                'checkin_id' => 523,
                'employee_id' => 13663,
            ),
            58 => 
            array (
                'checkin_id' => 560,
                'employee_id' => 13663,
            ),
            59 => 
            array (
                'checkin_id' => 561,
                'employee_id' => 13663,
            ),
            60 => 
            array (
                'checkin_id' => 63,
                'employee_id' => 13664,
            ),
            61 => 
            array (
                'checkin_id' => 97,
                'employee_id' => 13664,
            ),
            62 => 
            array (
                'checkin_id' => 100,
                'employee_id' => 13664,
            ),
            63 => 
            array (
                'checkin_id' => 104,
                'employee_id' => 13664,
            ),
            64 => 
            array (
                'checkin_id' => 110,
                'employee_id' => 13664,
            ),
            65 => 
            array (
                'checkin_id' => 223,
                'employee_id' => 13664,
            ),
            66 => 
            array (
                'checkin_id' => 330,
                'employee_id' => 13664,
            ),
            67 => 
            array (
                'checkin_id' => 443,
                'employee_id' => 13664,
            ),
            68 => 
            array (
                'checkin_id' => 473,
                'employee_id' => 13664,
            ),
            69 => 
            array (
                'checkin_id' => 492,
                'employee_id' => 13664,
            ),
            70 => 
            array (
                'checkin_id' => 53,
                'employee_id' => 13665,
            ),
            71 => 
            array (
                'checkin_id' => 60,
                'employee_id' => 13665,
            ),
            72 => 
            array (
                'checkin_id' => 72,
                'employee_id' => 13665,
            ),
            73 => 
            array (
                'checkin_id' => 76,
                'employee_id' => 13665,
            ),
            74 => 
            array (
                'checkin_id' => 85,
                'employee_id' => 13665,
            ),
            75 => 
            array (
                'checkin_id' => 92,
                'employee_id' => 13665,
            ),
            76 => 
            array (
                'checkin_id' => 102,
                'employee_id' => 13665,
            ),
            77 => 
            array (
                'checkin_id' => 139,
                'employee_id' => 13665,
            ),
            78 => 
            array (
                'checkin_id' => 149,
                'employee_id' => 13665,
            ),
            79 => 
            array (
                'checkin_id' => 152,
                'employee_id' => 13665,
            ),
            80 => 
            array (
                'checkin_id' => 162,
                'employee_id' => 13665,
            ),
            81 => 
            array (
                'checkin_id' => 167,
                'employee_id' => 13665,
            ),
            82 => 
            array (
                'checkin_id' => 172,
                'employee_id' => 13665,
            ),
            83 => 
            array (
                'checkin_id' => 189,
                'employee_id' => 13665,
            ),
            84 => 
            array (
                'checkin_id' => 201,
                'employee_id' => 13665,
            ),
            85 => 
            array (
                'checkin_id' => 221,
                'employee_id' => 13665,
            ),
            86 => 
            array (
                'checkin_id' => 287,
                'employee_id' => 13665,
            ),
            87 => 
            array (
                'checkin_id' => 295,
                'employee_id' => 13665,
            ),
            88 => 
            array (
                'checkin_id' => 327,
                'employee_id' => 13665,
            ),
            89 => 
            array (
                'checkin_id' => 360,
                'employee_id' => 13665,
            ),
            90 => 
            array (
                'checkin_id' => 384,
                'employee_id' => 13665,
            ),
            91 => 
            array (
                'checkin_id' => 416,
                'employee_id' => 13665,
            ),
            92 => 
            array (
                'checkin_id' => 419,
                'employee_id' => 13665,
            ),
            93 => 
            array (
                'checkin_id' => 177,
                'employee_id' => 13666,
            ),
            94 => 
            array (
                'checkin_id' => 315,
                'employee_id' => 13666,
            ),
            95 => 
            array (
                'checkin_id' => 423,
                'employee_id' => 13666,
            ),
            96 => 
            array (
                'checkin_id' => 481,
                'employee_id' => 13666,
            ),
            97 => 
            array (
                'checkin_id' => 517,
                'employee_id' => 13666,
            ),
            98 => 
            array (
                'checkin_id' => 558,
                'employee_id' => 13666,
            ),
            99 => 
            array (
                'checkin_id' => 599,
                'employee_id' => 13666,
            ),
            100 => 
            array (
                'checkin_id' => 58,
                'employee_id' => 13667,
            ),
            101 => 
            array (
                'checkin_id' => 84,
                'employee_id' => 13667,
            ),
            102 => 
            array (
                'checkin_id' => 103,
                'employee_id' => 13667,
            ),
            103 => 
            array (
                'checkin_id' => 106,
                'employee_id' => 13667,
            ),
            104 => 
            array (
                'checkin_id' => 119,
                'employee_id' => 13667,
            ),
            105 => 
            array (
                'checkin_id' => 150,
                'employee_id' => 13667,
            ),
            106 => 
            array (
                'checkin_id' => 178,
                'employee_id' => 13667,
            ),
            107 => 
            array (
                'checkin_id' => 197,
                'employee_id' => 13667,
            ),
            108 => 
            array (
                'checkin_id' => 209,
                'employee_id' => 13667,
            ),
            109 => 
            array (
                'checkin_id' => 216,
                'employee_id' => 13667,
            ),
            110 => 
            array (
                'checkin_id' => 220,
                'employee_id' => 13667,
            ),
            111 => 
            array (
                'checkin_id' => 245,
                'employee_id' => 13667,
            ),
            112 => 
            array (
                'checkin_id' => 248,
                'employee_id' => 13667,
            ),
            113 => 
            array (
                'checkin_id' => 263,
                'employee_id' => 13667,
            ),
            114 => 
            array (
                'checkin_id' => 265,
                'employee_id' => 13667,
            ),
            115 => 
            array (
                'checkin_id' => 276,
                'employee_id' => 13667,
            ),
            116 => 
            array (
                'checkin_id' => 310,
                'employee_id' => 13667,
            ),
            117 => 
            array (
                'checkin_id' => 329,
                'employee_id' => 13667,
            ),
            118 => 
            array (
                'checkin_id' => 362,
                'employee_id' => 13667,
            ),
            119 => 
            array (
                'checkin_id' => 408,
                'employee_id' => 13667,
            ),
            120 => 
            array (
                'checkin_id' => 438,
                'employee_id' => 13667,
            ),
            121 => 
            array (
                'checkin_id' => 509,
                'employee_id' => 13667,
            ),
            122 => 
            array (
                'checkin_id' => 527,
                'employee_id' => 13667,
            ),
            123 => 
            array (
                'checkin_id' => 572,
                'employee_id' => 13667,
            ),
            124 => 
            array (
                'checkin_id' => 181,
                'employee_id' => 13668,
            ),
            125 => 
            array (
                'checkin_id' => 217,
                'employee_id' => 13668,
            ),
            126 => 
            array (
                'checkin_id' => 445,
                'employee_id' => 13668,
            ),
            127 => 
            array (
                'checkin_id' => 510,
                'employee_id' => 13668,
            ),
            128 => 
            array (
                'checkin_id' => 528,
                'employee_id' => 13668,
            ),
            129 => 
            array (
                'checkin_id' => 575,
                'employee_id' => 13668,
            ),
            130 => 
            array (
                'checkin_id' => 274,
                'employee_id' => 13669,
            ),
            131 => 
            array (
                'checkin_id' => 300,
                'employee_id' => 13669,
            ),
            132 => 
            array (
                'checkin_id' => 304,
                'employee_id' => 13669,
            ),
            133 => 
            array (
                'checkin_id' => 322,
                'employee_id' => 13669,
            ),
            134 => 
            array (
                'checkin_id' => 334,
                'employee_id' => 13669,
            ),
            135 => 
            array (
                'checkin_id' => 428,
                'employee_id' => 13669,
            ),
            136 => 
            array (
                'checkin_id' => 462,
                'employee_id' => 13669,
            ),
            137 => 
            array (
                'checkin_id' => 494,
                'employee_id' => 13669,
            ),
            138 => 
            array (
                'checkin_id' => 524,
                'employee_id' => 13669,
            ),
            139 => 
            array (
                'checkin_id' => 559,
                'employee_id' => 13669,
            ),
            140 => 
            array (
                'checkin_id' => 35,
                'employee_id' => 13670,
            ),
            141 => 
            array (
                'checkin_id' => 54,
                'employee_id' => 13670,
            ),
            142 => 
            array (
                'checkin_id' => 71,
                'employee_id' => 13670,
            ),
            143 => 
            array (
                'checkin_id' => 140,
                'employee_id' => 13670,
            ),
            144 => 
            array (
                'checkin_id' => 156,
                'employee_id' => 13670,
            ),
            145 => 
            array (
                'checkin_id' => 161,
                'employee_id' => 13670,
            ),
            146 => 
            array (
                'checkin_id' => 224,
                'employee_id' => 13670,
            ),
            147 => 
            array (
                'checkin_id' => 147,
                'employee_id' => 13671,
            ),
            148 => 
            array (
                'checkin_id' => 424,
                'employee_id' => 13671,
            ),
            149 => 
            array (
                'checkin_id' => 529,
                'employee_id' => 13671,
            ),
            150 => 
            array (
                'checkin_id' => 566,
                'employee_id' => 13671,
            ),
            151 => 
            array (
                'checkin_id' => 567,
                'employee_id' => 13671,
            ),
            152 => 
            array (
                'checkin_id' => 158,
                'employee_id' => 13672,
            ),
            153 => 
            array (
                'checkin_id' => 176,
                'employee_id' => 13672,
            ),
            154 => 
            array (
                'checkin_id' => 370,
                'employee_id' => 13672,
            ),
            155 => 
            array (
                'checkin_id' => 431,
                'employee_id' => 13672,
            ),
            156 => 
            array (
                'checkin_id' => 448,
                'employee_id' => 13672,
            ),
            157 => 
            array (
                'checkin_id' => 480,
                'employee_id' => 13672,
            ),
            158 => 
            array (
                'checkin_id' => 516,
                'employee_id' => 13672,
            ),
            159 => 
            array (
                'checkin_id' => 563,
                'employee_id' => 13672,
            ),
            160 => 
            array (
                'checkin_id' => 55,
                'employee_id' => 13673,
            ),
            161 => 
            array (
                'checkin_id' => 79,
                'employee_id' => 13673,
            ),
            162 => 
            array (
                'checkin_id' => 108,
                'employee_id' => 13673,
            ),
            163 => 
            array (
                'checkin_id' => 114,
                'employee_id' => 13673,
            ),
            164 => 
            array (
                'checkin_id' => 222,
                'employee_id' => 13673,
            ),
            165 => 
            array (
                'checkin_id' => 253,
                'employee_id' => 13673,
            ),
            166 => 
            array (
                'checkin_id' => 285,
                'employee_id' => 13673,
            ),
            167 => 
            array (
                'checkin_id' => 292,
                'employee_id' => 13673,
            ),
            168 => 
            array (
                'checkin_id' => 299,
                'employee_id' => 13673,
            ),
            169 => 
            array (
                'checkin_id' => 413,
                'employee_id' => 13673,
            ),
            170 => 
            array (
                'checkin_id' => 434,
                'employee_id' => 13673,
            ),
            171 => 
            array (
                'checkin_id' => 489,
                'employee_id' => 13673,
            ),
            172 => 
            array (
                'checkin_id' => 521,
                'employee_id' => 13673,
            ),
            173 => 
            array (
                'checkin_id' => 573,
                'employee_id' => 13673,
            ),
            174 => 
            array (
                'checkin_id' => 48,
                'employee_id' => 13674,
            ),
            175 => 
            array (
                'checkin_id' => 56,
                'employee_id' => 13674,
            ),
            176 => 
            array (
                'checkin_id' => 101,
                'employee_id' => 13674,
            ),
            177 => 
            array (
                'checkin_id' => 122,
                'employee_id' => 13674,
            ),
            178 => 
            array (
                'checkin_id' => 124,
                'employee_id' => 13674,
            ),
            179 => 
            array (
                'checkin_id' => 141,
                'employee_id' => 13674,
            ),
            180 => 
            array (
                'checkin_id' => 159,
                'employee_id' => 13674,
            ),
            181 => 
            array (
                'checkin_id' => 165,
                'employee_id' => 13674,
            ),
            182 => 
            array (
                'checkin_id' => 171,
                'employee_id' => 13674,
            ),
            183 => 
            array (
                'checkin_id' => 183,
                'employee_id' => 13674,
            ),
            184 => 
            array (
                'checkin_id' => 190,
                'employee_id' => 13674,
            ),
            185 => 
            array (
                'checkin_id' => 52,
                'employee_id' => 13675,
            ),
            186 => 
            array (
                'checkin_id' => 120,
                'employee_id' => 13675,
            ),
            187 => 
            array (
                'checkin_id' => 127,
                'employee_id' => 13675,
            ),
            188 => 
            array (
                'checkin_id' => 142,
                'employee_id' => 13675,
            ),
            189 => 
            array (
                'checkin_id' => 145,
                'employee_id' => 13675,
            ),
            190 => 
            array (
                'checkin_id' => 291,
                'employee_id' => 13675,
            ),
            191 => 
            array (
                'checkin_id' => 303,
                'employee_id' => 13675,
            ),
            192 => 
            array (
                'checkin_id' => 364,
                'employee_id' => 13675,
            ),
            193 => 
            array (
                'checkin_id' => 433,
                'employee_id' => 13675,
            ),
            194 => 
            array (
                'checkin_id' => 439,
                'employee_id' => 13675,
            ),
            195 => 
            array (
                'checkin_id' => 51,
                'employee_id' => 13677,
            ),
            196 => 
            array (
                'checkin_id' => 117,
                'employee_id' => 13677,
            ),
            197 => 
            array (
                'checkin_id' => 125,
                'employee_id' => 13677,
            ),
            198 => 
            array (
                'checkin_id' => 93,
                'employee_id' => 13678,
            ),
            199 => 
            array (
                'checkin_id' => 240,
                'employee_id' => 13678,
            ),
            200 => 
            array (
                'checkin_id' => 412,
                'employee_id' => 13678,
            ),
            201 => 
            array (
                'checkin_id' => 446,
                'employee_id' => 13678,
            ),
            202 => 
            array (
                'checkin_id' => 465,
                'employee_id' => 13678,
            ),
            203 => 
            array (
                'checkin_id' => 131,
                'employee_id' => 13679,
            ),
            204 => 
            array (
                'checkin_id' => 154,
                'employee_id' => 13679,
            ),
            205 => 
            array (
                'checkin_id' => 50,
                'employee_id' => 13681,
            ),
            206 => 
            array (
                'checkin_id' => 61,
                'employee_id' => 13681,
            ),
            207 => 
            array (
                'checkin_id' => 77,
                'employee_id' => 13681,
            ),
            208 => 
            array (
                'checkin_id' => 111,
                'employee_id' => 13681,
            ),
            209 => 
            array (
                'checkin_id' => 126,
                'employee_id' => 13681,
            ),
            210 => 
            array (
                'checkin_id' => 160,
                'employee_id' => 13681,
            ),
            211 => 
            array (
                'checkin_id' => 169,
                'employee_id' => 13681,
            ),
            212 => 
            array (
                'checkin_id' => 194,
                'employee_id' => 13681,
            ),
            213 => 
            array (
                'checkin_id' => 214,
                'employee_id' => 13681,
            ),
            214 => 
            array (
                'checkin_id' => 218,
                'employee_id' => 13681,
            ),
            215 => 
            array (
                'checkin_id' => 225,
                'employee_id' => 13681,
            ),
            216 => 
            array (
                'checkin_id' => 357,
                'employee_id' => 13681,
            ),
            217 => 
            array (
                'checkin_id' => 376,
                'employee_id' => 13681,
            ),
            218 => 
            array (
                'checkin_id' => 382,
                'employee_id' => 13681,
            ),
            219 => 
            array (
                'checkin_id' => 404,
                'employee_id' => 13681,
            ),
            220 => 
            array (
                'checkin_id' => 420,
                'employee_id' => 13681,
            ),
            221 => 
            array (
                'checkin_id' => 466,
                'employee_id' => 13681,
            ),
            222 => 
            array (
                'checkin_id' => 137,
                'employee_id' => 13682,
            ),
            223 => 
            array (
                'checkin_id' => 144,
                'employee_id' => 13682,
            ),
            224 => 
            array (
                'checkin_id' => 46,
                'employee_id' => 13683,
            ),
            225 => 
            array (
                'checkin_id' => 74,
                'employee_id' => 13685,
            ),
            226 => 
            array (
                'checkin_id' => 86,
                'employee_id' => 13685,
            ),
            227 => 
            array (
                'checkin_id' => 91,
                'employee_id' => 13685,
            ),
            228 => 
            array (
                'checkin_id' => 98,
                'employee_id' => 13685,
            ),
            229 => 
            array (
                'checkin_id' => 107,
                'employee_id' => 13685,
            ),
            230 => 
            array (
                'checkin_id' => 170,
                'employee_id' => 13685,
            ),
            231 => 
            array (
                'checkin_id' => 179,
                'employee_id' => 13685,
            ),
            232 => 
            array (
                'checkin_id' => 187,
                'employee_id' => 13685,
            ),
            233 => 
            array (
                'checkin_id' => 202,
                'employee_id' => 13685,
            ),
            234 => 
            array (
                'checkin_id' => 211,
                'employee_id' => 13685,
            ),
            235 => 
            array (
                'checkin_id' => 215,
                'employee_id' => 13685,
            ),
            236 => 
            array (
                'checkin_id' => 234,
                'employee_id' => 13685,
            ),
            237 => 
            array (
                'checkin_id' => 247,
                'employee_id' => 13685,
            ),
            238 => 
            array (
                'checkin_id' => 251,
                'employee_id' => 13685,
            ),
            239 => 
            array (
                'checkin_id' => 256,
                'employee_id' => 13685,
            ),
            240 => 
            array (
                'checkin_id' => 275,
                'employee_id' => 13685,
            ),
            241 => 
            array (
                'checkin_id' => 284,
                'employee_id' => 13685,
            ),
            242 => 
            array (
                'checkin_id' => 288,
                'employee_id' => 13685,
            ),
            243 => 
            array (
                'checkin_id' => 296,
                'employee_id' => 13685,
            ),
            244 => 
            array (
                'checkin_id' => 319,
                'employee_id' => 13685,
            ),
            245 => 
            array (
                'checkin_id' => 335,
                'employee_id' => 13685,
            ),
            246 => 
            array (
                'checkin_id' => 341,
                'employee_id' => 13685,
            ),
            247 => 
            array (
                'checkin_id' => 59,
                'employee_id' => 13687,
            ),
            248 => 
            array (
                'checkin_id' => 121,
                'employee_id' => 13687,
            ),
            249 => 
            array (
                'checkin_id' => 146,
                'employee_id' => 13687,
            ),
            250 => 
            array (
                'checkin_id' => 163,
                'employee_id' => 13687,
            ),
            251 => 
            array (
                'checkin_id' => 166,
                'employee_id' => 13687,
            ),
            252 => 
            array (
                'checkin_id' => 281,
                'employee_id' => 13687,
            ),
            253 => 
            array (
                'checkin_id' => 290,
                'employee_id' => 13687,
            ),
            254 => 
            array (
                'checkin_id' => 306,
                'employee_id' => 13687,
            ),
            255 => 
            array (
                'checkin_id' => 316,
                'employee_id' => 13687,
            ),
            256 => 
            array (
                'checkin_id' => 323,
                'employee_id' => 13687,
            ),
            257 => 
            array (
                'checkin_id' => 345,
                'employee_id' => 13687,
            ),
            258 => 
            array (
                'checkin_id' => 371,
                'employee_id' => 13687,
            ),
            259 => 
            array (
                'checkin_id' => 387,
                'employee_id' => 13687,
            ),
            260 => 
            array (
                'checkin_id' => 415,
                'employee_id' => 13687,
            ),
            261 => 
            array (
                'checkin_id' => 417,
                'employee_id' => 13687,
            ),
            262 => 
            array (
                'checkin_id' => 437,
                'employee_id' => 13687,
            ),
            263 => 
            array (
                'checkin_id' => 490,
                'employee_id' => 13687,
            ),
            264 => 
            array (
                'checkin_id' => 522,
                'employee_id' => 13687,
            ),
            265 => 
            array (
                'checkin_id' => 576,
                'employee_id' => 13687,
            ),
            266 => 
            array (
                'checkin_id' => 64,
                'employee_id' => 13688,
            ),
            267 => 
            array (
                'checkin_id' => 233,
                'employee_id' => 13690,
            ),
            268 => 
            array (
                'checkin_id' => 241,
                'employee_id' => 13690,
            ),
            269 => 
            array (
                'checkin_id' => 249,
                'employee_id' => 13690,
            ),
            270 => 
            array (
                'checkin_id' => 44,
                'employee_id' => 13691,
            ),
            271 => 
            array (
                'checkin_id' => 129,
                'employee_id' => 13691,
            ),
            272 => 
            array (
                'checkin_id' => 136,
                'employee_id' => 13691,
            ),
            273 => 
            array (
                'checkin_id' => 155,
                'employee_id' => 13691,
            ),
            274 => 
            array (
                'checkin_id' => 174,
                'employee_id' => 13691,
            ),
            275 => 
            array (
                'checkin_id' => 188,
                'employee_id' => 13691,
            ),
            276 => 
            array (
                'checkin_id' => 196,
                'employee_id' => 13691,
            ),
            277 => 
            array (
                'checkin_id' => 226,
                'employee_id' => 13691,
            ),
            278 => 
            array (
                'checkin_id' => 243,
                'employee_id' => 13691,
            ),
            279 => 
            array (
                'checkin_id' => 252,
                'employee_id' => 13691,
            ),
            280 => 
            array (
                'checkin_id' => 254,
                'employee_id' => 13691,
            ),
            281 => 
            array (
                'checkin_id' => 348,
                'employee_id' => 13691,
            ),
            282 => 
            array (
                'checkin_id' => 354,
                'employee_id' => 13691,
            ),
            283 => 
            array (
                'checkin_id' => 359,
                'employee_id' => 13691,
            ),
            284 => 
            array (
                'checkin_id' => 27,
                'employee_id' => 13692,
            ),
            285 => 
            array (
                'checkin_id' => 33,
                'employee_id' => 13692,
            ),
            286 => 
            array (
                'checkin_id' => 42,
                'employee_id' => 13692,
            ),
            287 => 
            array (
                'checkin_id' => 592,
                'employee_id' => 13699,
            ),
            288 => 
            array (
                'checkin_id' => 381,
                'employee_id' => 13700,
            ),
            289 => 
            array (
                'checkin_id' => 386,
                'employee_id' => 13700,
            ),
            290 => 
            array (
                'checkin_id' => 425,
                'employee_id' => 13700,
            ),
            291 => 
            array (
                'checkin_id' => 548,
                'employee_id' => 13700,
            ),
            292 => 
            array (
                'checkin_id' => 578,
                'employee_id' => 13700,
            ),
            293 => 
            array (
                'checkin_id' => 20,
                'employee_id' => 13703,
            ),
            294 => 
            array (
                'checkin_id' => 237,
                'employee_id' => 13703,
            ),
            295 => 
            array (
                'checkin_id' => 326,
                'employee_id' => 13703,
            ),
            296 => 
            array (
                'checkin_id' => 470,
                'employee_id' => 13703,
            ),
            297 => 
            array (
                'checkin_id' => 471,
                'employee_id' => 13703,
            ),
            298 => 
            array (
                'checkin_id' => 25,
                'employee_id' => 13707,
            ),
            299 => 
            array (
                'checkin_id' => 21,
                'employee_id' => 13709,
            ),
            300 => 
            array (
                'checkin_id' => 366,
                'employee_id' => 13709,
            ),
            301 => 
            array (
                'checkin_id' => 367,
                'employee_id' => 13709,
            ),
            302 => 
            array (
                'checkin_id' => 467,
                'employee_id' => 13709,
            ),
            303 => 
            array (
                'checkin_id' => 41,
                'employee_id' => 13710,
            ),
            304 => 
            array (
                'checkin_id' => 180,
                'employee_id' => 13710,
            ),
            305 => 
            array (
                'checkin_id' => 193,
                'employee_id' => 13710,
            ),
            306 => 
            array (
                'checkin_id' => 198,
                'employee_id' => 13710,
            ),
            307 => 
            array (
                'checkin_id' => 205,
                'employee_id' => 13710,
            ),
            308 => 
            array (
                'checkin_id' => 208,
                'employee_id' => 13710,
            ),
            309 => 
            array (
                'checkin_id' => 231,
                'employee_id' => 13710,
            ),
            310 => 
            array (
                'checkin_id' => 269,
                'employee_id' => 13710,
            ),
            311 => 
            array (
                'checkin_id' => 313,
                'employee_id' => 13710,
            ),
            312 => 
            array (
                'checkin_id' => 394,
                'employee_id' => 13710,
            ),
            313 => 
            array (
                'checkin_id' => 472,
                'employee_id' => 13710,
            ),
            314 => 
            array (
                'checkin_id' => 502,
                'employee_id' => 13710,
            ),
            315 => 
            array (
                'checkin_id' => 503,
                'employee_id' => 13710,
            ),
            316 => 
            array (
                'checkin_id' => 511,
                'employee_id' => 13710,
            ),
            317 => 
            array (
                'checkin_id' => 513,
                'employee_id' => 13710,
            ),
            318 => 
            array (
                'checkin_id' => 542,
                'employee_id' => 13710,
            ),
            319 => 
            array (
                'checkin_id' => 546,
                'employee_id' => 13710,
            ),
            320 => 
            array (
                'checkin_id' => 555,
                'employee_id' => 13710,
            ),
            321 => 
            array (
                'checkin_id' => 587,
                'employee_id' => 13710,
            ),
            322 => 
            array (
                'checkin_id' => 598,
                'employee_id' => 13710,
            ),
            323 => 
            array (
                'checkin_id' => 600,
                'employee_id' => 13710,
            ),
            324 => 
            array (
                'checkin_id' => 332,
                'employee_id' => 13711,
            ),
            325 => 
            array (
                'checkin_id' => 18,
                'employee_id' => 13712,
            ),
            326 => 
            array (
                'checkin_id' => 400,
                'employee_id' => 13714,
            ),
            327 => 
            array (
                'checkin_id' => 498,
                'employee_id' => 13714,
            ),
            328 => 
            array (
                'checkin_id' => 583,
                'employee_id' => 13714,
            ),
            329 => 
            array (
                'checkin_id' => 37,
                'employee_id' => 13715,
            ),
            330 => 
            array (
                'checkin_id' => 368,
                'employee_id' => 13715,
            ),
            331 => 
            array (
                'checkin_id' => 39,
                'employee_id' => 13716,
            ),
            332 => 
            array (
                'checkin_id' => 238,
                'employee_id' => 13716,
            ),
            333 => 
            array (
                'checkin_id' => 369,
                'employee_id' => 13716,
            ),
            334 => 
            array (
                'checkin_id' => 396,
                'employee_id' => 13716,
            ),
            335 => 
            array (
                'checkin_id' => 591,
                'employee_id' => 13716,
            ),
            336 => 
            array (
                'checkin_id' => 175,
                'employee_id' => 13717,
            ),
            337 => 
            array (
                'checkin_id' => 430,
                'employee_id' => 13717,
            ),
            338 => 
            array (
                'checkin_id' => 469,
                'employee_id' => 13718,
            ),
            339 => 
            array (
                'checkin_id' => 32,
                'employee_id' => 13719,
            ),
            340 => 
            array (
                'checkin_id' => 38,
                'employee_id' => 13719,
            ),
            341 => 
            array (
                'checkin_id' => 19,
                'employee_id' => 13720,
            ),
            342 => 
            array (
                'checkin_id' => 36,
                'employee_id' => 13722,
            ),
            343 => 
            array (
                'checkin_id' => 22,
                'employee_id' => 13725,
            ),
            344 => 
            array (
                'checkin_id' => 185,
                'employee_id' => 13726,
            ),
            345 => 
            array (
                'checkin_id' => 70,
                'employee_id' => 13732,
            ),
            346 => 
            array (
                'checkin_id' => 83,
                'employee_id' => 13732,
            ),
            347 => 
            array (
                'checkin_id' => 173,
                'employee_id' => 13732,
            ),
            348 => 
            array (
                'checkin_id' => 199,
                'employee_id' => 13732,
            ),
            349 => 
            array (
                'checkin_id' => 227,
                'employee_id' => 13733,
            ),
            350 => 
            array (
                'checkin_id' => 280,
                'employee_id' => 13733,
            ),
            351 => 
            array (
                'checkin_id' => 282,
                'employee_id' => 13733,
            ),
            352 => 
            array (
                'checkin_id' => 320,
                'employee_id' => 13733,
            ),
            353 => 
            array (
                'checkin_id' => 347,
                'employee_id' => 13733,
            ),
            354 => 
            array (
                'checkin_id' => 590,
                'employee_id' => 13736,
            ),
            355 => 
            array (
                'checkin_id' => 331,
                'employee_id' => 13740,
            ),
            356 => 
            array (
                'checkin_id' => 113,
                'employee_id' => 13742,
            ),
            357 => 
            array (
                'checkin_id' => 115,
                'employee_id' => 13742,
            ),
            358 => 
            array (
                'checkin_id' => 143,
                'employee_id' => 13742,
            ),
            359 => 
            array (
                'checkin_id' => 148,
                'employee_id' => 13742,
            ),
            360 => 
            array (
                'checkin_id' => 151,
                'employee_id' => 13742,
            ),
            361 => 
            array (
                'checkin_id' => 168,
                'employee_id' => 13742,
            ),
            362 => 
            array (
                'checkin_id' => 182,
                'employee_id' => 13742,
            ),
            363 => 
            array (
                'checkin_id' => 203,
                'employee_id' => 13742,
            ),
            364 => 
            array (
                'checkin_id' => 318,
                'employee_id' => 13742,
            ),
            365 => 
            array (
                'checkin_id' => 409,
                'employee_id' => 13742,
            ),
            366 => 
            array (
                'checkin_id' => 436,
                'employee_id' => 13742,
            ),
            367 => 
            array (
                'checkin_id' => 454,
                'employee_id' => 13742,
            ),
            368 => 
            array (
                'checkin_id' => 474,
                'employee_id' => 13742,
            ),
            369 => 
            array (
                'checkin_id' => 532,
                'employee_id' => 13742,
            ),
            370 => 
            array (
                'checkin_id' => 23,
                'employee_id' => 13768,
            ),
            371 => 
            array (
                'checkin_id' => 31,
                'employee_id' => 13768,
            ),
            372 => 
            array (
                'checkin_id' => 191,
                'employee_id' => 13768,
            ),
            373 => 
            array (
                'checkin_id' => 213,
                'employee_id' => 13768,
            ),
            374 => 
            array (
                'checkin_id' => 219,
                'employee_id' => 13768,
            ),
            375 => 
            array (
                'checkin_id' => 232,
                'employee_id' => 13768,
            ),
            376 => 
            array (
                'checkin_id' => 246,
                'employee_id' => 13768,
            ),
            377 => 
            array (
                'checkin_id' => 250,
                'employee_id' => 13768,
            ),
            378 => 
            array (
                'checkin_id' => 273,
                'employee_id' => 13768,
            ),
            379 => 
            array (
                'checkin_id' => 283,
                'employee_id' => 13768,
            ),
            380 => 
            array (
                'checkin_id' => 286,
                'employee_id' => 13768,
            ),
            381 => 
            array (
                'checkin_id' => 328,
                'employee_id' => 13768,
            ),
            382 => 
            array (
                'checkin_id' => 257,
                'employee_id' => 13776,
            ),
            383 => 
            array (
                'checkin_id' => 294,
                'employee_id' => 13776,
            ),
            384 => 
            array (
                'checkin_id' => 312,
                'employee_id' => 13776,
            ),
            385 => 
            array (
                'checkin_id' => 260,
                'employee_id' => 13821,
            ),
            386 => 
            array (
                'checkin_id' => 267,
                'employee_id' => 13821,
            ),
            387 => 
            array (
                'checkin_id' => 311,
                'employee_id' => 13821,
            ),
            388 => 
            array (
                'checkin_id' => 255,
                'employee_id' => 13838,
            ),
            389 => 
            array (
                'checkin_id' => 264,
                'employee_id' => 13838,
            ),
            390 => 
            array (
                'checkin_id' => 293,
                'employee_id' => 13838,
            ),
            391 => 
            array (
                'checkin_id' => 298,
                'employee_id' => 13838,
            ),
            392 => 
            array (
                'checkin_id' => 309,
                'employee_id' => 13838,
            ),
            393 => 
            array (
                'checkin_id' => 317,
                'employee_id' => 13838,
            ),
            394 => 
            array (
                'checkin_id' => 325,
                'employee_id' => 13838,
            ),
            395 => 
            array (
                'checkin_id' => 262,
                'employee_id' => 13931,
            ),
            396 => 
            array (
                'checkin_id' => 268,
                'employee_id' => 13973,
            ),
            397 => 
            array (
                'checkin_id' => 349,
                'employee_id' => 13973,
            ),
            398 => 
            array (
                'checkin_id' => 442,
                'employee_id' => 13973,
            ),
            399 => 
            array (
                'checkin_id' => 67,
                'employee_id' => 14313,
            ),
            400 => 
            array (
                'checkin_id' => 68,
                'employee_id' => 14313,
            ),
            401 => 
            array (
                'checkin_id' => 258,
                'employee_id' => 14395,
            ),
            402 => 
            array (
                'checkin_id' => 358,
                'employee_id' => 14395,
            ),
            403 => 
            array (
                'checkin_id' => 333,
                'employee_id' => 14608,
            ),
            404 => 
            array (
                'checkin_id' => 338,
                'employee_id' => 14608,
            ),
            405 => 
            array (
                'checkin_id' => 356,
                'employee_id' => 14608,
            ),
            406 => 
            array (
                'checkin_id' => 497,
                'employee_id' => 14608,
            ),
            407 => 
            array (
                'checkin_id' => 351,
                'employee_id' => 14805,
            ),
            408 => 
            array (
                'checkin_id' => 515,
                'employee_id' => 14805,
            ),
            409 => 
            array (
                'checkin_id' => 204,
                'employee_id' => 14865,
            ),
            410 => 
            array (
                'checkin_id' => 212,
                'employee_id' => 14865,
            ),
            411 => 
            array (
                'checkin_id' => 355,
                'employee_id' => 14865,
            ),
            412 => 
            array (
                'checkin_id' => 395,
                'employee_id' => 14865,
            ),
            413 => 
            array (
                'checkin_id' => 440,
                'employee_id' => 14865,
            ),
            414 => 
            array (
                'checkin_id' => 441,
                'employee_id' => 14865,
            ),
            415 => 
            array (
                'checkin_id' => 507,
                'employee_id' => 14865,
            ),
            416 => 
            array (
                'checkin_id' => 551,
                'employee_id' => 14865,
            ),
            417 => 
            array (
                'checkin_id' => 596,
                'employee_id' => 14865,
            ),
            418 => 
            array (
                'checkin_id' => 350,
                'employee_id' => 15055,
            ),
            419 => 
            array (
                'checkin_id' => 353,
                'employee_id' => 15055,
            ),
            420 => 
            array (
                'checkin_id' => 302,
                'employee_id' => 15133,
            ),
            421 => 
            array (
                'checkin_id' => 305,
                'employee_id' => 15133,
            ),
            422 => 
            array (
                'checkin_id' => 314,
                'employee_id' => 15133,
            ),
            423 => 
            array (
                'checkin_id' => 407,
                'employee_id' => 15133,
            ),
            424 => 
            array (
                'checkin_id' => 447,
                'employee_id' => 15133,
            ),
            425 => 
            array (
                'checkin_id' => 495,
                'employee_id' => 15133,
            ),
            426 => 
            array (
                'checkin_id' => 526,
                'employee_id' => 15133,
            ),
            427 => 
            array (
                'checkin_id' => 570,
                'employee_id' => 15133,
            ),
            428 => 
            array (
                'checkin_id' => 571,
                'employee_id' => 15133,
            ),
            429 => 
            array (
                'checkin_id' => 337,
                'employee_id' => 15370,
            ),
            430 => 
            array (
                'checkin_id' => 398,
                'employee_id' => 15370,
            ),
            431 => 
            array (
                'checkin_id' => 499,
                'employee_id' => 15370,
            ),
            432 => 
            array (
                'checkin_id' => 49,
                'employee_id' => 15458,
            ),
            433 => 
            array (
                'checkin_id' => 95,
                'employee_id' => 15458,
            ),
            434 => 
            array (
                'checkin_id' => 112,
                'employee_id' => 15458,
            ),
            435 => 
            array (
                'checkin_id' => 116,
                'employee_id' => 15458,
            ),
            436 => 
            array (
                'checkin_id' => 130,
                'employee_id' => 15458,
            ),
            437 => 
            array (
                'checkin_id' => 186,
                'employee_id' => 15458,
            ),
            438 => 
            array (
                'checkin_id' => 388,
                'employee_id' => 15458,
            ),
            439 => 
            array (
                'checkin_id' => 432,
                'employee_id' => 15458,
            ),
            440 => 
            array (
                'checkin_id' => 47,
                'employee_id' => 15460,
            ),
            441 => 
            array (
                'checkin_id' => 87,
                'employee_id' => 15541,
            ),
            442 => 
            array (
                'checkin_id' => 88,
                'employee_id' => 15541,
            ),
            443 => 
            array (
                'checkin_id' => 62,
                'employee_id' => 15624,
            ),
            444 => 
            array (
                'checkin_id' => 65,
                'employee_id' => 15624,
            ),
            445 => 
            array (
                'checkin_id' => 582,
                'employee_id' => 15626,
            ),
            446 => 
            array (
                'checkin_id' => 66,
                'employee_id' => 15630,
            ),
            447 => 
            array (
                'checkin_id' => 89,
                'employee_id' => 15630,
            ),
            448 => 
            array (
                'checkin_id' => 73,
                'employee_id' => 15677,
            ),
            449 => 
            array (
                'checkin_id' => 99,
                'employee_id' => 15677,
            ),
            450 => 
            array (
                'checkin_id' => 75,
                'employee_id' => 15697,
            ),
            451 => 
            array (
                'checkin_id' => 78,
                'employee_id' => 15718,
            ),
            452 => 
            array (
                'checkin_id' => 272,
                'employee_id' => 15718,
            ),
            453 => 
            array (
                'checkin_id' => 289,
                'employee_id' => 15718,
            ),
            454 => 
            array (
                'checkin_id' => 297,
                'employee_id' => 15718,
            ),
            455 => 
            array (
                'checkin_id' => 308,
                'employee_id' => 15718,
            ),
            456 => 
            array (
                'checkin_id' => 342,
                'employee_id' => 15718,
            ),
            457 => 
            array (
                'checkin_id' => 373,
                'employee_id' => 15718,
            ),
            458 => 
            array (
                'checkin_id' => 379,
                'employee_id' => 15718,
            ),
            459 => 
            array (
                'checkin_id' => 411,
                'employee_id' => 15718,
            ),
            460 => 
            array (
                'checkin_id' => 461,
                'employee_id' => 15718,
            ),
            461 => 
            array (
                'checkin_id' => 525,
                'employee_id' => 15718,
            ),
            462 => 
            array (
                'checkin_id' => 562,
                'employee_id' => 15718,
            ),
            463 => 
            array (
                'checkin_id' => 90,
                'employee_id' => 15809,
            ),
            464 => 
            array (
                'checkin_id' => 135,
                'employee_id' => 15809,
            ),
            465 => 
            array (
                'checkin_id' => 157,
                'employee_id' => 15809,
            ),
            466 => 
            array (
                'checkin_id' => 207,
                'employee_id' => 15809,
            ),
            467 => 
            array (
                'checkin_id' => 184,
                'employee_id' => 15837,
            ),
            468 => 
            array (
                'checkin_id' => 192,
                'employee_id' => 15837,
            ),
            469 => 
            array (
                'checkin_id' => 200,
                'employee_id' => 15837,
            ),
            470 => 
            array (
                'checkin_id' => 206,
                'employee_id' => 15837,
            ),
            471 => 
            array (
                'checkin_id' => 210,
                'employee_id' => 15837,
            ),
            472 => 
            array (
                'checkin_id' => 235,
                'employee_id' => 15837,
            ),
            473 => 
            array (
                'checkin_id' => 236,
                'employee_id' => 15837,
            ),
            474 => 
            array (
                'checkin_id' => 259,
                'employee_id' => 15837,
            ),
            475 => 
            array (
                'checkin_id' => 261,
                'employee_id' => 15837,
            ),
            476 => 
            array (
                'checkin_id' => 457,
                'employee_id' => 16451,
            ),
            477 => 
            array (
                'checkin_id' => 512,
                'employee_id' => 16451,
            ),
            478 => 
            array (
                'checkin_id' => 533,
                'employee_id' => 16451,
            ),
            479 => 
            array (
                'checkin_id' => 228,
                'employee_id' => 16579,
            ),
            480 => 
            array (
                'checkin_id' => 242,
                'employee_id' => 16579,
            ),
            481 => 
            array (
                'checkin_id' => 279,
                'employee_id' => 16579,
            ),
            482 => 
            array (
                'checkin_id' => 321,
                'employee_id' => 16579,
            ),
            483 => 
            array (
                'checkin_id' => 336,
                'employee_id' => 16579,
            ),
            484 => 
            array (
                'checkin_id' => 346,
                'employee_id' => 16579,
            ),
            485 => 
            array (
                'checkin_id' => 405,
                'employee_id' => 16579,
            ),
            486 => 
            array (
                'checkin_id' => 414,
                'employee_id' => 16579,
            ),
            487 => 
            array (
                'checkin_id' => 476,
                'employee_id' => 16579,
            ),
            488 => 
            array (
                'checkin_id' => 229,
                'employee_id' => 16581,
            ),
            489 => 
            array (
                'checkin_id' => 278,
                'employee_id' => 16581,
            ),
            490 => 
            array (
                'checkin_id' => 339,
                'employee_id' => 16581,
            ),
            491 => 
            array (
                'checkin_id' => 230,
                'employee_id' => 16582,
            ),
            492 => 
            array (
                'checkin_id' => 266,
                'employee_id' => 16582,
            ),
            493 => 
            array (
                'checkin_id' => 277,
                'employee_id' => 16582,
            ),
            494 => 
            array (
                'checkin_id' => 340,
                'employee_id' => 16582,
            ),
            495 => 
            array (
                'checkin_id' => 390,
                'employee_id' => 16582,
            ),
            496 => 
            array (
                'checkin_id' => 422,
                'employee_id' => 16582,
            ),
            497 => 
            array (
                'checkin_id' => 485,
                'employee_id' => 16582,
            ),
            498 => 
            array (
                'checkin_id' => 574,
                'employee_id' => 16582,
            ),
            499 => 
            array (
                'checkin_id' => 506,
                'employee_id' => 16618,
            ),
        ));
        \DB::table('employee_checkin_records')->insert(array (
            0 => 
            array (
                'checkin_id' => 401,
                'employee_id' => 16826,
            ),
            1 => 
            array (
                'checkin_id' => 553,
                'employee_id' => 16826,
            ),
            2 => 
            array (
                'checkin_id' => 514,
                'employee_id' => 16948,
            ),
            3 => 
            array (
                'checkin_id' => 543,
                'employee_id' => 17441,
            ),
            4 => 
            array (
                'checkin_id' => 427,
                'employee_id' => 17559,
            ),
            5 => 
            array (
                'checkin_id' => 301,
                'employee_id' => 17843,
            ),
            6 => 
            array (
                'checkin_id' => 307,
                'employee_id' => 17843,
            ),
            7 => 
            array (
                'checkin_id' => 324,
                'employee_id' => 17843,
            ),
            8 => 
            array (
                'checkin_id' => 344,
                'employee_id' => 17843,
            ),
            9 => 
            array (
                'checkin_id' => 426,
                'employee_id' => 17843,
            ),
            10 => 
            array (
                'checkin_id' => 488,
                'employee_id' => 17843,
            ),
            11 => 
            array (
                'checkin_id' => 577,
                'employee_id' => 17843,
            ),
            12 => 
            array (
                'checkin_id' => 455,
                'employee_id' => 17967,
            ),
            13 => 
            array (
                'checkin_id' => 459,
                'employee_id' => 17967,
            ),
            14 => 
            array (
                'checkin_id' => 483,
                'employee_id' => 17967,
            ),
            15 => 
            array (
                'checkin_id' => 519,
                'employee_id' => 17967,
            ),
            16 => 
            array (
                'checkin_id' => 530,
                'employee_id' => 18184,
            ),
            17 => 
            array (
                'checkin_id' => 580,
                'employee_id' => 18184,
            ),
            18 => 
            array (
                'checkin_id' => 585,
                'employee_id' => 18260,
            ),
            19 => 
            array (
                'checkin_id' => 458,
                'employee_id' => 18270,
            ),
            20 => 
            array (
                'checkin_id' => 531,
                'employee_id' => 18270,
            ),
            21 => 
            array (
                'checkin_id' => 579,
                'employee_id' => 18270,
            ),
            22 => 
            array (
                'checkin_id' => 534,
                'employee_id' => 18326,
            ),
            23 => 
            array (
                'checkin_id' => 535,
                'employee_id' => 18326,
            ),
            24 => 
            array (
                'checkin_id' => 536,
                'employee_id' => 18326,
            ),
            25 => 
            array (
                'checkin_id' => 537,
                'employee_id' => 18326,
            ),
            26 => 
            array (
                'checkin_id' => 538,
                'employee_id' => 18326,
            ),
            27 => 
            array (
                'checkin_id' => 539,
                'employee_id' => 18326,
            ),
            28 => 
            array (
                'checkin_id' => 545,
                'employee_id' => 18326,
            ),
            29 => 
            array (
                'checkin_id' => 504,
                'employee_id' => 18334,
            ),
            30 => 
            array (
                'checkin_id' => 552,
                'employee_id' => 18636,
            ),
            31 => 
            array (
                'checkin_id' => 377,
                'employee_id' => 18664,
            ),
            32 => 
            array (
                'checkin_id' => 452,
                'employee_id' => 18664,
            ),
            33 => 
            array (
                'checkin_id' => 496,
                'employee_id' => 18664,
            ),
            34 => 
            array (
                'checkin_id' => 541,
                'employee_id' => 18960,
            ),
            35 => 
            array (
                'checkin_id' => 453,
                'employee_id' => 18984,
            ),
            36 => 
            array (
                'checkin_id' => 491,
                'employee_id' => 18984,
            ),
            37 => 
            array (
                'checkin_id' => 554,
                'employee_id' => 18984,
            ),
            38 => 
            array (
                'checkin_id' => 456,
                'employee_id' => 19012,
            ),
            39 => 
            array (
                'checkin_id' => 475,
                'employee_id' => 19012,
            ),
            40 => 
            array (
                'checkin_id' => 477,
                'employee_id' => 19015,
            ),
            41 => 
            array (
                'checkin_id' => 365,
                'employee_id' => 19425,
            ),
            42 => 
            array (
                'checkin_id' => 449,
                'employee_id' => 19425,
            ),
            43 => 
            array (
                'checkin_id' => 361,
                'employee_id' => 19426,
            ),
            44 => 
            array (
                'checkin_id' => 375,
                'employee_id' => 19426,
            ),
            45 => 
            array (
                'checkin_id' => 406,
                'employee_id' => 19426,
            ),
            46 => 
            array (
                'checkin_id' => 435,
                'employee_id' => 19426,
            ),
            47 => 
            array (
                'checkin_id' => 482,
                'employee_id' => 19426,
            ),
            48 => 
            array (
                'checkin_id' => 518,
                'employee_id' => 19426,
            ),
            49 => 
            array (
                'checkin_id' => 463,
                'employee_id' => 19645,
            ),
            50 => 
            array (
                'checkin_id' => 363,
                'employee_id' => 19647,
            ),
            51 => 
            array (
                'checkin_id' => 372,
                'employee_id' => 19647,
            ),
            52 => 
            array (
                'checkin_id' => 378,
                'employee_id' => 19647,
            ),
            53 => 
            array (
                'checkin_id' => 501,
                'employee_id' => 19753,
            ),
            54 => 
            array (
                'checkin_id' => 374,
                'employee_id' => 19856,
            ),
            55 => 
            array (
                'checkin_id' => 383,
                'employee_id' => 19856,
            ),
            56 => 
            array (
                'checkin_id' => 385,
                'employee_id' => 19856,
            ),
            57 => 
            array (
                'checkin_id' => 393,
                'employee_id' => 19856,
            ),
            58 => 
            array (
                'checkin_id' => 399,
                'employee_id' => 19856,
            ),
            59 => 
            array (
                'checkin_id' => 410,
                'employee_id' => 19856,
            ),
            60 => 
            array (
                'checkin_id' => 421,
                'employee_id' => 19856,
            ),
            61 => 
            array (
                'checkin_id' => 484,
                'employee_id' => 19856,
            ),
            62 => 
            array (
                'checkin_id' => 451,
                'employee_id' => 19900,
            ),
            63 => 
            array (
                'checkin_id' => 464,
                'employee_id' => 19900,
            ),
            64 => 
            array (
                'checkin_id' => 500,
                'employee_id' => 19900,
            ),
            65 => 
            array (
                'checkin_id' => 508,
                'employee_id' => 19900,
            ),
            66 => 
            array (
                'checkin_id' => 550,
                'employee_id' => 20658,
            ),
            67 => 
            array (
                'checkin_id' => 397,
                'employee_id' => 21146,
            ),
            68 => 
            array (
                'checkin_id' => 429,
                'employee_id' => 21146,
            ),
            69 => 
            array (
                'checkin_id' => 450,
                'employee_id' => 21146,
            ),
            70 => 
            array (
                'checkin_id' => 557,
                'employee_id' => 22700,
            ),
            71 => 
            array (
                'checkin_id' => 568,
                'employee_id' => 22700,
            ),
            72 => 
            array (
                'checkin_id' => 540,
                'employee_id' => 22952,
            ),
            73 => 
            array (
                'checkin_id' => 549,
                'employee_id' => 23105,
            ),
            74 => 
            array (
                'checkin_id' => 588,
                'employee_id' => 23743,
            ),
            75 => 
            array (
                'checkin_id' => 547,
                'employee_id' => 24895,
            ),
            76 => 
            array (
                'checkin_id' => 544,
                'employee_id' => 24924,
            ),
            77 => 
            array (
                'checkin_id' => 584,
                'employee_id' => 25740,
            ),
            78 => 
            array (
                'checkin_id' => 556,
                'employee_id' => 26335,
            ),
            79 => 
            array (
                'checkin_id' => 586,
                'employee_id' => 26388,
            ),
            80 => 
            array (
                'checkin_id' => 569,
                'employee_id' => 30678,
            ),
            81 => 
            array (
                'checkin_id' => 593,
                'employee_id' => 36978,
            ),
            82 => 
            array (
                'checkin_id' => 594,
                'employee_id' => 37073,
            ),
            83 => 
            array (
                'checkin_id' => 595,
                'employee_id' => 37406,
            ),
            84 => 
            array (
                'checkin_id' => 597,
                'employee_id' => 37582,
            ),
            85 => 
            array (
                'checkin_id' => 604,
                'employee_id' => 49671,
            ),
            86 => 
            array (
                'checkin_id' => 602,
                'employee_id' => 54450,
            ),
            87 => 
            array (
                'checkin_id' => 605,
                'employee_id' => 55404,
            ),
        ));
        
        
    }
}