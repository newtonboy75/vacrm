<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ModelHasRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('model_has_roles')->delete();
        
        \DB::table('model_has_roles')->insert(array (
            0 => 
            array (
                'model_id' => 1,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 1,
            ),
            1 => 
            array (
                'model_id' => 3,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 1,
            ),
            2 => 
            array (
                'model_id' => 3,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            3 => 
            array (
                'model_id' => 3,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 15,
            ),
            4 => 
            array (
                'model_id' => 9,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 1,
            ),
            5 => 
            array (
                'model_id' => 9,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            6 => 
            array (
                'model_id' => 10,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 1,
            ),
            7 => 
            array (
                'model_id' => 10,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            8 => 
            array (
                'model_id' => 10,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 7,
            ),
            9 => 
            array (
                'model_id' => 11,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 1,
            ),
            10 => 
            array (
                'model_id' => 11,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            11 => 
            array (
                'model_id' => 11,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            12 => 
            array (
                'model_id' => 11,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 8,
            ),
            13 => 
            array (
                'model_id' => 12,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 1,
            ),
            14 => 
            array (
                'model_id' => 12,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            15 => 
            array (
                'model_id' => 12,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 7,
            ),
            16 => 
            array (
                'model_id' => 13,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            17 => 
            array (
                'model_id' => 13,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 6,
            ),
            18 => 
            array (
                'model_id' => 14,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            19 => 
            array (
                'model_id' => 15,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            20 => 
            array (
                'model_id' => 15,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 7,
            ),
            21 => 
            array (
                'model_id' => 16,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 2,
            ),
            22 => 
            array (
                'model_id' => 16,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 6,
            ),
            23 => 
            array (
                'model_id' => 16,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            24 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 1,
            ),
            25 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 2,
            ),
            26 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            27 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            28 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            29 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 6,
            ),
            30 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 7,
            ),
            31 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 8,
            ),
            32 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 9,
            ),
            33 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            34 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 12,
            ),
            35 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 13,
            ),
            36 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 14,
            ),
            37 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 15,
            ),
            38 => 
            array (
                'model_id' => 17,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 17,
            ),
            39 => 
            array (
                'model_id' => 18,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            40 => 
            array (
                'model_id' => 18,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            41 => 
            array (
                'model_id' => 18,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 17,
            ),
            42 => 
            array (
                'model_id' => 19,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            43 => 
            array (
                'model_id' => 19,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            44 => 
            array (
                'model_id' => 20,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            45 => 
            array (
                'model_id' => 21,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 1,
            ),
            46 => 
            array (
                'model_id' => 21,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            47 => 
            array (
                'model_id' => 21,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            48 => 
            array (
                'model_id' => 22,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            49 => 
            array (
                'model_id' => 22,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            50 => 
            array (
                'model_id' => 23,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            51 => 
            array (
                'model_id' => 23,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            52 => 
            array (
                'model_id' => 24,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            53 => 
            array (
                'model_id' => 24,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            54 => 
            array (
                'model_id' => 25,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            55 => 
            array (
                'model_id' => 26,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            56 => 
            array (
                'model_id' => 26,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 7,
            ),
            57 => 
            array (
                'model_id' => 27,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            58 => 
            array (
                'model_id' => 28,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 17,
            ),
            59 => 
            array (
                'model_id' => 29,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            60 => 
            array (
                'model_id' => 30,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            61 => 
            array (
                'model_id' => 30,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            62 => 
            array (
                'model_id' => 31,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            63 => 
            array (
                'model_id' => 32,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            64 => 
            array (
                'model_id' => 33,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            65 => 
            array (
                'model_id' => 33,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            66 => 
            array (
                'model_id' => 33,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 8,
            ),
            67 => 
            array (
                'model_id' => 34,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 6,
            ),
            68 => 
            array (
                'model_id' => 35,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            69 => 
            array (
                'model_id' => 35,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            70 => 
            array (
                'model_id' => 36,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            71 => 
            array (
                'model_id' => 37,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            72 => 
            array (
                'model_id' => 37,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 7,
            ),
            73 => 
            array (
                'model_id' => 38,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            74 => 
            array (
                'model_id' => 38,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            75 => 
            array (
                'model_id' => 39,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            76 => 
            array (
                'model_id' => 40,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            77 => 
            array (
                'model_id' => 40,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 7,
            ),
            78 => 
            array (
                'model_id' => 41,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            79 => 
            array (
                'model_id' => 42,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            80 => 
            array (
                'model_id' => 42,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            81 => 
            array (
                'model_id' => 43,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            82 => 
            array (
                'model_id' => 43,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 7,
            ),
            83 => 
            array (
                'model_id' => 44,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            84 => 
            array (
                'model_id' => 44,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 7,
            ),
            85 => 
            array (
                'model_id' => 45,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            86 => 
            array (
                'model_id' => 45,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            87 => 
            array (
                'model_id' => 46,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            88 => 
            array (
                'model_id' => 46,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            89 => 
            array (
                'model_id' => 47,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            90 => 
            array (
                'model_id' => 47,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            91 => 
            array (
                'model_id' => 48,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            92 => 
            array (
                'model_id' => 49,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            93 => 
            array (
                'model_id' => 49,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            94 => 
            array (
                'model_id' => 49,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            95 => 
            array (
                'model_id' => 50,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            96 => 
            array (
                'model_id' => 51,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            97 => 
            array (
                'model_id' => 51,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            98 => 
            array (
                'model_id' => 52,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            99 => 
            array (
                'model_id' => 53,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 8,
            ),
            100 => 
            array (
                'model_id' => 53,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            101 => 
            array (
                'model_id' => 54,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            102 => 
            array (
                'model_id' => 54,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
            103 => 
            array (
                'model_id' => 54,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            104 => 
            array (
                'model_id' => 55,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            105 => 
            array (
                'model_id' => 55,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 6,
            ),
            106 => 
            array (
                'model_id' => 55,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            107 => 
            array (
                'model_id' => 56,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            108 => 
            array (
                'model_id' => 56,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            109 => 
            array (
                'model_id' => 57,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 5,
            ),
            110 => 
            array (
                'model_id' => 57,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 8,
            ),
            111 => 
            array (
                'model_id' => 57,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            112 => 
            array (
                'model_id' => 58,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            113 => 
            array (
                'model_id' => 59,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            114 => 
            array (
                'model_id' => 60,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 10,
            ),
            115 => 
            array (
                'model_id' => 61,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 3,
            ),
            116 => 
            array (
                'model_id' => 61,
                'model_type' => 'App\\Models\\Auth\\User',
                'role_id' => 4,
            ),
        ));
        
        
    }
}