<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'created_at' => '2019-09-25 13:37:32',
                'guard_name' => 'web',
                'id' => 1,
                'name' => 'administrator',
                'updated_at' => '2019-09-25 13:37:32',
            ),
            1 => 
            array (
                'created_at' => '2019-09-25 13:37:32',
                'guard_name' => 'web',
                'id' => 2,
                'name' => 'user',
                'updated_at' => '2019-09-25 13:37:32',
            ),
            2 => 
            array (
                'created_at' => NULL,
                'guard_name' => 'web',
                'id' => 3,
                'name' => 'recruiter',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'created_at' => NULL,
                'guard_name' => 'web',
                'id' => 4,
                'name' => 'sourcer',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'created_at' => NULL,
                'guard_name' => 'web',
                'id' => 5,
                'name' => 'trainer',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'created_at' => NULL,
                'guard_name' => 'web',
                'id' => 6,
                'name' => 'hr',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'created_at' => NULL,
                'guard_name' => 'web',
                'id' => 7,
                'name' => 'recruiter manager',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'created_at' => NULL,
                'guard_name' => 'web',
                'id' => 8,
                'name' => 'training manager',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'created_at' => NULL,
                'guard_name' => 'web',
                'id' => 9,
                'name' => 'country manager',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'created_at' => NULL,
                'guard_name' => 'web',
                'id' => 10,
                'name' => 'coaches',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'created_at' => '2019-11-12 16:00:00',
                'guard_name' => 'web',
                'id' => 12,
                'name' => 'client',
                'updated_at' => '2019-11-12 16:00:00',
            ),
            11 => 
            array (
                'created_at' => '2019-12-12 14:47:32',
                'guard_name' => 'web',
                'id' => 13,
                'name' => 'marketing specialist',
                'updated_at' => '2019-12-12 14:47:32',
            ),
            12 => 
            array (
                'created_at' => '2020-01-09 16:22:08',
                'guard_name' => 'web',
                'id' => 14,
                'name' => 'qa',
                'updated_at' => '2020-01-09 16:22:08',
            ),
            13 => 
            array (
                'created_at' => '2020-02-14 20:21:56',
                'guard_name' => 'web',
                'id' => 15,
                'name' => 'dev',
                'updated_at' => '2020-02-14 20:21:56',
            ),
            14 => 
            array (
                'created_at' => '2020-06-02 23:30:04',
                'guard_name' => 'web',
                'id' => 17,
                'name' => 'client services manager',
                'updated_at' => '2020-06-02 23:30:04',
            ),
        ));
        
        
    }
}