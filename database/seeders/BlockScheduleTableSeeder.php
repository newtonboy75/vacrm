<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BlockScheduleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('block_schedule')->delete();
        
        \DB::table('block_schedule')->insert(array (
            0 => 
            array (
                'id' => 1,
                'interview_id' => 226,
                'recruiter_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'interview_id' => 227,
                'recruiter_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'interview_id' => 228,
                'recruiter_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'interview_id' => 229,
                'recruiter_id' => 4,
            ),
            4 => 
            array (
                'id' => 5,
                'interview_id' => 230,
                'recruiter_id' => 4,
            ),
            5 => 
            array (
                'id' => 6,
                'interview_id' => 231,
                'recruiter_id' => 4,
            ),
            6 => 
            array (
                'id' => 7,
                'interview_id' => 232,
                'recruiter_id' => 3,
            ),
            7 => 
            array (
                'id' => 8,
                'interview_id' => 233,
                'recruiter_id' => 3,
            ),
            8 => 
            array (
                'id' => 9,
                'interview_id' => 234,
                'recruiter_id' => 3,
            ),
            9 => 
            array (
                'id' => 10,
                'interview_id' => 235,
                'recruiter_id' => 3,
            ),
            10 => 
            array (
                'id' => 11,
                'interview_id' => 236,
                'recruiter_id' => 3,
            ),
            11 => 
            array (
                'id' => 12,
                'interview_id' => 237,
                'recruiter_id' => 1,
            ),
            12 => 
            array (
                'id' => 13,
                'interview_id' => 238,
                'recruiter_id' => 1,
            ),
            13 => 
            array (
                'id' => 14,
                'interview_id' => 239,
                'recruiter_id' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'interview_id' => 240,
                'recruiter_id' => 1,
            ),
            15 => 
            array (
                'id' => 16,
                'interview_id' => 241,
                'recruiter_id' => 1,
            ),
            16 => 
            array (
                'id' => 17,
                'interview_id' => 242,
                'recruiter_id' => 1,
            ),
            17 => 
            array (
                'id' => 18,
                'interview_id' => 243,
                'recruiter_id' => 1,
            ),
            18 => 
            array (
                'id' => 19,
                'interview_id' => 268,
                'recruiter_id' => 4,
            ),
            19 => 
            array (
                'id' => 20,
                'interview_id' => 284,
                'recruiter_id' => 3,
            ),
            20 => 
            array (
                'id' => 21,
                'interview_id' => 290,
                'recruiter_id' => 3,
            ),
            21 => 
            array (
                'id' => 22,
                'interview_id' => 291,
                'recruiter_id' => 3,
            ),
            22 => 
            array (
                'id' => 23,
                'interview_id' => 292,
                'recruiter_id' => 3,
            ),
            23 => 
            array (
                'id' => 24,
                'interview_id' => 293,
                'recruiter_id' => 3,
            ),
            24 => 
            array (
                'id' => 25,
                'interview_id' => 294,
                'recruiter_id' => 3,
            ),
            25 => 
            array (
                'id' => 26,
                'interview_id' => 296,
                'recruiter_id' => 3,
            ),
            26 => 
            array (
                'id' => 27,
                'interview_id' => 317,
                'recruiter_id' => 20,
            ),
            27 => 
            array (
                'id' => 28,
                'interview_id' => 318,
                'recruiter_id' => 20,
            ),
        ));
        
        
    }
}