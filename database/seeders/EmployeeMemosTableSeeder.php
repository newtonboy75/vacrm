<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EmployeeMemosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('employee_memos')->delete();
        
        \DB::table('employee_memos')->insert(array (
            0 => 
            array (
                'employee_id' => 13306,
                'memo_id' => 1,
            ),
            1 => 
            array (
                'employee_id' => 13306,
                'memo_id' => 2,
            ),
            2 => 
            array (
                'employee_id' => 13306,
                'memo_id' => 4,
            ),
            3 => 
            array (
                'employee_id' => 13306,
                'memo_id' => 10,
            ),
            4 => 
            array (
                'employee_id' => 13306,
                'memo_id' => 11,
            ),
            5 => 
            array (
                'employee_id' => 13306,
                'memo_id' => 14,
            ),
            6 => 
            array (
                'employee_id' => 13323,
                'memo_id' => 7,
            ),
            7 => 
            array (
                'employee_id' => 13323,
                'memo_id' => 41,
            ),
            8 => 
            array (
                'employee_id' => 13323,
                'memo_id' => 42,
            ),
            9 => 
            array (
                'employee_id' => 13643,
                'memo_id' => 44,
            ),
            10 => 
            array (
                'employee_id' => 13646,
                'memo_id' => 19,
            ),
            11 => 
            array (
                'employee_id' => 13675,
                'memo_id' => 43,
            ),
            12 => 
            array (
                'employee_id' => 13685,
                'memo_id' => 9,
            ),
            13 => 
            array (
                'employee_id' => 13688,
                'memo_id' => 27,
            ),
            14 => 
            array (
                'employee_id' => 13690,
                'memo_id' => 24,
            ),
            15 => 
            array (
                'employee_id' => 13690,
                'memo_id' => 31,
            ),
            16 => 
            array (
                'employee_id' => 13696,
                'memo_id' => 22,
            ),
            17 => 
            array (
                'employee_id' => 13710,
                'memo_id' => 28,
            ),
            18 => 
            array (
                'employee_id' => 13715,
                'memo_id' => 30,
            ),
            19 => 
            array (
                'employee_id' => 13716,
                'memo_id' => 29,
            ),
            20 => 
            array (
                'employee_id' => 13722,
                'memo_id' => 20,
            ),
            21 => 
            array (
                'employee_id' => 13725,
                'memo_id' => 21,
            ),
            22 => 
            array (
                'employee_id' => 13729,
                'memo_id' => 23,
            ),
            23 => 
            array (
                'employee_id' => 13729,
                'memo_id' => 38,
            ),
            24 => 
            array (
                'employee_id' => 13734,
                'memo_id' => 37,
            ),
            25 => 
            array (
                'employee_id' => 13737,
                'memo_id' => 25,
            ),
            26 => 
            array (
                'employee_id' => 13737,
                'memo_id' => 36,
            ),
            27 => 
            array (
                'employee_id' => 13739,
                'memo_id' => 32,
            ),
            28 => 
            array (
                'employee_id' => 13742,
                'memo_id' => 15,
            ),
            29 => 
            array (
                'employee_id' => 13747,
                'memo_id' => 3,
            ),
            30 => 
            array (
                'employee_id' => 13766,
                'memo_id' => 5,
            ),
            31 => 
            array (
                'employee_id' => 13768,
                'memo_id' => 18,
            ),
            32 => 
            array (
                'employee_id' => 14231,
                'memo_id' => 16,
            ),
            33 => 
            array (
                'employee_id' => 14591,
                'memo_id' => 17,
            ),
            34 => 
            array (
                'employee_id' => 15746,
                'memo_id' => 39,
            ),
            35 => 
            array (
                'employee_id' => 15746,
                'memo_id' => 40,
            ),
            36 => 
            array (
                'employee_id' => 16551,
                'memo_id' => 47,
            ),
            37 => 
            array (
                'employee_id' => 18405,
                'memo_id' => 45,
            ),
            38 => 
            array (
                'employee_id' => 24924,
                'memo_id' => 46,
            ),
            39 => 
            array (
                'employee_id' => 49678,
                'memo_id' => 49,
            ),
            40 => 
            array (
                'employee_id' => 54450,
                'memo_id' => 48,
            ),
            41 => 
            array (
                'employee_id' => 55404,
                'memo_id' => 50,
            ),
        ));
        
        
    }
}