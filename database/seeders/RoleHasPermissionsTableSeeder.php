<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleHasPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_has_permissions')->delete();
        
        \DB::table('role_has_permissions')->insert(array (
            0 => 
            array (
                'permission_id' => 1,
                'role_id' => 3,
            ),
            1 => 
            array (
                'permission_id' => 1,
                'role_id' => 4,
            ),
            2 => 
            array (
                'permission_id' => 1,
                'role_id' => 5,
            ),
            3 => 
            array (
                'permission_id' => 1,
                'role_id' => 6,
            ),
            4 => 
            array (
                'permission_id' => 1,
                'role_id' => 10,
            ),
            5 => 
            array (
                'permission_id' => 1,
                'role_id' => 13,
            ),
            6 => 
            array (
                'permission_id' => 1,
                'role_id' => 14,
            ),
            7 => 
            array (
                'permission_id' => 1,
                'role_id' => 15,
            ),
            8 => 
            array (
                'permission_id' => 1,
                'role_id' => 17,
            ),
        ));
        
        
    }
}