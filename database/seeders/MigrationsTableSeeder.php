<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MigrationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('migrations')->delete();
        
        \DB::table('migrations')->insert(array (
            0 => 
            array (
                'batch' => 1,
                'id' => 1,
                'migration' => '2014_10_12_000000_create_users_table',
            ),
            1 => 
            array (
                'batch' => 1,
                'id' => 2,
                'migration' => '2014_10_12_100000_create_password_resets_table',
            ),
            2 => 
            array (
                'batch' => 1,
                'id' => 3,
                'migration' => '2017_09_03_144628_create_permission_tables',
            ),
            3 => 
            array (
                'batch' => 1,
                'id' => 4,
                'migration' => '2017_09_11_174816_create_social_accounts_table',
            ),
            4 => 
            array (
                'batch' => 1,
                'id' => 5,
                'migration' => '2017_09_26_140332_create_cache_table',
            ),
            5 => 
            array (
                'batch' => 1,
                'id' => 6,
                'migration' => '2017_09_26_140528_create_sessions_table',
            ),
            6 => 
            array (
                'batch' => 1,
                'id' => 7,
                'migration' => '2017_09_26_140609_create_jobs_table',
            ),
            7 => 
            array (
                'batch' => 1,
                'id' => 8,
                'migration' => '2018_04_08_033256_create_password_histories_table',
            ),
            8 => 
            array (
                'batch' => 1,
                'id' => 9,
                'migration' => '2018_11_21_000001_create_ledgers_table',
            ),
            9 => 
            array (
                'batch' => 1,
                'id' => 10,
                'migration' => '2019_08_19_000000_create_failed_jobs_table',
            ),
        ));
        
        
    }
}