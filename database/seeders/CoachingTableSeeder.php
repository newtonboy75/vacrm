<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CoachingTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('coaching')->delete();
        
        \DB::table('coaching')->insert(array (
            0 => 
            array (
                'date_served' => '2019-11-19 22:31:00',
                'date_signed' => '2019-11-19 22:31:00',
                'id' => 1,
                'issued_by' => 1,
                'memo' => NULL,
                'status' => 1,
                'violation' => NULL,
            ),
            1 => 
            array (
                'date_served' => '2019-11-19 22:31:00',
                'date_signed' => '2019-11-19 22:31:00',
                'id' => 2,
                'issued_by' => 1,
                'memo' => NULL,
                'status' => 1,
                'violation' => NULL,
            ),
            2 => 
            array (
                'date_served' => '2000-01-12 04:00:00',
                'date_signed' => '2000-01-12 04:00:00',
                'id' => 3,
                'issued_by' => 3,
                'memo' => 'coaching_1Nov_Thu_2019_02_09_17-3.png',
                'status' => 1,
                'violation' => 'testerrrrr',
            ),
            3 => 
            array (
                'date_served' => '2000-01-12 04:00:00',
                'date_signed' => '2000-01-12 04:00:00',
                'id' => 4,
                'issued_by' => 1,
                'memo' => 'coaching_1Nov_Thu_2019_02_13_05-4.png',
                'status' => 1,
                'violation' => 'ok',
            ),
            4 => 
            array (
                'date_served' => '2019-11-29 21:50:00',
                'date_signed' => '2019-11-29 21:50:00',
                'id' => 6,
                'issued_by' => 1,
                'memo' => 'coaching_2Nov_Fri_2019_09_50_32-6.jpg',
                'status' => 1,
                'violation' => NULL,
            ),
            5 => 
            array (
                'date_served' => '2019-12-24 20:48:00',
                'date_signed' => '2019-12-23 20:48:00',
                'id' => 7,
                'issued_by' => 1,
                'memo' => NULL,
                'status' => 1,
                'violation' => NULL,
            ),
            6 => 
            array (
                'date_served' => '2020-02-08 03:35:00',
                'date_signed' => '2020-02-08 03:35:00',
                'id' => 8,
                'issued_by' => 21,
                'memo' => 'coaching_13720Feb_Fri_2020_07_39_27-8.docx',
                'status' => 1,
            'violation' => 'Poor perfomance (Client base)',
            ),
            7 => 
            array (
                'date_served' => '2020-02-03 11:53:00',
                'date_signed' => '2020-02-04 07:42:00',
                'id' => 9,
                'issued_by' => 22,
                'memo' => 'coaching_13703Feb_Tue_2020_03_55_36-9.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            8 => 
            array (
                'date_served' => '2020-02-03 11:57:00',
                'date_signed' => '2020-02-04 04:00:00',
                'id' => 10,
                'issued_by' => 22,
                'memo' => 'coaching_13709Feb_Tue_2020_03_58_47-10.docx',
                'status' => 1,
                'violation' => 'January 2020 Attendance Infractions',
            ),
            9 => 
            array (
                'date_served' => '2020-02-04 11:59:00',
                'date_signed' => '2020-02-05 03:59:00',
                'id' => 11,
                'issued_by' => 22,
                'memo' => 'coaching_13722Feb_Tue_2020_04_00_18-11.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            10 => 
            array (
                'date_served' => '2020-02-06 11:01:00',
                'date_signed' => '2020-02-07 12:01:00',
                'id' => 12,
                'issued_by' => 22,
                'memo' => 'coaching_13711Feb_Tue_2020_04_01_57-12.docx',
                'status' => 1,
                'violation' => 'January 2020. Attendance Infractions',
            ),
            11 => 
            array (
                'date_served' => '2020-02-05 11:04:00',
                'date_signed' => '2020-02-06 12:04:00',
                'id' => 13,
                'issued_by' => 22,
                'memo' => 'coaching_13708Feb_Tue_2020_04_05_45-13.docx',
                'status' => 1,
                'violation' => 'January 2020 Attendance Infractions',
            ),
            12 => 
            array (
                'date_served' => '2020-02-05 11:07:00',
                'date_signed' => '2020-02-06 12:07:00',
                'id' => 14,
                'issued_by' => 22,
                'memo' => 'coaching_13710Feb_Tue_2020_04_08_01-14.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            13 => 
            array (
                'date_served' => '2020-02-04 11:08:00',
                'date_signed' => '2020-02-05 12:09:00',
                'id' => 15,
                'issued_by' => 22,
                'memo' => 'coaching_13717Feb_Tue_2020_04_09_21-15.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            14 => 
            array (
                'date_served' => '2020-02-03 11:10:00',
                'date_signed' => '2020-02-04 02:10:00',
                'id' => 16,
                'issued_by' => 22,
                'memo' => 'coaching_13712Feb_Tue_2020_04_11_45-16.docx',
                'status' => 1,
                'violation' => NULL,
            ),
            15 => 
            array (
                'date_served' => '2020-02-03 11:13:00',
                'date_signed' => '2020-02-04 12:13:00',
                'id' => 17,
                'issued_by' => 22,
                'memo' => 'coaching_13719Feb_Tue_2020_04_14_07-17.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            16 => 
            array (
                'date_served' => '2020-02-03 11:15:00',
                'date_signed' => '2020-02-04 12:15:00',
                'id' => 18,
                'issued_by' => 22,
                'memo' => 'coaching_13720Feb_Tue_2020_04_15_59-18.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            17 => 
            array (
                'date_served' => '2020-02-03 11:17:00',
                'date_signed' => '2020-02-04 12:17:00',
                'id' => 19,
                'issued_by' => 22,
                'memo' => 'coaching_13713Feb_Tue_2020_04_18_13-19.docx',
                'status' => 1,
                'violation' => 'January 2020 Attendance Infractions',
            ),
            18 => 
            array (
                'date_served' => '2020-02-19 12:34:00',
                'date_signed' => '2020-02-19 12:35:00',
                'id' => 20,
                'issued_by' => 24,
                'memo' => NULL,
                'status' => 1,
                'violation' => NULL,
            ),
            19 => 
            array (
                'date_served' => '2020-02-26 04:08:00',
                'date_signed' => '2020-02-26 04:08:00',
                'id' => 21,
                'issued_by' => 19,
                'memo' => 'coaching_13306Feb_Wed_2020_04_09_15-21.pdf',
                'status' => 1,
                'violation' => 'sdfsdfds',
            ),
            20 => 
            array (
                'date_served' => '2020-02-28 10:04:00',
                'date_signed' => '2020-02-29 05:05:00',
                'id' => 22,
                'issued_by' => 22,
                'memo' => 'coaching_13768Feb_Sat_2020_05_05_42-22.docx',
                'status' => 1,
                'violation' => 'No Call No Show',
            ),
            21 => 
            array (
                'date_served' => '2020-03-03 10:43:00',
                'date_signed' => '2020-03-03 12:43:00',
                'id' => 23,
                'issued_by' => 22,
                'memo' => 'coaching_13715Mar_Wed_2020_12_53_24-23.docx',
                'status' => 1,
                'violation' => 'February Attendance Infractions',
            ),
            22 => 
            array (
                'date_served' => '2020-03-04 11:57:00',
                'date_signed' => '2020-03-04 11:59:00',
                'id' => 24,
                'issued_by' => 22,
                'memo' => 'coaching_13710Mar_Wed_2020_12_59_52-24.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Ifnfractions',
            ),
            23 => 
            array (
                'date_served' => '2020-03-03 10:01:00',
                'date_signed' => '2020-03-03 11:59:00',
                'id' => 25,
                'issued_by' => 22,
                'memo' => 'coaching_13722Mar_Wed_2020_01_03_30-25.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            24 => 
            array (
                'date_served' => '2020-03-04 10:04:00',
                'date_signed' => '2020-03-04 11:56:00',
                'id' => 26,
                'issued_by' => 22,
                'memo' => 'coaching_13716Mar_Wed_2020_01_05_02-26.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            25 => 
            array (
                'date_served' => '2020-03-31 12:08:00',
                'date_signed' => '2020-03-31 12:00:00',
                'id' => 27,
                'issued_by' => 24,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'tardiness',
            ),
            26 => 
            array (
                'date_served' => '2020-03-31 12:00:00',
                'date_signed' => '2020-03-31 12:00:00',
                'id' => 28,
                'issued_by' => 24,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'tardiness',
            ),
            27 => 
            array (
                'date_served' => '2020-04-01 10:50:00',
                'date_signed' => '2020-04-01 10:50:00',
                'id' => 44,
                'issued_by' => 19,
                'memo' => 'coaching_13666Apr_Thu_2020_10_52_24-44.pdf',
                'status' => 1,
                'violation' => 'No Violation - Scorecard for ISA Quarterly Bonues',
            ),
            28 => 
            array (
                'date_served' => '2020-04-02 09:50:00',
                'date_signed' => '2020-04-02 09:50:00',
                'id' => 45,
                'issued_by' => 19,
                'memo' => 'coaching_13675Apr_Fri_2020_09_51_20-45.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction - March',
            ),
            29 => 
            array (
                'date_served' => '2020-02-08 03:35:00',
                'date_signed' => '2020-02-08 03:35:00',
                'id' => 46,
                'issued_by' => 21,
                'memo' => 'coaching_13649Apr_Tue_2020_02_37_07-46.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            30 => 
            array (
                'date_served' => '2020-04-06 08:57:00',
                'date_signed' => '2020-04-06 08:57:00',
                'id' => 47,
                'issued_by' => 21,
                'memo' => 'coaching_13323Apr_Tue_2020_08_57_13-47.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            31 => 
            array (
                'date_served' => '2020-04-06 12:00:00',
                'date_signed' => '2020-04-07 12:00:00',
                'id' => 48,
                'issued_by' => 24,
                'memo' => 'coaching_13728Apr_Tue_2020_11_57_22-48.PNG',
                'status' => 1,
                'violation' => 'not sending of reports regularly',
            ),
            32 => 
            array (
                'date_served' => '2020-04-13 10:00:00',
                'date_signed' => '2020-04-13 10:00:00',
                'id' => 49,
                'issued_by' => 19,
                'memo' => 'coaching_13674Apr_Tue_2020_10_01_00-49.jpeg',
                'status' => 1,
            'violation' => 'No violation - Medical Certificate submitted (for Operation)',
            ),
            33 => 
            array (
                'date_served' => '2020-05-01 12:40:00',
                'date_signed' => '2020-05-01 12:40:00',
                'id' => 50,
                'issued_by' => 21,
                'memo' => 'coaching_13649May_Sat_2020_12_40_42-50.docx',
                'status' => 1,
                'violation' => 'Attendance issues',
            ),
            34 => 
            array (
                'date_served' => '2020-05-04 03:41:00',
                'date_signed' => '2020-05-04 03:42:00',
                'id' => 51,
                'issued_by' => 21,
                'memo' => 'coaching_13643Jul_Sat_2020_05_56_10-51.docx',
                'status' => 1,
                'violation' => 'Attendance issues',
            ),
            35 => 
            array (
                'date_served' => '2020-05-08 10:52:00',
                'date_signed' => '2020-05-08 11:52:00',
                'id' => 52,
                'issued_by' => 22,
                'memo' => 'coaching_13710May_Wed_2020_10_54_59-52.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            36 => 
            array (
                'date_served' => '2020-06-05 01:20:00',
                'date_signed' => '2020-06-05 01:36:00',
                'id' => 53,
                'issued_by' => 22,
                'memo' => 'coaching_13776Jun_Fri_2020_01_46_55-53.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            37 => 
            array (
                'date_served' => '2020-06-05 12:00:00',
                'date_signed' => '2020-06-05 12:15:00',
                'id' => 54,
                'issued_by' => 22,
                'memo' => 'coaching_13821Jun_Fri_2020_02_04_40-54.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            38 => 
            array (
                'date_served' => '2020-06-05 03:30:00',
                'date_signed' => '2020-06-05 03:45:00',
                'id' => 55,
                'issued_by' => 22,
                'memo' => 'coaching_13710Jun_Fri_2020_06_54_00-55.docx',
                'status' => 1,
                'violation' => 'Multiple Attendane Infractions',
            ),
            39 => 
            array (
                'date_served' => '2020-07-03 05:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 56,
                'issued_by' => 21,
                'memo' => 'coaching_13323Jul_Sat_2020_05_54_00-56.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            40 => 
            array (
                'date_served' => '2020-07-03 01:00:00',
                'date_signed' => '2020-07-04 01:00:00',
                'id' => 57,
                'issued_by' => 21,
                'memo' => 'coaching_13323Jul_Sat_2020_01_43_59-57.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            41 => 
            array (
                'date_served' => '2020-07-03 05:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 58,
                'issued_by' => 21,
                'memo' => 'coaching_17385Jul_Sat_2020_05_54_57-58.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            42 => 
            array (
                'date_served' => '2020-07-03 05:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 59,
                'issued_by' => 21,
                'memo' => 'coaching_13643Jul_Sat_2020_05_56_10-59.docx',
                'status' => 1,
                'violation' => NULL,
            ),
            43 => 
            array (
                'date_served' => '2020-07-03 05:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 60,
                'issued_by' => 21,
                'memo' => 'coaching_13649Jul_Sat_2020_05_56_56-60.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            44 => 
            array (
                'date_served' => '2020-07-08 06:00:00',
                'date_signed' => '2020-07-08 06:15:00',
                'id' => 61,
                'issued_by' => 22,
                'memo' => 'coaching_13710Jul_Wed_2020_06_53_34-61.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            45 => 
            array (
                'date_served' => '2020-07-08 03:06:00',
                'date_signed' => '2020-07-08 03:14:00',
                'id' => 62,
                'issued_by' => 22,
                'memo' => 'coaching_14865Jul_Wed_2020_07_53_24-62.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            46 => 
            array (
                'date_served' => '2020-07-08 05:00:00',
                'date_signed' => '2020-07-08 05:07:00',
                'id' => 63,
                'issued_by' => 22,
                'memo' => 'coaching_13716Jul_Wed_2020_08_09_55-63.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            47 => 
            array (
                'date_served' => '2020-07-08 05:32:00',
                'date_signed' => '2020-07-08 05:48:00',
                'id' => 64,
                'issued_by' => 22,
                'memo' => 'coaching_21146Jul_Thu_2020_02_37_38-64.docx',
                'status' => 1,
                'violation' => 'Mulitiple Attendance Infractions',
            ),
            48 => 
            array (
                'date_served' => '2020-07-08 02:40:00',
                'date_signed' => '2020-07-08 02:46:00',
                'id' => 65,
                'issued_by' => 22,
                'memo' => 'coaching_15370Jul_Thu_2020_04_30_17-65.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            49 => 
            array (
                'date_served' => '2020-07-07 10:30:00',
                'date_signed' => '2020-07-07 11:03:00',
                'id' => 66,
                'issued_by' => 22,
                'memo' => 'coaching_13655Jul_Thu_2020_05_39_08-66.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            50 => 
            array (
                'date_served' => '2020-07-14 02:00:00',
                'date_signed' => '2020-07-14 02:00:00',
                'id' => 67,
                'issued_by' => 30,
                'memo' => 'coaching_13663Jul_Tue_2020_02_45_23-67.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            51 => 
            array (
                'date_served' => '2020-07-14 02:00:00',
                'date_signed' => '2020-07-14 02:00:00',
                'id' => 68,
                'issued_by' => 30,
                'memo' => 'coaching_13742Jul_Tue_2020_02_51_45-68.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            52 => 
            array (
                'date_served' => '2020-07-14 02:00:00',
                'date_signed' => '2020-07-14 02:00:00',
                'id' => 69,
                'issued_by' => 30,
                'memo' => 'coaching_13662Jul_Tue_2020_02_52_40-69.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            53 => 
            array (
                'date_served' => '2020-07-14 02:00:00',
                'date_signed' => '2020-07-14 02:00:00',
                'id' => 70,
                'issued_by' => 30,
                'memo' => 'coaching_16582Jul_Tue_2020_02_53_16-70.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            54 => 
            array (
                'date_served' => '2020-07-14 09:00:00',
                'date_signed' => '2020-07-14 02:00:00',
                'id' => 71,
                'issued_by' => 30,
                'memo' => 'coaching_13667Jul_Tue_2020_09_24_56-71.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            55 => 
            array (
                'date_served' => '2020-08-04 08:00:00',
                'date_signed' => '2020-08-04 08:00:00',
                'id' => 72,
                'issued_by' => 30,
                'memo' => 'coaching_18664Aug_Thu_2020_08_23_59-72.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            56 => 
            array (
                'date_served' => '2020-08-04 08:00:00',
                'date_signed' => '2020-08-04 08:00:00',
                'id' => 74,
                'issued_by' => 30,
                'memo' => 'coaching_13681Aug_Thu_2020_08_25_27-74.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            57 => 
            array (
                'date_served' => '2020-08-04 08:00:00',
                'date_signed' => '2020-08-04 08:00:00',
                'id' => 75,
                'issued_by' => 30,
                'memo' => 'coaching_16582Aug_Thu_2020_08_26_20-75.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            58 => 
            array (
                'date_served' => '2020-08-04 08:30:00',
                'date_signed' => '2020-08-04 08:30:00',
                'id' => 76,
                'issued_by' => 30,
                'memo' => 'coaching_13742Aug_Thu_2020_08_30_31-76.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            59 => 
            array (
                'date_served' => '2020-08-04 08:00:00',
                'date_signed' => '2020-08-04 08:00:00',
                'id' => 77,
                'issued_by' => 30,
                'memo' => 'coaching_13687Aug_Thu_2020_08_31_27-77.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            60 => 
            array (
                'date_served' => '2020-08-04 08:00:00',
                'date_signed' => '2020-08-04 08:00:00',
                'id' => 78,
                'issued_by' => 30,
                'memo' => 'coaching_13700Aug_Thu_2020_08_32_24-78.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            61 => 
            array (
                'date_served' => '2020-08-04 08:00:00',
                'date_signed' => '2020-08-04 08:00:00',
                'id' => 79,
                'issued_by' => 30,
                'memo' => 'coaching_16579Aug_Thu_2020_08_33_49-79.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            62 => 
            array (
                'date_served' => '2020-08-11 01:15:00',
                'date_signed' => '2020-08-11 01:30:00',
                'id' => 80,
                'issued_by' => 22,
                'memo' => 'coaching_13709Aug_Tue_2020_02_09_18-80.docx',
                'status' => 1,
                'violation' => 'July 2020 Multiple Attendance Infractions',
            ),
            63 => 
            array (
                'date_served' => '2020-08-05 08:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 81,
                'issued_by' => 21,
                'memo' => 'coaching_16551Aug_Fri_2020_08_41_54-81.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            64 => 
            array (
                'date_served' => '2020-08-05 08:45:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 82,
                'issued_by' => 21,
                'memo' => 'coaching_17541Aug_Fri_2020_08_46_39-82.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            65 => 
            array (
                'date_served' => '2020-08-05 08:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 83,
                'issued_by' => 21,
                'memo' => 'coaching_15541Aug_Fri_2020_08_47_27-83.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            66 => 
            array (
                'date_served' => '2020-08-05 08:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 84,
                'issued_by' => 21,
                'memo' => 'coaching_13643Aug_Fri_2020_08_48_29-84.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            67 => 
            array (
                'date_served' => '2020-08-05 08:00:00',
                'date_signed' => '2020-08-05 08:00:00',
                'id' => 85,
                'issued_by' => 21,
                'memo' => 'coaching_17155Aug_Fri_2020_08_49_28-85.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            68 => 
            array (
                'date_served' => '2020-08-05 08:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 86,
                'issued_by' => 21,
                'memo' => 'coaching_13323Aug_Fri_2020_08_50_16-86.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            69 => 
            array (
                'date_served' => '2020-08-05 08:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 87,
                'issued_by' => 21,
                'memo' => 'coaching_13641Aug_Fri_2020_08_51_00-87.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            70 => 
            array (
                'date_served' => '2020-08-05 08:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 88,
                'issued_by' => 21,
                'memo' => 'coaching_18161Aug_Fri_2020_08_53_52-88.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            71 => 
            array (
                'date_served' => '2020-08-19 12:15:00',
                'date_signed' => '2020-08-19 12:38:00',
                'id' => 89,
                'issued_by' => 22,
                'memo' => 'coaching_13703Aug_Wed_2020_12_44_00-89.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            72 => 
            array (
                'date_served' => '2020-08-15 06:49:00',
                'date_signed' => '2020-08-15 07:04:00',
                'id' => 90,
                'issued_by' => 22,
                'memo' => 'coaching_13710Aug_Wed_2020_02_06_31-90.docx',
                'status' => 1,
                'violation' => 'Mulitiple Attendance Infractions',
            ),
            73 => 
            array (
                'date_served' => '2020-09-15 01:00:00',
                'date_signed' => '2020-09-15 01:00:00',
                'id' => 91,
                'issued_by' => 22,
                'memo' => 'coaching_13714Sep_Wed_2020_12_02_03-91.docx',
                'status' => 1,
                'violation' => 'Multiple Attendanced Infractions',
            ),
            74 => 
            array (
                'date_served' => '2020-09-15 01:22:00',
                'date_signed' => '2020-09-15 01:31:00',
                'id' => 92,
                'issued_by' => 22,
                'memo' => 'coaching_15370Sep_Wed_2020_12_09_26-92.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            75 => 
            array (
                'date_served' => '2020-09-16 11:58:00',
                'date_signed' => '2020-09-17 12:10:00',
                'id' => 93,
                'issued_by' => 22,
                'memo' => 'coaching_19900Sep_Thu_2020_12_04_22-93.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            76 => 
            array (
                'date_served' => '2020-09-18 01:12:00',
                'date_signed' => '2020-09-18 01:19:00',
                'id' => 94,
                'issued_by' => 22,
                'memo' => 'coaching_13710Sep_Fri_2020_01_40_09-94.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            77 => 
            array (
                'date_served' => '2020-10-08 05:41:00',
                'date_signed' => '2020-10-08 05:45:00',
                'id' => 95,
                'issued_by' => 22,
                'memo' => 'coaching_18837Oct_Thu_2020_07_39_30-95.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            78 => 
            array (
                'date_served' => '2020-10-07 11:25:00',
                'date_signed' => '2020-10-08 12:45:00',
                'id' => 96,
                'issued_by' => 22,
                'memo' => 'coaching_19900Oct_Thu_2020_07_41_34-96.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            79 => 
            array (
                'date_served' => '2020-10-08 04:06:00',
                'date_signed' => '2020-10-08 04:12:00',
                'id' => 97,
                'issued_by' => 22,
                'memo' => 'coaching_22952Oct_Thu_2020_07_44_31-97.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            80 => 
            array (
                'date_served' => '2020-10-08 09:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 98,
                'issued_by' => 21,
                'memo' => 'coaching_17149Oct_Thu_2020_09_35_08-98.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            81 => 
            array (
                'date_served' => '2020-10-08 09:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 99,
                'issued_by' => 21,
                'memo' => 'coaching_23132Oct_Thu_2020_09_39_48-99.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            82 => 
            array (
                'date_served' => '2020-10-08 09:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 100,
                'issued_by' => 21,
                'memo' => 'coaching_16551Oct_Thu_2020_09_41_17-100.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            83 => 
            array (
                'date_served' => '2020-10-08 09:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 101,
                'issued_by' => 21,
                'memo' => 'coaching_13649Oct_Thu_2020_09_43_22-101.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            84 => 
            array (
                'date_served' => '2020-10-08 09:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 102,
                'issued_by' => 21,
                'memo' => 'coaching_13660Oct_Thu_2020_09_45_11-102.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            85 => 
            array (
                'date_served' => '2020-10-08 09:45:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 103,
                'issued_by' => 21,
                'memo' => 'coaching_13323Oct_Thu_2020_09_45_53-103.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            86 => 
            array (
                'date_served' => '2020-10-08 11:00:00',
                'date_signed' => '2020-10-08 11:00:00',
                'id' => 104,
                'issued_by' => 30,
                'memo' => 'coaching_16451Oct_Thu_2020_11_02_45-104.docx',
                'status' => 1,
                'violation' => 'Verbal Warning Acknowledgement',
            ),
            87 => 
            array (
                'date_served' => '2020-09-04 11:00:00',
                'date_signed' => '2020-09-04 11:00:00',
                'id' => 105,
                'issued_by' => 30,
                'memo' => 'coaching_13665Oct_Thu_2020_11_07_08-105.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            88 => 
            array (
                'date_served' => '2020-09-04 11:00:00',
                'date_signed' => '2020-09-04 11:00:00',
                'id' => 106,
                'issued_by' => 30,
                'memo' => 'coaching_17967Oct_Thu_2020_11_07_59-106.docx',
                'status' => 1,
                'violation' => NULL,
            ),
            89 => 
            array (
                'date_served' => '2020-09-04 11:00:00',
                'date_signed' => '2020-09-04 11:00:00',
                'id' => 107,
                'issued_by' => 30,
                'memo' => 'coaching_13687Oct_Thu_2020_11_08_48-107.docx',
                'status' => 1,
                'violation' => NULL,
            ),
            90 => 
            array (
                'date_served' => '2020-09-04 11:00:00',
                'date_signed' => '2020-09-04 11:00:00',
                'id' => 108,
                'issued_by' => 30,
                'memo' => 'coaching_16451Oct_Thu_2020_11_09_50-108.docx',
                'status' => 1,
                'violation' => 'System Manipulation',
            ),
            91 => 
            array (
                'date_served' => '2020-09-04 11:00:00',
                'date_signed' => '2020-10-04 11:00:00',
                'id' => 109,
                'issued_by' => 30,
                'memo' => 'coaching_18270Oct_Thu_2020_11_10_34-109.docx',
                'status' => 1,
                'violation' => NULL,
            ),
            92 => 
            array (
                'date_served' => '2020-09-04 11:00:00',
                'date_signed' => '2020-09-04 11:00:00',
                'id' => 110,
                'issued_by' => 30,
                'memo' => 'coaching_16582Oct_Thu_2020_11_11_13-110.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            93 => 
            array (
                'date_served' => '2020-10-07 11:00:00',
                'date_signed' => '2020-10-07 11:00:00',
                'id' => 111,
                'issued_by' => 30,
                'memo' => 'coaching_16579Oct_Thu_2020_11_12_36-111.docx',
                'status' => 1,
                'violation' => NULL,
            ),
            94 => 
            array (
                'date_served' => '2020-10-07 11:00:00',
                'date_signed' => '2020-10-07 11:00:00',
                'id' => 112,
                'issued_by' => 30,
                'memo' => 'coaching_19856Oct_Thu_2020_11_13_44-112.docx',
                'status' => 1,
                'violation' => NULL,
            ),
            95 => 
            array (
                'date_served' => '2020-10-07 11:00:00',
                'date_signed' => '2020-10-07 11:00:00',
                'id' => 113,
                'issued_by' => 30,
                'memo' => 'coaching_13665Oct_Thu_2020_11_14_18-113.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            96 => 
            array (
                'date_served' => '2020-10-07 11:00:00',
                'date_signed' => '2020-10-07 11:00:00',
                'id' => 114,
                'issued_by' => 30,
                'memo' => 'coaching_13667Oct_Thu_2020_11_14_58-114.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            97 => 
            array (
                'date_served' => '2020-10-07 11:15:00',
                'date_signed' => '2020-10-07 11:15:00',
                'id' => 115,
                'issued_by' => 30,
                'memo' => 'coaching_16451Oct_Thu_2020_11_16_16-115.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            98 => 
            array (
                'date_served' => '2020-10-07 11:00:00',
                'date_signed' => '2020-10-07 11:00:00',
                'id' => 116,
                'issued_by' => 30,
                'memo' => 'coaching_18664Oct_Thu_2020_11_16_55-116.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            99 => 
            array (
                'date_served' => '2020-10-08 01:00:00',
                'date_signed' => '2020-10-08 01:00:00',
                'id' => 117,
                'issued_by' => 31,
                'memo' => 'coaching_20978Oct_Fri_2020_01_27_00-117.docx',
                'status' => 1,
                'violation' => 'September Multiple Attendance Infractions',
            ),
            100 => 
            array (
                'date_served' => '2020-10-08 01:00:00',
                'date_signed' => '2020-10-08 01:00:00',
                'id' => 118,
                'issued_by' => 31,
                'memo' => 'coaching_22468Oct_Fri_2020_02_09_25-118.docx',
                'status' => 1,
                'violation' => 'September Multiple Attendance Infractions',
            ),
            101 => 
            array (
                'date_served' => '2020-10-08 01:00:00',
                'date_signed' => '2020-10-08 01:00:00',
                'id' => 119,
                'issued_by' => 31,
                'memo' => 'coaching_19359Oct_Fri_2020_02_10_18-119.docx',
                'status' => 1,
                'violation' => 'September Multiple Attendance Infractions',
            ),
            102 => 
            array (
                'date_served' => '2020-10-08 07:00:00',
                'date_signed' => '2020-10-08 01:00:00',
                'id' => 120,
                'issued_by' => 31,
                'memo' => 'coaching_23132Oct_Fri_2020_07_36_29-120.docx',
                'status' => 1,
                'violation' => 'September Multiple Attendance Infractions',
            ),
            103 => 
            array (
                'date_served' => '2020-10-08 01:00:00',
                'date_signed' => '2020-10-08 01:00:00',
                'id' => 121,
                'issued_by' => 31,
                'memo' => 'coaching_13731Oct_Fri_2020_07_39_44-121.docx',
                'status' => 1,
                'violation' => 'September Multiple Attendance Infractions',
            ),
            104 => 
            array (
                'date_served' => '2020-10-08 01:00:00',
                'date_signed' => '2020-10-08 01:00:00',
                'id' => 122,
                'issued_by' => 31,
                'memo' => 'coaching_20156Oct_Fri_2020_07_40_50-122.docx',
                'status' => 1,
                'violation' => 'September Multiple Attendance Infractions',
            ),
            105 => 
            array (
                'date_served' => '2020-10-10 01:57:00',
                'date_signed' => '2020-10-10 02:05:00',
                'id' => 124,
                'issued_by' => 22,
                'memo' => 'coaching_13710Oct_Sat_2020_06_34_55-124.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            106 => 
            array (
                'date_served' => '1970-01-01 08:00:00',
                'date_signed' => '2020-11-06 01:24:00',
                'id' => 125,
                'issued_by' => 22,
                'memo' => 'coaching_14865Nov_Tue_2020_12_56_46-125.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance infractions - October 2020',
            ),
            107 => 
            array (
                'date_served' => '2020-11-06 03:58:00',
                'date_signed' => '2020-11-06 04:05:00',
                'id' => 126,
                'issued_by' => 22,
                'memo' => 'coaching_18636Nov_Sat_2020_01_20_06-126.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - Oct 2020',
            ),
            108 => 
            array (
                'date_served' => '2020-11-06 03:19:00',
                'date_signed' => '2020-11-06 03:25:00',
                'id' => 127,
                'issued_by' => 22,
                'memo' => 'coaching_16826Nov_Sat_2020_01_28_45-127.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - Oct 2020',
            ),
            109 => 
            array (
                'date_served' => '2020-11-06 02:27:00',
                'date_signed' => '2020-11-06 03:06:00',
                'id' => 128,
                'issued_by' => 22,
                'memo' => 'coaching_22952Nov_Sat_2020_01_50_21-128.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - October 2020',
            ),
            110 => 
            array (
                'date_served' => '2020-11-07 06:20:00',
                'date_signed' => '2020-11-07 06:26:00',
                'id' => 129,
                'issued_by' => 22,
                'memo' => 'coaching_19540Nov_Sat_2020_07_05_22-129.docx',
                'status' => 1,
                'violation' => 'Mulitlple Attendance Infractions - October 2020',
            ),
            111 => 
            array (
                'date_served' => '2020-11-06 05:58:00',
                'date_signed' => '2020-11-06 06:05:00',
                'id' => 130,
                'issued_by' => 22,
                'memo' => 'coaching_18960Nov_Tue_2020_12_55_44-130.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - October 2020',
            ),
            112 => 
            array (
                'date_served' => '2020-11-07 07:00:00',
                'date_signed' => '2020-11-07 07:07:00',
                'id' => 131,
                'issued_by' => 22,
                'memo' => 'coaching_18326Nov_Sat_2020_07_30_37-131.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - OCt 2020',
            ),
            113 => 
            array (
                'date_served' => '2020-11-07 07:10:00',
                'date_signed' => '2020-11-07 07:15:00',
                'id' => 132,
                'issued_by' => 22,
                'memo' => 'coaching_13710Nov_Sat_2020_07_32_20-132.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - Oct 2020',
            ),
            114 => 
            array (
                'date_served' => '2020-11-10 01:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 133,
                'issued_by' => 21,
                'memo' => 'coaching_17149Nov_Tue_2020_01_03_17-133.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            115 => 
            array (
                'date_served' => '2020-11-10 01:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 134,
                'issued_by' => 21,
                'memo' => 'coaching_13641Nov_Tue_2020_01_04_56-134.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            116 => 
            array (
                'date_served' => '2020-11-10 01:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 135,
                'issued_by' => 21,
                'memo' => 'coaching_13696Nov_Tue_2020_01_05_54-135.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            117 => 
            array (
                'date_served' => '2020-11-10 01:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 136,
                'issued_by' => 21,
                'memo' => 'coaching_17441Nov_Tue_2020_01_09_19-136.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            118 => 
            array (
                'date_served' => '2020-11-10 02:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 137,
                'issued_by' => 21,
                'memo' => 'coaching_13649Nov_Tue_2020_02_09_57-137.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            119 => 
            array (
                'date_served' => '2020-11-10 02:00:00',
                'date_signed' => '2020-11-10 02:00:00',
                'id' => 138,
                'issued_by' => 21,
                'memo' => 'coaching_24287Nov_Tue_2020_02_11_09-138.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            120 => 
            array (
                'date_served' => '2020-11-06 02:00:00',
                'date_signed' => '2020-11-06 02:00:00',
                'id' => 139,
                'issued_by' => 30,
                'memo' => 'coaching_15626Nov_Tue_2020_02_26_38-139.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            121 => 
            array (
                'date_served' => '2020-11-06 02:00:00',
                'date_signed' => '2020-11-06 02:00:00',
                'id' => 140,
                'issued_by' => 30,
                'memo' => 'coaching_13665Nov_Tue_2020_02_28_00-140.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            122 => 
            array (
                'date_served' => '2020-11-06 02:00:00',
                'date_signed' => '2020-11-06 02:00:00',
                'id' => 141,
                'issued_by' => 30,
                'memo' => 'coaching_13667Nov_Tue_2020_02_28_44-141.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            123 => 
            array (
                'date_served' => '2020-11-06 02:00:00',
                'date_signed' => '2020-11-06 02:00:00',
                'id' => 142,
                'issued_by' => 30,
                'memo' => 'coaching_19012Nov_Tue_2020_02_29_29-142.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            124 => 
            array (
                'date_served' => '2020-11-06 02:30:00',
                'date_signed' => '2020-11-06 02:30:00',
                'id' => 143,
                'issued_by' => 30,
                'memo' => 'coaching_18664Nov_Tue_2020_02_30_28-143.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            125 => 
            array (
                'date_served' => '2020-11-06 02:00:00',
                'date_signed' => '2020-11-06 02:00:00',
                'id' => 144,
                'issued_by' => 30,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Multiple Attendace Infraction',
            ),
            126 => 
            array (
                'date_served' => '2020-11-06 02:00:00',
                'date_signed' => '2020-11-06 02:00:00',
                'id' => 145,
                'issued_by' => 30,
                'memo' => 'coaching_30678Nov_Tue_2020_02_44_07-145.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            127 => 
            array (
                'date_served' => '2020-12-07 10:00:00',
                'date_signed' => '2020-12-08 10:00:00',
                'id' => 146,
                'issued_by' => 29,
                'memo' => 'coaching_18886Dec_Tue_2020_10_49_56-146.pdf',
                'status' => 1,
                'violation' => 'Performance Concern',
            ),
            128 => 
            array (
                'date_served' => '2020-12-07 10:00:00',
                'date_signed' => '2020-12-07 10:00:00',
                'id' => 147,
                'issued_by' => 29,
                'memo' => 'coaching_18270Dec_Tue_2020_10_53_56-147.pdf',
                'status' => 1,
                'violation' => 'Attendance Infractions',
            ),
            129 => 
            array (
                'date_served' => '2020-12-07 10:00:00',
                'date_signed' => '2020-12-08 10:00:00',
                'id' => 148,
                'issued_by' => 29,
                'memo' => 'coaching_18405Dec_Tue_2020_11_15_19-148.pdf',
                'status' => 1,
                'violation' => 'Attendance Infractions',
            ),
            130 => 
            array (
                'date_served' => '2020-12-16 02:40:00',
                'date_signed' => '2020-12-16 02:50:00',
                'id' => 152,
                'issued_by' => 22,
                'memo' => 'coaching_16948Dec_Tue_2020_03_50_50-152.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - November',
            ),
            131 => 
            array (
                'date_served' => '2020-12-16 02:30:00',
                'date_signed' => '2020-12-16 02:38:00',
                'id' => 153,
                'issued_by' => 22,
                'memo' => 'coaching_23105Dec_Tue_2020_03_52_24-153.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - November',
            ),
            132 => 
            array (
                'date_served' => '2020-12-16 02:00:00',
                'date_signed' => '2020-12-16 02:15:00',
                'id' => 154,
                'issued_by' => 22,
                'memo' => 'coaching_22952Dec_Tue_2020_03_54_18-154.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - November',
            ),
            133 => 
            array (
                'date_served' => '2020-12-16 03:00:00',
                'date_signed' => '2020-12-16 03:15:00',
                'id' => 155,
                'issued_by' => 22,
                'memo' => 'coaching_18960Dec_Tue_2020_03_55_47-155.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - November',
            ),
            134 => 
            array (
                'date_served' => '2020-12-16 03:15:00',
                'date_signed' => '2020-12-16 03:25:00',
                'id' => 156,
                'issued_by' => 22,
                'memo' => 'coaching_13710Dec_Tue_2020_04_00_09-156.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - November',
            ),
            135 => 
            array (
                'date_served' => '2020-12-16 03:25:00',
                'date_signed' => '2020-12-16 03:35:00',
                'id' => 157,
                'issued_by' => 22,
                'memo' => 'coaching_18334Dec_Tue_2020_04_04_30-157.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Ifnfractions - November 2020',
            ),
            136 => 
            array (
                'date_served' => '2021-01-14 06:04:00',
                'date_signed' => '2021-01-14 06:23:00',
                'id' => 158,
                'issued_by' => 22,
                'memo' => 'coaching_22700Jan_Thu_2021_06_32_56-158.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions December 2020',
            ),
            137 => 
            array (
                'date_served' => '2021-01-08 02:00:00',
                'date_signed' => '2021-01-08 02:20:00',
                'id' => 159,
                'issued_by' => 22,
                'memo' => 'coaching_13710Jan_Sat_2021_02_25_58-159.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - December 2020',
            ),
            138 => 
            array (
                'date_served' => '2021-01-09 02:00:00',
                'date_signed' => '2021-01-09 02:13:00',
                'id' => 160,
                'issued_by' => 22,
                'memo' => 'coaching_23743Jan_Sat_2021_02_36_40-160.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions - December 2020',
            ),
            139 => 
            array (
                'date_served' => '2021-01-22 05:00:00',
                'date_signed' => '2021-01-22 06:00:00',
                'id' => 161,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Absence with Notification/Coaching with Verbal Warning',
            ),
            140 => 
            array (
                'date_served' => '2021-01-26 04:00:00',
                'date_signed' => '2021-01-26 04:00:00',
                'id' => 162,
                'issued_by' => 30,
                'memo' => 'coaching_13667Jan_Fri_2021_04_36_28-162.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            141 => 
            array (
                'date_served' => '2021-01-28 04:00:00',
                'date_signed' => '2021-01-28 04:00:00',
                'id' => 163,
                'issued_by' => 30,
                'memo' => 'coaching_13669Jan_Fri_2021_04_39_25-163.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            142 => 
            array (
                'date_served' => '2021-01-26 04:00:00',
                'date_signed' => '2021-01-26 04:00:00',
                'id' => 164,
                'issued_by' => 30,
                'memo' => 'coaching_18984Jan_Fri_2021_04_40_36-164.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            143 => 
            array (
                'date_served' => '2021-01-28 04:00:00',
                'date_signed' => '2021-01-28 04:00:00',
                'id' => 165,
                'issued_by' => 30,
                'memo' => 'coaching_24387Jan_Fri_2021_04_41_37-165.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            144 => 
            array (
                'date_served' => '2021-01-26 04:00:00',
                'date_signed' => '2021-01-26 04:00:00',
                'id' => 166,
                'issued_by' => 30,
                'memo' => 'coaching_18260Jan_Fri_2021_04_42_54-166.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            145 => 
            array (
                'date_served' => '2021-01-28 04:00:00',
                'date_signed' => '2021-01-28 04:00:00',
                'id' => 167,
                'issued_by' => 30,
                'memo' => 'coaching_29329Jan_Fri_2021_04_43_38-167.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            146 => 
            array (
                'date_served' => '2021-01-26 04:45:00',
                'date_signed' => '2021-01-26 04:45:00',
                'id' => 168,
                'issued_by' => 19,
                'memo' => 'coaching_25740Jan_Fri_2021_04_46_05-168.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            147 => 
            array (
                'date_served' => '2021-01-27 04:45:00',
                'date_signed' => '2021-01-27 04:45:00',
                'id' => 169,
                'issued_by' => 30,
                'memo' => 'coaching_25740Jan_Fri_2021_04_46_05-169.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            148 => 
            array (
                'date_served' => '2021-01-27 05:00:00',
                'date_signed' => '2021-01-27 05:00:00',
                'id' => 170,
                'issued_by' => 30,
                'memo' => 'coaching_13700Jan_Fri_2021_05_04_33-170.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            149 => 
            array (
                'date_served' => '2021-01-28 06:00:00',
                'date_signed' => '2021-01-28 06:00:00',
                'id' => 171,
                'issued_by' => 30,
                'memo' => 'coaching_13700Jan_Fri_2021_06_12_26-171.docx',
                'status' => 1,
                'violation' => 'System Manipulation',
            ),
            150 => 
            array (
                'date_served' => '2021-01-29 10:00:00',
                'date_signed' => '2021-01-29 10:00:00',
                'id' => 172,
                'issued_by' => 30,
                'memo' => 'coaching_18184Jan_Fri_2021_10_10_38-172.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            151 => 
            array (
                'date_served' => '2021-01-29 07:00:00',
                'date_signed' => '2021-01-29 07:00:00',
                'id' => 173,
                'issued_by' => 30,
                'memo' => 'coaching_13700Jan_Sat_2021_07_05_52-173.pdf',
                'status' => 1,
                'violation' => 'Waiver',
            ),
            152 => 
            array (
                'date_served' => '2021-01-29 07:00:00',
                'date_signed' => '2021-01-29 07:00:00',
                'id' => 174,
                'issued_by' => 30,
                'memo' => 'coaching_13644Jan_Sat_2021_07_06_52-174.pdf',
                'status' => 1,
                'violation' => 'Waiver',
            ),
            153 => 
            array (
                'date_served' => '2021-01-29 07:00:00',
                'date_signed' => '2021-01-29 07:00:00',
                'id' => 175,
                'issued_by' => 30,
                'memo' => 'coaching_33876Jan_Sat_2021_07_07_38-175.pdf',
                'status' => 1,
                'violation' => 'Waiver',
            ),
            154 => 
            array (
                'date_served' => '2021-01-29 07:00:00',
                'date_signed' => '2021-01-29 07:00:00',
                'id' => 176,
                'issued_by' => 30,
                'memo' => 'coaching_13667Jan_Sat_2021_07_08_37-176.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            155 => 
            array (
                'date_served' => '2021-01-29 07:00:00',
                'date_signed' => '2021-01-29 07:00:00',
                'id' => 177,
                'issued_by' => 30,
                'memo' => 'coaching_15133Jan_Sat_2021_07_09_21-177.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            156 => 
            array (
                'date_served' => '2021-01-29 07:15:00',
                'date_signed' => '2021-01-29 07:15:00',
                'id' => 178,
                'issued_by' => 30,
                'memo' => 'coaching_25740Jan_Sat_2021_07_15_32-178.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            157 => 
            array (
                'date_served' => '2021-02-01 05:00:00',
                'date_signed' => '2021-02-01 05:00:00',
                'id' => 179,
                'issued_by' => 30,
                'memo' => 'coaching_19012Feb_Tue_2021_05_07_20-179.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            158 => 
            array (
                'date_served' => '2021-02-01 05:00:00',
                'date_signed' => '2021-02-01 05:00:00',
                'id' => 180,
                'issued_by' => 30,
                'memo' => 'coaching_13667Feb_Tue_2021_05_07_59-180.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            159 => 
            array (
                'date_served' => '2021-01-14 05:00:00',
                'date_signed' => '2021-02-14 05:00:00',
                'id' => 181,
                'issued_by' => 30,
                'memo' => 'coaching_26599Feb_Tue_2021_05_09_31-181.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            160 => 
            array (
                'date_served' => '2021-01-14 05:00:00',
                'date_signed' => '2021-02-14 05:00:00',
                'id' => 182,
                'issued_by' => 30,
                'memo' => 'coaching_26599Feb_Tue_2021_05_09_34-182.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            161 => 
            array (
                'date_served' => '2021-01-14 05:00:00',
                'date_signed' => '2021-01-14 05:00:00',
                'id' => 183,
                'issued_by' => 30,
                'memo' => 'coaching_19012Feb_Tue_2021_05_10_50-183.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            162 => 
            array (
                'date_served' => '2021-01-14 05:00:00',
                'date_signed' => '2021-01-14 05:00:00',
                'id' => 184,
                'issued_by' => 30,
                'memo' => 'coaching_13742Feb_Tue_2021_05_11_50-184.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            163 => 
            array (
                'date_served' => '2021-01-14 05:00:00',
                'date_signed' => '2021-01-14 05:00:00',
                'id' => 185,
                'issued_by' => 30,
                'memo' => 'coaching_13665Feb_Tue_2021_05_12_44-185.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            164 => 
            array (
                'date_served' => '2021-01-14 05:00:00',
                'date_signed' => '2021-01-14 05:00:00',
                'id' => 186,
                'issued_by' => 30,
                'memo' => 'coaching_13700Feb_Tue_2021_05_13_41-186.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            165 => 
            array (
                'date_served' => '2021-01-14 05:00:00',
                'date_signed' => '2021-01-14 05:00:00',
                'id' => 187,
                'issued_by' => 30,
                'memo' => 'coaching_15626Feb_Tue_2021_05_14_27-187.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            166 => 
            array (
                'date_served' => '2020-12-09 05:00:00',
                'date_signed' => '2020-12-09 05:00:00',
                'id' => 188,
                'issued_by' => 30,
                'memo' => 'coaching_13662Feb_Tue_2021_05_16_50-188.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            167 => 
            array (
                'date_served' => '2020-12-09 05:00:00',
                'date_signed' => '2020-12-09 05:00:00',
                'id' => 189,
                'issued_by' => 30,
                'memo' => 'coaching_15626Feb_Tue_2021_05_17_36-189.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            168 => 
            array (
                'date_served' => '2020-12-09 05:00:00',
                'date_signed' => '2020-12-09 05:00:00',
                'id' => 190,
                'issued_by' => 30,
                'memo' => 'coaching_18184Feb_Tue_2021_05_18_14-190.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            169 => 
            array (
                'date_served' => '2020-12-09 05:00:00',
                'date_signed' => '2020-12-09 05:00:00',
                'id' => 191,
                'issued_by' => 30,
                'memo' => 'coaching_15133Feb_Tue_2021_05_19_00-191.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            170 => 
            array (
                'date_served' => '2020-12-09 05:00:00',
                'date_signed' => '2020-12-09 05:00:00',
                'id' => 192,
                'issued_by' => 30,
                'memo' => 'coaching_13700Feb_Tue_2021_05_19_45-192.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            171 => 
            array (
                'date_served' => '2020-12-09 09:00:00',
                'date_signed' => '2020-12-09 05:00:00',
                'id' => 193,
                'issued_by' => 30,
                'memo' => 'coaching_19012Feb_Tue_2021_05_20_29-193.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            172 => 
            array (
                'date_served' => '2020-12-09 05:00:00',
                'date_signed' => '2020-12-09 05:00:00',
                'id' => 194,
                'issued_by' => 30,
                'memo' => 'coaching_30678Feb_Tue_2021_05_21_21-194.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            173 => 
            array (
                'date_served' => '2020-12-09 05:00:00',
                'date_signed' => '2020-12-09 05:00:00',
                'id' => 195,
                'issued_by' => 30,
                'memo' => 'coaching_16582Feb_Tue_2021_05_22_22-195.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance Infraction',
            ),
            174 => 
            array (
                'date_served' => '2021-02-01 05:00:00',
                'date_signed' => '2021-02-01 05:00:00',
                'id' => 196,
                'issued_by' => 30,
                'memo' => 'coaching_30678Feb_Tue_2021_05_36_34-196.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            175 => 
            array (
                'date_served' => '2021-02-01 09:00:00',
                'date_signed' => '2021-02-01 09:00:00',
                'id' => 197,
                'issued_by' => 30,
                'memo' => 'coaching_18664Feb_Tue_2021_09_43_21-197.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            176 => 
            array (
                'date_served' => '2021-02-05 12:00:00',
                'date_signed' => '2021-02-05 12:00:00',
                'id' => 198,
                'issued_by' => 30,
                'memo' => 'coaching_18184Feb_Sat_2021_12_17_16-198.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            177 => 
            array (
                'date_served' => '2021-02-05 12:00:00',
                'date_signed' => '2021-02-06 12:00:00',
                'id' => 199,
                'issued_by' => 21,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Attendance issues',
            ),
            178 => 
            array (
                'date_served' => '2021-02-04 11:30:00',
                'date_signed' => '2021-02-04 02:00:00',
                'id' => 200,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Tardiness - Coaching and Verbal Warning',
            ),
            179 => 
            array (
                'date_served' => '2021-02-09 12:00:00',
                'date_signed' => '2021-02-09 12:00:00',
                'id' => 201,
                'issued_by' => 35,
                'memo' => 'coaching_13736Feb_Wed_2021_12_26_42-201.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            180 => 
            array (
                'date_served' => '2021-01-27 12:00:00',
                'date_signed' => '2021-01-27 12:00:00',
                'id' => 202,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Tardiness - Coaching and Verbal Warning',
            ),
            181 => 
            array (
                'date_served' => '2021-02-04 01:00:00',
                'date_signed' => '2021-02-04 01:00:00',
                'id' => 203,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Overbreak',
            ),
            182 => 
            array (
                'date_served' => '2021-02-09 11:34:00',
                'date_signed' => '2021-02-09 11:45:00',
                'id' => 204,
                'issued_by' => 19,
                'memo' => 'coaching_13716Feb_Wed_2021_02_57_51-204.pdf',
                'status' => 1,
                'violation' => 'Coaching log on IDLE',
            ),
            183 => 
            array (
                'date_served' => '2021-02-05 08:00:00',
                'date_signed' => '2021-02-05 08:00:00',
                'id' => 205,
                'issued_by' => 19,
                'memo' => 'coaching_13714Feb_Wed_2021_08_48_34-205.pdf',
                'status' => 1,
                'violation' => 'Attendanced Infraction',
            ),
            184 => 
            array (
                'date_served' => '2021-02-04 08:00:00',
                'date_signed' => '2021-02-04 08:00:00',
                'id' => 206,
                'issued_by' => 30,
                'memo' => 'coaching_13714Feb_Wed_2021_08_48_34-206.pdf',
                'status' => 1,
                'violation' => 'Attendanced Infraction',
            ),
            185 => 
            array (
                'date_served' => '2021-02-04 08:00:00',
                'date_signed' => '2021-02-04 08:00:00',
                'id' => 207,
                'issued_by' => 30,
                'memo' => 'coaching_13665Feb_Wed_2021_08_49_47-207.jpg',
                'status' => 1,
                'violation' => 'Attendanced Infraction',
            ),
            186 => 
            array (
                'date_served' => '2021-02-04 08:00:00',
                'date_signed' => '2021-02-05 08:00:00',
                'id' => 208,
                'issued_by' => 30,
                'memo' => 'coaching_36992Feb_Wed_2021_08_52_35-208.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            187 => 
            array (
                'date_served' => '2021-02-04 08:00:00',
                'date_signed' => '2021-02-04 08:00:00',
                'id' => 209,
                'issued_by' => 30,
                'memo' => 'coaching_36993Feb_Wed_2021_08_57_34-209.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            188 => 
            array (
                'date_served' => '2021-02-04 08:00:00',
                'date_signed' => '2021-02-04 08:00:00',
                'id' => 210,
                'issued_by' => 30,
                'memo' => 'coaching_36999Feb_Wed_2021_08_59_52-210.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            189 => 
            array (
                'date_served' => '2021-02-05 09:00:00',
                'date_signed' => '2021-02-05 09:00:00',
                'id' => 211,
                'issued_by' => 30,
                'memo' => 'coaching_37000Feb_Wed_2021_09_01_45-211.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            190 => 
            array (
                'date_served' => '2021-02-04 09:00:00',
                'date_signed' => '2021-02-04 09:00:00',
                'id' => 212,
                'issued_by' => 30,
                'memo' => 'coaching_30678Feb_Wed_2021_09_02_37-212.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            191 => 
            array (
                'date_served' => '2021-02-11 03:39:00',
                'date_signed' => '2021-02-11 04:18:00',
                'id' => 213,
                'issued_by' => 22,
                'memo' => 'coaching_37073Feb_Thu_2021_07_25_28-213.pdf',
                'status' => 1,
                'violation' => 'Signed Coaching Log_IDLE',
            ),
            192 => 
            array (
                'date_served' => '2021-02-11 11:00:00',
                'date_signed' => '2021-02-11 11:00:00',
                'id' => 214,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
            'violation' => 'Idle ( Written Warning)',
            ),
            193 => 
            array (
                'date_served' => '2021-02-11 11:00:00',
                'date_signed' => '2021-02-11 11:00:00',
                'id' => 215,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Idle',
            ),
            194 => 
            array (
                'date_served' => '2021-02-10 02:00:00',
                'date_signed' => '2021-02-10 02:00:00',
                'id' => 216,
                'issued_by' => 30,
                'memo' => 'coaching_37190Feb_Sat_2021_02_48_58-216.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            195 => 
            array (
                'date_served' => '2021-02-12 02:00:00',
                'date_signed' => '2021-02-12 02:00:00',
                'id' => 217,
                'issued_by' => 30,
                'memo' => 'coaching_36992Feb_Sat_2021_02_50_01-217.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            196 => 
            array (
                'date_served' => '2021-02-12 02:00:00',
                'date_signed' => '2021-02-12 02:00:00',
                'id' => 218,
                'issued_by' => 30,
                'memo' => 'coaching_13667Feb_Sat_2021_02_52_33-218.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            197 => 
            array (
                'date_served' => '2021-02-11 02:00:00',
                'date_signed' => '2021-02-11 02:00:00',
                'id' => 219,
                'issued_by' => 30,
                'memo' => 'coaching_13669Feb_Sat_2021_02_53_36-219.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            198 => 
            array (
                'date_served' => '2021-02-10 02:00:00',
                'date_signed' => '2021-02-10 02:00:00',
                'id' => 220,
                'issued_by' => 30,
                'memo' => 'coaching_13665Feb_Sat_2021_02_54_35-220.webp',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            199 => 
            array (
                'date_served' => '2021-02-16 09:00:00',
                'date_signed' => '2021-02-16 09:00:00',
                'id' => 221,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
            'violation' => 'Idle ( Verbal Warning)',
            ),
            200 => 
            array (
                'date_served' => '2021-02-17 05:00:00',
                'date_signed' => '2021-02-16 05:00:00',
                'id' => 222,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Tardiness - Coaching and Verbal Warning',
            ),
            201 => 
            array (
                'date_served' => '2021-02-15 03:00:00',
                'date_signed' => '2021-02-15 03:00:00',
                'id' => 223,
                'issued_by' => 30,
                'memo' => 'coaching_36992Feb_Thu_2021_03_34_53-223.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            202 => 
            array (
                'date_served' => '2021-02-15 03:00:00',
                'date_signed' => '2021-02-16 03:00:00',
                'id' => 225,
                'issued_by' => 30,
                'memo' => 'coaching_36993Feb_Thu_2021_03_36_18-225.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            203 => 
            array (
                'date_served' => '2021-02-15 03:00:00',
                'date_signed' => '2021-02-16 03:00:00',
                'id' => 226,
                'issued_by' => 30,
                'memo' => 'coaching_36993Feb_Thu_2021_03_36_19-226.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            204 => 
            array (
                'date_served' => '2021-02-19 12:00:00',
                'date_signed' => '2021-02-19 12:00:00',
                'id' => 227,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
            'violation' => 'VA Tracker Negligence ( Did not Log Out)',
            ),
            205 => 
            array (
                'date_served' => '2021-02-16 01:00:00',
                'date_signed' => '2021-02-18 01:00:00',
                'id' => 228,
                'issued_by' => 35,
                'memo' => 'coaching_37582Feb_Fri_2021_01_25_37-228.eml',
                'status' => 1,
                'violation' => 'Tracker Manipulation',
            ),
            206 => 
            array (
                'date_served' => '2021-02-18 04:30:00',
                'date_signed' => '2021-02-18 04:30:00',
                'id' => 229,
                'issued_by' => 30,
                'memo' => 'coaching_13662Feb_Fri_2021_04_31_09-229.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            207 => 
            array (
                'date_served' => '2021-02-18 04:00:00',
                'date_signed' => '2021-02-18 04:00:00',
                'id' => 230,
                'issued_by' => 30,
                'memo' => 'coaching_15371Feb_Fri_2021_04_32_00-230.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            208 => 
            array (
                'date_served' => '2021-02-18 04:00:00',
                'date_signed' => '2021-02-18 04:00:00',
                'id' => 231,
                'issued_by' => 30,
                'memo' => 'coaching_37586Feb_Fri_2021_04_35_02-231.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            209 => 
            array (
                'date_served' => '2021-02-18 04:00:00',
                'date_signed' => '2021-02-18 04:00:00',
                'id' => 232,
                'issued_by' => 30,
                'memo' => 'coaching_13667Feb_Fri_2021_04_35_48-232.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            210 => 
            array (
                'date_served' => '2021-02-18 04:00:00',
                'date_signed' => '2021-02-18 04:00:00',
                'id' => 233,
                'issued_by' => 30,
                'memo' => 'coaching_15718Feb_Fri_2021_04_36_30-233.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            211 => 
            array (
                'date_served' => '2021-02-19 06:00:00',
                'date_signed' => '2021-02-19 07:00:00',
                'id' => 234,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Idle second occurrence',
            ),
            212 => 
            array (
                'date_served' => '2021-02-18 01:00:00',
                'date_signed' => '2021-02-18 01:00:00',
                'id' => 235,
                'issued_by' => 30,
                'memo' => 'coaching_37190Feb_Sat_2021_01_07_27-235.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            213 => 
            array (
                'date_served' => '2021-02-18 01:00:00',
                'date_signed' => '2021-02-18 01:00:00',
                'id' => 236,
                'issued_by' => 30,
                'memo' => 'coaching_13669Feb_Sat_2021_01_08_23-236.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            214 => 
            array (
                'date_served' => '2021-02-18 01:00:00',
                'date_signed' => '2021-02-18 01:00:00',
                'id' => 237,
                'issued_by' => 30,
                'memo' => 'coaching_13714Feb_Sat_2021_01_09_05-237.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            215 => 
            array (
                'date_served' => '2021-02-20 10:00:00',
                'date_signed' => '2021-02-20 10:00:00',
                'id' => 238,
                'issued_by' => 30,
                'memo' => 'coaching_36992Feb_Wed_2021_10_39_35-238.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            216 => 
            array (
                'date_served' => '2021-02-23 10:00:00',
                'date_signed' => '2021-02-24 10:00:00',
                'id' => 239,
                'issued_by' => 30,
                'memo' => 'coaching_36992Feb_Wed_2021_10_39_35-239.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            217 => 
            array (
                'date_served' => '2021-02-23 10:00:00',
                'date_signed' => '2021-02-24 10:00:00',
                'id' => 240,
                'issued_by' => 30,
                'memo' => 'coaching_13662Feb_Wed_2021_10_40_50-240.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            218 => 
            array (
                'date_served' => '2021-02-23 11:00:00',
                'date_signed' => '2021-02-24 11:00:00',
                'id' => 241,
                'issued_by' => 30,
                'memo' => 'coaching_37955Feb_Thu_2021_12_00_12-241.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            219 => 
            array (
                'date_served' => '2021-02-23 12:00:00',
                'date_signed' => '2021-02-24 12:00:00',
                'id' => 242,
                'issued_by' => 30,
                'memo' => 'coaching_37956Feb_Thu_2021_12_09_35-242.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            220 => 
            array (
                'date_served' => '2021-02-19 02:00:00',
                'date_signed' => '2021-02-19 02:00:00',
                'id' => 243,
                'issued_by' => 30,
                'memo' => 'coaching_37965Feb_Thu_2021_02_08_28-243.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            221 => 
            array (
                'date_served' => '2021-02-23 02:00:00',
                'date_signed' => '2021-02-24 02:00:00',
                'id' => 244,
                'issued_by' => 30,
                'memo' => 'coaching_37966Feb_Thu_2021_02_10_01-244.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            222 => 
            array (
                'date_served' => '2021-02-19 02:00:00',
                'date_signed' => '2021-02-20 02:00:00',
                'id' => 245,
                'issued_by' => 30,
                'memo' => 'coaching_37967Feb_Thu_2021_02_12_03-245.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            223 => 
            array (
                'date_served' => '2021-02-19 04:00:00',
                'date_signed' => '2021-02-19 04:15:00',
                'id' => 246,
                'issued_by' => 22,
                'memo' => 'coaching_13710Feb_Thu_2021_04_23_24-246.docx',
                'status' => 1,
                'violation' => 'Negligence - Multiple Attendance Infractions',
            ),
            224 => 
            array (
                'date_served' => '2021-02-21 06:43:00',
                'date_signed' => '2021-02-21 01:45:00',
                'id' => 247,
                'issued_by' => 22,
                'memo' => 'coaching_13710Feb_Thu_2021_04_27_05-247.docx',
                'status' => 1,
                'violation' => 'Offense on Integrity: Dishonesty/ Misleading',
            ),
            225 => 
            array (
                'date_served' => '2021-02-25 11:00:00',
                'date_signed' => '2021-02-25 11:00:00',
                'id' => 248,
                'issued_by' => 30,
                'memo' => 'coaching_37966Feb_Fri_2021_11_42_55-248.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            226 => 
            array (
                'date_served' => '2021-02-25 11:00:00',
                'date_signed' => '2021-02-25 11:00:00',
                'id' => 249,
                'issued_by' => 30,
                'memo' => 'coaching_37586Feb_Fri_2021_11_43_36-249.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            227 => 
            array (
                'date_served' => '2021-03-10 04:00:00',
                'date_signed' => '2021-03-10 04:00:00',
                'id' => 250,
                'issued_by' => 30,
                'memo' => 'coaching_13667Mar_Wed_2021_04_39_15-250.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            228 => 
            array (
                'date_served' => '2021-03-10 04:00:00',
                'date_signed' => '2021-03-10 04:00:00',
                'id' => 251,
                'issued_by' => 30,
                'memo' => 'coaching_39105Mar_Wed_2021_04_46_37-251.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            229 => 
            array (
                'date_served' => '2021-03-10 04:00:00',
                'date_signed' => '2021-03-10 04:00:00',
                'id' => 252,
                'issued_by' => 19,
                'memo' => 'coaching_36992Mar_Wed_2021_04_50_45-252.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            230 => 
            array (
                'date_served' => '2021-03-10 04:00:00',
                'date_signed' => '2021-03-10 04:00:00',
                'id' => 253,
                'issued_by' => 19,
                'memo' => 'coaching_36992Mar_Wed_2021_04_50_46-253.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            231 => 
            array (
                'date_served' => '2021-03-10 04:00:00',
                'date_signed' => '2021-03-10 04:00:00',
                'id' => 254,
                'issued_by' => 30,
                'memo' => 'coaching_36992Mar_Wed_2021_04_50_46-254.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            232 => 
            array (
                'date_served' => '2021-03-10 04:00:00',
                'date_signed' => '2021-03-10 04:00:00',
                'id' => 255,
                'issued_by' => 30,
                'memo' => 'coaching_37955Mar_Wed_2021_04_51_52-255.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            233 => 
            array (
                'date_served' => '2021-03-10 04:00:00',
                'date_signed' => '2021-03-10 04:00:00',
                'id' => 256,
                'issued_by' => 30,
                'memo' => 'coaching_13663Mar_Wed_2021_04_52_52-256.pdf',
                'status' => 1,
                'violation' => 'Attendance Infractions',
            ),
            234 => 
            array (
                'date_served' => '2021-03-10 04:00:00',
                'date_signed' => '2021-03-10 04:00:00',
                'id' => 258,
                'issued_by' => 30,
                'memo' => 'coaching_36993Mar_Wed_2021_04_54_00-258.pdf',
                'status' => 1,
                'violation' => 'Attendance Infractions',
            ),
            235 => 
            array (
                'date_served' => '2021-03-10 04:00:00',
                'date_signed' => '2021-03-10 04:00:00',
                'id' => 259,
                'issued_by' => 30,
                'memo' => 'coaching_37000Mar_Wed_2021_04_55_02-259.pdf',
                'status' => 1,
                'violation' => 'Attendance Infractions',
            ),
            236 => 
            array (
                'date_served' => '2021-03-10 05:00:00',
                'date_signed' => '2021-03-10 05:00:00',
                'id' => 260,
                'issued_by' => 30,
                'memo' => 'coaching_39108Mar_Wed_2021_05_02_02-260.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            237 => 
            array (
                'date_served' => '2021-03-10 05:00:00',
                'date_signed' => '2021-03-10 05:00:00',
                'id' => 261,
                'issued_by' => 30,
                'memo' => 'coaching_30678Mar_Wed_2021_05_02_33-261.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            238 => 
            array (
                'date_served' => '2021-03-10 05:00:00',
                'date_signed' => '2021-03-10 05:00:00',
                'id' => 262,
                'issued_by' => 30,
                'memo' => 'coaching_37966Mar_Wed_2021_05_03_07-262.pdf',
                'status' => 1,
                'violation' => 'Attendace Infraction',
            ),
            239 => 
            array (
                'date_served' => '2021-03-10 05:00:00',
                'date_signed' => '2021-03-10 05:00:00',
                'id' => 263,
                'issued_by' => 30,
                'memo' => 'coaching_39109Mar_Wed_2021_05_04_25-263.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            240 => 
            array (
                'date_served' => '2021-03-18 04:00:00',
                'date_signed' => '2021-03-18 05:00:00',
                'id' => 264,
                'issued_by' => 41,
                'memo' => 'coaching_40246Mar_Sat_2021_05_09_51-264.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            241 => 
            array (
                'date_served' => '2021-04-02 11:00:00',
                'date_signed' => '2021-04-06 11:00:00',
                'id' => 265,
                'issued_by' => 41,
                'memo' => 'coaching_40246Apr_Tue_2021_11_13_32-265.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            242 => 
            array (
                'date_served' => '1970-01-01 08:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 266,
                'issued_by' => 29,
                'memo' => 'coaching_42292Apr_Sat_2021_12_22_02-266.pdf',
                'status' => 1,
                'violation' => NULL,
            ),
            243 => 
            array (
                'date_served' => '2021-04-13 05:00:00',
                'date_signed' => '2021-04-13 05:45:00',
                'id' => 267,
                'issued_by' => 41,
                'memo' => 'coaching_42477Apr_Tue_2021_05_42_34-267.pdf',
                'status' => 1,
                'violation' => 'Early Out',
            ),
            244 => 
            array (
                'date_served' => '2021-04-13 06:00:00',
                'date_signed' => '2021-04-13 06:30:00',
                'id' => 268,
                'issued_by' => 41,
                'memo' => 'coaching_40412Apr_Tue_2021_06_40_58-268.pdf',
                'status' => 1,
                'violation' => 'Early Out',
            ),
            245 => 
            array (
                'date_served' => '2021-04-20 12:45:00',
                'date_signed' => '2021-04-20 12:45:00',
                'id' => 269,
                'issued_by' => 41,
                'memo' => 'coaching_43457Apr_Sat_2021_12_11_20-269.docx',
                'status' => 1,
                'violation' => 'High Keyboard stroke',
            ),
            246 => 
            array (
                'date_served' => '2021-04-21 11:30:00',
                'date_signed' => '2021-04-21 11:45:00',
                'id' => 270,
                'issued_by' => 41,
                'memo' => 'coaching_40479Apr_Wed_2021_11_48_50-270.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            247 => 
            array (
                'date_served' => '2021-04-20 11:00:00',
                'date_signed' => '2021-04-20 11:15:00',
                'id' => 271,
                'issued_by' => 22,
                'memo' => 'coaching_13710Apr_Thu_2021_02_07_47-271.docx',
                'status' => 1,
                'violation' => 'Multiple Attendance Infractions',
            ),
            248 => 
            array (
                'date_served' => '2021-04-26 12:45:00',
                'date_signed' => '2021-04-27 01:00:00',
                'id' => 272,
                'issued_by' => 41,
                'memo' => 'coaching_37965Apr_Wed_2021_01_02_48-272.pdf',
                'status' => 1,
                'violation' => 'Late and Idle',
            ),
            249 => 
            array (
                'date_served' => '2021-04-30 10:00:00',
                'date_signed' => '2021-04-30 10:15:00',
                'id' => 273,
                'issued_by' => 41,
                'memo' => 'coaching_40479May_Sat_2021_02_12_04-273.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            250 => 
            array (
                'date_served' => '2021-05-06 11:45:00',
                'date_signed' => '2021-05-06 12:00:00',
                'id' => 274,
                'issued_by' => 41,
                'memo' => 'coaching_45305May_Fri_2021_12_00_25-274.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            251 => 
            array (
                'date_served' => '2021-05-05 01:00:00',
                'date_signed' => '2021-05-06 01:00:00',
                'id' => 275,
                'issued_by' => 41,
                'memo' => 'coaching_42477May_Fri_2021_04_49_51-275.docx',
                'status' => 1,
                'violation' => 'High Keyboard Activity',
            ),
            252 => 
            array (
                'date_served' => '2021-05-17 05:00:00',
                'date_signed' => '2021-05-18 12:00:00',
                'id' => 276,
                'issued_by' => 41,
                'memo' => 'coaching_13323May_Wed_2021_02_18_53-276.pdf',
                'status' => 1,
                'violation' => 'absence',
            ),
            253 => 
            array (
                'date_served' => '2021-05-19 10:30:00',
                'date_signed' => '2021-05-19 11:00:00',
                'id' => 277,
                'issued_by' => 41,
                'memo' => 'coaching_37965May_Wed_2021_10_59_43-277.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            254 => 
            array (
                'date_served' => '2021-05-19 10:00:00',
                'date_signed' => '2021-05-19 11:00:00',
                'id' => 278,
                'issued_by' => 41,
                'memo' => 'coaching_13656May_Wed_2021_11_01_53-278.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            255 => 
            array (
                'date_served' => '2021-05-24 01:00:00',
                'date_signed' => '2021-05-24 01:00:00',
                'id' => 279,
                'issued_by' => 21,
                'memo' => 'coaching_13649May_Tue_2021_01_22_32-279.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            256 => 
            array (
                'date_served' => '2021-05-25 01:15:00',
                'date_signed' => '2021-05-25 01:30:00',
                'id' => 280,
                'issued_by' => 41,
                'memo' => 'coaching_47648May_Tue_2021_01_42_13-280.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            257 => 
            array (
                'date_served' => '2021-05-21 01:00:00',
                'date_signed' => '2021-05-26 04:30:00',
                'id' => 281,
                'issued_by' => 41,
                'memo' => 'coaching_37965May_Wed_2021_04_38_29-281.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            258 => 
            array (
                'date_served' => '2021-05-28 12:15:00',
                'date_signed' => '2021-05-28 12:30:00',
                'id' => 282,
                'issued_by' => 41,
                'memo' => 'coaching_36901May_Fri_2021_12_38_30-282.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            259 => 
            array (
                'date_served' => '2021-05-28 12:30:00',
                'date_signed' => '2021-05-28 12:45:00',
                'id' => 283,
                'issued_by' => 41,
                'memo' => 'coaching_47983May_Fri_2021_12_53_39-283.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            260 => 
            array (
                'date_served' => '2021-05-20 10:00:00',
                'date_signed' => '2021-05-28 10:00:00',
                'id' => 284,
                'issued_by' => 41,
                'memo' => 'coaching_13323May_Fri_2021_10_07_19-284.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            261 => 
            array (
                'date_served' => '2021-06-01 11:00:00',
                'date_signed' => '2021-06-02 12:00:00',
                'id' => 285,
                'issued_by' => 41,
                'memo' => 'coaching_47648Jun_Wed_2021_12_47_02-285.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            262 => 
            array (
                'date_served' => '2021-06-01 11:30:00',
                'date_signed' => '2021-06-02 01:00:00',
                'id' => 286,
                'issued_by' => 41,
                'memo' => 'coaching_37965Jun_Wed_2021_01_30_14-286.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            263 => 
            array (
                'date_served' => '2021-06-02 11:00:00',
                'date_signed' => '2021-06-02 11:15:00',
                'id' => 287,
                'issued_by' => 41,
                'memo' => 'coaching_47648Jun_Wed_2021_11_23_18-287.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            264 => 
            array (
                'date_served' => '2021-06-10 01:00:00',
                'date_signed' => '2021-06-10 01:30:00',
                'id' => 288,
                'issued_by' => 41,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Idle',
            ),
            265 => 
            array (
                'date_served' => '2021-06-02 02:00:00',
                'date_signed' => '2021-06-02 02:00:00',
                'id' => 289,
                'issued_by' => 50,
                'memo' => 'coaching_49670Jun_Thu_2021_01_56_37-289.pdf',
                'status' => 1,
                'violation' => 'Coaching Log',
            ),
            266 => 
            array (
                'date_served' => '2021-06-09 11:00:00',
                'date_signed' => '2021-06-10 06:00:00',
                'id' => 290,
                'issued_by' => 41,
                'memo' => 'coaching_37965Jun_Thu_2021_06_19_30-290.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            267 => 
            array (
                'date_served' => '2021-06-12 12:30:00',
                'date_signed' => '2021-06-12 07:45:00',
                'id' => 291,
                'issued_by' => 41,
                'memo' => 'coaching_36901Jun_Mon_2021_10_29_58-291.pdf',
                'status' => 1,
                'violation' => 'Late and Idle',
            ),
            268 => 
            array (
                'date_served' => '2021-06-17 01:30:00',
                'date_signed' => '2021-06-17 04:30:00',
                'id' => 292,
                'issued_by' => 41,
                'memo' => 'coaching_47980Jun_Thu_2021_04_46_30-292.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            269 => 
            array (
                'date_served' => '2021-06-22 06:00:00',
                'date_signed' => '2021-06-02 02:45:00',
                'id' => 293,
                'issued_by' => 50,
                'memo' => 'coaching_43511Jun_Tue_2021_06_54_20-293.docx',
                'status' => 1,
                'violation' => NULL,
            ),
            270 => 
            array (
                'date_served' => '2021-06-24 10:00:00',
                'date_signed' => '2021-06-24 10:30:00',
                'id' => 294,
                'issued_by' => 41,
                'memo' => 'coaching_47983Jun_Thu_2021_10_32_38-294.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            271 => 
            array (
                'date_served' => '2021-06-25 10:45:00',
                'date_signed' => '2021-06-26 01:00:00',
                'id' => 295,
                'issued_by' => 41,
                'memo' => 'coaching_51861Jun_Sat_2021_01_13_06-295.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            272 => 
            array (
                'date_served' => '2021-06-25 10:00:00',
                'date_signed' => '2021-06-27 05:00:00',
                'id' => 296,
                'issued_by' => 52,
                'memo' => 'coaching_52160Jun_Mon_2021_10_50_09-296.xlsx',
                'status' => 1,
                'violation' => 'VA Tracker negligence',
            ),
            273 => 
            array (
                'date_served' => '2021-07-02 11:30:00',
                'date_signed' => '2021-07-02 11:45:00',
                'id' => 297,
                'issued_by' => 41,
                'memo' => 'coaching_47983Jul_Fri_2021_11_51_09-297.pdf',
                'status' => 1,
                'violation' => 'Over break',
            ),
            274 => 
            array (
                'date_served' => '2021-07-02 11:00:00',
                'date_signed' => '2021-07-02 11:00:00',
                'id' => 298,
                'issued_by' => 52,
                'memo' => 'coaching_52649Jul_Sat_2021_12_01_13-298.xlsx',
                'status' => 1,
                'violation' => 'High Frequency Keyboard',
            ),
            275 => 
            array (
                'date_served' => '2021-07-06 11:45:00',
                'date_signed' => '2021-07-07 12:00:00',
                'id' => 299,
                'issued_by' => 41,
                'memo' => 'coaching_47660Jul_Wed_2021_12_47_04-299.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            276 => 
            array (
                'date_served' => '2021-07-07 09:00:00',
                'date_signed' => '2021-07-07 09:00:00',
                'id' => 300,
                'issued_by' => 52,
                'memo' => 'coaching_52649Jul_Wed_2021_09_10_03-300.docx',
                'status' => 1,
                'violation' => 'System Manipulation',
            ),
            277 => 
            array (
                'date_served' => '2021-07-09 10:30:00',
                'date_signed' => '2021-07-09 10:45:00',
                'id' => 301,
                'issued_by' => 41,
                'memo' => 'coaching_37190Jul_Fri_2021_10_53_31-301.pdf',
                'status' => 1,
                'violation' => 'Over break',
            ),
            278 => 
            array (
                'date_served' => '2021-07-10 04:45:00',
                'date_signed' => '2021-07-10 05:00:00',
                'id' => 302,
                'issued_by' => 41,
                'memo' => 'coaching_51861Jul_Sat_2021_04_57_02-302.pdf',
                'status' => 1,
                'violation' => 'Over break',
            ),
            279 => 
            array (
                'date_served' => '2021-07-16 12:15:00',
                'date_signed' => '2021-07-16 12:30:00',
                'id' => 303,
                'issued_by' => 41,
                'memo' => 'coaching_54348Jul_Fri_2021_12_35_31-303.pdf',
                'status' => 1,
                'violation' => 'Idle',
            ),
            280 => 
            array (
                'date_served' => '2021-06-14 08:00:00',
                'date_signed' => '2021-07-14 08:00:00',
                'id' => 304,
                'issued_by' => 35,
                'memo' => 'coaching_49673Jul_Fri_2021_08_45_11-304.docx',
                'status' => 1,
                'violation' => 'Idle',
            ),
            281 => 
            array (
                'date_served' => '2021-07-13 11:00:00',
                'date_signed' => '2021-07-13 11:00:00',
                'id' => 305,
                'issued_by' => 21,
                'memo' => 'coaching_13649Jul_Fri_2021_11_29_49-305.docx',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            282 => 
            array (
                'date_served' => '2021-03-19 12:00:00',
                'date_signed' => '2021-03-19 12:00:00',
                'id' => 306,
                'issued_by' => 35,
                'memo' => 'coaching_49672Jul_Sat_2021_12_23_14-306.docx',
                'status' => 1,
                'violation' => 'Tardiness - Written Warning',
            ),
            283 => 
            array (
                'date_served' => '2021-07-16 11:30:00',
                'date_signed' => '2021-07-17 12:30:00',
                'id' => 307,
                'issued_by' => 41,
                'memo' => 'coaching_47978Jul_Sat_2021_12_59_43-307.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            284 => 
            array (
                'date_served' => '2021-07-16 11:30:00',
                'date_signed' => '2021-07-16 11:45:00',
                'id' => 308,
                'issued_by' => 41,
                'memo' => 'coaching_47985Jul_Sat_2021_01_01_29-308.pdf',
                'status' => 1,
                'violation' => 'Overbreak',
            ),
            285 => 
            array (
                'date_served' => '2021-07-17 01:00:00',
                'date_signed' => '2021-07-17 01:30:00',
                'id' => 309,
                'issued_by' => 41,
                'memo' => 'coaching_51861Jul_Sat_2021_02_09_20-309.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            286 => 
            array (
                'date_served' => '2021-07-19 10:15:00',
                'date_signed' => '2021-07-19 10:30:00',
                'id' => 310,
                'issued_by' => 41,
                'memo' => 'coaching_13656Jul_Mon_2021_10_42_18-310.pdf',
                'status' => 1,
                'violation' => 'Absent',
            ),
            287 => 
            array (
                'date_served' => '2021-07-21 03:00:00',
                'date_signed' => '2021-07-21 03:00:00',
                'id' => 311,
                'issued_by' => 50,
                'memo' => 'coaching_49678Jul_Wed_2021_03_55_28-311.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            288 => 
            array (
                'date_served' => '2021-07-20 07:00:00',
                'date_signed' => '2021-07-20 07:00:00',
                'id' => 312,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Tardiness - Coaching and Verbal Warning',
            ),
            289 => 
            array (
                'date_served' => '2021-07-20 10:00:00',
                'date_signed' => '2021-07-21 07:00:00',
                'id' => 313,
                'issued_by' => 35,
                'memo' => NULL,
                'status' => 1,
            'violation' => 'Idle ( Possible Monetary Deduction)',
            ),
            290 => 
            array (
                'date_served' => '2021-07-20 02:30:00',
                'date_signed' => '2021-07-21 07:00:00',
                'id' => 314,
                'issued_by' => 29,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Tardiness - Coaching and Verbal Warning',
            ),
            291 => 
            array (
                'date_served' => '2021-07-22 12:00:00',
                'date_signed' => '2021-07-22 12:00:00',
                'id' => 315,
                'issued_by' => 50,
                'memo' => 'coaching_49666Jul_Thu_2021_12_19_42-315.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            292 => 
            array (
                'date_served' => '2021-07-22 06:00:00',
                'date_signed' => '2021-07-22 06:00:00',
                'id' => 316,
                'issued_by' => 58,
                'memo' => 'coaching_16579Jul_Thu_2021_06_27_26-316.pdf',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            293 => 
            array (
                'date_served' => '2021-07-22 06:00:00',
                'date_signed' => '2021-07-22 06:00:00',
                'id' => 317,
                'issued_by' => 58,
                'memo' => 'coaching_13665Jul_Thu_2021_06_29_55-317.pdf',
                'status' => 1,
                'violation' => 'System Idle',
            ),
            294 => 
            array (
                'date_served' => '2021-07-22 07:00:00',
                'date_signed' => '2021-07-22 07:00:00',
                'id' => 318,
                'issued_by' => 58,
                'memo' => 'coaching_55183Jul_Thu_2021_07_01_01-318.pdf',
                'status' => 1,
                'violation' => 'Multiple Attendance infraction',
            ),
            295 => 
            array (
                'date_served' => '2021-07-22 12:00:00',
                'date_signed' => '2021-07-22 12:00:00',
                'id' => 319,
                'issued_by' => 50,
                'memo' => 'coaching_55341Jul_Fri_2021_12_34_46-319.docx',
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            296 => 
            array (
                'date_served' => '2021-07-24 05:15:00',
                'date_signed' => '2021-07-24 05:45:00',
                'id' => 320,
                'issued_by' => 41,
                'memo' => 'coaching_47976Jul_Sat_2021_06_27_15-320.pdf',
                'status' => 1,
                'violation' => 'Absent',
            ),
            297 => 
            array (
                'date_served' => '2021-07-24 05:15:00',
                'date_signed' => '2021-07-26 11:00:00',
                'id' => 321,
                'issued_by' => 41,
                'memo' => 'coaching_40480Jul_Mon_2021_11_50_00-321.pdf',
                'status' => 1,
                'violation' => 'Late',
            ),
            298 => 
            array (
                'date_served' => '2021-07-28 03:00:00',
                'date_signed' => '2021-07-29 03:00:00',
                'id' => 322,
                'issued_by' => 29,
                'memo' => 'coaching_55404Jul_Wed_2021_03_50_48-322.png',
                'status' => 1,
                'violation' => 'test',
            ),
            299 => 
            array (
                'date_served' => '2021-07-28 03:00:00',
                'date_signed' => '2021-07-30 03:00:00',
                'id' => 323,
                'issued_by' => 29,
                'memo' => 'coaching_55404Jul_Wed_2021_03_52_25-323.png',
                'status' => 1,
                'violation' => 'test2',
            ),
            300 => 
            array (
                'date_served' => '2021-07-28 10:45:00',
                'date_signed' => '2021-07-28 11:45:00',
                'id' => 324,
                'issued_by' => 41,
                'memo' => 'coaching_47982Jul_Thu_2021_02_14_45-324.pdf',
                'status' => 1,
                'violation' => 'Absent',
            ),
            301 => 
            array (
                'date_served' => '2021-07-28 02:00:00',
                'date_signed' => '2021-07-28 02:00:00',
                'id' => 325,
                'issued_by' => 35,
                'memo' => 'coaching_47795Jul_Thu_2021_02_14_59-325.pdf',
                'status' => 1,
                'violation' => 'Tardiness - Written Warning',
            ),
            302 => 
            array (
                'date_served' => '2021-08-04 03:00:00',
                'date_signed' => '2021-08-04 03:00:00',
                'id' => 326,
                'issued_by' => 58,
                'memo' => 'coaching_57306Aug_Wed_2021_04_00_07-326.pdf',
                'status' => 1,
                'violation' => 'Non compliance to EOD',
            ),
            303 => 
            array (
                'date_served' => '2021-08-05 05:00:00',
                'date_signed' => '2021-08-05 05:00:00',
                'id' => 327,
                'issued_by' => 58,
                'memo' => 'coaching_57454Aug_Thu_2021_05_59_08-327.pdf',
                'status' => 1,
                'violation' => 'High Usage of Keyboard',
            ),
        ));
        
        
    }
}