<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MemoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('memo')->delete();
        
        \DB::table('memo')->insert(array (
            0 => 
            array (
                'date_served' => '2020-02-08 05:55:00',
                'date_signed' => '2020-02-08 05:55:00',
                'id' => 1,
                'issued_by' => 19,
                'memo' => '5e3de7bc60ddd',
                'status' => 1,
                'violation' => NULL,
            ),
            1 => 
            array (
                'date_served' => '2020-02-08 06:44:00',
                'date_signed' => '2020-02-08 06:44:00',
                'id' => 2,
                'issued_by' => 19,
                'memo' => '5e3de84a398a7',
                'status' => 1,
                'violation' => 'sdfsfsdf',
            ),
            2 => 
            array (
                'date_served' => '2020-02-08 10:15:00',
                'date_signed' => '2020-02-08 10:15:00',
                'id' => 3,
                'issued_by' => 11,
                'memo' => '5e3e19cb118ff',
                'status' => 1,
                'violation' => 'sfsf',
            ),
            3 => 
            array (
                'date_served' => '2020-02-09 04:18:00',
                'date_signed' => '2020-02-09 04:18:00',
                'id' => 4,
                'issued_by' => 19,
                'memo' => '5e4319156f252',
                'status' => 1,
                'violation' => NULL,
            ),
            4 => 
            array (
                'date_served' => '2020-02-12 05:07:00',
                'date_signed' => '2020-02-12 05:07:00',
                'id' => 5,
                'issued_by' => 16,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            5 => 
            array (
                'date_served' => '2020-02-05 04:22:00',
                'date_signed' => '1970-01-01 12:00:00',
                'id' => 7,
                'issued_by' => 19,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            6 => 
            array (
                'date_served' => '2020-02-03 06:16:00',
                'date_signed' => '2020-02-12 06:17:00',
                'id' => 9,
                'issued_by' => 19,
                'memo' => NULL,
                'status' => 2,
                'violation' => 'Multiple Attendance Infraction',
            ),
            7 => 
            array (
                'date_served' => '2020-02-14 03:44:00',
                'date_signed' => '2020-02-14 03:44:00',
                'id' => 10,
                'issued_by' => 19,
                'memo' => '5e45a7443cb3a',
                'status' => 1,
                'violation' => 'test',
            ),
            8 => 
            array (
                'date_served' => '2020-02-14 03:48:00',
                'date_signed' => '2020-02-14 03:48:00',
                'id' => 11,
                'issued_by' => 11,
                'memo' => '5e45a83c2812f',
                'status' => 1,
                'violation' => 'test termination',
            ),
            9 => 
            array (
                'date_served' => '2020-02-26 04:10:00',
                'date_signed' => '2020-02-26 04:10:00',
                'id' => 14,
                'issued_by' => 19,
                'memo' => '5e557f6691619',
                'status' => 1,
                'violation' => 'sdfsdfdsf',
            ),
            10 => 
            array (
                'date_served' => '2020-02-26 04:39:00',
                'date_signed' => '2020-02-26 05:39:00',
                'id' => 15,
                'issued_by' => 16,
                'memo' => '5e55872f11c1c',
                'status' => 1,
                'violation' => 'FTE for Dishonesty',
            ),
            11 => 
            array (
                'date_served' => '2020-02-26 08:50:00',
                'date_signed' => '2020-02-28 08:50:00',
                'id' => 16,
                'issued_by' => 16,
                'memo' => '5e55c20780568',
                'status' => 1,
                'violation' => 'NTE for Incomplete internet connection and/or back-up computer',
            ),
            12 => 
            array (
                'date_served' => '2020-03-03 11:16:00',
                'date_signed' => '2020-03-05 11:16:00',
                'id' => 17,
                'issued_by' => 16,
                'memo' => '5e5e75ff6e9f9',
                'status' => 1,
                'violation' => 'FTE for Dishonesty',
            ),
            13 => 
            array (
                'date_served' => '2020-03-04 06:06:00',
                'date_signed' => '2020-03-06 06:06:00',
                'id' => 18,
                'issued_by' => 16,
                'memo' => '5e5ed554925c3',
                'status' => 1,
                'violation' => 'FTE for NCNS',
            ),
            14 => 
            array (
                'date_served' => '2020-03-06 02:54:00',
                'date_signed' => '2020-03-09 02:54:00',
                'id' => 19,
                'issued_by' => 16,
                'memo' => '5e614b8821dc5',
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            15 => 
            array (
                'date_served' => '2020-03-06 06:06:00',
                'date_signed' => '2020-03-09 06:06:00',
                'id' => 20,
                'issued_by' => 16,
                'memo' => '5e617840082b2',
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            16 => 
            array (
                'date_served' => '2020-03-07 01:18:00',
                'date_signed' => '2020-03-09 01:18:00',
                'id' => 21,
                'issued_by' => 16,
                'memo' => '5e62867dd1674',
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            17 => 
            array (
                'date_served' => '2020-03-07 03:11:00',
                'date_signed' => '2020-03-09 03:11:00',
                'id' => 22,
                'issued_by' => 16,
                'memo' => '5e62a11a442c4',
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            18 => 
            array (
                'date_served' => '2020-03-07 03:51:00',
                'date_signed' => '2020-03-09 03:51:00',
                'id' => 23,
                'issued_by' => 23,
                'memo' => '5e62ab9a5653f',
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            19 => 
            array (
                'date_served' => '2020-03-07 04:00:00',
                'date_signed' => '2020-03-09 04:00:00',
                'id' => 24,
                'issued_by' => 24,
                'memo' => '5e62ac68df3b7',
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            20 => 
            array (
                'date_served' => '2020-03-07 05:17:00',
                'date_signed' => '2020-03-09 05:18:00',
                'id' => 25,
                'issued_by' => 19,
                'memo' => '5e62be9f15875',
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            21 => 
            array (
                'date_served' => '2020-03-07 05:44:00',
                'date_signed' => '2020-03-09 05:44:00',
                'id' => 27,
                'issued_by' => 19,
                'memo' => '5e62c4dbd7bea',
                'status' => 1,
                'violation' => 'FTE for Dishonesty',
            ),
            22 => 
            array (
                'date_served' => '2020-03-10 04:00:00',
                'date_signed' => '2020-03-12 04:00:00',
                'id' => 28,
                'issued_by' => 16,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            23 => 
            array (
                'date_served' => '2020-03-10 04:37:00',
                'date_signed' => '2020-03-12 04:37:00',
                'id' => 29,
                'issued_by' => 16,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            24 => 
            array (
                'date_served' => '2020-03-10 04:48:00',
                'date_signed' => '2020-03-12 04:48:00',
                'id' => 30,
                'issued_by' => 16,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            25 => 
            array (
                'date_served' => '2020-04-02 07:07:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 31,
                'issued_by' => 23,
                'memo' => NULL,
                'status' => 1,
                'violation' => NULL,
            ),
            26 => 
            array (
                'date_served' => '2020-04-04 01:34:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 32,
                'issued_by' => 19,
                'memo' => '5e87747372bd4',
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            27 => 
            array (
                'date_served' => '2020-04-04 01:48:00',
                'date_signed' => '2020-04-06 01:48:00',
                'id' => 36,
                'issued_by' => 16,
                'memo' => NULL,
                'status' => 1,
                'violation' => NULL,
            ),
            28 => 
            array (
                'date_served' => '2020-04-04 01:52:00',
                'date_signed' => '2020-04-06 01:52:00',
                'id' => 37,
                'issued_by' => 16,
                'memo' => '5e877873da21c',
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            29 => 
            array (
                'date_served' => '2020-04-04 01:56:00',
                'date_signed' => '2020-04-06 01:56:00',
                'id' => 38,
                'issued_by' => 16,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            30 => 
            array (
                'date_served' => '2020-04-04 01:58:00',
                'date_signed' => '2020-04-04 01:58:00',
                'id' => 39,
                'issued_by' => 19,
                'memo' => '5e877f4b95b42',
                'status' => 1,
                'violation' => 'test',
            ),
            31 => 
            array (
                'date_served' => '2020-04-04 02:24:00',
                'date_signed' => '2020-04-04 02:24:00',
                'id' => 40,
                'issued_by' => 19,
                'memo' => '5e8c97250d94d',
                'status' => 1,
                'violation' => 'test2',
            ),
            32 => 
            array (
                'date_served' => '2020-04-07 12:59:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 41,
                'issued_by' => 21,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            33 => 
            array (
                'date_served' => '1970-01-01 08:00:00',
                'date_signed' => '1970-01-01 08:00:00',
                'id' => 42,
                'issued_by' => 19,
                'memo' => NULL,
                'status' => 1,
                'violation' => NULL,
            ),
            34 => 
            array (
                'date_served' => '2020-04-08 11:56:00',
                'date_signed' => '2020-04-10 11:56:00',
                'id' => 43,
                'issued_by' => 19,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'FTE for multiple attendance offenses',
            ),
            35 => 
            array (
                'date_served' => '2020-05-01 03:41:00',
                'date_signed' => '2020-05-04 03:41:00',
                'id' => 44,
                'issued_by' => 21,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Attendance issues',
            ),
            36 => 
            array (
                'date_served' => '2020-08-18 03:00:00',
                'date_signed' => '2020-08-18 03:00:00',
                'id' => 45,
                'issued_by' => 29,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Attendance Infractions',
            ),
            37 => 
            array (
                'date_served' => '2020-12-09 09:00:00',
                'date_signed' => '2020-12-10 12:00:00',
                'id' => 46,
                'issued_by' => 21,
                'memo' => '601d6dcf29e01',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            38 => 
            array (
                'date_served' => '2021-01-12 09:00:00',
                'date_signed' => '2020-12-07 12:00:00',
                'id' => 47,
                'issued_by' => 21,
                'memo' => '601d6fc16989c',
                'status' => 1,
                'violation' => 'Multiple attendance issues',
            ),
            39 => 
            array (
                'date_served' => '2021-07-16 10:00:00',
                'date_signed' => '2021-07-16 10:00:00',
                'id' => 48,
                'issued_by' => 3,
                'memo' => '60f19ab600fdf',
                'status' => 1,
                'violation' => 'test',
            ),
            40 => 
            array (
                'date_served' => '2021-07-21 03:00:00',
                'date_signed' => '2021-07-21 03:00:00',
                'id' => 49,
                'issued_by' => 50,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'Attendance Infraction',
            ),
            41 => 
            array (
                'date_served' => '1970-01-01 08:00:00',
                'date_signed' => '2021-07-28 03:00:00',
                'id' => 50,
                'issued_by' => 50,
                'memo' => NULL,
                'status' => 1,
                'violation' => 'test',
            ),
        ));
        
        
    }
}