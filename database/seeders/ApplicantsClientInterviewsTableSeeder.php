<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ApplicantsClientInterviewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('applicants_client_interviews')->delete();
        
        \DB::table('applicants_client_interviews')->insert(array (
            0 => 
            array (
                'applicant_id' => 1,
                'interview_id' => 262,
            ),
            1 => 
            array (
                'applicant_id' => 3,
                'interview_id' => 267,
            ),
            2 => 
            array (
                'applicant_id' => 2,
                'interview_id' => 269,
            ),
            3 => 
            array (
                'applicant_id' => 5,
                'interview_id' => 273,
            ),
            4 => 
            array (
                'applicant_id' => 13269,
                'interview_id' => 281,
            ),
            5 => 
            array (
                'applicant_id' => 13274,
                'interview_id' => 283,
            ),
            6 => 
            array (
                'applicant_id' => 13304,
                'interview_id' => 303,
            ),
            7 => 
            array (
                'applicant_id' => 13306,
                'interview_id' => 305,
            ),
            8 => 
            array (
                'applicant_id' => 13657,
                'interview_id' => 341,
            ),
            9 => 
            array (
                'applicant_id' => 13739,
                'interview_id' => 669,
            ),
            10 => 
            array (
                'applicant_id' => 13684,
                'interview_id' => 1009,
            ),
            11 => 
            array (
                'applicant_id' => 13657,
                'interview_id' => 1010,
            ),
            12 => 
            array (
                'applicant_id' => 13715,
                'interview_id' => 1012,
            ),
            13 => 
            array (
                'applicant_id' => 13733,
                'interview_id' => 3048,
            ),
            14 => 
            array (
                'applicant_id' => 13776,
                'interview_id' => 3397,
            ),
            15 => 
            array (
                'applicant_id' => 18664,
                'interview_id' => 6442,
            ),
            16 => 
            array (
                'applicant_id' => 17542,
                'interview_id' => 8004,
            ),
            17 => 
            array (
                'applicant_id' => 18664,
                'interview_id' => 8360,
            ),
            18 => 
            array (
                'applicant_id' => 17967,
                'interview_id' => 15877,
            ),
            19 => 
            array (
                'applicant_id' => 55404,
                'interview_id' => 52686,
            ),
        ));
        
        
    }
}