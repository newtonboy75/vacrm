<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FosUserUserGroupTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('fos_user_user_group')->delete();
        
        \DB::table('fos_user_user_group')->insert(array (
            0 => 
            array (
                'group_id' => 1,
                'user_id' => 13269,
            ),
        ));
        
        
    }
}