<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EmployeesJobsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('employees_jobs')->delete();
        
        \DB::table('employees_jobs')->insert(array (
            0 => 
            array (
                'employee_id' => 1,
                'job_id' => 163,
            ),
            1 => 
            array (
                'employee_id' => 4074,
                'job_id' => 23,
            ),
            2 => 
            array (
                'employee_id' => 9581,
                'job_id' => 66,
            ),
            3 => 
            array (
                'employee_id' => 9602,
                'job_id' => 111,
            ),
            4 => 
            array (
                'employee_id' => 13269,
                'job_id' => 59,
            ),
            5 => 
            array (
                'employee_id' => 13306,
                'job_id' => 112,
            ),
            6 => 
            array (
                'employee_id' => 13306,
                'job_id' => 113,
            ),
            7 => 
            array (
                'employee_id' => 13321,
                'job_id' => 3,
            ),
            8 => 
            array (
                'employee_id' => 13323,
                'job_id' => 114,
            ),
            9 => 
            array (
                'employee_id' => 13326,
                'job_id' => 28,
            ),
            10 => 
            array (
                'employee_id' => 13376,
                'job_id' => 127,
            ),
            11 => 
            array (
                'employee_id' => 13376,
                'job_id' => 128,
            ),
            12 => 
            array (
                'employee_id' => 13376,
                'job_id' => 129,
            ),
            13 => 
            array (
                'employee_id' => 13433,
                'job_id' => 46,
            ),
            14 => 
            array (
                'employee_id' => 13641,
                'job_id' => 31,
            ),
            15 => 
            array (
                'employee_id' => 13642,
                'job_id' => 48,
            ),
            16 => 
            array (
                'employee_id' => 13643,
                'job_id' => 84,
            ),
            17 => 
            array (
                'employee_id' => 13644,
                'job_id' => 1,
            ),
            18 => 
            array (
                'employee_id' => 13645,
                'job_id' => 91,
            ),
            19 => 
            array (
                'employee_id' => 13646,
                'job_id' => 107,
            ),
            20 => 
            array (
                'employee_id' => 13647,
                'job_id' => 123,
            ),
            21 => 
            array (
                'employee_id' => 13648,
                'job_id' => 90,
            ),
            22 => 
            array (
                'employee_id' => 13649,
                'job_id' => 47,
            ),
            23 => 
            array (
                'employee_id' => 13650,
                'job_id' => 36,
            ),
            24 => 
            array (
                'employee_id' => 13651,
                'job_id' => 100,
            ),
            25 => 
            array (
                'employee_id' => 13652,
                'job_id' => 33,
            ),
            26 => 
            array (
                'employee_id' => 13654,
                'job_id' => 53,
            ),
            27 => 
            array (
                'employee_id' => 13655,
                'job_id' => 45,
            ),
            28 => 
            array (
                'employee_id' => 13656,
                'job_id' => 54,
            ),
            29 => 
            array (
                'employee_id' => 13657,
                'job_id' => 32,
            ),
            30 => 
            array (
                'employee_id' => 13657,
                'job_id' => 116,
            ),
            31 => 
            array (
                'employee_id' => 13657,
                'job_id' => 117,
            ),
            32 => 
            array (
                'employee_id' => 13657,
                'job_id' => 120,
            ),
            33 => 
            array (
                'employee_id' => 13657,
                'job_id' => 121,
            ),
            34 => 
            array (
                'employee_id' => 13658,
                'job_id' => 60,
            ),
            35 => 
            array (
                'employee_id' => 13659,
                'job_id' => 55,
            ),
            36 => 
            array (
                'employee_id' => 13660,
                'job_id' => 58,
            ),
            37 => 
            array (
                'employee_id' => 13661,
                'job_id' => 115,
            ),
            38 => 
            array (
                'employee_id' => 13662,
                'job_id' => 4,
            ),
            39 => 
            array (
                'employee_id' => 13663,
                'job_id' => 37,
            ),
            40 => 
            array (
                'employee_id' => 13663,
                'job_id' => 38,
            ),
            41 => 
            array (
                'employee_id' => 13664,
                'job_id' => 42,
            ),
            42 => 
            array (
                'employee_id' => 13664,
                'job_id' => 43,
            ),
            43 => 
            array (
                'employee_id' => 13665,
                'job_id' => 50,
            ),
            44 => 
            array (
                'employee_id' => 13666,
                'job_id' => 57,
            ),
            45 => 
            array (
                'employee_id' => 13667,
                'job_id' => 62,
            ),
            46 => 
            array (
                'employee_id' => 13668,
                'job_id' => 67,
            ),
            47 => 
            array (
                'employee_id' => 13669,
                'job_id' => 74,
            ),
            48 => 
            array (
                'employee_id' => 13670,
                'job_id' => 79,
            ),
            49 => 
            array (
                'employee_id' => 13670,
                'job_id' => 80,
            ),
            50 => 
            array (
                'employee_id' => 13670,
                'job_id' => 153,
            ),
            51 => 
            array (
                'employee_id' => 13671,
                'job_id' => 83,
            ),
            52 => 
            array (
                'employee_id' => 13672,
                'job_id' => 85,
            ),
            53 => 
            array (
                'employee_id' => 13673,
                'job_id' => 89,
            ),
            54 => 
            array (
                'employee_id' => 13674,
                'job_id' => 30,
            ),
            55 => 
            array (
                'employee_id' => 13675,
                'job_id' => 108,
            ),
            56 => 
            array (
                'employee_id' => 13676,
                'job_id' => 99,
            ),
            57 => 
            array (
                'employee_id' => 13677,
                'job_id' => 21,
            ),
            58 => 
            array (
                'employee_id' => 13677,
                'job_id' => 22,
            ),
            59 => 
            array (
                'employee_id' => 13677,
                'job_id' => 136,
            ),
            60 => 
            array (
                'employee_id' => 13678,
                'job_id' => 87,
            ),
            61 => 
            array (
                'employee_id' => 13679,
                'job_id' => 81,
            ),
            62 => 
            array (
                'employee_id' => 13680,
                'job_id' => 101,
            ),
            63 => 
            array (
                'employee_id' => 13681,
                'job_id' => 109,
            ),
            64 => 
            array (
                'employee_id' => 13682,
                'job_id' => 8,
            ),
            65 => 
            array (
                'employee_id' => 13682,
                'job_id' => 9,
            ),
            66 => 
            array (
                'employee_id' => 13684,
                'job_id' => 44,
            ),
            67 => 
            array (
                'employee_id' => 13684,
                'job_id' => 119,
            ),
            68 => 
            array (
                'employee_id' => 13685,
                'job_id' => 64,
            ),
            69 => 
            array (
                'employee_id' => 13685,
                'job_id' => 65,
            ),
            70 => 
            array (
                'employee_id' => 13687,
                'job_id' => 77,
            ),
            71 => 
            array (
                'employee_id' => 13687,
                'job_id' => 78,
            ),
            72 => 
            array (
                'employee_id' => 13688,
                'job_id' => 34,
            ),
            73 => 
            array (
                'employee_id' => 13689,
                'job_id' => 5,
            ),
            74 => 
            array (
                'employee_id' => 13689,
                'job_id' => 6,
            ),
            75 => 
            array (
                'employee_id' => 13689,
                'job_id' => 7,
            ),
            76 => 
            array (
                'employee_id' => 13690,
                'job_id' => 12,
            ),
            77 => 
            array (
                'employee_id' => 13690,
                'job_id' => 13,
            ),
            78 => 
            array (
                'employee_id' => 13691,
                'job_id' => 16,
            ),
            79 => 
            array (
                'employee_id' => 13691,
                'job_id' => 135,
            ),
            80 => 
            array (
                'employee_id' => 13692,
                'job_id' => 19,
            ),
            81 => 
            array (
                'employee_id' => 13692,
                'job_id' => 20,
            ),
            82 => 
            array (
                'employee_id' => 13693,
                'job_id' => 137,
            ),
            83 => 
            array (
                'employee_id' => 13694,
                'job_id' => 29,
            ),
            84 => 
            array (
                'employee_id' => 13695,
                'job_id' => 35,
            ),
            85 => 
            array (
                'employee_id' => 13696,
                'job_id' => 41,
            ),
            86 => 
            array (
                'employee_id' => 13697,
                'job_id' => 49,
            ),
            87 => 
            array (
                'employee_id' => 13698,
                'job_id' => 70,
            ),
            88 => 
            array (
                'employee_id' => 13698,
                'job_id' => 71,
            ),
            89 => 
            array (
                'employee_id' => 13699,
                'job_id' => 75,
            ),
            90 => 
            array (
                'employee_id' => 13699,
                'job_id' => 76,
            ),
            91 => 
            array (
                'employee_id' => 13700,
                'job_id' => 86,
            ),
            92 => 
            array (
                'employee_id' => 13702,
                'job_id' => 88,
            ),
            93 => 
            array (
                'employee_id' => 13702,
                'job_id' => 146,
            ),
            94 => 
            array (
                'employee_id' => 13703,
                'job_id' => 93,
            ),
            95 => 
            array (
                'employee_id' => 13703,
                'job_id' => 145,
            ),
            96 => 
            array (
                'employee_id' => 13704,
                'job_id' => 94,
            ),
            97 => 
            array (
                'employee_id' => 13704,
                'job_id' => 95,
            ),
            98 => 
            array (
                'employee_id' => 13704,
                'job_id' => 96,
            ),
            99 => 
            array (
                'employee_id' => 13705,
                'job_id' => 97,
            ),
            100 => 
            array (
                'employee_id' => 13705,
                'job_id' => 98,
            ),
            101 => 
            array (
                'employee_id' => 13705,
                'job_id' => 144,
            ),
            102 => 
            array (
                'employee_id' => 13706,
                'job_id' => 105,
            ),
            103 => 
            array (
                'employee_id' => 13706,
                'job_id' => 106,
            ),
            104 => 
            array (
                'employee_id' => 13706,
                'job_id' => 143,
            ),
            105 => 
            array (
                'employee_id' => 13707,
                'job_id' => 141,
            ),
            106 => 
            array (
                'employee_id' => 13709,
                'job_id' => 2,
            ),
            107 => 
            array (
                'employee_id' => 13715,
                'job_id' => 122,
            ),
            108 => 
            array (
                'employee_id' => 13728,
                'job_id' => 72,
            ),
            109 => 
            array (
                'employee_id' => 13728,
                'job_id' => 73,
            ),
            110 => 
            array (
                'employee_id' => 13729,
                'job_id' => 39,
            ),
            111 => 
            array (
                'employee_id' => 13729,
                'job_id' => 40,
            ),
            112 => 
            array (
                'employee_id' => 13730,
                'job_id' => 56,
            ),
            113 => 
            array (
                'employee_id' => 13731,
                'job_id' => 82,
            ),
            114 => 
            array (
                'employee_id' => 13732,
                'job_id' => 14,
            ),
            115 => 
            array (
                'employee_id' => 13732,
                'job_id' => 15,
            ),
            116 => 
            array (
                'employee_id' => 13733,
                'job_id' => 25,
            ),
            117 => 
            array (
                'employee_id' => 13733,
                'job_id' => 26,
            ),
            118 => 
            array (
                'employee_id' => 13733,
                'job_id' => 150,
            ),
            119 => 
            array (
                'employee_id' => 13733,
                'job_id' => 151,
            ),
            120 => 
            array (
                'employee_id' => 13734,
                'job_id' => 10,
            ),
            121 => 
            array (
                'employee_id' => 13734,
                'job_id' => 11,
            ),
            122 => 
            array (
                'employee_id' => 13735,
                'job_id' => 92,
            ),
            123 => 
            array (
                'employee_id' => 13736,
                'job_id' => 27,
            ),
            124 => 
            array (
                'employee_id' => 13737,
                'job_id' => 17,
            ),
            125 => 
            array (
                'employee_id' => 13738,
                'job_id' => 51,
            ),
            126 => 
            array (
                'employee_id' => 13738,
                'job_id' => 52,
            ),
            127 => 
            array (
                'employee_id' => 13739,
                'job_id' => 61,
            ),
            128 => 
            array (
                'employee_id' => 13739,
                'job_id' => 118,
            ),
            129 => 
            array (
                'employee_id' => 13740,
                'job_id' => 63,
            ),
            130 => 
            array (
                'employee_id' => 13741,
                'job_id' => 68,
            ),
            131 => 
            array (
                'employee_id' => 13741,
                'job_id' => 69,
            ),
            132 => 
            array (
                'employee_id' => 13742,
                'job_id' => 102,
            ),
            133 => 
            array (
                'employee_id' => 13742,
                'job_id' => 103,
            ),
            134 => 
            array (
                'employee_id' => 13742,
                'job_id' => 104,
            ),
            135 => 
            array (
                'employee_id' => 13776,
                'job_id' => 154,
            ),
            136 => 
            array (
                'employee_id' => 13776,
                'job_id' => 155,
            ),
            137 => 
            array (
                'employee_id' => 15369,
                'job_id' => 138,
            ),
            138 => 
            array (
                'employee_id' => 15369,
                'job_id' => 139,
            ),
            139 => 
            array (
                'employee_id' => 15370,
                'job_id' => 140,
            ),
            140 => 
            array (
                'employee_id' => 15541,
                'job_id' => 125,
            ),
            141 => 
            array (
                'employee_id' => 15630,
                'job_id' => 126,
            ),
            142 => 
            array (
                'employee_id' => 15677,
                'job_id' => 148,
            ),
            143 => 
            array (
                'employee_id' => 15801,
                'job_id' => 142,
            ),
            144 => 
            array (
                'employee_id' => 15809,
                'job_id' => 147,
            ),
            145 => 
            array (
                'employee_id' => 15809,
                'job_id' => 152,
            ),
            146 => 
            array (
                'employee_id' => 15826,
                'job_id' => 149,
            ),
            147 => 
            array (
                'employee_id' => 17542,
                'job_id' => 157,
            ),
            148 => 
            array (
                'employee_id' => 17967,
                'job_id' => 159,
            ),
            149 => 
            array (
                'employee_id' => 18664,
                'job_id' => 156,
            ),
            150 => 
            array (
                'employee_id' => 18664,
                'job_id' => 158,
            ),
            151 => 
            array (
                'employee_id' => 55404,
                'job_id' => 160,
            ),
        ));
        
        
    }
}