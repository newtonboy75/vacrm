<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\ApplicantRecordController;
use App\Http\Controllers\Backend\SourcingRecordController;
use App\Http\Controllers\Backend\TalentAcquisitionRecordController;
use App\Http\Controllers\Backend\ReferenceCheckController;
use App\Http\Controllers\Backend\HomeEvaluationRecordController;
use App\Http\Controllers\Backend\AdministrativeController;
use App\Http\Controllers\Backend\DashboardController;
use App\ApplicantReference;
use App\ReferenceRecord;
use App\TrainingRecord;
use App\ApplicantRecord;
use App\Http\Controllers\Backend\PreTrainingRequirementsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::post('create', function(Request $request) {
  $applicant_create = new ApplicantRecordController;
    return $applicant_create->create($request);
});

Route::post('change_cv', function(Request $request) {
  $applicant_create = new ApplicantRecordController;
    return $applicant_create->change_cv($request);
});

Route::get('load', function(Request $request) {
  $applicant_create = new ApplicantRecordController;
    return $applicant_create->load($request);
});

Route::get('load-talent', function(Request $request) {
  $applicant_create = new TalentAcquisitionRecordController;
    return $applicant_create->load($request);
});


Route::get('load-ref', function(Request $request) {
  $applicant_create = new ReferenceCheckController;
    return $applicant_create->load($request);
});

Route::post('login_remote', function(Request $request) {
  $applicant_create = new ApplicantRecordController;
  return $applicant_create->login_remote($request);
});

Route::post('check_progress', function(Request $request) {
  $applicant_create = new ApplicantRecordController;
  return $applicant_create->check_progress($request);
});

Route::post('submit_form_ref', function(Request $request) {
  $applicant_create = new ReferenceCheckController;
  $applicant_references = new ApplicantReference;
  $reference = new ReferenceRecord;
  return $applicant_create->submit_form_ref($request, $applicant_references, $reference);
});

Route::post('create_remote', function(Request $request) {
  $applicant_create = new HomeEvaluationRecordController;
  return $applicant_create->create_remote_home($request);
});

Route::post('invitee_created', function(Request $request) {
  $applicant_create = new AdministrativeController;
  return $applicant_create->invitee_created($request);
});

Route::post('invitee_cancelled', function(Request $request) {
  $applicant_create = new AdministrativeController;
  return $applicant_create->invitee_cancelled($request);
});

Route::post('get_interview_info', function(Request $request) {
  $applicant_create = new AdministrativeController;
  return $applicant_create->get_interview_info($request);
});

Route::post('set_sched', function(Request $request) {
  $applicant_create = new AdministrativeController;
  return $applicant_create->set_sched($request);
});

Route::post('delete_schedule', function(Request $request) {
  $applicant_create = new AdministrativeController;
  return $applicant_create->delete_schedule($request);
});

Route::post('get_available_sched', function(Request $request) {
  $applicant_create = new DashboardController;
  return $applicant_create->load_interview_schedules($request);
});

Route::post('submit_pre_training', function(Request $request) {
  $training_records = new TrainingRecord;
  $applicantRecord = new ApplicantRecord;
  $submit_pre_training = new PreTrainingRequirementsController;
  return $submit_pre_training->update($request, $training_records, $applicantRecord);
});

Route::post('get_interview_sched', function(Request $request) {
  $sched = new ApplicantRecordController;
  return $sched->get_sched_remote($request);
});

Route::post('schedule_change', function(Request $request) {
  $sched = new ApplicantRecordController;
  return $sched->change_schedule_remote($request);
});

Route::post('reset_pass', function(Request $request) {
  $sched = new ApplicantRecordController;
  return $sched->reset_pass($request);
});
