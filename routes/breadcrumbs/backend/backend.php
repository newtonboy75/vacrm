<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.sourcing-record', function ($trail) {
    $trail->push('Sourcing Record', route('admin.sourcing-record'));
});

Breadcrumbs::for('admin.talent-acquisition-record', function ($trail) {
    $trail->push('Talent Acquisition Record', route('admin.talent-acquisition-record'));
});

Breadcrumbs::for('admin.reference-check', function ($trail) {
    $trail->push('Reference Check', route('admin.reference-check'));
});

Breadcrumbs::for('admin.home-evaluation-record', function ($trail) {
    $trail->push('Home Evaluation Record', route('admin.home-evaluation-record'));
});

Breadcrumbs::for('admin.pre-training-requirements', function ($trail) {
    $trail->push('Pre Training Requirements', route('admin.pre-training-requirements'));
});

Breadcrumbs::for('admin.training-record', function ($trail) {
    $trail->push('Training Record', route('admin.training-record'));
});

Breadcrumbs::for('admin.placement-record', function ($trail) {
    $trail->push('Placement Record', route('admin.placement-record'));
});

Breadcrumbs::for('admin.hr-operation-record', function ($trail) {
    $trail->push('Operation Record', route('admin.hr-operation-record'));
});

Breadcrumbs::for('admin.sourcing-record.edit', function ($trail, $id) {
    $trail->push('Sourcing Record Edit ', route('admin.sourcing-record.edit', $id));
});

Breadcrumbs::for('admin.talent-acquisition-record.edit', function ($trail, $id) {
    $trail->push('Talent Acquisition Record', route('admin.talent-acquisition-record.edit', $id));
});

Breadcrumbs::for('admin.reference-check.edit', function ($trail, $id) {
    $trail->push('Reference Check', route('admin.reference-check.edit', $id));
});

Breadcrumbs::for('admin.home-evaluation-record.edit', function ($trail, $id) {
    $trail->push('Home Evaluation Record', route('admin.home-evaluation-record.edit', $id));
});

Breadcrumbs::for('admin.pre-training-requirements.edit', function ($trail, $id) {
    $trail->push('Pre-Training Requirements', route('admin.pre-training-requirements.edit', $id));
});

Breadcrumbs::for('admin.training-record.edit', function ($trail, $id) {
    $trail->push('Training Record', route('admin.training-record.edit', $id));
});

Breadcrumbs::for('admin.recruiters-hub', function ($trail) {
    $trail->push('Recruiters Hub', route('admin.recruiters-hub'));
});

Breadcrumbs::for('admin.placement-record.edit', function ($trail, $id) {
    $trail->push('Placement Record', route('admin.placement-record.edit', $id));
});

Breadcrumbs::for('admin.hr-operation-record.edit', function ($trail, $id) {
    $trail->push('Operation Record', route('admin.hr-operation-record.edit', $id));
});

Breadcrumbs::for('admin.employee', function ($trail, $id) {
    $trail->push('Employee', route('admin.employee', $id));
});

Breadcrumbs::for('admin.clients', function ($trail) {
    $trail->push('Clients', route('admin.clients'));
});

Breadcrumbs::for('admin.client', function ($trail, $id) {
    $trail->push('Client', route('admin.client', $id));
});

Breadcrumbs::for('admin.esig_final_notice', function ($trail, $id) {
    $trail->push('Send Final Notice', route('admin.esig_final_notice', $id));
});

Breadcrumbs::for('admin.disclosure-form', function ($trail, $id) {
    $trail->push('Disclosure Form', route('admin.disclosure-form', $id));
});

Breadcrumbs::for('admin.caf', function ($trail, $id) {
    $trail->push('Title Here', route('admin.caf', $id));
});

Breadcrumbs::for('admin.esignature', function ($trail) {
    $trail->push('Esignature', route('admin.esignature'));
});

Breadcrumbs::for('admin.admin-log', function ($trail) {
    $trail->push('Admin Log', route('admin.admin-log'));
});

Breadcrumbs::for('admin.outbox', function ($trail) {
    $trail->push('Outbox', route('admin.outbox'));
});

Breadcrumbs::for('admin.profile', function ($trail, $action) {
    $trail->push('Profile', route('admin.profile', $action));
});


require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
