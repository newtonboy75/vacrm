<?php

use App\Http\Controllers\Backend\HrOperationRecordController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);

Route::group(['prefix'=>'hr-operation-record'], function(){
    Route::get('/', ['as' => 'hr-operation-record', 'uses' => 'HrOperationRecordController@index'])->middleware('role:administrator|hr|coaches|placement specialist|training manager|country manager|client services manager');
    Route::get('load', ['as' => 'load', 'uses' => 'HrOperationRecordController@load'])->name('hr-operation-record.load');
    Route::get('{id}/edit', [HrOperationRecordController::class, 'edit'])->name('hr-operation-record.edit')->middleware('role:administrator|hr|coaches|placement specialist|training manager|country manager:client services manager');
    Route::post('update', ['as' => 'update', 'uses' => 'HrOperationRecordController@update']);
    Route::post('get_groups', ['as' => 'update', 'uses' => 'HrOperationRecordController@get_groups']);
    Route::post('get_coaches', ['as' => 'update', 'uses' => 'HrOperationRecordController@get_coaches']);
    Route::post('delete', ['as' => 'delete', 'uses' => 'HrOperationRecordController@delete']);
    Route::post('get_contract', ['as' => 'get_contract', 'uses' => 'HrOperationRecordController@get_contract']);
    Route::post('employee', ['as' => 'employee', 'uses' => 'HrOperationRecordController@employee']);
    Route::post('load-contract', ['as' => 'load-contract', 'uses' => 'HrOperationRecordController@load_contract']);
    Route::get('success/{id}/{uid}', ['as' => 'success', 'uses' => 'HrOperationRecordController@success']);
    Route::get('contract_lists/{id}', ['as' => 'contract_lists', 'uses' => 'HrOperationRecordController@contract_lists'])->name('hr-operation-record.contract_lists');
    Route::post('esig_final_notice', ['as' => 'esig_final_notice', 'uses' => 'HrOperationRecordController@esig_final_notice2']);
    Route::get('esig_final_notice/{id}/{type?}/{memoid?}', ['as' => 'esig_final_notice', 'uses' => 'HrOperationRecordController@esig_final_notice']);
    Route::post('esig-hold', ['as' => 'esig-hold', 'uses' => 'HrOperationRecordController@esig_hold']);
    Route::get('esig-gva-scorecard/{id}', ['as' => 'esig-gva-scorecard', 'uses' => 'HrOperationRecordController@gva_scorecard'])->name('hr-operation-record.esig-gva-scorecard');
    Route::get('esig-isa-scorecard/{id}/{type}', ['as' => 'esig-isa-scorecard', 'uses' => 'HrOperationRecordController@scorecard'])->name('hr-operation-record.esig-isa-scorecard');
    Route::post('va-checkin', ['as' => 'va-checkin', 'uses' => 'PdfCreatorController@va_checkin']);
    Route::get('disclosure-form/{id}', ['as' => 'disclosure-form', 'uses' => 'HrOperationRecordController@disclosure_form']);
    Route::get('memorandum-form/{id}', ['as' => 'memorandum-form', 'uses' => 'HrOperationRecordController@memorandum_form']);
    Route::get('assurance-form/{id}', ['as' => 'assurance-form', 'uses' => 'HrOperationRecordController@assurance_form']);
    Route::get('codeofconduct-form/{id}', ['as' => 'codeofconduct-form', 'uses' => 'HrOperationRecordController@coc_form']);
    Route::get('ica-form/{id}', ['as' => 'ica-form', 'uses' => 'HrOperationRecordController@ica_form']);
    Route::get('nca-form/{id}', ['as' => 'nca-form', 'uses' => 'HrOperationRecordController@nca_form']);
    Route::post('send_final_notice', ['as' => 'send_final_notice', 'uses' => 'PdfCreatorController@send_final_notice']);
    Route::post('uploader', ['as' => 'uploader', 'uses' => 'PdfCreatorController@uploader']);
    Route::get('fte-form', ['as' => 'fte-form', 'uses' => 'HrOperationRecordController@fte_form']);
    Route::get('fte-form-get/{id}', ['as' => 'fte-form-get', 'uses' => 'HrOperationRecordController@fte_form_get']);
    Route::post('termination-form', ['as' => 'termination-form', 'uses' => 'HrOperationRecordController@termination']);
    Route::post('termination-form-send', ['as' => 'termination-form-send', 'uses' => 'HrOperationRecordController@terminationSend']);
    Route::get('download/{id}', ['as' => 'download', 'uses' => 'HrOperationRecordController@download']);
    Route::get('caf/{id}/{memoid?}', ['as' => 'caf', 'uses' => 'HrOperationRecordController@caf']);
    Route::post('caf-form-send', ['as' => 'caf-form-send', 'uses' => 'HrOperationRecordController@cafSend']);
    Route::post('delete-checkin', ['as' => 'delete-checkin', 'uses' => 'HrOperationRecordController@delChecking']);
    Route::post('get_clients', ['uses' => 'HrOperationRecordController@get_clients']);
});
