<?php

use App\Http\Controllers\Backend\ClientsRecordController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);

Route::group(['prefix'=>'clients'], function(){
    Route::get('/', ['as' => 'clients', 'uses' => 'ClientsRecordController@index'])->middleware('role:administrator|recruiter manager|country manager|training manager|coaches|client services manager');
    Route::get('client/{id}', ['as' => 'client', 'uses' => 'ClientsRecordController@client_info'])->middleware('role:administrator|recruiter manager|country manager|training manager|coaches|client services manager');
    Route::get('load', ['uses' => 'ClientsRecordController@load']);
    Route::get('list', ['uses' => 'ClientsRecordController@list']);
    Route::post('save', ['uses' => 'ClientsRecordController@save_new_client']);
    Route::post('destroy', ['uses' => 'ClientsRecordController@destroy_client']);
    Route::post('get_client_lists', ['uses' => 'ClientsRecordController@get_client_lists']);
    Route::post('get_va_lists', ['uses' => 'ClientsRecordController@get_va_lists']);
    Route::post('client_placement', ['uses' => 'ClientsRecordController@client_placement']);
    Route::get('cancellation', ['uses' => 'ClientsRecordController@load_cancellation']);
    Route::get('replacement', ['uses' => 'ClientsRecordController@load_replacements']);
    Route::get('suspension', ['uses' => 'ClientsRecordController@load_suspension']);
    Route::get('stats', ['uses' => 'ClientsRecordController@load_stats']);
    Route::get('years', ['uses' => 'ClientsRecordController@load_stats_years']);
    Route::get('lists', ['uses' => 'ClientsRecordController@list']);
    Route::post('remove', ['uses' => 'ClientsRecordController@remove_client']);
    Route::get('search', ['uses' => 'ClientsRecordController@search']);
    Route::post('save_info', ['uses' => 'ClientsRecordController@save_info']);
    Route::post('update_client_info', ['uses' => 'ClientsRecordController@update_client_info']);
    Route::get('single/{id}', ['uses' => 'ClientsRecordController@single']);
    Route::post('new_client_info', ['uses' => 'ClientsRecordController@new_client_info']);
    Route::post('notes', ['uses' => 'ClientsRecordController@notes']);
    Route::get('note/{id}', ['uses' => 'ClientsRecordController@note']);
    Route::post('uploader', ['uses' => 'ClientsRecordController@uploader']);
    Route::post('get-note', ['uses' => 'ClientsRecordController@getNote']);
    Route::post('destroy-note', ['uses' => 'ClientsRecordController@destroyNote']);
});
