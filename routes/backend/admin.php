<?php
date_default_timezone_set('Asia/Manila');
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\AdministrativeController;
use App\Http\Controllers\Backend\SmsSender;
use App\Http\Controllers\Backend\AdminLogController;
use App\Http\Controllers\Backend\ApplicantRecordController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::post('dashboard/mailer', [DashboardController::class, 'mailer'])->name('mailer');
Route::get('recruiters-hub', [DashboardController::class, 'recruiters_hub'])->name('recruiters-hub')->middleware('role:administrator|recruiter|recruiter manager|training_manager|country_manager');
Route::post('remove_exp', [AdministrativeController::class, 'remove_exp'])->name('remove_exp');

Route::post('invitee_created', [AdministrativeController::class, 'invitee_created'])->name('invitee_created');
Route::post('invitee_cancelled', [AdministrativeController::class, 'invitee_cancelled'])->name('invitee_cancelled');

Route::post('load_interview_schedules', [DashboardController::class, 'load_interview_schedules'])->name('load_interview_schedules');
Route::post('load_recruiters', [DashboardController::class, 'load_recruiters'])->name('load_recruiters');
Route::post('set_sched_by_recruiter', [AdministrativeController::class, 'set_sched_by_recruiter'])->name('set_sched_by_recruiter');
Route::post('get_interview_info_by_recruiter', [AdministrativeController::class, 'get_interview_info_by_recruiter'])->name('get_interview_info_by_recruiter');
Route::post('set_schedule_by_recruiter', [AdministrativeController::class, 'set_schedule_by_recruiter'])->name('set_schedule_by_recruiter');
Route::post('send_sms', [SmsSender::class, 'sendSMS'])->name('sendSMS');
Route::get('esignature/{type?}/{to?}/{memoid?}/{id?}', [DashboardController::class, 'esignature'])->name('esignature');
Route::post('esignature-save', [DashboardController::class, 'esignature_save'])->name('esignature-save');
Route::post('esignature-send', [DashboardController::class, 'esignature_send'])->name('esignature-send');
Route::post('avatar', [DashboardController::class, 'avatar'])->name('avatar');
Route::get('admin-log', [AdminLogController::class, 'index'])->name('admin-log');
Route::get('admin-log-load', [AdminLogController::class, 'load'])->name('admin-log-load');
Route::get('outbox', [AdministrativeController::class, 'outbox'])->name('outbox');
Route::post('outbox-type', [AdministrativeController::class, 'outboxType'])->name('outbox-type');
Route::post('outbox-body', [AdministrativeController::class, 'outboxBody'])->name('outbox-body');
Route::get('download-file/{path?}/{filename?}', [ApplicantRecordController::class, 'downloadFromS3'])->name('download-file');
Route::get('refresh-server', [AdministrativeController::class, 'performServerRefresh'])->name('refresh-server')->middleware('role:administrator|recruiter|recruiter manager|training_manager|country_manager');
Route::get('read_csv', [AdministrativeController::class, 'read_csv'])->name('read_csv');
Route::get('process_csv/{filename}', ['as' => 'view', 'uses' => 'AdministrativeController@process_csv']);
Route::get('profile/{action}', [DashboardController::class, 'profile'])->name('profile');
Route::post('profile-update', [DashboardController::class, 'profile_update'])->name('profile-update');
Route::get('get-chart/{date_from}/{date_to}/{url}', ['uses' => 'AdministrativeController@getChart']);
Route::post('export-ref/{tab}', ['uses' => 'AdministrativeController@exportReferrer']);
