<?php

use App\Http\Controllers\Backend\HomeEvaluationRecordController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
//Route::get('home-evaluation-record', [HomeEvaluationRecordController::class, 'index'])->name('home-evaluation-record');

Route::group(['prefix'=>'home-evaluation-record'], function(){
    Route::get('/', ['as' => 'home-evaluation-record', 'uses' => 'HomeEvaluationRecordController@index'])->middleware('role:administrator|trainer|training manager|country manager|hr');
    Route::get('load', ['as' => 'load', 'uses' => 'HomeEvaluationRecordController@load'])->name('home-evaluation-record.load');
    Route::get('{id}/edit', [HomeEvaluationRecordController::class, 'edit'])->name('home-evaluation-record.edit')->middleware('role:administrator|trainer|training manager|country manager|hr');
    Route::post('update', ['uses' => 'HomeEvaluationRecordController@update']);
    Route::post('upload', ['uses' => 'HomeEvaluationRecordController@uploadImage']);
    Route::post('send_email', ['uses' => 'HomeEvaluationRecordController@sendEmail']);
});
