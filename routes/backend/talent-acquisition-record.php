<?php

use App\Http\Controllers\Backend\TalentAcquisitionRecordController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);


Route::group(['prefix'=>'talent-acquisition-record'], function(){
    Route::get('/', ['as' => 'talent-acquisition-record', 'uses' => 'TalentAcquisitionRecordController@index'])->middleware('role:administrator|sourcer|recruiter manager|recruiter|training_manager|country_manager');
    Route::get('load', ['as' => 'load', 'uses' => 'TalentAcquisitionRecordController@load'])->name('talent-acquisition-record.load');
    Route::get('{id}/edit', [TalentAcquisitionRecordController::class, 'edit'])->name('talent-acquisition-record.edit')->middleware('role:administrator|sourcer|recruiter manager|recruiter|training_manager|country_manager');
    Route::post('update', ['uses' => 'TalentAcquisitionRecordController@update']);
    Route::post('delete', ['uses' => 'TalentAcquisitionRecordController@delete']);
    Route::post('send_email', ['uses' => 'TalentAcquisitionRecordController@sendEmail']);
    Route::post('resched', ['uses' => 'TalentAcquisitionRecordController@resched']);

});
