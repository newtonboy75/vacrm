<?php

use App\Http\Controllers\Backend\TrainingRecordController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);

Route::group(['prefix'=>'training-record'], function(){
    Route::get('/', ['as' => 'training-record', 'uses' => 'TrainingRecordController@index'])->middleware('role:administrator|trainer|training manager|country manager');
    Route::get('load', ['as' => 'load', 'uses' => 'TrainingRecordController@load'])->name('training-record.load');
    Route::get('{id}/edit', [TrainingRecordController::class, 'edit'])->name('training-record.edit')->middleware('role:administrator|trainer|training manager|country manager');
    Route::post('update', ['uses' => 'TrainingRecordController@update'])->name('training-record.update');
    Route::post('create_placement_record', ['uses' => 'TrainingRecordController@create_placement_record'])->name('training-record.create_placement_record');
});
