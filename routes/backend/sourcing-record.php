<?php

use App\Http\Controllers\Backend\SourcingRecordController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
//Route::get('sourcing-record', [SourcingRecordController::class, 'index'])->name('sourcing-record');

Route::group(['prefix'=>'sourcing-record'], function(){
    Route::get('/', ['as' => 'sourcing-record', 'uses' => 'SourcingRecordController@index'])->middleware('role:administrator|sourcer|recruiter manager|recruiter|training manager|country manager');
    Route::get('load', ['as' => 'load', 'uses' => 'ApplicantRecordController@load'])->name('sourcing-record.load');
    Route::get('{id}/edit', [SourcingRecordController::class, 'edit'])->name('sourcing-record.edit')->middleware('role:administrator|sourcer|recruiter manager|recruiter|training manager|country manager');
    Route::post('update', ['uses' => 'SourcingRecordController@update']);
    Route::post('delete', ['uses' => 'SourcingRecordController@delete']);
    Route::post('send_email', ['uses' => 'SourcingRecordController@sendEmail']);
    Route::get('{id}/delete', [SourcingRecordController::class, 'deleteApplication'])->name('sourcing-record.delete');
    Route::post('cv_upload', ['uses' => 'SourcingRecordController@cvUpload']);    
});
