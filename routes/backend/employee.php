<?php

use App\Http\Controllers\Backend\PreTrainingRequirementsController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);

Route::group(['prefix'=>'employee'], function(){
    Route::get('{id}', [PreTrainingRequirementsController::class, 'edit'])->name('employee');
});
