<?php

use App\Http\Controllers\Backend\PreTrainingRequirementsController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
//Route::get('pre-training-requirements', [PreTrainingRequirementsController::class, 'index'])->name('pre-training-requirements');

Route::group(['prefix'=>'pre-training-requirements'], function(){
    Route::get('/', ['as' => 'pre-training-requirements', 'uses' => 'PreTrainingRequirementsController@index'])->middleware('role:administrator|recruiter manager|training manager|country manager|hr');
    Route::get('load', ['as' => 'load', 'uses' => 'PreTrainingRequirementsController@load'])->name('pre-training-requirements.load');
    Route::get('{id}/edit', [PreTrainingRequirementsController::class, 'edit'])->name('pre-training-requirements.edit')->middleware('role:administrator|recruiter manager|training manager|country manager|hr');
    Route::post('update', ['uses' => 'PreTrainingRequirementsController@update']);
    Route::post('delete', ['uses' => 'PreTrainingRequirementsController@delete']);
    Route::post('create_training_record', ['uses' => 'PreTrainingRequirementsController@create_training_record']);
    Route::get('revoke/{id}', [PreTrainingRequirementsController::class, 'revoke'])->name('pre-training-requirements.revoke');
    Route::post('trainers', ['uses' => 'PreTrainingRequirementsController@get_trainers']);
    Route::get('batches', ['uses' => 'PreTrainingRequirementsController@batches']);
    Route::post('batch', ['uses' => 'PreTrainingRequirementsController@batch']);
    Route::post('destroy-batch', ['uses' => 'PreTrainingRequirementsController@destroy_batch']);
    Route::post('batch-trainers', ['uses' => 'PreTrainingRequirementsController@batch_trainers']);
    Route::post('batch-email', ['uses' => 'PreTrainingRequirementsController@sendNotificationBatch']);
    Route::post('get-new-batches', ['uses' => 'PreTrainingRequirementsController@getNewBatch']);
    Route::post('get-batch-details', ['uses' => 'PreTrainingRequirementsController@getNewBatchDetails']);
    Route::post('batch-get', ['uses' => 'PreTrainingRequirementsController@getNewBatcheshDetails']);
    //Route::post('batch-date', ['uses' => 'PreTrainingRequirementsController@getNewBatchDates']);
});
