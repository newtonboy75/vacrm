<?php

use App\Http\Controllers\Backend\PlacementRecordController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);


Route::group(['prefix'=>'placement-record'], function(){
    Route::get('/', ['as' => 'placement-record', 'uses' => 'PlacementRecordController@index'])->middleware('role:administrator|coaches|placement specialist| trainer|training manager|country manager|hr|client services manager');
    Route::get('load', ['as' => 'load', 'uses' => 'PlacementRecordController@load'])->name('placement-record.load');
    Route::get('{id}/edit', [PlacementRecordController::class, 'edit'])->name('placement-record.edit')->middleware('role:administrator|coaches|placement specialist| trainer|training manager|country manager|hr|client services manager');
    Route::post('update', ['uses' => 'PlacementRecordController@update']);
    Route::post('delete', ['uses' => 'PlacementRecordController@delete']);
    Route::post('add_new_client', ['uses' => 'PlacementRecordController@add_new_client']);
    Route::post('hired', ['uses' => 'PlacementRecordController@hired']);
    Route::post('get_clients', ['uses' => 'PlacementRecordController@get_clients']);
    Route::post('create_hr_record', ['uses' => 'PlacementRecordController@create_hr_record']);
});
