<?php

use App\Http\Controllers\Backend\ReferenceCheckController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
//Route::get('reference-check', [ReferenceCheckController::class, 'index'])->name('reference-check');


Route::group(['prefix'=>'reference-check'], function(){
    Route::get('/', ['as' => 'reference-check', 'uses' => 'ReferenceCheckController@index'])->middleware('role:administrator|hr|training manager|country manager');
    Route::get('load', ['as' => 'load', 'uses' => 'ReferenceCheckController@load'])->name('reference-check.load');
    Route::get('{id}/edit', [ReferenceCheckController::class, 'edit'])->name('reference-check.edit')->middleware('role:administrator|hr|training manager|country manager');
    Route::post('update', ['uses' => 'ReferenceCheckController@update']);
    Route::post('delete', ['uses' => 'ReferenceCheckController@delete']);
    Route::post('send_email', ['uses' => 'ReferenceCheckController@sendEmail']);
});
